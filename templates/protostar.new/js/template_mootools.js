/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @since       3.2
 */
 
function cleanInsureDIYPopup(div_id) {
	$(div_id).setStyle('display','none');
}

function showInsureDIYPopup(div_id) {
	$(div_id).setStyle('display','block');
}

function isNumberKey(evt)
{
  var charCode = (evt.which) ? evt.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
	}
  return true;
}