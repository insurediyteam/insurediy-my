<?php

/**
 * @package     Joomla.Site
 * @subpackage  mod_checkbox
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Helper for mod_checkbox
 *
 * @package     Joomla.Site
 * @subpackage  mod_checkbox
 * @since       3.0
 */
class ModCheckboxHelper {

	public static function getType() {
		return "";
	}

}
