<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<script type="text/javascript">
	function flag_send_post(mylink) {
		jQuery('#flag_send_form').attr('action', mylink);
		jQuery('#flag_send_form').submit();
	}
</script>
<form id="flag_send_form" method="post" action="">
	<ul class="insurediy-flag-country<?php echo $moduleclass_sfx; ?>">
		<li><a href="#" onclick="javascript:flag_send_post('<?php echo $params->get('link1') ?>')"><img <?php echo ($params->get('glow1')?'class="active"':'') ?> src="<?php echo $params->get('flag1')?>" alt="flag1" /></a></li>
		<li><a href="#" onclick="javascript:flag_send_post('<?php echo $params->get('link2') ?>')"><img <?php echo ($params->get('glow2')?'class="active"':'') ?> src="<?php echo $params->get('flag2')?>" alt="flag1" /></a></li>
		<li><a href="#" onclick="javascript:flag_send_post('<?php echo $params->get('link3') ?>')"><img <?php echo ($params->get('glow3')?'class="active"':'') ?> src="<?php echo $params->get('flag3')?>" alt="flag1" /></a></li>
	</ul>
	<input type="hidden" name="flag" value="0" />
</form>
