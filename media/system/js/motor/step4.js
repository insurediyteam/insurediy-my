import variables from './variables';
import functions from './functions';
import urijs from 'urijs';
import _ from 'lodash';
import Swal from 'sweetalert2';
import axios from '../plugins/axios';
import qs from 'querystring';

jQuery(".back-5-btn, .back-last-choose-insurer").on('click', function (e) {
    functions.quoteStep(3);

    functions.saveQuote();

    jQuery(".motor-quote-result-middle-wrapper").addClass("res").removeClass("hide");
    jQuery('.pad.insurediy-form').css('padding-top', '0');
    jQuery(".insurediy-form.main-form").addClass("pad");
    jQuery(".motor-quote-result-middle-wrapper").show();

    jQuery('.my-step4,.my-last-choose-insurer,.comparison-table,.motor-result-column-dummy').hide();
    jQuery('.my-step3').show();
    let motorResultTable = jQuery(`.motor-quote-result-middle-wrapper .motor-result-row.first`);
    motorResultTable.removeClass('fixed');
    jQuery('.motor-quote-result-block-body, .motor-quote-premium-header, .motor-quote-diy-points').removeClass('my-transition-hide');
    jQuery('.motor-quote-result-block-header').removeClass('height-auto');
    jQuery(`.motor-result-column`).not(`.motor-result-column-dummy`).attr('style', '');

    functions.resetQuoteResultsStyle();
    functions.lockBuyNowBtn();
    variables.enableFixedQuotes = true;
    functions.updateBuyNowBtnTitle();

    functions.scrollTop(0);
});

jQuery('#continue-5-btn').on('click', function () {
    if (jQuery(`#squaredFour3`).prop('checked')) {
        let thisElement = jQuery(this);
        let elementSiblings = thisElement.siblings();

        if (document.paymentForm.orderRef.value && document.paymentForm.orderRef.value !== 'undefined') {
            thisElement.prop('disabled', true).LoadingOverlay('show');
            elementSiblings.prop('disabled', true).LoadingOverlay('show');

            let addOrderRefToUrl = (name) => {
                let url = urijs(document.paymentForm[`${name}Url`].value);
                url.addQuery('orderRef', document.paymentForm.orderRef.value);
                document.paymentForm[`${name}Url`].value = url.toString();
            }

            let names = [
                'success',
                'fail',
                'cancel',
            ].forEach(addOrderRefToUrl);

            functions.quoteStep(5);

            functions.saveQuote((data, status) => {
                if (status === 'success') {
                    // submit form paydollar
                    jQuery('#paymentForm').submit();

                    thisElement.prop('disabled', false).LoadingOverlay('hide', true);
                    elementSiblings.prop('disabled', false).LoadingOverlay('hide', true);
                }
            });
        }
        else {
            Swal.fire({
                icon: 'error',
                title: `Something went wrong!`,
                text: `Can't do payment.`
            });
        }
    }
    else {
        Swal.fire({
            icon: 'warning',
            title: `Attention!`,
            text: `You must agree with above statements!`
        });
    }
});

jQuery('#jform_apply_promo_code').on('submit', function (el) {
    el.preventDefault();

    let form = jQuery(this);

    let promoCode = form.find(`input[name='jform[promo_code]']`).eq(0).val();

    axios.post(functions.motorRouteAjaxJson('checkPromoCode'), qs.stringify({
        promoCode: promoCode,
    })).then((response) => {
        if (!response.data) {
            Swal.fire("Error!", `Invalid promo code: '${promoCode}'`, 'error');
            return;
        }

        functions.addPromo('CAR20', parseFloat(response.data.promo_value));

        form.hide();

        jQuery(`.nopromo-car20`).hide();
    })
    .catch((error) => {
        Swal.fire("Error!", `Invalid promo code: '${promoCode}'`, 'error');
    });

    if (promoCode === 'CAR20') {
    }
});

jQuery('#jform_apply_promo_code_referral').on('submit', function (el) {
    el.preventDefault();
    let form = jQuery(this);

    let promoCode = form.find(`input[name='jform[promo_code]']`).eq(0).val();

    let formSubmitBtn = form.find(`input[type='submit']`);
    formSubmitBtn.LoadingOverlay('show');

    jQuery.ajax({
        type: 'POST',
        url: functions.motorRouteAjaxJson(`getUserFromReferral`),
        data: {
            referral: promoCode
        },
        success: (data) => {
            if (data) {
                functions.addPromo(`Referral${promoCode}`, 5.00);

                form.hide();
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: `Invalid!`,
                    text: `Invalid referral code: ${promoCode}!`
                });
            }

            formSubmitBtn.LoadingOverlay('hide', true);
        },
        error: () => {
            formSubmitBtn.LoadingOverlay('hide', true);
        }
    });
});
