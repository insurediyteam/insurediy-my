import functions from './functions';
import variables from './variables';

// accordion close and open start
jQuery(".motor-accordion").on("click", function () {
    let currentElement = jQuery(this);
    let otherAccordion = jQuery(".motor-accordion").not(this);

    otherAccordion.addClass("active");
    otherAccordion.next()
        .css("max-height", "0")
        .css("padding-bottom", "0")
        .slideUp(variables.motorAccordionDuration, () => {
        });

    currentElement.removeClass("active");
    currentElement.next()
        .css("max-height", "5000px")
        .css("padding-bottom", "20px")
        .slideDown(variables.motorAccordionDuration, () => {
            currentElement[0].dispatchEvent(new Event(`accordion:open`));
            functions.scrollTop(currentElement.offset().top - 280);
            if(!currentElement.is(':visible')) { 
                currentElement.next().hide();
            }
        });
});
