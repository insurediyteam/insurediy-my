import variables from './variables';
import accounting from 'accounting';
import _ from 'lodash';
import stringSimilarity from 'string-similarity';
import axios from '../plugins/axios';
import qs from 'qs';
import Swal from 'sweetalert2';

const functions = {};

functions.resetQuoteResultsStyle = function () {
    jQuery('.res.motor-quote-result-middle-wrapper').addClass('fixed');
    jQuery('.res .motor-quote-result-block-body, .res .motor-quote-premium-header, .res .motor-quote-diy-points').addClass(`my-transition-hide`);
    jQuery('.res .motor-quote-result-block-header').addClass('height-auto');
    jQuery('.not-final').removeClass('hide');
    jQuery('.pad.insurediy-form').css('padding-top', 'auto');
}

functions.fixedQuoteResultsStyle = function () {
    jQuery('.res.motor-quote-result-middle-wrapper').removeClass('fixed');
    jQuery('.res .motor-quote-result-block-body, .res .motor-quote-premium-header, .res .motor-quote-diy-points').removeClass(`my-transition-hide`);
    jQuery('.res .motor-quote-result-block-header').removeClass('height-auto');
    jQuery('.not-final').addClass('hide');
    jQuery('.pad.insurediy-form').css('padding-top', '0');
}

functions.radioClicked = function (obj) {
    var otherRadios = document.getElementsByName(obj.getAttribute('name'));
    var j;
    let element = jQuery(obj);

    if (element.parent().find('label[class^="radio-label"]').hasClass("radio-label-logo")) {
        for (j = 0; j < otherRadios.length; j++) {
            jQuery(otherRadios[j]).parent().find('label[class^="radio-label"]').removeClass("active");
        }

        element.parent().find('label[class^="radio-label"]').toggleClass("active");
    } else {
        for (j = 0; j < otherRadios.length; j++) {
            jQuery(otherRadios[j]).parent().find('span.radio-label-title').removeClass("active");
        }

        element.parent().find('span.radio-label-title').toggleClass("active");
    }

    element.prop('checked', true);
}

functions.loanClicked = function (loanVal) {
    if (loanVal) {
        jQuery(".loan-company").removeClass("hide");
    } else {
        jQuery(".loan-company").addClass("hide");
    }
}

functions.basisCoverage = (coverageVal) => {
    if (coverageVal == 1) {
        jQuery("#field-helper").html("Market Value: Your vehicle is insured based on market value.");
        jQuery(".insured-title").html("<b>Market</b> Value Sum Insured");
        jQuery("#sum-insured").val(jQuery("#market-value").val());
        jQuery("#sum-insured").prop("disabled", true);
    } else {
        jQuery("#field-helper").html("Agreed Value: Your vehicle is insured based on agreed value.");
        jQuery(".insured-title").html("<b>Agreed</b> Value Sum Insured");
        let sumInsuredInput = jQuery("#sum-insured");
        sumInsuredInput.prop("disabled", false);
        sumInsuredInput.val(sumInsuredInput.data('defaultvalue'));
    }
}

functions.addDriver = function (obj) {
    let tabContainer = jQuery(obj).parents(`.tab-wrapper`).parent().parent();
    let driver = jQuery(`#driver-wrapper div.tab-wrapper`).length;
    if (driver <= variables.driverLimit) {
        functions.scrollTop(jQuery('#driver-wrapper').offset().top - 400);

        tabContainer.find(".tab-pane.d-pane").removeClass("active");

        var dateOfBirth2 = dateOfBirth.replace(/jform_dateOfBirth/g, "jform[dateOfBirth][" + driver + "]");

        jQuery(".tab-header-container.d-tab-header-container")
            .append(
                `<div class="tab-pane active d-pane" onClick="changeTab(this);" id="driver-${driver}" data-id="driver-panel-${driver}">
                <img class="tab-pane-image" alt="remove driver" src="${BASE_URL}images/websiteicon/driver-logo.png" />
                    <span class="tab-pane-title">DRIVER ${(driver + 1)}</span>
                </div>`
            );

        tabContainer.find('.tab-wrapper').addClass('display-none');

        let driverForm = jQuery(`
            <div class="tab-wrapper" id="driver-panel-${driver}", style="height: 662px;">
                <div class="panel-left">
                    <div class="input-wrapper">
                        <span class="input-title">Name</span>
                        <div class="input-container-block">
                            <input type="text" class="input-text front" required name="jform[name][]" value=""/>
                        </div>
                    </div>
                    <div class="input-wrapper">
                        <span class="input-title">IC Number</span>
                        <div class="input-container-block">
                            <input type="text" class="input-text front" required name="jform[icNum][]" value=""/>
                        </div>
                    </div>
                </div>
                <div class="panel-right">
                    <div class="input-wrapper">
                        <span class="input-title">Years of driving experience?</span>
                        <span class="input-sub-title">Based on current driving licence issued date</span>
                        <div class="input-container-block">
                            <div class="select_mate" data-mate-select="active" >
                                <select id="driver-driving-exp-${driver}" required name="jform[yearsOfDrivingExp][]" onchange="" onclick="return false;" id="" class="hide-select custom-select" required>
                                </select>
                            </div>
                                <!-- Custom select structure --> 
                        </div><!-- End div center   -->
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="input-wrapper add-driver jquery">
                    <div onClick="removeDriver(this);" style="float:left" data-id="${driver}">
                        <img class="add-driver-img" src="${BASE_URL}images/websiteicon/remove-driver-logo.png"/>
                        <span class="remove-driver-title">REMOVE THIS DRIVER</span>
                    </div>
                    <div onClick="addDriver(this);" style="float:right">
                        <img class="add-driver-img" src="${BASE_URL}images/websiteicon/add-driver-logo.png"/>
                        <span class="add-driver-title">ADD A DRIVER</span>
                    </div>
                </div>
            </div>
        `);
        let drivingExpOptions = jQuery(`[name='jform[drivingExp]'] option`).each((key, option) => {
                let optionElement = jQuery(option);
                driverForm.find(`#driver-driving-exp-${driver}`).append(`
                    <option value="${optionElement.val()}">${optionElement.text()}</option>
                `);
            });
        driverForm.find('.custom-select').select2({
            width: '300px'
        });
        tabContainer.find('.tab-content-container.d-tab-content-container').append(driverForm);

    }
}

functions.removeDriver = function (obj) {
    var dataId = jQuery(obj).data('id');
    var newId = dataId - 1;

    if (!jQuery('#driver-' + newId).length) {
        newId = 0;
    }

    jQuery("#driver-" + dataId).remove();
    jQuery("#driver-panel-" + dataId).remove();

    jQuery("#driver-wrapper .tab-pane.d-pane").removeClass("active");
    jQuery("#driver-" + newId).addClass("active");
    jQuery('#driver-wrapper .tab-wrapper').addClass('display-none');
    jQuery("#driver-panel-" + newId).removeClass("display-none");

    // reset id
    jQuery(`#driver-wrapper .tab-pane`).each((key, elem) => {
        let tabElement = jQuery(elem);
        if(key > 0) {
            let newDriverPanelId = `driver-panel-${key}`;
            tabElement.find(`.tab-pane-title`).text(`DRIVER ${key + 1}`);

            // reset element input keys
            let tabContainer = jQuery(`#${tabElement.data('id')}`);
            tabContainer.attr('id', newDriverPanelId);
            tabElement.attr('data-id', newDriverPanelId);
            tabElement.attr('id', `driver-${key}`);
            tabElement.data('id', newDriverPanelId);
        }
    });
}

functions.changeTab = function (obj) {
    let tabElement = jQuery(obj);
    let tabContainer = tabElement.parent().parent();
    tabContainer.find(".tab-pane.d-pane").removeClass("active");
    tabElement.addClass("active");
    tabContainer.find('.tab-wrapper').addClass('display-none');
    jQuery(`#${tabElement.attr("data-id")}`).removeClass("display-none");
}

functions.changeDsummaryTab = function (obj) {
    jQuery(".tab-pane.dsummary-pane").removeClass("active");
    jQuery(obj).addClass("active");
    jQuery('.tab-wrapper.dsummary-wrapper').addClass('display-none');
    jQuery("#" + jQuery(obj).attr("data-id")).removeClass("display-none");
}

functions.changeLoginTab = function (obj) {
    jQuery(".tab-pane.login-pane").removeClass("active");
    jQuery(obj).addClass("active");
    jQuery('.tab-wrapper.login-wrapper').addClass('display-none');
    jQuery("#" + jQuery(obj).attr("data-id")).removeClass("display-none");
}

functions.changePopTab = function (obj) {
    jQuery(".tab-pane.pop-pane").removeClass("active");
    jQuery(obj).addClass("active");
    jQuery('.tab-wrapper.pop-wrapper').addClass('display-none');
    jQuery("#" + jQuery(obj).attr("data-id")).removeClass("display-none");
    jQuery("#loginpopup").html(jQuery(obj).attr("data-title"));
}

functions.loadCarModels = _.debounce((callback) => {
    let carMakeCode = jQuery("select[name='jform\[carMake\]']").val();

    if (variables.selectedQuote.make != carMakeCode) {

        variables.selectedQuote.make = carMakeCode;

        let getQuoteBtn = jQuery(`#check-veh`);
        getQuoteBtn.prop('disabled', true)
            .LoadingOverlay('show');

        let insurerModelSelectWrappers = jQuery(".insurer-model-select-wrapper");
        insurerModelSelectWrappers.LoadingOverlay("show");

        let restoreLoading = () => {
            getQuoteBtn.prop('disabled', false)
                .LoadingOverlay('hide', true);

            insurerModelSelectWrappers.LoadingOverlay("hide");
        }

        jQuery.ajax({
            type: "POST",
            url: functions.motorRouteAjaxJson("getModelList"),
            data: 'carMake=' + carMakeCode,
            dataType: "json",
            success: function (result) {
                let selectObjects = {};
                let selectObjectsGeneral = jQuery(`.insurer-model-select`);
                selectObjectsGeneral.html(`<option value="">- Select Car Model -</option>`);

                result.forEach(function (item, index) {
                    item.provider_ids.forEach((providerId, key) => {
                        if(typeof(selectObjects[providerId])) {
                            selectObjects[providerId] = jQuery(`#insurer-model-select-${providerId}`);
                        }

                        selectObjects[providerId].append(
                            `<option value="${item.model_code}">${item.model_name}</option>`
                        );
                    });
                });

                selectObjectsGeneral.val('');

                let defaultVehicleDataProviderCode = 'axa';
                functions.getQuoteProviders((providers) => {
                    let defaultVehicleDataProvider = providers.find((provider) => provider.code == defaultVehicleDataProviderCode);
                    let selectObject = jQuery(`#insurer-model-select-${defaultVehicleDataProvider.id}`);

                    if (variables.lastVehicleData) {
                        let selectedOption = selectObject.find(`option[value=${variables.lastVehicleData.model}]`)
                            .eq(0)
                            .prop('selected', true);
                    }

                    selectObjectsGeneral.trigger('change');

                    restoreLoading();
    
                    if (typeof (callback) === 'function') {
                        callback(selectObject, 'success');
                    }
                });
            },
            error: function (xhr, textStatus, errorThrown) {
                Swal.fire({
                    icon: 'error',
                    title: `Something went wrong!`,
                    text: `request failed : ${errorThrown}`
                });

                restoreLoading();

                if (typeof (callback) === 'function') {
                    callback(null, 'error');
                }
            }
        });
    }

    callback(null, 'skipped');

}, 100);

functions.autoselectEmptyInsurerCarModels = () => {
    let selectObjectsGeneral = jQuery(`.insurer-model-select`);

    let selectedSelect = selectObjectsGeneral.filter((key, obj) => jQuery(obj).val());
    if(selectedSelect.length) {
        let selectedSelectTextValue = functions.getElementInputValue(selectedSelect.eq(0), 'select');
        let map = {}
        let unselectedSelects = selectObjectsGeneral
            .filter((key, obj) => !jQuery(obj).val())
            .each((key, select) => {
                let selectElement = jQuery(select);
                
                let selectElementOptions = selectElement.find('option');
                let options = [];
                selectElementOptions.each((key, option) => {
                    let optionElement = jQuery(option);
                    options.push(optionElement.text());
                });
    
                let bestMatch = stringSimilarity.findBestMatch(selectedSelectTextValue, options);
    
                if(bestMatch.bestMatch.rating >= 0.5) {
                    let selectMatchOptionIndex = selectElement.find(`option:nth-child(${bestMatch.bestMatchIndex + 1})`).val();
                    selectElement.val(selectMatchOptionIndex).trigger('change');
                }
            });
    }
}

/**
 * Get insurers available
 */
functions.getQuoteProviders = function (callback) {
    if (variables.providersCache === null) {
        try {
            let providersData = JSON.parse(atob(jQuery(`#adminForm`).data('providers')));

            variables.providersCache = providersData;
            callback(providersData, 'success');
            return;
        } catch (error) {
            console.log(error);
        }

        // deprecated
        jQuery.ajax({
            type: "GET",
            url: functions.motorRouteAjaxJson('getQuoteProviders'),
            dataType: "json",
            success: function (data, status) {
                variables.providersCache = data;
                callback(data, 'success');
            },
            error: function () {
                callback(null, 'error');
            }
        });
    }
    else {
        callback(variables.providersCache, 'success');
    }
}

functions.isJson = function (text) {
    if (typeof (text) === 'object') {
        return true;
    }

    try {
        obj = JSON.parse(text);
    } catch (error) {
        return false;
    }

    return typeof (obj) === 'object';
}

functions.getMarketValue = () => {
    let BreakException = {};
    let marketValue = 0;

    let marketValueSources = [
        variables.selectedQuote.marketValue,
        jQuery(`#select-model-market-value`).val(),
    ];
    if(variables.lastVehicleData) {
        marketValueSources.push(variables.lastVehicleData.marketValue);
    }

    try {
        marketValueSources.forEach((val) => {
            if(val && val != 0) {
                marketValue = val;
                throw BreakException;
            }
        });
    }
    catch (e) {
        // ignore
    }

    return marketValue;
}

functions.updateBuyNowBtnTitle = () => {
    jQuery(`.motor-result-table .motor-result-row.first .buy-now-btn`).attr('title', '');
    jQuery(`.motor-result-table .motor-result-row.first .buy-now-btn.btn-disabled`).attr('title', 'more information needed to get quote');
}

functions.getQuoteData = function (payload, provider, callback) {
    if (!(typeof (payload) === 'object' && typeof (callback) === 'function')) {
        return null;
    }

    let marketValue = functions.getMarketValue();
    let defaultPayload = {
        'jform[marketVal]': marketValue,
        'jform[marketValuation]': marketValue,
        'jform[marketValue]': marketValue,
        'jform[sumInsured]': marketValue,
    };

    let optionalDefaultPayload = functions.composePayload([
        'carMakeYear',
        'chassisNo',
        'engineNo',
        'vehicleEngineCc',
    ]);

    defaultPayload = _.merge(defaultPayload, optionalDefaultPayload);

    payload = _.merge(defaultPayload, payload);

    let motorResultRow = jQuery('.motor-result-table .motor-result-row.first');

    let resultColumnId = `${provider.code}-column`;
    let resultColumn = jQuery(`
        <div class="motor-result-column motor-column-provider-${provider.id}" id="${resultColumnId}">
            <div class="motor-quote-result-brand-logo-container">
                <img src="${rootUrl}${provider.image_file}" class="motor-quote-result-brand-logo my-transition">
            </div>
            <div class="motor-quote-result-block-header">
                <div class="motor-quote-premium-header my-transition my-transition-ease-in-out"></div>
                <div class="motor-quote-premium" >-<span class="motor-quote-premium-promo"></span></div>
                <div class="motor-quote-discount">
                    
                </div>
                <div class="motor-quote-diy-points my-transition my-transition-ease-in-out">
                    Best Geographical Coverage
                </div>
            </div>
            <div class="motor-quote-result-block-footer">
                <button
                    type="button"
                    class="motor-btn buy-now-btn btn-disabled my-transition my-transition-ease-in-out"
                    data-renewal="${BASE_URL}media/com_insurediymotor/providers/${provider.code}/renewal.pdf"
                >
                    BUY NOW
                </button>
            </div>
        </div>
    `);
    motorResultRow.append(resultColumn);

    resultColumn = jQuery(`#${resultColumnId}`);
    let moreInfo = "More information needed to get quote.";

    jQuery.LoadingOverlaySetup({
        background      : "rgba(0, 0, 0, 0.5)",
        image           : BASE_URL + "images/spinner/Spin-1s-200px.svg",
        imageAnimation  : "rotate_right 2000ms",
        imageColor      : "#ffcc00"
    });
    
    resultColumn.LoadingOverlay('show');

    let buyNowBtn = resultColumn.find(`.buy-now-btn`);
    let quoteFailed = () => {
        resultColumn.LoadingOverlay('hide', true);
        jQuery(`#${resultColumnId} .motor-quote-result-block-body`).html(moreInfo);
        buyNowBtn.prop('disabled', true);
        if (functions.quoteStep() == 4) {
            resultColumn.find(`.buy-now-btn`).addClass("btn-disabled");
            resultColumn.find(`.buy-now-btn`).removeClass("green-btn");
        }
    }

    // renewal
    buyNowBtn.text('BUY NOW');
    let currentInsurerId = jQuery(`[name="jform[currentInsurer]"]`).val();
    if (currentInsurerId == provider.id) {
        resultColumn.LoadingOverlay('hide', true);
        buyNowBtn.text('RENEW')
            .removeClass("btn-disabled")
            .removeClass('green-btn')
            .prop('disabled', false);

        callback(null, 'renew');

        return;
    } 

    if (provider.skip_first_quote == 1 && functions.quoteStep() != 4) {
        quoteFailed();

        callback(null, 'error');
        return;
    }

    if (!payload[`jform[carModel]`]) {
        quoteFailed();

        callback(null, 'error');
        return;
    }

    jQuery.ajax({
        type: "POST",
        url: functions.motorRouteAjaxJson('getQuoteProvider&provider_id=' + provider.id),
        data: payload,
        dataType: "json",
        timeout: 600000, // sets timeout to 3 seconds
        success: function (quote) {
            if (quote) {
                let quoteTotal = accounting.unformat(quote.total);
                resultColumn.data('quote', quote);
                resultColumn.data('provider', provider);
                resultColumn.find('.motor-quote-premium').eq(0).html(`${accounting.formatMoney(quoteTotal, 'MYR ', 2)}<span class="motor-quote-premium-promo"></span>`)

                resultColumn.LoadingOverlay('hide', true);
            }
            else {
                quoteFailed();
            }

            if (functions.quoteStep() == 4) {
                resultColumn.find(`.buy-now-btn`).removeClass("btn-disabled");
                resultColumn.find(`.buy-now-btn`).addClass("green-btn");
                resultColumn.find(`.buy-now-btn`).prop('disabled', false);
            }
            else {
                resultColumn.find(`.buy-now-btn`).prop('disabled', true);
            }

            functions.updateBuyNowBtnTitle();

            callback(quote, 'success');
        },
        error: function () {
            quoteFailed();

            callback(null, 'error');
        }
    });
}

functions.getAllQuoteData = function (callback, payload, quoteProviderCallback, providersFilterCallback) {
    if (typeof (payload) === 'undefined') {
        payload = {};
    }

    jQuery('.motor-result-row.first .motor-result-column').not(`.motor-result-column-dummy`).remove();
    jQuery('.motor-result-divider').remove();

    functions.getQuoteProviders(function (providers, status) {
        if (status !== 'success') {
            return;
        }

        if (typeof (providersFilterCallback) === 'function') {
            providers = providers.filter(providersFilterCallback);
        }

        let motorResultRow = jQuery('.motor-result-table tr.motor-result-row.first');

        let processCount = 0;
        let quotes = [];

        providers.forEach(function (provider) {
            payload[`jform[carModel]`] = jQuery(`#insurer-model-select-${provider.id}`).val();

            functions.getQuoteData(payload, provider, function (quote, status) {
                processCount++;

                let newQuoteData = {
                    provider: provider,
                    quote: quote,
                    status: status
                };
                quotes.push(newQuoteData);

                let wait = setTimeout(function () {
                    if (processCount === providers.length) {

                        if (typeof (callback) === 'function') {
                            functions.updateComparisonTable(quotes);

                            callback(quotes);
                        }
                    }
                }, 300);
            })
        });

        if (typeof (quoteProviderCallback) === 'function') {
            quoteProviderCallback();
        }
    });
}

functions.updateComparisonTable = (quotes) => {
    let maxLengthTable = {};
    let quoteResultCache = {};
    let tableKeys = [];
    let groupTable = {};

    let getQuoteResult = (quotes, provider) => {
        if(typeof(quoteResultCache[provider.id]) === 'undefined') {
            quoteResultCache[provider.id] = quotes.find((quoteResult) => {
                return quoteResult.provider.id == provider.id;
            });
        }

        return quoteResultCache[provider.id].quote;
    }

    functions.getQuoteProviders((providers) => {
        // mapping comparison table read max length
        providers.forEach((provider) => {
            let quoteResult = getQuoteResult(quotes, provider);

            if (quoteResult && typeof (quoteResult.comparisonTable) === 'object') {
                quoteResult.comparisonTable.forEach((comparisonTableData) => {
                    if(typeof(maxLengthTable[comparisonTableData.id]) === 'undefined') {
                        maxLengthTable[comparisonTableData.id] = 0;
                        tableKeys.push(comparisonTableData.id);
                    }
    
                    if (maxLengthTable[comparisonTableData.id] < comparisonTableData.data.length) {
                        maxLengthTable[comparisonTableData.id] = comparisonTableData.data.length;
                        groupTable[comparisonTableData.id] = comparisonTableData.data;
                    }
                });
            }
        });

        tableKeys.forEach((tableKey) => {
            let comparisonTable = jQuery(`#${tableKey}`);
            comparisonTable.empty();
            let comparisonTableGroupRows = groupTable[tableKey];

            // comparison table update contents
            if(typeof(comparisonTableGroupRows) === 'undefined') {
                return;
            }
            
            // populate table rows & columns
            comparisonTableGroupRows.forEach((comparisonTableGroupRow, comparisonTableGroupRowKey) => {
                // row
                let newComparisonTableRow = jQuery(`
                    <div class="motor-result-row d-flex" id="${comparisonTableGroupRow.id}">
                        <div class="motor-result-column motor-result-column-title d-flex align-items-center justify-content-center">
                            ${comparisonTableGroupRow.group}
                        </div>
                    </div>
                `);

                // columns (default with "x" value)
                providers.forEach((provider) => {
                    let comparisonTableColumn = jQuery(`
                            <div id="${comparisonTableGroupRow.id}-${provider.id}" class="motor-result-column d-flex align-items-center justify-content-center motor-column-provider-${provider.id}">
                                <i class="fa fa-2x comparison-table-icon fa-times"></i>
                            </div>
                        `);
    
                    newComparisonTableRow.append(comparisonTableColumn);
                });

                comparisonTable.append(newComparisonTableRow);
            })

            // update by quote results
            providers.forEach((provider) => {
                let quoteResult = getQuoteResult(quotes, provider);
                let providerComparisonTable = quoteResult ? quoteResult.comparisonTable.find((data) => {
                    return data.id == tableKey;
                }) : {
                    id: tableKey,
                    data: [],
                };

                providerComparisonTable.data.forEach((data, dataKey) => {
                    jQuery(`#${data.id}-${provider.id}`).html(data.text);
                });
            });
        });
    });

    // hide ones that fails to get quote
    if(functions.quoteStep() > 3) {
        quotes.forEach((quote) => {
            let columns = jQuery(`.motor-column-provider-${quote.provider.id}`);
            if(quote.quote) {
                columns.attr('style', '');
            }
            else {
                columns.attr('style', 'display: none !important;');
            }
        });
    }

    functions.resetMotorResultSpacer();
}

functions.composePayload = function (keys) {
    let payload = {};

    keys.forEach(function (value) {
        let payloadKey = `jform[${value}]`;
        let inputElementSelector = `[name='jform\[${value}\]']`;
        let inputElement = jQuery(inputElementSelector);
        if(inputElement.is(`input[type=radio]`)) {
            payload[payloadKey] = jQuery(`${inputElementSelector}:checked`).val();
        }
        else {
            payload[payloadKey] = inputElement.val();
        }
    });

    return payload;
}

functions.unlockBuyNowBtn = function () {
    jQuery(".buy-now-btn").each(function (index, elem) {
        let buttonElement = jQuery(elem);
        buttonElement.addClass("continue-4-btn");
    });
}

functions.lockBuyNowBtn = () => {
    jQuery(".buy-now-btn").each(function (index, elem) {
        let buttonElement = jQuery(elem);
        buttonElement.removeClass("continue-4-btn");
        buttonElement.removeClass(`green-btn`);
        buttonElement.addClass('btn-disabled');
        buttonElement.prop('disabled', true);
    });
}

functions.showStep2 = function () {
    jQuery(".hidden-wrapper").slideDown();

    functions.scrollTop();

    jQuery(".insurediy-form.main-form").addClass("pad");
    jQuery('.my-step3').hide();
    jQuery(".my-step2").show();
    jQuery(".motor-quote-result-middle-wrapper").addClass("res").removeClass("hide");
    jQuery(".motor-quote-result-middle-wrapper").show();

    jQuery(`.motor-accordion:visible`).eq(0).trigger('click');
}

functions.showStep3 = function () {
    jQuery('.my-step2,.my-step1').hide();
    jQuery('.my-step3').show();
    jQuery.LoadingOverlay("hide");
    jQuery(".motor-quote-result-middle-wrapper").addClass("res").removeClass("hide");

    jQuery(`.motor-accordion:visible`).eq(0).trigger('click');
}

functions.saveQuote = (callback) => {
    jQuery(`.insurer-model-select`).each((key, select) => {
        let thisElement = jQuery(select);
        variables.selectedQuote.models[thisElement.data('id')] = thisElement.val();
    });
    jQuery('#quoteSummaryJson').val(JSON.stringify(variables.selectedQuote));
    jQuery('#quotePromosJson').val(JSON.stringify(variables.promos));

    let adminForm = jQuery('#adminForm');
    let disabledInputs = adminForm.find('input:disabled,select:disabled').prop('disabled', false);

    axios.post(
        functions.motorRoute('form.quoteSave&format=json'),
        jQuery('#adminForm').serialize(),
        {
            responseType: 'json',
        }
    ).then((response) => {
        let data = response.data;
        if (typeof (callback) == 'function') {
            callback(data, 'success');
        }

        document.paymentForm.orderRef.value = data.payment_request_id;

        disabledInputs.prop('disabled', true);
    }).catch((error) => {
        console.log({error});
        if (typeof (callback) == 'function') {
            callback(null, 'error');
        }

        disabledInputs.prop('disabled', true);
    });
}

functions.quoteStep = (step) => {
    let quoteStepElement = jQuery(`#quoteStep`);

    if (typeof (step) === 'undefined') {
        return parseInt(functions.urlParam('step', quoteStepElement.val()));
    }

    let urlSearch = functions.urlParams();
    urlSearch.set('step', step);
    let url = new URL(window.location.href);
    url.search = urlSearch;
    window.history.pushState({step: step}, '', url.href);

    functions.updateBreadcrumbs(step);

    // trigger first motor accordion
    jQuery(`.motor-accordion.my-step${step}`).eq(0).trigger(`click`);

    return quoteStepElement.val(step);
}

functions.updateBreadcrumbs = (step) => {
    let toggleClasses = [
        `insurediy-motor-breadcrumb`,
        `insurediy-motor-breadcrumb-active`,
    ];

    let nthStep = parseInt(step) > 4 ? 4 : step;
    
    jQuery(`#motor-breadcrumb .${toggleClasses[1]}`).attr('class', toggleClasses[0]);
    jQuery(`#motor-breadcrumb a`).eq(nthStep - 1).children().attr('class', toggleClasses[1]);
}

functions.getCurrentQuote = () => {
    try {
        return JSON.parse(atob(jQuery(`#adminForm`).data('currentquote')));
    }
    catch (e) {
        return null;
    }
}

functions.syncDrivers = () => {
    // simulate click add driver, and fill data based on previous form
    let addDriverBtn = jQuery(`#addDriverButton`);

    let driverCount = addDriverBtn.data('currentcount');

    if (driverCount > 1) {
        let currentQuote = functions.getCurrentQuote();

        currentQuote.drivers.forEach((driver, key) => {
            if (key > 0) {
                let driverPanel = jQuery(`#driver-panel-${key}`);
                if(!driverPanel.length) {
                    addDriverBtn.trigger('click');
                    driverPanel = jQuery(`#driver-panel-${key}`);
                    let dName = driverPanel.find(`[name="jform[name][]"]`);
                }

                driverPanel.find(`[name="jform[name][]"]`).val(driver.name);
                driverPanel.find(`[name="jform[icNum][]"]`).val(driver.nric);
                driverPanel.find(`[name="jform[yearsOfDrivingExp][]"]`).val(driver.exp).trigger('change');
            }
        });
    }
}

/**
 * Get select element caption / text value
 */
functions.getElementInputValue = function (element, type) {
    let inputValue = element.eq(0).val();
    if (typeof (type) === 'undefined') {
        type = 'text';
    }

    if (type === 'select') {
        inputValue = element.eq(0).find(`option[value='${inputValue}']`).eq(0).text();
    }

    if (type === 'radio') {
        inputValue = element.filter(`:checked`).val();
    }

    return inputValue;
}

functions.calculateFinal = () => {
    let quoteFinal = accounting.unformat(variables.selectedQuote.data.total);

    variables.promos.forEach((promo) => {
        quoteFinal += accounting.unformat(promo.value);
    });

    variables.selectedQuote.final = quoteFinal;

    let table = jQuery('table.summary-table').eq(0);
    table.find('td.summary-total-amount')
        .eq(0)
        .text(accounting.formatMoney(quoteFinal, 'RM '));

    document.paymentForm.amount.value = quoteFinal;
}

functions.addPromo = (code, value, description = null) => {
    variables.promos.push({
        code: code,
        value: (0.0 - value),
        description: description
    });

    let sumTableDetail = jQuery('#summary-table-detail');
    sumTableDetail.find('ul.summary-desc-list').append(`
        <li class="desc-list summary-desc-promo-item">
            Promo ${code} Discount: <b>- RM ${value.toFixed(2)}</b>
        </li>
    `);
    sumTableDetail.find('ul.summary-subtotal-list').append(`
        <li class="desc-list summary-subtotal-promo-item">
            - RM ${value.toFixed(2)}
        </li>
    `);

    functions.calculateFinal();
}


/**
 * Get root / base url
 */
functions.getUrl = function (url) {
    if (typeof (url) !== 'string') {
        return null;
    }

    return BASE_URL + url;
}

/**
 * Get Motor Component's Route URL
 */
functions.motorRoute = function (task, format) {
    if (typeof (task) !== 'string') {
        return null;
    }

    let url = functions.getUrl('index.php?option=com_insurediymotor&task=' + task);

    if (typeof (format) === 'string') {
        url = url + '&format=' + format;
    }

    return url;
}

/**
 * Shorthand function for motor route to call ajax.json.php
 */
functions.motorRouteAjaxJson = function (method) {
    if (!typeof (method) === 'string') {
        return null;
    }

    return functions.motorRoute('ajax.' + method, 'json');
}

functions.getVehicleData = async (callback) => {
    let successRequest = false,
        requestCount = 0,
        maxRetry = 2;

    let result = null;
    let status = 'error';

    while (!successRequest && requestCount < maxRetry) {
        try {
            let [data, responseStatus] = await new Promise((resolve, reject) => {
                axios.post(
                    functions.motorRouteAjaxJson('getCarInfo'),
                    'regNum=' + jQuery("input[name='jform\[vehicleRegNo\]']").val() + '&carCondition=' + jQuery("select[name='jform\[carCondition\]']").val(),
                ).then((response) => {
                    response.status === 200 ? resolve([response.data, 'success']) : reject([response.data, 'error']);
                }).catch((error) => {
                    reject([error, 'error']);
                });
            });

            if (responseStatus === 'success') {
                successRequest = true;
                result = data;

                variables.lastVehicleData = data;

                jQuery("input[name='jform\[vehicleEngineCc\]']").val(data["cc"]);
                jQuery("input[name='jform\[carMakeYear\]']").val(data["year"]);
                jQuery("select[name='jform\[carMake\]']").val(data["make"]).trigger('change');
                jQuery("input[name='jform\[chassisNo\]']").val(data["chassis"]);
                jQuery("input[name='jform\[engineNo\]']").val(data["engine"]);
                jQuery("input[name='jform\[marketVal\]']").val(data["marketValue"]);
                jQuery("input[name='jform\[marketValuation\]']").val(data["marketValue"]);
                jQuery("input[name='jform\[selectModelMarketValue\]']").val(data["marketValue"]);

                status = responseStatus;
            }
        } catch (error) {
            // ignore
        }
        requestCount += 1;
    }

    disableCarMakeSelectOnChange = true;

    callback(result, status);
}

functions.restoreSession = () => {
    return new Promise((resolve, reject) => {
        let make = variables.selectedQuote.make;
        variables.selectedQuote.make = null;
        console.log(`restoring car make: ${make}`);
        jQuery("select[name='jform\[carMake\]']").val(make).trigger('change');

        disableCarMakeSelectOnChange = true;

        functions.loadCarModels(() => {
            functions.getQuoteProviders((providers) => {
                providers.forEach((provider) => {
                    if (typeof (variables.selectedQuote.models[provider.id]) !== 'undefined' && variables.selectedQuote.models[provider.id]) {
                        jQuery(`#insurer-model-select-${provider.id}`).val(variables.selectedQuote.models[provider.id])
                    }
                });

                disableCarMakeSelectOnChange = false;

                resolve('restored');
            })
        });
    });
}

functions.scrollTop = _.debounce((value) => {
    value = typeof(value) === 'undefined' ? 0 : value;

    console.log(`scrolling to ${value}`);

    jQuery('html, body').animate({
        scrollTop: value
    }, 'slow');
}, 300);

functions.urlParams = () => {
    return new URLSearchParams(window.location.search);
}

functions.urlParam = (key, defaultValue = null) => {
    let urlParams = functions.urlParams();

    return urlParams.get(key) !== null ? urlParams.get(key) : defaultValue;
};

functions.getLockedCarDetailsSelector = () => {
    return [
        `[name="jform[chassisNo]"]`,
        `[name="jform[carMakeYear]"]`,
        `[name="jform[engineNo]"]`,
        `[name="jform[vehicleEngineCc]"]`,
    ].join(',');
};

functions.lockCarDetailsInput = () => {
    jQuery(functions.getLockedCarDetailsSelector()).prop('disabled', true);
}

functions.unlockCarDetailsInput = () => {
    jQuery(functions.getLockedCarDetailsSelector()).prop('disabled', false);
}

functions.claimPastYearClicked = (elem) => {
    let claimPastYearForm = jQuery(`#panel-claim-past-year`);
    if(elem.value == 'yes') {
        claimPastYearForm.fadeIn();
    }
    else {
        claimPastYearForm.fadeOut();
    }
}

functions.formValidator = (apply = true, options = {}) => {
    if (apply) {
        let adminForm = jQuery(`#adminForm`);
        variables.validator = adminForm.validate(options);
        adminForm.trigger('submit');
    }
    else {
        if (variables.validator !== null) {
            variables.validator.destroy();
        }
    }
}

functions.step1Validation = (apply = true) => {
    functions.formValidator(apply, {
        rules: {
            'jform[vehicleRegNo]': {
                required: true,
            },
        },
    });
} 

functions.resetMotorResultSpacer = () => {
    jQuery(`.motor-result-row .motor-result-column-spacer`).remove();

    if(functions.quoteStep() == 4) {
        // jQuery(`.motor-result-row`).append(`
        //     <div class="motor-result-column-spacer"></div>
        // `);
    }
}

functions.autoExpandMotorAccordion = (targetElement) => {
    targetElement = jQuery(targetElement);

    if(targetElement.length) {
        // auto expand motor accordion
        let motorAccordion = targetElement.parents(`div.panel`).prev();
        let timeout = 10;
        if (motorAccordion.length) {
            motorAccordion.trigger('click');
            let motorAccordionIsActive = motorAccordion.is(`.active`);
            if(motorAccordionIsActive) {
                timeout = 400;
            }
        }

        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(`motor accordion expanded`);
            }, timeout);
        });
    }

    return new Promise((resolve, reject) => {
        reject(`motor accordion not found`);
    });
}

export default functions;
