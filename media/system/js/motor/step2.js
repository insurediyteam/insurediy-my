import variables from './variables';
import functions from './functions';
import _ from 'lodash';
import { 
    step2ClaimPastElementSelectors,
    step2ElementSelectors,
    validateElements,
    handleValidationError
} from './validation';

jQuery("#continue-step2").on('click', function (obj) {
    let validator = validateElements(step2ElementSelectors);
    if (!validator.isValid) {
        handleValidationError(validator.selector, -300);
        return;
    }
    if (functions.getElementInputValue(jQuery(`[name='jform[claimPast]']`), 'radio') == 'yes') {
        validator = validateElements(step2ClaimPastElementSelectors);
        if (!validator.isValid) {
            handleValidationError(validator.selector, -300);
            return;
        }
    }

    let toStep3 = (callback) => {
        jQuery.LoadingOverlay("show", {
            image: "",
            custom: variables.customElement
        });
        jQuery(".not-final").hide();
        functions.scrollTop();

        let marketValue = jQuery(`#select-model-market-value`).val();
        jQuery(`[name="jform[marketValue]"]`).val(marketValue);
        jQuery(`[name="jform[marketValuation]"]`).val(marketValue);
        jQuery(`#basisCoverage`).trigger('change');
        jQuery(`[name="jform[ncdDisplay]"]`).val(variables.ncdOptions[jQuery(`[name="jform[ncd]"]:checked`).val()]);
        jQuery(`[name='jform[driverOccupation][]']`).eq(0).empty().append(jQuery(`[name="jform[occupation]"] option:selected`).clone()).trigger('change');
        jQuery(`[name='jform[yearsOfDrivingExp][]']`).eq(0).val(jQuery(`[name="jform[drivingExp]"]`).val()).trigger('change');

        functions.getAllQuoteData(function (quotes) {
            let quoteResults = quotes;

            if (typeof (callback) === 'function') {
                callback();
            }
        }, functions.composePayload([
            'vehicleRegNo',
            'carBodyType',
            'carCondition',
            'currentInsurer',
            'chassisNo',
            'engineNo',
            'carMake',
            'functionalMod',
            'carMakeYear',
            'vehicleEngineCc',
            'marketVal',
            'airbags',
            'performanceMod',
            'vehicleUse',
            'purchaseDate',
            'purchasePrice',
            'marketValuation',
            'carFuel',
            'vehType',
            'seatNumber',
            'abs',
            'coverType',
            'garage',
            'antiTeft',
            'occupation',
            'drivingExp',
            'ehailing',
            'ncd',
            'currentRenew',
            'currentSum',
            'claimPast',
            'damageOwn',
            'windscreen',
            'theft',
            'tpClaim'
        ]), function () {
            functions.showStep3();

            jQuery.LoadingOverlay("hide");
        });
    }

    if (!variables.restoring) {
        functions.quoteStep(3);

        toStep3(() => {
            functions.saveQuote();
        });

    }
    else {
        jQuery.LoadingOverlay('show');

        functions.syncDrivers();

        functions.restoreSession().then((result) => {
            jQuery.LoadingOverlay('hide', true);

            toStep3();
        });
    }

});

jQuery(".back-3-btn").on('click', function (e) {
    functions.quoteStep(1);

    functions.saveQuote();

    jQuery(".hidden-wrapper").hide();
    jQuery("#check-veh").fadeIn();
    jQuery(".motor-quote-result-middle-wrapper").addClass("hide");
    jQuery('.pad.insurediy-form').css('padding-top', '0');
    functions.scrollTop();

    jQuery(`#continue-to-select-model, .select-model-step`).show();

    jQuery('.my-step2, .my-step1, .my-step3').hide();

    jQuery(`.motor-accordion.select-model-step`).trigger('click');

    functions.scrollTop(0);
});
