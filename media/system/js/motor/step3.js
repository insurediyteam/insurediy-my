import variables from './variables';
import functions from './functions';
import _ from 'lodash';
import {
    step3ElementSelectors,
    validateElements,
    handleValidationError
} from './validation';

jQuery(".back-4-btn").on('click', function (e) {
    functions.quoteStep(2);

    functions.saveQuote();

    jQuery(".motor-quote-result-middle-wrapper").addClass("res").removeClass("hide");
    jQuery('.pad.insurediy-form').css('padding-top', '0');
    jQuery(".insurediy-form.main-form").addClass("pad");
    jQuery(".motor-quote-result-middle-wrapper").show();
    functions.scrollTop();

    jQuery(`
        .my-step3,
        .comparison-table
    `).hide();
    functions.resetMotorResultSpacer();

    jQuery('.my-step2, .my-step1').show();
    
    jQuery.LoadingOverlay("hide", true);

    functions.scrollTop(0);
});

jQuery(".continue-4-btn").on('click', function (obj) {
    // validate additional drivers if any
    let drivers = jQuery(`#driver-wrapper .tab-pane`);
    if(drivers.length > 1) {
        let BreakException = {};

        try {
            drivers.each((key, elem) => {
                let element = jQuery(elem);

                if(key > 0) {
                    element.trigger('click');
                    let tabContainerId = `#${element.data('id')}`;
                    let tabContainer = jQuery(tabContainerId);
    
                    tabContainer.find(`input,select`).each((inputKey, inputElem) => {
                        let inputElement = jQuery(inputElem);
    
                        let selector = `${tabContainerId} [name='${inputElement.attr('name')}']`;
                        functions.autoExpandMotorAccordion(selector);

                        let isValid = inputElement.val();
                        if (!isValid) {
                            // show tab
                            handleValidationError(selector, -300);
                            jQuery(`form#adminForm`).validate().element(selector);
                            throw BreakException;
                        }
                    });
                }
            });
        } catch (error) {
            return;
        }
    }

    // validate step3 general
    drivers.eq(0).trigger('click');
    let validator = validateElements(step3ElementSelectors);
    if (!validator.isValid) {
        handleValidationError(validator.selector, -300);
        return;
    }

    let toStep4 = (callback) => {
        jQuery.LoadingOverlay("show", {
            image: "",
            custom: variables.customElement
        });
        jQuery(".not-final").hide();
        functions.scrollTop();
        jQuery(`#mainLoadingOverlay`).parent().addClass(`mainLoading`);

        jQuery(`[name="jform[ncdDisplay]"]`).val(variables.ncdOptions[jQuery(`[name="jform[ncd]"]:checked`).val()]);

        let marketValue = jQuery(`#select-model-market-value`).val();
        jQuery(`[name="jform[marketValue]"]`).val(marketValue);
        jQuery(`[name="jform[marketValuation]"]`).val(marketValue);

        let arrayHashTable = {};

        let formSerialize = decodeURI(jQuery(`#adminForm`).serialize()).replace(/\+/g, ' ').split(`&`);
        let payload = {};
        formSerialize.forEach((value) => {
            let [name, inputValue] = value.split(`=`);

            if (name == `task` || name == `jform[quoteSummaryJson]` || name == `jform[quotePromosJson]`) {
                return;
            }

            if (name.indexOf(`][]`) >= 0) {
                if (typeof (arrayHashTable[name]) === 'undefined') {
                    arrayHashTable[name] = 0;
                }

                payload[name.replace(`][]`, `][${arrayHashTable[name]}]`)] = inputValue;

                arrayHashTable[name]++;
            }
            else {
                payload[name] = inputValue;
            }
        });

        functions.getAllQuoteData(
            (quotes) => {
                let quoteResults = quotes;
                jQuery(`.comparison-table`).show();
                jQuery(`.comparison-table`).not(`.comparison-table-title`)
                    .each((key, elem) => {
                        let element = jQuery(elem);
                        let elemChilds = element.children();
                        if(!elemChilds.length) {
                            element.hide();
                            element.prev().hide();
                        }
                    })
                jQuery(`.comparison-table-title`).LoadingOverlay('hide', true);
                jQuery(`.motor-result-column-dummy`).show();
                let motorResultTable = jQuery(`.motor-quote-result-middle-wrapper .motor-result-row.first`);
                motorResultTable.addClass('fixed');

                jQuery.LoadingOverlay("hide", true);

                if (typeof (callback) === 'function') {
                    callback();
                }
            },
            payload,
            () => {
                jQuery('.my-step1,.my-step3').hide();
                jQuery('.my-last-choose-insurer').show();
                functions.unlockBuyNowBtn();

                variables.enableFixedQuotes = false;
                functions.fixedQuoteResultsStyle();
                jQuery('.motor-quote-result-middle-wrapper').removeClass('hide');

                // show adjustment column & comparison table
                jQuery(`.comparison-table`).hide();
                jQuery(`.comparison-table-title`).show().LoadingOverlay('show');
            }
        );
    }

    if (!variables.restoring) {
        functions.quoteStep(4);

        toStep4(() => {
            functions.saveQuote();
        });

    }
    else {
        jQuery.LoadingOverlay('show');

        functions.syncDrivers();

        functions.restoreSession().then((result) => {
            jQuery.LoadingOverlay('hide', true);

            toStep4();
        });
    }
});

jQuery(`#basisCoverage`).on('change', _.debounce((el) => {
    let thisElement = jQuery(el.target);
    functions.basisCoverage(thisElement.val());
}, 100));

jQuery(`.windscreen-extracov-input`).on('input change keyup', _.debounce((el) => {
    let thisElement = jQuery(el.target);
    jQuery(`.windscreen-extracov-input`).prop('disabled', false);

    if(thisElement.val() && parseInt(thisElement.val()) > 0) {
        jQuery(`.windscreen-extracov-input`).not(el.target).prop('disabled', true);
    }
}, 100));

jQuery(`[name="jform[loanCompany]"]`).on('change', (el) => {
    let thisElement = jQuery(el.target);
    let loanCompanyOtherInputWrapper = jQuery(`[name="jform[loanCompanyOther]"]`).parents(`.input-wrapper`);
    if(thisElement.find(`option:selected`).text() === 'Other') {
        loanCompanyOtherInputWrapper.show();
    }
    else {
        loanCompanyOtherInputWrapper.hide();
    }
}).trigger('change');
