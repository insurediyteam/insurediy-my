import variables from './variables';
import functions from './functions';
import _ from 'lodash';
import Swal from 'sweetalert2';

const validateModelSelect = () => {
    let validCount = 0;

    jQuery(".insurer-model-select").each((key, elem) => {
        let element = jQuery(elem);

        if (element.val()) {
            validCount++;
        }
    });
    
    return validCount > 0;
}

jQuery(`#back-model-select-btn`).on('click', () => {
    jQuery('.my-step1').show();
    jQuery('.motor-accordion.my-step1').eq(0).trigger('click');
    jQuery('.select-model-step').hide();
    functions.scrollTop();
});

jQuery("#check-veh").on('click', function (obj) {
    let toStep2 = (callback) => {
        functions.scrollTop();

        jQuery.LoadingOverlay("show", {
            image: "",
            custom: variables.customElement
        });

        variables.selectedQuote.marketValue = jQuery(`#select-model-market-value`).val();
        jQuery(`[name='jform[marketVal]']`).val(variables.selectedQuote.marketValue);
        jQuery("#sum-insured").data('defaultvalue', variables.selectedQuote.marketValue)

        jQuery('.select-model-step').hide();

        functions.showStep2();

        functions.getAllQuoteData(function () {
            if (typeof (callback) === 'function') {
                callback();
            }
        }, functions.composePayload([
            'vehicleRegNo',
            'carBodyType',
            'carCondition',
            'currentInsurer'
        ]), function () {
            jQuery(`.my-step1`).show();
            jQuery('.select-model-step, #continue-to-select-model').hide();
            jQuery("#check-veh").LoadingOverlay("hide");
            jQuery("#check-veh").fadeOut();

            jQuery('.motor-accordion.my-step1').eq(0).trigger('click');

            jQuery.LoadingOverlay("hide");
        });
    }

    if(!validateModelSelect()) {
        Swal.fire(
            "Error!",
            "Please select at least one model!",
            "error"
        );

        return;
    }

    if (!variables.restoring) {
        functions.quoteStep(2);

        toStep2(() => {
            functions.saveQuote();
        });
    }
    else {
        functions.restoreSession().then(() => {
            toStep2();
        })
    }
});

jQuery("select[name='jform\[carMake\]']").on('change', function (obj) {
    if (!disableCarMakeSelectOnChange) {
        functions.loadCarModels();
    }
});

// jQuery(`.insurer-model-select-logo`).on('click', _.debounce(() => functions.autoselectEmptyInsurerCarModels(), 100));

jQuery(".insurer-model-select").on('change', function () {
    var selectId = "#" + jQuery(this).attr('id');
    var nextExpand = selectId.substr(selectId.length - 1);
    var nextExpandId = parseInt(nextExpand) + 1;
    var nextExpandFull = "#insurer-model-select-" + nextExpandId;
    if (jQuery(this).val() != "") {
        jQuery(nextExpandFull).select2("open");
    }

}).on('select2:opening', (e) => {
    let element = jQuery(e.target);

    element.prev().fadeIn();
}).on('select2:close', (e) => {
    let element = jQuery(e.target);

    element.prev().fadeOut();
});

let selectObjectsGeneral = jQuery(`.insurer-model-select`);
selectObjectsGeneral.prepend(`<option value="">- Select Car Model -</option>`);
selectObjectsGeneral.select2({
    width: '158px',
}); 
