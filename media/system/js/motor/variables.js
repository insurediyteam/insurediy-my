import functions from "./functions";

const variables = {};

variables.selectedQuote = {
    data: null,
    provider: null,
    final: 0,
    make: null,
    models: {},
    marketValue: null,
};
variables.promos = [];
variables.providersCache = null;
variables.ncdOptions = {
    "2": "0%",
    "3": "25%",
    "4": "30%",
    "5": "38.33%",
    "6": "45%",
    "7": "55%",
}

variables.lastVehicleData = undefined;

variables.enableFixedQuotes = true;

variables.restoring = parseInt(jQuery('#adminForm').data('restore'));

variables.customElement = jQuery('\
    <div id="mainLoadingOverlay" class="overlay-container">\
        <div class="overlay-content-top">\
            <img class="overlay-content-img" src="' + BASE_URL + 'images/websiteicon/company-logo.png"/>\
        </div>\
        <div class="overlay-content-middle">\
            <div class="overlay-content-header">\
                <span class="overlay-content-title">We are gathering quotations for you <br></span>\
                <span class="overlay-content-brand">Please wait a moment . . .</span>\
            </div>\
        </div>\
        <div class="overlay-content-bottom">\
            <div class="spinner">\
                <div></div>\
                <div></div>\
                <div></div>\
                <div></div>\
                <div></div>\
            </div>\
        </div>\
    </div>\
');

variables.customSpinner = jQuery('<div class="loadingio-spinner-spin-vl9cx1jb78l"><div class="ldio-t8qnwyeouh9">\
<div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div>\
</div></div>');

variables.steps = [
    () => {
        jQuery.LoadingOverlay("show", {
            image: "",
            custom: variables.customElement
        });
        functions.restoreSession().then(() => {
            jQuery.LoadingOverlay('hide', true);
            jQuery('html, body').animate({
                scrollTop: 0
            }, 'slow');
        });
    },
    { //step 1 to 2
        element: "#check-veh",
        trigger: "click"
    },
    { //step 2 to 3
        element: "#continue-step2",
        trigger: "click"
    },
    { //step 3 to 4
        element: ".continue-4-btn",
        trigger: "click"
    },
    { //step 3 to 4 OR 5
        element: ".continue-4-btn",
        trigger: "click"
    }
];

variables.driverLimit = 4;

variables.paymentRefId = null;

variables.validator = null;

variables.motorAccordionDuration = 300;

export default variables;
