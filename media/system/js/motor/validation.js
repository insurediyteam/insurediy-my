import _ from 'lodash';
import Swal from 'sweetalert2';
import functions from './functions';
import variables from './variables';

var validator = jQuery("form#adminForm").validate({
    rules: {
        "jform[vehicleRegNo]": {
            required: true,
            validRegNum: true,
        },
        "jform[carBodyType]": {
            required: true,
        },
        "jform[chassisNo]": {
            required: true,
        },
        "jform[engineNo]": {
            required: true,
        },
        "jform[functionalMod]": {
            required: true,
        },
        "jform[carMakeYear]": {
            required: true,
        },
        "jform[vehicleEngineCc]": {
            required: true,
        },
        "jform[airbags]": {
            required: true,
        },
        "jform[performanceMod]": {
            required: true,
        },
        "jform[vehicleUse]": {
            required: true,
        },
        "jform[purchaseDate]": {
            required: true,
        },
        "jform[purchasePrice]": {
            required: true,
        },
        "jform[marketValuation]": {
            required: true,
        },
        "jform[carIsLocal]": {
            required: true,
        },
        "jform[carFuel]": {
            required: true,
        },
        "jform[vehType]": {
            required: true,
        },
        "jform[seatNumber]": {
            required: true,
        },
        "jform[abs]": {
            required: true,
        },
        "jform[garage]": {
            required: true,
        },
        "jform[occupation]": {
            required: true,
        },
        "jform[ehailingProtection]": {
            required: true,
        },
        "jform[ncd]": {
            required: true,
        },
        "jform[claimPast]": {
            required: true,
        },
        "jform[damageOwn]": {
            required: true,
        },
        "jform[windscreen]": {
            required: true,
        },
        "jform[theft]": {
            required: true,
        },
        "jform[tpClaim]": {
            required: true,
        },
        "jform[Gender][0]": {
            required: true,
        },
        "jform[name][0]": {
            required: true,
        },
        "jform[dateOfBirth][0]": {
            required: true,
        },
        "jform[maritalStatus][0]": {
            required: true,
        },
        "jform[yearsOfDrivingExp][0]": {
            required: true,
        },
        "jform[driverOccupation][0]": {
            required: true,
        },
        "jform[occupationStatus][0]": {
            required: true,
        },
        "jform[occupationNature][0]": {
            required: true,
        },
        "jform[addressOne][0]": {
            required: true,
        },
        "jform[icNum][0]": {
            required: true,
        },
        "jform[email][0]": {
            required: true,
        },
        "jform[taxExpiry]": {
            required: true,
        },
        "jform[loan]": {
            required: true,
        },
        "jform[cart]": {
            required: true,
        },
        "jform[specialPeril]": {
            required: true,
        },
        "jform[liabilityPass]": {
            required: true,
        },
        "jform[ehailing]": {
            required: true,
        },
        "jform[strike]": {
            required: true,
        },
        "jform[waiver]": {
            required: true,
        },
    },
    messages: {
        "jform[vehicleRegNo]": {
            required: "Please input car registration no.",
        },
        "jform[carBodyType]": {
            required: "Please select car body type.",
        },
    },
    errorPlacement: function (error, element) {
        element.parents(`.input-wrapper`).append(error);
    },
});

jQuery.validator.addMethod("validRegNum", function validRegNum(str, element) {
    var carReg = jQuery(`input[name='jform[vehicleRegNo]']`).val();
    var len = carReg.length;
    if (len >= 4) {
        return true;
    }
    else {
        return false;
    }
},
    "Registration number should be at least 4 chars"
);

export const step1ElementSelectors = [
    `[name='jform[vehicleRegNo]']`,
    `[name='jform[carBodyType]']`,
];

export const step2ElementSelectors = [
    `[name='jform[chassisNo]']`,
    `[name='jform[engineNo]']`,
    `[name='jform[functionalMod]']`,
    `[name='jform[carMakeYear]']`,
    `[name='jform[vehicleEngineCc]']`,
    `[name='jform[airbags]']`,
    `[name='jform[performanceMod]']`,
    `[name='jform[vehicleUse]']`,
    `[name='jform[purchaseDate]']`,
    `[name='jform[purchasePrice]']`,
    `[name='jform[marketValuation]']`,
    `[name='jform[carIsLocal]']`,
    `[name='jform[carFuel]']`,
    `[name='jform[vehType]']`,
    `[name='jform[seatNumber]']`,
    `[name='jform[abs]']`,
    `[name='jform[garage]']`,
    `[name='jform[occupation]']`,
    `[name='jform[ehailingProtection]']`,
    `[name='jform[ncd]']`,
    `[name='jform[claimPast]']`,
];

export const step2ClaimPastElementSelectors = [
    `[name='jform[damageOwn]']`,
    `[name='jform[windscreen]']`,
    `[name='jform[theft]']`,
    `[name='jform[tpClaim]']`,
];

export const step3ElementSelectors = [
    `[name='jform[Gender][]']`,
    `[name='jform[name][]']`,
    `[name='jform[dateOfBirth][]']`,
    `[name='jform[maritalStatus][]']`,
    `[name='jform[yearsOfDrivingExp][]']`,
    `[name='jform[driverOccupation][]']`,
    `[name='jform[occupationStatus][]']`,
    `[name='jform[occupationNature][]']`,
    `[name='jform[addressOne][]']`,
    `[name='jform[postalCode][]']`,
    `[name='jform[icNum][]']`,
    `[name='jform[email][]']`,
    `[name='jform[taxExpiry]']`,
    `[name='jform[loan]']`,
    `[name='jform[cart]']`,
    `[name='jform[specialPeril]']`,
    `[name='jform[liabilityPass]']`,
    `[name='jform[ehailing]']`,
    `[name='jform[strike]']`,
    `[name='jform[waiver]']`,
];

const registerElements = (selectors) => {
    selectors.forEach((selector) => {
        jQuery(selector).on('change keyup', _.debounce(() => {
            let validate = jQuery("form#adminForm").validate().element(selector);

            console.log({ [selector]: validate });
        }, 100));
    });
};
registerElements([
    ...step1ElementSelectors,
    ...step2ElementSelectors,
    ...step2ClaimPastElementSelectors,
    ...step3ElementSelectors,
]);

class BreakException extends Error {
    constructor(selector) {
        super('');
        this.selector = selector;
    }
}

class ValidatorResult {
    constructor(isValid, selector) {
        this.isValid = isValid;
        this.selector = selector;
    }
}

export const validateElements = (selectors, beforeValidateHook = () => {}) => {
    let validate = false;

    try {
        selectors.forEach((selector) => {
            try {
                let motorAccordionDuration = variables.motorAccordionDuration;
                variables.motorAccordionDuration = 0;
                functions.autoExpandMotorAccordion(selector);
                variables.motorAccordionDuration = motorAccordionDuration;

                beforeValidateHook();

                validate = jQuery("form#adminForm").validate().element(selector);
            }
            catch (e) {
                // ignore
                console.error(e);
                validate = false;
            }
    
            if (!validate) {
                throw new BreakException(selector);
            }
        });
    }
    catch (e) {
        return new ValidatorResult(false, e.selector);
    }

    return new ValidatorResult(validate, null);
}

export const handleValidationError = (selector, scrollTopAdjustment = -150, beforeAlertHook = () => {}) => {
    let targetElement = jQuery(selector);
    let afterAlert = () => {
        console.log(`selector: ${selector} not found`);
    };

    if(targetElement.length > 0) {
        if(targetElement.length > 1) {
            targetElement = targetElement.eq(0);
        }

        // auto scroll to show element to user
        let scrollTopTo = targetElement.offset().top;
        let targetElementInputWrapper = targetElement.parents(`.input-wrapper`);
        if (targetElementInputWrapper.length) {
            scrollTopTo = targetElementInputWrapper.offset().top;
        }
        scrollTopTo += scrollTopAdjustment;

        afterAlert = () => {
            functions.scrollTop(scrollTopTo);
        }
    }

    Swal.fire(
        "Invalid Data!",
        "Please complete the required input(s)!",
        "error",
    ).then(afterAlert);
}


// // car details 1 validation start
// var carDetailsOneInput = ".car-details-1 input"; // input & radio
// var carDetailsOneSelect = ".car-details-1 select"; // dropdown
// var carDetailsOneRadio = ".car-details-1 .radio-label span"; // radio span
// jQuery(carDetailsOneInput + "," + carDetailsOneSelect).on("click change keyup", function () {
//     var isValid = true;
//     var radioValid = false;
//     console.log(jQuery(this).attr("name") + "value: " + jQuery(this).val());
//     jQuery(carDetailsOneInput + "," + carDetailsOneSelect).each(function () {
//         if (jQuery(this).val() === "") {
//             isValid = false;
//             console.log(jQuery(this).attr("name") + " blank!");
//         }
//     });

//     jQuery(carDetailsOneRadio).each(function () {
//         if (jQuery(this).hasClass("active")) {
//             radioValid = true;
//             console.log("all radio good");
//         }
//     });

//     if (isValid && radioValid) {
//         var nextRow = ".car-details-2-accordion";
//         jQuery(".motor-accordion").not(nextRow).each(function (index) { // close all other except next one
//             jQuery(this).addClass("active"); // change -+ sign
//             jQuery(this).next().css("max-height", "0").css("padding-bottom", "0");
//             jQuery(this).next().slideUp();
//         });

//         jQuery(nextRow).removeClass("active");
//         jQuery(nextRow).next().css("max-height", "5000px").css("padding-bottom", "20px");
//         jQuery(nextRow).next().slideDown();

//     }

// });
// // car details 1 validation end
