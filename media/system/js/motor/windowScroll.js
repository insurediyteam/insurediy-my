import variables from './variables';
import functions from './functions';
import _ from 'lodash';

jQuery(window).scroll(() => {
    let step3 = jQuery(`.motor-accordion.my-step3`).eq(0);
    let scrollPos = jQuery(window).scrollTop();
    let shouldExpand = scrollPos > 135;
    if (variables.enableFixedQuotes) {
        if (shouldExpand) {
            jQuery('.res.motor-quote-result-middle-wrapper').addClass('fixed');
            jQuery('.res .motor-quote-result-block-body, .res .motor-quote-premium-header, .res .motor-quote-diy-points').addClass('my-transition-hide');
            jQuery('.res .motor-quote-result-block-header').addClass('height-auto');
            jQuery('.not-final').removeClass('hide');
            jQuery('.pad.insurediy-form').css('padding-top', '190px');
            jQuery('.loadingoverlay').css('background-color', 'transparent');

            if (functions.quoteStep() == 3) {
                step3.addClass(`first-accordion-margin-top`);
            }
        } else {
            functions.fixedQuoteResultsStyle();
            jQuery('.loadingoverlay').css('background-color', 'rgba(252, 252, 252, 0.5)');

            if (functions.quoteStep() == 3) {
                step3.removeClass(`first-accordion-margin-top`);
            }
        }
    }
});
