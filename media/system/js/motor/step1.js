import variables from './variables';
import functions from './functions';
import _ from 'lodash';
import Swal from 'sweetalert2';
import { validateElements, step1ElementSelectors, handleValidationError } from './validation';

jQuery(`#continue-to-select-model`).on('click', (obj) => {
    let thisElement = jQuery(obj);

    let validator = validateElements(step1ElementSelectors);

    if (!validator.isValid) {
        handleValidationError(validator.selector);

        return;
    }

    jQuery('.my-step1').hide();
    jQuery('.select-model-step').show();
    functions.scrollTop();
    jQuery(`.motor-accordion.select-model-step`).trigger('click');

    let currentVehRegNo = jQuery("input[name='jform\[vehicleRegNo\]']").val();
    if (
        !variables.selectedQuote.make ||
        (
            variables.lastVehicleData &&
            variables.lastVehicleData.vehicleData.vehRegNo != currentVehRegNo
        ) ||
        !variables.lastVehicleData
    ) {
        jQuery.LoadingOverlay("show", {
            image: "",
            custom: variables.customElement
        });
        disableCarMakeSelectOnChange = true;

        functions.getVehicleData((data, status) => {
            jQuery.LoadingOverlay('hide', false);

            if (status !== 'success') {
                Swal.fire({
                    icon: 'warning',
                    title: `Failed to get car info!`,
                    text: `Please key in your market value.`
                }).then(() => {
                    let marketValueInputElement = jQuery(`#select-model-market-value`);
                    marketValueInputElement.focus();
                    functions.scrollTop(marketValueInputElement.offset().top - 200);
                });
            }
            else {
                functions.lockCarDetailsInput();
            }

            functions.loadCarModels((object) => {
                disableCarMakeSelectOnChange = false;
                functions.scrollTop();
                if (data) {
                    jQuery(`#select-model-market-value`).prop('disabled', true);
                }
            });
        });
    }
});

jQuery(`select[name="jform[carBodyType]"]`).select2({
    ajax: {
        url: `${BASE_URL}index.php?option=com_insurediymotor&view=form&Itemid=528&lang=en&task=ajax.bodyTypes&format=json`,
        dataType: 'json'
    }
});

jQuery("input[name='jform\[vehicleRegNo\]']").on('input change keyup', _.debounce((el) => {
    let thisElement = jQuery(el.target);

    thisElement.val(thisElement.val().toUpperCase());
}));
