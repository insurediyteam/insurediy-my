import variables from './variables';
import functions from './functions';
import accounting from 'accounting';
import _ from 'lodash';

// on buy now click
jQuery(document).on('click', '.buy-now-btn', function (el) {
    let element = jQuery(this);

    jQuery(".motor-quote-result-middle-wrapper").removeClass("res").addClass("hide");
    jQuery('.pad.insurediy-form').css('padding-top', '0');
    jQuery(".insurediy-form.main-form").removeClass("pad");
    jQuery(".motor-quote-result-middle-wrapper").hide();
    jQuery('.my-step3').hide();
    jQuery('.my-step4').show();
    functions.scrollTop();

    if (element.prop('disabled')) {
        return false;
    }

    if (element.text() == 'RENEW') {
        window.location = element.data('renewal');

        return false;
    }

    jQuery('.my-last-choose-insurer').hide();
    let resultColumn = element.closest('.motor-result-column').eq(0);

    let quote = resultColumn.data('quote');
    let provider = resultColumn.data('provider');
    let quoteTotalValue = accounting.unformat(quote.total);

    let table = jQuery('table.summary-table').eq(0);
    table.find('.partner-title').eq(0).text(provider.name);
    table.find('ul.summary-desc-list .desc-list')
        .eq(0)
        .html(`<b>${provider.name} Plan</b>`);
    table.find('ul.summary-subtotal-list .desc-list')
        .eq(0)
        .text(accounting.formatMoney(quoteTotalValue, 'RM '));
    table.find('td.summary-total-amount')
        .eq(0)
        .text(accounting.formatMoney(quoteTotalValue, 'RM '));

    variables.selectedQuote.data = quote;
    variables.selectedQuote.provider = provider;
    variables.selectedQuote.final = quoteTotalValue;

    functions.calculateFinal();

    let summaryFieldValues = jQuery("div.order-summary .field-value");
    jQuery.each(summaryFieldValues, function (key, value) {
        let element = jQuery(value);
        let inputName = element.data('inputname');

        if (typeof (inputName) === 'undefined') {
            return;
        }

        if (inputName == 'jform[carModel]') {
            element.text(functions.getElementInputValue(jQuery(`#insurer-model-select-${provider.id}`), 'select'));
            return;
        }

        let inputElement = jQuery(`[name='${inputName}']`);
        let inputType = element.data('type');
        let inputValue = functions.getElementInputValue(inputElement, inputType);

        let isCurrency = element.data('currency') === true;
        if (isCurrency) {
            inputValue = accounting.formatMoney(inputValue, 'RM ');
        }

        element.text(inputValue);
    });

    let tab = jQuery('#summary-driver-details-tab');
    let details = jQuery('#summary-driver-details');
    tab.empty();
    details.empty();

    let driverFields = [
        'Gender',
        'salutation',
        'name',
        'dateOfBirth',
        'race',
        'maritalStatus',
        'yearsOfDrivingExp',
        'driverOccupation',
        'occupationStatus',
        'occupationNature',
        'addressOne',
        'addressTwo',
        'icNum',
        'email'
    ];
    jQuery(`#driver-wrapper .tab-wrapper`).each((i, tabWrapper) => {
        let tabWrapperElement = jQuery(tabWrapper);
        let tabTitle = i === 0 ? 'MAIN DRIVER' : `DRIVER ${i + 1}`;
        let tabPaneClass = i === 0 ? `tab-pane dsummary-pane active` : `tab-pane dsummary-pane`;

        tab.append(`
            <div class="${tabPaneClass}" onClick="changeDsummaryTab(this);" id="summary-driver-${i}" data-id="sdriver-panel-${i}">
                <img class="tab-pane-image" src="${BASE_URL}images/websiteicon/driver-logo.png" />
                <span class="tab-pane-title">${tabTitle}</span>
            </div>
        `);

        let detailsElement = i === 0 ? jQuery(`
            <div class="tab-wrapper dsummary-wrapper" id="sdriver-panel-${i}" style="margin-left: 10px;">
                <div class="panel-left" style="margin:0;padding-left:20px;">
                    <div class="input-wrapper">
                        <div class="field-title">Gender</div>
                        <div class="field-Gender field-value" data-type="radio"></div>
                    </div>
                    <div class="input-wrapper">
                        <div class="field-title">Salutation</div>
                        <div data-type="select" class="field-salutation field-value"></div>
                    </div>
                    <div class="input-wrapper">
                        <div class="field-title">Name</div>
                        <div class="field-name field-value"></div>
                    </div>
                    <div class="input-wrapper">
                        <div class="field-title">Date of Birth</div>
                        <div class="field-dateOfBirth field-value"></div>
                    </div>
                    <div class="input-wrapper">
                        <div class="field-title">Race</div>
                        <div data-type="select" class="field-race field-value"></div>
                    </div>
                    <div class="input-wrapper">
                        <div class="field-title">Marital Status</div>
                        <div class="field-maritalStatus field-value"></div>
                    </div>
                    <div class="input-wrapper">
                        <div class="field-title">Years of Driving Experience</div>
                        <div data-type="select" class="field-yearsOfDrivingExp field-value"></div>
                    </div>
                </div>
                <div class="panel-right" style="margin:0;padding-left: 15px;">
                    <div class="input-wrapper">
                        <div class="field-title">Occupation</div>
                        <div data-type="select" class="field-driverOccupation field-value"></div>
                    </div>
                    <div class="input-wrapper">
                        <div class="field-title">Occupation Status</div>
                        <div data-type="select" class="field-occupationStatus field-value"></div>
                    </div>
                    <div class="input-wrapper">
                        <div class="field-title">Nature of Occupation</div>
                        <div data-type="select" class="field-occupationNature field-value"></div>
                    </div>
                    <div class="input-wrapper">
                        <div class="field-title">Mailing Address Line 1</div>
                        <div class="field-addressOne field-value"></div>
                    </div>
                    <div class="input-wrapper">
                        <div class="field-title">Mailing Address Line 2</div>
                        <div class="field-addressTwo field-value"></div>
                    </div>
                    <div class="input-wrapper">
                        <div class="field-title">IC Number</div>
                        <div class="field-icNum field-value"></div>
                    </div>
                    <div class="input-wrapper">
                        <div class="field-title">Email</div>
                        <div class="field-email field-value"></div>
                    </div>
                </div>
            </div>
        `) : jQuery(`
            <div class="tab-wrapper dsummary-wrapper display-none" id="sdriver-panel-${i}" style="margin-left: 10px;">
                <div class="panel-left" style="margin:0;padding-left:20px;">
                    <div class="input-wrapper">
                        <div class="field-title">Name</div>
                        <div class="field-name field-value"></div>
                    </div>
                    <div class="input-wrapper">
                        <div class="field-title">IC Number</div>
                        <div class="field-icNum field-value"></div>
                    </div>
                </div>
                <div class="panel-right" style="margin:0;padding-left: 15px;">
                    <div class="input-wrapper">
                        <div class="field-title">Years of Driving Experience</div>
                        <div data-type="select" class="field-yearsOfDrivingExp field-value"></div>
                    </div>
                </div>
            </div>
        `)

        driverFields.forEach(function (value) {
            let inputElement = tabWrapperElement.find(`[name='jform[${value}][]']`);

            if (inputElement.length) {
                let detailFieldValueSelector = `div.field-value.field-${value}`;
                let inputType = detailsElement.find(detailFieldValueSelector).eq(0).data('type');
                let inputValue = functions.getElementInputValue(inputElement, inputType);
                detailsElement.find(detailFieldValueSelector).eq(0).text(inputValue);
            }
        });

        details.append(detailsElement);
    });

    let currentInsurer = jQuery(`[name='jform[currentInsurer]']`).val();
    if (!currentInsurer) {
        jQuery('.current-insurance-summary').hide();
    }

    // fill extra coverages
    let extraCovFields = jQuery('.field-value.extra-cov');
    extraCovFields.text('Not Available');
    extraCovFields.each((extraCovKey, extraCovElem) => {
        extraCovElem = jQuery(extraCovElem);
        let extraCovValue = variables.selectedQuote.data.extraCoverage[extraCovElem.data('name')];
        if (typeof (extraCovValue) !== 'undefined' && extraCovValue != 0) {
            extraCovElem.text(accounting.formatMoney(extraCovValue, 'MYR '));
        }
    });

    // set final basis coverage & market value
    jQuery(`#finalSumInsured`).val(quote.sumInsured);
    jQuery(`#finalBasisCoverage`).val(quote.basisCoverage);

    jQuery('.my-step4 .motor-accordion').eq(0).trigger('click');

    functions.saveQuote();
});
