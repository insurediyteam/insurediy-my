import axios from 'axios';
import _ from 'lodash';
import qs from 'qs';

axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

export default axios;

const csrfToken = JSON.parse(document.getElementsByClassName('joomla-script-options')[0].innerHTML)['csrf.token'];

export function dataWithCsrf(data) {
    return _.merge(data, {
        [csrfToken]: "1",
    });
}

export async function userLogin(username, password) {
    console.log('logging in...');
    try {
        let payload = dataWithCsrf({
            username: username,
            password: password,
            Submit: '',
            option: 'com_users',
            task: 'user.login',
            return: btoa(window.location),
        });

        let { data } = axios.post('/index.php?lang=en', qs.stringify(payload));
    } catch (error) {
        console.error(error);
    }
}
