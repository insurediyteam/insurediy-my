import variables from './motor/variables';
import functions from './motor/functions';
import _ from 'lodash';

Object.assign(window, functions);

require('./motor/motorAccordion');
require('./motor/validation');
require('./motor/windowScroll');
require('./motor/step1');
require('./motor/step-select-model');
require('./motor/step2');
require('./motor/step3');
require('./motor/step-final-result');
require('./motor/step4');

jQuery(document).ready(function() {
    let quoteStep = functions.quoteStep();

    /**
     * Restore session
     */
    if (variables.restoring) {
        let step = variables.steps[quoteStep - 1];

        if(selectedQuote) {
            variables.selectedQuote = selectedQuote;
        }

        if (step && typeof(step) === 'object') {
            let stepElement = jQuery(step.element);
            if(stepElement.length > 1) {
                stepElement = stepElement.eq(0);
            }
    
            stepElement.trigger(step.trigger);
        }

        if (step && typeof (step) === 'function') {
            step();
        }
    
        variables.restoring = 0;
    }

    if(quoteStep > 3) {
        jQuery(`#basisCoverage`).trigger('change');
    }

    functions.scrollTop();
    
    functions.updateBreadcrumbs(quoteStep);

    // date of birth init
    jQuery(`.dob-input`).datepicker({
        changeMonth: true,
        changeYear: true,
        showOn: "focus",
        dateFormat: "dd-M-yy",
        yearRange: jsOptions.datepicker.dob.yearRange,
        defaultDate: jsOptions.datepicker.dob.defaultDate,
        onChangeMonthYear: function (y, m, i) {
            var d = i.selectedDay;
            jQuery(this).datepicker("setDate", new Date(y, m - 1, d));
        }
    });

    jQuery(`select.occupation-select`).select2({
        ajax: {
            url: `${BASE_URL}index.php?option=com_insurediymotor&view=form&Itemid=528&lang=en&task=ajax.occupations&format=json`,
            dataType: 'json'
        }
    });

    jQuery(`select.loan-company-select`).select2({
        ajax: {
            url: `${BASE_URL}index.php?option=com_insurediymotor&Itemid=528&lang=en&task=ajax.loans&format=json`,
            dataType: 'json'
        }
    });

    jQuery(`.loginpopup, .regpopup`).on('click', (e) => {
        e.preventDefault();
        let thisElem = jQuery(e);
        let modalElem = jQuery(`#dialog-alert-login`);
        modalElem.modal('show');
        modalElem.find(`.pop-${thisElem.attr('class')}`).click();
    });

    // trigger claim past year update
    if (jQuery(`[name='jform[claimPast]']:checked`).length) {
        jQuery(`[name='jform[claimPast]']:checked`).click();
    }
    
    console.log([
        functions.getCurrentQuote(), 
        variables.selectedQuote,
        quoteStep,
    ]);
});
