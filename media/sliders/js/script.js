/**
 * Main JavaScript file
 *
 * @package         Sliders
 * @version         3.4.0
 *
 * @author          Peter van Westen <peter@nonumber.nl>
 * @link            http://www.nonumber.nl
 * @copyright       Copyright © 2014 NoNumber All Rights Reserved
 * @license         http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

(function($) {
	nnSliders = {
		show: function(id, scroll, ignoreparents) {
			var openparents = 0;
			var $el = $('#' + id);
			if (!ignoreparents) {
				$el.closest('.nn_sliders').closest('.accordion-body').each(function(i, parent) {
					if (!$(parent).hasClass('in')) {
						nnSliders.show(parent.id, 0);
						openparents = 1;
					}
				});
			}
			if (openparents) {
				nnSliders.show(id, scroll, 1);
			} else {
				if (!$el.hasClass('in')) {
					$el.collapse({
						toggle: true,
						parent: $el.parent().parent()
					});
					if (!$el.hasClass('in')) {
						$el.collapse('toggle');
					}
				} else {
				}

				$el.focus();
			}
		},
	}

	$(document).ready(function() {
		$('.nn_sliders > .accordion-group > .accordion-body').on('show show.bs.collapse', function(e) {
			$(this).parent().addClass('active');
			e.stopPropagation();
		});
		$('.nn_sliders > .accordion-group > .accordion-body').on('hidden hidden.bs.collapse', function(e) {
			$(this).parent().removeClass('active');
			e.stopPropagation();
		});

		if (nn_sliders_use_hash) {
			if (window.location.hash) {
				var id = window.location.hash.replace('#', '');
				if (!nn_sliders_urlscroll && $('#' + id + '.nn_sliders').length > 0) {
					// scroll to top to prevent browser scrolling
					$('html,body').animate({ scrollTop: 0 });
				}
				nnSliders.show(id, nn_sliders_urlscroll);
			}
			$('.nn_sliders > .accordion-group > .accordion-body').on('show show.bs.collapse', function(e) {
				// prevent scrolling on setting hash, so temp empty the id of the element
				var id = this.id
				this.id = '';
				window.location.hash = id;
				this.id = id;
				e.stopPropagation();
			});
		}
	});
})(jQuery);

/* For custom use */
function openAllSliders() {
	(function($) {
		$('.nn_sliders').each(function(e, el) {
			id = $(el).find('.accordion-body').first().attr('id');
			nnSliders.show(id);
		});
	})(jQuery);
}

function closeAllSliders() {
	(function($) {
		$('.nn_sliders').each(function(e, el) {
			id = $(el).find('.accordion-body').first().attr('id');
			var $el = $('#' + id);
			$el.collapse('hide');
		});
	})(jQuery);
}
