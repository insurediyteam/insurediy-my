<?php

/**
 * @package     Insurediy.Plugin
 * @subpackage  User.insurediy
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Insurediy User plugin
 *
 * @package     Insurediy.Plugin
 * @subpackage  User.insurediy
 * @since       1.5
 */
class PlgUserInsurediy extends JPlugin {

	/**
	 * Application object
	 *
	 * @var    JApplicationCms
	 * @since  3.2
	 */
	protected $app;

	/**
	 * Database object
	 *
	 * @var    JDatabaseDriver
	 * @since  3.2
	 */
	protected $db;

	/**
	 * True to use strong password encryption
	 *
	 * @var    boolean
	 * @since  3.2
	 */
	protected $useStrongEncryption;

	/**
	 * Remove all sessions for the user name
	 *
	 * Method is called after user data is deleted from the database
	 *
	 * @param   array    $user     Holds the user data
	 * @param   boolean  $success  True if user was succesfully stored in the database
	 * @param   string   $msg      Message
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	public function onUserAfterDelete($user, $success, $msg) {

		return true;
	}

	/**
	 * Utility method to act on a user after it has been saved.
	 *
	 * This method sends a registration email to new users created in the backend.
	 *
	 * @param   array    $user     Holds the new user data.
	 * @param   boolean  $isnew    True if a new user is stored.
	 * @param   boolean  $success  True if user was succesfully stored in the database.
	 * @param   string   $msg      Message.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function onUserAfterSave($user, $isnew, $success, $msg) {
		if ($isnew) {
			$referral_id = Myhelper::generateReferralId($user);
			$db = JFactory::getDBO();
			$query = $db->getQuery(TRUE)
					->update("#__users")
					->set("referral_id = " . $db->quote($referral_id))
					->where("id = " . $user['id']);
			return $db->setQuery($query)->execute();
		}
	}

}
