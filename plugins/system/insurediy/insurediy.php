<?php

defined('_JEXEC') or die;

// Note: order this after My System plugin

class plgSystemInsureDIY extends JPlugin {

	public function __construct(&$subject, $config) {
		parent::__construct($subject, $config);
	}

	public function onAfterInitialise() {
		// got referral id in request
		$refid = JFactory::getApplication()->input->get("refid", FALSE, "string");
		$user = MyHelper::getCurrentUser();
		$session = JFactory::getSession();
		if ($user && $user->referred_by) {
			$session->set(SESSION_KEY_REFID, NULL);
			return TRUE;
		}
		if ($refid) {
			$session->set(SESSION_KEY_REFID, $refid);
		}
		return TRUE;
	}

	public function onAfterRoute() {

	}

	public function onBeforeRender() {
		
	}

}
