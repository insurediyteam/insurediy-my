<?php
	
	define( '_JEXEC', 1 );
	define('JPATH_BASE', dirname(__FILE__));
	define( 'DS', DIRECTORY_SEPARATOR );
	//setlocale(LC_ALL, 'en_US.UTF-8');	
	set_time_limit(0);
	
	$filename = '';
	
	if(isset($_GET['filename'])) $filename = $_GET['filename']; else {echo 'You have not set filename';exit;}
	
	require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
	require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );
	
	$db = JFactory::getDBO();
	
	$image_file_path = JPATH_SITE.'/images/csv';
	
	$d = dir($image_file_path) or die("Wrong path: $image_file_path");
	
	while (false !== ($entry = $d->read())) {
		
		if($entry != '.' && $entry != '..' && !is_dir($entry)) {
			
			/* check the CSV extensions */
			if(!preg_match('/^'.$filename.'$/i', $entry))
			continue;
			
			/* if have same file name, use that id */
			if (($handle = fopen($image_file_path.'/'.$entry, "r")) !== FALSE) {
				
				$line = fgetcsv($handle, 4096, ","); // database header;
				
				$header = array();
				
				foreach($line as $t) :
				$header[] = $t;
				endforeach;
				
				$cnt = count($header);
				
    		while (($data = fgetcsv($handle, 4096, ",")) !== FALSE) {

					/* 	Data Field */
					$company	= 	trim($data[0]);
					$year_plan 			= 	trim($data[1]);
					$is_smoking	= 	trim($data[2]);
					$gender	= 	trim($data[3]);
					$age		=		(int) $data[4];
					$sum_insured = (int) $data[5];
					$lookup_index = trim($data[6]);
					
					$premium = (float) $data[8];
					
					$field_name = array('`plan_index_code`','`insurer_code`','`year_code`','`is_smoking`','`gender`','`age`','`sum_insured`','`price`');
					
					$values = array();
					$values[] = $db->Quote($lookup_index, false);
					$values[] = $db->Quote($company, false);
					$values[] = $db->Quote($year_plan, false);
					$values[] = $db->Quote((($is_smoking == 'NS')? 0 : 1), false);
					$values[] = $db->Quote($gender, false);
					$values[] = $db->Quote($age,false);
					$values[] = $db->Quote($sum_insured, false);
					$values[] = $db->Quote($premium, false);
					
					$query = " INSERT INTO #__insure_life_plans(".implode(',',$field_name).") VALUES (".implode(',', $values).");";
						$db->setQuery( $query );
						$db->query();
						
				}
				
    		fclose($handle);
				//unlink($image_file_path.'/'.$entry);
				
			}
			
		}
	}
	echo 'Success';
	$d->close();
	exit;
?>