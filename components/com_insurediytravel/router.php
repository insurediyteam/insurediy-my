<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediytravel
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Function to build a InsurediyTravel URL route.
 *
 * @return  array  The array of query string values for which to build a route.
 * @return  array  The URL route with segments represented as an array.
 * @since    1.5
 */
function InsurediyTravelBuildRoute(&$query) {
	// Declare static variables.
	static $items;
	static $default;
	static $form;


	$segments = array();

	// Get the relevant menu items if not loaded.
	if (empty($items)) {
		// Get all relevant menu items.
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$items = $menu->getItems('component', 'com_insurediytravel');

		// Build an array of serialized query strings to menu item id mappings.
		for ($i = 0, $n = count($items); $i < $n; $i++) {

			// Check to see if we have found the form menu item.
			if (empty($form) && !empty($items[$i]->query['view']) && ($items[$i]->query['view'] == 'form')) {
				$form = $items[$i]->id;
			}
		}

		// Set the default menu item to use for com_insurediytravel if possible.
		if ($form) {
			$default = $form;
		}
	}

	if (!empty($query['view'])) {
		switch ($query['view']) {
			default:
			case 'form':
				if ($query['Itemid'] = $form) {
					unset($query['view']);
				} else {
					$query['Itemid'] = $default;
				}
				break;
		}
	}

	return $segments;
}

/**
 * Function to parse a InsurediyTravel URL route.
 *
 * @return  array  The URL route with segments represented as an array.
 * @return  array  The array of variables to set in the request.
 * @since    1.5
 */
function InsurediyTravelParseRoute($segments) {
	$vars = array();

	// Only run routine if there are segments to parse.
	if (count($segments) < 1) {
		return;
	}

	// Get the package from the route segments.
	$form_id = array_pop($segments);

	if (!is_numeric($form_id)) {
		$vars['view'] = 'form';
		return $vars;
	}
	// Set the package id if present.
	if ($form_id) {
		// Set the package id.
		$vars['form_id'] = (int) $form_id;

		// Set the view to package if not already set.
		if (empty($vars['view'])) {
			$vars['view'] = 'form';
		}
	} else {
		JError::raiseError(404, JText::_('JGLOBAL_RESOURCE_NOT_FOUND'));
	}

	return $vars;
}
