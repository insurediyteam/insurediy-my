<?php
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	//JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-form select');
	JHtml::_('bootstrap.tooltip');
	JHtml::_('script', 'system/jquery.validate.js', false, true);
	
	$params = $this->state->get('params');
	$form = $this->form;
	$quotation = $this->quotation;
	$travellers = $quotation['travellers'];
	$loadTravellers = !empty($travellers);
	
	// remarketing email
	$user = $this->user;
	$products = [];
	$products[0]["premium"] = $quotation["selected_plan"]->premium;
	$products[0]["code"] = $quotation["selected_plan"]->insurer_code;

	$mailQueue = (object)array(
		"email" => $user->email,
		"products" => $products,
		"type" => "travel-after",
		"parent" => "travel"
	);

	RemarketingHelpersRemarketing::newRemarketing($mailQueue);
	// end of remarketing email
?>
<style>
	.chzn-container{margin-bottom:9px;}
</style>
<div class="insurediy-form">
	<div class="header-top-wrapper">
		<div class="header-top">
			<?php echo InsureDIYTravelHelper::renderHeader('icon-travel', JText::_('COM_INSUREDIYTRAVEL_PAGE_HEADING_YOUR_TRIP'), 3); ?>
		</div>
	</div>
	<div class="edit<?php echo $this->pageclass_sfx; ?> insurediy-form-content">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<form action="<?php echo JRoute::_('index.php?option=com_insurediytravel'); ?>" method="post" name="adminForm" id="adminForm" class="form-vertical">
			<div id="fields-holder">
				<?php
					for ($i = 0; $i < $this->no_of_travellers; $i++):
					if($i) {$form->setFormControl("jform" . ($i + 1));}
					$form->setValue("firstname", "", "");
					$form->setValue("lastname", "", "");
					$form->setValue("gender", "", "");
					$form->setValue("id_no", "", "");
					$form->setValue("relation", "", "");
					$form->setValue("dob", "", "");
					
					// if new input
					if($i == 0 && !empty($this->first_traveller)){
						$form->setValue("firstname", "", $this->first_traveller->name);
						$form->setValue("lastname", "", $this->first_traveller->lastname);
						$form->setValue("gender", "", $this->first_traveller->gender);
						$form->setValue("dob", "", $this->first_traveller->dob);
					}
					
					if ($loadTravellers) {
						$form->setValue("firstname", "", $travellers[$i]->firstname);
						$form->setValue("lastname", "", $travellers[$i]->lastname);
						$form->setValue("gender", "", $travellers[$i]->gender);
						$form->setValue("identity_type", "", $travellers[$i]->identity_type);
						$form->setValue("id_no", "", $travellers[$i]->id_no);
						$form->setValue("relation", "", $travellers[$i]->relation);
						$form->setValue("dob", "", $travellers[$i]->dob);
						$form->setValue("nationality", "", $travellers[$i]->nationality);
						$form->setValue("valid_pass_type", "", $travellers[$i]->valid_pass_type);
						$form->setValue("valid_pass_no", "", $travellers[$i]->valid_pass_no);
					} 
					
					//echo print_r($travellers);
				?>
				<script type="text/javascript">
					jQuery(document).ready(function () {
						<?php if($travellers[$i]->nationality != 'MY') { ?>
							jQuery('#jform<?php echo ($i)?($i+1):'' ?>_valid_pass_type').addClass('required');
							jQuery('#jform<?php echo ($i)?($i+1):'' ?>_valid_pass_no').addClass('required');
						<?php } ?>
					});
				</script>
				<fieldset id="fields" class="insurediy-form-fields">
					<legend>
						<?php echo JText::_("COM_INSUREDIYTRAVEL_TEXT_TRAVELLER") . " " . ($i + 1); ?>
					</legend>
					<div class="control-row">
						<div class="span4">
							<div class="control-group">
								<div class="control-label"><?php echo $form->getLabel('firstname'); ?></div>
								<div class="controls"><?php echo $form->getInput('firstname'); ?></div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="span4" style="margin:0px;">
							<div class="control-group">
								<div class="control-label"><?php echo $form->getLabel('lastname'); ?></div>
								<div class="controls"><?php echo $form->getInput('lastname'); ?></div>
							</div>
						</div>
						<div class="span4" style="margin:0px;">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('dob'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('dob'); ?></div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<div class="control-row">
						<div class="span4"><div class="control-label"><?php echo $this->form->getLabel('gender'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('gender'); ?></div>
							<div class="clear"></div>
						</div>
						<div class="span4" style="margin:0px;">
							<div class="control-label"><?php echo $this->form->getLabel('relation'); ?>
								<div id="error-container"></div>
							</div>
							<div class="controls"><?php echo $this->form->getInput('relation'); ?></div>
							<div class="clear"></div>
						</div>
						<div class="span4" style="margin:0px;">
							<div class="control-label"><?php echo $this->form->getLabel('nationality'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('nationality'); ?>
								<div class="error-container"></div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</div>
					<div class="control-row">
						<div class="span6" style="margin:0px;">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('id_no'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('id_no'); ?></div>
							</div>
						</div>
						<div class="span5" style="margin:0px;">
							<div>
								<div class="control-label"><?php echo $this->form->getLabel('valid_pass_no'); ?></div>
								<div class="control-group" style="float:left;width:60%;">
									<div class="controls"><?php echo $this->form->getInput('valid_pass_type'); ?><div class="error-container"></div></div>
								</div>
								<div class="control-group" style="float:left;width:40%">
									<div class="controls" ><?php echo $this->form->getInput('valid_pass_no'); ?><div class="error-container"></div></div>
								</div>	
								<div class="clear"></div>
							</div>
						</div>
						<div class="clear"></div>
					</div>

					<div class="clear"></div>
				</fieldset>
				<?php endfor; ?>
			</div>
			<div class="clear"></div>
			<input type="hidden" name="task" value="form.step3save" />
			<input type="hidden" name="quotation_id" value="<?php echo $quotation['id']; ?>" />
			
			<?php echo JHtml::_('form.token'); ?>
			<div style="float:left;margin:15px 0;">
				<div>{modulepos insurediy-secured-ssl}</div>
			</div>
			<div class="btn-toolbar" style="float:right">
				<div>
					<button style="margin-right:10px;" type="button" onclick="javascript:document.adminForm.task.value = 'form.back';
					document.adminForm.submit();" class="btn btn-primary validate"><?php echo JText::_('BTN_BACK') ?></button>
					<button type="submit" class="btn btn-primary validate" ><?php echo JText::_('JCONTINUE') ?></button>
				</div>
			</div>
			<div class="clear"></div>
		</form>
	</div>
</div>

<script>
	jQuery(document).ready(function () {

		<?php for ($i = 0; $i < $this->no_of_travellers; $i++): ?>
		jQuery('#jform<?php echo ($i)?($i+1):'' ?>_nationality').change(function() {
			if(jQuery(this).val() != 'MY') {
				jQuery('#jform<?php echo ($i)?($i+1):'' ?>_valid_pass_type').addClass('required');
				jQuery('#jform<?php echo ($i)?($i+1):'' ?>_valid_pass_type').attr('required',true);
				jQuery('#jform<?php echo ($i)?($i+1):'' ?>_valid_pass_type').chosen();
				jQuery('#jform<?php echo ($i)?($i+1):'' ?>_valid_pass_no').addClass('required');
				jQuery('#jform<?php echo ($i)?($i+1):'' ?>_valid_pass_no').attr('required',true);
			} else {
				jQuery('#jform<?php echo ($i)?($i+1):'' ?>_valid_pass_type').removeClass('required');
				jQuery('#jform<?php echo ($i)?($i+1):'' ?>_valid_pass_type').removeAttr('required');
				jQuery('#jform<?php echo ($i)?($i+1):'' ?>_valid_pass_type').chosen();
				jQuery('#jform<?php echo ($i)?($i+1):'' ?>_valid_pass_no').removeClass('required');
				jQuery('#jform<?php echo ($i)?($i+1):'' ?>_valid_pass_no').removeAttr('required');
			}
		});
					
		<?php endfor; ?>
		
		jQuery("#adminForm").validate({
			ignore: ":hidden:not(select)", // <-- option so that hidden elements are validated
			errorPlacement: function(error, element) {
				switch(element.attr("name")) {
					<?php for ($i = 0; $i < $this->no_of_travellers; $i++): ?>
					case 'jform<?php echo ($i)?($i+1):'' ?>[nationality]': error.insertAfter("#jform<?php echo ($i)?($i+1):'' ?>_nationality_chzn");break;
					case 'jform<?php echo ($i)?($i+1):'' ?>[valid_pass_type]': error.insertAfter("#jform<?php echo ($i)?($i+1):'' ?>_valid_pass_type_chzn");break;
					<?php endfor; ?>
					default:error.insertAfter(element);break;
				}
			}
		});
		
		var jForm = jQuery("form#adminForm");
		
		jForm.submit(function () {
			var errors = jForm.find("label.error");
			if (errors.length > 0) {
				var label = jForm.find("label.error:first");
				jQuery('html, body').animate({
					'scrollTop': label.offset().top - 400
				}, 300);
			}
		});
	});
</script>