<?php
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	//JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-form select');
	JHtml::_('MyBehavior.jsInsurediy');
	JHtml::_('bootstrap.tooltip');
	JHtml::_('script', 'system/jquery.validate.js', false, true);
	
	$currency = $this->currency;
	$quotation = $this->quotation;
	$form = & $this->form;
	$fldGroup = "contact-details";
	$beneficiaries = $this->quotation["beneficiaries"];
	
?>

<script type="text/javascript">
	
	var row = <?php echo count($beneficiaries); ?>;
	var jRow;
	
	function duplicateRow(div_id) {
		if(jQuery('.epi-beneficiaries-wrapper').length >= 4) return;
		row = row + 1;
		var jOriginal = jQuery("#" + div_id);
		var jClone = jRow.clone();
		var newId = jOriginal.prop("id") + "_" + row;
		jClone.prop("id", newId);
		jClone.addClass("epi-beneficiaries-wrapper");
		jClone.find("#jform_ben_relationship_chzn").remove();
		
		jClone.find("input").each(function () {
			var jThis = jQuery(this);
			jThis.prop("id", jThis.prop("id") + "_" + row);
			jThis.prop("name", jThis.prop("name").substr(0,jThis.prop("name").length-2) + "[" + row + "]");
			jThis.prop("readonly", "");
			jThis.val("");
			if (jThis.hasClass("validate-calendar")) {
				jClone.find(".ui-datepicker-trigger").remove();
				jThis.removeClass("hasDatepicker").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "<?php echo JURI::root() ?>/media/system/images/ico-calendar.png",
					dateFormat: "dd-mm-yy",
					yearRange: "-80:+0",
					maxDate:0,
					minDate:"-80Y +1D"
				});
			}
		});
		
		jClone.find("select").each(function () {
			var jSelect = jQuery(this);
			var newSelectId = jSelect.prop("id") + "_" + row;
			jSelect.prop("id", newSelectId);		
			jSelect.prop("name", jSelect.prop("name").substr(0,jSelect.prop("name").length-2) + "[" + row + "]");
			jSelect.chosen({
				disable_search_threshold: 10,
				allow_single_deselect: true,
				width: "95%"
			});
		});
		
		jClone.prepend("<button type=\"button\" class=\"close\" aria-hidden=\"true\" onclick=\"javascript:removeBeneficiary('#" + newId + "');\">×</button>");
		jQuery("#ajax-load-existing-beneficiaries").append(jClone);
		
		jQuery("select[id^='jform_ben_nationality_"+row+"']").change(function(){
			if(jQuery(this).val() !='MY') {
				jQuery('#jform_ben_valid_pass_type_'+ row).addClass('required');
				jQuery('#jform_ben_valid_pass_type_'+ row).attr('required',true);
				jQuery('#jform_ben_valid_pass_type_'+ row).chosen();
				jQuery('#jform_ben_valid_pass_no_'+ row).addClass('required');
				jQuery('#jform_ben_valid_pass_no_'+ row).attr('required',true);
			} else {
				jQuery('#jform_ben_valid_pass_type_'+ row).removeClass('required');
				jQuery('#jform_ben_valid_pass_type_'+ row).removeAttr('required');
				jQuery('#jform_ben_valid_pass_type_'+ row).chosen();
				jQuery('#jform_ben_valid_pass_no_'+ row).removeClass('required');
				jQuery('#jform_ben_valid_pass_no_'+ row).removeAttr('required');
			}
		});
		
	}
	
	
	
	function removeBeneficiary(id) {
		jQuery(id).remove();
	}
	
	function toggleEdit(div_id, isEdit) {
		//do edit
		if (isEdit === true) {
			$(div_id).toggleClass("readonly", true);
			} else if (isEdit === false) {
			$(div_id).toggleClass("readonly", false);
			} else {
			$(div_id).toggleClass("readonly");
		}
		if (jQuery("#" + div_id).hasClass("readonly")) {
			$$("#" + div_id + " input:not('.ignore-edit')").set("readonly", "readonly");
			//			$$("#" + div_id + " select:not('.ignore-edit')").set("disabled", "disabled");
			jQuery("#" + div_id + " select:not('.ignore-edit')").prop('disabled', true).trigger("liszt:updated");
			jQuery("#" + div_id + " select:not('.ignore-edit')").prop('disabled', false);
			} else {
			$$("#" + div_id + " input:not('.ignore-edit')").set("readonly", "");
			//			$$("#" + div_id + " select:not('.ignore-edit')").set("disabled", "");
			jQuery("#" + div_id + " select:not('.ignore-edit')").prop('disabled', false).trigger("liszt:updated");
		}
		//		$$("#" + div_id + " .chzn-container").toggleClass("chzn-disabled");
	}
	
	function toggleThis(div_id, btn_id) {
		new Fx.Slide(div_id, {resetHeight: true}).toggle();
		$(btn_id).toggleClass("active");
	}
	
	function nameChanged(showMsg) {
		var fname = jQuery("#jform_contact_firstname");
		var lname = jQuery("#jform_contact_lastname");
		var accName = jQuery("#jform_bank_acct_holder_name");
		accName.val(fname.val() + " " + lname.val());
		if (showMsg) {
			$('insurediy-popup-box').setStyle('display', 'block');
		}
	}
	
	function checkData(div_id) {
		
		var firstTime = <?php echo json_encode(($this->current_user->is_firsttime) ? 1 : 0) ?>;
		if (firstTime == "1") {
			return false;
		}
		
		return true;
	}
	
	function onNumberOnlyAndFormat(input_id) {
		var jInput = jQuery(input_id);
		jInput.keydown(function (event) {
			var jThis = jQuery(this);
			var val = jThis.val();
			var hasDot = val.indexOf(".");
			// Allow only backspace and delete
			if (event.keyCode == 9 || event.keyCode == 46 || event.keyCode == 8 || (event.keyCode == 190 && hasDot == -1)) {
				// let it happen, don't do anything
			}
			else {
				// Ensure that it is a number and stop the keypress
				if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105)) {
					// 0-9
					} else {
					event.preventDefault();
				}
			}
		});
		jInput.on("keyup change", function () {
			var jThis = jQuery(this);
			var val = jThis.val();
			if (val.length > 0) {
				jThis.val(numberFormat(val));
			}
		});
	}
	
	function numberFormat(number) {
		number = parseFloat(number.replace(/,/g, ''));
		return number.formatMoney(0, '.', ',');
	}
	
	Number.prototype.formatMoney = function (c, d, t) {
		var n = this;
		c = isNaN(c = Math.abs(c)) ? 2 : c;
		d = d === undefined ? "." : d;
		t = t === undefined ? "," : t;
		var s = n < 0 ? "-" : "";
		var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "";
		var j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	}
	
	function initializeForm() {
		jQuery("#adminForm").validate({
			ignore: ":hidden:not(select)", // <-- option so that hidden elements are validated
			errorPlacement: function(error, element) {
				if(element.hasClass('chzn-done')) {
					error.insertAfter('#' + element.attr("id") + '_chzn');
				} else {
					error.insertAfter(element);
				}
			}
			
		});
	}

	window.addEvent('domready', function () {
		
		var jOriginal = jQuery("#existing-beneficiaries");
		jRow = jOriginal.clone();

		initializeForm();
		
		jQuery('#jform_contact_nationality').change(function() {
			if(jQuery(this).val() != 'MY') {
				jQuery('#jform_contact_valid_pass_type').addClass('required');
				jQuery('#jform_contact_valid_pass_type').attr('required',true);
				jQuery('#jform_contact_valid_pass_type').chosen();
				jQuery('#jform_contact_valid_pass_no').addClass('required');
				jQuery('#jform_contact_valid_pass_no').attr('required',true);
				} else {
				jQuery('#jform_contact_valid_pass_type').removeClass('required');
				jQuery('#jform_contact_valid_pass_type').removeAttr('required');
				jQuery('#jform_contact_valid_pass_type').chosen();
				jQuery('#jform_contact_valid_pass_no').removeClass('required');
				jQuery('#jform_contact_valid_pass_no').removeAttr('required');
			}
		});
		
		jQuery("select[id^='jform_ben_nationality']").on('chosen:ready change',function(){
			var row = jQuery(this).attr("id").replace("jform_ben_nationality","");
			row = row.replace("_","");
			if(row){ row = "_" + row;}
			
			if(jQuery(this).val() !='MY') {
				jQuery("#jform_ben_valid_pass_type" + row).addClass('required');
				jQuery("#jform_ben_valid_pass_type" + row).attr('required',true);
				jQuery("#jform_ben_valid_pass_type" + row).chosen();
				jQuery("#jform_ben_valid_pass_no" + row).addClass('required');
				jQuery("#jform_ben_valid_pass_no" + row).attr('required',true);
			} 
			else 
			{
				jQuery("#jform_ben_valid_pass_type" + row).removeClass('required');
				jQuery("#jform_ben_valid_pass_type" + row).removeAttr('required');
				jQuery("#jform_ben_valid_pass_type" + row).chosen();
				jQuery("#jform_ben_valid_pass_no" + row).removeClass('required');
				jQuery("#jform_ben_valid_pass_no" + row).removeAttr('required');
			}
			
		});
		
		jQuery("select[id^='jform_ben_nationality']").chosen().trigger("chosen:ready");
		
		var jForm = jQuery("form#adminForm");
		jForm.submit(function () {
			
			var errors = jForm.find("label.error");
			if (errors.length > 0) {
				var label = jForm.find("label.error:first");
				jQuery('html, body').animate({
					'scrollTop': label.offset().top - 400
				}, 300);
			}
			
		});
		
		var fname = jQuery("#jform_contact_firstname");
		var lname = jQuery("#jform_contact_lastname");
		var hasData = checkData('contact-details-fields');
		
		if (hasData) {
			toggleEdit('contact-details-fields', true);
			} else {
			jQuery("#edit-btn").hide();
		}
		var idType = jQuery("#jform_contact_identity_type");
		var doe = jQuery("#contact_identity_doe");
		
	});
</script>
<style>
	.chzn-container{margin-bottom:9px;}
	.close{position:absolute;right:20px;}
</style>
<div class="insurediy-form bb-form">
	<div class="header-top-wrapper">
		<div class="header-top">
			<?php echo InsureDIYTravelHelper::renderHeader('icon-travel', JText::_('COM_INSUREDIYTRAVEL_PAGE_HEADING_CONTACT_DETAILS'), 3); ?>
		</div>
	</div>
	<div style="padding:20px;margin-top: 58px;">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<form action="<?php echo JRoute::_('index.php?option=com_insurediytravel&view=form'); ?>" method="post" name="adminForm" id="adminForm" novalidate class="form-vertical">
			<fieldset class="insurediy-travel-data insurediy-form-fields">
				<div id="contact-details-fields" class="contact-details-fields">
					<div class="row-left">
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('contact_firstname'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('contact_firstname'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('contact_lastname'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('contact_lastname'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('contact_contact_no'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('contact_contact_no'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('contact_nationality'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('contact_nationality'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('contact_identity_no'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('contact_identity_no'); ?>
								<div class="error-container"></div>
							</div>
						</div>
					</div>
					<div class="row-mid"></div>
					<div class="row-right">
						<div>
							<div class="span12 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_street_name'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_street_name'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div>
							<div class="span12 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_street_name2'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_street_name2'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div>
							<div class="span4 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_city'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_city'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="span4 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_district_name'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_district_name'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="span4 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_postalcode'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_postalcode'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div>
							<div class="span6 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_country'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_country'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div>
							<div class="span10 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_valid_pass_no'); ?></div>
								<div style="width: 59%;margin-right: 1%;float: left;">
									<div class="controls">
										<?php echo $this->form->getInput('contact_valid_pass_type'); ?>
										<div class="error-container"></div>
									</div>
								</div>
								<div style="width: 40%;float: left;">
									<div class="control-group">
										<div class="controls">
											<?php echo $this->form->getInput('contact_valid_pass_no'); ?>
											<div class="error-container"></div>
										</div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
							<div class="span2 control-group">
								<div class="controls" style="padding-top: 30px;text-align: right;">
									<a id="edit-btn" href="javascript:toggleEdit('contact-details-fields')" class="btn btn-primary" style="padding: 5px 20px;"><?php echo JText::_("BTN_EDIT"); ?></a>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				
				<!-- Beneficiaries -->
				<div class="insurediy-beneficiaries">
					<div class="insurediy-contact-detail-add-another-policy">
						<div class="title-text"><a href="javascript:duplicateRow('existing-beneficiaries');initializeForm();"><?php echo JText::_("COM_INSUREDIYTRAVEL_ADD_ANOTHER_BENEFICIARIES"); ?></a></div>
					</div>
					<br />
					<div id="ajax-load-existing-beneficiaries">
						<?php if (empty($beneficiaries)): // EMPTY ?>
						<div id="existing-beneficiaries" class="epi-beneficiaries-wrapper">
							<div>
								<div class="control-group span3">
									<div class="control-label"><?php echo $form->getLabel("ben_name"); ?></div>
									<div class="controls">
										<?php echo $form->getInput("ben_name"); ?><div class="error-container"></div>
									</div>
								</div>
								<div class="control-group span6">
									<div class="control-label"><?php echo $form->getLabel("ben_identity_card_no"); ?></div>
									<div class="controls" style="width:100%;"><?php echo $form->getInput("ben_identity_card_no"); ?><div class="error-container"></div></div>
								</div>
								<div class="control-group span3">
									<div class="control-label"><?php echo $form->getLabel("ben_gender"); ?></div>
									<div class="controls" style="width:100%;"><?php echo $form->getInput("ben_gender"); ?></div>
								</div>
								<div class="clear"></div>
							</div>
							<div>
								<div class="control-group span3">
									<div class="control-label"><?php echo $form->getLabel("ben_address"); ?></div>
									<div class="controls" style="position:relative;"><?php echo $form->getInput("ben_address"); ?><div class="error-container"></div></div>
								</div>
								<div class="control-group span6">
									<div class="control-label"><?php echo $form->getLabel("ben_relationship"); ?></div>
									<div class="controls"><?php echo $form->getInput("ben_relationship"); ?><div class="error-container"></div></div>
								</div>
								<div class="control-group span3">
									<div class="control-label"><?php echo $form->getLabel("ben_dob"); ?></div>
									<div class="controls"><?php echo $form->getInput("ben_dob"); ?></div>
								</div>
								<div class="clear"></div>
							</div>
							<div>
								<div class="control-group span3">
									<div class="control-label"><?php echo $form->getLabel("ben_nationality"); ?></div>
									<div class="controls"><?php echo $form->getInput("ben_nationality"); ?></div>
								</div>
								<div class="control-group span6">
									<div class="control-label"><?php echo $form->getLabel("ben_valid_pass_no"); ?></div>
									<div class="controls" style="width:100%;">
										<div class="" style="float:left;width:60%;"><?php echo $form->getInput("ben_valid_pass_type"); ?></div>
										<div class="" style="float:left;width:40%;"><?php echo $form->getInput("ben_valid_pass_no"); ?></div>
										<div class="clear"></div>
										<div class="error-container"></div>
									</div>
								</div>
								<div class="control-group span3">
									<div class="control-label"><?php echo $form->getLabel("ben_percent_share"); ?></div>
									<div class="controls" style="width:100%;"><?php echo $form->getInput("ben_percent_share"); ?><div class="error-container"></div></div>
								</div>
								<div class="clear"></div>
							</div>
							<div class="clear"></div>
							<br />
							<div class="grey-hrule"></div>
							<br />
						</div>
						<?php else: ?>
						<?php
							foreach ($beneficiaries as $k => $beneficiary):
							
							$form->setValue("ben_name", "", $beneficiary->ben_name);
							$form->setValue("ben_identity_card_no", "", $beneficiary->ben_identity_card_no);
							$form->setValue("ben_gender", "", $beneficiary->ben_gender);
							$form->setValue("ben_address", "", $beneficiary->ben_address);
							$form->setValue("ben_relationship", "", $beneficiary->ben_relationship);
							$form->setValue("ben_nationality", "", $beneficiary->ben_nationality);
							$form->setValue("ben_dob", "", (($beneficiary->ben_dob != '0000-00-00')?$beneficiary->ben_dob:''));
							$form->setValue("ben_percent_share", "", $beneficiary->ben_percent_share);
							$form->setValue("ben_valid_pass_type", "", $beneficiary->ben_valid_pass_type);
							$form->setValue("ben_valid_pass_no", "", $beneficiary->ben_valid_pass_no);
							
							if($k) {
								$row = $k + 1;
								$form->setFieldAttribute("ben_name", "id", "ben_name_" . $row);
								$form->setFieldAttribute("ben_identity_card_no", "id", "ben_identity_card_no_" . $row);
								$form->setFieldAttribute("ben_gender", "id", "ben_gender_" . $row);
								$form->setFieldAttribute("ben_address", "id", "ben_address_" . $row);
								$form->setFieldAttribute("ben_relationship", "id", "ben_relationship_" . $row);
								$form->setFieldAttribute("ben_nationality", "id", "ben_nationality_" . $row);
								$form->setFieldAttribute("ben_dob", "id", "ben_dob_" . $row);
								$form->setFieldAttribute("ben_percent_share", "id", "ben_percent_share_" . $row);
								$form->setFieldAttribute("ben_valid_pass_type", "id", "ben_valid_pass_type_" . $row);
								$form->setFieldAttribute("ben_valid_pass_no", "id", "ben_valid_pass_no_" . $row);
							}
							
						?>
						<script type="text/javascript">
						jQuery(document).ready(function () {
							jQuery('#existing-beneficiaries<?php echo ($k) ? "_" . $k : ""; ?>').find('input').each(function () {
								var jThis = jQuery(this);
								jThis.prop("name", jThis.prop("name").substr(0,jThis.prop("name").length-2) + "[<?php echo $k ?>]");
							});	
						<?php /*if($travellers[$i]->nationality != 'MY') { ?>
							jQuery('#jform<?php echo ($k)?($k+1):'' ?>_valid_pass_type').addClass('required');
							jQuery('#jform<?php echo ($k)?($k+1):'' ?>_valid_pass_no').addClass('required');
						<?php }*/ ?>
							
						});
						</script>
						<div id="existing-beneficiaries<?php echo ($k) ? "_" . $k : ""; ?>" class="epi-beneficiaries-wrapper">
							<?php if ($k): ?>
							<button type="button" class="close" aria-hidden="true" onclick="javascript:removeBeneficiary('#existing-beneficiaries<?php echo ($k) ? "_" . $k : ""; ?>');">×</button>
							<?php endif; ?>
							<div>
								<div class="control-group span3">
									<div class="control-label"><?php echo $form->getLabel("ben_name"); ?></div>
									<div class="controls">
										<?php echo $form->getInput("ben_name"); ?><div class="error-container"></div>
									</div>
								</div>
								<div class="control-group span6">
									<div class="control-label"><?php echo $form->getLabel("ben_identity_card_no"); ?></div>
									<div class="controls" style="width:100%;"><?php echo $form->getInput("ben_identity_card_no"); ?><div class="error-container"></div></div>
								</div>
								<div class="control-group span3">
									<div class="control-label"><?php echo $form->getLabel("ben_gender"); ?></div>
									<div class="controls" style="width:100%;"><?php echo $form->getInput("ben_gender"); ?></div>
								</div>
								<div class="clear"></div>
							</div>
							<div>
								<div class="control-group span3">
									<div class="control-label"><?php echo $form->getLabel("ben_address"); ?></div>
									<div class="controls" style="position:relative;"><?php echo $form->getInput("ben_address"); ?><div class="error-container"></div></div>
								</div>
								<div class="control-group span6">
									<div class="control-label"><?php echo $form->getLabel("ben_relationship"); ?></div>
									<div class="controls"><?php echo $form->getInput("ben_relationship"); ?><div class="error-container"></div></div>
								</div>
								<div class="control-group span3">
									<div class="control-label"><?php echo $form->getLabel("ben_dob"); ?></div>
									<div class="controls"><?php echo $form->getInput("ben_dob"); ?></div>
								</div>
								<div class="clear"></div>
							</div>
							<div>
								<div class="control-group span3">
									<div class="control-label"><?php echo $form->getLabel("ben_nationality"); ?></div>
									<div class="controls"><?php echo $form->getInput("ben_nationality"); ?></div>
								</div>
								<div class="control-group span6">
									<div class="control-label"><?php echo $form->getLabel("ben_valid_pass_no"); ?></div>
									<div class="controls" style="width:100%;">
										<div class="" style="float:left;width:60%;"><?php echo $form->getInput("ben_valid_pass_type"); ?></div>
										<div class="" style="float:left;width:40%;"><?php echo $form->getInput("ben_valid_pass_no"); ?></div>
										<div class="clear"></div>
										<div class="error-container"></div>
									</div>
								</div>
								<div class="control-group span3">
									<div class="control-label"><?php echo $form->getLabel("ben_percent_share"); ?></div>
									<div class="controls" style="width:100%;"><?php echo $form->getInput("ben_percent_share"); ?><div class="error-container"></div></div>
								</div>
								<div class="clear"></div>
							</div>
							<div class="clear"></div>
							<br />
							<div class="grey-hrule"></div>
							<br />
						</div>	
						<?php endforeach; ?>	
						<?php endif; ?>
					</div>
					
					
					<!-- Social Stuffs Start -->
					<div class="insurediy-contact-detail-promotion-code" style="margin-bottom: 10px;">
						<div class="title-text"><?php echo JText::_("SOCIAL_PROMO_CODE_TITLE"); ?></div>
						<div class="title-text2">
							<span style="font-size:14px;line-height: 30px;"><strong><?php echo JText::_("SOCIAL_PROMO_CODE_LABEL"); ?></strong></span>&nbsp;&nbsp;&nbsp;
						</div>
						<div style="float:right;width: 262px;">
							<input name="jform[promo_code]" id="jform_promo_code" type="text" /> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("SOCIAL_PROMO_CODE_DESC"); ?>">
						</div>
						<div class="clear"></div>
					</div>
					
					<div class="grey-hrule"></div>
					<div class="container5 clearfix" style="padding: 20px 10px; margin-top:10px;">
						<div class="clearfix">
							<div class="cpan5"><img src="images/DIY_Rewards.png" style="margin: 10px auto;display: block;" alt="Rewards" /></div>
							<div class="cpan55">
								<?php if (!$this->user->referred_by): ?>
								<div class="clearfix">
									<div class="cpan2">
										<div class="blue-title-text"><?php echo JText::_("SOCIAL_REFERRAL_TITLE"); ?> <img class="hasTooltip" alt="help-quote" style="margin-top: -5px;" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("SOCIAL_REFERRAL_DESC"); ?>"/></div>
										<span><?php echo JText::_("SOCIAL_REFERRAL_MSG_HEADER"); ?> <?php echo JText::_("SOCIAL_REFERRAL_MSG"); ?></span>
									</div>
									<div style="float:right;width: 262px;">
										<?php echo $form->getInput("referred_by"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("SOCIAL_REFERRAL_DESC"); ?>" />
									</div>
								</div>
								<div class="spacer-1"></div>
								<div class="grey-hrule"></div>
								<div class="spacer-1"></div>
								<?php endif; ?>
								
								<div class="clearfix">
									<div class="cpan2">
										<span class="blue-title-text" style="font-size: 19px;font-family:'sourcesanspro-semibold' "><?php echo JText::_("SOCIAL_USE_YOUR_SOCIAL_NETWORK"); ?><br/>
										<?php echo JText::_("SOCIAL_SHARE_TITLE"); ?></span>
										<div class="spacer-1"></div>
										<span style="font-size: 13px;"><?php echo InsureDIYTravelHelper::getRefExp(); ?></span>
									</div>
									<div class="cpan2">
										<div style="float:right;" class="social-fb-msg">
											<?php echo InsureDIYTravelHelper::getRefMsg(); ?>
										</div>
										<div class="clearfix">
											<div style="float:right;margin-right: 20px;padding-top:6px;">
												<div class="social-btn-wrapper">
													<script type="text/javascript">var sharer = "<?php echo InsureDIYHelper::getFbSharer() ?>";</script>
													<a href="javascript: void(0)" target="_parent" onclick="window.open(sharer, 'sharer', 'width=626,height=436');"><img src="images/layout-icons/facebook.png"></a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
					<!-- Social Stuffs End-->
					<input type="hidden" name="task" value="form.step4save" />
					<input type="hidden" name="quotation_id" value="<?php echo $quotation['id']; ?>" />
					
					<?php echo JHtml::_('form.token'); ?>
					<div style="float:left;margin:15px 0;">
						<div>{modulepos insurediy-secured-ssl}</div>
					</div>
					<div class="btn-toolbar" style="float:right">
						<div>
							<button style="margin-right:10px;" type="button" onclick="javascript:document.adminForm.task.value = 'form.back';
							document.adminForm.submit();" class="btn btn-primary validate"><?php echo JText::_('BTN_BACK') ?></button>
							<button type="submit" class="btn btn-primary validate"><?php echo JText::_('JCONTINUE') ?></button>
						</div>
					</div>
					<div class="clear"></div>
				</fieldset>
			</form>
		</div>
	</div>
	<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>
	
	<?php /* Popup Box */ ?>
	<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
		<div class="header">
			<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
			<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
			<div class="clear"></div>
		</div>
		<div class="padding">
			<div><?php echo JText::_("COM_INSUREDIYTRAVEL_NAME_CHANGE_NOTICE_MSG"); ?></div>
			<div style="text-align:right;"><input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
				return f
			alse;" value="<?php echo JText::_("JOK"); ?>" /></div>
		</div>
	</div>
	
	<script>
		dataLayer.push({
			'event': 'checkoutOption',
			'ecommerce': {
				'checkout': {
					'actionField': {'step': 2}
				}
			}
		});
	</script>
	
