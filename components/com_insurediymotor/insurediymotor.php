<?php

use Services\Bugsnag;
use Services\BootEloquent;

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediymotor
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

require_once JPATH_COMPONENT . '/vendor/autoload.php';

date_default_timezone_set('Asia/Kuala_Lumpur');

$bugsnag = new Bugsnag;
$bugsnag->register();

$bootEloquent = new BootEloquent;
$bootEloquent->run();

JLoader::import('incs.form.field', JPATH_ROOT);

require_once JPATH_COMPONENT . '/helpers/insurediymotor.php';
//require_once JPATH_ROOT . '/components/com_insurediypromocodes/helpers/insurediypromocodes.php';
// require_once JPATH_ROOT . '/components/com_joomailermailchimpintegration/helpers/joomailermailchimphelper.php';

// $motorQuotation = new MotorQuotationMy();
// $motorQuotation->run();

$controller = JControllerLegacy::getInstance('InsureDIYMotor');

$task = JFactory::getApplication()->input->get('task');

$controller->execute($task);
$controller->redirect();
