<?php

use Services\Client\ComparisonTableData;

return [
    'provider' => 'amg',
    'payload' => [
        "jform[marketVal]" => "150000",
        "jform[marketValuation]" => "150000",
        "jform[marketValue]" => "150000",
        "jform[sumInsured]" => "150000",
        "jform[carMakeYear]" => "2018",
        "jform[chassisNo]" => "PM18E380905C28947",
        "jform[engineNo]" => "F3091928",
        "jform[vehicleEngineCc]" => "1499",
        "jform[carMake]" => "15",
        "jform[selectModelMarketValue]" => "150000",
        "carModels[]" => "8442",
        "jform[vehicleRegNo]" => "PNN9551",
        "jform[carBodyType]" => "15",
        "jform[functionalMod]" => "false",
        "jform[carCondition]" => "u",
        "jform[currentInsurer]" => "",
        "jform[airbags]" => "1",
        "jform[performanceMod]" => "false",
        "jform[vehicleUse]" => "5",
        "jform[purchaseDate]" => "11-Mar-2020",
        "jform[purchasePrice]" => "17600",
        "jform[carIsLocal]" => "local",
        "jform[carFuel]" => "Petrol",
        "jform[vehType]" => "new",
        "jform[seatNumber]" => "5",
        "jform[abs]" => "yes",
        "jform[coverType]" => "C",
        "jform[garage]" => "1",
        "jform[antiTeft]" => "1",
        "jform[occupation]" => "228",
        "jform[drivingExp]" => "25",
        "jform[ehailingProtection]" => "yes",
        "jform[ncd]" => "7",
        "jform[currentRenew]" => "3600",
        "jform[currentSum]" => "0",
        "jform[claimPast]" => "no",
        "jform[Gender][0]" => "MALE",
        "jform[salutation][0]" => "MR",
        "jform[name][0]" => "Easvaren Arunasalam ",
        "jform[dateOfBirth][0]" => "28-Nov-1974",
        "jform[race][0]" => "Indian",
        "jform[maritalStatus][0]" => "Married",
        "jform[yearsOfDrivingExp][0]" => "25",
        "jform[driverOccupation][0]" => "228",
        "jform[occupationStatus][0]" => "1",
        "jform[occupationNature][0]" => "INDO",
        "jform[addressOne][0]" => "M2 5 1 Mayang Apartment",
        "jform[addressTwo][0]" => "Jalan Mayang Pasir Bayan Baru",
        "jform[postalCode][0]" => "11950",
        "jform[icNum][0]" => "741128095239",
        "jform[email][0]" => "easvar16@gmail.com",
        "jform[location]" => "2",
        "jform[carAddress]" => "Mahsuri Apartment ",
        "jform[logBook]" => "",
        "jform[taxExpiry]" => "11-Mar-2020",
        "jform[loan]" => "yes",
        "jform[loanCompany]" => "252",
        "jform[basisCoverage]" => "2",
        "jform[oldCarNo]" => "",
        "jform[cart]" => "0",
        "jform[specialPeril]" => "1",
        "jform[liabilityPass]" => "1",
        "jform[ehailing]" => "0",
        "jform[strike]" => "0",
        "jform[waiver]" => "0",
        "jform[laminated]" => "",
        "jform[tinting]" => "5000",
        "jform[step]" => "4",
        "jform[id]" => "0",
        "d1e38a8f5ca0469ef7124703d718a942" => "1",
        "jform[carModel]" => "4117",
    ],
    'expectedQuoteValue' => 5248.40,
    'expectedComparisonTable' => [
        new ComparisonTableData(
            ComparisonTableData::GROUP_BASIS_VALUATION[0],
            ComparisonTableData::GROUP_BASIS_VALUATION[1],
            'Agreed Value'
        ),
        new ComparisonTableData(
            ComparisonTableData::GROUP_EXTRA_COVERAGE_PREFIX . 'laminated',
            ComparisonTableData::GROUP_EXTRA_COVERAGE_MAP['laminated'],
            'MYR 5,000.00'
        ),
    ],
];
