<?php

return [
    'provider' => 'zurich',
    'payload' => [
        "jform[marketVal]" => "60000.0",
        "jform[marketValuation]" => "60000.0",
        "jform[marketValue]" => "60000.0",
        "jform[sumInsured]" => "75000.0",
        "jform[carMakeYear]" => "2018",
        "jform[chassisNo]" => "PPVZZZ60ZJL000879",
        "jform[engineNo]" => "CLS698993",
        "jform[vehicleEngineCc]" => "1598",
        "jform[carMake]" => "174",
        "carModels[]" => "10539",
        "jform[vehicleRegNo]" => "VCD1883",
        "jform[carBodyType]" => "15",
        "jform[functionalMod]" => "true",
        "jform[carCondition]" => "u",
        "jform[currentInsurer]" => "",
        "jform[airbags]" => "2",
        "jform[performanceMod]" => "true",
        "jform[vehicleUse]" => "5",
        "jform[purchaseDate]" => "12-Mar-2020",
        "jform[purchasePrice]" => "84000",
        "jform[carIsLocal]" => "imported",
        "jform[carFuel]" => "Petrol",
        "jform[vehType]" => "old",
        "jform[seatNumber]" => "5",
        "jform[abs]" => "yes",
        "jform[coverType]" => "C",
        "jform[garage]" => "1",
        "jform[antiTeft]" => "1",
        "jform[occupation]" => "317",
        "jform[drivingExp]" => "10",
        "jform[ehailingProtection]" => "no",
        "jform[ncd]" => "7",
        "jform[currentRenew]" => "0",
        "jform[currentSum]" => "76000",
        "jform[claimPast]" => "no",
        "jform[Gender][0]" => "MALE",
        "jform[salutation][0]" => "MR",
        "jform[name][0]" => "Wong Yik Foong",
        "jform[dateOfBirth][0]" => "27-Sep-1988",
        "jform[race][0]" => "Chinese",
        "jform[maritalStatus][0]" => "Married",
        "jform[yearsOfDrivingExp][0]" => "10",
        "jform[driverOccupation][0]" => "317",
        "jform[occupationStatus][0]" => "1",
        "jform[occupationNature][0]" => "INDO",
        "jform[addressOne][0]" => "Icon City",
        "jform[addressTwo][0]" => "",
        "jform[postalCode][0]" => "47300",
        "jform[icNum][0]" => "880927145191",
        "jform[email][0]" => "traviswong0927@gmail.com",
        "jform[name][1]" => "lim Jya Im",
        "jform[icNum][1]" => "900908025480",
        "jform[yearsOfDrivingExp][1]" => "8",
        "jform[location]" => "1",
        "jform[carAddress]" => "Icon City",
        "jform[logBook]" => "",
        "jform[taxExpiry]" => "12-Mar-2020",
        "jform[loan]" => "yes",
        "jform[loanCompany]" => "66",
        "jform[basisCoverage]" => "2",
        "jform[oldCarNo]" => "",
        "jform[cart]" => "0",
        "jform[specialPeril]" => "0",
        "jform[liabilityPass]" => "0",
        "jform[ehailing]" => "0",
        "jform[strike]" => "0",
        "jform[waiver]" => "0",
        "jform[laminated]" => "0",
        "jform[tinting]" => "0",
        "jform[step]" => "4",
        "jform[id]" => "0",
        "5b05242f1729a3bddaa2b5ba06050802" => "1",
        "jform[carModel]" => "8192",
    ],
    'expectedQuoteValue' => 2717.12,
];
