<?php

use Models\Eloquent\Provider;
use Concerns\InteractsWithUser;
use Concerns\InteractsWithJInput;
use Concerns\InteractsWithJConfig;
use Services\VehicleMakeClientSync;
use Services\VehicleModelClientSync;
use Illuminate\Database\Capsule\Manager;
use Services\LoanCompanyClientSync;
use Services\OccupationClientSync;
use Services\VehicleBodyTypeClientSync;

defined('_JEXEC') or die;

class InsureDIYMotorControllerSync extends JControllerForm {

    use InteractsWithJInput, InteractsWithJConfig, InteractsWithUser;

	public function isDry() {
		return $this->getInput('dryrun', 'false') === 'true';
    }
    
    public function getSelectedClient() {
        $providerCode = $this->getInput('provider', 'axa');

		/** @var Provider $provider */
        $provider = Provider::where('code', $providerCode)->first();

        return $provider->getClientService();
    }

	public function make() {
		if($this->isDry()) {
			Manager::beginTransaction();
        }
        
		$axaSync = new VehicleMakeClientSync($this->getSelectedClient());
		$axaSync->run();

		if($this->isDry()) {
			Manager::rollBack();
		}

		echo 'sucess' . PHP_EOL;
		JFactory::getApplication()->close(); // or jexit();
	}

	public function model() {
		if($this->isDry()) {
			Manager::beginTransaction();
		}

		try {
			$axaSync = new VehicleModelClientSync($this->getSelectedClient());
			$axaSync->run();

			echo 'sucess';
		}
		catch (\Exception $e) {
			var_dump($e);

			exit;
		}

		if($this->isDry()) {
			Manager::rollBack();
		}

		JFactory::getApplication()->close(); // or jexit();
	}

	public function occupation() {
		if($this->isDry()) {
			Manager::beginTransaction();
        }
        
		$axaSync = new OccupationClientSync($this->getSelectedClient());
		$axaSync->run();

		if($this->isDry()) {
			Manager::rollBack();
		}

		echo 'sucess' . PHP_EOL;
		JFactory::getApplication()->close(); // or jexit();
	}

	public function bodyType() {
		if($this->isDry()) {
			Manager::beginTransaction();
        }
        
		$axaSync = new VehicleBodyTypeClientSync($this->getSelectedClient());
		$axaSync->run();

		if($this->isDry()) {
			Manager::rollBack();
		}

		echo 'sucess' . PHP_EOL;
		JFactory::getApplication()->close(); // or jexit();
	}

	public function loanCompany() {
		if($this->isDry()) {
			Manager::beginTransaction();
        }
        
		$axaSync = new LoanCompanyClientSync($this->getSelectedClient());
		$axaSync->run();

		if($this->isDry()) {
			Manager::rollBack();
		}

		echo 'sucess' . PHP_EOL;
		JFactory::getApplication()->close(); // or jexit();
	}

	/**
	 * Syncronize all
	 *
	 * @return void
	 */
	public function all() {
		if($this->isDry()) {
			Manager::beginTransaction();
		}
		
		$syncs = [
			VehicleMakeClientSync::class,
			VehicleModelClientSync::class,
			VehicleBodyTypeClientSync::class,
			OccupationClientSync::class,
		];
		$providerFilter = $this->getInput('provider', null);
		$providers = Provider::when($providerFilter, function($query) use ($providerFilter) {
			$query->whereCode($providerFilter);
		})->get();

		foreach ($providers as $provider) {
			foreach ($syncs as $sync) {
				$syncInstance = new $sync($provider->getClientService());
				$syncInstance->run();
			}
		}

		if($this->isDry()) {
			Manager::rollBack();
		}

		echo 'sucess' . PHP_EOL;
		JFactory::getApplication()->close(); // or jexit();
	}

}
