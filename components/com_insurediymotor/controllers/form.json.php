<?php

use Carbon\Carbon;
use Models\Eloquent\Quote;
use Concerns\RespondJsonTrait;
use Concerns\InteractsWithUser;
use Concerns\QuoteSessionTrait;
use Models\Eloquent\QuoteDriver;
use Concerns\InteractsWithJInput;
use Services\Client\OptionalArray;
use Illuminate\Database\Capsule\Manager as DB;
use Services\BaseClient;

defined('_JEXEC') or die;

class InsureDIYMotorControllerForm extends JControllerForm {

	use InteractsWithJInput,
		InteractsWithUser,
		RespondJsonTrait,
        QuoteSessionTrait;
        
	public function quoteSave() {
        $app = JFactory::getApplication();
        $session = JFactory::getSession();

		$posts = new OptionalArray($this->getInputInstance()->get('jform', [], 'array'));
		
		$quoteSummary = $posts['quoteSummaryJson'] ? json_decode($posts['quoteSummaryJson'], true) : null;
		$quotePromos = json_decode($posts['quotePromosJson'], true);
		$quotePromosSum = collect($quotePromos)->map(function($promo) {
				return MyHelper::unformatCurrency($promo['value']);
			})
			->sum();

		$quoteValue = $quoteSummary ? MyHelper::unformatCurrency($quoteSummary['data']['total']) + $quotePromosSum : null;

		$quote = $this->currentQuote($session);

		$carModel = $quoteSummary && $quoteSummary['provider'] ?
			$quoteSummary['models'][$quoteSummary['provider']['id']] :
			null;

		$basisCoverage = $posts['basisCoverage'];
		if($posts['finalBasisCoverage']) {
			$posts['finalBasisCoverage'] == BaseClient::BASIS_COVERAGE_AGREED_VALUE ? '2' : '1';
		}

		$newQuoteData = collect([
			'car_reg_no' => $posts['vehicleRegNo'],
			'car_chassis_no' => $posts['chassisNo'],
			'car_engine_no' => $posts['engineNo'],
			'body_type_id' => $posts['carBodyType'],
			'model_id' => $carModel,
			'car_has_functional_mod' => $posts['functionalMod'],
			'car_condition' => $posts['carCondition'],
			'previous_provider_id' => $posts['currentInsurer'],
			'car_make_year' => $posts['carMakeYear'],
			'car_engine_capacity' => $posts['vehicleEngineCc'],
			'car_market_value' => $posts['marketVal'],
			'car_airbags' => $posts['airbags'],
			'car_has_performance_mod' => $posts['performanceMod'],
			'car_use' => $posts['vehicleUse'],
			'car_purchase_date' => $posts['purchaseDate'] ? Carbon::createFromFormat('d-M-Y', $posts['purchaseDate']) : null,
			'car_purchase_price' => $posts['purchasePrice'],
			'car_fuel' => $posts['carFuel'],
			'car_is_new' => $posts['vehType'] != 'old',
			'car_seats' => $posts['seatNumber'],
			'car_has_abs' => $posts['abs'] == 'yes',
			'cover_type' => $posts['coverType'],
			'car_garage' => $posts['garage'],
			'car_anti_theft' => $posts['antiTeft'],
			'occupation_id' => $posts['occupation'],
			'year_of_driving_experience' => $posts['drivingExp'],
			'car_has_e_hailing' => $posts['ehailingProtection'] == 'yes',
			'ncd_rate' => $posts['ncd'],
			'' => $posts['currentRenew'],
			'previous_sum_insured' => $posts['currentSum'],
			'has_past_claims' => $posts['claimPast'] == 'yes',
			'car_damage' => $posts['damageOwn'],
			'car_windscreen' => $posts['windscreen'],
			'car_theft' => $posts['theft'],
			'car_tp_claim' => $posts['tpClaim'],
			'car_location' => $posts['location'],
			'car_address' => $posts['carAddress'],
			'car_logbook' => $posts['logBook'],
			'car_tax_expiry' => $posts['taxExpiry'] ? Carbon::createFromFormat('d-M-Y', $posts['taxExpiry']) : null,
			'car_loan' => isset($posts['loan']) && $posts['loan'] == 'yes' ? 1 : 0,
			'car_loan_company' => $posts['loanCompany'],
			'car_loan_company_other' => $posts['loanCompanyOther'],
			'basis_coverage' => $basisCoverage,
			'sum_insured' => $posts['finalSumInsured'] ? $posts['finalSumInsured'] : $posts['sumInsured'],
			'ncd_protector' => isset($posts['ncdProtector']) ? $posts['ncdProtector'] == 'yes' : null,
			'car_old_registration_number' => $posts['oldCarNo'],
			'cart' => $posts['cart'],
			'specialPeril' => $posts['specialPeril'],
			'liabilityPass' => $posts['liabilityPass'],
			'ehailing' => $posts['ehailing'],
			'strike' => $posts['strike'],
			'waiver' => $posts['waiver'],
			'laminated' => $posts['laminated'],
			'tinting' => $posts['tinting'],
			'value' => $quoteValue,
			'user_id' => $this->getUserModel() ? $this->getUserModel()->id : null,
			'provider_id' => $quoteSummary ? $quoteSummary['provider']['id'] : null,
			'policy_start_date' => Carbon::today(),
			'step' => $posts['step'],
			'car_is_local' => $posts['carIsLocal'] == 'local',
		])->filter(function($data) {
			return ($data !== null && $data !== '') || $data === 0;
		})->toArray();

		DB::beginTransaction();
	
		try {

			if($quote) {
				$quote->update($newQuoteData);
			}
			else {
				$quote = new Quote($newQuoteData);
				$quote->save();

				$quote->updateUuid();
			}

			$driversCount = count($posts['name']);
			if($driversCount && $quote->drivers->count()) {
				$quote->drivers()->delete();
			}
			$driverDataFields = [
				'Gender' => 'gender',
				'salutation' => 'salutation',
				'name' => 'name',
				'dateOfBirth' => 'dob',
				'race' => 'race',
				'maritalStatus' => 'marital_status',
				'yearsOfDrivingExp' => 'exp',
				'driverOccupation' => 'occ',
				'occupationStatus' => 'occ_status',
				'occupationNature' => 'occ_nature',
				'addressOne' => 'address_one',
				'addressTwo' => 'address_two',
				'icNum' => 'nric',
				'email' => 'email',
				'postalCode' => 'postal_code',
			];
			for($i = 0; $i < $driversCount; $i++) {
				$newDriver = [
					'quote_id' => $quote->id,
				];

				foreach($driverDataFields as $field => $dbField) {
					if(!(isset($posts[$field]) && isset($posts[$field][$i]))) {
						continue;
					}

					$newDriver[$dbField] = $posts[$field][$i];

					if($dbField == 'dob') {
						$newDriver[$dbField] = $newDriver[$dbField] ? Carbon::createFromFormat('d-M-Y', $newDriver[$dbField]) : null;
					}
				}

				if(isset($newDriver['gender'])) {
					$newDriver['is_applicant'] = 1;
				}

				$newDriver = new QuoteDriver($newDriver);
				$newDriver->save();
			}

			/**
			 * Save quote details
			 */
			if($quote->details->count()) {
				$quote->details()->delete(); // reset
			}

			if($quoteSummary) {
				$quote->details()->create([
					'value' => MyHelper::unformatCurrency($quoteSummary['data']['total']),
					'description' => 'Quote Payable',
				]);
				foreach ($quotePromos as $promo) {
					$check = $quote->details()->whereCode($promo['code'])->first();
					if(!$check) {
						$quote->details()->create([
							'code' => $promo['code'],
							'value' => MyHelper::unformatCurrency($promo['value']),
							'description' => $promo['description'],
						]);
					}
				}
			}

			DB::commit();

		}
		catch (Exception $e) {
			DB::rollBack();

			print_r($e->getMessage());
			http_response_code(500);
			exit;

		}

		$this->currentQuote($session, $quote->id);

		$this->selectedQuote($session, $quoteSummary);

		$this->respondJson($quote);

    }
    
}
