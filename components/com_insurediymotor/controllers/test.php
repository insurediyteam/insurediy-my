<?php

use Services\Bugsnag;
use Services\AxaClient;
use Services\MsigClient;
use Models\Eloquent\Make;
use Services\ChubbClient;
use Services\QuoteSession;
use Services\ZurichClient;
use Models\Eloquent\Provider;
use Concerns\RespondJsonTrait;
use Concerns\InteractsWithUser;
use Concerns\QuoteSessionTrait;
use Concerns\InteractsWithJInput;
use Concerns\InteractsWithJConfig;
use Models\Eloquent\ModelProvider;
use Models\Eloquent\Quote;

defined('_JEXEC') or die;

class InsureDIYMotorControllerTest extends JControllerForm {

    use InteractsWithJInput, InteractsWithJConfig, InteractsWithUser, RespondJsonTrait;

    // public function __construct($config = [])
    // {
    //     parent::__construct($config);

    //     restore_error_handler();
    //     restore_exception_handler();
    // }

    public function display($cachable = false, $urlparams = array())
    {
        echo 'test';
    }

    public function chubbClient() {
        return new ChubbClient;
    }

    public function axaClient() {
        return new AxaClient;
    }
    
    public function getSelectedClient() {
        $providerCode = $this->getInput('provider', 'axa');

		/** @var Provider $provider */
        $provider = Provider::where('code', $providerCode)->first();

        return $provider->getClientService();
    }

    protected function generateInputs() {
        return [
            'vehicleRegNo' => 'PKV8098',
            'chassisNo' => 'PMHGM2660CD203734',
            'engineNo' => 'L15A74603698',
            'carBodyType' => 47,
            'carMake' => 47,
            'carModel' => 1,
            'functionalMod' => 'true',
            'carCondition' => 'u',
            'currentInsurer' => '',
            'carMakeYear' => '2013',
            'vehicleEngineCc' => '1500',
            'marketVal' => '38000.0',
            'airbags' => '1',
            'performanceMod' => 'true',
            'vehicleUse' => '5',
            'purchaseDate' => '01-Jan-2014',
            'purchasePrice' => '50000.0',
            'marketValuation' => '36000.0', // agreed value
            'carFuel' => 'Petrol',
            'vehType' => 'new',
            'seatNumber' => '4',
            'abs' => 'yes',
            'coverType' => 'C',
            'garage' => '1',
            'antiTeft' => '1',
            'occupation' => 7,
            'drivingExp' => '1',
            'ehailing' => 'yes', // todo
            'ncd' => '2',
            'currentRenew' => '',
            'currentSum' => '',
            'claimPast' => 'no',
            'damageOwn' => '0',
            'windscreen' => '0',
            'theft' => '0',
            'tpClaim' => '0',
            'Gender' => [
                'male',
            ],
            'salutation' => [
                '1',
            ],
            'name' => [
                'Firman Taruna Nugraha',
            ],
            'dateOfBirth' => [
                '12-08-1992',
            ],
            'race' => [
                '1',
            ],
            'maritalStatus' => [
                'Single',
            ],
            'yearsOfDrivingExp' => [
                '16',
            ],
            'driverOccupation' => [
                1,
            ],
            'occupationStatus' => [
                'INDO',
            ],
            'addressOne' => [
                'GERAI NO 1 LRG MASJID 34250',
            ],
            'addressTwo' => [
                'TANJONG PIANDANG',
            ],
            'icNum' => [
                '930906-14-7043',
            ],
            'email' => [
                'firmantr3@gmail.com',
            ],
            'location' => 1,
            'carAddress' => 'test',
            'logBook' => '123456',
            'taxExpiry' => '12-08-2020',
            'loan' => 'no',
            'loanCompany' => null,
            'marketValue' => '38000',
            'basisCoverage' => '1',
            'sumInsured' => '36000',
            'ncdProtector' => 'yes',
            'oldCarNo' => '',
            'cart' => '',
            'specialPeril' => '',
            'liabilityPass' => '',
            'ehailing' => '',
            'strike' => '',
            'waiver' => '',
            'laminated' => '',
            'tinting' => '',
        ];
    }

    protected function getProvider($code = 'axa') {
        return Provider::where('code', $this->getInput('provider', $code))->first();
    }

    public function quoteTransformer() {
        $provider = $this->getProvider($this->getInput('provider', 'axa'));
        $this->respondJson($provider->getClientService()->transformer->inputs($this->generateInputs()));
    }

    public function quote() {
        restore_error_handler();
        restore_exception_handler();
        
        $start = microtime(true);
        $provider = $this->getProvider($this->getInput('provider', 'axa'));
        $quoteData = $provider->getQuote($this->generateInputs());
        $this->respondJson(array_merge($quoteData, [
            'runtime' => (microtime(true) - $start),
        ]));
    }

    public function chubbQuote() {
        header('Content-Type: application/json');

        echo json_encode($this->chubbClient()->quotation());

        exit;
    }

    public function makeList() {
        var_dump($this->getSelectedClient()->vehicle->makeListData());

        exit;
    }

    public function modelList() {
        $selectedClient = $this->getSelectedClient();

        $make = Make::whereName($this->getInput('make', 'ALFA ROMEO'))->first();

        $makeProvider = $make->providers()
            ->whereProviderId($selectedClient->getModel()->id)
            ->first();

        if(!$makeProvider) {
            echo 'no data';jexit();
        }

        var_dump($selectedClient->vehicle->modelListData($makeProvider->code));

        exit;
    }

    public function occupationList() {
        var_dump($this->getSelectedClient()->occupation->listData());

        exit;
    }

    public function bodyList() {
        var_dump($this->getSelectedClient()->vehicle->bodyListData());

        exit;
    }

    public function loanCompanyList() {
        var_dump($this->getSelectedClient()->loanCompany->listData());

        exit;
    }

    public function dbModel() {
        $chubbModels = ModelProvider::
            whereHas('model', function($query) {
                $query->where('code', 'alfa-romeo-145');
            })
            ->whereHas('provider', function($query) {
                $query->where('code', 'chubb');
            })
            ->whereHas('detailables', function($query) {
                $query->where('description', 'Year Make')
                    ->where('value', '1995');
            })
            ->with(['model', 'detailables'])
            ->get();

        header('Content-Type: application/json');

        echo json_encode($chubbModels);exit;
    }

    public function bugsnag() {
        // Bugsnag::getInstance()->notifyException(new Exception('test'));
        // Bugsnag::getInstance()->notifyException(
        //     'scrapeError',
        //     json_encode([
        //         'class' => get_class($this),
        //         'message' => 'Can\'t get client data!',
        //         'data' => [
        //             'asd',
        //         ], 
        //     ])
        // );
        Bugsnag::test();
    }

    public function getTempQuote() {
        $this->respondJson(QuoteSession::getInstance()->toArray());
    }

    public function setTempQuote() {
        $quote = QuoteSession::getInstance();
        $quote->set($this->generateInputs());

        $this->respondJson(['status' => 'ok']);
    }

    public function flushTempQuote() {
        QuoteSession::getInstance()->flush();
    }

    /**
     * Search vehicle data
     *
     * @return array
     */
    public function vehicleSearch() {
        $client = $this->getSelectedClient();

        $results = $client->vehicle->modelListData('volkswagen');
        
        print_r($results);
        jexit();
    }

    public function clearQuoteSession() {
        $session = JFactory::getSession();

        $session->set(QuoteSessionTrait::sessionCurrentQuoteId(), null);
        $session->set(QuoteSessionTrait::sessionSelectedQuoteId(), null);

        jexit();
    }

    public function uuid() {
        echo \Ramsey\Uuid\Uuid::uuid4()->toString();
        jexit();
    }

    public function params() {
        $app = JFactory::getApplication();
        /** @var \Joomla\Registry\Registry $params */
        $params = $app->getParams();

        $this->respondJson($params);
    }

    public function mstDataWithKey() {
        $mstData = require(__DIR__ . '/testdata/mstData.php');

        /** @var \Services\ZurichClient\Generator $zurichGenerator */
        $zurichGenerator = \Models\Eloquent\Provider::whereCode('zurich')
            ->first()->getClientService()->generator;

        $this->respondJson($zurichGenerator->mstDataWithKeys($mstData));
        jexit();
    }

    public function schDataWithKey() {
        $mstData = require(__DIR__ . '/testdata/schData.php');

        /** @var \Services\ZurichClient\Generator $zurichGenerator */
        $zurichGenerator = \Models\Eloquent\Provider::whereCode('zurich')
            ->first()->getClientService()->generator;

        $this->respondJson($zurichGenerator->schDataWithKeys($mstData));
        jexit();
    }

    public function route() {
        echo MyHelper::jRoute('/test');
        exit;
    }

    public function queryFormData() {
        $data = 'ncdReplyNo=&oldCoverTypeCode=&headerEffDate=26%2F01%2F2020&encdVehClass=&encdPercent=0.0&isFoundInWNCD=false&encdCutOffDate=20110701&isFoundInWESI=false&isCovernoteFound=N&hasReadFromWESI=false&clntNRIC=811228-01-5284&clntOldIC=&effDt=20200126&hNcdReplyMsg=null&esiReplyMsg=&esiSucessfulRefNo=&ismMarketValue=0&recommendedSI=0&prevInputOfYrManf=null&prevIsReqISMValuation=&currIsReqISMValuation=&isESIClauseSelected=false&searchType=&selectedId=&ncdsent=&standard=true&isTheftProne=false&esiReply=&autoRated=true&occupationCode=5ACC&occupationDesc=ACCOUNTANT&gender=F&coverTypeCode=CO&vehicleClass=PC&CIFormNo=MX1&registrationNo=PKV8098&location=JHR&region=W&engineNo=L15A74603698&chassisNo=PMHGM2660CD203734&makeCode=11&modelCode=11++++03&prevModelCode=&prevModelDesc=&makeDesc=HONDA+CITY&modelDesc=CITY&tonnage=1497.00&capacityHidden=0&capacity=1497&capacityType=C&yearOfManufacture=2012&variantNvic=E+4D+SEDAN+5+SP+AUTOMATIC-HH012A&nvic=HH012A&numOfSeats=4&logBookNo=&color=&permittedDrivers=01&vehicleRegnClass=02&antiTheftDevices=12&safetyFeatures=06&garageLocation=31&importedType=01&purposeOfUse=01&purchaseDate=&purchasePrice=0.00&exInsurer=&exPolicyNo=&exVehicleRegNo=&previousInceptionDate=&previousExpiryDate=&newScreen=false&vehicleSumInsured=29000&oldSumInsured=29000.0&totalSumInsured=29000&motorBasicPremium=1136.85&loadingAmount=0&grossBasicPremium=1136.85&ncbPercent=0.000&ncbAmount=0.00&ncbVariation=true&ncdClaimsFreeYr=0&ncdAnniversaryDay=&ncdAnniversaryMth=&nettPremium=1136.85&hCoverID=E101&additionalCovers%5B0%5D.premium=0.0&hCoverID=E102&additionalCovers%5B1%5D.premium=0.0&hCoverID=GDI&additionalCovers%5B2%5D.premium=0.0&hCoverID=M003&additionalCovers%5B3%5D.premium=0.0&hCoverID=M011&additionalCovers%5B4%5D.premium=0.0&hCoverID=M012&additionalCovers%5B5%5D.premium=0.0&hCoverID=M013&additionalCovers%5B6%5D.premium=0.0&hCoverID=M018&additionalCovers%5B7%5D.premium=0.0&hCoverID=ND&additionalCovers%5B8%5D.premium=0.0&hCoverID=EC07&additionalCovers%5B9%5D.limitAmount=0&additionalCovers%5B9%5D.premium=0.0&hCoverID=EC14&additionalCovers%5B10%5D.limitAmount=0&additionalCovers%5B10%5D.premium=0.0&hCoverID=EC21&additionalCovers%5B11%5D.limitAmount=0&additionalCovers%5B11%5D.premium=0.0&hCoverID=M001&additionalCovers%5B12%5D.limitAmount=0.00&additionalCovers%5B12%5D.premium=0.0&hCoverID=E111&additionalCovers%5B13%5D.premium=0.0&hCoverID=E89&additionalCovers%5B14%5D.limitAmount=0.00&additionalCovers%5B14%5D.premium=0.0&hCoverID=E97&additionalCovers%5B15%5D.limitAmount=0.00&additionalCovers%5B15%5D.premium=0.0&hCoverID=E97A&additionalCovers%5B16%5D.limitAmount=0.00&additionalCovers%5B16%5D.premium=0.0&hCoverID=M010&additionalCovers%5B17%5D.limitAmount=0.00&additionalCovers%5B17%5D.premium=0.0&hCoverID=E105&additionalCovers%5B18%5D.premium=0.0&hCoverID=E22&additionalCovers%5B19%5D.premium=0.0&hCoverID=LLP&additionalCovers%5B20%5D.premium=0.0&hCoverID=E72&additionalCovers%5B21%5D.premium=0.0&hCoverID=E25&additionalCovers%5B22%5D.premium=0.0&hCoverID=E57A&additionalCovers%5B23%5D.premium=0.0&totalPremium=1136.85&postedPremium=1136.85&excessType=D&excessAmount=290&oldExcessAmount=0.0&interestedParties%5B0%5D.ipName=&interestedParties%5B0%5D.roleDesc=&interestedParties%5B0%5D.bankRefNo=&interestedParties%5B0%5D.ipId=&interestedParties%5B0%5D.roleId=';
        $data = urldecode($data);
        $data = collect(explode('&', $data))->map(function($item) {
            $tmp = explode('=', $item);
            return [
                'key' => $tmp[0],
                'value' => $tmp[1],
            ];

            return [
                $tmp[0] => $tmp[1],
            ];
        })->filter(function($item) {
            return $item['key'] == 'hCoverID';
        })
        ->values()
        ->toArray();
        // parse_str(urldecode($data), $result);

        // echo http_build_query($data);exit;

        echo var_export($data, TRUE);exit;

        $this->respondJson($data);exit;
    }

    public function cliMessage($message) {
        echo $message . PHP_EOL . PHP_EOL;
    }

	public function assertQuoteTests() {
        $testCases = scandir(__DIR__ . '/QuoteTests');

        $stats = [
            'success' => 0,
            'failed' => 0,
            'total' => 0,
            'rate' => null,
        ];

        $filter = $this->getInput('provider', null);
        $filterFile = $this->getInput('file', null);

        $counter = 0;

        foreach ($testCases as $key => $testCaseFile) {
            if(in_array($testCaseFile, ['.', '..'])) {
                continue;
            }

            $testCaseFilePath = __DIR__ . '/QuoteTests/' . $testCaseFile;
            $fileInfo = new SplFileInfo($testCaseFilePath);
            if($fileInfo->getExtension() != 'php') {
                continue;
            }

            $testCase = require($testCaseFilePath);

            if($filter && $filter != $testCase['provider']) {
                continue;
            }

            if($filterFile && $filterFile != $testCaseFile) {
                continue;
            }

            $counter++;

            echo ($counter) . ". Testing {$testCaseFile} ..." . PHP_EOL;

            $motorMyCli = realpath(__DIR__ . '/../../../cli/motor-my.php');
            $result = exec("php {$motorMyCli} --task=test.quoteTest --file={$testCaseFile}");
            $quote = null;
            if($result) {
                $quote = json_decode($result, true);
            }

            if($quote || $testCase['expectedQuoteValue'] === null) {
                $value = $quote ? \MyHelper::unformatCurrency($quote['total']) : null;
                $assert = $value == $testCase['expectedQuoteValue'];
                $assertDescription = $assert ? "OK!" : "Failed!";
                
                $this->cliMessage("[{$testCaseFile}] Quote Value: {$assertDescription} Expected `{$testCase['expectedQuoteValue']}`, getting `{$value}`.");

                if($assert) {
                    $stats['success']++;
                }
                else {
                    $stats['failed']++;
                }

                if(isset($testCase['expectedComparisonTable']) && count($testCase['expectedComparisonTable'])) {
                    if($quote) {
                        $actualCompareTables = collect($quote['comparisonTable'])->map(function($data) {
                                return $data['data'];
                            })
                            ->collapse();
                        foreach ($testCase['expectedComparisonTable'] as $key => $data) {
                            $search = $actualCompareTables->search((array) $data);

                            if($search === false) {
                                $this->cliMessage("[{$testCaseFile}] Compare Table: Failed! Expected compare table: `{$data->id}|{$data->text}`, but not found.");
                                $stats['failed']++;
                            }
                            else {
                                $this->cliMessage("[{$testCaseFile}] Compare Table: OK! Expected compare table: `{$data->id}|{$data->text}` found.");
                                $stats['success']++;
                            }
                        }
                    }
                    else {
                        $this->cliMessage("Expected some comparison table but failed to get quote!");
                        $stats['failed']++;
                    }
                }
            }
            else {
                echo "Failed to get quote, skipping." . PHP_EOL . PHP_EOL;
            }
        }

        $stats['rate'] = ($stats['success'] / ($stats['success'] + $stats['failed'])) * 100 . '%';

        print_r($stats);
        JFactory::getApplication()->close();
    }
    
    public function quoteTest() {
        $testCaseFile = $this->getInput('file');
        
        $testCaseFilePath = __DIR__ . '/QuoteTests/' . $testCaseFile;

        $testCase = require($testCaseFilePath);
        
        parse_str(http_build_query($testCase['payload']), $payload);
        $postData = (new JInput($payload))->get('jform', [], 'array');
        // if($counter == 2) {
        //     var_dump($postData);exit;
        // }
        
        /** @var Provider $provider */
        $provider = Provider::whereCode($testCase['provider'])->first();
        
        $quote = null;
        try {
            $quote = $provider->getQuote($postData);
        }
        catch (\Exception $e) {
            // ignore
            // var_dump($e);
            
        }

        echo $quote ? json_encode($quote) : null;
        JFactory::getApplication()->close();
    }

    public function msigCreateClient() {
        /** @var Provider $provider */
        $provider = Provider::whereCode('msig')->first();

        /** @var MsigClient $client */
        $client = $provider->getClientService();

        $client->authenticate();

        $response = $client->getHttpClient()->get('/MYPORTAL/PolicyMgmt/PolicyMgmtHome.jsp', [
            // 'debug' => true,
        ]);
        $response = $client->request(function(\GuzzleHttp\Client $httpClient) {
            return $httpClient->get('/MYPORTAL/PolicyMgmt/PolicyMgmtHome.jsp', [
                // 'debug' => true,
            ]);
        });

        $response = $client->request(function(\GuzzleHttp\Client $httpClient) {
            return $httpClient->get('/MYPORTAL/CreateProposal.action', [
                // 'debug' => true,
                'query' => [
                    'ID' => '1094',
                    'type' => '2',
                    'accountNo' => 'KLFIND01',
                ],
                'headers' => [
                    'Referer' => 'https://my-genlink.msig-asia.com/MYPORTAL/PolicyMgmt/PolicyMgmtHome.jsp',
                ],
            ]);
        });
        $response = $client->request(function(\GuzzleHttp\Client $httpClient) use ($client) {
            return $httpClient->post('/MYPORTAL/ClientMgmt/PersonalClientServ', [
                'form_params' => $client->generator->createClient(
                    json_decode('{"regno":"jtq8573","chassisNo":"PMHRW1830KD713145","engineNo":"L15BH7603111","makeCode":"11","makeDesc":"HONDA CR-V","modelCode":"11    05","capacity":"1498","tonnage":"1498.00","ncd":"55.00","yearOfManufacture":"2019","vehicleSumInsured":"138000","totalSumInsured":"138000","purchasePrice":"138000.00","numOfSeats":"5","location":"SEL","garageLocation":"31","antiTheftDevices":"12","safetyFeatures":"06","importedType":"01","postalCode":"47600","postal":"47600","fullName":"lc","address1":"adr1","address2":"adr2","salutation":"MR","region":"W","clientName":"lc","clntNRIC":"987654-01-1234","NRIC":"987654-01-1234","DOB":"24\/05\/2104","gender":"M","townDesc":"U.E.P., SUBANG JAYA, PJ","townCode":"BPJ"}', true)
                ),
            ]);
        });

        echo $response->getBody();
        JFactory::getApplication()->close();
    }

    public function config() {
        $params = JComponentHelper::getParams('com_insurediymotor');
        var_dump($this->getJoomlaConfig('fromname'));exit;
        echo \InsureDIYHelper::replaceVariables($params->get("cecformat"), [
            'anjay' => 'asd',
        ]);exit;
        echo $params->get('cecformat');exit;
    }

    public function mail() {
        $quote = Quote::latest()->first();
        $quote->sendEmail(\InsureDIYHelper::replaceVariables($quote->getComponentRegistry("cecformat"), $quote->toArray()));
        var_dump($quote);exit;
    }

}
