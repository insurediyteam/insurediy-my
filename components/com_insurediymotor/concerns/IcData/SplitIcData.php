<?php 

namespace Concerns\IcData;

use Exception;

class SplitIcData {

    public $fragments;

    public function __construct($number)
    {   
        if(strlen($number) != 12) {
            throw new Exception('Invalid IC Number!');
        }

        $this->fragments = [
            substr($number, 0, 6),
            substr($number, 6, 2),
            substr($number, 8, 4),
        ];
    }

    public function first() {
        return $this->fragments[0];
    }

    public function second() {
        return $this->fragments[1];
    }

    public function third() {
        return $this->fragments[2];
    }

}
