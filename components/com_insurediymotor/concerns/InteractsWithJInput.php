<?php 

namespace Concerns;

trait InteractsWithJInput {

    /**
     * Get config values
     *
     * @param null|string $index
     * @return mixed
     */
    protected function getInput($key, $default = null) {
        return $this->getInputInstance()->get($key, $default);
    }

    /** @return \JInput */
    protected function getInputInstance() {
        return \JFactory::getApplication()->input;
    }

}
