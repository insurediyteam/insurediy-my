<?php 

namespace Concerns;

use Exception;
use Models\Eloquent\Quote;

trait QuoteSessionTrait {
    
	public static function sessionCurrentQuoteId() {
		return 'my.motor.currentQuoteId';
	}

	public static function sessionSelectedQuoteId() {
		return 'my.motor.selectedQuote';
	}
    
	public function currentQuote($session, $set = null) {
		if ($set) {
			$session->set(static::sessionCurrentQuoteId(), $set);
			
			return true;
		}

		$currentQuoteId = $session->get(static::sessionCurrentQuoteId());
		if ($currentQuoteId) {
			return Quote::find($currentQuoteId);
		}

		return null;
	}
	
	public function selectedQuote($session, $set = null) {
		if ($set) {
			$session->set(static::sessionSelectedQuoteId(), json_encode($set));
			
			return true;
		}

		$current = $session->get(static::sessionSelectedQuoteId());

		$current = @json_decode($current, true) ? json_decode($current, true) : null;

		return $current;
	}
    
}
