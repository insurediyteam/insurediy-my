<?php 

namespace Concerns;

trait RespondSelect2Trait {
    
	protected function respondSelect2Data($query, $mapFunction, $searchField = 'name', $perPage = 20) {
		$paginated = $query->where($searchField, 'like', "{$this->getInput('q', '')}%")
			->paginate($perPage, ['*'], 'page', $this->getInput('page', 1));
		$results = collect($paginated->items())->map($mapFunction);

		return $this->respondJson([
			'results' => $results,
			'pagination' => [
				'more' => $paginated->currentPage() != $paginated->lastPage(),
			],
		]);
    }
    
}
