<?php

namespace Concerns;

use JFactory;
use Joomla\CMS\MVC\Controller\BaseController;

trait RespondJsonTrait {

	protected function expectsJson() {
		return isset($_SERVER['HTTP_ACCEPT']) && strpos($_SERVER['HTTP_ACCEPT'], 'application/json') === 0;
	}

	/**
	 * Send json data response
	 */
	public function respondJson($data, $statusCode = null) {
		header('Content-Type: application/json');
		http_response_code($statusCode);

		echo json_encode($data);

		JFactory::getApplication()->close();

		return;
	}

}
