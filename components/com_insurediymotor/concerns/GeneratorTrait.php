<?php 

namespace Concerns;

use Concerns\IcData;

trait GeneratorTrait {

    /**
     * Selected additional coverages
     *
     * @var array
     */
    public $selectedExtraCoverages = [];

    public $extraCoverages = [
        'cart' =>  350,
        'specialPeril' => 0,
        'liabilityPass' => 0,
        'ehailing' => 0,
        'strike' => 0,
        'waiver' => 0,
        'laminated' => 0,
        'tinting' => 0,
    ];

    /**
     * Add Extra Coverage
     *
     * @param string $name
     * @param int $sumInsured
     * @return void
     */
    public function addExtra($name, $sumInsured) {
        if(!in_array($name, $this->selectedExtraCoverages)) {
            array_push($this->selectedExtraCoverages, $name);
        }

        $this->extraCoverages[$name] = $sumInsured;

        $this->onAfterAddExtra($name);
    }

    /**
     * After add extra callback
     *
     * @param string $name
     * @return void
     */
    public function onAfterAddExtra($name) {

    }
    

    /**
     * Add extra cover
     *
     * @param integer $sumInsured
     * @return void
     */
    public function readExtrasFromInputs($inputs = []) {
        $extras = collect($inputs)->only(array_keys($this->extraCoverages))->toArray();

        foreach($extras as $key => $extra) {
            if($extra > 0) {
                $this->addExtra($key, $extra);
            }
        }
    }

    /**
     * Filter fillable data
     *
     * @param array $keys
     * @param array $data
     * @return array
     */
    public function filterDataByKeys($keys, $data) {
        return array_filter($data, function($key) use ($keys) {
            return in_array($key, $keys);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * Merge and filter data
     *
     * @param array $defaults
     * @param array $overrides
     * @return array
     */
    public function mergeAndOverride($defaults, $overrides = [], $updateSharedData = false) {
        $data = array_merge($defaults, $overrides);

        if($updateSharedData) {
            $this->updateSharedQuotationData($data);
        }

        return $this->filterDataByKeys(array_keys($defaults), $data);
    }

    public function icData($number) {
        return new IcData($number);
    }

}
