<?php 

namespace Concerns;

use Carbon\Carbon;
use Concerns\IcData\SplitIcData;

class IcData {

    /** @var string */
    public $number;

    /** @var string */
    public $numberDashed;

    /** @var Carbon */
    public $dob;

    /** @var SplitIcData */
    public $split;

    /** @var string */
    public $gender;

    public function __construct($number)
    {
        // strip dashes
        $number = str_replace('-', '', $number);
        
        $splitIC = new SplitIcData($number);

        // calculate year of birth from first 2 digit IC
        $currentYear = Carbon::now()->format('Y');
        $currentYearFirstTwoDigit = substr($currentYear, 0, 2);
        $currentYearLastTwoDigit = substr($currentYear, 2, 2);
        $icYearLastTwoDigit = substr($splitIC->first(), 2, 2);
        if($icYearLastTwoDigit < $currentYearLastTwoDigit) {
            $currentYearFirstTwoDigit--;
        }
        
        $dob = Carbon::createFromFormat('Ymd', $currentYearFirstTwoDigit . $splitIC->first());

        $this->number = $number;
        $this->numberDashed = "{$splitIC->first()}-{$splitIC->second()}-{$splitIC->third()}";
        $this->dob = $dob;
        $this->split = $splitIC;
        $this->gender = (substr($number, 0, -1) % 2) == 0 ? 'female' : 'male';
    }

}
