<?php 

namespace Concerns;

use Models\Eloquent\Base;
use Illuminate\Support\Str;
use Services\Client\OptionalArray;

trait TransformerTrait {

    /**
     * Convert array to OptionalArray object
     *
     * @param array $array
     * @return OptionalArray
     */
    public function optional($array) {
        return new OptionalArray($array);
    }

    /**
     * Remove fields that is equivalent to null
     *
     * @param array $array
     * @return array
     */
    public function filterValue($array) {
        if($array instanceof OptionalArray) {
            $array = $array->toArray();
        }
        
        return array_filter($array, function($value) {
            return $value !== null;
        });
    }

    /**
     * Get body type code by it's provider
     *
     * @param string $bodyTypeId
     * @return string|null
     */
    public function getCarBodyTypeCode($bodyTypeId, $strict = true) {
        return $this->getProviderChildCode('body_type', $bodyTypeId, $strict);
    }

    /**
     * Get make code by it's provider
     *
     * @param string $make
     * @return string|null
     */
    public function getCarMakeCode($makeId, $strict = true) {
        return $this->getProviderChildCode('make', $makeId, $strict);
    }

    /**
     * Get model code by it's provider
     *
     * @param string $model
     * @return string|null
     */
    public function getCarModelCode($modelId, $strict = true) {
        return $this->getProviderChildCode('model', $modelId, $strict);
    }

    /**
     * Get model code by it's provider
     *
     * @param string $model
     * @return string|null
     */
    public function getOccupationCode($modelId, $strict = true) {
        return $this->getProviderChildCode('occupation', $modelId, $strict);
    }

    /**
     * Get model code by it's provider
     *
     * @param string $model
     * @return string|null
     */
    public function getLoanCompanyCode($modelId, $strict = true) {
        return $this->getProviderChildCode('loan_company', $modelId, $strict);
    }

    /**
     * Get the code field value
     *
     * @param string $childTableName
     * @param mixed $parentId
     * @return string|null
     */
    public function getProviderChildCode($childTableName, $parentId, $strict = true) {
        return $this->getProviderChildAttribute($childTableName, $parentId, 'code', $strict);
    }

    /**
     * Get the attribute field value
     *
     * @param string $childTableName
     * @param mixed $parentId
     * @param string $attribute
     * @return string|null
     */
    public function getProviderChildAttribute($childTableName, $parentId, $attribute, $strict = true) {
        $model = $this->getProviderChild($childTableName, $parentId, $strict);

        return $model ? $model->{$attribute} : null;
    }

    /**
     * Get associated $bodyTypeId(make|model|occupation|body_type)_providers child table by it's parent
     *
     * @param string $childTableName
     * @param mixed $parentId
     * @return Base // MakeProvider|ModelProvider
     */
    public function getProviderChild($childTableName, $parentId, $strict = true) {
        $childTableNamePlural = Str::plural($childTableName);
        
        if ($parentId) {
            try {
                $providerChild = $this->getModel()
                    ->{$childTableNamePlural}()
                    ->where("{$childTableName}_id", $parentId)
                    ->first();
            }
            catch (\Exception $e) {
                // ignore
            }
    
            if ($providerChild) {
                return $providerChild;
            }

            if($strict) {
                throw new \Exception("Missing {$childTableNamePlural} where {$childTableName}_id: {$parentId}");
            }
        }

        return null;
    }
    
    /**
     * convert values from select options
     *
     * @param string $name
     * @param string $value
     * @return mixed
     */
    protected function select($name, $value) {
        return $this->optional(static::${$name})[$value];
    }

    protected function getDrivers($inputs) {
        $drivers = [];
        
        if($inputs['Gender']) {
            foreach ($inputs['icNum'] as $key => $value) {
                array_push($drivers, $this->optional([
                    'Gender' => $this->optional($inputs['Gender'])[$key],
                    'salutation' => $this->optional($inputs['salutation'])[$key],
                    'dateOfBirth' => $this->optional($inputs['dateOfBirth'])[$key],
                    'maritalStatus' => $this->optional($inputs['maritalStatus'])[$key],
                    'yearsOfDrivingExp' => $this->optional($inputs['yearsOfDrivingExp'])[$key],
                    'addressOne' => $this->optional($inputs['addressOne'])[$key],
                    'addressTwo' => $this->optional($inputs['addressTwo'])[$key],
                    'icNum' => $this->optional($inputs['icNum'])[$key],
                    'email' => $this->optional($inputs['email'])[$key],
                    'name' => $this->optional($inputs['name'])[$key],
                    'postalCode' => $this->optional($inputs['postalCode'])[$key],
                ]));
            }
        }

        return $drivers;
    }

    protected function getMainDriverIcNumber($inputs) {
        $mainDriver = $this->getMainDriver($inputs);

        return $mainDriver['icNum'];
    }

    protected function getMainDriver($inputs) {
        $drivers = $this->getDrivers($inputs);

        if(!count($drivers)) {
            return new OptionalArray([]);
        }

        return new OptionalArray($drivers[0]);
    }

}
