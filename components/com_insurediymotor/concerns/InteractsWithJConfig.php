<?php 

namespace Concerns;

trait InteractsWithJConfig {

    protected $componentName = 'motor_my';

    protected $jConfig;

    protected $componentRegistry;

    /**
     * Get config values
     *
     * @param null|string $index
     * @return mixed
     */
    protected function getConfig($index = null, $default = null) {
        $config = \JFactory::getConfig()
            ->toArray();

        if($index) {
            return isset($config[$index]) ? $config[$index] : $default;
        }

        return $config;
    }

    public function getJoomlaConfig($name, $default = null) {
        if(!isset($this->jConfig)) {
            $this->jConfig = \JFactory::getConfig();
        }

        return $this->jConfig->get($name, $default);
    }

    protected function getComponentConfig() {
        return $this->getConfig($this->componentName);
    }

    /** @return \Joomla\Registry\Registry */
    public function getComponentRegistry($path = null, $default = null) {
        if(!isset($this->componentRegistry)) {
            $this->componentRegistry = \JComponentHelper::getParams('com_insurediymotor');
        }

        if(!$path) {
            return $this->componentRegistry;
        }

        return $this->componentRegistry->get($path, $default);
    }

}
