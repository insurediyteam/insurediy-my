<?php 

namespace Concerns;

use Models\Eloquent\User;

trait InteractsWithUser {

    /** @return \Joomla\CMS\User\User */
    public function getUser() {
        return \JFactory::getUser();
    }

    public function getUserModel() {
        if ($this->getUser()->guest) {
            return null;
        }
        
        return User::find($this->getUser()->id);
    }

}
