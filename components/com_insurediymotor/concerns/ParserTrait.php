<?php 

namespace Concerns;

use MyHelper;
use Services\Client\BaseListData;
use Symfony\Component\DomCrawler\Crawler;

trait ParserTrait {
    

    /**
     * Get select options data from dom object
     *
     * @param \Symfony\Component\DomCrawler\Crawler $domObject
     * @param string $selectNameAttr
     * @return array
     */
    public function selectDataFromDom($domObject) {
        $data = [];

        if($domObject instanceof Crawler) {
            $options = $domObject->filter('option');
            $options->each(function($node, $i) use (&$data) {
                if($node instanceof Crawler) {
                    array_push($data, [
                        'value' => $node->attr('value'),
                        'text' => $node->text(),
                    ]);
                }
            });
        }
        
        return $data;
    }

    /**
     * Get BaseListData array from select dom
     *
     * @param \Symfony\Component\DomCrawler\Crawler $domObject
     * @return array
     */
    public function listDataFromDom($domObject) {
        return array_map(function($option) {
            return new BaseListData($option['value'], $option['text']);
        }, $this->selectDataFromDom($domObject));
    }

    /**
     * Removes unnecessary chars
     *
     * @param string $string
     * @return string
     */
    protected function trim($string) {
        return trim(html_entity_decode($string), " \t\n\r\0\x0B\xC2\xA0");
    }
    
    /**
     * parse table dom object to array
     *
     * @param Crawler $tableDom
     * @param string $rowSelector
     * @return array
     */
    public function tableDomToArray($tableDom, $rowSelector = 'tr') {
        $tableRows = $tableDom->filter($rowSelector);

        $rows = [];

        $tableRows->each(function(Crawler $node, $i) use (&$rows) {
            $columns = [];

            $node->filter('td')->each(function(Crawler $node, $i) use (&$columns) {
                array_push($columns, $this->trim($node->text()));
            });

            array_push($rows, $columns);
        });

        return $rows;
    }

    /**
     * Get amount of formatted currency
     *
     * @param string $money
     * @return float
     */
    public function getAmount($money)
    {
        return MyHelper::unformatCurrency($money);
    }
    
}
