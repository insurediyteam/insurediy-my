<?php 

namespace Models\Eloquent;

use Models\Eloquent\Concerns\HasDetailables;

class ModelProvider extends Base {

    use HasDetailables;

    protected $fillable = [
        'model_id',
        'provider_id',
        'code',
        'name',
        'hash',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function model() {
        return $this->belongsTo(Model::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function provider() {
        return $this->belongsTo(Provider::class);
    }

}
