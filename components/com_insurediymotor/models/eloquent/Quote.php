<?php 

namespace Models\Eloquent;

use Ramsey\Uuid\Uuid;
use Models\Eloquent\Concerns\HasUuid;
use Models\Eloquent\Concerns\HasDetailables;
use Models\Eloquent\Concerns\Searchable;
use MyHelper;

class Quote extends Base {

    use HasDetailables, HasUuid, Searchable;

    public static $conditions = [
        'u' => 'Registered',
        'n' => 'New Registration',
        'T' => 'Registered - Transfer Owner',
        '6' => 'Motor Trade License - Registered',
        '9' => 'Motor Trade License - New Registration',
        '7' => 'International Circulation Permit (ICP)',
        '8' => 'Foreign Commercial Vehicle License',
    ];

    public static $carUses = [
        '5' => 'Private (Drive Work Daily)',
        '1' => 'Business (Private Car)',
        '2' => 'PTE Use (Public Transport Work)',
    ];

    public static $coverTypes = [
        'C' => 'Comprehensive',
        // 'T' => 'Third Party', still not working
    ];

    public static $ncdRates = [
        '2' => '0%',
        '3' => '25%',
        '4' => '30%',
        '5' => '38.33%',
        '6' => '45%',
        '7' => '55%',
    ];

    public static $carLocations = [
        '1' => 'Selangor',
        '2' => 'Johor',
        '3' => 'Kedah',
        '4' => 'Kelantan', // east
        '5' => 'Labuan',
        '6' => 'Melaka',
        '7' => 'Negeri Sembilan',
        '8' => 'Pahang', // east
        '9' => 'Perak',
        '10' => 'Perlis',
        '11' => 'Pulau Pinang',
        '12' => 'Sabah',
        '13' => 'Sarawak',
        '14' => 'Terengganu', //east
    ];

    public static $carAirbags = [
        '1' => '1',
        '2' => '2',
        '3' => 'More than 2',
    ];

    public static $carGarages = [
        '1' => 'Locked Garage',
        '2' => 'Unlocked Garage',
    ];

    public static $carAntiThefts = [
        '1' => 'Alarm with Immobilizers',
        '2' => 'Factory Fitted Alarm',
        '3' => 'Alarm, Immobiliser, GPS etc',
        '4' => 'No Alarm',
        '5' => 'Others',
    ];

    public static $drivingExperiences = [
        '0' => 'Below 1 year',
        '1' => '1 year',
        '2' => '2 years',
        '3' => '3 years',
        '4' => '4 years',
        '5' => '5 years',
        '6' => '6 years',
        '7' => '7 years',
        '8' => '8 years',
        '9' => '9 years',
        '10' => '10 years',
        '11' => '11 years',
        '12' => '12 years',
        '13' => '13 years',
        '14' => '14 years',
        '15' => '15 years',
        '16' => '16 years',
        '17' => '17 years',
        '18' => '18 years',
        '19' => '19 years',
        '20' => '20 years',
        '21' => '21 years',
        '22' => '22 years',
        '23' => '23 years',
        '24' => '24 years',
        '25' => '25 years',
        '26' => '26 years',
        '27' => '27 years',
        '28' => '28 years',
        '29' => '29 years',
        '30' => '30 years',
        '31' => '31 years',
        '32' => '32 years',
        '34' => '34 years',
        '35' => '35 years',
        '36' => '36 years',
        '37' => '37 years',
        '38' => '38 years',
        '39' => '39 years',
        '40' => '40 years',
        '41' => 'More than 40 years',
    ];

    public static $damageOwns = [
        '0' => '0',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '>4',
    ];

    public static $damageWindscreens = [
        '0' => '0',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '>4',
    ];

    public static $damageThefts = [
        '0' => '0',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '>4',
    ];

    public static $damageTpClaims = [
        '0' => '0',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '>4',
    ];

    public static $basisCoverages = [
        // '1' => 'Market Value',
        '2' => 'Agreed Value',
    ];

    protected $fillable = [
        'user_id',
        'model_id',
        'provider_id',
        'previous_provider_id',
        'body_type_id',
        'occupation_id',
        'cover_type',
        'car_reg_no',
        'car_engine_no',
        'car_chassis_no',
        'car_airbags',
        'car_condition',
        'car_make_year',
        'car_engine_capacity',
        'car_market_value',
        'car_use',
        'car_purchase_date',
        'car_fuel',
        'car_is_new',
        'car_seats',
        'car_has_performance_mod',
        'car_has_functional_mod',
        'car_has_abs',
        'car_garage',
        'car_anti_theft',
        'car_has_e_hailing',
        'ncd_rate',
        'previous_sum_insured',
        'sum_insured',
        'has_past_claims',
        'car_purchase_price',
        'year_of_driving_experience',
        'car_damage',
        'car_windscreen',
        'car_theft',
        'car_tp_claim',
        'car_location',
        'car_address',
        'car_logbook',
        'car_tax_expiry',
        'car_loan',
        'car_loan_company',
        'car_loan_company_other',
        'car_log_document',
        'ncd_protector',
        'value',
        'policy_start_date',
        'cart',
        'specialPeril',
        'liabilityPass',
        'strike',
        'waiver',
        'laminated',
        'tinting',
        'ehailing',
        'step',
        'payment_request_id',
        'basis_coverage',
        'car_is_local',
        'payment_amount',
        'payment_txn_id',
        'payment_stage',
    ];

    protected $dates = [
        'deleted_at',
        'policy_start_date',
        'car_coe_expire_date',
        'car_purchase_date',
        'car_tax_expiry',
    ];

    public function generateUuid() {
        return md5(Uuid::uuid4()->toString());
    }

    public function getUuidKey()
    {
        return 'payment_request_id';
    }

    /**
     * Relations
     */

    /** @var \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /** @var \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function model() {
        return $this->belongsTo(Model::class);
    }

    /** @var \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function provider() {
        return $this->belongsTo(Provider::class);
    }

    /** @var \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function previous_provider() {
        return $this->belongsTo(Provider::class);
    }

    public function details() {
        return $this->hasMany(QuoteDetail::class);
    }

    public function drivers() {
        return $this->hasMany(QuoteDriver::class);
    }

    public function body_type() {
        return $this->belongsTo(BodyType::class);
    }

    public function occupation() {
        return $this->belongsTo(Occupation::class);
    }

    public function transaction() {
        return $this->belongsTo(Transaction::class, 'payment_request_id', 'unique_order_no');
    }

    public function loan_company() {
        return $this->belongsTo(LoanCompany::class, 'car_loan_company', 'id');
    }

    /**
     * query scopes
     */

    public function scopePending(\Illuminate\Database\Eloquent\Builder $query) {
        return $query->whereNull('payment_amount');
    }

    public function scopeCompleted(\Illuminate\Database\Eloquent\Builder $query) {
        return $query->where('step', 5)
            ->whereNotNull('payment_amount');
    }

    public function getCarConditionFmtAttribute() {
        return static::$conditions[$this->car_condition];
    }

    public function getCarUseFmtAttribute() {
        return static::$carUses[$this->car_use];
    }

    public function getCoverTypeFmtAttribute() {
        return static::$coverTypes[$this->cover_type];
    }

    public function getNcdRateFmtAttribute() {
        return isset(static::$ncdRates[$this->ncd_rate]) ? static::$ncdRates[$this->ncd_rate] : null;
    }

    public function getCarLocationFmtAttribute() {
        return static::$carLocations[$this->car_location];
    }

    public function getCarGarageFmtAttribute() {
        return static::$carGarages[$this->car_garage];
    }

    public function getCarAntiTheftFmtAttribute() {
        return static::$carAntiThefts[$this->car_anti_theft];
    }

    public function getDrivingExperienceFmtAttribute() {
        return static::$drivingExperiences[$this->year_of_driving_experience];
    }

    public function getDamageOwnFmtAttribute() {
        return static::$damageOwns[$this->car_damage];
    }

    public function getDamageTheftFmtAttribute() {
        return static::$damageThefts[$this->car_theft];
    }

    public function getDamageTpClaimFmtAttribute() {
        return static::$damageTpClaims[$this->car_tp_claim];
    }

    public function getBasisCoverageFmtAttribute() {
        return static::$basisCoverages[$this->basis_coverage];
    }

    public function getQuoteStageAttribute() {
        return ($this->step == "5" && $this->payment_amount) ? \JText::_("COM_INSUREDIYMOTOR_QUOTATION_STEP_COMPLETED") : "Step {$this->step}";
    }

    public function getCarLogDocumentUrlAttribute() {
        if(!$this->car_log_document) {
            return null;
        }

        return MyHelper::rootUrl() . '/' . $this->car_log_document;
    }

    /**
     * Alias for save
     *
     * @param array $options
     * @return bool
     */
    public function store($options = []) {
        return $this->save($options);
    }
    
	public function sendEmail($body, $mode = true, $attachments = null) {
		$mailer = \JFactory::getMailer();

        $mailer->ClearAttachments();
        $mailer->ClearAllRecipients();
        $mailer->sendMail(
            $this->getJoomlaConfig('mailfrom'),
            $this->getJoomlaConfig('fromname'),
            $this->user ? $this->user->email : $this->drivers()->first()->email,
            'InsureDIY - Malaysia Motor Insurance',
            $body,
            $mode,
            NULL,
            [
                $this->getComponentRegistry('sales_email'),
            ],
            $attachments
        );
    }
    
	public function getEmailBodies($params, $data) {
		$array = array();
		$array['cover_letter'] = \InsureDIYHelper::replaceVariables($params->get("clformat"), $data);
		$array['ce_customer'] = \InsureDIYHelper::replaceVariables($params->get("cecformat"), $data);
		$array['ce_service'] = \InsureDIYHelper::replaceVariables($params->get("cesformat"), $data);
		return $array;
	}

    public function finishPayment() {
        $this->createPolicyRecord();
        $this->createPoints();
        $this->sendEmail(\InsureDIYHelper::replaceVariables($this->getComponentRegistry("cecformat"), $this->toArray()));
    }

    protected function createPolicyRecord() {
		$db = \JFactory::getDbo();
		$obj = new \stdClass();
		$obj->user_id = $this->user_id;
		$obj->type = 7; // motor should be 6.
		$obj->insurer = $this->provider->name;
		$obj->currency = 344; // HK$
		$obj->status = 2; // Set as pending. 1 => active, 0 => inactive, 2 => pending
		$obj->quotation_id = $this->id;
		$db->insertObject("#__insure_policies", $obj);
    }

    protected function createPoints() {
        
    }

    public static function locationRegion($location) {
        if(in_array($location, [
            '4', '8', '14',
        ])) {
            return 'east';
        }

        return 'west';
    }

    public function getLoanCompanyFmtAttribute() {
        if($this->car_loan_company_other) {
            return $this->car_loan_company_other;
        }

        if($this->car_loan_company) {
            return $this->loan_company->name;
        }
    }

    public function checkPromoCodeUsed($code) {
        return $this->details()
            ->where('code', $code)
            ->first() !== null;
    }

}
