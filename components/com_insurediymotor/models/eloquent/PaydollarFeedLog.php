<?php 

namespace Models\Eloquent;

class PaydollarFeedLog extends BaseJoomlaModel {
    
    protected $table = '#__paydollar_feed_log';
    
    public function transaction() {
        return $this->hasOne(Transaction::class, 'feed_id', 'id');
    }

}
