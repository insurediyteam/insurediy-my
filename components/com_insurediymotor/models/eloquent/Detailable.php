<?php 

namespace Models\Eloquent;

class Detailable extends Base {

    protected $fillable = [
        'detailable_id',
        'detailable_type',
        'description',
        'value',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function detailable() {
        return $this->morphTo();
    }

    /**
     * Search by description value and value's value
     *
     * @param Builder $query
     * @param string $description
     * @param string $value
     * @return Builder
     */
    public function scopeDescriptionValue($query, $description, $value) {
        return $query->where([
            ['description', $description],
            ['value', $value],
        ]);
    }

}
