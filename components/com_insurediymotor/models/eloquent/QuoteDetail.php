<?php 

namespace Models\Eloquent;

class QuoteDetail extends Base {

    protected $fillable = [
        'quote_id',
        'code',
        'value',
        'description',
        'extra',
    ];

    public function quote() {
        return $this->belongsTo(Quote::class);
    }

}
