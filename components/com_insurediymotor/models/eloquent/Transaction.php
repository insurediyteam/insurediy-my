<?php 

namespace Models\Eloquent;

class Transaction extends BaseJoomlaModel {
    
    public function motor_quotes() {
        return $this->hasMany(Quote::class, 'payment_request_id', 'unique_order_no');
    }

    public function feed_log() {
        return $this->belongsTo(PaydollarFeedLog::class, 'feed_id', 'id');
    }

    /**
     * Table prefix for related component
     *
     * @return string
     */
    public function getPrefixName() {
        return "#__insure_";
    }

}
