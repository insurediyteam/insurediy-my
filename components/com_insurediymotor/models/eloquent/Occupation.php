<?php 

namespace Models\Eloquent;

class Occupation extends Base {

    protected $fillable = [
        'code',
        'name',
    ];

}
