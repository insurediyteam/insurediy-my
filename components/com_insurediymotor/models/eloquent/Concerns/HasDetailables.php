<?php 

namespace Models\Eloquent\Concerns;

use Models\Eloquent\Detailable;

trait HasDetailables {

    public function detailables() {
        /** @var \Illuminate\Database\Eloquent\Model $this */
        return $this->morphMany(Detailable::class, 'detailable');
    }

}
