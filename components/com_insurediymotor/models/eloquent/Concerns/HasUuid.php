<?php 

namespace Models\Eloquent\Concerns;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

trait HasUuid {

    public function getUuidKey() {
        return 'uuid';
    }

    public function generateUuid() {
        return Uuid::uuid4()->toString();
    }

    public function updateUuid() {
        if($this instanceof Model) {
            $this->update([
                $this->getUuidKey() => $this->generateUuid(),
            ]);
        }

        return $this;
    }

}
