<?php

namespace Models\Eloquent\Concerns;

use Models\Eloquent\Detailable;
use Illuminate\Database\Eloquent\Builder;
use JFactory;

trait Searchable
{

    /**
     * get query builder with search
     *
     * @param array $fields
     * @param string $searchInput
     * @param callable $hook
     * @return Builder
     */
    public static function queryWithSearch($fields, $searchInput = 'filter_search', $hook = null)
    {
        $app = JFactory::getApplication();
        $searchValue = $app->input->get($searchInput, null);

        if(!$searchValue) {
            return static::query();
        }

        return static::where(function($query) use ($fields, $searchValue, $hook) {
            $counter = 0;

            if($hook) {
                $hook($query);

                $counter++;
            }

            foreach ($fields as $key => $field) {
                $relationFieldDefinition = is_numeric($key) ? $field : $key;
                $isRelation = strpos($relationFieldDefinition, '.') !== false;

                if($isRelation) {
                    list($relation, $relationField) = explode('.', $relationFieldDefinition);
                    $relationFunction = $counter ? "orWhereHas" : 'whereHas';
                    $query->{$relationFunction}($relation, function($query) use ($relationField, $key, $searchValue, $field, $relationFieldDefinition) {
                        static::applyQuery(
                            $query,
                            $relationField,
                            $relationFieldDefinition == $key ? $field : $key,
                            'where',
                            $searchValue
                        );
                    });
                }
                else {
                    $function = $counter ? "orWhere" : 'where';
    
                    static::applyQuery($query, $key, $field, $function, $searchValue);
                }
                
                $counter++;
            }
        });
    }

    public static function applyQuery($query, $key, $field, $function, $searchValue) {
        if(is_numeric($key)) {
            $query->{$function}($field, $searchValue);
        }
        else {
            if($field == 'like') {
                $searchValue = "%{$searchValue}%";
            }
            $query->{$function}($key, $field, $searchValue);
        }
    }

}
