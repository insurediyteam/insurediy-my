<?php 

namespace Models\Eloquent;

use Models\Eloquent\Concerns\HasDetailables;

class MakeProvider extends Base {

    use HasDetailables;

    protected $fillable = [
        'make_id',
        'provider_id',
        'code',
        'name',
        'hash',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function make() {
        return $this->belongsTo(Make::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function provider() {
        return $this->belongsTo(Provider::class);
    }

}
