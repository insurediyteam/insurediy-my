<?php 

namespace Models\Eloquent;

class Make extends Base {
    
    protected $fillable = [
        'code',
        'name',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany */
    public function models() {
        return $this->hasMany(Model::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany */
    public function providers() {
        return $this->hasMany(MakeProvider::class);
    }

}
