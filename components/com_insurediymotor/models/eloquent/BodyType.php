<?php 

namespace Models\Eloquent;

class BodyType extends Base {

    protected $fillable = [
        'code',
        'name',
    ];

}
