<?php

namespace Models\Eloquent;

class TemporaryQuote extends Base
{

    protected $fillable = [
        'user_id',
        'data',
    ];

    protected $casts = [
        'data' => 'json',
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

}
