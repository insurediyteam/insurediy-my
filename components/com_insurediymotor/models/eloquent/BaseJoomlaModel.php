<?php

namespace Models\Eloquent;

use Concerns\InteractsWithJConfig;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

abstract class BaseJoomlaModel extends Model
{

    use InteractsWithJConfig;

    /**
     * Get the table associated with the model.
     *
     * @return string
     */
    public function getTable()
    {
        if (!isset($this->table)) {
            $this->table = $this->getPrefixName() .
            str_replace('\\', '', Str::snake(Str::plural(class_basename($this))));
        }

        $this->table = $this->parseTablePrefix($this->table);

        return $this->table;
    }

    /**
     * Parse prefixed table in joomla style
     *
     * @param string $table
     * @return string
     */
    protected function parseTablePrefix($table)
    {
        $dbPrefix = $this->getConfig('dbprefix');

        return str_replace('#__', $dbPrefix, $table);
    }

    /**
     * Table prefix for related component
     *
     * @return string
     */
    public function getPrefixName()
    {
        return "#__";
    }

}
