<?php 

namespace Models\Eloquent;

use Models\Eloquent\Concerns\HasDetailables;

class LoanCompanyProvider extends Base {

    use HasDetailables;

    protected $fillable = [
        'provider_id',
        'loan_company_id',
        'code',
        'name',
        'hash',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function loan_company() {
        return $this->belongsTo(LoanCompany::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function provider() {
        return $this->belongsTo(Provider::class);
    }

}
