<?php 

namespace Models\Eloquent;

use Concerns\InteractsWithUser;

class User extends BaseJoomlaModel {

    use InteractsWithUser;

    protected $casts = [
        'params' => 'json',
    ];

    protected $hidden = [
        'password',
    ];
    
    public static function getCurrent() {
        $user = $this->getUser();

        if($user->guest) {
            return null;
        }
        
        return static::find($user->id);
    }

    public function temporary_quotes() {
        return $this->hasMany(TemporaryQuote::class, 'user_id');
    }

    /**
     * Table prefix for related component
     *
     * @return string
     */
    public function getPrefixName() {
        return "#__";
    }

}
