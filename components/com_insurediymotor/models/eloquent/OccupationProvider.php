<?php 

namespace Models\Eloquent;

use Models\Eloquent\Concerns\HasDetailables;

class OccupationProvider extends Base {

    use HasDetailables;

    protected $fillable = [
        'provider_id',
        'occupation_id',
        'code',
        'name',
        'hash',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function occupation() {
        return $this->belongsTo(Occupation::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function provider() {
        return $this->belongsTo(Provider::class);
    }

}
