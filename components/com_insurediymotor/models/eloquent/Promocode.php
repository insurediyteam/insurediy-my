<?php

namespace Models\Eloquent;

use Carbon\Carbon;
use Concerns\InteractsWithUser;

class Promocode extends BaseJoomlaModel
{

    use InteractsWithUser;
    
    const PRODUCT_TRAVEL = 'travel';
    const PRODUCT_DOMESTIC = 'domestic';
    const PRODUCT_DOMESTICBETA = 'domesticbeta';
    const PRODUCT_LIFE = 'life';
    const PRODUCT_MOTOR = 'motor';

    protected $casts = [
        'used_by' => 'json',
    ];
    
    public function scopeAvailable($query, $productName) {
        $today = Carbon::today()->format('Y-m-d');
        return $query->where('applicable_products', 'like', "%{$productName}%")
            ->where([
                ['start_date', '>=', $today],
                ['end_date', '<=', $today],
            ])
            ->published()
            ->notTrashed();
    }

    /**
     * Table prefix for related component
     *
     * @return string
     */
    public function getPrefixName()
    {
        return "#__insure_";
    }

    public function scopePublished($query) {
        return $query->wherePublish(1);
    }

    public function scopeNotTrashed($query) {
        return $query->where('trash', 0);
    }

    public function scopeTrashed($query) {
        return $query->where('trash', 1);
    }

    public function quote_details() {
        return $this->hasMany(QuoteDetail::class, 'code', 'code');
    }

    public function updateUsedBy($userId, $promoType, $quoteId) {
        $this->used_by = collect($this->used_by)->push([
            'user_id' => $userId,
            'promo_type' => $promoType,
            'quotation_id' => $quoteId,
        ])->toArray();
        $this->save();
    }

}
