<?php 

namespace Models\Eloquent;

class QuoteDriver extends Base {

    public static $occupationStatuses = [
        '1' => 'Full Time Employment',
        '2' => 'Retired',
    ];

    public static $occupationNatures = [
        'INDO' => 'Indoor',
        'OUTD' => 'Outdoor',
    ];

    public static $salutations = [
        'MR' => 'Mr',
        'MRS' => 'Mrs',
        'MS' => 'Ms',
        'DR' => 'Dr',
        'DATO' => 'Dato',
        'DATUK' => 'Datuk',
        'DATIN' => 'Datin',
        'TANSRI' => 'Tan Sri',
        'PUANSRI' => 'Puan Sri',
        'COMPANY' => 'Company',
    ];

    public static $maritalStatuses = [
        'Married' => 'Married',
        'Single' => 'Single',
    ];

    public static $races = [
        'Chinese' => 'Chinese',
        'Foreigner' => 'Foreigner',
        'Indian' => 'Indian',
        'Malay' => 'Malay',
        'Native' => 'Native',
        'Others' => 'Others',
    ];
    
    protected $fillable = [
        'quote_id',
        'gender',
        'salutation',
        'name',
        'race',
        'email',
        'nric',
        'contact',
        'dob',
        'exp',
        'occ',
        'occ_status',
        'occ_nature',
        'marital_status',
        'main_relation',
        'demerit_point',
        'is_applicant',
        'claim_no',
        'claim_amount',
        'valid_license',
        'address_one',
        'address_two',
        'postal_code',
    ];

    protected $dates = [
        'dob',
    ];

    /** @var \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function quote() {
        return $this->belongsTo(Quote::class);
    }

    public function occupation() {
        return $this->belongsTo(Occupation::class, 'occ', 'id');
    }

}
