<?php

namespace Models\Eloquent;

use Illuminate\Database\Eloquent\SoftDeletes;

abstract class Base extends BaseJoomlaModel {

    use SoftDeletes;
    /**
     * Table prefix for related component
     *
     * @return string
     */
    public function getPrefixName() {
        return "#__insure_motor_my_";
    }

}
