<?php 

namespace Models\Eloquent;

class Provider extends Base {

    /** @var array */
    protected $fillable = [
        'code',
        'name',
        'cookie',
        'options',
        'lockdown',
        'short_description',
        'description',
    ];

    protected $hidden = [
        'password',
        'user',
    ];

    protected $casts = [
        'options' => 'json',
    ];

    /** @var \Services\BaseClient */
    protected $clientService;

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany */
    public function quotes() {
        return $this->hasMany(Quote::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany */
    public function makes() {
        return $this->hasMany(MakeProvider::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany */
    public function models() {
        return $this->hasMany(ModelProvider::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany */
    public function body_types() {
        return $this->hasMany(BodyTypeProvider::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany */
    public function occupations() {
        return $this->hasMany(OccupationProvider::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany */
    public function loan_companies() {
        return $this->hasMany(LoanCompanyProvider::class);
    }

    /** @return \Services\BaseClient|null */
    public function getClientService() {
        if (!isset($this->clientService)) {
            $this->clientService = $this->type ? new $this->type :null;
        }

        return $this->clientService;
    }

    /**
     * Get quote data from client service
     *
     * @param array $inputs
     * @return array|null
     */
    public function getQuote($inputs = []) {
        $clientService = $this->getClientService();

        $clientService->generator->readExtrasFromInputs($inputs);

        if($clientService) {
            return [
                'data' => $clientService->quotation($inputs),
                'total' => $clientService->getQuotationValue(),
                'sumInsured' => $clientService->getSumInsured(),
                'basisCoverage' => $clientService->getBasisCoverage(),
                'extraCoverage' => [
                    'cart' => $clientService->getExtraCoverageSumInsured('cart'),
                    'specialPeril' => $clientService->getExtraCoverageSumInsured('specialPeril'),
                    'liabilityPass' => $clientService->getExtraCoverageSumInsured('liabilityPass'),
                    'ehailing' => $clientService->getExtraCoverageSumInsured('ehailing'),
                    'strike' => $clientService->getExtraCoverageSumInsured('strike'),
                    'waiver' => $clientService->getExtraCoverageSumInsured('waiver'),
                    'laminated' => $clientService->getExtraCoverageSumInsured('laminated'),
                    'tinting' => $clientService->getExtraCoverageSumInsured('tinting'),
                ],
                'comparisonTable' => $clientService->getCompareTableData(),
            ];
        }

        return null;
    }
    
}
