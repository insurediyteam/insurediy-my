<?php 

namespace Models\Eloquent;

class LoanCompany extends Base {

    protected $fillable = [
        'code',
        'name',
    ];

}
