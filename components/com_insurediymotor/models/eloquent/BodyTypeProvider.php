<?php 

namespace Models\Eloquent;

use Models\Eloquent\Concerns\HasDetailables;

class BodyTypeProvider extends Base {

    use HasDetailables;

    protected $fillable = [
        'provider_id',
        'body_type_id',
        'code',
        'name',
        'hash',
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function body_type() {
        return $this->belongsTo(BodyType::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo */
    public function provider() {
        return $this->belongsTo(Provider::class);
    }

}
