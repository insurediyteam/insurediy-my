<?php
	/**
		* @package     Joomla.Site
		* @subpackage  com_insurediymotor
		*
		* @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
		* @license     GNU General Public License version 2 or later; see LICENSE.txt
	*/
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-form-content select');
	JHtml::_('script', 'system/loadingoverlay.min.js', false, true);
	JHtml::_('script', 'system/select2.min.js', false, true);
	JHtml::_('stylesheet', 'system/select2.css', false, true);

	/** @var InsureDIYMotorViewForm $this */
?>

	<div class="edit<?php echo $this->pageclass_sfx; ?> insurediy-motor-form-content center">
		<div class="delay">
		<div class="insurediy-thankyou-top-container">
			<img src="images/websiteicon/thankyou-icon.png" class="insurediy-thankyou-tick-icon"/>
		</div>
		<div class="insurediy-thankyou-middle-container">
			<span class="insurediy-thankyou-h1">
				Thank You!
			</span>
			<span class="insurediy-thankyou-h2">
				for choosing to purchase your <br>policy through InsureDIY Pte Ltd.
			</span>
		</div>
		</div>
		<div class="insurediy-thankyou-middle-container" style="margin-bottom:25px;">
			<span class="insurediy-thankyou-h5">
				<b>Payment Transaction ID: </b> <span style="color: #5197d5;"><?php echo $this->currentQuote ? $this->currentQuote->payment_txn_id : null; ?></span>
			</span>
		</div>

		<?php if ($this->currentQuote && !$this->currentQuote->car_log_document): ?>
		<form action="index.php?option=com_insurediymotor&task=form.uploadCarLogDocument" method="POST" enctype="multipart/form-data">
			<?php echo JHtml::_('form.token'); ?>
			<div class="input-wrapper" style="margin-top: 10px;">
				<span>Please upload your car log document.</span>
				<br>
				<span class="input-title" style="margin-right: 0;">Car Log Document</span>
				<div class="input-container-block">
					<input type="file" class="input-text front" name="car_log_document" accept=".pdf,.jpg,.png" style="
						border: 1px solid #C5D212;
						padding: 4px;
					" />
				</div>
				<br>
				<button class="motor-btn" type="submit">
					UPLOAD
				</button>
			</div>
		</form>
		<?php endif;?>

		<div class="insurediy-thankyou-bottom-container">
			<span class="insurediy-thankyou-h3">
				Your policy will be emailed to you shortly.
			</span>
			<br>
			<span class="insurediy-thankyou-h4">
				<?php echo JText::_("COM_INSUREDIYMOTOR_EXPLANATION_OUR_STAFFS_WILL_CONTACT"); ?>
			</span>
			
		</div>
	</div>
</div>


