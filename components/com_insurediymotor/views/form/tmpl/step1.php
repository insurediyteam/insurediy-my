<?php
	defined('_JEXEC') or die;
	
	JHtml::_('script', 'system/core.js', false, true);
	JHtml::_('behavior.keepalive');
	//JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-form-content select');
	JHtml::_('script', 'system/jquery.validate.v1.19.1.js', false, true);
	JHtml::_('script', 'system/loadingoverlay.min.js', false, true);
	JHtml::_('script', 'system/select2.min.js', false, true);
	JHtml::_('stylesheet', 'system/select2.css', false, true);
	JHtml::_('stylesheet', MyHelper::mix('/css/motor.css'));
	JHtml::_('stylesheet', "https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
	$providers = \Models\Eloquent\Provider::where('enabled', 1)->get();

	/** @var InsureDIYMotorViewForm $this */
?>
	
	<div class="insurediy-form montserat-font main-form" style="background-color:#fff;">
		<div class="motor-header-top-wrapper">
			<?php echo InsureDIYMotorHelper::renderHeader('icon-travel', JText::_('COM_INSUREDIYMOTOR_PAGE_HEADING'), 1); ?>
		</div>
		<div class="motor-quote-result-middle-wrapper hide">
			<div class="motor-result-table">
				<div class="motor-result-row d-flex flex-nowrap even first">
                    <div style="display: none;" class="motor-result-column motor-result-column-title motor-result-column-dummy">
                        &nbsp;
					</div>
				</div>
				
				<?php if($this->user->guest): ?>
				<div class="motor-quote-result-block-body my-transition my-transition-ease-in-out d-flex align-items-center justify-content-center">
					<a class="loginpopup" href="#">Login and use</a>
					<span class="mx-2">promo code: CAR20 for RM20 off!</span>
					<a class="regpopup" href="#">Register Now!</a>
				</div>
				<?php endif; ?>
				
				<h3 style="display: none;" class="comparison-table comparison-table-title mt-2">Cover Types &amp; Excess</h3>
				<div style="display: none;" class="comparison-table" id="coverTypeAndExcesses"></div>
				
				<h3 style="display: none;" class="comparison-table comparison-table-title mt-2">Extra Benefits</h3>
				<div style="display: none;" class="comparison-table" id="additionalBenefits"></div>
				
				<h3 style="display: none;" class="comparison-table comparison-table-title mt-2">Others</h3>
				<div style="display: none;" class="comparison-table" id="comparisonTableOthers"></div>
			</div>
			<div class="not-final hide">
				The premiums shown are estimated. To get your personal quote, please fill in your detail below.
			</div>
		</div>
		<div class="edit<?php echo $this->pageclass_sfx; ?> insurediy-motor-form-content">
			<form 
				action="<?php echo JRoute::_('index.php?option=com_insurediymotor&view=form'); ?>"
				data-restore="<?php echo (int) ($this->currentQuote !== null) ?>"
				method="post"
				name="adminForm"
				id="adminForm"
				class="form-vertical"
				enctype="multipart/form-data"
				data-currentquote="<?php echo $this->currentQuote ? base64_encode(json_encode($this->currentQuote->toArray())) : null; ?>"
				data-providers="<?php echo base64_encode(json_encode($providers->toArray())); ?>"
			>
				<div class="motor-form-input-wrapper no-login">
					<div class="motor-accordion select-model-step hidden-wrapper">SELECT CAR MODEL</div>
					<div class="panel select-model-step hidden-wrapper" style="max-height: 5000px!important;">
						<div class="row font-weight-bold f14">
							<div class="col-12">
								<ol>
									<li>Select your car model. Each insurer has different list, so simply select one that is closest</li>
									<li>Click on <span class="text-red">"Get Quote"</span> to see offers by our insurers</li>
								</ol>
							</div>
							<div class="col-12">
								<p class="car-model-text">My car model for each insurer is</p>
								<p class="text-red choose-all">*Please choose for all insurers</p>
							</div>
						</div>
						<div class="d-flex justify-content-center">
							<div class="p-3 text-center">
								<span class="input-title m-0">Car Make</span>
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
									<select name="jform[carMake]" class="hide-select custom-select" required>
										<?php 
											$carMakes = \Models\Eloquent\Make::select(['name', 'id'])->get()->toArray();
	
											foreach ($carMakes as $carMake) {
										?>
										<option <?php echo $this->selectedQuote && $this->selectedQuote['make'] && $this->selectedQuote['make'] == $carMake['id'] ? 'selected' : null; ?> value="<?php echo $carMake['id']; ?>"><?php echo $carMake['name']; ?></option>
										<?php 
											}
										?>
									</select>
									</div>
								</div>
							</div>
							<div class="p-3 text-center">
								<span class="input-title m-0">Input your Car's Sum Insured</span>
								<div class="input-container-block">
									<input id="select-model-market-value" type="number" placeholder="Input your Car's Sum Insured" class="input-text front" name="jform[selectModelMarketValue]" value="<?php echo $this->selectedQuote && isset($this->selectedQuote['marketValue']) ? $this->selectedQuote['marketValue'] : ''; ?>"/>
									<p class="text-red choose-all">*This value currently reflects the proposed Agreed Value for your car.</p>
								</div>
							</div>
						</div>

						<?php 
							$selectedModels = collect([]);
							if($this->selectedQuote && $this->selectedQuote['models']) {
								$selectedModels = \Models\Eloquent\Model::whereIn('id', array_values($this->selectedQuote['models']))->get()->keyBy('id');
							}
						?>
						<div id="motor-model-select-table" class="d-flex justify-content-center">
							<?php foreach($providers as $key => $provider): ?>
							<div class="p-2 insurer-model-select-wrapper round-select">
								<img class="img-fluid insurer-model-select-logo insurer-logo" alt="insurer logo" src="<?php echo MyHelper::route('/' . $provider->image_file); ?>">
								<div class="cc-input model-select">
									<div class="cc-pointer <?php echo ($key > 2) ? 'cc-pointer-left' : null; ?>" style="display: none;">
										<img src="<?php echo MyHelper::rootUrl(); ?>/images/com_insurediymotor/log-card.jpeg"  class="region" alt="log card example helper">
										<img src="<?php echo MyHelper::rootUrl(); ?>/images/region-mark.gif" class="region-mark" alt="region mark helper animation">
										<div class="dir"></div>
									</div>
									<select
										id="insurer-model-select-<?php echo $provider->id; ?>"
										data-id="<?php echo $provider->id; ?>"
										class="insurer-model-select mt-3"
										name="carModels[]"
									>
										<?php if($this->selectedQuote['models'][$provider->id]): ?>
											<?php
												$selectedModel = $selectedModels[$this->selectedQuote['models'][$provider->id]];
											?>
											<option selected value="<?php echo $selectedModel->id; ?>"><?php echo $selectedModel->name; ?></option>
										<?php endif; ?>
									</select>
								</div>
							</div>
							<?php endforeach; ?>
						</div>
						
						<div class="d-flex justify-content-center mt-5 mb-0">
							<div class="row">
								<div class="col-12">
									<button type="button" id="back-model-select-btn" class="motor-btn back-btn mr20"><span class="white-arrow-left"></span>BACK</button>
									<button type="button" id="check-veh" class="motor-btn validate">GET QUOTE<span class="white-arrow"></span></button>
								</div>
							</div>
					
						</div>
						<div class="row">
						<div class="col-12">
							<p class="text-red text-center mt-3">*Please note that quotes from some insurers may not be available.</p>
						</div>
						</div>
					</div>

					<div class="motor-accordion my-step1">CAR DETAILS</div>
					<div class="panel my-step1 car-details-1" style="max-height: 5000px!important;">
						<div class="panel-left">
							<div class="input-wrapper">
								<span class="input-title">Car Registration No</span>
								<div class="input-container-block">
									<input autocomplete="off" type="text" class="input-text front" name="jform[vehicleRegNo]" value="<?php echo $this->currentQuote ? $this->currentQuote['car_reg_no'] : null; ?>"/>
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title">Car Body Type</span>
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
										<select name="jform[carBodyType]" class="hide-select custom-select" style="width:300px;" required>
											<?php if($this->currentQuote): ?>
											<option selected value="<?php echo $this->currentQuote->body_type->id; ?>"><?php echo $this->currentQuote->body_type->name; ?></option>
											<?php else: ?>
											<option value="">- PLEASE SELECT -</option>
											<?php endif; ?>
										</select>
									</div>
								</div>
							</div>
							
							<div class="input-wrapper hidden-wrapper">
								<span class="input-title">Chassis No</span>
								<div class="input-container-block">
									<input type="text" class="input-text front" name="jform[chassisNo]" value="<?php echo $this->currentQuote ? $this->currentQuote->car_chassis_no : null; ?>" />
								</div>
							</div>
							<div class="input-wrapper hidden-wrapper">
								<span class="input-title">Engine No</span>
								<div class="input-container-block">
									<input type="text" class="input-text front" name="jform[engineNo]" value="<?php echo $this->currentQuote ? $this->currentQuote->car_engine_no : null; ?>" />
								</div>
							</div>
							
							<div class="input-wrapper hidden-wrapper">
								<span class="input-title">Agreed Value for Car's Sum Insured</span>
								<div class="input-container-block relative">
									<i class="rm">RM</i>
									<input type="text" class="input-text front with-rm" name="jform[marketVal]" value="<?php echo $this->currentQuote ? $this->currentQuote->car_market_value : null; ?>" disabled/>
								</div>
							</div>
							
							<div class="input-wrapper hidden-wrapper">
								<span class="input-title">Functional Modifications</span>
								<div class="input-container-inline">
									<input type="radio" id="radio1" data-type="radio" class="input-radio" name="jform[functionalMod]" value="true" <?php if($this->currentQuote && $this->currentQuote->car_has_functional_mod){ echo "checked"; }?> onclick="radioClicked(this);"/>
									<label class="radio-label" for="radio1">
										<span class="radio-label-title <?php if($this->currentQuote && $this->currentQuote->car_has_functional_mod){ echo "active"; }?>">Yes</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" id="radio2" data-type="radio" class="input-radio" name="jform[functionalMod]" value="false" <?php if($this->currentQuote && !$this->currentQuote->car_has_functional_mod){ echo "checked"; }?> onclick="radioClicked(this);" required/>
									<label class="radio-label" for="radio2">
										<span class="radio-label-title <?php if($this->currentQuote && !$this->currentQuote->car_has_functional_mod){ echo "active"; }?>">No</span>
									</label>
								</div>
							</div>

						</div>
						<div class="panel-right">
						<div class="input-wrapper">
								<span class="input-title">Car Condition</span>
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
									<select name="jform[carCondition]" class="hide-select custom-select" required>
										<?php foreach(\Models\Eloquent\Quote::$conditions as $key => $condition): ?>
											<?php if($this->currentQuote && $this->currentQuote->car_condition): ?>
											<option <?php echo $this->currentQuote->car_condition == $key ? 'selected' : null; ?> value="<?php echo $key; ?>"><?php echo $condition; ?></option>
											<?php else: ?>
											<option <?php echo $key == 'u' ? 'selected' : null; ?> value="<?php echo $key; ?>"><?php echo $condition; ?></option>
											<?php endif; ?>
										<?php endforeach; ?>
									</select>
									</div>
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title">Current Insurer</span>
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
										<select name="jform[currentInsurer]" onchange="" onclick="return false;"  class="hide-select custom-select">
											<option value="" selected>Select Current Insurer</option>
											<?php
												$providers = \Models\Eloquent\Provider::get();
												foreach ($providers as $provider) {
											?>
											<option 
												<?php echo $this->currentQuote && $this->currentQuote->previous_provider_id == $provider->id ? 'selected' : null; ?>
												value="<?php echo $provider->id; ?>"
											>
												<?php echo $provider->name; ?>
											</option>
											<?php 
												}

												unset($providers);
											?>
										</select>
									</div>
								</div>
							</div>
							
							<div class="input-wrapper hidden-wrapper">
								<span class="input-title">Car Year</span>
								<div class="input-container-block">
									<input type="text" class="input-text front" name="jform[carMakeYear]" value="<?php echo $this->currentQuote ? $this->currentQuote->car_make_year : null; ?>" />
								</div>
							</div>
							<div class="input-wrapper hidden-wrapper">
								<span class="input-title">Engine CC</span>
								<div class="input-container-block">
									<input type="text" class="input-text front" name="jform[vehicleEngineCc]" value="<?php echo $this->currentQuote ? $this->currentQuote->car_engine_capacity : null; ?>" />
								</div>
							</div>

							<div class="input-wrapper hidden-wrapper">
								<span class="input-title">Airbags</span>
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
										<select name="jform[airbags]" onchange="" onclick="return false;"  class="hide-select custom-select">
											<option value="" <?php echo $this->currentQuote && $this->currentQuote->car_airbags ? null : 'selected'; ?>>Select Airbags</option>

											<?php foreach(\Models\Eloquent\Quote::$carAirbags as $key => $value): ?>
											<option value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->car_airbags == $key ? 'selected' : null; ?>><?php echo $value; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
							</div>

							<div class="input-wrapper hidden-wrapper">
								<span class="input-title">Performance and Aesthetic Modifications</span>
								<div class="input-container-inline">
									<input type="radio" id="per1" class="input-radio" data-type="radio" name="jform[performanceMod]" value="true" <?php if($this->currentQuote && $this->currentQuote->car_has_performance_mod){ echo "checked"; }?> onclick="radioClicked(this);"/>
									<label class="radio-label" for="per1">
										<span class="radio-label-title <?php if($this->currentQuote && $this->currentQuote->car_has_performance_mod){ echo "active"; }?>">Yes</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" id="per2" class="input-radio" data-type="radio" name="jform[performanceMod]" value="false" <?php if($this->currentQuote && !$this->currentQuote->car_has_performance_mod){ echo "checked"; }?> onclick="radioClicked(this);" required/>
									<label class="radio-label" for="per2">
										<span class="radio-label-title <?php if($this->currentQuote && !$this->currentQuote->car_has_performance_mod){ echo "active"; }?>">No</span>
									</label>
								</div>
							</div>
						</div>
						<div style="width:100%;height:50px;"></div>
						<button type="button" id="continue-to-select-model" class="motor-btn validate">CONTINUE<span class="white-arrow"></span></button>
					</div>
					<div class="motor-accordion car-details-2-accordion my-step2">CAR DETAILS</div>
					<div class="panel my-step2 step-car-details-2">
						<div class="panel-left">
							<div class="input-wrapper">
								<span class="input-title">Use of Vehicle</span>
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
										<select name="jform[vehicleUse]" onchange="" onclick="return false;"  class="hide-select custom-select" required>
											<?php foreach (\Models\Eloquent\Quote::$carUses as $key => $value): ?>
											<option value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->car_use == $key ? 'selected' : null; ?>><?php echo $value; ?></option>
											<?php endforeach;?>
										</select>
									</div>
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title">Vehicle Purchase Date</span>
								<div class="input-container-block">
									<?php echo $this->form->getInput('purchaseDate', null, ($this->currentQuote && $this->currentQuote->car_purchase_date ? $this->currentQuote->car_purchase_date->format('d-M-Y') : null)); ?>
									<?php if(isset($data["purchaseDate"])) { 
										echo '<script>
													jQuery(function(){
														jQuery( "#jform_purchaseDate" ).datepicker({
															changeMonth: true,
															changeYear: true,
															showOn: "focus",
															minDate: 0,
															dateFormat : "dd-M-yy",
															yearRange: "1920:2050",
															onChangeMonthYear:function(y, m, i){                                
														        var d = i.selectedDay;
														        jQuery(this).datepicker("setDate", new Date(y, m-1, d));
														    }
														});
	 													jQuery( "#jform_purchaseDate" ).datepicker("setDate", "' . $data["purchaseDate"] . '");
													});
											</script>';
									} 
									?>
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title">Car Purchase Price</span>
								<div class="input-container-block relative">
									<i class="rm">RM</i>
									<input type="number" class="input-text front with-rm" name="jform[purchasePrice]" value="<?php echo $this->currentQuote ? $this->currentQuote->car_purchase_price : null; ?>"/>
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title">Vehicle Market Valuation</span>
								<div class="input-container-block relative">
									<i class="rm">RM</i>
									<input type="number" class="input-text front with-rm" name="jform[marketValuation]" value="<?php echo $this->currentQuote ? $this->currentQuote->car_market_value : null; ?>"/>
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title">Local or Imported Vehicle</span>
								<div class="input-container-inline">
									<input type="radio" <?php echo $this->currentQuote && $this->currentQuote->car_is_local ? 'checked' : null; ?> id="ncdp1" class="input-radio" name="jform[carIsLocal]" value="local" onclick="radioClicked(this);"/>
									<label class="radio-label" for="ncdp1">
										<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->car_is_local ? 'active' : null; ?>">Local</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" <?php echo $this->currentQuote && !$this->currentQuote->car_is_local ? 'checked' : null; ?> id="ncdp2" class="input-radio" name="jform[carIsLocal]" value="imported" onclick="radioClicked(this);" required/>
									<label class="radio-label" for="ncdp2">
										<span class="radio-label-title <?php echo $this->currentQuote && !$this->currentQuote->car_is_local ? 'active' : null; ?>">Imported</span>
									</label>
								</div>
							</div>
						</div>
						<div class="panel-right">
							<div class="input-wrapper">
								<span class="input-title">Car Fuel Type</span>
								<div class="input-container-inline">
									<input type="radio" data-type="radio" id="fuel1" class="input-radio" name="jform[carFuel]" value="Petrol" <?php if($this->currentQuote && $this->currentQuote->car_fuel == 'Petrol'){ echo "checked"; }?> onclick="radioClicked(this);"/>
									<label class="radio-label" for="fuel1">
										<span class="radio-label-title <?php if($this->currentQuote && $this->currentQuote->car_fuel == 'Petrol'){ echo "active"; }?>">Petrol</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" data-type="radio" id="fuel2" class="input-radio" name="jform[carFuel]" value="Diesel" <?php if($this->currentQuote && $this->currentQuote->car_fuel == 'Diesel'){ echo "checked"; }?> onclick="radioClicked(this);" required/>
									<label class="radio-label" for="fuel2">
										<span class="radio-label-title <?php if($this->currentQuote && $this->currentQuote->car_fuel == 'Diesel'){ echo "active"; }?>">Diesel</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" data-type="radio" id="fuel3" class="input-radio" name="jform[carFuel]" value="Gas" <?php if($this->currentQuote && $this->currentQuote->car_fuel == 'Gas'){ echo "checked"; }?> onclick="radioClicked(this);" required/>
									<label class="radio-label" for="fuel3">
										<span class="radio-label-title <?php if($this->currentQuote && $this->currentQuote->car_fuel == 'Gas'){ echo "active"; }?>">Gas</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" data-type="radio" id="fuel4" class="input-radio" name="jform[carFuel]" value="Watt" <?php if($this->currentQuote && $this->currentQuote->car_fuel == 'Watt'){ echo "checked"; }?> onclick="radioClicked(this);" required/>
									<label class="radio-label" for="fuel4">
										<span class="radio-label-title <?php if($this->currentQuote && $this->currentQuote->car_fuel == 'Watt'){ echo "active"; }?>">Watt</span>
									</label>
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title">Vehicle Type <img class="hasTooltip" alt="help-quote" src="<?php echo MyHelper::route('images/help-quote.png') ?>" title="" data-original-title="Old Vehicle: more than 10 years old, New Vehicle: less than 10 years old"></span>
								<div class="input-container-inline">
									<input type="radio" data-type="radio" id="vehType1" class="input-radio" name="jform[vehType]" value="old" <?php if($this->currentQuote && !$this->currentQuote->car_is_new){ echo "checked"; }?> onclick="radioClicked(this);"/>
									<label class="radio-label" for="vehType1">
										<span class="radio-label-title <?php if($this->currentQuote && !$this->currentQuote->car_is_new){ echo "active"; }?>">Old Vehicle</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" data-type="radio" id="vehType2" class="input-radio" name="jform[vehType]" value="new" <?php if($this->currentQuote && $this->currentQuote->car_is_new){ echo "checked"; }?> onclick="radioClicked(this);" required/>
									<label class="radio-label" for="vehType2">
										<span class="radio-label-title <?php if($this->currentQuote && $this->currentQuote->car_is_new){ echo "active"; }?>">New Vehicle</span>
									</label>
								</div>
							</div>
							<div class="input-wrapper seat">
								<span class="input-title">Number of Seat</span>
								<div class="input-container-inline">
									<input type="radio" data-type="radio" id="seat1" class="input-radio" name="jform[seatNumber]" value="2" <?php if($this->currentQuote && $this->currentQuote->car_seats == 2){ echo "checked"; }?> onclick="radioClicked(this);"/>
									<label class="radio-label" for="seat1">
										<span class="radio-label-title <?php if($this->currentQuote && $this->currentQuote->car_seats == 2){ echo "active"; }?>">2</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" data-type="radio" id="seat3" class="input-radio" name="jform[seatNumber]" value="3" <?php if($this->currentQuote && $this->currentQuote->car_seats == 3){ echo "checked"; }?> onclick="radioClicked(this);"/>
									<label class="radio-label" for="seat3">
										<span class="radio-label-title <?php if($this->currentQuote && $this->currentQuote->car_seats == 3){ echo "active"; }?>">3</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" data-type="radio" id="seat4" class="input-radio" name="jform[seatNumber]" value="4" <?php if($this->currentQuote && $this->currentQuote->car_seats == 4){ echo "checked"; }?> onclick="radioClicked(this);"/>
									<label class="radio-label" for="seat4">
										<span class="radio-label-title <?php if($this->currentQuote && $this->currentQuote->car_seats == 4){ echo "active"; }?>">4</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" data-type="radio" id="seat5" class="input-radio" name="jform[seatNumber]" value="5" <?php if($this->currentQuote && $this->currentQuote->car_seats == 5){ echo "checked"; }?> onclick="radioClicked(this);"/>
									<label class="radio-label" for="seat5">
										<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->car_seats == 5 ? "active" : null; ?>">5</span>
									</label>
								</div>
								<div class="clear"></div>
								<div class="input-container-inline">
									<input type="radio" id="seat6" class="input-radio" name="jform[seatNumber]" value="6" <?php if($this->currentQuote && $this->currentQuote->car_seats == 6){ echo "checked"; }?> onclick="radioClicked(this);"/>
									<label class="radio-label mb-0" for="seat6">
										<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->car_seats == 6 ? "active" : null; ?>">6</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" id="seat7" class="input-radio" name="jform[seatNumber]" value="7" <?php if($this->currentQuote && $this->currentQuote->car_seats == 7){ echo "checked"; }?> onclick="radioClicked(this);"/>
									<label class="radio-label mb-0" for="seat7">
										<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->car_seats == 7 ? "active" : null; ?>">7</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" id="seat8" class="input-radio" name="jform[seatNumber]" value="8" <?php if($this->currentQuote && $this->currentQuote->car_seats == 8){ echo "checked"; }?> onclick="radioClicked(this);"/>
									<label class="radio-label mb-0" for="seat8">
										<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->car_seats == 8 ? "active" : null; ?>">8</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" id="seat9" class="input-radio" name="jform[seatNumber]" value="9" <?php if($this->currentQuote && $this->currentQuote->car_seats == 9){ echo "checked"; }?> onclick="radioClicked(this);"/>
									<label class="radio-label mb-0" for="seat9">
										<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->car_seats == 9 ? "active" : null; ?>">9</span>
									</label>
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title">Anti-Lock Braking System (ABS)</span>
								<div class="input-container-inline">
									<input type="radio" id="abs1" class="input-radio" name="jform[abs]" value="yes" <?php if($this->currentQuote && $this->currentQuote->car_has_abs){ echo "checked"; }?> onclick="radioClicked(this);"/>
									<label class="radio-label" for="abs1">
										<span class="radio-label-title <?php if($this->currentQuote && $this->currentQuote->car_has_abs){ echo "active"; }?>">Yes</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" id="abs2" class="input-radio" name="jform[abs]" value="no" <?php if($this->currentQuote && !$this->currentQuote->car_has_abs){ echo "checked"; }?> onclick="radioClicked(this);" required/>
									<label class="radio-label" for="abs2">
										<span class="radio-label-title <?php if($this->currentQuote && !$this->currentQuote->car_has_abs){ echo "active"; }?>">No</span>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="motor-accordion my-step2">OTHER DETAILS</div>
					<div class="panel my-step2">
						<div class="panel-left">
							<div class="input-wrapper">
								<span class="input-title">Cover Type</span>
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
										<select name="jform[coverType]" onchange="" onclick="return false;"  class="hide-select custom-select" required>
											<?php foreach (\Models\Eloquent\Quote::$coverTypes as $key => $value): ?>
											<option value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->cover_type == $key ? 'selected' : null; ?>><?php echo $value; ?></option>
											<?php endforeach;?>
										</select>
									</div>
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title">Garaged</span>
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
										<select name="jform[garage]" onchange="" onclick="return false;"  class="hide-select custom-select" required>
											<?php foreach (\Models\Eloquent\Quote::$carGarages as $key => $value): ?>
												<option value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->car_garage == $key ? 'selected' : null; ?>><?php echo $value; ?></option>
											<?php endforeach;?>
										</select>
									</div>
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title">Anti-Theft</span>
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
										<select name="jform[antiTeft]" onchange="" onclick="return false;" class="hide-select custom-select" required>
											<?php foreach (\Models\Eloquent\Quote::$carAntiThefts as $key => $value): ?>
												<option value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->car_anti_theft == $key ? 'selected' : null; ?>><?php echo $value; ?></option>
											<?php endforeach;?>
										</select>
									</div>
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title">Main Driver's Occupation</span>
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
										<select name="jform[occupation]" onchange="" onclick="return false;" class="hide-select custom-select occupation-select" style="width:300px;" required>
											<?php if($this->currentQuote && $this->currentQuote->occupation): ?>
											<option value="<?php echo $this->currentQuote->occupation->id; ?>"><?php echo $this->currentQuote->occupation->name; ?></option>
											<?php else: ?>
											<option value="">- PLEASE SELECT -</option>
											<?php endif; ?>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="panel-right">
							<div class="input-wrapper">
								<span class="input-title">Main Driver's Years of Driving Experience</span>
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
										<select name="jform[drivingExp]" onchange="" onclick="return false;"  class="hide-select custom-select" required>
											<?php foreach (\Models\Eloquent\Quote::$drivingExperiences as $key => $value): ?>
												<option value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->year_of_driving_experience == $key ? 'selected' : null; ?>><?php echo $value; ?></option>
											<?php endforeach;?>
										</select>
									</div>
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title">E-Hailing Protection</span>
								<div class="input-container-inline">
									<input type="radio" id="ehailingProtection1" class="input-radio" name="jform[ehailingProtection]" value="yes" <?php if($this->currentQuote && $this->currentQuote->car_has_e_hailing){ echo "checked"; }?> onclick="radioClicked(this);"/>
									<label class="radio-label" for="ehailingProtection1">
										<span class="radio-label-title <?php if($this->currentQuote && $this->currentQuote->car_has_e_hailing){ echo "active"; }?>">Yes</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" id="ehailingProtection2" class="input-radio" name="jform[ehailingProtection]" value="no" <?php if($this->currentQuote && !$this->currentQuote->car_has_e_hailing){ echo "checked"; }?> onclick="radioClicked(this);" required/>
									<label class="radio-label" for="ehailingProtection2">
										<span class="radio-label-title <?php if($this->currentQuote && !$this->currentQuote->car_has_e_hailing){ echo "active"; }?>">No</span>
									</label>
								</div>
							</div>
						<div class="input-wrapper ncd">
								<span class="input-title">No Claims Discount Percentage</span>
								<?php foreach(\Models\Eloquent\Quote::$ncdRates as $key => $ncdRate): ?>
								<div class="input-container-inline">
									<input type="radio" id="ncd<?php echo $key; ?>" class="input-radio" name="jform[ncd]" value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->ncd_rate == $key ? 'checked' : null; ?> onclick="radioClicked(this);"/>
									<label class="radio-label" for="ncd<?php echo $key; ?>">
										<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->ncd_rate == $key ? 'active' : null; ?>"><?php echo $ncdRate; ?></span>
									</label>
								</div>
								<?php endforeach; ?>
							</div>
						</div>
						
					</div>
					<div class="motor-accordion my-step2">CURRENT INSURER COVER DETAILS</div>
					<div class="panel my-step2">
						<div class="panel-left">
							<div class="input-wrapper">
								<span class="input-title">Current Renewal Premium</span>
								<div class="input-container-block relative">
									<i class="rm">RM</i>
									<input type="number" class="input-text front with-rm" name="jform[currentRenew]" value="0"/>
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title">Current Sum Insured</span>
								<div class="input-container-block relative">
									<i class="rm">RM</i>
									<input type="number" class="input-text front with-rm" name="jform[currentSum]" value="0"/>
								</div>
							</div>
							<div class="input-wrapper past3yrs">
								<span class="input-title long">Have you suffered losses or made any claims in past 3 years</span>
								<div class="input-container-inline">
									<input type="radio" id="claimPast1" class="input-radio" name="jform[claimPast]" value="yes" <?php if($this->currentQuote && $this->currentQuote->has_past_claims){ echo "checked"; }?> onclick="radioClicked(this);claimPastYearClicked(this);"/>
									<label class="radio-label" for="claimPast1">
										<span class="radio-label-title <?php if($this->currentQuote && $this->currentQuote->has_past_claims){ echo "active"; }?>">Yes</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" id="claimPast2" class="input-radio" name="jform[claimPast]" value="no" <?php if($this->currentQuote && !$this->currentQuote->has_past_claims){ echo "checked"; }?> onclick="radioClicked(this);claimPastYearClicked(this);" required/>
									<label class="radio-label" for="claimPast2">
										<span class="radio-label-title <?php if($this->currentQuote && !$this->currentQuote->has_past_claims){ echo "active"; }?>">No</span>
									</label>
								</div>
							</div>
							
						</div>
						<div id="panel-claim-past-year" class="panel-right" style="display: none;">
							<div class="input-wrapper damage-title"><span class="input-title">Number of Claims in the Past 3 Years on</span></div>
							<div class="input-wrapper damage">
								
								<span class="input-title">- damage to own vehicle</span>
								
								<?php foreach (\Models\Eloquent\Quote::$damageOwns as $key => $damageOwn): ?>
								<div class="input-container-inline">
									<input type="radio" id="damageOwn<?php echo $key; ?>" class="input-radio" name="jform[damageOwn]" value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->car_damage == $key ? 'checked' : null; ?> onclick="radioClicked(this);"/>
									<label class="radio-label" for="damageOwn<?php echo $key; ?>">
										<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->car_damage == $key ? 'active' : null; ?>"><?php echo $damageOwn; ?></span>
									</label>
								</div>
								<?php endforeach;?>
							</div>
							<div class="input-wrapper damage">
								<span class="input-title">- windscreen</span>
								<?php foreach (\Models\Eloquent\Quote::$damageWindscreens as $key => $damageWindscreen): ?>
								<div class="input-container-inline">
									<input type="radio" id="damageWindscreen<?php echo $key; ?>" class="input-radio" name="jform[windscreen]" value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->car_windscreen == $key ? 'checked' : null; ?> onclick="radioClicked(this);"/>
									<label class="radio-label" for="damageWindscreen<?php echo $key; ?>">
										<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->car_windscreen == $key ? 'active' : null; ?>"><?php echo $damageWindscreen; ?></span>
									</label>
								</div>
								<?php endforeach;?>
							</div>
							<div class="input-wrapper damage">
								<span class="input-title">- theft</span>
								<?php foreach (\Models\Eloquent\Quote::$damageThefts as $key => $damageTheft): ?>
								<div class="input-container-inline">
									<input type="radio" id="damageTheft<?php echo $key; ?>" class="input-radio" name="jform[theft]" value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->car_theft == $key ? 'checked' : null; ?> onclick="radioClicked(this);"/>
									<label class="radio-label" for="damageTheft<?php echo $key; ?>">
										<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->car_theft == $key ? 'active' : null; ?>"><?php echo $damageTheft; ?></span>
									</label>
								</div>
								<?php endforeach;?>
							</div>
							<div class="input-wrapper damage">
								<span class="input-title">- third party claim</span>
								
								<?php foreach (\Models\Eloquent\Quote::$damageTpClaims as $key => $damageTpClaim): ?>
								<div class="input-container-inline">
									<input type="radio" id="damageTpClaim<?php echo $key; ?>" class="input-radio" name="jform[tpClaim]" value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->car_tp_claim == $key ? 'checked' : null; ?> onclick="radioClicked(this);"/>
									<label class="radio-label" for="damageTpClaim<?php echo $key; ?>">
										<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->car_tp_claim == $key ? 'active' : null; ?>"><?php echo $damageTpClaim; ?></span>
									</label>
								</div>
								<?php endforeach;?>
							</div>
						</div>
					</div>
					<div class="motor-btn-wrapper my-step2">
						<div class="motor-btn-group">
							<button type="button" class="motor-btn back-3-btn back-btn mr20"><span class="white-arrow-left"></span>BACK</button>
							<button id="continue-step2" type="button" class="continue-3-btn motor-btn">CONTINUE<span class="white-arrow"></span></button>
						</div>
					</div>
					<div class="motor-accordion my-step3">DRIVER(S)</div>
					<div class="panel my-step3" id="driver-wrapper" style="max-height: 5000px!important;">
						<div class="tab-header-container d-tab-header-container">
							<div class="tab-pane active d-pane" onClick="changeTab(this);" id="driver-0" data-id="driver-panel-0">
								<img class="tab-pane-image" alt="add driver" src="<?php echo MyHelper::route('images/websiteicon/driver-logo.png'); ?>" />
								<span class="tab-pane-title">MAIN DRIVER</span>
							</div>
						</div>
						<div class="tab-content-container d-tab-content-container">
							<div class="tab-wrapper d-wrapper" id="driver-panel-0">
								<div class="panel-left">
									<div class="input-wrapper gender-wrapper">
										<span class="input-title">What is your gender?</span>
										<br>
										<div class="input-container-inline">
											<input <?php echo $this->currentQuote && $this->currentQuote->drivers->count() && $this->currentQuote->drivers->first()->gender == 'MALE' ? 'checked' : null; ?> type="radio" id="radio-gender-0-M" class="input-radio driver-gender" name="jform[Gender][]" value="MALE" onclick="radioClicked(this);" />
											<label class="radio-label-logo <?php echo $this->currentQuote && $this->currentQuote->drivers->count() && $this->currentQuote->drivers->first()->gender == 'MALE' ? 'active' : null; ?>" for="radio-gender-0-M">
												<img class="radio-label-gender-icon" alt="male" src="<?php echo MyHelper::route('images/websiteicon/male-logo.png'); ?>"/>
												<span class="radio-label-title-gender">Male</span>
											</label>
										</div>
										<div class="input-container-inline">
											<input <?php echo $this->currentQuote && $this->currentQuote->drivers->count() && $this->currentQuote->drivers->first()->gender == "FEMALE" ? 'checked' : null; ?> type="radio" id="radio-gender-0-F" class="input-radio driver-gender" name="jform[Gender][]" value="FEMALE" onclick="radioClicked(this);" />
											<label class="radio-label-logo <?php echo $this->currentQuote && $this->currentQuote->drivers->count() && $this->currentQuote->drivers->first()->gender == "FEMALE" ? 'active' : null; ?>" for="radio-gender-0-F">
												<img class="radio-label-gender-icon" alt="female" src="<?php echo MyHelper::route('images/websiteicon/female-logo.png'); ?>"/>
												<span class="radio-label-title-gender">Female</span>
											</label>
										</div>
									</div>
									<div class="input-wrapper">
										<span class="input-title">Salutation</span>
										<div class="input-container-block">
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[salutation][]" onchange="" onclick="return false;"  class="hide-select custom-select">
													<option value="" disabled>- PLEASE SELECT -</option>
													<?php foreach (\Models\Eloquent\QuoteDriver::$salutations as $key => $value): ?>
														<option value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->drivers->count() && $this->currentQuote->drivers->first()->salutation == $key ? 'selected' : null; ?>><?php echo $value; ?></option>
													<?php endforeach;?>
												</select>
											</div>
										</div>
									</div>
									<div class="input-wrapper">
										<span class="input-title">Name</span>
										<div class="input-container-block">
											<input type="text" class="input-text front driver-name" name="jform[name][]" value="<?php echo $this->currentQuote && $this->currentQuote->drivers->count() ? $this->currentQuote->drivers->first()->name : null; ?>"/>
										</div>
									</div>
									<div class="input-wrapper">
										<span class="input-title">Date Of Birth</span>
										<div class="input-container-block">
											<input type="text" title="" class="input-text calendar-input dob-input" readonly name="jform[dateOfBirth][]" value="<?php echo $this->currentQuote && $this->currentQuote->drivers->count() && $this->currentQuote->drivers->first()->dob ? $this->currentQuote->drivers->first()->dob->format('d-M-Y') : null; ?>" size="30" maxlength="20" class="inputbox validate-calendar" placeholder="DD-MM-YYYY" required="" aria-required="true" />
										</div>
									</div>
									<div class="input-wrapper">
										<span class="input-title">Race</span>
										<div class="input-container-block">
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[race][]" onchange="" onclick="return false;"  class="hide-select custom-select">
													<option value="" disabled>- PLEASE SELECT -</option>
													<?php foreach (\Models\Eloquent\QuoteDriver::$races as $key => $value): ?>
														<option value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->drivers->count() && $this->currentQuote->drivers->first()->race == $key ? 'selected' : null; ?>><?php echo $value; ?></option>
													<?php endforeach;?>
												</select>
											</div>
										</div>
									</div>
									<div class="input-wrapper">
										<span class="input-title">Marital Status</span>
										<div class="input-container-block">
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[maritalStatus][]" onchange="" onclick="return false;"  class="hide-select custom-select">
													<option value="" disabled>- PLEASE SELECT -</option>
													<?php foreach (\Models\Eloquent\QuoteDriver::$maritalStatuses as $key => $value): ?>
														<option value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->drivers->count() && $this->currentQuote->drivers->first()->marital_status == $key ? 'selected' : null; ?>><?php echo $value; ?></option>
													<?php endforeach;?>
												</select>
											</div>
											<!-- Custom select structure --> 
										</div>
									</div>
									<div class="input-wrapper">
										<span class="input-title">Years of driving experience?</span>
										<div class="input-container-block">
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[yearsOfDrivingExp][]" onchange="" onclick="return false;"  class="hide-select custom-select">
													<option value="" <?php !$this->currentQuote ? 'selected' : null; ?>>Select Driving Experience</option>
													
													<?php foreach (\Models\Eloquent\Quote::$drivingExperiences as $key => $value): ?>
														<option value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->drivers->count() && $this->currentQuote->drivers->first()->exp == $key ? 'selected' : null; ?>><?php echo $value; ?></option>
													<?php endforeach;?>
												</select>
											</div>
											<!-- Custom select structure --> 
										</div><!-- End div center   -->
									</div>
								</div>
								<!-- end left -->
								<div class="panel-right driver-right">
									<div class="input-wrapper" style="margin-top: 16px;">
										<span class="input-title">Occupation</span>
										<div class="input-container-block">
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[driverOccupation][]" onchange="" onclick="return false;"  class="hide-select custom-select occupation-select" style="width:300px;">
													<?php if ($this->currentQuote && $this->currentQuote->drivers->first() && $this->currentQuote->drivers->first()->occupation): ?>
													<option value="<?php echo $this->currentQuote->drivers->first()->occupation->id; ?>"><?php echo $this->currentQuote->drivers->first()->occupation->name; ?></option>
													<?php else: ?>
													<option value="" disabled>- PLEASE SELECT -</option>
													<?php endif;?>			
												</select>
											</div>
											<!-- Custom select structure --> 
										</div>
									</div>
									<div class="input-wrapper">
										<span class="input-title">Occupation Status</span>
										<div class="input-container-block">
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[occupationStatus][]" onchange="" onclick="return false;"  class="hide-select custom-select">
													<?php foreach (\Models\Eloquent\QuoteDriver::$occupationStatuses as $key => $value): ?>
														<option value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->drivers->count() && $this->currentQuote->drivers->first()->occ_status == $key ? 'selected' : null; ?>><?php echo $value; ?></option>
													<?php endforeach;?>
												</select>
											</div>
											<!-- Custom select structure --> 
										</div>
									</div>
									<div class="input-wrapper">
										<span class="input-title">Occupation Nature</span>
										<div class="input-container-block">
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[occupationNature][]" onchange="" onclick="return false;"  class="hide-select custom-select">
													<?php foreach (\Models\Eloquent\QuoteDriver::$occupationNatures as $key => $value): ?>
														<option value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->drivers->count() && $this->currentQuote->drivers->first()->occ_nature == $key ? 'selected' : null; ?>><?php echo $value; ?></option>
													<?php endforeach;?>
												</select>
											</div>
											<!-- Custom select structure --> 
										</div>
									</div>
									<div class="input-wrapper">
										<span class="input-title">Mailing Address Line 1</span>
										<div class="input-container-block">
											<input type="text" class="input-text front" name="jform[addressOne][]" value="<?php echo $this->currentQuote && $this->currentQuote->drivers->count() ? $this->currentQuote->drivers->first()->address_one : null; ?>"/>
										</div>
									</div>
									<div class="input-wrapper">
										<span class="input-title">Mailing Address Line 2</span>
										<div class="input-container-block">
											<input type="text" class="input-text front" name="jform[addressTwo][]" value="<?php echo $this->currentQuote && $this->currentQuote->drivers->count() ? $this->currentQuote->drivers->first()->address_two : null; ?>"/>
										</div>
									</div>
									<div class="input-wrapper">
										<span class="input-title">Address Postal Code</span>
										<div class="input-container-block">
											<input type="text" class="input-text front" name="jform[postalCode][]" value="<?php echo $this->currentQuote && $this->currentQuote->drivers->count() ? $this->currentQuote->drivers->first()->postal_code : null; ?>"/>
										</div>
									</div>
									<div class="input-wrapper">
										<span class="input-title">IC Number</span>
										<div class="input-container-block">
											<input type="text" class="input-text front" name="jform[icNum][]" value="<?php echo $this->currentQuote && $this->currentQuote->drivers->count() ? $this->currentQuote->drivers->first()->nric : null; ?>"/>
										</div>
									</div>
									<div class="input-wrapper">
										<span class="input-title">Email</span>
										<div class="input-container-block">
											<input type="text" class="input-text front" name="jform[email][]" value="<?php echo $this->currentQuote && $this->currentQuote->drivers->count() ? $this->currentQuote->drivers->first()->email : null; ?>"/>
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="input-wrapper add-driver">
									<div id="addDriverButton" data-currentcount="<?php echo $this->currentQuote && $this->currentQuote->drivers->count() ? $this->currentQuote->drivers->count() : '0'; ?>" onClick="addDriver(this);" style="float:right">
										<img class="add-driver-img" alt="add driver" src="<?php echo MyHelper::route('images/websiteicon/add-driver-logo.png'); ?>"/>
										<span class="add-driver-title">ADD A DRIVER</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="motor-accordion my-step3">ADDITIONAL CAR DETAILS</div>
					<div class="panel my-step3" style="max-height: 5000px!important;">
						<div class="panel-left">
							<div class="input-wrapper">
								<span class="input-title">Geographical Location</span>
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
										<select name="jform[location]" onchange="" onclick="return false;"  class="hide-select custom-select" required>
											<?php foreach (\Models\Eloquent\Quote::$carLocations as $key => $value): ?>
												<option value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->car_location == $key ? 'selected' : null; ?>><?php echo $value; ?></option>
											<?php endforeach;?>
										</select>
									</div>
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title">Address of Your Car Normally Kept</span>
								<div class="input-container-block">
									<input type="text" class="input-text front" name="jform[carAddress]" value="<?php echo $this->currentQuote ? $this->currentQuote->car_address : null; ?>"/>
								</div>
							</div>
							<div class="input-wrapper" style="margin-top: 10px;">
								<span class="input-title">Log Book No (if applicable)</span>
								<div class="input-container-block">
									<input type="text" class="input-text front" name="jform[logBook]" value="<?php echo $this->currentQuote ? $this->currentQuote->car_logbook : null; ?>"/>
								</div>
							</div>
						</div>
						<div class="panel-right">
						<div class="input-wrapper">
							<span class="input-title">Road Tax Expiry Date</span>
							<div class="input-container-block">
								<?php echo $this->form->getInput('taxExpiry', null, $this->currentQuote && $this->currentQuote->car_tax_expiry ? $this->currentQuote->car_tax_expiry->format('d-M-Y') : null); ?>
							</div>
						</div>
						<div class="input-wrapper">
							<span class="input-title">Any Existing Loan on Your Car</span>
							<div class="input-container-inline">
								<input type="radio" <?php echo $this->currentQuote && $this->currentQuote->car_loan ? 'checked' : null; ?> id="loan1" class="input-radio" name="jform[loan]" value="yes" onclick="radioClicked(this);loanClicked(true);"/>
								<label class="radio-label" for="loan1">
									<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->car_loan ? 'active' : null; ?>">Yes</span>
								</label>
							</div>
							<div class="input-container-inline">
								<input type="radio" <?php echo $this->currentQuote && !$this->currentQuote->car_loan ? 'checked' : null; ?> id="loan2" class="input-radio" name="jform[loan]" value="no" onclick="radioClicked(this);loanClicked(false);" required/>
								<label class="radio-label" for="loan2">
									<span class="radio-label-title <?php echo $this->currentQuote && !$this->currentQuote->car_loan ? 'active' : null; ?>">No</span>
								</label>
							</div>
						</div>
						<div class="input-wrapper loan-company <?php echo $this->currentQuote && $this->currentQuote->car_loan ? null : 'hide'; ?>">
							<span class="input-title">Loan Company</span>
							<div class="input-container-block">
								<div class="select_mate" data-mate-select="active" >
									<select name="jform[loanCompany]" onchange="" onclick="return false;"  class="hide-select custom-select loan-company-select" style="width:300px;" required>
										<?php if($this->currentQuote && $this->currentQuote->loan_company): ?>
										<option value="<?php echo $this->currentQuote->loan_company->id; ?>"><?php echo $this->currentQuote->loan_company->name; ?></option>
										<?php else: ?>
										<option value="">- PLEASE SELECT -</option>
										<?php endif; ?>
									</select>
								</div>
							</div>
						</div>
						<div class="input-wrapper loan-company <?php echo $this->currentQuote && $this->currentQuote->car_loan ? null : 'hide'; ?>">
							<span class="input-title">Loan Company</span>
							<div class="input-container-block">
								<input type="text" placeholder="Please type your Loan Company name." class="input-text front" name="jform[loanCompanyOther]" value="<?php echo $this->currentQuote ? $this->currentQuote->car_loan_company_other : null; ?>"/>
							</div>
						</div>
						</div>
					</div>
					<div class="motor-accordion my-step3">NEW INSURANCE COVER DETAILS</div>
					<div class="panel my-step3" style="max-height: 5000px!important;">
						<div class="panel-left">
							<div class="input-wrapper">
								<span class="input-title">Vehicle Market Valuation</span>
								<div class="input-container-block relative">
									<i class="rm">RM</i>
									<input type="number" class="input-text front with-rm" id="market-value" name="jform[marketValue]" value="<?php echo $this->currentQuote ? $this->currentQuote->car_market_value : '0'; ?>" disabled/>
								</div>
							</div>
							<div class="input-wrapper input-helper">
								<span class="input-title">Basis Coverage</span>
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
										<select id="basisCoverage" name="jform[basisCoverage]" class="hide-select custom-select" required>
											<?php foreach (\Models\Eloquent\Quote::$basisCoverages as $key => $value): ?>
												<option value="<?php echo $key; ?>" <?php echo $this->currentQuote && $this->currentQuote->basis_coverage == $key ? 'selected' : null; ?>><?php echo $value; ?></option>
											<?php endforeach;?>
										</select>
										<span id="field-helper" class="field-helper-select">Market Value: Your vehicle is insured based on market value.</span>
									</div>
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title insured-title"><b>Market</b> Value Sum Insured</span>
								<div class="input-container-block relative">
									<i class="rm">RM</i>
									<input type="number" class="input-text front with-rm" id="sum-insured" name="jform[sumInsured]" data-defaultvalue="<?php echo $this->currentQuote ? ($this->currentQuote->sum_insured ? $this->currentQuote->sum_insured : $this->currentQuote->car_market_value) : '0'; ?>" disabled />
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title">No Claim Discount Percentage</span>
								<div class="input-container-block">
									<input type="text" class="input-text front" name="jform[ncdDisplay]" value="<?php echo $this->currentQuote ? $this->currentQuote->ncd_rate_fmt : null; ?>" disabled />
								</div>
							</div>
							<div class="input-wrapper input-helper">
								<span class="input-title">Old Car Registration Number</span>
								<div class="input-container-block">
									<input type="text" class="input-text front" name="jform[oldCarNo]" value="<?php echo $this->currentQuote ? $this->currentQuote->car_old_registration_number : null; ?>" />
									<span class="field-helper-input">For the transfer of NCD if required</span>
								</div>
							</div>
						</div>
						<div class="panel-right optional-cover">
							<div class="input-wrapper damage-title"><span class="input-title">Optional Cover Selections</span></div>
							<div class="input-wrapper damage">
								<span class="input-title">Compensation for Assessed Repair Time - CART</span>
								
								<div class="input-container-inline">
									<input type="radio" <?php echo $this->currentQuote && $this->currentQuote->cart ? 'checked' : null; ?> id="cart1" class="input-radio" name="jform[cart]" value="1" onclick="radioClicked(this);"/>
									<label class="radio-label" for="cart1">
										<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->cart ? 'active' : null; ?>">Yes</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" <?php echo $this->currentQuote && !$this->currentQuote->cart ? 'checked' : null; ?> id="cart2" class="input-radio" name="jform[cart]" value="0" onclick="radioClicked(this);" required/>
									<label class="radio-label" for="cart2">
										<span class="radio-label-title <?php echo $this->currentQuote && !$this->currentQuote->cart ? 'active' : null; ?>">No</span>
									</label>
								</div>
							</div>
							<div class="input-wrapper damage">
								<span class="input-title">Inclusion of Special Perils or Convultion of Nature</span>
								
								<div class="input-container-inline">
									<input type="radio" <?php echo $this->currentQuote && $this->currentQuote->specialPeril ? 'checked' : null; ?> id="specialPeril1" class="input-radio" name="jform[specialPeril]" value="1" onclick="radioClicked(this);"/>
									<label class="radio-label" for="specialPeril1">
										<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->specialPeril ? 'active' : null; ?>">Yes</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" <?php echo $this->currentQuote && !$this->currentQuote->specialPeril ? 'checked' : null; ?> id="specialPeril2" class="input-radio" name="jform[specialPeril]" value="0" onclick="radioClicked(this);" required/>
									<label class="radio-label" for="specialPeril2">
										<span class="radio-label-title <?php echo $this->currentQuote && !$this->currentQuote->specialPeril ? 'active' : null; ?>">No</span>
									</label>
								</div>
							</div>
							<div class="input-wrapper damage">
								<span class="input-title">Legal Liability of Passengers</span>
								
								<div class="input-container-inline">
									<input type="radio" <?php echo $this->currentQuote && $this->currentQuote->liabilityPass ? 'checked' : null; ?> id="liabilityPass1" class="input-radio" name="jform[liabilityPass]" value="1" onclick="radioClicked(this);"/>
									<label class="radio-label" for="liabilityPass1">
										<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->liabilityPass ? 'active' : null; ?>">Yes</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" <?php echo $this->currentQuote && !$this->currentQuote->liabilityPass ? 'checked' : null; ?> id="liabilityPass2" class="input-radio" name="jform[liabilityPass]" value="0" onclick="radioClicked(this);" required/>
									<label class="radio-label" for="liabilityPass2">
										<span class="radio-label-title <?php echo $this->currentQuote && !$this->currentQuote->liabilityPass ? 'active' : null; ?>">No</span>
									</label>
								</div>
							</div>
							<div class="input-wrapper damage">
								<span class="input-title">Private Hire Car or E-Hailing</span>
								
								<div class="input-container-inline">
									<input type="radio" <?php echo $this->currentQuote && $this->currentQuote->ehailing ? 'checked' : null; ?> id="ehailing1" class="input-radio" name="jform[ehailing]" value="1" onclick="radioClicked(this);"/>
									<label class="radio-label" for="ehailing1">
										<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->ehailing ? 'active' : null; ?>">Yes</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" <?php echo $this->currentQuote && !$this->currentQuote->ehailing ? 'checked' : null; ?> id="ehailing2" class="input-radio" name="jform[ehailing]" value="0" onclick="radioClicked(this);" required/>
									<label class="radio-label" for="ehailing2">
										<span class="radio-label-title <?php echo $this->currentQuote && !$this->currentQuote->ehailing ? 'active' : null; ?>">No</span>
									</label>
								</div>
							</div>
							<div class="input-wrapper damage">
								<span class="input-title">Strike, Riot and Civil Commotion</span>
								
								<div class="input-container-inline">
									<input type="radio" <?php echo $this->currentQuote && $this->currentQuote->strike ? 'checked' : null; ?> id="strike1" class="input-radio" name="jform[strike]" value="1" onclick="radioClicked(this);"/>
									<label class="radio-label" for="strike1">
										<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->strike ? 'active' : null; ?>">Yes</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" <?php echo $this->currentQuote && !$this->currentQuote->strike ? 'checked' : null; ?> id="strike2" class="input-radio" name="jform[strike]" value="0" onclick="radioClicked(this);" required/>
									<label class="radio-label" for="strike2">
										<span class="radio-label-title <?php echo $this->currentQuote && !$this->currentQuote->strike ? 'active' : null; ?>">No</span>
									</label>
								</div>
							</div>
							<div class="input-wrapper damage">
								<span class="input-title">Waiver of Betterment</span>
								
								<div class="input-container-inline">
									<input type="radio" <?php echo $this->currentQuote && $this->currentQuote->waiver ? 'checked' : null; ?> id="waiver1" class="input-radio" name="jform[waiver]" value="1" onclick="radioClicked(this);"/>
									<label class="radio-label" for="waiver1">
										<span class="radio-label-title <?php echo $this->currentQuote && $this->currentQuote->waiver ? 'active' : null; ?>">Yes</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" <?php echo $this->currentQuote && !$this->currentQuote->waiver ? 'checked' : null; ?> id="waiver2" class="input-radio" name="jform[waiver]" value="0" onclick="radioClicked(this);" required/>
									<label class="radio-label" for="waiver2">
										<span class="radio-label-title <?php echo $this->currentQuote && !$this->currentQuote->waiver ? 'active' : null; ?>">No</span>
									</label>
								</div>
							</div>
							<div class="input-wrapper damage">
								<span class="input-title">Windscreen Damage on Tempered or Laminated Glass</span>
								<div class="input-container-block">
									<i class="rm">RM</i>
									<input type="number" class="input-text front with-rm windscreen-extracov-input" name="jform[laminated]" value="<?php echo $this->currentQuote ? $this->currentQuote->laminated : '0'; ?>" />
								</div>
							</div>
							<div class="input-wrapper damage">
								<span class="input-title">Windscreen Damage on Tinting Film</span>
								<div class="input-container-block">
									<i class="rm">RM</i>
									<input type="number" class="input-text front with-rm windscreen-extracov-input" name="jform[tinting]" value="<?php echo $this->currentQuote ? $this->currentQuote->tinting : '0'; ?>" />
								</div>
							</div>
						</div>
					</div>
				</div>

				<input id="quoteStep" type="hidden" name="jform[step]" value="<?php echo $this->currentQuote ? $this->currentQuote->step : "1"; ?>" />
				<input id="finalBasisCoverage" type="hidden" name="jform[finalBasisCoverage]" value="" />
				<input id="finalSumInsured" type="hidden" name="jform[finalSumInsured]" value="" />
		
				<input type="hidden" name="task" value="form.quoteSave" />
				<?php echo $this->form->getInput('id'); ?>
				<?php if (isset($itemid) && $itemid): ?>
					<input type="hidden" name="Itemid" value="$itemid" />
				<?php endif; ?>
				<?php echo JHtml::_('form.token'); ?>
				
				<input id="quoteSummaryJson" type="hidden" name="jform[quoteSummaryJson]" value="">
				<input id="quotePromosJson" type="hidden" name="jform[quotePromosJson]" value="">
				
				<?php 
					if(!$this->user->guest) {
				?>
				<div class="motor-btn-wrapper my-step3">
					<div class="motor-btn-group">
						<button type="button" class="motor-btn back-4-btn back-btn mr20"><span class="white-arrow-left"></span>BACK</button>
						<button type="button" class="continue-4-btn motor-btn">CONTINUE<span class="white-arrow"></span></button>
					</div>
				</div>
				<?php
					}
				?>
				
			</form>
			<div class="clear"></div>
			<?php 
				if($this->user->guest) {
			?>	
			<div class="motor-form-input-wrapper login-section my-step3" <?php echo (isset($user) && $user->id > 0) ? "style=\"display:none;\"" : ""; ?>>
				<div class="motor-accordion">LOGIN/REGISTER</div>
				<div class="panel my-step3" style="margin-bottom:20px;max-height: 5000px!important;">
					<div class="tab-header-container">
						<div class="tab-pane active login-pane" onclick="changeLoginTab(this);" id="login-0" data-id="login-panel" data-title="Login and enjoy up to RM20 off">
							<span class="tab-pane-title">LOGIN</span>
						</div>
						<div class="tab-pane login-pane" onclick="changeLoginTab(this);" id="register-1" data-id="register-panel" data-title="Register and enjoy up to RM20 off"> 
							<span class="tab-pane-title">REGISTER</span>
						</div>
					</div>
					<div class="tab-content-container">
						<div class="tab-wrapper login-wrapper" id="login-panel">
						    <div class="red-text">Login and enjoy up to RM20 off!</div>
							<div class="insurediy-form">
								<form id="login-form-popup">
								<div class="input-wrapper">
									<span class="input-title">Email</span>
									<div class="input-container-block">
										<input type="text" class="input-text" style="width:90%;" name="username" />
									</div>
								</div>
								<div class="input-wrapper">
									<span class="input-title">Password</span>
									<div class="input-container-block">
										<input type="password" class="input-text" style="width:90%;" name="password" value="" />
										<input type="hidden" name="option" value="com_insurediymotor" />
										<input type="hidden" name="task" value="user.ajaxLogin" />
										<input type="hidden" name="return" value="" />
										<?php echo JHtml::_('form.token'); ?>
									</div>
								</div>
								<div class="motor-btn-wrapper">
									<div class="motor-btn-group">
										<button type="button" class="back-4-btn motor-btn back-btn"><span class="white-arrow-left"></span>BACK</button>
										<button type="button" class="continue-4-btn motor-btn">CONTINUE<span class="white-arrow"></span></button>
									</div>
									<div class="no-continue continue-4-btn">No thanks, I will pay full amount</div>
								</div>
								</form>
							</div>
						</div>
						<div class="tab-wrapper display-none login-wrapper" id="register-panel">
							<div class="red-text">Register and enjoy up to RM20 off!</div>
							<div class="insurediy-form">
							    <form id="register-form-popup">
								<div class="input-wrapper">
									<span class="input-title">Email</span>
									<div class="input-container-block">
										<input type="text" class="input-text" style="width:90%;" name="email" />
									</div>
								</div>
								<div class="input-wrapper">
									<span class="input-title">Password</span>
									<div class="input-container-block">
										<input type="password" class="input-text" style="width:90%;" name="password" value="" />
									</div>
								</div>
								<div class="input-wrapper">
									<span class="input-title">Confirm Password</span>
									<div class="input-container-block">
										<input type="password" class="input-text" style="width:90%;" name="password2" value="" />
										<input type="hidden" name="option" value="com_insurediymotor" />
										<input type="hidden" name="task" value="user.ajaxRegister" />
										<input type="hidden" name="return" value="" />
										<?php echo JHtml::_('form.token'); ?>
									</div>
								</div>
								<div class="motor-btn-wrapper">
									<div class="motor-btn-group">
										<button type="button" class="back-4-btn motor-btn back-btn"><span class="white-arrow-left"></span>BACK</button>
										<button type="button" class="continue-4-btn motor-btn">CONTINUE<span class="white-arrow"></span></button>
									</div>
									<div class="no-continue continue-4-btn">No thanks, I will pay full amount</div>
								</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
				}
			?>
			<div class="motor-form-input-wrapper my-last-choose-insurer" style="display: none;">
				<div class="motor-btn-wrapper">
					<div class="motor-btn-group">
						<button type="button" class="motor-btn back-last-choose-insurer back-btn"><span class="white-arrow-left"></span>BACK</button>
					</div>
				</div>
			</div>
			<div class="motor-form-input-wrapper my-step4">
				<div class="motor-accordion">APPLICATION SUMMARY</div>
				<div class="panel" style="max-height: 5000px!important;">
					<div class="center-panel">
						<table class="summary-table">
							<tr class="border-bottom">
								<th class="table-header">Provider</th>
								<th class="table-header border-left border-right">Description</th>
								<th class="table-header">Total Premium</th>
							</tr>
							<tr id="summary-table-detail" class="border-bottom">
								<td class="partner-title">
									Insurer
								</td>
								<td class="border-left border-right content-column" style="vertical-align: top;">
									<ul class="summary-desc-list">
										<li class="desc-list">
											<b>Insurer Plan</b>
										</li>
										<li class="desc-list">
											Cover Type : <span style="color:#5197d5;">COMPREHENSIVE</span>
										</li>
										<li class="desc-list">
											Use code <span style="color:#fc5b6c;font-weight:bold;">CAR20</span> to get RM20 off!
										</li>
										<li class="desc-list">
											Policy Start Date : <span class="font-bold"><?php echo date('d-M-Y'); ?></span>
										</li>
									</ul>
								</td>
								<td class="table-header font-bold">
									<ul class="summary-subtotal-list">
										<li class="desc-list">RM 0.00</li>
										<li class="desc-list">&nbsp;</li>
										<li class="desc-list">&nbsp;</li>
									</ul>
								</td>
							</tr>
							<tr>
								<td></td>
								<td class="border-left border-right table-header font-bold summary-total-amount-title" style="text-align: right">
								Total
								</td>
								<td class="table-header font-bold summary-total-amount">RM 0.00</td>
							</tr>
						</table>

						<div class="center-container">
							<?php
								if(!$this->user->guest && (!$this->currentQuote || !$this->currentQuote->checkPromoCodeUsed('CAR20'))) {
							?>
							<div class="promo-code-container">
								<div class="promobox1">
									<form action="" method="post" id="jform_apply_promo_code" class="form-validate form-vertical">
										<div id="promo-words"><span>Use promo code <b class="red-text">CAR20</b> to get RM20 off!</span></div>
										<img class="promo-img" src="<?php echo MyHelper::route('images/promo.png') ?>">
										<input name="jform[promo_code]" id="promo_code" type="text" value="" />
										<input type="submit" class="idd_blackbutton2 promo_button" value="Apply" />
									</form>
									<form action="" method="post" id="jform_apply_promo_code_referral" class="form-validate form-vertical">
										<div id="promo-words"><span>Use your friend's referral code to get RM5 off!</span></div>
										<img class="promo-img" src="<?php echo MyHelper::route('images/handshake.png') ?>">
										<input name="jform[promo_code]" id="promo_code_referral" type="text" value="" />
										<input type="submit" class="idd_blackbutton2 promo_button" value="Apply" />
									</form>
									<div class="share-ref">
										<div class="referral-sub-header w-100">Share your referral code with your friend!</div>
										<div id="referral-copy1-wrapper">
											<div id="referral-copy1" style="text-align:center;"> </div>
										</div>
										<div class="share-button">
											<a href="#"><img src="<?php echo MyHelper::route('images/fb-share.png') ?>" alt="" class="fb-share"></a>
											<a href="#"><img src="<?php echo MyHelper::route('images/twitter-share.png') ?>" alt="" class="fb-share"></a>
											<a href="#"><img src="<?php echo MyHelper::route('images/wa-share.png') ?>" alt="" class="fb-share"></a>
										</div>
										
									</div>
								</div>
								<!--<img class="promo-code-accept" src="images/websiteicon/accept.png" />-->
								<!-- <img class="promo-code-question" src="images/websiteicon/question-mark.png" />-->
							</div>
							<?php 
								}
							?>
							<div class="small-wordings">
								The amount shown above is the total premium payable for your policy.
							</div>
							<div class="bold-wordings">
								NOTE: Premium and excess amount may be subject to changes after review by the insurer. Addtional excess may apply to young/inexperienced/elderly drivers (if any). 
								All private car policyholders are responsible for an unnamed driver excess.
							</div>
							<div class="standard-wordings">
								On verification with your previous insurer and if NCD does not tally with the advice, the insurer shall proceed to recover NCD either via additional premium or via endorsement and shorten the period of insurance concurrently.
								Please note that NCD Protector (if any) is non-transferrable to another insurer. The NCD Protector will not necessarily protect you against non-renewal or cancellation of 
								your policy by your insurer. 
							</div>
						</div>
					</div>
				</div>
			</div>
		<div class="motor-form-input-wrapper order-summary my-step4">
			<div class="motor-accordion">ORDER SUMMARY</div>
			<div class="panel" style="max-height: 5000px!important;">
				<div class="panel-title">Car Details</div>
				<div class="panel-left" style="margin:0;">
					<div class="input-wrapper">
						<div class="field-title">Car Registration Number</div>
						<div data-inputname="jform[vehicleRegNo]" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Car Make</div>
						<div data-inputname="jform[carMake]" data-type="select" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Car Model</div>
						<div data-inputname="jform[carModel]" data-type="select" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Engine or Motor Number</div>
						<div data-inputname="jform[engineNo]" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Chassis Number</div>
						<div data-inputname="jform[chassisNo]" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Car Cubic Capacity</div>
						<div data-inputname="jform[vehicleEngineCc]" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Year of Manufacturing</div>
						<div data-inputname="jform[carMakeYear]" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Vehicle Purchase Date</div>
						<div data-inputname="jform[purchaseDate]" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Car Purchase Price</div>
						<div data-inputname="jform[purchasePrice]" data-currency="true" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Performance and Aesthetic Modifications</div>
						<div data-inputname="jform[performanceMod]" data-type="radio" class="field-value"></div>
					</div>
					
					
				</div>
				<div class="panel-right">
					<div class="input-wrapper">
						<div class="field-title">Car Body Type</div>
						<div data-inputname="jform[carBodyType]" data-type="select" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Local or Imported Vehicle</div>
						<div data-inputname="jform[carIsLocal]" data-type="radio" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Airbags</div>
						<div data-inputname="jform[airbags]" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Use of Vehicle</div>
						<div data-inputname="jform[vehicleUse]" data-type="select" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Car Fuel Type</div>
						<div data-inputname="jform[carFuel]" data-type="radio" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Vehicle Type</div>
						<div data-inputname="jform[vehType]" data-type="radio" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Number of Seats</div>
						<div data-inputname="jform[seatNumber]" data-type="radio" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Anti-Lock Braking System or ABS</div>
						<div data-inputname="jform[abs]" data-type="radio" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Vehicle Market Valuation</div>
						<div data-inputname="jform[marketValue]" data-currency="true" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Functional Modification</div>
						<div data-inputname="jform[functionalMod]" data-type="radio" class="field-value"></div>
					</div>
				</div>
				<div class="panel-title one-block">Driver Details</div>
					<div id='summary-driver-details-tab' class="tab-header-container" style="width:92%; margin: 5px 0px 10px 30px;">
						<?php for($i=1; $i<= 2; $i++):?>
							<?php if($i == 1):?>
								<div class="tab-pane active dsummary-pane"  onClick="changeDsummaryTab(this);" id="driver-0" data-id="sdriver-panel-0">
									<img class="tab-pane-image" src="<?php echo MyHelper::route('images/websiteicon/driver-logo.png'); ?>" />
									<span class="tab-pane-title">MAIN DRIVER</span>
								</div>
							<?php else: ?>
								<div class="tab-pane dsummary-pane"  onClick="changeDsummaryTab(this);" id="driver-0" data-id="sdriver-panel-<?php echo $i-1; ?>">
									<img class="tab-pane-image" src="<?php echo MyHelper::route('images/websiteicon/driver-logo.png'); ?>" />
									<span class="tab-pane-title">DRIVER <?php echo $i; ?></span>
								</div>
							<?php  endif; ?>
						<?php endfor; ?>
					</div>
					<div id='summary-driver-details' class="tab-content-container" style="margin-bottom:20px;">
						<?php for($i=0; $i< 2; $i++):?>
								<div class="tab-wrapper dsummary-wrapper <?php if($i != 0) echo "display-none"; ?>" id="sdriver-panel-<?php echo $i; ?>" style="margin-left: 10px;">
									<div class="panel-left" style="margin:0;padding-left:20px;">
										<?php if($i==0): ?>
										<div class="input-wrapper">
											<div class="field-title">Gender</div>
											<div class="field-value"></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Salutation</div>
											<div class="field-value"></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Name</div>
											<div class="field-value"></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Date of Birth</div>
											<div class="field-value"></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Race</div>
											<div class="field-value"></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Marital Status</div>
											<div class="field-value"></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Total driving-offence Points in the Last 3 Years</div>
											<div class="field-value"></div>
										</div>
										<?php else: ?>
										<div class="input-wrapper">
											<div class="field-title">Name</div>
											<div class="field-value"></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">IC Number</div>
											<div class="field-value"></div>
										</div>
										<?php endif; ?>
									</div>
									<div class="panel-right" style="margin:0;padding-left: 15px;">
										<?php if($i==0): ?>
										<div class="input-wrapper">
											<div class="field-title">Occupation</div>
											<div class="field-value"></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Occupation Status</div>
											<div class="field-value"></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Nature of Occupation</div>
											<div class="field-value"></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Mailing Address Line 1</div>
											<div class="field-value"></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Mailing Address Line 2</div>
											<div class="field-value"></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">IC Number</div>
											<div class="field-value"></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Email</div>
											<div class="field-value"></div>
										</div>
										<?php else: ?>
										<div class="input-wrapper">
											<div class="field-title">Years of Driving Experience</div>
											<div class="field-value"></div>
										</div>
										<?php endif; ?>
									</div>
								</div>
						<?php endfor; ?>
						
						
					</div>

				<div class="panel-title current-insurance-summary">Current Insurance Cover Details</div>
				<div class="panel-left current-insurance-summary" style="margin:0;">
					<div class="input-wrapper">
						<div class="field-title">Current Insurer</div>
						<div data-inputname="jform[currentInsurer]" data-type="select" class="field-value">-</div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Current Renewal Premium</div>
						<div data-inputname="jform[currentRenew]" data-currency="true" class="field-value">RM 0.00</div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Current Sum Insured</div>
						<div data-inputname="jform[currentSum]" data-currency="true" class="field-value">RM 0.00</div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Have you suffered losses or made any claim in past 3 years</div>
						<div data-inputname="jform[claimPast]" class="field-value">Yes</div>
					</div>
				</div>
				<div class="panel-right current-insurance-summary">
					<div class="input-wrapper">
						<div class="field-title parent-title">Number of Claims in the Past 3 Years on</div>
						<div class="field-title">-damage to own vehicle</div>
						<div data-inputname="jform[damageOwn]" class="field-value">0</div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">-windscreen</div>
						<div data-inputname="jform[windscreen]" class="field-value">0</div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">-theft</div>
						<div data-inputname="jform[theft]" class="field-value">0</div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">-third party claim</div>
						<div data-inputname="jform[tpClaim]" class="field-value">>0</div>
					</div>
				</div>

				<div class="panel-title one-block">New Insurance Cover Details</div>
				<div class="panel-left" style="margin:0;">
					<div class="input-wrapper">
						<div class="field-title">Basic of Coverage</div>
						<div data-inputname="jform[basisCoverage]" data-type="select" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Market Value Sum Insured</div>
						<div data-inputname="jform[sumInsured]" data-currency="true" class="field-value"></div>
					</div>
				</div>
				<div class="panel-right">
					<div class="input-wrapper">
						<div class="field-title">NCD Protector</div>
						<div data-inputname="jform[ncdProtector]" data-type="radio" class="field-value"></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Old Car Registration Number</div>
						<div data-inputname="jform[oldCarNo]" class="field-value"></div>
					</div>
				</div>
				
				<div class="panel-title one-block">Optional Cover Selections</div>
				<div class="panel-left" style="margin:0;">
					<div class="input-wrapper">
						<div class="field-title">Compensation for Assessed Repair Time - CART</div>
						<div data-name="cart" class="extra-cov field-value h35">RM 0.00</div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Inclusion of Special Perils</div>
						<div data-name="specialPerils" class="extra-cov field-value h35">RM 0.00</div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Passenger Liability</div>
						<div data-name="liabilityPass" class="extra-cov field-value">RM 0.00</div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Private Hire Car / E-Hailing</div>
						<div data-name="ehailing" class="extra-cov field-value">RM 0.00</div>
					</div>
				</div>
				<div class="panel-right">
					<div class="input-wrapper">
						<div class="field-title">Strike, Riot and Civil<br>Commotion</div>
						<div data-name="strike" class="extra-cov field-value h35">RM 0.00</div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Waiver of Betterment</div>
						<div data-name="waiver" class="extra-cov field-value">RM 0.00</div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Windscreen Damage on Tempered or Laminated Glass (Inclusive of Labour Cost)</div>
						<div data-name="laminated" class="extra-cov field-value" style="height:55px;">RM 0.00</div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Windscreen Damage on Tinting Film (Inclusive of Labour Cost)</div>
						<div data-name="tinting" class="extra-cov field-value">RM 0.00</div>
					</div>
				</div>
			</div>

			<div class="motor-form-input-wrapper my-step4">
			<div class="motor-accordion">IMPORTANT NOTICE & POLICYHOLDER DECLARATIONS</div>
			<div class="panel" style="max-height: 5000px!important;">
				<div class="panel-top">
					<div class="panel-declaration">
						Important notice:<br><br>
						<ul>
						<li>My existing car insurance will be expired within 3 months’ time;</li>
						<li>I agree not to lend my car to a person to drive who is under 25 years of age or a person who has not held for a period of 2 years a driving license.</li>
						<li>I and all named drivers have not suffered from any physical or mental infirmity that may affect my/their ability to drive.</li>
						<li>I understand that any incorrect information provided may invalidate the quote and the insurance.</li>
						<li>I/We clearly understand this declaration shall be incorporated in and taken as the basis of a proposed contract.</li>
						</ul>
						<br>I / We hereby declare and agree that:<br><br>
						<ul>
							<li>No information or representation made or given by or to any person shall be binding on
						the Company unless it is in writing and is presented and approved by the Company.</li>
							<li>All written information, whether or not written by my own hand, submitted by me / us in this
						application form and in the Company issued questionnaires or other documents submitted by
						me / us in connection with this application	are true, complete and correct. In respect of any information
						or answers that I / we did not provide personally, I / we have checked their contents to ensure
						that they are true, correct and complete. I / We understand that the Company, believing them
						to be such, will rely and act on them, otherwise any policy issued hereunder may be void.</li>
							<li>All information and documents provided by me / us (as defined under (b)) together with the
						relevant policy issued shall constitute the entire contract between myself / ourselves and the
						Company.</li>
						</ul>
								
					</div>
				</div>
				<div class="panel-bottom">
					<div class="input-wrapper-block-top">
						<div class="input-panel-left">
							<div class="squaredFour">
								<input type="checkbox" id="squaredFour3" name="jform[agreePolicyDeclaration]" value="true" required/>
								<label for="squaredFour3"></label>
							</div>
						</div>
						<div class="input-panel-right" style="width:auto;">
							<div class="input-h2">
								I / We have read, understood and accepted the above statements which apply to all person covered under this policy.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="motor-btn-wrapper">
			<div class="motor-btn-group">
				<div class="red-text mb20 nopromo-car20">You are going to pay full amount without using a promo code</div>
				<button type="button" class="motor-btn  back-btn mr20 back-5-btn"><span class="white-arrow-left"></span>BACK</button>
				<button id="continue-5-btn" type="button" class="motor-btn continue-5-btn">CONTINUE<span class="white-arrow"></span></button>
			</div>
		</div>

		<form action="<?php echo $this->getComponentRegistry('pdUrl'); ?>" method="POST" id="paymentForm" name="paymentForm" style="display: none;">
			<input type="hidden" name="orderRef" value="">
			<input type="hidden" name="merchantId" value="<?php echo $this->getComponentRegistry('merchantId'); ?>">
			<input type="hidden" name="payType" value="N">
			<input type="hidden" name="amount" value="">
			<input type="hidden" name="currCode" value="<?php echo $this->getComponentRegistry('currCode'); ?>">
			<input type="hidden" name="mpsMode" value="<?php echo $this->getComponentRegistry('mpsMode'); ?>" >
			<input type="hidden" name="successUrl" value="<?php echo MyHelper::jRoute('index.php?option=com_insurediymotor&layout=thankyou'); ?>">
			<input type="hidden" name="failUrl" value="<?php echo MyHelper::jRoute('index.php?option=com_insurediymotor&layout=error'); ?>">
			<input type="hidden" name="cancelUrl" value="<?php echo MyHelper::jRoute('index.php?option=com_insurediymotor&layout=cancelled'); ?>">
			<input type="hidden" name="lang" value="E">
			<input type="hidden" name="payMethod" value="ALL">
			<?php if($this->getComponentRegistry('hashCode')): ?>
				<input type="hidden" name="secureHash" value="<?php echo $this->getComponentRegistry('hashCode'); ?>">		
			<?php endif; ?>
			<input type="hidden" name="remark" value="motor">
			<input type="hidden" name="templateId" value="1">
		</form>
			
		</div>
	</div>
	<div class="clear"></div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>
<div id="dialog-alert-login" style="display:none;" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="loginpopup">
  <div class="modal-dialog" role="document">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="loginpopup">Login and enjoy up to RM20 off</h4>
      </div>
      <div class="modal-body">
	  	<div class="tab-header-container">
			<div class="tab-pane pop-pane pop-loginpopup active" onclick="changePopTab(this);" id="pop-login-0" data-id="pop-login-panel" data-title="Login and enjoy up to RM20 off">
				<span class="tab-pane-title">LOGIN</span>
			</div>
			<div class="tab-pane pop-pane pop-regpopup" onclick="changePopTab(this);" id="pop-register-1" data-id="pop-register-panel" data-title="Register and enjoy up to RM20 off"> 
				<span class="tab-pane-title">REGISTER</span>
			</div>
		</div>
		<div class="tab-content-container">
			<div class="tab-wrapper pop-wrapper" id="pop-login-panel">
				<form id="login-form-popup-final">
				<div class="insurediy-form">
					<div class="input-wrapper">
						<span class="input-title">Email</span>
						<div class="input-container-block">
							<input type="text" class="input-text" style="width:100%;" name="username" />
						</div>
					</div>
					<div class="input-wrapper">
						<span class="input-title">Password</span>
						<div class="input-container-block">
							<input type="password" class="input-text w-100" name="password" value="" />
							<input type="hidden" name="option" value="com_insurediymotor" />
							<input type="hidden" name="task" value="user.ajaxLogin" />
							<input type="hidden" name="return" value="" />
							<?php echo JHtml::_('form.token'); ?>
						</div>
					</div>
				</div>
				<div class="motor-btn-wrapper">
					<div class="motor-btn-group">
						<button type="submit" id="submit_button" class="motor-btn validate">LOGIN</button>
					</div>
				</div>
				</form>
			</div>
			<div class="tab-wrapper pop-wrapper display-none" id="pop-register-panel">
				<div class="insurediy-form">
					<form id="register-form-popup-final">
					<div class="input-wrapper">
						<span class="input-title">Email</span>
						<div class="input-container-block">
							<input type="text" class="input-text w-100" name="email" />
						</div>
					</div>
					<div class="input-wrapper">
						<span class="input-title">Password</span>
						<div class="input-container-block">
							<input type="password" class="input-text w-100" name="password" value="" />
						</div>
					</div>
					<div class="input-wrapper">
						<span class="input-title">Confirm Password</span>
						<div class="input-container-block">
							<input type="password" class="input-text w-100" name="password2" value="" />
							<input type="hidden" name="option" value="com_insurediymotor" />
							<input type="hidden" name="task" value="user.ajaxRegister" />
							<input type="hidden" name="return" value="" />
							<?php echo JHtml::_('form.token'); ?>
						</div>
					</div>
					<div class="motor-btn-wrapper">
						<div class="motor-btn-group">
							<button type="submit" id="submit_button_register" class="motor-btn validate">REGISTER</button>
						</div>
					</div>
					<div class="red-text no-continue">No thanks, I will pay full amount.</div>
					</form>
				</div>
			</div>
		</div>	
      </div>
  </div>
</div>

<script>
// correct root url
var BASE_URL = '<?php echo MyHelper::rootUrl(); ?>';

</script>

<script type="text/javascript">

	jQuery('.custom-select').select2({ width: '300px' });
	jQuery("#open-register").on("click", function (obj) {
		jQuery(".login-section").slideDown();
	});
	// accordion close and open end

	var dateOfBirth = '<?php echo $this->form->getInput('dateOfBirth '); ?>';

	var disableCarMakeSelectOnChange = false;
	var timeoutLoadCarModels;
	var rootUrl = '<?php echo MyHelper::rootUrl(); ?>';
	var selectedQuote = <?php echo json_encode($this->selectedQuote) ?>;
	var jsOptions = <?php echo $this->jsOptions; ?>;
	var configCurrency = '<?php echo $this->getComponentRegistry('currency'); ?>';
</script>

<script src="<?php echo MyHelper::mix('/js/motor.js'); ?>" type="text/javascript"></script>
