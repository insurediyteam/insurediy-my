<?php
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	JHtml::_('MyBehavior.jsInsurediy');
	JHtml::_('script', 'system/jquery.validate.js', false, true);
	JHtml::_('script', 'system/select2.min.js', false, true);
	JHtml::_('stylesheet', 'system/select2.css', false, true);
	JHtml::_('script', 'system/loadingoverlay.min.js', false, true);
	
	$currency = $this->currency;
	
	$form = & $this->form;
	$fldGroup = "contact-details";
	$data = $this->item;

	$data["country"] = "HK";
	$data["driverNationality"][0] = "HK";
	$data["driverNationality"][1] = "HK";
	$data["driverNationality"][2] = "HK";
	$data["driverNationality"][3] = "HK";
	$data["driverNationality"][4] = "HK";
	$carData = $this->makeModel;
	$bodyType = $this->bodyType;
//	print_r($data);
// 	echo "<br><hr><br>";
// 	print_r($this->result);
	
	if($this->result) {
		if(!$this->result->success) {
			$errorMsg = "";
			
			foreach($this->result["error_message"] as $item) {
				$errorMsg .= $item->errorDesc . "<br>";
			}
			
			JFactory::getApplication()->enqueueMessage(JText::_($errorMsg), 'Warning');
		}
	}
?>
<script>
<?php
if($this->result) {
	if(!$this->result->success) {
		echo "console.log('". json_encode($this->result)."');";
	}
}
?>
</script>
<div class="insurediy-form bb-form montserat-font">
	<div class="motor-header-top-wrapper">
		<?php echo InsureDIYMotorHelper::renderHeader('icon-travel', JText::_('COM_INSUREDIYMOTOR_PAGE_HEADING'), 4); ?>
	</div>
	<div class="edit<?php echo $this->pageclass_sfx; ?> insurediy-motor-form-content">
		<form action="<?php echo JRoute::_('index.php?option=com_insurediymotor&view=form'); ?>" method="post" name="adminForm" id="adminForm">
			<div class="motor-form-input-wrapper">
				<div class="motor-accordion">CAR DETAILS</div>
				<div class="panel">
					<div class="panel-left" style="margin-top:10px">
						<div class="input-half-wrapper">
							<span class="input-title">Type of Body</span>
							<div class="input-container-block">
								<input type="text" class="input-text" name="jform[vehicleBodyType]" value="<?php if(isset($data["carBodyType"])) echo strtoupper($bodyType[$data["carBodyType"]]); ?>" disabled/>
							</div>
						</div>
						<div class="input-half-wrapper">
							<span class="input-title">Cubic Capacity</span>
							<div class="input-container-block">
								<input type="text" class="input-text" name="jform[vehicleCc]" value="<?php if(isset($data["vehicleEngineCc"])) echo $data["vehicleEngineCc"]; ?>" disabled/>
							</div>
						</div>
						<div class="input-half-wrapper">
							<span class="input-title">Engine No.</span>
							<div class="input-container-block">
								<input type="text" class="input-text" name="jform[vehicleEngineNo]" value="<?php if(isset($data["vehicleEngineNo"])) echo $data["vehicleEngineNo"]; ?>" required/>
							</div>
						</div>
						<div class="input-half-wrapper">
							<span class="input-title">Chassis No.</span>
							<div class="input-container-block">
								<input type="text" class="input-text" name="jform[vehicleChassisNo]" value="<?php if(isset($data["vehicleChassisNo"])) echo $data["vehicleChassisNo"]; ?>" required/>
							</div>
						</div>
						<?php if($data["NCDPoints"] > 0): ?>
							<div class="input-wrapper">
								<span class="input-title">Previous Policy No.</span>
								<span class="small-note">Input NA if this is a new car with no previous insurance policy</span>
								<div class="input-container-block">
									<input type="text" class="input-text" name="jform[previousPolicyNo]" value="<?php if(isset($data["previousPolicyNo"])) echo $data["previousPolicyNo"]; ?>" required/>
								</div>
							</div>
						<?php else: ?>
							<input type="hidden" name="jform[previousPolicyNo]" value="NA" />
						<?php endif; ?>
					</div>
					<div class="panel-right">
						<div class="input-wrapper">
							<span class="input-title" style="margin:0;">Seating Capacity</span><span style="font-size:10px; font-weight:bold;"> (including Driver)</span>
							<div class="input-container-block">
								<input type="text" class="input-text" style="width:66%;" name="jform[vehicleSeatingCapacity]" value="<?php if(isset($carData->passenger)) {echo $carData->passenger;} else { echo "Not Applicable"; }?>" disabled/>
							</div>
						</div>
						<div class="input-wrapper" style="height: 94px;">
						</div>
						<?php if($data["NCDPoints"] > 0): ?>
							<div class="input-wrapper">
								<span class="input-title">Previous Car Registration No.</span>
								<span class="small-note">Input NA if this is a new car with no previous car registration number</span>
								<div class="input-container-block">
									<input type="text" class="input-text" name="jform[previousRegNo]" style="height: 35px!important;" value="<?php if(isset($data["previousRegNo"])) echo $data["previousRegNo"]; ?>" required />
								</div>
							</div>
						<?php else: ?>
							<input type="hidden" name="jform[previousRegNo]" value="NA" />
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="motor-form-input-wrapper">
				<div class="motor-accordion">DRIVER DETAILS</div>
				<div class="panel">
					<div class="tab-header-container" style="width:100%; margin-bottom:10px;">
						<?php for($i=1; $i<= count($data["Gender"]); $i++):?>
							<?php if($i == 1):?>
								<div class="tab-pane active"  onClick="changeTab(this);" id="driver-0" data-id="driver-panel-0">
									<img class="tab-pane-image" src="images/websiteicon/driver-logo.png" />
									<span class="tab-pane-title">MAIN DRIVER</span>
								</div>
							<?php else: ?>
								<div class="tab-pane"  onClick="changeTab(this);" id="driver-<?php echo $i-1; ?>" data-id="driver-panel-<?php echo $i-1; ?>">
									<img class="tab-pane-image" src="images/websiteicon/driver-logo.png" />
									<span class="tab-pane-title">DRIVER <?php echo $i; ?></span>
								</div>
							<?php  endif; ?>
						<?php endfor; ?>
					</div>
					<div class="tab-content-container driver-container" style="margin-bottom:20px;">
						<?php for($i=0; $i< count($data["Gender"]); $i++):?>
								<div class="tab-wrapper <?php echo $i>=1 ? "display-none" : ""; ?>" id="driver-panel-<?php echo $i; ?>">
									<div class="input-sub-wrapper-2" style="display: inline-block;width: 40%;margin-right: 80px;vertical-align: top;">
										<span class="input-title">HKID number</span>
										<div class="input-container-block">
											<input type="text" class="input-text" name="jform[Nric][<?php echo $i; ?>]" value="<?php if(isset($data["Nric"][$i])) echo $data["Nric"][$i]; ?>" required/>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="display: inline-block;width: 40%;margin-right: 80px;vertical-align: top;">
										<span class="input-title">First Name</span>
										<div class="input-container-block">
											<input type="text" class="input-text no-commas" name="jform[firstName][<?php echo $i; ?>]" style="" value="<?php if(isset($data["firstName"][$i])) echo $data["firstName"][$i]; ?>" required/>
											<label for="jform[firstName][<?php echo $i; ?>]" class="error" style="display: none;">Comma is not allowed</label>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="display: inline-block;width: 40%;margin-right: 80px;vertical-align: top;">
										<span class="input-title">Last Name</span>
										<div class="input-container-block">
											<input type="text" class="input-text no-commas" name="jform[lastName][<?php echo $i; ?>]" style="" value="<?php if(isset($data["lastName"][$i])) echo $data["lastName"][$i]; ?>" required/>
											<label for="jform[lastName][<?php echo $i; ?>]" class="error" style="display: none;">Comma is not allowed</label>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="display: inline-block;width: 40%;margin-right: 80px;vertical-align: top;">
										<span class="input-title">Email</span>
										<div class="input-container-block">
											<input type="text" style="width: 83%;" class="input-text" name="jform[Email][<?php echo $i; ?>]" value="<?php if(isset($data["Email"][$i])) echo $data["Email"][$i]; ?>" required/>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="display: inline-block;width: 40%;margin-right: 80px;vertical-align: top;">
										<span class="input-title">What is your nationality?</span>
										<div class="insurediy-motor-custom-dropdown">
											<select name="jform[driverNationality][<?php echo $i; ?>]" class="custom-select" required>
												<?php 
													$countryList = $this->form->getInput('country_list');
													
													foreach($countryList as $country) {
														foreach($country as $key => $val) {
															if(isset($data["driverNationality"][$i])){ 
																if($data["driverNationality"][$i]== $key) {
 																	echo '<option value="' . $key . '" selected="selected">' . $val . '</option>';
 																}
 																else {
 																	echo '<option value="' . $key . '">' . $val . '</option>';
 																}
 															}
 															else {
 																echo '<option value="' . $key . '">' . $val . '</option>';
 															}
														}
														
													}
												?>
											</select>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="display: inline-block;width: 40%;margin-right: 80px;vertical-align: top;">
										<span class="input-title">Total driving-offence points in the last 3 years?</span>
										<div class="input-container-block">
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[offencePoint][<?php echo $i; ?>]" onchange="" onclick="return false;" id="" class="hide-select custom-select">
													<option value="0" <?php if(isset($data["offencePoint"][$i]) && $data["offencePoint"][$i] == "0") echo "selected"; ?>>0</option>
													<option value="3" <?php if(isset($data["offencePoint"][$i]) && $data["offencePoint"][$i] == "3") echo "selected"; ?>>3</option>
													<option value="5" <?php if(isset($data["offencePoint"][$i]) && $data["offencePoint"][$i] == "5") echo "selected"; ?>>5</option>
													<option value="6" <?php if(isset($data["offencePoint"][$i]) && $data["offencePoint"][$i] == "6") echo "selected"; ?>>6</option>
													<option value="8" <?php if(isset($data["offencePoint"][$i]) && $data["offencePoint"][$i] == "8") echo "selected"; ?>>8</option>
													<option value="99" <?php if(isset($data["offencePoint"][$i]) && $data["offencePoint"][$i] == "99") echo "selected"; ?>>Above 8</option>
												</select>
											</div>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="display: inline-block;width: 40%;margin-right: 80px;vertical-align: top;">
										<span class="input-title" style="margin-top: 10px;">Highest driving-offence point in the last 3 years?</span>
										<div class="input-container-block">
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[highestOffencePoint][<?php echo $i; ?>]" onchange="" onclick="return false;" id="" class="hide-select custom-select">
													<option value="0" <?php if(isset($data["highestOffencePoint"][$i]) && $data["highestOffencePoint"][$i] == "0") echo "selected"; ?>>0</option>
													<option value="3" <?php if(isset($data["highestOffencePoint"][$i]) && $data["highestOffencePoint"][$i] == "3") echo "selected"; ?>>3</option>
													<option value="5" <?php if(isset($data["highestOffencePoint"][$i]) && $data["highestOffencePoint"][$i] == "5") echo "selected"; ?>>5</option>
													<option value="8" <?php if(isset($data["highestOffencePoint"][$i]) && $data["highestOffencePoint"][$i] == "8") echo "selected"; ?>>8</option>
													<option value="10" <?php if(isset($data["highestOffencePoint"][$i]) && $data["highestOffencePoint"][$i] == "10") echo "selected"; ?>>10</option>
													<option value="99" <?php if(isset($data["highestOffencePoint"][$i]) && $data["highestOffencePoint"][$i] == "99") echo "selected"; ?>>Above 10</option>
												
												</select>
											</div>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="display: inline-block;width: 40%;margin-right: 80px;vertical-align: top;">
										<span class="input-title" style="margin-top: 10px;">Any claims in the last 3 years?</span>
										<div class="input-container-block">
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[claimsPast3Years][<?php echo $i; ?>]" onchange="" onclick="return false;" id="" class="hide-select custom-select past3yrs">
													<option value="0" <?php if(isset($data["claimsPast3Years"][$i])){ if($data["claimsPast3Years"][$i] == 0) echo "selected"; } ?>>0</option>
													<option value="1" <?php if(isset($data["claimsPast3Years"][$i])){ if($data["claimsPast3Years"][$i] == 1) echo "selected"; } ?>>1</option>
													<option value="2" <?php if(isset($data["claimsPast3Years"][$i])){ if($data["claimsPast3Years"][$i] == 2) echo "selected"; } ?>>2</option>
													<option value="3" <?php if(isset($data["claimsPast3Years"][$i])){ if($data["claimsPast3Years"][$i] == 3) echo "selected"; } ?>>3</option>
													<option value="4" <?php if(isset($data["claimsPast3Years"][$i])){ if($data["claimsPast3Years"][$i] == 4) echo "selected"; } ?>>4</option>
													<option value="5" <?php if(isset($data["claimsPast3Years"][$i])){ if($data["claimsPast3Years"][$i] == 5) echo "selected"; } ?>>5</option>													
												</select>
											</div>
										</div>
									</div>
									<div class="input-sub-wrapper-2 claimAmtWrapper" style="display: inline-block;width: 40%;margin-right: 80px;vertical-align: top;<?php if(isset($data["claimsPast3Years"][$i])){ if($data["claimsPast3Years"][$i] == 0) echo 'display:none'; }else { echo 'display:none';}?>" >
										<span class="input-title" style="margin-top: 10px;">Total Amount of Claim</span>
										<div class="input-container-block">
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[claimAmount][<?php echo $i; ?>]" onchange="" onclick="return false;" id="" class="hide-select custom-select">
													<option value="0" <?php if(!isset($data["claimAmount"][$i])){ echo 'selected="selected"'; }else{ if($data["claimAmount"][$i]== 0) echo 'selected="selected"'; }?>>0</option>
													<option value="5000" <?php if(isset($data["claimAmount"][$i])){ if($data["claimAmount"][$i]== 5000) echo 'selected="selected"'; } ?>>&lt;=5000</option>
													<option value="10000" <?php if(isset($data["claimAmount"][$i])){ if($data["claimAmount"][$i]== 10000) echo 'selected="selected"'; } ?>>&lt;=10000</option>
													<option value="30000" <?php if(isset($data["claimAmount"][$i])){ if($data["claimAmount"][$i]== 30000) echo 'selected="selected"'; } ?>>&lt;=30000</option>
													<option value="60000" <?php if(isset($data["claimAmount"][$i])){ if($data["claimAmount"][$i]== 60000) echo 'selected="selected"'; } ?>>&lt;=60000</option>
													<option value="60001" <?php if(isset($data["claimAmount"][$i])){ if($data["claimAmount"][$i]== 60001) echo 'selected="selected"'; } ?>>&gt;60000</option>
												</select>
											</div>
										</div>
									</div>
									
								</div>
						<?php endfor; ?>
						
						
					</div>
				</div>
			</div>
			<div class="motor-form-input-wrapper">
				<div class="motor-accordion">POLICYHOLDER DETAILS</div>
				<div class="panel" style="margin-bottom:20px;">
					<div class="input-wrapper-block-top">
						<span class="input-title">Policy holders / Main Driver</span>
						<div class="insurediy-motor-custom-dropdown">
							<input type="hidden" name="jform[driverApplicant]" class="input-text" value="0" />
						</div>
					</div>	
					<div class="input-sub-wrapper-2">
						<span class="input-title">Block No.</span>
						<div class="input-container-block">
							<input type="text" class="input-text" name="jform[blockNo]" style="height: 35px!important;" value="<?php if(isset($data["blockNo"])) echo $data["blockNo"]; ?>" required maxlength="10"/>
						</div>
					</div>
					<div class="input-sub-wrapper-2">
						<span class="input-title">Street Name</span>
						<div class="input-container-block">
							<input type="text" class="input-text" name="jform[streetName]" style="height: 35px!important;" value="<?php if(isset($data["streetName"])) echo $data["streetName"]; ?>" required maxlength="40"/>
						</div>
					</div>
					<div class="input-sub-wrapper-2">
						<span class="input-title">Unit No.</span>
						<div class="input-container-block">
							<input type="text" class="input-text" name="jform[unitNo]" style="height: 35px!important;" value="<?php if(isset($data["unitNo"])) echo $data["unitNo"]; ?>" required maxlength="10"/>
						</div>
					</div>
					<div class="input-sub-wrapper-2">
						<span class="input-title">Building Name</span>
						<div class="input-container-block">
							<input type="text" class="input-text" name="jform[buildingName]" style="height: 35px!important;" value="<?php if(isset($data["buildingName"])) echo $data["buildingName"]; ?>" />
						</div>
					</div>
					<div class="input-sub-wrapper-2" style="display:none;">
						<span class="input-title">Postal Code</span>
						<div class="input-container-block">
							<input type="text" class="input-text" name="jform[zipCode]" style="height: 35px!important;" value="0" required maxlength="6"/>
						</div>
					</div>	
					<div class="input-sub-wrapper-2">
						<span class="input-title">Country</span>
						<div class="insurediy-motor-custom-dropdown" style="margin-top: -4px;">
							<select name="jform[country]" class="custom-select" required>
								<?php 
									$countryList = $this->form->getInput('country_list');
									print_r($countryList);
									
									foreach($countryList as $country) {
										foreach($country as $key => $val) {
											if(isset($data["country"])){
												if($data["country"]== $key) {
													echo '<option value="' . $key . '" selected="selected">' . $val . '</option>';
												}
												else {
													echo '<option value="' . $key . '">' . $val . '</option>';
												}
											}
											else {
												echo '<option value="' . $key . '">' . $val . '</option>';
											}
										}
										
									}
								?>
							</select>
							<div class="error-container"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="motor-form-input-wrapper">
				<div class="motor-accordion">FINANCIAL DETAILS</div>
				<div class="panel">
					<div class="panel-bottom">
						<div class="input-sub-wrapper">
							<span class="input-title">Are you under financial loan?</span>
							<br>
							<div class="input-container-inline">
								<input type="radio" id="radio-workshop-0" class="input-radio" name="jform[loan]" <?php if(isset($data["loan"])){ if($data["loan"] == "Yes") echo "checked"; } ?> value="Yes" onclick="radioClicked(this);" required/>
								<label class="radio-label" for="radio-workshop-0">
									<span class="radio-label-title <?php if(isset($data["loan"])){ if($data["loan"] == "Yes") echo "active";} ?>">Yes</span>
								</label>
							</div>
							<div class="input-container-inline">
								<input type="radio" id="radio-workshop-1" class="input-radio" name="jform[loan]" <?php if(isset($data["loan"])){ if($data["loan"] == "No") echo "checked"; } ?>  value="No" onclick="radioClicked(this);" required/>
								<label class="radio-label" for="radio-workshop-1">
									<span class="radio-label-title <?php if(isset($data["loan"])){ if($data["loan"] == "No") echo "active";} ?>">No</span>
								</label>
							</div>
						</div>
						<div class="input-sub-wrapper" id="financial-loan" <?php if(isset($data["loan"])){ if($data["loan"] == "No") echo 'style="display:none;"'; } ?>>
							<span class="input-title">Financial Loan Company</span>
							<div class="input-container-block">
								<select name="jform[loanCompany]" onchange="" onclick="return false;" id="" class="hide-select custom-select">
								<option value="" selected>Select Loan Company</option>
								<?php foreach($this->loanCompany as $key => $val):?>
									<option value="<?php echo $val->mortgagee_code;?>" <?php if(isset($data["loanCompany"])){ if($val->mortgagee_code == $data["loanCompany"]) echo 'selected="selected"'; } ?>><?php echo $val->mortgagee_name;?></option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>	
					</div>
				</div>
			</div>
			<input type="hidden" name="task" value="form.step4save" />
			<input type="hidden" name="quotation_id" value="<?php //echo $quotation['id']; ?>" />
			
			<?php echo JHtml::_('form.token'); ?>
			
			<?php echo InsureDIYMotorHelper::renderProceedButton(true);?>
			
			<div class="clear"></div>
		</form>
	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>

<?php /* Popup Box */ ?>
<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
	<div class="header">
		<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div><?php echo JText::_("COM_INSUREDIYMOTOR_NAME_CHANGE_NOTICE_MSG"); ?></div>
		<div style="text-align:right;"><input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
			return f
		alse;" value="<?php echo JText::_("JOK"); ?>" /></div>
	</div>
</div>

<script>
jQuery(document).ready(function(){
	jQuery('.custom-select').select2({width: '180px'});
	jQuery('.driver-container .custom-select').select2({width: '343px'});
	jQuery('.driver-container input').css("width", "343px");
	
	jQuery(".past3yrs").change(function(){
	var thisParent = jQuery(this).parent().parent().parent();
	var thisAmount = jQuery(thisParent).next();
	if(jQuery(this).val() == 0) {
		var firstOption = jQuery("select[name='jform[claimAmount]']").find("option:first-child").html();
		jQuery("select[name='jform[claimAmount]']").prop('selectedIndex',0);
		jQuery("select[name='jform[claimAmount]']").next().find(".select2-selection__rendered").html(firstOption);
		
		jQuery(thisAmount).css("display", "none");
	}

	if(jQuery(this).val() > 0) {
		jQuery(thisAmount).css("display", "block");
	}
});
});

var formValidator = jQuery("form#adminForm").validate();

var acc = document.getElementsByClassName("motor-accordion");
var i;

for (i = 0; i < acc.length; i++) {
	acc[i].nextElementSibling.style.maxHeight = (acc[i].nextElementSibling.scrollHeight + 500) + "px";
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = (panel.scrollHeight + 500) + "px";
    } 
  }
}

jQuery.LoadingOverlaySetup({
    color           : "rgba(0, 0, 0, 0.4)"
});

var customElement = jQuery('\
		<div class="overlay-container">\
		<div class="overlay-content-top">\
			<img class="overlay-content-img" src="<?php echo JURI::base(); ?>/images/websiteicon/company-logo.png"/>\
		</div>\
		<div class="overlay-content-middle">\
			<div class="overlay-content-header">\
				<span class="overlay-content-title">Thank you for your patience, we are consolidating your details. <br><br></span>\
				<span class="overlay-content-brand">Please wait a moment . . .</span>\
			</div>\
		</div>\
		<div class="overlay-content-bottom">\
			<div class="spinner">\
			  <div></div>\
			  <div></div>\
			  <div></div>\
			  <div></div>\
			  <div></div>\
			</div>\
		</div>\
	</div>\
');

function validateNRIC(str, type) {
    if (str.length != 9) 
        return false;

    str = str.toUpperCase();

    var i, 
        icArray = [];
    for(i = 0; i < 9; i++) {
        icArray[i] = str.charAt(i);
    }

    icArray[1] = parseInt(icArray[1], 10) * 2;
    icArray[2] = parseInt(icArray[2], 10) * 7;
    icArray[3] = parseInt(icArray[3], 10) * 6;
    icArray[4] = parseInt(icArray[4], 10) * 5;
    icArray[5] = parseInt(icArray[5], 10) * 4;
    icArray[6] = parseInt(icArray[6], 10) * 3;
    icArray[7] = parseInt(icArray[7], 10) * 2;

    var weight = 0;
    for(i = 1; i < 8; i++) {
        weight += icArray[i];
    }

    var offset = (icArray[0] == "T" || icArray[0] == "G") ? 4:0;
    var temp = (offset + weight) % 11;

    var st = ["J","Z","I","H","G","F","E","D","C","B","A"];
    var fg = ["X","W","U","T","R","Q","P","N","M","L","K"];

    var theAlpha;
//	    if (icArray[0] == "S" || icArray[0] == "T") { theAlpha = st[temp]; }
//	    else if (icArray[0] == "F" || icArray[0] == "G") { theAlpha = fg[temp]; }

	var icType = type;

	if (icType == "N" || icType == "B") { theAlpha = st[temp]; }
    else if (icType == "E" || icType == "W") { theAlpha = fg[temp]; }

    return (icArray[8] === theAlpha);
}

function validateHkid(str, element) {
		var strValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

		// basic check length
		if (str.length < 8)
			return false;
	
		// handling bracket
		if (str.charAt(str.length-3) == '(' && str.charAt(str.length-1) == ')')
			str = str.substring(0, str.length - 3) + str.charAt(str.length -2);

		// convert to upper case
		str = str.toUpperCase();

		// regular expression to check pattern and split
		var hkidPat = /^([A-Z]{1,2})([0-9]{6})([A0-9])$/;
		var matchArray = str.match(hkidPat);

		// not match, return false
		if (matchArray == null)
			return false;

		// the character part, numeric part and check digit part
		var charPart = matchArray[1];
		var numPart = matchArray[2];
		var checkDigit = matchArray[3];

		// calculate the checksum for character part
		var checkSum = 0;
		if (charPart.length == 2) {
			checkSum += 9 * (10 + strValidChars.indexOf(charPart.charAt(0)));
			checkSum += 8 * (10 + strValidChars.indexOf(charPart.charAt(1)));
		} else {
			checkSum += 9 * 36;
			checkSum += 8 * (10 + strValidChars.indexOf(charPart));
		}

		// calculate the checksum for numeric part
		for (var i = 0, j = 7; i < numPart.length; i++, j--)
			checkSum += j * numPart.charAt(i);

		// verify the check digit
		var remaining = checkSum % 11;
		var verify = remaining == 0 ? 0 : 11 - remaining;

		return verify == checkDigit || (verify == 10 && checkDigit == 'A');
	}

jQuery("form#adminForm").on("submit", function(obj) {
	var firstName = jQuery("[name^=jform\\[firstName\\]]");
	var lastName = jQuery("[id^=jform\\[lastName\\]]");
	var Nric = jQuery("[name^=jform\\[Nric\\]]");
	var Occupation = jQuery("[name^=jform\\[Occupation\\]]");
	var Email = jQuery("[name^=jform\\[Email\\]]");
	var race = jQuery("[name^=jform\\[driverRace\\]]");
	
	flag = validateField(firstName);
	flag = validateField(lastName);
	flag = validateField(Nric);
	flag = validateField(Occupation);
	flag = validateField(Email);
	flag = validateField(race);
	flag = validateRules();

	if(flag && formValidator.numberOfInvalids() == 0) {
		
		jQuery.LoadingOverlay("show", {
	        image   : "",
	        custom  : customElement
	    });
	}
	
	return flag;
});

jQuery("[name^=jform\\[loan\\]]").on("click", function(obj) {
	$value = jQuery(this).val();

	if($value == "Yes") {
		jQuery("#financial-loan").css('display', "inline-block");
	}
	else {
		var firstOption = jQuery("[name^=jform\\[loanCompany\\]]").find("option:first-child").html();
		
		jQuery("#financial-loan").css('display', "none");
		jQuery("[name^=jform\\[loanCompany\\]]").prop('selectedIndex',0);
		jQuery("[name^=jform\\[loanCompany\\]]").next().find(".select2-selection__rendered").html(firstOption);
	}
});

function validateRules() {
	var Nric = jQuery("[name^=jform\\[Nric\\]]");
	var firstName = jQuery("[name^=jform\\[firstName\\]]");
	var lastName = jQuery("[name^=jform\\[lastName\\]]");
	var Email = jQuery("[name^=jform\\[Email\\]]");
	var nricType = jQuery("[name^=jform\\[icType\\]]");
	var nationality = jQuery("[name^=jform\\[driverNationality\\]]");
	var nricList = new Array();
		
	for (var i = 0, len = Nric.length; i < len; i++) {
		var result = validateHkid(jQuery(Nric[i]).val(), false);
		nricList[i] = jQuery(Nric[i]).val();
		
		if(!result) {
			var errors = {};
			var name = jQuery(Nric[i]).attr("name");
			
			errors[name] = "Invalid HKID Format";
			
			formValidator.showErrors(errors);

			jQuery(".tab-pane").removeClass("active");
			jQuery("#driver-"+i).addClass("active");
			jQuery('.tab-wrapper').addClass('display-none');
			jQuery("#driver-panel-"+i).removeClass("display-none");

			jQuery(window).scrollTop((jQuery(Nric[i]).offset().top)-285);
			
			return false;
		}
	}

	for (var i = 0, len = firstName.length; i < len; i++) {
		if(jQuery(firstName[i]).val() == "") {
			var errors = {};
			var name = jQuery(firstName[i]).attr("name");
			
			errors[name] = "Please enter first name";
			
			formValidator.showErrors(errors);

			jQuery(".tab-pane").removeClass("active");
			jQuery("#driver-"+i).addClass("active");
			jQuery('.tab-wrapper').addClass('display-none');
			jQuery("#driver-panel-"+i).removeClass("display-none");

			jQuery(window).scrollTop((jQuery(Nric[i]).offset().top)-285);
			
			return false;
		}
	}

	for (var i = 0, len = lastName.length; i < len; i++) {
		if(jQuery(lastName[i]).val() == "") {
			var errors = {};
			var name = jQuery(lastName[i]).attr("name");
			
			errors[name] = "Please enter last name";
			
			formValidator.showErrors(errors);

			jQuery(".tab-pane").removeClass("active");
			jQuery("#driver-"+i).addClass("active");
			jQuery('.tab-wrapper').addClass('display-none');
			jQuery("#driver-panel-"+i).removeClass("display-none");

			jQuery(window).scrollTop((jQuery(Nric[i]).offset().top)-285);
			
			return false;
		}
	}

	for (var i = 0, len = Email.length; i < len; i++) {
		if(jQuery(Email[i]).val() == "") {
			var errors = {};
			var name = jQuery(Email[i]).attr("name");
			
			errors[name] = "Please enter email";
			
			formValidator.showErrors(errors);

			jQuery(".tab-pane").removeClass("active");
			jQuery("#driver-"+i).addClass("active");
			jQuery('.tab-wrapper').addClass('display-none');
			jQuery("#driver-panel-"+i).removeClass("display-none");

			jQuery(window).scrollTop((jQuery(Nric[i]).offset().top)-285);
			
			return false;
		}
	}
	
	if(jQuery("[name^=jform\\[loan\\]]:checked").val() == "Yes") {
		if(jQuery("[name^=jform\\[loanCompany\\]]").val() == "" || jQuery("[name^=jform\\[loanCompany\\]]").val() == null) {					
			var errors = {};
			var name = jQuery("[name^=jform\\[loanCompany\\]]").attr("name");
			
			errors[name] = "Please fill in company name";
			
			formValidator.showErrors(errors);
			
			return false;
		}
	}

	return true;
}

function checkDuplicate(list) {
	for (var i = 0, len = list.length; i < len; i++) {
		var temp = list.slice();

		delete temp[i];
		
		var result = temp.indexOf(list[i]);

		if(result != -1) {
			return true;
		}
	}

	return false;
}

function validateField(Obj) {
	var flag = "";
	var counter = 0;
	for (var i = 0, len = Obj.length; i < len; i++) {
		
		if(jQuery(Obj[i]).attr('type') == "radio") {
			if(jQuery(Obj[i]).prop("checked")) {
				flag = jQuery(Obj[i]).attr("name");
			}
			else {
				if(jQuery(Obj[i]).attr("name") == flag) {
					flag = "";
					counter = 0;
				}
				else {
					counter = jQuery(Obj[i]).attr("name").replace("jform[Gender][", "").replace("]", "");
				}
			}
		}
		else {
			if(jQuery(Obj[i]).val() == "" || jQuery(Obj[i]).val() == null) {					
				jQuery(".tab-pane").removeClass("active");
				jQuery("#driver-"+i).addClass("active");
				jQuery('.tab-wrapper').addClass('display-none');
				jQuery("#driver-panel-"+i).removeClass("display-none");
				
				return false;
			}
		}
	}

	if(jQuery(Obj[0]).attr('type') == "radio") {
		jQuery(".tab-pane").removeClass("active");
		jQuery("#driver-"+counter).addClass("active");
		jQuery('.tab-wrapper').addClass('display-none');
		jQuery("#driver-panel-"+counter).removeClass("display-none");

		return false;
	}
	return true;
}

// var next = document.getElementById("radio-next-button");

// next.onclick = function() {
// 	document.getElementById("year-block-1").classList.remove("display-block");
// 	document.getElementById("year-block-1").classList.add("display-none");
// 	document.getElementById("year-block-2").classList.remove("display-none");
// 	document.getElementById("year-block-2").classList.add("display-block");
// }

// var back = document.getElementById("radio-back-button");

// back.onclick = function() {
// 	document.getElementById("year-block-2").classList.remove("display-block");
// 	document.getElementById("year-block-2").classList.add("display-none");
// 	document.getElementById("year-block-1").classList.remove("display-none");
// 	document.getElementById("year-block-1").classList.add("display-block");
// }

function radioClicked(obj) {
	var otherRadios = document.getElementsByName(obj.getAttribute('name'));
	var j;
	
	if(jQuery(obj).parent().find('label[class^="radio-label"]').hasClass("radio-label-logo")) {
		for(j=0;j < otherRadios.length; j++) {
			jQuery(otherRadios[j]).parent().find('label[class^="radio-label"]').removeClass("active");
		}

		jQuery(obj).parent().find('label[class^="radio-label"]').toggleClass("active");
	}
	else {
		for(j=0;j < otherRadios.length; j++) {
			jQuery(otherRadios[j]).parent().find('span.radio-label-title').removeClass("active");
		}

		jQuery(obj).parent().find('span.radio-label-title').toggleClass("active");
	}
	
	
//		if(obj.nextElementSibling.children[0].classList[0].indexOf("icon") !== -1) {
//			for(j=0;j < otherRadios.length; j++) {
//				otherRadios[j].nextElementSibling.classList.remove("active");
//			}
		
		
//		}
//		else {
//			for(j=0;j < otherRadios.length; j++) {
//				otherRadios[j].nextElementSibling.children[0].classList.remove("active");
//			}
		
//			obj.nextElementSibling.children[0].classList.toggle("active");
//		}
}

function changeTab(obj) {
	jQuery(".tab-pane").removeClass("active");
	jQuery(obj).addClass("active");
	jQuery('.tab-wrapper').addClass('display-none');
	jQuery("#"+jQuery(obj).attr("data-id")).removeClass("display-none");
}

function pushHeight(obj) {
	var panelHeight = jQuery(obj).parents().eq(4).height();
	var height = jQuery(obj).parent().find(".chzn-drop").height();
	var toggleFlag = jQuery(obj).parent().find(".chzn-single").attr("data-extend");
	
	if(typeof toggleFlag == "undefined") {
		jQuery(obj).attr("data-extend", "false");
		toggleFlag = jQuery(obj).parent().find(".chzn-single").attr("data-extend");
	}

	if(toggleFlag == "false") {
		jQuery(obj).parents().eq(4).animate({height:(panelHeight+height)},200);
		jQuery(obj).parent().find(".chzn-single").attr("data-extend", "true");
	}
	else {
		jQuery(obj).parents().eq(4).animate({height:(panelHeight-height)},200);
		jQuery(obj).parent().find(".chzn-single").attr("data-extend", "false");
	}	
}

jQuery(document).on("click", function(obj){
	var dropdown = jQuery(event.target).parent().attr('class');
	var dropdownObj = jQuery(event.target).closest('.chzn-single');
	
	if(jQuery(dropdownObj).attr('class') == "chzn-single") {
		var panelHeight = jQuery(dropdownObj).closest('.panel').height();
		var height = jQuery(dropdownObj).siblings(".chzn-drop").height();
		var toggleFlag = jQuery(dropdownObj).attr("data-extend");
		
		if(typeof toggleFlag == "undefined") {
			jQuery(dropdownObj).attr("data-extend", "false");
			toggleFlag = jQuery(dropdownObj).attr("data-extend");
		}

		if(toggleFlag == "false") {
			jQuery(dropdownObj).closest('.panel').animate({height:(panelHeight+height)},200);
			jQuery(dropdownObj).attr("data-extend", "true");
		}
		else {
			jQuery(dropdownObj).closest('.panel').animate({height:(panelHeight-height)},200);
			jQuery(dropdownObj).attr("data-extend", "false");
		}	
		//jQuery(dropdownObj).parents().eq(5).animate({height:(height+200)},200);
	}

	if(dropdown == "chzn-results") {
		dropdownObj = jQuery(event.target).parents().eq(2);

		var panelHeight = jQuery(dropdownObj).parents().eq(4).height();
		var height = jQuery(dropdownObj).find(".chzn-drop").height();
		var toggleFlag = jQuery(dropdownObj).find(".chzn-single").attr("data-extend");
		
		if(typeof toggleFlag == "undefined") {
			jQuery(dropdownObj).find(".chzn-single").attr("data-extend", "false");
			toggleFlag = jQuery(dropdownObj).find(".chzn-single").attr("data-extend");
		}

		if(toggleFlag == "false") {
			jQuery(dropdownObj).parents().eq(4).animate({height:(panelHeight+height)},200);
			jQuery(".chzn-single").attr("data-extend", "false");
			jQuery(dropdownObj).find(".chzn-single").attr("data-extend", "true");
		}
		else {
			jQuery(dropdownObj).parents().eq(4).animate({height:(panelHeight-height)},200);
			jQuery(".chzn-single").attr("data-extend", "false");
		}	
	}
});

jQuery(".no-commas").on('keydown', function(e){
    if (!e) var e = window.event;
     if (e.keyCode  ==  '188' )
     {
    	 jQuery(this).next().html("Comma is not allowed");
         jQuery(this).next().css("display", "block");
         return false;
     }
});
</script>

<script>
	dataLayer.push({
		'event': 'checkoutOption',
		'ecommerce': {
			'checkout': {
				'actionField': {'step': 1}
			}
		}
	});
	dataLayer.push({
		'event': 'checkoutOption',
		'ecommerce': {
			'checkout': {
				'actionField': {'step': 2}
			}
		}
	});
</script>
