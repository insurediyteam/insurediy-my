<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediymotor
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

use Carbon\Carbon;
use Concerns\InteractsWithJConfig;
use Concerns\QuoteSessionTrait;
use Models\Eloquent\Occupation;

defined('_JEXEC') or die;

/**
 * HTML Article View class for the component
 *
 * @package     Joomla.Site
 * @subpackage  com_insurediymotor
 * @since       1.5
 */
class InsureDIYMotorViewForm extends JViewLegacy {

	use QuoteSessionTrait, InteractsWithJConfig;

	protected $form;
	protected $item;
	protected $state;
	protected $result;
	protected $makeModel;
	protected $plans;
	protected $carData;
	protected $loanCompany;
	/** @var \Joomla\CMS\User\User */
	protected $user;
	protected $jsOptions;
	protected $paymentUrl;
	/**
	 * Current quotation data
	 *
	 * @var array
	 */
	protected $currentQuote;
	/**
	 * current selected insurer | quote summary | selected car model per insurer
	 *
	 * @var array
	 */
	protected $selectedQuote;

	public function display($tpl = null) {
		$session = JFactory::getSession();
		$model = $this->getModel();
		$params = JComponentHelper::getParams('com_insurediymotor');
		//$mailchimp = JComponentHelper::getParams('com_joomailermailchimpintegration');
		$user = JFactory::getUser();

		/** @var \Joomla\CMS\Form\Form $form */
		$form = $this->get("Form");
		//$quotation = InsureDIYMotorHelper::getQuotation();
		//$quotation_id = InsureDIYMotorHelper::getQid();
		$currentUser = MyHelper::getCurrentUser();

		$this->currentQuote = $this->currentQuote($session);
		if($this->currentQuote) {
			$this->currentQuote->load([
				'model',
				'body_type',
				'drivers',
				'drivers.occupation',
				'loan_company',
			]);
		}
		
		$this->selectedQuote = $this->selectedQuote($session);

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseWarning(500, implode("\n", $errors));
			return false;
		}
		
		$step = JFactory::getApplication()->input->get("step", FALSE);

		$layout = InsureDIYMotorHelper::getLayout();
		
		//$this->quotation = $quotation;
		$this->setLayout($layout);
		$this->form = $form;
		$this->state = $this->get('State');
		$this->currency = $params->get("currency", "MYR");
		$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
		$this->params = $params;
		$this->user = $user;
		$this->current_user = $currentUser;
		$this->flag_banner_path = 'images/flag_banners';
		$this->_prepareDocument();
		
		switch($layout) {
			case "thankyou":
				$this->item = $session->get("motor.data", FALSE);
				$this->result = $session->get("motor.result", FALSE);
				// $this->docs = $model->getDriverDocument($user->id);

				if($this->currentQuote && $this->currentQuote->step == 5 && $this->currentQuote->car_log_document) {
					$session->set(QuoteSessionTrait::sessionCurrentQuoteId(), null);
					$session->set(QuoteSessionTrait::sessionSelectedQuoteId(), null);
				}
				else {

				}
			case "cancelled":
				$this->result = $session->get("motor.result", FALSE);
				//InsureDIYMotorHelper::clearSessionData();
			break;	
		}

		$this->jsOptions = json_encode([
			'datepicker' => [
				'dob' => [
					"yearRange" => Carbon::today()->subYears(70)->format('Y') . ":" . Carbon::today()->subYears(25)->format('Y'),
					'defaultDate' => "01-JAN-" . Carbon::today()->subYears(35)->format('Y'), 
				],
			],
		]);

		$this->paymentUrl = $this->getConfig('payment_url', 'https://test.paydollar.com/b2cDemo/eng/payment/payForm.jsp');
		
		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 */
	protected function _prepareDocument() {
		$app = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		$head = JText::_('COM_INSUREDIYMOTOR_FORM_PAGE_HEADING');
		
// 		if (empty($this->item->id)) {
// 			$head = JText::_('COM_INSUREDIYMOTOR_FORM_PAGE_HEADING');
// 		} else {
// 			$head = JText::_('COM_INSUREDIYMOTOR_FORM_EDIT_PAGE_HEADING');
// 		}

		if ($menu) {
// 			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
			
			
			$description = $menu->params->get('menu-meta_description');
			
			if ($description) {
				$this->document->setDescription($menu->params->get('menu-meta_description'));
			}
			
			if ($menu->params->get('menu-meta_keywords')) {
				$this->document->setMetadata('keywords', $menu->params->get('menu-meta_keywords'));
			}
			
			if ($menu->params->get('robots')) {
				$this->document->setMetadata('robots', $menu->params->get('robots'));
			}
		}
		
		$this->params->def('page_heading', $head);

		$title = $this->params->def('page_title', $head);
		if ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		} elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		$this->document->setTitle($title);

		//facebook stuffs
		$this->document->setMetaData("og:title", InsureDIYMotorHelper::getFbTitle());
		$this->document->setMetaData("og:description", InsureDIYMotorHelper::getFbDesc());
		$this->document->setMetaData("og:image", InsureDIYMotorHelper::getFbImage());
		$this->document->setMetaData("og:app_id", InsureDIYMotorHelper::getFbAppId());
		$this->document->setMetaData("og:site_name", InsureDIYMotorHelper::getFbSiteName());
	}

	protected function getComparisonTbRows() {
		$rows = array(
		);
		return $rows;
	}

}
