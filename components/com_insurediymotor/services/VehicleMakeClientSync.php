<?php 

namespace Services;

use Models\Eloquent\Make;
use Models\Eloquent\Detailable;
use Models\Eloquent\MakeProvider;
use Services\Client\BaseListData;

class VehicleMakeClientSync extends BaseClientSync {

    protected function reset($switch) {
        if($switch) {
            Make::getQuery()->delete();
            MakeProvider::getQuery()->delete();
            Detailable::where('detailable_type', MakeProvider::class)->delete();
        }
    }

    protected function getMakeList() {
        return $this->getClientVehicle()
            ->makeListData();
    }

    public function createVehicleMake(BaseListData $listData) {
        $make = Make::firstOrCreate(
            [
                'code' => $this->codeSlug($listData->description),
            ],
            [
                'name' => $listData->description,
            ]
        );

        $messageDescription = "make #{$make->id}: {$listData->description}" . PHP_EOL;
        if($make->wasRecentlyCreated) {
            echo "Created new {$messageDescription}";
        }
        else {
            echo "Skipped {$messageDescription}";
        }

        return $make;
    }

    public function createVehicleMakeProvider(Make $vehicleMake, BaseListData $listData) {
        return MakeProvider::firstOrCreate([
                'hash' => $listData->getHash([
                    'make_id' => $vehicleMake->id,
                    'provider_id' => $this->getModelId(),
                ]),
            ], [
                'make_id' => $vehicleMake->id,
                'provider_id' => $this->getModelId(),
                'code' => $listData->value,
                'name' => $listData->description,
            ]);
    }

    public function run() {
        foreach($this->getMakeList() as $make) {
            $vehicleMake = $this->createVehicleMake($make);

            $vehicleMakeProvider = $this->createVehicleMakeProvider($vehicleMake, $make);

            $messageDescription = "make provider ({$this->client->code()}): {$make->description}" . PHP_EOL;
            if($vehicleMakeProvider->wasRecentlyCreated) {
                echo "Created new {$messageDescription}";

                foreach($make->extras as $extra) {
                    $vehicleMakeProvider->detailables()
                        ->create($extra);
                }
            }
            else {
                echo "Skipped {$messageDescription}";
            }
        }
    }

}
