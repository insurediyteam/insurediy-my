<?php 

namespace Services\ChubbClient;

use Services\ChubbClient;
use Services\BaseClientSubclass;
use Services\Client\BaseListData;
use Services\Contracts\Client\Subclass\VehicleContract;

class Vehicle extends BaseClientSubclass implements VehicleContract {

    protected $carModelSelectorPageCount;

    protected $carModelSelectorTableArray;

    public static $quotationAccess = false;

    public function quoteStep1Form() {
        $this->authenticate();

        if(!static::$quotationAccess) {
            $this->getHttpClient()->get(ChubbClient::ROUTE_QUOTE_FORM_STEP1, [
                // 'debug' => true,
            ]);

            static::$quotationAccess = true;
        }
    }

    public function carSelectorRequest($params = []) {
        $this->authenticate();

        $requestOptions = [
            'query' => array_merge([
                'MAINCLS' => 'KHBVMR',
                'SUBCLS' => 'CO',
                'REGION' => 'W',
            ], $params),
            'headers' => [
                'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
            ],
            // 'debug' => true,
        ];

        try {
            $response = $this->getHttpClient()->get(ChubbClient::ROUTE_CAR_MAKE, $requestOptions);
        }
        catch (\Exception $e) {
            /** @var ChubbClient $this */
            $this->logout();

            /** @var self $this */
            return $this->carSelectorRequest($params);
        }

        return $response;
    }

    public function carSelectorDom() {
        $html = (string) $this->carSelectorRequest()->getBody();

        return $this->htmlToDom($html);
    }

    /**
     * @param string
     * @param int
     */
    public function carModelSelectorDom($make, $page = 1) {

        $html = (string) $this->carSelectorRequest([
            'pageNo' => $page,
            'MAKE' => $make,
            'POLMAKE' => '',
            'VIX' => '',
            'MODEL' => '',
            'YEAR' => '',
            'ZCAPCTY' => '',
            'MODEL_DESC' => '',
            'pageRange' => '30',
        ])->getBody();

        $carModelSelectorDom = $this->htmlToDom($html);

        $pageNavDom = $carModelSelectorDom->filter('.eBlink2 a');

        $this->carModelSelectorPageCount = count($pageNavDom);

        $table = $this->parseCarModelTable($carModelSelectorDom);
    }

    public function parseCarModelTable($carModelSelectorDom) {
        $response = $this->carModelSelectorTableRequest($carModelSelectorDom);

        $responseHtml = (string) $response->getBody();

        $dom = $this->htmlToDom($responseHtml);

        $tableDom = $dom->filter('body > div > form > table')->first();

        $this->carModelSelectorTableArray = $this->parser->tableDomToArray($tableDom);

        return $this;
    }

    public function carModelSelectorTableRequest($carModelSelectorDom) {
        $iframeTable = $carModelSelectorDom->filter('#iframeTable')->first();

        $url = $iframeTable->attr('src');

        $response = $this->getHttpClient()->get($url, [
            // 'debug' => true,
        ]);

        return $response;
    }

    public function makeListData() {
        $selectDomObject = $this->carSelectorDom()->filter('select[name=\'MAKE\']')->first();
        
        $data = $this->parser->selectDataFromDom($selectDomObject);

        array_shift($data);

        return array_map(function($value) {
            return new BaseListData($value['value'], $value['text']);
        }, $data);
    }

    public function modelListData($makeCode, $type = null, $cover = null, $use = null) {
        $pageCounter = 1;

        $data = [];

        while(!isset($this->carModelSelectorPageCount) || $pageCounter <= $this->carModelSelectorPageCount) {
            $carModelSelectorDom = $this->carModelSelectorDom($makeCode, $pageCounter);

            $dataMap = [];
            $dataDescriptions = [];

            foreach($this->carModelSelectorTableArray as $key => $carData) {
                if($key === 0) {
                    $dataDescriptions = $carData;
                    $dataMap = array_flip($dataDescriptions);

                    continue;
                }

                $code = null;
                $description = null;
                $extras = [];

                foreach ($carData as $carDataKey => $value) {
                    if(!($value || $dataDescriptions[$carDataKey])) {
                        continue;
                    }

                    switch ($dataDescriptions[$carDataKey]) {
                        case 'Code': 
                            $code = $value;
                        break;

                        case 'Description': 
                            $description = $value;
                        break;

                        default: 
                            array_push($extras, [
                                'value' => $value,
                                'description' => $dataDescriptions[$carDataKey],
                            ]);
                        break;
                    }
                }

                $makeModelListData = new BaseListData(
                    $code,
                    $description,
                    $extras
                );

                array_push($data, $makeModelListData);
            }

            $pageCounter++;
        }

        return $data;
    }

    public function bodyListData() {
        $vehicleClassRequest = $this->vehicleClassDataRequest();

        $vehicleClassDataHtml = (string) $vehicleClassRequest->getBody();

        $data = $this->parser->vehicleBodyData($vehicleClassDataHtml);
        
        array_shift($data);

        return array_map(function($value) {
            return new BaseListData($value['value'], $value['text']);
        }, $data);;
    }

}
