<?php 

namespace Services\ChubbClient;

use Concerns\ParserTrait;
use Illuminate\Support\Str;
use Services\BaseClientSubclass;
use Symfony\Component\DomCrawler\Crawler;

class Parser extends BaseClientSubclass {

    use ParserTrait;

    public $debug = false;

    public static $extraCoverageMaps = [
        [
            'MP-CART/14D/RM50',
            'cart',
        ],
        [
            'Flood Cover / Special Perils',
            'specialPeril',
        ],
        [
            'Legal Liability of Passengers',
            'liabilityPass',
        ],
        // [
        //     'Betterment Buyback',
        //     'waiver',
        // ],
        [
            'Breakage of Windscreen, Tinted',
            'tinting',
        ],
        [
            'Breakage Of Windscreen',
            'laminated',
        ],
    ];


    /**
     * Parse quotation html data to array
     *
     * @param string $htmlString
     * @return array
     */
    public function quotationData($htmlString) {
        $quotationData = [
            'BASICPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['BASICPREM'].value = \"", '";')[0],
            'TRAILERPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['TRAILERPREM'].value = \"", '";')[0],
            'TOTALBASIC' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['TOTALBASIC'].value = \"", '";')[0],
            'TOTALBASICPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['TOTALBASICPREM'].value = \"", '";')[0],
            'LOADAMT' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['LOADAMT'].value = ", ';')[0],
            'NCDAMT' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['NCDAMT'].value = \"", '";')[0],
            'TRANSFER_FEE' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['TRANSFER_FEE'].value = ", ';')[0],
            'APREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['APREM'].value = \"", '";')[0],
            'GPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['GPREM'].value = \"", '";')[0],
            'ACTPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['ACTPREM'].value = \"", '";')[0],
            'TOTPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['TOTPREM'].value = \"", '";')[0],
            'TOTPREM_BR' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['TOTPREM_BR'].value = \"", '";')[0],
            'STAXAMT' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['STAXAMT'].value = \"", '";')[0],
            'STAXAMT_BR' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['STAXAMT_BR'].value = \"", '";')[0],
            'GST_AMT' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['GST_AMT'].value = \"", '";')[0],
            'COMMAMT' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['COMMAMT'].value = \"", '";')[0],
            'GST_COMMAMT' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['GST_COMMAMT'].value = \"", '";')[0],
            'PREM_AFTER_LOAD' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['PREM_AFTER_LOAD'].value = \"", '";')[0],
            'AR_AMT' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['AR_AMT'].value = \"", '";')[0],
            'VNA_TOT_BASICPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['VNA_TOT_BASICPREM'].value = \"", '";')[0],
            'VNB_TOT_BASICPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['VNB_TOT_BASICPREM'].value = \"", '";')[0],
            'VNC_TOT_BASICPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['VNC_TOT_BASICPREM'].value = \"", '";')[0],
            'MB_TOTBASICPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['MB_TOTBASICPREM'].value = \"", '";')[0],
            'MB_GPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['MB_GPREM'].value 		= \"", '";')[0],
            'MB_GST_AMT' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['MB_GST_AMT'].value 		= \"", '";')[0],
            'MB_STAXAMT' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['MB_STAXAMT'].value = \"", '";')[0],
            'MB_TOTPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['MB_TOTPREM'].value 		= \"", '";')[0],
            'FINAL_TOTPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['FINAL_TOTPREM'].value 	= \"", '";')[0],
            'MB_COMMAMT' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['MB_COMMAMT'].value 		= \"", '";')[0],
            'MB_GST_COMMAMT' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['MB_GST_COMMAMT'].value 	= \"", '";')[0],
            'DT_CHECK' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['DT_CHECK'].value = \"", '";')[0],
            'DT_UKEY' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['DT_UKEY'].value  = \"", '";')[0],
            'VERKEY' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['VERKEY'].value 	 = \"", '";')[0],
            'H1_BASICPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['H1_BASICPREM'].value = \"", '";')[0],
            'H2_BASICPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['H2_BASICPREM'].value = \"", '";')[0],
            'H3_BASICPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['H3_BASICPREM'].value = \"", '";')[0],
            'H4_BASICPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['H4_BASICPREM'].value = \"", '";')[0],
            'H5_BASICPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['H5_BASICPREM'].value = \"", '";')[0],
            'H6_BASICPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['H6_BASICPREM'].value = \"", '";')[0],
            'R1_BASICPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['R1_BASICPREM'].value = \"", '";')[0],
            'S1_BASICPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['S1_BASICPREM'].value = \"", '";')[0],
            'T_ACTPREM' => $this->client->stringBetween($htmlString, "parent.document.forms['mainform'].elements['T_ACTPREM'].value = \"", '";')[0],
        ];

        $this->setQuotationValue($quotationData);

        return $quotationData;
    }

    public function readOptions($htmlString) {
        preg_match_all('/new Option\((.+?)\);/', $htmlString, $matchesAll, PREG_SET_ORDER);

        return array_map(function($matches) {
            $array = explode(',', trim(str_replace('"', '', $matches[1])));

            return [
                'value' => $array[1],
                'text' => $array[0],
            ];
        }, $matchesAll);
    }

    /**
     * Parse occupation data
     *
     * @param string $htmlString
     * @return array
     */
    public function occupationData($htmlString) {
        $bodyDataJsHtml = $this->stringBetween($htmlString, '/** display occupation **/', '/** display occupation status **/')[0];

        return $this->readOptions($bodyDataJsHtml);
    }

    /**
     * Parse loan company data
     *
     * @param string $htmlString
     * @return array
     */
    public function loanCompanyData($htmlString) {
        $bodyDataJsHtml = $this->stringBetween($htmlString, '/** display coverage type**/', 'fnLoadFinTypeDropdown();')[0];

        return $this->readOptions($bodyDataJsHtml);
    }

    public function vehicleBodyData($htmlString) {
        $bodyDataJsHtml = $this->stringBetween($htmlString, '/** display vehicle body**/', '/** display safety code**/')[0];

        return $this->readOptions($bodyDataJsHtml);
    }

    public function extraCoverage($htmlString) {
        $htmlDom = $this->htmlToDom($htmlString);

        $tableRows = $htmlDom->filter('table tr');

        $self = &$this;

        $extraCoverageResults = collect([]);

        $tableRows->each(function(Crawler $node, $i) use ($self, &$extraCoverageResults) {
            $tableDatas = $node->filter('td');

            if($i == 0 || count($tableDatas) != 4) {
                return false;
            }

            $name = $self->trim($tableDatas->eq(1)->text());
            $value = $self->trim($tableDatas->eq(3)->text());

            $extraCoverageCode = $self->addToExtraCoverageSumIns($name, $value);
            if($extraCoverageCode) {
                $extraCoverageResults->push($extraCoverageCode);
            }
        });

        return $extraCoverageResults->toArray();
    }

    public function addToExtraCoverageSumIns($name, $value) {
        $found = null;
        foreach (static::$extraCoverageMaps as $key => $map) {
            if(Str::contains($name, $map[0])) {
                $found = $map;

                break;
            }
        }

        if($found) {
            $this->setExtraCoverageSumInsured($map[1], $value);

            return $map[1];
        }

        return null;
    }

    public function calculateExcess(\GuzzleHttp\Psr7\Response $response) {
        $body = (string) $response->getBody();

        return [
            'EXCESS_MIN_ALLOW' => $this->stringBetween($body, "parent.document.forms['mainform'].elements['EXCESS_MIN_ALLOW'].value = \"", "\";")[0],
            'EXCESS_MAX_ALLOW' => $this->stringBetween($body, "parent.document.forms['mainform'].elements['EXCESS_MAX_ALLOW'].value = \"", "\";")[0],
            'EXCESS' => $this->stringBetween($body, "parent.document.forms['mainform'].elements['EXCESS'].value = \"", "\";")[0],
        ];
    }

}
