<?php 

namespace Services\ChubbClient;

use Carbon\Carbon;
use Concerns\GeneratorTrait;
use Concerns\IcData;
use Services\BaseClientSubclass;

class Generator extends BaseClientSubclass {

    use GeneratorTrait;

    public $excessData = [
        'EXCESS_MIN_ALLOW' => null,
        'EXCESS_MAX_ALLOW' => null,
        'EXCESS' => '0',
    ];

    /**
     * Generate template / defaults data for axa quotation
     *
     * @param array $overrides
     * @return array
     */
    public function step1QuotationData(array $overrides = []) {
        $defaults = [
            'ACCODE' => 'V1902AHQ',
            'ACNAME' => 'InsureDIY Sdn Bhd',
            'ADDRESS_1' => 'GERAI NO 1 LRG MASJID 34250',
            'ADDRESS_2' => '',
            'ADDRESS_3' => '',
            'ADDRESS_4' => '',
            'ADDRESS_IND' => 'H',
            'ALLRIDER2' => 'N',
            'ANTITHEFT' => '13',
            'BLACKLIST_IND' => '',
            'BODY_LOAD' => '',
            'BUSINESS_NO' => '',
            'CALC_MTD' => 'P',
            'CAP' => '1495',
            'CHASSIS' => 'PM2M603S002046918',
            'CLAIMNO' => '0',
            'CNCODE' => 'NEW',
            'CNTYPE' => 'NWOO',
            'CNTYPE2' => '',
            'CONTACT_ID' => '',
            'CONTACT_INITIAL' => 'Y',
            'CONTACT_SEARCHKEY' => '',
            'CONTACT_TYPE' => 'I',
            'CONTACT_TYPE2' => 'I',
            'COUNTRY' => 'MYS',
            'CURR_PAGE' => 'pop_cnMB_add_1.jsp',
            'DAYS' => '366',
            'DID_NO' => '',
            'DOB' => '06-09-1993',
            'DRIVAGE' => '26',
            'DRIVE_EXP_LENGTH' => '07',
            'EFFDATE' => Carbon::today()->format('d-m-Y'),
            'EFFDATE2' => Carbon::today()->format('d-m-Y'),
            'EMAIL' => '',
            'ENDORSEMENT' => 'N',
            'END_DATE' => Carbon::today()->addYears(1)->subDays(2)->format('d-m-Y'),
            'ENGINE' => 'R76A07E',
            'EXPDATE' => Carbon::today()->addYears(1)->subDays(1)->format('d-m-Y'),
            'EXTEND_IND' => '',
            'FAX_NO_HOME' => '',
            'FAX_NO_OFFICE' => '',
            'FINTYPE' => '',
            'FLEETNO' => '',
            'FUELTYPE' => 'P',
            'FUNCTIONAL_MOD' => '01',
            'GARAGE' => '03',
            'GENDER' => 'M',
            'GEOLOCATION' => 'MPCOA1',
            'GNAME' => 'DOE',
            'INCEPTION_DATE' => '',
            'INFORCE' => 'true',
            'ISSDATE' => Carbon::today()->format('d-m-Y'), //today
            'LOADIND' => 'Y',
            'LOANCOM' => '',
            'LOGBOOK' => '123456',
            'MAINCLS' => 'MT',
            'MAKE' => 'PK046',
            'MANUAL_CNOTENO' => '',
            'MARITAL_STATUS' => 'S',
            'MARKETVALUE' => '26000.00',
            'MOBILE_NO' => '60123456789',
            'MODEL' => 'Perodua Myvi 1.5 ZHS',
            'MODEL_REFER' => 'N',
            'NAME' => 'JOHN',
            'NAMEDRIVER_1' => '',
            'NATIONALITY' => 'MYS',
            'ND' => 'JOHN DOE',
            'NEW_IC_NO' => '930906-14-7043',
            'NUMSEAT' => '4',
            'NVIC' => 'I0E12A',
            'OCCUPATION_CODE' => 'ACCT',
            'OCCUPATION_STATUS' => '02',
            'OCCUPATION_TEXT' => '',
            'OLD_IC_NO' => '',
            'OWNERSHIP' => 'I',
            'PAGE' => 'pop_cnMB_add_2.jsp',
            'PERFORMANCE_MOD' => '01',
            'PERMITDRIVER' => '03',
            'PNAME' => 'Chubb Insurance Malaysia Berhad',
            'POLACCODE' => '',
            'POLEFFDATE' => '',
            'POLEFF_DATE' => '',
            'POLEXPDATE' => '',
            'POLEXP_DATE' => '',
            'POLRI_IND' => '',
            'POLSTATUS' => 'NM',
            'POSTCODE' => '34251',
            'PREVPOL' => '',
            'PRINCIPLE' => '09',
            'QUICK_QUOTE' => '',
            'RACE' => 'MAL',
            'RDURATION' => '12',
            'REFERIND' => '',
            'REGION' => 'W',
            'REGION2' => 'W',
            'RENEWAL' => '',
            'REXPDATE' => Carbon::today()->addYears(1)->subDays(1)->format('d-m-Y'),
            'RISKMSG' => '',
            'RPNP_IND' => 'N',
            'SAFETY' => '05',
            'SALUTATION' => 'MR',
            'SRATE' => '1.00',
            'START_DATE' => Carbon::today()->format('d-m-Y'), //today
            'STATE' => 'PRK',
            'SUBCLS' => 'CO',
            'SUBID' => '',
            'SUBUSERGROUP' => 'XAUW101',
            'TEL_NO_HOME' => '',
            'TEL_NO_OFFICE' => '',
            'TOTND' => '1',
            'TRADE' => '',
            'TRAFFIC_VIOLATION' => '01',
            'TYPE' => 'ONE',
            'UOM' => 'C',
            'URL' => '',
            'VEHBODY' => 'SALOON',
            'VEHCLS' => 'KHBVMR',
            'VEHCOLOR' => '',
            'VEHNO' => 'MCG772',
            'VEHPURCHASE_DATE' => '14-11-2018',
            'VEHPURCHASE_PRICE' => '0.00',
            'VEHUSE' => '01',
            'VEH_IMPORT_DUTY' => '03',
            'YEARMAKE' => '2012',
            'dob' => '',
            'gender' => '',
            'ic_no' => '',
            'occupation' => '',
            'relationship' => '',
            'yearIssue' => '',
        ];

        $merged = $this->mergeAndOverride($defaults, $overrides, true);

        return $merged;
    }

    /**
     * generate second (step 2) quotation
     *
     * @param array $overrides
     * @return array
     */
    public function step2QuotationData($overrides = []) {
        $defaults = [
            'CODE' => [
                'KHB^A^VNB^1.1^HG',
                'KHB^A^VNB^1.1^HB',
                'KHB^A^VNB^1.1^HC',
                'KHB^A^VNB^1.1^HD',
            ],
            'CURR_PAGE' => 'pop_cnMB_add_2.jsp',
            'KHB^A^VNA^1.1^HB_CHKBOX_M' => 'M',
            'KHB^A^VNA^1.1^HB_SUBTITLE' => 'Providing towing and minor roadside repair service due to accident and<br/>breakdown.',
            'KHB^A^VNA^1.1^HC_CHKBOX_M' => 'M',
            'KHB^A^VNA^1.1^HC_SUBTITLE' => 'Providing replacement car service up to 10 days due to accident and breakdown<br/>when it occurs more than 100km away from your home and the repair takes more <br/>than 48 hours',
            'KHB^A^VNA^1.1^HD_CHKBOX_M' => 'M',
            'KHB^A^VNA^1.1^HD_SI_OVERWRITE' => '',
            'KHB^A^VNA^1.1^HD_SUBTITLE' => 'This is to reimburse the hotel accommodation expenses incurred due to vehicle <br/>breakdown & accident up to 5 days if 100km away from home and repair labor <br/>hours exceeds 48 hours',
            'KHB^A^VNA^1.1^HD_SUMINS' => '1000.00',
            'KHB^A^VNB^1.1^HG_CHKBOX_M' => 'M',
            'KHB^A^VNB^1.1^HG_SI_OVERWRITE' => '',
            'KHB^A^VNB^1.1^HG_SUBTITLE' => 'Provide cash relief in the event of house breaking',
            'KHB^A^VNB^1.1^HG_SUMINS' => '1000.00',
            'PAGE' => 'pop_cnMB_add_3.jsp',
            'PLAN_CODE' => 'A',
            'TYPE' => 'TWO',
        ];

        return $this->mergeAndOverride($defaults, $overrides);
    }

    public function step3QuotationData($overrides = []) {
        $defaults = [
            'CURR_PAGE' => 'pop_cnMB_add_3.jsp',
            'PAGE' => 'pop_cnMB_add_4.jsp',
            'Q1_A2' => 'GERAI NO 1 LRG MASJID 34250, TANJONG PIANDANG, PERAK DARUL RIDZUAN 34251',
            'Q1_Code' => 'Q1',
            'Q2_A1' => 'N',
            'Q2_A2' => '',
            'Q2_Code' => 'Q2',
            'Q3_Code' => 'Q3',
            'TYPE' => 'THREE',
        ];

        return $this->mergeAndOverride($defaults, $overrides);
    }

    public function calculateExcess($overrides = []) {
        $defaults = [
            "CLS" => "KHBVMR",
            "SUBCLS" => $this->getSharedQuotationData('SUBCLS', 'CO'),
            "PRINCIPLE" => "09",
            "SUM" => number_format($this->getSharedQuotationData('MARKETVALUE', '26000'), 0, '.', ''),
            "CAP" => $this->getSharedQuotationData('CAP', '1495'),
            "YRMAKE" => "2012",
            "REFERIND" => "HR",
            "NCDPCT" => "0",
            "TRAILERSUM" => "0",
            "BODY" => "",
            "REGION" => $this->getSharedQuotationData('REGION', 'W'),
            "CLAIMEXP" => "0",
            "CLAIMNO" => "0",
            "CNTYPE" => $this->getSharedQuotationData('CNTYPE', 'NWOO'),
            "EFFDATE" => Carbon::today()->format('d-m-Y'),
            "POLEFF_DATE" => "",
            "TYPE" => "",
            "UOM" => "C",
            "BUNDLE_IND" => "Y",
            "EXCESS_CODE" => "1",
            "MAKE" => "PK046",
        ];

        return $this->mergeAndOverride($defaults, $overrides, true);
    }

    public function quotationTariff($overrides = []) {
        $defaults = [
            'CLS' => 'KHBVMR',
            'SUBCLS' => $this->getSharedQuotationData('SUBCLS', 'CO'),
            'ALLRIDER' => 'N',
            'CAP' => $this->getSharedQuotationData('CAP', '1495'),
            'COMM' => '10',
            'SUM' => number_format($this->getSharedQuotationData('MARKETVALUE', '26000'), 0, '.', ''),
            'TRAILER' => '0',
            'LOADPCT' => '0',
            'NCD' => '0',
            'EXTRA' => '0',
            'STAX' => '6',
            'SRATE' => '1.00',
            'TRAILERIND' => '',
            'STAMP' => '10',
            'REGION' => $this->getSharedQuotationData('REGION', 'W'),
            'CNTYPE' => $this->getSharedQuotationData('CNTYPE', 'NWOO'),
            'NCDWITHDRAW' => '0',
            'REBATE' => '0',
            'ACTYPE' => 'NM',
            'NCDEFFRATE' => '1',
            'NCDWITHDRAWRATE' => '0',
            'UOM' => 'C',
            'STEPFROM' => 'FOUR',
            'AFTEREXTRA' => 'N',
            'GST_PCT' => '0.00',
            'GST_COMMPCT' => '0.00',
            'EFFDATE' => Carbon::today()->format('d-m-Y'),
            'EXCESS' => $this->excessData['EXCESS'],
            'QUO_START_DATE' => Carbon::today()->format('Ymd'),
            'REBATE_AGENT' => 'N',
            'DOB' => $this->getSharedQuotationData('DOB', '06-09-1993'),
            "DISC_PCT" => "0",
        ];

        return $this->mergeAndOverride($defaults, $overrides, true);
    }

    public function occupationData($overrides = []) {
        $defaults = [
            'FORM' => 'mainform',
            'TYPE' => 'CONTACTLOAD',
            'LOADIND' => '',
            'CNTYPE' => 'NWOO',
            'CONTACT_INITIAL' => 'N',
            'CONTACT_TYPE' => 'I',
            'ALTCONTACTID' => '',
            'NEW_IC_NO' => '930906-14-7043',
            'OLD_IC_NO' => '',
            'BUSINESS_NO' => '',
            'NAME' => 'JOHN',
        ];

        return $this->mergeAndOverride($defaults, $overrides);
    }

    public function vehicleClassParameters($overrides = []) {
        $defaults = [
            'FORM' => 'mainform',
            'LOADIND' => '',
            'CNTYPE' => 'NWOO',
            'PREVPOL' => '',
            'SUBUSERGROUP' => 'XAUW101',
            'VEHCLS' => '',
            'SUBCLS' => '',
            'VEHUSE' => $this->getSharedQuotationData('VEHUSE', ''),
            'GEOLOCATION' => '',
            'VEHBODY' => '',
            'GARAGE' => '',
            'SAFETY' => '',
            'ANTITHEFT' => '',
            'FUELTYPE' => '',
            'PERMITDRIVER' => '',
            'RDURATION' => '',
            'OWNERSHIP' => '',
            'CONTACT_TYPE' => 'I',
            'PRIME_TRAILER_IND' => '',
        ];

        return $this->mergeAndOverride($defaults, $overrides);
    }

    public function addExtraCoveragePayload($code, $sum, $prem = '0') {
        return $this->extraCoveragePayload('add', $code, $sum, $prem);
    }

    public function extraCoveragePayload($type = 'add', $code, $sum, $prem = '0') {
        $payload = [
            'TYPE' => $type,
            'EXTRACODE' => $code,
            'EXTRASUM' => is_numeric($sum) ? number_format($sum, 2, '.', '') : $sum,
            'EXTRAPREM' => number_format($prem, 2, '.', ''),
            'VEHCLS' => 'KHBVMR',
            'SUBCLS' => 'CO',
            'EXTRAAPREM' => '0.00',
            'VDMO_PREM_FACTOR' => '',
            'VSRATE' => '1.00',
        ];

        if($type == 'remove') {
            $payload['EXTRAFLG'] = '';
        }

        return $payload;
    }

    public function additionalDriver($driver, $overrides = []) {
        $ic = new IcData($driver['icNum']);

        return $this->mergeAndOverride([
            'CONTACT_TYPE' => 'I',
            'CLS' => 'KHBVMR',
            'SUBCLS' => 'CO',
            'VEHUSE' => $this->getSharedQuotationData('VEHUSE', '01'),
            'pageType' => 'CN',
            'actionType' => 'ADD',
            'TYPE' => 'add',
            'ic_no' => $ic->number,
            'dob' => $ic->dob->format('d-m-Y'),
            'gender' => $ic->gender == 'male' ? 'M' : 'F',
            'occupation' => 'ACCT',
            'yearIssue' => '',
            'relationship' => '02',
            'ND' => $driver['name'],
        ], $overrides);
    }

    public function checkIsmNCD($overrides = []) {
        $defaults = [
            "ISMDOCTYPE" => "ENQ",
            "TYPE" => "NCDRequest",
            "NAMESPACE" => "NCDServices",
            "NCDVEHNO" => "MCG772",
            "NCDFROM" => "",
            "CNPOL" => "",
            "ACTYPE" => "NM",
            "VEHNO" => "MCG772",
        ];

        return $this->mergeAndOverride($defaults, $overrides);
    }

}
