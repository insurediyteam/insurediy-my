<?php 

namespace Services\ChubbClient;

use Exception;
use Carbon\Carbon;
use Concerns\IcData;
use Services\ChubbClient;
use Concerns\TransformerTrait;
use Services\BaseClientSubclass;
use Services\Client\ComparisonTableData;

class Transformer extends BaseClientSubclass {

    use TransformerTrait;

    /**
     * airbag:abs
     *
     * @var array
     */
    public static $safeties = [
        '1:no' => '01',
        '2:no' => '02',
        '1:yes' => '05',
        '2:yes' => '06',
        '3:yes' => '07',
    ];

    public static $vehicleFroms = [
        'new' => 'CBN',
        'old' => 'CBU',
    ];

    public static $coverTypes = [
        'C' => 'CO',
    ];

    public static $garages = [
        '1' => '03',
        '2' => '04',
    ];

    public static $antitds = [
        '1' => '12',
        '2' => '13',
        '3' => '11',
        '4' => '10',
        '5' => '19',
    ];

    public static $provNCDs = [
        '2' => '0.00',
        '3' => '25.00',
        '4' => '30.00',
        '5' => '38.33',
        '6' => '45.00',
        '7' => '55.00',
    ];

    public static $genders = [
        'MALE' => 'M',
        'FEMALE' => 'F',
    ];

    public static $maritalstats = [
        'Married' => 'M',
        'Single' => 'S',
    ];
    public static $areas = [
        '1' => 'SEL',
        '2' => 'J|JOHOR',
        '3' => 'K|KEDAH',
        '4' => 'D|KELANTAN',
        '5' => 'L|LABUAN',
        '6' => 'M|MELAKA',
        '7' => 'N|NEGERI SEMBILAN',
        '8' => 'C|PAHANG',
        '9' => 'PRK',
        '10' => 'R|PERLIS',
        '11' => 'P|PULAU PINANG',
        '12' => 'S|SABAH',
        '13' => 'Q|SARAWAK',
        '14' => 'T|TERENGGANU',
    ];

    public static $vehicle_uses = [
        '5' => '01',
        '1' => '02',
        '2' => '04',
    ];

    public static $fuels = [
        'Petrol' => 'P',
        'Diesel' => 'D',
        'Gas' => 'G',
    ];

    public function getDrvExpLength($years) {
        $years = intval($years);

        if($years == 0 || $years == 1) {
            return '01';
        }

        if($years == 2) {
            return '02';
        }

        if($years == 3) {
            return '03';
        }

        if($years > 3 && $years <= 5) {
            return '04';
        }

        if($years > 5 && $years <= 10) {
            return '05';
        }

        if($years > 10 && $years <= 15) {
            return '06';
        }

        if($years > 15 && $years <= 20) {
            return '07';
        }

        if($years > 20 && $years <= 25) {
            return '08';
        }

        if($years > 25 && $years <= 30) {
            return '09';
        }

        if($years > 30 && $years <= 40) {
            return '10';
        }

        return '11';
    }

    public function getGeolocation($location) {
        if(in_array($location, ['3', '9', '14', '4'])) {
            return 'MPCOA4';
        }

        if(in_array($location, ['7', '8'])) {
            return 'MPCOA3';
        }

        if(in_array($location, ['12', '13', '5'])) {
            return 'MPCOA5';
        }

        if(in_array($location, ['2', '11'])) {
            return 'MPCOA2';
        }

        return 'MPCOA1';
    }

    /**
     * Transform to compatible generator overrides
     *
     * @param array $inputs
     * @return array
     */
    public function inputs($inputs) {
        // convert to optional array, returns null when calling undefined key / index / props
        $inputs = $this->optional($inputs);
        
        $mainDriver = $this->getMainDriver($inputs);
        
        $transformedInputs = [
            'VEHNO' => $inputs['vehicleRegNo'],
            'NCDVEHNO' => $inputs['vehicleRegNo'],
            'VEHBODY' => $this->getCarBodyTypeCode($inputs['carBodyType'], false),
            'CHASSIS' => $inputs['chassisNo'],
            'ENGINE' => $inputs['engineNo'],
            'MAKE' => $this->getCarModelCode($inputs['carModel']),
            'MODEL' => $this->getProviderChildAttribute('model', $inputs['carModel'], 'name'),
            'FUNCTIONAL_MOD' => $inputs['functionalMod'] === 'no' ? '01' : '05',
            'YEARMAKE' => $inputs['carMakeYear'],
            'CAP' => $inputs['vehicleEngineCc'],
            'SAFETY' => $this->select('safeties', $inputs['airbags'] . ':' . $inputs['abs']),
            'PERFORMANCE_MOD' => $inputs['performanceMod'] === 'no' ? '01' : '02',
            'VEHUSE' => $this->select('vehicle_uses', $inputs['vehicleUse']),
            'VEHPURCHASE_DATE' =>$inputs['purchaseDate'],
            'VEHPURCHASE_PRICE' => $inputs['purchasePrice'],
            'FUELTYPE' => $this->select('fuels', $inputs['carFuel']),
            'MARKETVALUE' => $inputs['marketValue'],
            'NUMSEAT' => $inputs['seatNumber'],
            'GARAGE' => $this->select('garages', $inputs['garage']),
            'SUBCLS' => $this->select('coverTypes', $inputs['coverType']),
            'ANTITHEFT' => $this->select('antitds', $inputs['antiTeft']),
            'DRIVE_EXP_LENGTH' => $this->getDrvExpLength($inputs['drivingExp']),
            'NCD' => $this->select('provNCDs', $inputs['ncd']),
            'NCDPCT' => $this->select('provNCDs', $inputs['ncd']),
            'GEOLOCATION' => $this->getGeolocation($inputs['location']),
            'FINTYPE' => $inputs['loan'] == 'yes' ? 'HP' : '',
            'LOANCOM' => $inputs['loan'] == 'yes' ? $this->getLoanCompanyCode($inputs['loanCompany']) : '',
            'POSTCODE' => $mainDriver['postalCode'],
        ];

        if($this->config('filter_occupation', false)) {
            $transformedInputs['OCCUPATION_CODE'] = $this->getOccupationCode($inputs['occupation']);
        }

        /**
         * Override sum insured by the agreed value
         */
        if ($inputs['sumInsured']) {
            $transformedInputs['MARKETVALUE'] = $inputs['sumInsured'];
            $this->client->setSumInsured(intval($inputs['sumInsured']));
        }

        if ($transformedInputs['VEHPURCHASE_DATE']) {
            $transformedInputs['VEHPURCHASE_DATE'] = Carbon::createFromFormat('d-M-Y', $transformedInputs['VEHPURCHASE_DATE'])->format('d-m-Y');
        }

        if ($inputs['carFuel'] && !$transformedInputs['FUELTYPE']) {
            throw new Exception('Invalid car fuel');
        }

        if ($inputs['coverType'] && !$transformedInputs['SUBCLS']) {
            throw new Exception('Invalid cover type.');
        }

        // transform optional driver data
        $this->transformDriver($transformedInputs, $inputs);

        // filter, remove null-equal values
        return $this->filterValue($transformedInputs);
    }

    protected function transformDriver(&$transformedInputs, $inputs) {
        $drivers = $this->getDrivers($inputs);

        if(count($drivers) === 0) {
            return;
        }

        $driver = $drivers[0];

        $ic = new IcData($driver['icNum']);

        $transformedInputs['NEW_IC_NO'] = $ic->number;
        $transformedInputs['DRIVAGE'] = $ic->dob->age;
        $transformedInputs['DOB'] = $ic->dob->format('d-m-Y');
        $transformedInputs['TOTND'] = (string) count($drivers);
        $transformedInputs['ND'] = $driver['name'];
        $transformedInputs['NAME'] = $driver['name'];
        $transformedInputs['ADDRESS_1'] = $driver['addressOne'];
        $transformedInputs['ADDRESS_2'] = $driver['addressTwo'];

        ChubbClient::$drivers = $drivers;
    }

}
