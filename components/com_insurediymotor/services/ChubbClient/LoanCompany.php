<?php

namespace Services\ChubbClient;

use Services\ChubbClient;
use Services\BaseClientSubclass;
use Services\Client\BaseListData;
use Services\Contracts\Client\Subclass\LoanCompanyContract;

class LoanCompany extends BaseClientSubclass implements LoanCompanyContract
{

    public function listData()
    {
        /** @var ChubbClient $this */
        $response = $this->loanCompanyDataRequest();

        $occupationDataHtml = (string) $response->getBody();

        $data = $this->parser->loanCompanyData($occupationDataHtml);

        array_shift($data);

        return array_map(function ($option) {
            return new BaseListData($option['value'], $option['text']);
        }, $data);
    }

}
