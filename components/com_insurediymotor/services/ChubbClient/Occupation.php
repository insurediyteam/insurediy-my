<?php 

namespace Services\ChubbClient;

use Services\BaseClientSubclass;
use Services\Client\BaseListData;
use Services\Contracts\Client\Subclass\OccupationContract;

class Occupation extends BaseClientSubclass implements OccupationContract {

    public function listData() {
        /** @var ChubbClient $this */
        $occupationDataRequest = $this->occupationDataRequest();

        $occupationDataHtml = (string) $occupationDataRequest->getBody();

        $data = $this->parser->occupationData($occupationDataHtml);
        
        array_shift($data);

        return array_map(function($option) {
            return new BaseListData($option['value'], $option['text']);
        }, $data);
    }

}
