<?php 

namespace Services;

use Concerns\InteractsWithJConfig;
use Illuminate\Database\Capsule\Manager as Capsule;

class BootEloquent {
    
    use InteractsWithJConfig;

    public function run() {
        $capsule = new Capsule;

        $capsule->addConnection([
            'driver'    => 'mysql',
            'host'      => $this->getConfig('host'),
            'database'  => $this->getConfig('db'),
            'username'  => $this->getConfig('user'),
            'password'  => $this->getConfig('password'),
            'charset'   => 'latin1',
            'collation' => 'latin1_swedish_ci',
            'prefix'    => '',
            'strict'	=> false,
            'options'   => [
                \PDO::ATTR_EMULATE_PREPARES => true
            ],
        ], 'default');

        // Set the event dispatcher used by Eloquent models... (optional)
        // use Illuminate\Events\Dispatcher;
        // use Illuminate\Container\Container;
        // $capsule->setEventDispatcher(new Dispatcher(new Container));

        // Make this Capsule instance available globally via static methods... (optional)
        $capsule->setAsGlobal();

        // Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
        $capsule->bootEloquent();

    }

}
