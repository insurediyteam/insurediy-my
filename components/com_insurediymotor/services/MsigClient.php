<?php 

namespace Services;

use Carbon\Carbon;
use Concerns\IcData;
use RuntimeException;
use Services\Bugsnag;
use Services\BaseClient;
use Illuminate\Support\Str;
use GuzzleHttp\Psr7\Response;
use Services\MsigClient\Parser;
use GuzzleHttp\Cookie\SetCookie;
use Services\MsigClient\Vehicle;
use Services\MsigClient\Generator;
use Services\MsigClient\Occupation;
use Services\MsigClient\Transformer;
use Services\Client\ComparisonTableData;
use Symfony\Component\DomCrawler\Crawler;

class MsigClient extends BaseClient {

    const HOST = 'https://my-genlink.msig-asia.com';

    const ROUTE_LOGIN_FORM = '/MYPORTAL/UserMgmt/GenLinkLogin.jsp';
    const ROUTE_LOGIN = '/MYPORTAL/UserMgmt/j_security_check';
    const ROUTE_LOGOUT_1 = '/MYPORTAL/UserMgmt/exit.jsp?logout=1&userType=3';
    const ROUTE_LOGOUT_2 = '/MYPORTAL/UserMgmt/UserLogoutServlet?logout=1&userType=3';
    const ROUTE_LOGOUT_3 = '/MYPORTAL/UserMgmt/ibm_security_logout';
    const ROUTE_SECURITY_VERIFICATION = '/MYPORTAL/UserMgmt/SecurityVerificationServlet';
    const ROUTE_HOME = '/MYPORTAL/Home/HomePageServ?frame=top';

    const ROUTE_FORM = '/Motor2/CoverNote/NewQuote.aspx';
    const ROUTE_QUOTATION = '/Motor2/CoverNote/NewCNote.aspx/GetPremiumDtls';

    const ROUTE_GET_DROPDOWN = '/Motor2/CoverNote/NewCNote.aspx/GetDropDown';

    const ROUTE_MAKE = '/Motor2/wsAutoComplete.asmx/fnGetMakeData';
    const ROUTE_MODEL = '/Motor2/wsAutoComplete.asmx/fnGetModelData';

    public static $guest = true;

    /** @var Vehicle */
    public $vehicle;

    /** @var Generator */
    public $generator;

    /** @var Parser */
    public $parser;

    /** @var Occupation */
    public $occupation;

    /** @var Transformer */
    public $transformer;

    /** @var string */
    public $step1QuotationForm;

    /** @var \Symfony\Component\DomCrawler\Crawler */
    public $step1QuotationFormDom;

    protected $quotationValue;

    protected $makeModelDom;

    public static $coverageInputNames = [
        'liabilityPass' => 'additionalCovers[21]',
        'cart' => 'additionalCovers[10]',
        'waiver' => 'additionalCovers[7]',
        'specialPeril' => 'additionalCovers[14]',
        'strike' => 'additionalCovers[22]',
        'laminated' => 'additionalCovers[14]',
    ];

    public static function getExtraCoverageInputName($type, $suffix = null, $invert = false) {
        $inputNames = $invert ? array_flip(static::$coverageInputNames) : static::$coverageInputNames;

        if(!isset($inputNames[$type])) {
            return null;
        }

        return $inputNames[$type] . $suffix;
    }

    /**
     * Instantiate class
     */
    public function __construct()
    {
        parent::__construct();

        $this->vehicle = new Vehicle($this);
        $this->generator = new Generator($this);
        $this->parser = new Parser($this);
        $this->occupation = new Occupation($this);
        $this->transformer = new Transformer($this);
    }

    public function __destruct()
    {
        $this->logout();
    }

    /**
     * get Base URL
     *
     * @return string
     */
    public function getBaseUri() {
        return self::HOST;
    }

    /**
     * Keys to filter generated data
     *
     * @return array
     */
    protected function sharedQuotationDataKeys() {
        return [
            'regno',
            'ncdReplyNo',
            'oldCoverTypeCode',
            'headerEffDate',
            'encdVehClass',
            'encdPercent',
            'isFoundInWNCD',
            'encdCutOffDate',
            'isFoundInWESI',
            'isCovernoteFound',
            'hasReadFromWESI',
            'clntNRIC',
            'clntOldIC',
            'effDt',
            'hNcdReplyMsg',
            'esiReplyMsg',
            'esiSucessfulRefNo',
            'ismMarketValue',
            'recommendedSI',
            'prevInputOfYrManf',
            'prevIsReqISMValuation',
            'currIsReqISMValuation',
            'isESIClauseSelected',
            'searchType',
            'selectedId',
            'ncdsent',
            'standard',
            'isTheftProne',
            'esiReply',
            'autoRated',
            'occupationCode',
            'occupationDesc',
            'gender',
            'coverTypeCode',
            'vehicleClass',
            'CIFormNo',
            'registrationNo',
            'location',
            'region',
            'engineNo',
            'chassisNo',
            'makeCode',
            'modelCode',
            'prevModelCode',
            'prevModelDesc',
            'makeDesc',
            'modelDesc',
            'tonnage',
            'capacityHidden',
            'capacity',
            'capacityType',
            'yearOfManufacture',
            'variantNvic',
            'nvic',
            'numOfSeats',
            'logBookNo',
            'color',
            'permittedDrivers',
            'vehicleRegnClass',
            'antiTheftDevices',
            'safetyFeatures',
            'garageLocation',
            'importedType',
            'purposeOfUse',
            'purchaseDate',
            'purchasePrice',
            'exInsurer',
            'exPolicyNo',
            'exVehicleRegNo',
            'previousInceptionDate',
            'previousExpiryDate',
            'newScreen',
            'vehicleSumInsured',
            'oldSumInsured',
            'totalSumInsured',
            'motorBasicPremium',
            'loadingAmount',
            'grossBasicPremium',
            'ncbPercent',
            'ncbAmount',
            'ncbVariation',
            'ncdClaimsFreeYr',
            'ncdAnniversaryDay',
            'ncdAnniversaryMth',
            'nettPremium',
            'additionalCovers[0].premium',
            'additionalCovers[1].premium',
            'additionalCovers[2].premium',
            'additionalCovers[3].premium',
            'additionalCovers[4].premium',
            'additionalCovers[5].premium',
            'additionalCovers[6].premium',
            'additionalCovers[7].premium',
            'additionalCovers[8].premium',
            'additionalCovers[9].limitAmount',
            'additionalCovers[9].premium',
            'additionalCovers[10].limitAmount',
            'additionalCovers[10].premium',
            'additionalCovers[11].limitAmount',
            'additionalCovers[11].premium',
            'additionalCovers[12].limitAmount',
            'additionalCovers[12].premium',
            'additionalCovers[13].premium',
            'additionalCovers[14].limitAmount',
            'additionalCovers[14].premium',
            'additionalCovers[15].limitAmount',
            'additionalCovers[15].premium',
            'additionalCovers[16].limitAmount',
            'additionalCovers[16].premium',
            'additionalCovers[17].limitAmount',
            'additionalCovers[17].premium',
            'additionalCovers[18].premium',
            'additionalCovers[19].premium',
            'additionalCovers[20].premium',
            'additionalCovers[21].premium',
            'additionalCovers[22].premium',
            'additionalCovers[23].premium',
            'totalPremium',
            'postedPremium',
            'excessType',
            'excessAmount',
            'oldExcessAmount',
            'interestedParties[0].ipName',
            'interestedParties[0].roleDesc',
            'interestedParties[0].bankRefNo',
            'interestedParties[0].ipId',
            'interestedParties[0].roleId',
        ];
    }

    /**
     * Configuration key index
     *
     * @return string
     */
    public function configKey() {
        return 'msig';
    }

    /**
     * database code
     *
     * @return string
     */
    public function code() {
        return $this->configKey();
    }

    /**
     * Get options for generating http client
     *
     * @return array
     */
    public function getHttpClientOptions() {
        return [
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36;OS = Windows',
            ],
        ];
    }

    /**
     * Do request and then automatically updates the token
     *
     * @param callable $clientCallback
     * @return Response
     */
    public function request(callable $clientCallback) {
        // run client
        /** @var \GuzzleHttp\Psr7\Response $response */
        $response = $clientCallback($this->getHttpClient());

        if($response instanceof Response) {
            $responseBody = (string) $response->getBody();

            $token = $this->stringBetween($responseBody, "setCookie('token','", "');");
            // save token if found any
            if(count($token)) {
                $token = $token[0];
                // set token to cookie
                $cookie['token'] = $token;
                $setCookie = new SetCookie([
                    'Name' => 'token',
                    'Value' => $token,
                    'Domain' => 'my-genlink.msig-asia.com',
                ]);
                $cookieJarSet = $this->cookieJar->setCookie($setCookie);
            }
        }
        else {
            throw new RuntimeException('$clientCallback should return \GuzzleHttp\Psr7\Response instance');
        }

        return $response;
    }

    /**
     * Check authentication
     *
     * @return bool
     */
    public function checkAuth() {
        if (static::$guest === false) {
            return true;
        }

        $response = $this->getHttpClient()->get(self::ROUTE_HOME, [
            // 'debug' => true,
        ]);

        $responseBody = (string) $response->getBody();

        return ! (
            Str::contains($responseBody, 'Please enter your ID and Password to sign in') || 
            Str::contains($responseBody, 'Session Timeout')
        );
    }

    /**
     * Authenticate to server
     *
     * @return self
     */
    public function authenticate() {
        if(!$this->checkAuth()) {
            // login
            $auth = $this->config('auth');

            $loginData = [
                'def_langauge' => 'E',
                'IPAddress' => '127.0.0.1',
                'btnLogin.x' => '54',
                'btnLogin.y' => '3',
				'j_password' => $auth['password'],
                'j_username' => $auth['user'],
            ];
            
            try {
                // need to access login form first
                $login = $this->getHttpClient()->get(self::ROUTE_LOGIN_FORM, [
                    // 'debug' => true,
                ]);
                
                $login = $this->request(function(\GuzzleHttp\Client $client) use ($loginData) {
                    return $client->post(self::ROUTE_LOGIN, [
                        'form_params' => $loginData,
                        // 'debug' => true,
                    ]);
                });

                $responseBody = (string) $login->getBody();
                
                if(Str::contains($responseBody, 'Invalid User Name/ Password')) {
                    $this->doLockDown();
                }

                if(Str::contains($responseBody, 'You are currently login to GenLink via another session or have not logout properly from a previous session')) {
                    $this->getHttpClient()->get(static::ROUTE_SECURITY_VERIFICATION, [
                        'query' => $this->getModel()->options['unlockLogin'],
                    ]);
                }

                static::$guest = false;
            }
            catch (\Exception $e) {
                //
                var_dump($e);
                exit;
            }

        }

        return $this;
    }

    /**
     * Get quotation
     *
     * @param array $inputs
     * @return array
     */
    public function quotation($inputs = []) {
        $this->authenticate();
        
        $inputs = $this->transformer->inputs($inputs);

        $response = $this->step1QuotationRequest($inputs);
        
        if(isset($inputs['NRIC'])) {
            // enquire ncd
            $response = $this->enquireNCDRequest($inputs);
            $dom = $this->htmlToDom((string) $response->getBody());
            $response = $this->request(function(\GuzzleHttp\Client $client) use ($dom) {
                return $client->post('/MYPORTAL/NCD/InquireConfirmation.action', [
                    'query' => [
                        'act' => 'viewG',
                    ],
                    'form_params' => [
                        "ack" => "send",
                        "id" => $dom->filter('input[name=\'id\']')->first()->attr('value'),
                        "fr" => "pol",
                        "OK" => "Continue",
                    ],
                ]);
            });

            $data = $this->parser->enquireNCD($response);
            $this->addCoverTypeAndExcesses(
                new ComparisonTableData(
                    ComparisonTableData::GROUP_NCD_PERCENT[0],
                    ComparisonTableData::GROUP_NCD_PERCENT[1],
                    number_format(floatval($data['nextNCD']), 2, '.', ',') . '%'
                )
            );
        }

        // check NCD
        $response = $this->request(function(\GuzzleHttp\Client $client) use ($inputs) {
            return $client->post('/MYPORTAL/CheckNCD.action', [
                // 'debug' => true,
                'query' => $this->generator->checkNCDQuery($inputs),
                'body' => http_build_query(
                    array_merge(
                        $this->generator->checkNCDFormData($inputs)
                    )
                ),
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Referer' => 'https://my-genlink.msig-asia.com/MYPORTAL/SelectCoverage.action',
                ],
            ]);
        });
        $ncdData = $this->parser->checkNcdData($response);
        
        $response = $this->request(function(\GuzzleHttp\Client $client) use ($inputs, $ncdData) {
            return $client->post('/MYPORTAL/FindBasicPremiumAction.action', [
                // 'debug' => true,
                'query' => [
                    'isVisitedLoadScreen' => 'false',
                ],
                'body' => $this->generator->quotationQuery(
                    $this->generator->addExtraCover(
                        $this->generator->quotation(
                            array_merge($inputs, $this->generator->findBasicPremium($inputs), $ncdData)
                        )
                    )
                ),
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
            ]);
        });

        $this->parser->extraCoverage((string) $response->getBody());

        $premiumData = $this->parser->premiumParser((string) $response->getBody());

        $response = $this->request(function(\GuzzleHttp\Client $client) use ($premiumData) {
            return $client->post('/MYPORTAL/SaveMotorRisk.action', [
                // 'debug' => true,
                'body' => $this->generator->quotationQuery(
                    $this->generator->addExtraCover(
                        $this->generator->quotation($premiumData)
                    )
                ),
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
            ]);
        });

        $quotationData = $this->parser->quotationData((string) $response->getBody());

        return $quotationData;
    }

    /**
     * Step1 Form Submit Request
     *
     * @param array $inputs
     * @return Response
     */
    public function step1QuotationRequest($inputs = []) {
        $this->authenticate();

        $response = $this->getHttpClient()->get('/MYPORTAL/PolicyMgmt/PolicyMgmtHome.jsp', [
            // 'debug' => true,
        ]);
        $response = $this->request(function(\GuzzleHttp\Client $client) {
            return $client->get('/MYPORTAL/PolicyMgmt/PolicyMgmtHome.jsp', [
                // 'debug' => true,
            ]);
        });

        $response = $this->request(function(\GuzzleHttp\Client $client) {
            return $client->get('/MYPORTAL/CreateProposal.action', [
                // 'debug' => true,
                'query' => [
                    'ID' => '1094',
                    'type' => '2',
                    'accountNo' => 'KLFIND01',
                ],
                'headers' => [
                    'Referer' => 'https://my-genlink.msig-asia.com/MYPORTAL/PolicyMgmt/PolicyMgmtHome.jsp',
                ],
            ]);
        });

        $defaultClientData = "1480974|P|1";
        if(isset($inputs['clntNRIC'])) {
            $ic = new IcData($inputs['clntNRIC']);

            // search for client
            $_defaultClientData = $this->searchClientData($inputs['clntNRIC']);

            if(!$_defaultClientData) {
                // map post code
                $response = $this->request(function(\GuzzleHttp\Client $client) {
                    return $client->post('/MYPORTAL/CreatePersonalProspect.action', [
                        'form_params' => [
                            'currentClientType' => 'P',
                        ],
                    ]);
                });
                $postalCodeData = $this->parser->postalCodeData($response, $inputs['postalCode']);
                
                // create client
                $response = $this->request(function(\GuzzleHttp\Client $client) use ($inputs, $postalCodeData) {
                    return $client->post('/MYPORTAL/ClientMgmt/PersonalClientServ', [
                        'form_params' => $this->generator->createClient(
                            array_merge($inputs, $postalCodeData)
                        ),
                    ]);
                });
                
                // re-search agan to get id
                $_defaultClientData = $this->searchClientData($inputs['clntNRIC']);
            }

            if($_defaultClientData) {
                $defaultClientData = $_defaultClientData;
            }
            else {
                Bugsnag::getInstance()->notifyError(
                    'scrapeError',
                    json_encode([
                        'class' => get_class($this),
                        'message' => 'Can\'t get client data!',
                        'data' => isset($postalCodeData) ? array_merge($inputs, $postalCodeData) : [], 
                    ])
                );
            }
        }
        $response = $this->request(function(\GuzzleHttp\Client $client) use ($defaultClientData) {
            list($clientID, $clientType, $clientAcc) = explode('|', $defaultClientData);
            return $client->get('/MYPORTAL/AddClient.action', [
                // 'debug' => true,
                'query' => [
                    'clientID' => $clientID,
                    'clientType' => $clientType,
                    'clientAcc' => $clientAcc,
                ],
            ]);
        });

        $response = $this->request(function(\GuzzleHttp\Client $client) use ($inputs) {
            return $client->post('/MYPORTAL/SelectCoverage.action', [
                // 'debug' => true,
                'form_params' => $this->generator->selectCoverage($inputs),
            ]);
        });

        $dom = $this->htmlToDom((string) $response->getBody());
        $extraCoverageTableRows = $dom->filter('body > form > table:nth-child(35) tr');
        $this->generator->extraCoverCodes = [];
        $self = &$this;
        $extraCoverageTableRows->each(function(Crawler $node, $key) use ($self) {
            $coverIdInputs = $node->filter('input[name="hCoverID"]');
            if(!count($coverIdInputs)) {
                return;
            }

            array_push($self->generator->extraCoverCodes, [
                'key' => 'hCoverID',
                'value' => $coverIdInputs->first()->attr('value'),
            ]);
        });

        return $response;
    }

    /**
     * Set the value of quotationValue
     *
     * @return  self
     */ 
    public function setQuotationValue($quotationData)
    {
        $this->quotationValue = $quotationData;

        return $this;
    }

    public function occupationRequest() {
        return $this->getHttpClient()->post(static::ROUTE_GET_DROPDOWN, [
            'json' => [
                'DdlName' => "sdfOccup",
            ],
        ]);
    }

    public function makeDataRequest($name) {
        $this->authenticate();
        
        return $this->getHttpClient()->post(static::ROUTE_MAKE, [
            'json' => [
                'context' => [
                    'ComboType' => "Make",
                    'NumberOfItems' => 0,
                    'ProductCode' => 'PZ01',
                    'Text' => $name,
                ],
            ],
        ]);
    }

    public function modelDataRequest($makeCode, $keyword, $makeYear = null) {
        $this->authenticate();

        return $this->getHttpClient()->post(static::ROUTE_MODEL, [
            'json' => [
                'context' => [
                    'ComboType' => 'Model',
                    'FilterKeys' => "PZ01±{$makeCode}±{$makeYear}±N±",
                    'NumberOfItems' => 0,
                    'Text' => $keyword,
                ],
            ],
        ]);
    }

    public function logout() {
        $this->getHttpClient()->get(static::ROUTE_LOGOUT_1);
        $this->getHttpClient()->get(static::ROUTE_LOGOUT_2);
        $this->getHttpClient()->post(static::ROUTE_LOGOUT_3, [
            'form_params' => [
                'logoutExitPage' => '/UserMgmt/CGULogin.jsp',
            ],
        ]);
    }

    public function searchClientData($icNum) {
        $ic = new IcData($icNum);
        $response = $this->request(function(\GuzzleHttp\Client $client) use ($ic) {
            return $client->post('/MYPORTAL/ClientMgmt/SearchClientServ', [
                'form_params' => [
                    "clientName" => "",
                    "nricNo" => $ic->numberDashed,
                    "oldIC" => "",
                    "bizRegisterNo" => "",
                    "pageNo" => "1",
                    "searchNext" => "N",
                    "searchType" => "A",
                    "Action" => "Search",
                    "refPage" => "NB",
                    "clientType" => "P",
                ],
            ]);
        });

        $dom = $this->htmlToDom((string) $response->getBody());
        $radioElems = $dom->filter('input[type=RADIO]');
        if(count($radioElems)) { //found
            return $radioElems
                ->first()
                ->attr('value');
        }
        
        return null;
    }

    public function enquireNCDRequest($inputs) {
        return $this->request(function(\GuzzleHttp\Client $client) use ($inputs) {
            return $client->post('/MYPORTAL/NCD/CreateConfirmation.do', [
                'form_params' => $this->generator->enquireNCD($inputs),
            ]);
        });
    }
    
}
