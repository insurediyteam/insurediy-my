<?php 

namespace Services;

abstract class BaseClientSubclass {

    /** @var BaseClient */
    protected $client;

    public function __construct(BaseClient $client)
    {
        $this->client = &$client;
    }

    /**
     * convert XML string to array
     *
     * @param string $xmlString
     * @return array
     */
    public function xmlToArray($xmlString) {
        return $this->client->xmlToArray($xmlString);
    }

    /**
     * convert XML string to array
     *
     * @param string $xmlString
     * @return array
     */
    public function optionalArray(array $array, $key, $default = null)
    {
        return $this->client->optionalArray($array, $key, $default);
    }

    /**
     * get DOM object
     *
     * @param string $htmlString
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function htmlToDom($htmlString) {
        return $this->client->htmlToDom($htmlString);
    }

    /**
     * update $this->sahredQuotationData
     *
     * @param array $generatedData
     * @return self
     */
    protected function updateSharedQuotationData(array $generatedData) {
        return $this->client->updateSharedQuotationData($generatedData);
    }

    /**
     * @param string $index
     * @param mixed $default
     * @return mixed
     */
    protected function getSharedQuotationData($index, $default = null) {
        return $this->client->getSharedQuotationData($index, $default);
    }

    /**
     * get http client
     *
     * @return Client
     */
    public function getHttpClient() {
        return $this->client->getHttpClient();
    }

    /**
     * Authenticate to server
     *
     * @return BaseClient
     */
    public function authenticate() {
        return $this->client->authenticate();
    }

    /**
     * Get string between 2 delimiter start and end
     *
     * @param string $string
     * @param string $startDelimiter
     * @param string $endDelimiter
     * @return void
     */
    public function stringBetween($string, $startDelimiter, $endDelimiter) {
        return $this->client->stringBetween($string, $startDelimiter, $endDelimiter);
    }

    /**
     * Automagically calls parent (client) for convinience
     *
     * @param string $method
     * @param array $args
     * @return mixed
     */
    public function __call($method, $args) {
        if(count($args) == 1) {
            return $this->client->{$method}($args[0]);
        }

        if(count($args) == 2) {
            return $this->client->{$method}($args[0], $args[1]);
        }

        return $this->client->{$method}(...$args);
    }

    /**
     * Automagically get property from BaseClient
     *
     * @param string $property
     * @return mixed
     */
    public function __get($property) {
        return $this->client->{$property};
    }

}
