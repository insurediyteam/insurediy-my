<?php 

namespace Services\Contracts\Client\Subclass;

interface VehicleContract {

    /**
     * List vehicle makers
     *
     * @return array
     */
    public function makeListData();

    /**
     * List vehicle make models
     *
     * @param string $makeCode
     * @param mixed $type
     * @param mixed $cover
     * @param mixed $use
     * @return array
     */
    public function modelListData($makeCode, $type = null, $cover = null, $use = null);

    /**
     * List vehicle / car body
     *
     * @return array
     */
    public function bodyListData();

}
