<?php 

namespace Services\Contracts\Client\Subclass;

interface OccupationContract {

    /**
     * List occupation data
     *
     * @return array
     */
    public function listData();

}
