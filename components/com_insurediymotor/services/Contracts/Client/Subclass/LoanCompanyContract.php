<?php 

namespace Services\Contracts\Client\Subclass;

interface LoanCompanyContract {

    /**
     * List occupation data
     *
     * @return array
     */
    public function listData();

}
