<?php 

namespace Services;

use Concerns\IcData;
use Services\BaseClient;
use Illuminate\Support\Str;
use GuzzleHttp\Psr7\Response;
use Services\ChubbClient\Parser;
use Services\ChubbClient\Vehicle;
use Services\ChubbClient\Generator;
use Services\ChubbClient\Occupation;
use Services\ChubbClient\LoanCompany;
use Services\ChubbClient\Transformer;
use Services\Client\ComparisonTableData;

class ChubbClient extends BaseClient {

    const HOST = 'https://ebiz.chubb.my';

    const ROUTE_LOGIN = '/ecover/login/loginCheck.jsp';
    const ROUTE_HOME = '/ecover/menu/';
    const ROUTE_LOGOUT = '/ecover/login/logout.jsp';

    const ROUTE_QUOTE_FORM_STEP1 = '/ecover/motor/pop_cnMB_add_0.jsp';
    const ROUTE_QUOTATION = '/ecover/motor/pop_cnMB_add_route.jsp';
    const ROUTE_QUOTATION_TARIFF = '/ecover/detariff/Motor/returnMotor2.jsp';
    const ROUTE_QUOTATION_EXTRACOV_ADD = '/ecover/motor/pop_cnMB_extra2.jsp';

    const ROUTE_CAR_MAKE = '/ecover/common/search/search_model.jsp';
    const ROUTE_CAR_CLASS_DATA = '/ecover/motor/retrieve_vehcls.jsp';

    const ROUTE_RETRIEVE_AGENT_INFO = '/ecover/motor/retrieve_info_agent.jsp';

    const ROUTE_OCCUPATION_DATA = '/ecover/motor/retrieve_info_contact.jsp';

    public static $guest = true;

    /** @var Vehicle */
    public $vehicle;

    /** @var Generator */
    public $generator;

    /** @var Parser */
    public $parser;

    /** @var Occupation */
    public $occupation;

    /** @var string */
    public $step1QuotationForm;

    /** @var \Symfony\Component\DomCrawler\Crawler */
    public $step1QuotationFormDom;

    /** @var Transformer */
    public $transformer;

    /** @var LoanCompany */
    public $loanCompany;

    public $debug = false;

    /** @var array */
    public static $drivers = [];

    public static $extraCoverageCode = [
        'cart' => '112B', // code
        'tinting' => 'VV89',
        'specialPeril' => 'V057',
        'liabilityPass' => 'V072',
        'laminated' => 'V089',
    ];

    /**
     * Instantiate class
     */
    public function __construct()
    {
        parent::__construct();

        $this->vehicle = new Vehicle($this);
        $this->generator = new Generator($this);
        $this->parser = new Parser($this);
        $this->occupation = new Occupation($this);
        $this->transformer = new Transformer($this);
        $this->loanCompany = new LoanCompany($this);
    }

    /**
     * Need to logout, avoiding errors. IMPORTANT!!
     */
    public function __destruct()
    {
        $this->logout();
    }

    /**
     * get Base URL
     *
     * @return string
     */
    public function getBaseUri() {
        return self::HOST;
    }

    /**
     * Keys to filter generated data
     *
     * @return array
     */
    protected function sharedQuotationDataKeys() {
        return [
            'DOB',
            'REGION',
            'CNTYPE',
            'CAP',
            'SUBCLS',
            'MARKETVALUE',
            'SUM',
            'VEHUSE',
            'NCD',
        ];
    }

    /**
     * Configuration key index
     *
     * @return string
     */
    public function configKey() {
        return 'chubb';
    }

    /**
     * database code
     *
     * @return string
     */
    public function code() {
        return $this->configKey();
    }

    /**
     * Get options for generating http client
     *
     * @return array
     */
    public function getHttpClientOptions() {
        return [
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36',
                'Upgrade-Insecure-Requests' => '1',
                'Sec-Fetch-User' => '?1',
                'Sec-Fetch-Site' => 'same-origin',
                'Sec-Fetch-Mode' => 'navigate',
            ],
        ];
    }

    /**
     * Authenticate to server
     *
     * @return self
     */
    public function authenticate() {
        if(!$this->checkAuth()) {
            // workaround to avoid logout redirection that occurs on first run: reset the cookie
            // $this->getCookieJar()->clear();

            $configAuth = $this->config('auth');

            // login
            $loginData = [
                'INSCODE' => '09',
                'USERID' => $configAuth['user'],
                'PASSWORD' => hash('sha512', $configAuth['password'] . $configAuth['user']),
                'PASSWORD2' => hash('sha1', $configAuth['password']),
            ];
            
            // do login
            $response = $this->getHttpClient()->post(self::ROUTE_LOGIN, [
                'form_params' => $loginData,
                'headers' => [
                    'Origin' => 'https://ebiz.chubb.my',
                    'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
                ],
                // 'debug' => true,
            ]);

            if(Str::contains((string) $response->getBody(), 'Invalid Login ID / Password')) {
                $this->doLockDown();
            }

            static::$guest = false;

            // retrieve agent info
            $response = $this->getHttpClient()->post(static::ROUTE_RETRIEVE_AGENT_INFO, [
                'headers' => [
                    'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
                ],
                'query' => [
                    'FORM' => 'mainform',
                    'CLS' => 'MT',
                    'ACCODE' => '',
                    'LOADIND' => 'onload',
                ],
                'form_params' => [
                    'error' => 'Unknown error.',
                ],
                // 'debug' => true,
            ]);
        }

        return $this;
    }
    
    /**
     * Set the value of quotationValue
     *
     * @return  self
     */ 
    public function setQuotationValue($quotationData)
    {
        $this->quotationValue = $quotationData['TOTPREM'];

        return $this;
    }

    /**
     * Get quotation
     *
     * @param array $inputs
     * @return array
     */
    public function quotation($inputs = []) {
        $this->authenticate();

        $inputs = $this->transformer->inputs($inputs);

        // $response = $this->additionalDriverRequest($inputs);

        if(isset($inputs['POSTCODE'])) {
            // check post code, get state code !IMPORTANT
            $response = $this->getStateCodeRequest($inputs['POSTCODE']);
            $responseBody = (string) $response->getBody();
            $displayState = $this->stringBetween($responseBody, '/** display state **/', '/** display country **/')[0];
            $displayState = $this->stringBetween($displayState, 'optionCount = optionCount + 1;', 'optionCount = optionCount + 1;')[0];
            $stateOptionRaw = $this->stringBetween($displayState, 'new Option(', ',true,true');
            if(count($stateOptionRaw)) {
                $stateOption = json_decode("[" . $stateOptionRaw[0] . "]", true);
                $inputs['STATE'] = $stateOption[1];
            }
        }

        $response = $this->step1QuotationRequest($inputs);

        if($inputs) {
            $response = $this->step2QuotationRequest($inputs);
            
            $response = $this->step3QuotationRequest($inputs);
        }

        // verify NCD
        if(isset($inputs['VEHNO'])) {
            $response = $this->checkIsmNCDRequest($inputs);
            $responseBody = (string) $response->getBody();
            $ncd = $this->stringBetween($responseBody, 'var IsmNxtNCDLevel_pct = ', ';')[0];
            $inputs['NCD'] = $inputs['NCDPCT'] = number_format($ncd, 2, '.', '');
            
            $this->addCoverTypeAndExcesses(
                new ComparisonTableData(
                    ComparisonTableData::GROUP_NCD_PERCENT[0],
                    ComparisonTableData::GROUP_NCD_PERCENT[1],
                    $inputs['NCD'] . '%'
                )
            );
        }

        // excess
        $response = $this->calculateExcessRequest($inputs);
        $calculateExcessData = $this->parser->calculateExcess($response);
        $this->generator->excessData = $calculateExcessData;
        $this->addCoverTypeAndExcesses(
            new ComparisonTableData(
                ComparisonTableData::GROUP_OWN_DAMAGE_EXCESS[0],
                ComparisonTableData::GROUP_OWN_DAMAGE_EXCESS[1],
                ComparisonTableData::valueWithIcon($calculateExcessData['EXCESS'], true)
            )
        );

        $response = $this->quotationTariffRequest($inputs);
        
        $quotationData = $this->parser->quotationData((string) $response->getBody());

        if(count($this->generator->selectedExtraCoverages)) {
            $response = $this->extraCoverageRequest();
            
            $response = $this->quotationTariffRequest($inputs);
            
            $quotationData = $this->parser->quotationData((string) $response->getBody());

            $response = $this->extraCoverageTableRequest();

            $extraCoverages = $this->parser->extraCoverage((string) $response->getBody());

            $extraCoverageDiff = array_diff($extraCoverages, $this->generator->selectedExtraCoverages);

            if(count($extraCoverageDiff)) {
                foreach ($extraCoverageDiff as $key => $extraCoverage) {
                    try {
                        $response = $this->getHttpClient()->post(static::ROUTE_QUOTATION_EXTRACOV_ADD, [
                            'query' => $this->generator->extraCoveragePayload(
                                'remove',
                                static::$extraCoverageCode[$extraCoverage]
                            ),
                        ]);
                    } catch (\Throwable $th) {
                        //throw $th;
                        \Services\Bugsnag::getInstance()->notifyException($th);
                    }
                }
    
                $response = $this->quotationTariffRequest($inputs);
                
                $quotationData = $this->parser->quotationData((string) $response->getBody());
            }
        }

        return $quotationData;
    }

    /**
     * Check authentication
     *
     * @return bool
     */
    public function checkAuth() {
        if (! static::$guest) {
            return true;
        }
        
        $response = $this->getHttpClient()->get(self::ROUTE_HOME);

        $responseBody = (string) $response->getBody();

        return ! Str::contains($responseBody, 'login.jsp?type=3');

        return $response->getStatusCode() === 200;
    }

    /**
     * Logout from server
     *
     * @return Response
     */
    public function logout() {
        $response = $this->getHttpClient()->get(self::ROUTE_LOGOUT, [
            // 'debug' => true,
        ]);

        return $response;
    }

    /**
     * Main quotation request (Form Submit)
     *
     * @param array $postData
     * @return Response
     */
    public function quotationRequest($postData, $allowRedirect = true) {
        $clientOptions = [
            'form_params' => $postData,
            // 'debug' => true,
            'allow_redirect' => $allowRedirect,
        ];
        
        /**
         * Handles additional coverage
         */
        if(isset($postData['CODE'])) {
            $CODEs = $postData['CODE'];
            unset($postData['CODE']);

            $postBody = http_build_query($postData);
            foreach($CODEs as $CODE) {
                $postBody .= "&" . http_build_query(['CODE' => $CODE]);
            }

            // change client options manually
            unset($clientOptions['form_params']);
            $clientOptions['body'] = $postBody;
            $clientOptions['headers'] = [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
            ];
        }

        try {
            $response = $this->getHttpClient()->post(self::ROUTE_QUOTATION, $clientOptions);
        }
        catch (\Exception $e) {
            $this->logout();

            var_dump($e);exit;

            return false;
        }

        return $response;

    }

    /**
     * Get step 1 form quotation request
     *
     * @return Response
     */
    public function step1FormQuotationRequest() {
        $this->authenticate();
        
        return $this->getHttpClient()->get(static::ROUTE_QUOTE_FORM_STEP1);
    }

    /**
     * Step1 Form Submit Request
     *
     * @param array $inputs
     * @return Response
     */
    public function step1QuotationRequest($inputs) {
        $this->authenticate();
        
        $quotationData = $this->generator->step1QuotationData($inputs);

        $response = $this->quotationRequest($quotationData);

        return $response;
    }

    /**
     * Step 2 Form Submit Request
     *
     * @param array $inputs
     * @return Response
     */
    public function step2QuotationRequest($inputs) {
        $this->authenticate();
        
        $quotationData = $this->generator->step2QuotationData($inputs);

        $response = $this->quotationRequest($quotationData);

        return $response;
    }

    /**
     * Step 3 Form Submit Request
     *
     * @param array $inputs
     * @return Response
     */
    public function step3QuotationRequest($inputs) {
        $this->authenticate();
        
        $quotationData = $this->generator->step3QuotationData($inputs);

        $response = $this->quotationRequest($quotationData);

        return $response;
    }

    /**
     * Get quote calculation tariff
     *
     * @param array $inputs
     * @return Response
     */
    public function quotationTariffRequest($inputs) {
        $this->authenticate();
        
        $quotationData = $this->generator->quotationTariff($inputs);

        $response = $this->getHttpClient()->post(self::ROUTE_QUOTATION_TARIFF, [
            'form_params' => [
                'error' => 'Unknown error.',
            ],
            'query' => $quotationData,
            // 'debug' => true,
        ]);

        return $response;
    }

    public function occupationDataRequest() {
        $this->authenticate();

        $response = $this->getHttpClient()->post(static::ROUTE_OCCUPATION_DATA, [
            'query' => $this->generator->occupationData(),
        ]);

        return $response;
    }

    public function vehicleClassDataRequest() {
        $this->authenticate();

        return $this->getHttpClient()->post(static::ROUTE_CAR_CLASS_DATA, [
            'query' => $this->generator->vehicleClassParameters(),
        ]);
    }

    public function extraCoverageRequest() {
        $extraCoverageMap = [
            'cart' => 700,
            'tinting' => $this->generator->extraCoverages['tinting'],
            'specialPeril' => $this->getSharedQuotationData('SUM', 26000),
            'liabilityPass' => '-',
            'laminated' => $this->generator->extraCoverages['laminated'],
        ];

        foreach ($this->generator->selectedExtraCoverages as $key => $extraCoverage) {
            if(isset(static::$extraCoverageCode[$extraCoverage])) {
                $response = $this->getHttpClient()->post(static::ROUTE_QUOTATION_EXTRACOV_ADD, [
                    'query' => $this->generator->addExtraCoveragePayload(
                        static::$extraCoverageCode[$extraCoverage],
                        $extraCoverageMap[$extraCoverage]
                    ),
                ]);
            }
        }
    }

    public function extraCoverageTableRequest() {
        // get extra rate 
        $response = $this->getHttpClient()->post('/ecover/common/table/table4.jsp', [
            'query' => [
                'tableName' => 'tableForm',
                'submitTo' => '/ecover/motor/pop_cnMB_extra2.jsp?TYPE=remove&VEHCLS=KHBVMR&SUBCLS=CO&VDMO_PREM_FACTOR=CO&VSRATE=1.0',
                'target' => '',
                'keyFieldNo' => 'EXTRACODE',
            ],
        ]);

        return $response;
    }

    public function additionalDriverRequest($inputs) {
        $drivers = static::$drivers;

        $results = collect([]);

        if(count($drivers) <= 1) {
            return $results;
        }

        foreach ($drivers as $key => $driver) {
            if($key == 0) {
                $ic = new IcData($driver['icNum']);

                $response = $this->getHttpClient()->post('/ecover/motor/retrieve_info_contact.jsp', [
                    'query' => [
                        'FORM' => 'mainform',
                        'TYPE' => 'CONTACTLOAD',
                        'LOADIND' => 'contact',
                        'CNTYPE' => 'NWOO',
                        'CONTACT_INITIAL' => 'Y',
                        'CONTACT_TYPE' => 'I',
                        'ALTCONTACTID' => '',
                        'NEW_IC_NO' => $ic->number,
                        'OLD_IC_NO' => '',
                        'BUSINESS_NO' => '',
                        'NAME' => '',
                    ],
                ]);
            }
            else {
                $response = $this->getHttpClient()->get('/ecover/motor/pop_cnND_extra.jsp', [
                    'query' => $this->generator->additionalDriver($driver, $inputs),
                ]);
            }

            $results->push((string) $response->getBody());
        }

        return $results;
    }

    public function loanCompanyDataRequest() {
        $this->authenticate();

        $response = $this->getHttpClient()->post('/ecover/motor/retrieve_fintype.jsp?FORM=mainform&LOADIND=normal&CNTYPE=NWOO&PREVPOL=&FINTYPE=HP&LOANCOM=');

        return $response;
    }

    public function calculateExcessRequest($inputs) {
        return $this->getHttpClient()->post('/ecover/common/calculation/calExcess.jsp', [
            'query' => $this->generator->calculateExcess($inputs),
        ]);
    }

    public function checkIsmNCDRequest($inputs) {
        return $this->getHttpClient()->post('/ecover/common/check/checkIsmNcd_step4.jsp', [
            'query' => $this->generator->checkIsmNCD($inputs),
            'form_params' => [
                "classtype" => "",
                "data" => "",
            ],
        ]);
    }

    public function getStateCodeRequest($postCode) {
        return $this->getHttpClient()->post('/ecover/motor/retrieve_info_contact.jsp', [
            'query' => [
                "FORM" => "mainform",
                "TYPE" => "POSTCODE",
                "POSTCODE" => $postCode,
            ],
        ]);
    }
    
}
