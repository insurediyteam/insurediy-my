<?php 

namespace Services;

use Models\Eloquent\Detailable;
use Models\Eloquent\LoanCompany;
use Models\Eloquent\LoanCompanyProvider;
use Services\Client\BaseListData;

class LoanCompanyClientSync extends BaseClientSync {

    protected function reset($switch) {
        if($switch) {
            LoanCompany::getQuery()->delete();
            LoanCompanyProvider::getQuery()->delete();
            Detailable::where('detailable_type', LoanCompanyProvider::class)->delete();
        }
    }

    protected function getLoanCompanyList() {
        return $this->client->loanCompany->listData();
    }

    public function createLoanCompany(BaseListData $listData) {
        $loanCompany = LoanCompany::firstOrCreate(
            [
                'name' => $listData->description,
            ],
            [
                'code' => $this->codeSlug($listData->description),
            ]
        );

        $messageDescription = "loanCompany #{$loanCompany->id}: {$listData->description}" . PHP_EOL;
        if($loanCompany->wasRecentlyCreated) {
            echo "Created new {$messageDescription}";
        }
        else {
            echo "Skipped {$messageDescription}";
        }

        return $loanCompany;
    }

    public function createLoanCompanyProvider(LoanCompany $loanCompany, BaseListData $listData) {
        return LoanCompanyProvider::firstOrCreate([
                'hash' => $listData->getHash([
                    'loan_company_id' => $loanCompany->id,
                    'provider_id' => $this->getModelId(),
                ]),
            ], [
                'loan_company_id' => $loanCompany->id,
                'provider_id' => $this->getModelId(),
                'code' => $listData->value,
                'name' => $listData->description,
            ]);
    }

    public function run() {
        foreach($this->getLoanCompanyList() as $listData) {
            $loanCompany = $this->createLoanCompany($listData);

            $loanCompanyProvider = $this->createLoanCompanyProvider($loanCompany, $listData);

            $messageDescription = "loanCompany provider ({$this->client->code()}): {$listData->description}" . PHP_EOL;
            if($loanCompanyProvider->wasRecentlyCreated) {
                echo "Created new {$messageDescription}";

                foreach($listData->extras as $extra) {
                    $loanCompanyProvider->detailables()
                        ->create($extra);
                }
            }
            else {
                echo "Skipped {$messageDescription}";
            }
        }
    }

}
