<?php 

namespace Services\AxaClient;

use Exception;
use Concerns\IcData;
use Services\AxaClient;
use Concerns\TransformerTrait;
use Services\BaseClientSubclass;
use Services\Client\ComparisonTableData;

class Transformer extends BaseClientSubclass {

    use TransformerTrait;

    /**
     * airbag:abs
     *
     * @var array
     */
    public static $safeties = [
        '1:no' => '01',
        '2:no' => '02',
        '1:yes' => '05',
        '2:yes' => '06',
        '3:yes' => '07',
    ];

    public static $vehicleFroms = [
        'new' => 'CBN',
        'old' => 'CBU',
    ];

    public static $coverTypes = [
        'C' => 'VPPCO0201',
        'T' => 'VPPTP0201',
    ];

    public static $garages = [
        '1' => '04',
        '2' => '05',
    ];

    public static $antitds = [
        '1' => '12',
        '2' => '13',
        '3' => '11',
        '4' => '10',
        '5' => '19',
    ];

    public static $provNCDs = [
        '2' => '0.00',
        '3' => '25.00',
        '4' => '30.00',
        '5' => '38.33',
        '6' => '45.00',
        '7' => '55.00',
    ];

    public static $genders = [
        'MALE' => 'M',
        'FEMALE' => 'F',
    ];

    public static $maritalstats = [
        'Married' => 'M',
        'Single' => 'S',
    ];

    public static $areas = [
        '1' => 'B|SELANGOR',
        '2' => 'J|JOHOR',
        '3' => 'K|KEDAH',
        '4' => 'D|KELANTAN',
        '5' => 'L|LABUAN',
        '6' => 'M|MELAKA',
        '7' => 'N|NEGERI SEMBILAN',
        '8' => 'C|PAHANG',
        '9' => 'A|PERAK',
        '10' => 'R|PERLIS',
        '11' => 'P|PULAU PINANG',
        '12' => 'S|SABAH',
        '13' => 'Q|SARAWAK',
        '14' => 'T|TERENGGANU',
    ];

    /**
     * Transform to compatible generator overrides
     *
     * @param array $inputs
     * @return array
     */
    public function inputs($inputs) {
        // convert to optional array, returns null when calling undefined key / index / props
        $inputs = $this->optional($inputs);
        
        $mainDriver = $this->getMainDriver($inputs);
        
        $transformedInputs = [
            'preregno' => $inputs['vehicleRegNo'],
            'vehregno' => $inputs['vehicleRegNo'],
            'bodyID' => $this->getCarBodyTypeCode($inputs['carBodyType'], false),
            'chassisno' => $inputs['chassisNo'],
            'engineno' => $inputs['engineNo'],
            'make_ID' => $this->getCarMakeCode($inputs['carMake']),
            'model_ID' => $this->getCarModelCode($inputs['carModel']),
            'condition' => $inputs['carCondition'],
            'preinsurer' => $inputs['currentInsurer'],
            'makeyear' => $inputs['carMakeYear'],
            'provCapacity' => $inputs['vehicleEngineCc'],
            'abimarketval' => $inputs['marketVal'],
            'abisumins' => $inputs['marketVal'],
            'sum_insured' => $inputs['marketValue'],
            'safety' => $this->select('safeties', $inputs['airbags'] . ':' . $inputs['abs']),
            'vehiclefrom' => $this->select('vehicleFroms', $inputs['vehType']),
            'seat' => $inputs['seatNumber'],
            'cover_type' => $this->select('coverTypes', $inputs['coverType']),
            'garage' => $this->select('garages', $inputs['garage']),
            'antitd' => $this->select('antitds', $inputs['antiTeft']),
            'drvexp' => $inputs['drivingExp'],
            'provNCD' => $this->select('provNCDs', $inputs['ncd']),
            'claimyr' => $inputs['damageOwn'],
            'claimyr1' => $inputs['windscreen'],
            'claimyr2' => $inputs['theft'],
            'claimyr3' => $inputs['tpClaim'],
            'area' => $this->select('areas', $inputs['location']),
            'occupation' => $this->getOccupationCode($inputs['occupation'], $this->config('filter_occupation', false)),
            'icnumber' => $this->getMainDriverIcNumber($inputs),
            'postcode' => $mainDriver['postalCode'],
        ];

        /**
         * Override sum insured by the agreed value
         */
        if ($inputs['sumInsured']) {
            $transformedInputs['sum_insured'] = $inputs['sumInsured'];
            $this->client->setSumInsured(intval($inputs['sumInsured']));
        }

        // transform optional driver data
        $this->transformDriver($transformedInputs, $inputs);

        AxaClient::$drivers = $this->getDrivers($inputs);
        if(count(AxaClient::$drivers) > 1) {
            $transformedInputs['piamdrv'] = '02';
        }

        // filter, remove null-equal values
        return $this->filterValue($transformedInputs);
    }

    protected function transformDriver(&$transformedInputs, $inputs) {
        $drivers = $this->getDrivers($inputs);

        if (count($drivers) === 0) {
            return ;
        }

        $driver = $drivers[0];

        $icData = new IcData($driver['icNum']);
        
        $transformedInputs['gender'] = $this->select('genders', $driver['Gender']);
        $transformedInputs['nametitle'] = $driver['salutation'];
        $transformedInputs['DOB'] = $icData->dob->format('d/m/Y');
        $transformedInputs['maritalstat'] = $this->select('maritalstats', $driver['maritalStatus']);
        $transformedInputs['drvexp'] = $driver['yearsOfDrivingExp'];
        $transformedInputs['address1'] = $driver['addressOne'];
        $transformedInputs['address2'] = $driver['addressTwo'];
        $transformedInputs['new_ic1'] = $icData->split->first();
        $transformedInputs['new_ic2'] = $icData->split->second();
        $transformedInputs['new_ic3'] = $icData->split->third();
        $transformedInputs['email'] = $driver['email'];
    }

}
