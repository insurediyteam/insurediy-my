<?php 

namespace Services\AxaClient;

use Carbon\Carbon;
use Concerns\IcData;
use Concerns\GeneratorTrait;
use Services\AxaClient;
use Services\BaseClientSubclass;

class Generator extends BaseClientSubclass {

    use GeneratorTrait;

    public static $extraCoverageValues = [
        'cart' => 'CART|CART|CART|P',
        'specialPeril' => 'LIMI|LIMI|LIMI|J',
        'liabilityPass' => 'LLOP|LLOP|LLOP|B',
        'strike' => 'SRCC|SRCC|SRCC|J',
        'waiver' => 'WAIV|WAIV|WAIV|9',
        'laminated' => 'WSCR|WSCR|WSCR|E',
        'tinting' => 'WSCR|WSCR|WSCR|E',
    ];

    public function getCew() {
        $extraCoverageValues = static::$extraCoverageValues;

        return collect(array_intersect($this->selectedExtraCoverages, array_keys(static::$extraCoverageValues)))
            ->map(function($extraCov) use ($extraCoverageValues) {
                return $extraCoverageValues[$extraCov];
            })
            ->unique()
            ->values()
            ->toArray();
    }

    /**
     * Generate template / defaults data for axa quotation
     *
     * @param array $overrides
     * @return array
     */
    public function quotationData(array $overrides = []) {
        $effDate = Carbon::today()->addDay();
        $expDate = $effDate->copy()->addYears(1)->subDay(1);
        $ic = new IcData(isset($overrides['icnumber']) ? $overrides['icnumber'] : '930906-14-7043');

        $defaults = [
            'DOB' => $ic->dob->format('d/m/Y'),
            'ID' => 0,
            'abicheckflag' => '',
            'abimarketval' => '36000.0',
            'abiminsumins' => '',
            'abisumins' => '36000.0',
            'address1' => 'GERAI NO 1 LRG MASJID 34250',
            'address2' => 'TANJONG PIANDANG',
            'address3' => '',
            'agreeVal' => '',
            'agreedIndicator' => '',
            'antitd' => '12',
            'area' => 'A|PERAK',
            'bodyID' => 'SALOON|SALOON',
            'capacitycode' => 'CC',
            'chassisno' => 'PMHGM2660CD203734',
            'checkAgreed' => '',
            'chktick' => 'P',
            'claimfreeyear' => '0',
            'claimyr' => '0',
            'claimyr1' => '0',
            'claimyr2' => '0',
            'claimyr3' => '0',
            'condition' => 'u',
            'cover_type' => 'VPPCO0201',
            'debug' => 'Yes',
            'defaultval' => '',
            'drvexp' => '16',
            'edorselicense' => 'N',
            'effdate' => $effDate->format('d/m/Y'),
            'email' => 'vcckaskus@gmail.com',
            'emailH' => '',
            'engineno' => 'L15A74603698',
            'expdate' => $expDate->format('d/m/Y'),
            'factorvalueVEHOWN' => '',
            'fecode' => '',
            'fename' => '',
            'garage' => '04',
            'gender' => 'M',
            'hidAffectingFactor%VEHOWN' => '',
            'hidCheckMandatory' => 'VEHOWN|NO OF VEHICLE OWNED|FT|',
            'hidCounter' => '',
            'hidDivInd%VEHOWN' => 'Y',
            'hidDtcovvehsetcalcexcess' => 'Y',
            'hidDtcovvehsetcalcloading' => 'N',
            'hidDtcovvehsetdtindicator' => 'Y',
            'hidDtcovvehsetismabiind' => 'Y',
            'hidDtcovvehsetshowtariff' => 'N',
            'hidFactor' => 'VEHOWN',
            'hidMainFactCnt' => '1',
            'hidMainFactCnt1' => '0',
            'hidMainFactRBCnt' => '0',
            'hidMainFactRBCnt1' => '0',
            'hidMainFactorAns%VEHOWN' => '',
            'hidMainFactorCode%VEHOWN' => '',
            'hidTariffCoverCode' => '01',
            'hphoneno' => '',
            'id_type' => '1',
            'isCallFromVix' => 'N',
            'isCrossSell' => '',
            'ismmakecode' => '11',
            'ismmodelcode' => '03',
            'lessor' => '',
            'lessor2' => '',
            'location' => 'W',
            'logbookno' => '12345678',
            'make_ID' => '11',
            'makeyear' => '2012',
            'maritalstat' => 'S',
            'model_ID' => 'HONDA CITY VTEC 1.5I FACE NEW^4213',
            'moverno' => 'mstpolicyno',
            'mtcycrider' => 'A',
            'name' => 'JOHN DOE',
            'nametitle' => 'MR',
            'new_ic1' => '930906', // id card 930906-14-7043
            'new_ic2' => '14',
            'new_ic3' => '7043',
            'noofveh' => '0',
            'nvic' => 'HH012A',
            'occupation' => '02@NULL@EMPLOYED/SELF:NOT REQ VEH USE',
            'old_ic' => '',
            'ophoneno' => '',
            'passportno' => '',
            'piamdrv' => '03',
            'postcode' => '34251',
            'postfrom' => '',
            'preinsncd' => '0.00',
            'preinsurer' => '',
            'prepoleffdate' => '',
            'prepolexpdate' => '',
            'prepolicyno' => '',
            'preregno' => 'PKV8098',
            'prevagentcode' => '37337',
            'previousCrossSellCoverType' => '',
            'previousCrossSellCvnoteNo' => '',
            'previousCrossSellProductClass' => '',
            'previousCrossSellProductType' => '',
            'prevquoteno' => '',
            'prevstatus' => 'O',
            'provCapacity' => '1497',
            'provNCD' => '0.00',
            'purchasedate' => '',
            'purchaseprice' => '',
            'purpose' => 'NB',
            'rangeStatus' => '',
            'renewyear' => '0',
            'rtn' => '0',
            'safety' => '07',
            'seat' => '5',
            'sidecar' => 'N',
            'sum_insured' => '36000.0',
            'svaction' => 'EXTBEN',
            'town' => '',
            'tphoneno' => '60123456789',
            'tphonenoH' => '',
            'usage' => 'VPP0201',
            'uwagreedvalue' => 'Y',
            'vehicle' => 'VPP',
            'vehiclefrom' => 'CBN',
            'vehregno' => 'PKV8098',
        ];

        $merged = $this->mergeAndOverride($defaults, $overrides, true);

        return $merged;
    }

    /**
     * generate second (step 2) quotation
     *
     * @param array $overrides
     * @return array
     */
    public function secondQuotationData($overrides = []) {
        $windscreen = $this->extraCoverages['laminated'] ?: $this->extraCoverages['tinting'];
        $this->extraCoverages['laminated'] = $this->extraCoverages['laminated'] = $windscreen;
        
        $defaults = [
            'CART' => 50,
            'UNIT' => 7,
            'CARTSI' => $this->extraCoverages['cart'],
            'DESCACER|ACER|ACER|E' => 'VEHICLE ACCESSORIES - ROOF TOP/CANOPY',
            'DESCACES|ACES|ACES|E' => 'VEHICLE ACCESSORIES - RADIO/CD/MULTIMEDIA PLAYER',
            'DESCAFLD|AFLD|AFLD|B' => 'AXA FLEXI DRIVE',
            'DESCALLD|ALLD|ALLD|B' => 'ALL DRIVERS',
            'DESC' . static::$extraCoverageValues['cart'] => 'COMPENSATION FOR ASSESSED REPAIR TIME',
            'DESCDPPA|DPPA5B|DPPA5B|B' => 'DRIVER AND PASSENGERS PERSONAL ACCIDENT - PLAN B',
            'DESCDPPA|DPPA5C|DPPA5C|B' => 'DRIVER AND PASSENGERS PERSONAL ACCIDENT - PLAN C',
            'DESCDPPA|DPPA5H|DPPA5H|B' => 'DPP - PLAN A (WITH UNLIMITED TOWING)',
            'DESCDPPA|DPPA5I|DPPA5I|B' => 'DPP - PLAN B (WITH UNLIMITED TOWING)',
            'DESCDPPA|DPPA5J|DPPA5J|B' => 'DPP - PLAN C (WITH UNLIMITED TOWING)',
            'DESCDPPA|DPPA5|DPPA5|B' => 'DRIVER AND PASSENGERS PERSONAL ACCIDENT - PLAN A',
            'DESCEXTH|EXTH|EXTH|T' => 'EXTENSION TO THAILAND',
            'DESCFTHE|FTHE|FTHE|J' => 'FLOOD, TYPHOON, HURRICANE, EARTHQUAKE',
            'DESCLIMI|LIMI|LIMI|J' => 'LIMITED COVER FOR SPECIAL PERILS',
            'DESCLLOP|LLOP|LLOP|B' => 'LEGAL LIABILITY OF PASSENGERS',
            'DESCNCDP|NCDP|NCDP|Y' => 'NCD RELIEF',
            'DESCNGVT|NGVT|NGVT|E' => 'GAS CONVERSION KIT & TANK',
            'DESCPASS|PASS|PASS|Q' => 'PASSENGER LIABILITY COVER- P/C',
            'DESCSRCC|SRCC|SRCC|J' => 'STRIKE RIOT & CIVIL COMMOTION',
            'DESCWAIV|WAIV|WAIV|9' => 'WAIVER OF BETTERMENT',
            'DESCWSCR|WSCR|WSCR|E' => 'BREAKAGE OF GLASS IN WINDSCREENS OR WINDOWS',
            'SIACER|ACER|ACER|E' => '',
            'SIACES|ACES|ACES|E' => '',
            'SINGVT|NGVT|NGVT|E' => '',
            'SIWSCR|WSCR|WSCR|E' => $this->extraCoverages['laminated'] ?: $this->extraCoverages['tinting'],
            'UNITPASS|PASS|PASS|Q' => '0',
            'beneMandatory' => '',
            // 'cew' => ['DPPA|DPPA5H|DPPA5H|B'],
            'cew' => $this->getCew(),
            'classcode' => 'M',
            'gotPPP' => 'false',
            'helpbencode' => '',
            'insureddob' => $this->getSharedQuotationData('DOB', '06/09/1993'),
            'ismResCode' => '008',
            'motorBeneCheck' => '',
            'phoneNo' => $this->getSharedQuotationData('tphoneno', '60123456789'),
            'regNo' => $this->getSharedQuotationData('vehregno', 'PKV8098'),
            'riderBeneCheck' => '',
            'subclasscode' => $this->getSharedQuotationData('vehicle', 'VPP'),
            'svaction' => 'ADD_BEN',
        ];

        return $this->mergeAndOverride($defaults, $overrides);
    }

    public function additionalDrivers() {
        if(count(AxaClient::$drivers) <= 1) {
            return null;
        }

        $payload = http_build_query([
            'ismResCode' => '',
            'purpose' => 'NB',
            'svaction' => 'ADD_DRV',
        ]);
        foreach (AxaClient::$drivers as $key => $driver) {
            $ic = new IcData($driver['icNum']);

            $payload .= "&" . http_build_query([
                'newic' => $ic->number,
                'drvname' => $driver['name'],
                'drvexp' => $driver['yearsOfDrivingExp'],
                'oldic' => '',
                'age' => $ic->dob->age,
                'drvdob' => $ic->dob->format('d/m/Y'),
                'drvgender' => $ic->gender == 'female' ? 'F' : 'M',
                'drvrelation' => $key == 0 ? 'INSD' : '',
                'occupation' => $key == 0 ? $this->client->transformer->getOccupationCode($driver['driverOccupation'], false) : '',
            ]);
        }

        return $payload;
    }

}
