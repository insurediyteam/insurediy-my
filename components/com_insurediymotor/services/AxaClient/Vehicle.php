<?php 

namespace Services\AxaClient;

use Services\BaseClientSubclass;
use Services\Client\BaseListData;
use Services\Contracts\Client\Subclass\VehicleContract;

class Vehicle extends BaseClientSubclass implements VehicleContract {

    const ROUTE_CAR_INFO = '/Agency/Ajax/ajaxVix.jsp';
    const ROUTE_CAR_MARKET_INFO = '/Agency/Ajax/ajaxIsm.jsp';
    const ROUTE_CAR_MODEL = '/Agency/Ajax/getVehMakeMinor.jsp';

    /**
     * Get vehicle data by it's plate number
     *
     * @param string $plateNumber
     * @return array
     */
    public function data($plateNumber, $condition = 'u', $type = 'VPP') {
        $this->authenticate();

        $queryParams = [
            'vehRegno' => $plateNumber,
            'vehCondition' => $condition,
            'vehtype' => $type,
        ];

        $vehicleDataXmlString = (string) $this->getHttpClient()->get(self::ROUTE_CAR_INFO, [
            'query' => $queryParams,
        ])->getBody();

        return $this->xmlToArray($vehicleDataXmlString);
    }

    /**
     * Get vehicle market info
     *
     * @param array $vehicleData
     * @return array
     */
    public function marketInfo($vehicleData, $overrides = []) {
        $this->authenticate();

        if($vehicleData['ismResCode'] === 'NULL') {
            return [];
        }
        
        // get veh value
        $queryParams = array_merge([
            'purpose' => 'NB',
            'oriManYear' => $vehicleData['resVehYearMake'],
            'compcode' => '07',
            'vehregno' => $vehicleData['vehRegNo'],
            'make' => $vehicleData['resVehMake'],
            'model' => $vehicleData['resVehModel'],
            'inputcap' => $vehicleData['resVehCapacity'],
            'vehclass' => $vehicleData['resVehClass'],
            'makeyear' => $vehicleData['resVehYearMake'],
            'chassisno' => $vehicleData['resVehChassisNo'],
            'effdate' => null,
            'vehuse' => $vehicleData['resVehUse'],
            'condition' => $vehicleData['vehCondition'],
            'nvic' => $vehicleData['resVehNvic'],
            'region' => 'W',
            'covertype' => $vehicleData['resVehCoverType'],
            'vehtypecode' => 'VPP',
        ], $overrides);
        
        $vehicleMarketDataXmlString = (string) $this->getHttpClient()->get(self::ROUTE_CAR_MARKET_INFO, [
            'query' => $queryParams,
        ])->getBody();

        return $this->xmlToArray($vehicleMarketDataXmlString);
    }

    /**
     * Get all vehicle data
     *
     * @param string $plateNumber
     * @param string $condition
     * @param string $type
     * @param array $overrides
     * @return array
     */
    public function allData($plateNumber, $condition = 'u', $type = 'VPP', $overrides = []) {
        $vehicleData = $this->data($plateNumber, $condition, $type);
        $chassis = $this->optionalArray($vehicleData, 'resVehChassisNo');
        $engine = $this->optionalArray($vehicleData, 'resVehEngineNo');
        $cc = $this->optionalArray($vehicleData, 'resVehCapacity');
        $year = $this->optionalArray($vehicleData, 'resVehYearMake');
        $make = $this->optionalArray($vehicleData, 'resVehMake');
        $modelVal = $this->optionalArray($vehicleData, 'resVehModel');
        $model = $this->optionalArray($vehicleData, 'insminorcode');
        $nvic = $this->optionalArray($vehicleData, 'resVehNvic');
        $modelName = $this->optionalArray($vehicleData, 'insminordesc');
        $vehicleMarketData = $this->marketInfo($vehicleData, $overrides);
        $marketValue = $this->optionalArray($vehicleMarketData, 'marketvalue');

        return compact([
            'vehicleData',
            'vehicleMarketData',
            'chassis',
            'engine',
            'cc',
            'year',
            'make',
            'modelVal',
            'model',
            'nvic',
            'modelName',
            'marketValue',
        ]);
    }

    public function bodyListData() {
        $selectDomObject = $this->step1QuotationFormDom()->filter('select[name=\'bodyID\']')->first();
        
        $data = $this->parser->selectDataFromDom($selectDomObject);

        array_shift($data);

        return array_map(function($value) {
            return new BaseListData($value['value'], $value['text']);
        }, $data);
    }

    public function makeListData() {
        $selectDomObject = $this->step1QuotationFormDom()->filter('select[name=\'make_ID\']')->first();
        
        $data = $this->parser->selectDataFromDom($selectDomObject);

        array_shift($data);

        return array_map(function($value) {
            return new BaseListData($value['value'], $value['text']);
        }, $data);
    }

    public function modelListData($makeCode, $type = null, $cover = null, $use = null) {
        $this->authenticate();

        $response = $this->getHttpClient()->get(self::ROUTE_CAR_MODEL, [
            'query' => [
                'vehmakecode' => $makeCode,
                'vehicle_type' => $type,
                'vehcover' => $cover,
                'vehuse' => $use,
            ],
        ]);
        
        $xmlString = (string) $response->getBody();

        $array = $this->xmlToArray($xmlString);

        if(count($array) == 1) {
            $options = explode('|', $array[0]);
    
            return array_map(function($value) {
                list($name, $code) = explode('^', $value);
    
                return new BaseListData($value, $name);
            }, $options);
        }
        else {
            return [];
        }

    }

}
