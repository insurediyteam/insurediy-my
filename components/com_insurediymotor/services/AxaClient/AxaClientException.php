<?php

namespace Services\AxaClient;

use Exception;

class AxaClientException extends Exception {
    
    public static function agreedValueNotAllowed() {
        return new static('Vehicle does not meet the agreed value criteria: make/model, not within RM20,001 to RM750,000 sumins, more than 10 years old');
    }
    
    public static function unableToGetQuote() {
        return new static('Can\'t get quote!');
    }

}
