<?php 

namespace Services\AxaClient;

use Concerns\ParserTrait;
use Illuminate\Support\Str;
use Services\BaseClientSubclass;
use Services\Client\ComparisonTableData;
use Symfony\Component\DomCrawler\Crawler;

class Parser extends BaseClientSubclass {

    use ParserTrait;

    /**
     * Get select data options from JS variable
     *
     * @param string $html
     * @param string $valueStartDelimiter
     * @param string $valueEndDelimiter
     * @param string $textStartDelimiter
     * @param string $textEndDelimiter
     * @return array
     */
    public function selectDataFromJs($html, $valueStartDelimiter, $valueEndDelimiter, $textStartDelimiter, $textEndDelimiter) {
        $self = &$this;

        $valuesStr = $this->stringBetween($html, $valueStartDelimiter, $valueEndDelimiter);

        $values = array_map(
                function($value) use ($self) {
                return $self->trim(str_replace("'", '', $value));
            }, 
            explode(',', $valuesStr[0])
        );

        $textsStr = $this->stringBetween($html, $textStartDelimiter, $textEndDelimiter);

        $texts = array_map(
                function($value) use ($self) {
                return $self->trim(str_replace("'", '', $value));
            }, 
            explode(',', $textsStr[0])
        );

        $data = [];

        for($i = 0; $i < count($values); $i++) {
            array_push($data, [
                'value' => $values[$i],
                'text' => $texts[$i],
            ]);
        }
        
        return $data;
    }

    /**
     * Parse quotation html data to array
     *
     * @param string $htmlString
     * @return array
     */
    public function quotationData($htmlString) {
        $dom = $this->htmlToDom($htmlString);

        $premiumPricingTableRows = $dom->filter('body table table table')
            ->first()
            ->filter('tr');

        $quotations = [];

        $self = &$this;

        $premiumPricingTableRows->each(function(Crawler $node, $i) use (&$quotations, $self) {
            /** @var AxaClient $self */
            $tableDatas = $node->filter('td');

            if (count($tableDatas) != 3) {
                return false;
            }

            $name = $this->trim($tableDatas->eq(0)->text());
            $value = $this->trim($tableDatas->eq(2)->text());
            
            if(Str::contains($name, 'NCD')) {
                preg_match('/([0-9\.]+)/', $name, $matches);
                if(count($matches)) {
                    $self->addCoverTypeAndExcesses(
                        new ComparisonTableData(
                            ComparisonTableData::GROUP_NCD_PERCENT[0],
                            ComparisonTableData::GROUP_NCD_PERCENT[1],
                            $matches[0] . '%'
                        )
                    );
                }
            }

            array_push($quotations, [
                'name' => $name,
                'value' => $value,
            ]);

            $self->addToExtraCoverageSumIns($name, $value);
        });

        $excesses = $dom->filter('#theForm > table > tbody > tr:nth-child(2) > td > table:nth-child(1) > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(2)')->first()->text();
        $excesses = explode(' / ', $this->trim($excesses));
        /** @var \Services\AxaClient $this */
        $this->addCoverTypeAndExcesses(
            new ComparisonTableData(
                ComparisonTableData::GROUP_OWN_DAMAGE_EXCESS[0],
                ComparisonTableData::GROUP_OWN_DAMAGE_EXCESS[1],
                ComparisonTableData::valueWithIcon($excesses[0], true)
            )
        );
        $this->addCoverTypeAndExcesses(
            new ComparisonTableData(
                ComparisonTableData::GROUP_COMPULSORY_EXCESS[0],
                ComparisonTableData::GROUP_COMPULSORY_EXCESS[1],
                ComparisonTableData::valueWithIcon($excesses[1], true)
            )
        );

        return $quotations;
    }

    public function addToExtraCoverageSumIns($name, $value) {
        $maps = [
            [
                'COMPENSATION FOR ASSESSED REPAIR TIME',
                'cart',
            ],
            [
                'LIMITED COVER FOR SPECIAL PERILS',
                'specialPeril',
            ],
            [
                'LEGAL LIABILITY OF PASSENGERS',
                'liabilityPass',
            ],
            [
                'STRIKE RIOT & CIVIL COMMOTION',
                'strike',
            ],
            [
                'WAIVER OF BETTERMENT',
                'waiver',
            ],
            [
                'BREAKAGE OF GLASS IN WINDSCREENS OR WINDOWS',
                'laminated',
            ],
        ];

        $found = null;
        foreach ($maps as $key => $map) {
            if(Str::contains($name, $map[0])) {
                $found = $map;

                break;
            }
        }

        if($found) {
            /** @var \Services\AxaClient $this */
            $this->setExtraCoverageSumInsured($map[1], $value);
        }
    }

}
