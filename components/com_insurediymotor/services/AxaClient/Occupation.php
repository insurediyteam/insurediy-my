<?php 

namespace Services\AxaClient;

use Services\BaseClientSubclass;
use Services\Client\BaseListData;
use Services\Contracts\Client\Subclass\OccupationContract;

class Occupation extends BaseClientSubclass implements OccupationContract {

    public function listData() {
        $selectDatas = $this->parser
            ->selectDataFromJs(
                $this->step1QuotationForm(),
                'var Ooccupcode=new Array(',
                ');',
                'var Ooccupdesc=new Array(',
                ');'
            );

        $data = array_map(function($option) {
            return new BaseListData($option['value'] . '@' . $option['text'], $option['text']);
        }, $selectDatas);

        array_shift($data);

        return $data;
    }

}
