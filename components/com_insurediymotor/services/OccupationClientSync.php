<?php 

namespace Services;

use Models\Eloquent\Detailable;
use Models\Eloquent\Occupation;
use Models\Eloquent\OccupationProvider;
use Services\Client\BaseListData;

class OccupationClientSync extends BaseClientSync {

    protected function reset($switch) {
        if($switch) {
            Occupation::getQuery()->delete();
            OccupationProvider::getQuery()->delete();
            Detailable::where('detailable_type', OccupationProvider::class)->delete();
        }
    }

    protected function getOccupationList() {
        return $this->client->occupation->listData();
    }

    public function createOccupation(BaseListData $listData) {
        $occupation = Occupation::firstOrCreate(
            [
                'name' => $listData->description,
            ],
            [
                'code' => $this->codeSlug($listData->description),
            ]
        );

        $messageDescription = "occupation #{$occupation->id}: {$listData->description}" . PHP_EOL;
        if($occupation->wasRecentlyCreated) {
            echo "Created new {$messageDescription}";
        }
        else {
            echo "Skipped {$messageDescription}";
        }

        return $occupation;
    }

    public function createOccupationProvider(Occupation $occupation, BaseListData $listData) {
        return OccupationProvider::firstOrCreate([
                'hash' => $listData->getHash([
                    'occupation_id' => $occupation->id,
                    'provider_id' => $this->getModelId(),
                ]),
            ], [
                'occupation_id' => $occupation->id,
                'provider_id' => $this->getModelId(),
                'code' => $listData->value,
                'name' => $listData->description,
            ]);
    }

    public function run() {
        foreach($this->getOccupationList() as $listData) {
            $occupation = $this->createOccupation($listData);

            $occupationProvider = $this->createOccupationProvider($occupation, $listData);

            $messageDescription = "occupation provider ({$this->client->code()}): {$listData->description}" . PHP_EOL;
            if($occupationProvider->wasRecentlyCreated) {
                echo "Created new {$messageDescription}";

                foreach($listData->extras as $extra) {
                    $occupationProvider->detailables()
                        ->create($extra);
                }
            }
            else {
                echo "Skipped {$messageDescription}";
            }
        }
    }

}
