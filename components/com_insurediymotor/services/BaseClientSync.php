<?php 

namespace Services;

use Exception;
use Illuminate\Support\Str;
use Services\Contracts\Client\Subclass\VehicleContract;

abstract class BaseClientSync {

    /** @var BaseClient */
    protected $client;

    public function __construct($client, $reset = false)
    {
        if(!$client instanceof BaseClient) {
            throw new Exception('Invalid BaseClient');
        }
        
        $this->client = $client;

        $this->reset($reset);
    }

    /**
     * Reset database
     *
     * @param bool $switch
     * @return void
     */
    abstract protected function reset($switch);

    /** @return VehicleContract */
    protected function getClientVehicle() {
        return $this->client->vehicle;
    }

    protected function getModel() {
        return $this->client->getModel();
    }

    protected function getModelId() {
        return $this->getModel()->id;
    }

    protected function codeSlug($codeString) {
        return Str::slug(str_replace('.', ' ', Str::lower($codeString)));
    }

    /**
     * Run the service
     *
     * @return void
     */
    abstract public function run();
    
}
