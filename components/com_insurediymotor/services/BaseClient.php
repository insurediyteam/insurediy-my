<?php 

namespace Services;

use Concerns\InteractsWithJConfig;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use InsureDIYMotorHelper;
use Models\Eloquent\Provider;
use Services\Client\BaseClientException;
use Services\Client\ComparisonTableData;
use Services\Client\ProviderCookieJar;
use Symfony\Component\DomCrawler\Crawler;

abstract class BaseClient {

    use InteractsWithJConfig;

    const BASIS_COVERAGE_AGREED_VALUE = 'Agreed Value';
    const BASIS_COVERAGE_MARKET_VALUE = 'Market Value';

    /** @var array */
    protected $sharedQuotationData = [];

    /** @var Client */
    protected $httpClient;

    /** @var CookieJar */
    protected $cookieJar;

    /** @var int */
    protected $quotationValue;

    /** @var Provider */
    protected $model;

    /** @var float */
    protected $ncdPercent = 0;

    /** @var array */
    protected $coverTypeAndExcesses = [];

    protected $additionalBenefits = [];

    protected $others = [];

    public $agreedValue = true;

    public $sumInsured = null;

    public $basisCoverage = 'Agreed Value';

    /** @var array */
    public $extraCoverageSumInsureds = [
        'cart' => 0,
        'specialPeril' => 0,
        'liabilityPass' => 0,
        'ehailing' => 0,
        'strike' => 0,
        'waiver' => 0,
        'laminated' => 0,
        'tinting' => 0,
    ];

    public function __construct()
    {
        $this->addCoverTypeAndExcesses(
            new ComparisonTableData(
                ComparisonTableData::GROUP_COVER_TYPE[0],
                ComparisonTableData::GROUP_COVER_TYPE[1],
                'Comprehensive'
            )
        );
        $this->addCoverTypeAndExcesses(
            new ComparisonTableData(
                ComparisonTableData::GROUP_NCD_PERCENT[0],
                ComparisonTableData::GROUP_NCD_PERCENT[1],
                '0.00%'
            )
        );
        $this->setSumInsured(0);
        $this->setBasisCoverage(static::BASIS_COVERAGE_AGREED_VALUE);
        
        $this->addCoverTypeAndExcesses(
            new ComparisonTableData(
                ComparisonTableData::GROUP_OWN_DAMAGE_EXCESS[0],
                ComparisonTableData::GROUP_OWN_DAMAGE_EXCESS[1],
                ComparisonTableData::valueWithIcon(0, true)
            )
        );
        $this->addCoverTypeAndExcesses(
            new ComparisonTableData(
                ComparisonTableData::GROUP_COMPULSORY_EXCESS[0],
                ComparisonTableData::GROUP_COMPULSORY_EXCESS[1],
                ComparisonTableData::valueWithIcon(0, true)
            )
        );

        // populate additional coverages
        foreach (ComparisonTableData::GROUP_EXTRA_COVERAGE_MAP as $key => $value) {
            $this->addAdditionalBenefits(
                new ComparisonTableData(
                    ComparisonTableData::GROUP_EXTRA_COVERAGE_PREFIX . $key,
                    $value,
                    ComparisonTableData::valueWithIcon(0)
                )
            );
        }

        // brochure
        $model = $this->getModel();
        $brochureUrl = "media/com_insurediymotor/providers/{$model->code}/brochure.pdf";
        $downloadUrl = \MyHelper::rootUrl() . "{$brochureUrl}";
        $this->addOthers(
            new ComparisonTableData(
                ComparisonTableData::GROUP_BROCHURES[0],
                ComparisonTableData::GROUP_BROCHURES[1],
                "<a class=\"download2\" href=\"{$downloadUrl}\" onclick=\"window.open(this.href);return false;\"><i class=\"fa fa-download fa-fw\"></i> Download</a>"
            )
        );

        // check lockdown
        $this->checkLockdown();
    }

    protected function checkLockdown() {
        if($this->getModel()->fresh()->lockdown) {
            $exception = BaseClientException::clientLockdown();
            try {
                Bugsnag::getInstance()->notifyException($exception);
            }
            catch(Exception $e) {
                // ignore
            }
            throw $exception;
        }
    }

    public function doLockDown() {
        $model = $this->getModel();
        $model->update([
            'lockdown' => 1,
        ]);

        $this->checkLockdown();
    }

    /**
     * get Base URL
     *
     * @return string
     */
    abstract public function getBaseUri();

    /**
     * Get cookie jar
     *
     * @return CookieJar
     */
    public function getCookieJar() {
        if(!isset($this->cookieJar)) {
            $this->cookieJar = new ProviderCookieJar($this->getModel(), true);
        }

        return $this->cookieJar;
    }

    /**
     * get http client
     *
     * @return Client
     */
    public function getHttpClient() {
        if(!isset($this->httpClient)) {
            $this->httpClient = new Client(array_merge([
                'base_uri' => $this->getBaseUri(),
                'cookies' => $this->getCookieJar(),
                'headers' => [
                    'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0',
                ],
                // 'verify' => JPATH_COMPONENT . '/services/cert/cacert.pem',
                // 'verify' => false,
                // 'debug' => true,
                'allow_redirect' => false,
            ], $this->getHttpClientOptions()));
        }

        return $this->httpClient;
    }

    /**
     * Get options for generating http client
     *
     * @return array
     */
    public function getHttpClientOptions() {
        return [];
    }

    /**
     * Keys to filter generated data
     *
     * @return array
     */
    abstract protected function sharedQuotationDataKeys();

    /**
     * update $this->sahredQuotationData
     *
     * @param array $generatedData
     * @return self
     */
    public function updateSharedQuotationData(array $generatedData) {
        $keys = $this->sharedQuotationDataKeys();

        $this->sharedQuotationData = array_filter($generatedData, function($key) use ($keys) {
            return in_array($key, $keys);
        }, ARRAY_FILTER_USE_KEY);

        return $this;
    }

    /**
     * @param string $index
     * @param mixed $default
     * @return mixed
     */
    public function getSharedQuotationData($index, $default = null) {
        if(isset($this->sharedQuotationData[$index])) {
            return $this->sharedQuotationData[$index];
        }

        return $default;
    }

    /**
     * Configuration key index
     *
     * @return string
     */
    abstract public function configKey();

    /**
     * database code
     *
     * @return string
     */
    abstract public function code();

    /**
     * Get configuration
     *
     * @param string|null $index
     * @return array
     */
    public function config($index = null, $default = null) {
        $componentConfig = $this->getComponentConfig();
        $config = isset($componentConfig[$this->configKey()]) ? $componentConfig[$this->configKey()] : [];
        
        $model = $this->getModel();
        if ($model) {
            $config = array_merge($config, [
                'auth' => [
                    'user' => $model->user,
                    'password' => $model->password,
                ],
            ]);
        }

        if($index) {
            return isset($config[$index]) ? $config[$index] : $default;
        }

        return $config;
    }

    /**
     * Get the value of quotationValue
     */ 
    public function getQuotationValue()
    {
        if(!isset($this->quotationValue)) {
            $this->quotation();
        }

        if(intval($this->quotationValue) === 0) {
            throw new Exception("Failed to get quotation data: Quotation value equals zero.");
        }

        return $this->quotationValue;
    }
    
    /**
     * Set the value of quotationValue
     *
     * @return  self
     */ 
    abstract public function setQuotationValue($quotationData);

    /** 
     * Get database model (Provider) 
     * 
     * @return Provider
     * */
    public function getModel() {
        if (!isset($this->model)) {
            $this->model = Provider::whereCode($this->code())->first();
        }

        return $this->model;
    }

    /**
     * Authenticate to server
     *
     * @return self
     */
    abstract public function authenticate();

    /**
     * Get quotation
     *
     * @param array $inputs
     * @return array
     */
    abstract public function quotation($inputs = []);

    /**
     * convert XML string to array
     *
     * @param string $xmlString
     * @return array
     */
    public function xmlToArray($xmlString)
    {
        return InsureDIYMotorHelper::xmlToArray($xmlString);
    }

    /**
     * convert XML string to array
     *
     * @param string $xmlString
     * @return array
     */
    public function optionalArray(array $array, $key, $default = null)
    {
        return InsureDIYMotorHelper::optionalArray($array, $key, $default);
    }

    /**
     * get DOM object
     *
     * @param string $htmlString
     * @return Crawler
     */
    public function htmlToDom($htmlString) {
        return new Crawler($htmlString);
    }

    /**
     * Get string between 2 delimiter start and end
     *
     * @param string $string
     * @param string $startDelimiter
     * @param string $endDelimiter
     * @return void
     */
    public function stringBetween($string, $startDelimiter, $endDelimiter) {
        return InsureDIYMotorHelper::getContents($string, $startDelimiter, $endDelimiter);
    }

    public function setExtraCoverageSumInsured($name, $value) {
        $this->extraCoverageSumInsureds[$name] = $value;
        $this->addAdditionalBenefits(
            ComparisonTableData::makeExtraCoverageFromClient($this, $name)
        );

    }

    public function getExtraCoverageSumInsured($name)
    {
        return isset($this->extraCoverageSumInsureds[$name]) ? $this->extraCoverageSumInsureds[$name] : null;
    }


    /**
     * Set the value of cookieJar
     *
     * @return  self
     */ 
    public function setCookieJar(CookieJar $cookieJar)
    {
        $this->cookieJar = $cookieJar;

        $this->unsetHttpClient();

        return $this;
    }

    /** Reset Http Client */
    public function unsetHttpClient() {
        unset($this->httpClient);

        return $this;
    }

    public function getCoverageTypeAndExcesses() {
        return $this->coverTypeAndExcesses;
    }

    public function getAdditionalBenefits() {
        return $this->additionalBenefits;
    }

    public function getOthers() {
        return $this->others;
    }

    public function getCompareTableData() {
        return [
            [
                'id' => 'coverTypeAndExcesses',
                'data' => $this->getCoverageTypeAndExcesses(),
            ],
            [
                'id' => 'additionalBenefits',
                'data' => $this->getAdditionalBenefits(),
            ],
            [
                'id' => 'comparisonTableOthers',
                'data' => $this->getOthers(),
            ],
        ];
    }

    public function addCoverTypeAndExcesses(ComparisonTableData $data) {
        $this->addAndOverride($this->coverTypeAndExcesses, $data);
    }

    public function addAdditionalBenefits(ComparisonTableData $data) {
        $this->addAndOverride($this->additionalBenefits, $data);
    }

    public function addOthers(ComparisonTableData $data) {
        $this->addAndOverride($this->others, $data);
    }

    public function addAndOverride(&$comparisonTableList, ComparisonTableData $data) {
        $comparisonTableListCollect = collect($comparisonTableList);

        $seachPrevious = $comparisonTableListCollect->search(function($listData) use ($data) {
            return $listData->id == $data->id;
        });

        if($seachPrevious === false) {
            $comparisonTableListCollect->push($data);
        }
        else {
            $comparisonTableListCollect->put($seachPrevious, $data);
        }

        $comparisonTableList = $comparisonTableListCollect->toArray();
    }

    /**
     * Get the value of sumInsured
     */ 
    public function getSumInsured()
    {
        return $this->sumInsured;
    }

    /**
     * Set the value of sumInsured
     *
     * @return  self
     */ 
    public function setSumInsured($sumInsured)
    {
        $this->sumInsured = $sumInsured;
        
        $this->addCoverTypeAndExcesses(
            new ComparisonTableData(
                ComparisonTableData::GROUP_SUM_INSURED[0],
                ComparisonTableData::GROUP_SUM_INSURED[1],
                ComparisonTableData::valueWithIcon($sumInsured)
            )
        );

        return $this;
    }

    /**
     * Get the value of basisCoverage
     */ 
    public function getBasisCoverage()
    {
        return $this->basisCoverage;
    }

    /**
     * Set the value of basisCoverage
     *
     * @return  self
     */ 
    public function setBasisCoverage($basisCoverage)
    {
        $this->basisCoverage = $basisCoverage;

        $this->addCoverTypeAndExcesses(
            new ComparisonTableData(
                ComparisonTableData::GROUP_BASIS_VALUATION[0],
                ComparisonTableData::GROUP_BASIS_VALUATION[1],
                $basisCoverage
            )
        );

        return $this;
    }
}
