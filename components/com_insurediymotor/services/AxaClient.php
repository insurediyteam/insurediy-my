<?php 

namespace Services;

use Illuminate\Support\Str;
use GuzzleHttp\Psr7\Response;
use Services\AxaClient\Parser;
use Services\AxaClient\Vehicle;
use Services\AxaClient\Generator;
use Services\AxaClient\Occupation;
use Services\AxaClient\Transformer;
use Services\Client\ComparisonTableData;
use Services\AxaClient\AxaClientException;

class AxaClient extends BaseClient {

    const HOST = 'https://w6.financial-link.com.my';

    const ROUTE_LOGIN = '/Agency/LoginServlet/LoginServlet';
    const ROUTE_QUOTATION = '/Agency/Agentquotation/Agentquotation';
    const ROUTE_QUOTATION_FORM = '/Agency/agentquo.jsp';

    public static $guest = true;

    /** @var Vehicle */
    public $vehicle;

    /** @var Generator */
    public $generator;

    /** @var Parser */
    public $parser;

    /** @var Occupation */
    public $occupation;

    /** @var Transformer */
    public $transformer;

    /** @var string */
    public $step1QuotationForm;

    /** @var \Symfony\Component\DomCrawler\Crawler */
    public $step1QuotationFormDom;

    public static $drivers = [];

    /**
     * Instantiate class
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->vehicle = new Vehicle($this);
        $this->generator = new Generator($this);
        $this->parser = new Parser($this);
        $this->occupation = new Occupation($this);
        $this->transformer = new Transformer($this);
    }

    public function getBaseUri() {
        return self::HOST;
    }

    /**
     * Keys to filter generated data
     *
     * @return array
     */
    protected function sharedQuotationDataKeys() {
        return [
            'DOB',
            'vehicle',
            'tphoneno',
            'vehregno',
        ];
    }

    /**
     * Check authentication
     *
     * @return bool
     */
    public function checkAuth() {
        if (! static::$guest) {
            return true;
        }

        $quotationForm = $this->getHttpClient()->get(static::ROUTE_QUOTATION_FORM);

        $response = (string) $quotationForm->getBody();

        return ! Str::contains($response, 'Session Expired');
    }

    /**
     * Configuration key index
     *
     * @return string
     */
    public function configKey() {
        return 'axa';
    }

    public function code() {
        return $this->configKey();
    }

    /**
     * Authenticate to server
     *
     * @return self
     */
    public function authenticate() {
        if(!$this->checkAuth()) {
            // login
            $configAuth = $this->config('auth');

            $loginData = [
                'compcode' => '07',
                'loginURL' => 'loginAXA.jsp',
                'windowName' => 'a41d8867-6a02-4865-8e60-c67e6499bdc5',
                'Submit' => 'Submit',
                'loginID' => $configAuth['user'],
                'pwd' => $configAuth['password'],
            ];
            
            $response = $this->getHttpClient()->post(self::ROUTE_LOGIN, [
                'form_params' => $loginData,
            ]);


            if(strpos((string) $response->getBody(), "<input type='hidden' name='err' value='pswd'>") !== false) {
                $this->doLockDown();
            }

            static::$guest = false;
        }

        return $this;
    }

    /**
     * Do quote request
     *
     * @param array $postData
     * @return Response
     */
    public function quotationRequest($postData) {
        $clientOptions = [
            'form_params' => $postData,
        ];
        
        /**
         * Handles additional coverage
         */
        if(isset($postData['cew'])) {
            $cews = $postData['cew'];
            unset($postData['cew']);

            if(count(static::$drivers) > 1) {
                array_push($cews, "AND|NAME|NAME|A");

                $postData['DESCAND|NAME|NAME|A'] = 'ADDITIONAL NAMED DRIVER';
                $postData['UNITAND|NAME|NAME|A'] = (string) (count(static::$drivers) - 2);
            }

            $postBody = http_build_query($postData);
            foreach($cews as $cew) {
                $postBody .= "&" . http_build_query(['cew' => $cew]);
            }

            $clientOptions = [
                'body' => $postBody,
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
            ];
        }

        return $this->getHttpClient()->post(self::ROUTE_QUOTATION, $clientOptions);
    }

    /**
     * Perform step 1 quotation request
     *
     * @param array $inputs
     * @return Response
     */
    public function step1QuotationRequest($inputs) {
        $quotationInputs = $this->generator->quotationData($inputs);
        
        if($this->agreedValue) {
            $quotationInputs['uwagreedvalue'] = 'Y';
            $quotationInputs['mstpolicyno'] = '';
        }

        return $this->quotationRequest($quotationInputs);
    }

    /**
     * Perform step 2 quotation request
     *
     * @param array $inputs
     * @return Response
     */
    public function step2QuotationRequest($inputs) {
        $quotationInputs = $this->generator->secondQuotationData($inputs);

        return $this->quotationRequest($quotationInputs);
    }

    /**
     * Perform step 3 (multi driver) quotation request
     *
     * @param array $inputs
     * @return Response
     */
    public function step3QuotationRequest() {
        $payload = $this->generator->additionalDrivers();

        return $this->getHttpClient()->post(static::ROUTE_QUOTATION, [
            'body' => $payload,
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
        ]);
    }

    /**
     * Get quotation
     *
     * @param array $inputs
     * @return array
     */
    public function quotation($inputs = []) {
        $this->authenticate();

        $inputs = $this->transformer->inputs($inputs);

        try {
            $this->checkAgreedValueRequest($inputs);
        }
        catch(\Exception $e) {
            $this->agreedValue = false;

            $this->setBasisCoverage(static::BASIS_COVERAGE_MARKET_VALUE);

            if(isset($inputs['sum_insured'])) {
                $vehicleInfo = $this->vehicle->allData($inputs['vehregno']);
                
                $this->setSumInsured(intval($vehicleInfo['marketValue']));

                if(!($vehicleInfo && isset($vehicleInfo['marketValue']) && $vehicleInfo['marketValue'])) {
                    throw AxaClientException::unableToGetQuote();
                }

                $inputs['sum_insured'] = $vehicleInfo['marketValue'];
            }
        }

        $multiDriver = isset($inputs['piamdrv']) && $inputs['piamdrv'] == '02';

        $response = $this->step1QuotationRequest($inputs);

        $response = (string) $this->step2QuotationRequest($inputs)->getBody();

        if($multiDriver) {
            $response = (string) $this->step3QuotationRequest()->getBody();
        }

        $quotationData = $this->parser->quotationData($response);

        $this->setQuotationValue($quotationData);

        return $quotationData;
    }

    /**
     * request to first Quotation form
     *
     * @return Response
     */
    public function step1QuotationFormRequest() {
        $this->authenticate();

        return $this->getHttpClient()->get(self::ROUTE_QUOTATION_FORM);
    }

    /**
     * get first Quotation form html string
     *
     * @return string
     */
    public function step1QuotationForm() {
        if(!isset($this->step1QuotationForm)) {
            $formRequest = $this->step1QuotationFormRequest();
    
            $this->step1QuotationForm = (string) $formRequest->getBody();
        }

        return $this->step1QuotationForm;
    }

    /**
     * get first Quotation form dom object
     *
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function step1QuotationFormDom() {
        if(!isset($this->step1QuotationFormDom)) {
            $this->step1QuotationFormDom = $this->htmlToDom($this->step1QuotationForm());
        }

        return $this->step1QuotationFormDom;
    }
    
    /**
     * Set the value of quotationValue
     *
     * @return  self
     */ 
    public function setQuotationValue($quotationData)
    {
        $this->quotationValue = last($quotationData)['value'];

        return $this;
    }

    public function checkAgreedValueRequest($data) {
        if(!$this->agreedValue) {
            return true;
        }

        if(!isset($data['makeyear'])) {
            return true;
        }

        if(!isset($data['condition'])) {
            return true;
        }

        if(!isset($data['sum_insured'])) {
            return true;
        }

        if(!isset($data['make_ID'])) {
            return true;
        }

        if(!isset($data['model_ID'])) {
            return true;
        }

        $response = $this->getHttpClient()->get('/Agency/Ajax/checkUwAgreedVal.jsp', [
            'query' => [
                "coverCode" => "VPPCO0201",
                "vehMakeYear" => $data['makeyear'],
                "vehTypeCode" => "VPP",
                "vehUseCode" => "VPP0201",
                "condition" => $data['condition'],
                "sumIns" => $data['sum_insured'],
                "abiSumIns" => $data['sum_insured'],
                "abiMarketVal" => $data['sum_insured'],
                "vehMakeCode" => $data['make_ID'],
                "vehModel" => $data['model_ID'],
            ],
        ]);

        $responseBody = trim((string) $response->getBody());

        if(!$responseBody) {
            throw AxaClientException::agreedValueNotAllowed();
            
            return false;
        }

        $xmlArray = $this->xmlToArray($responseBody);
        $isAgreedValue = $xmlArray[0] == 'true';

        if(!$isAgreedValue) {
            throw AxaClientException::agreedValueNotAllowed();
        }
    }

}
