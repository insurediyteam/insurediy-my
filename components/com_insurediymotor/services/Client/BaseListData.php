<?php 

namespace Services\Client;

class BaseListData {

    public $value;

    public $description;

    public $extras;

    public function __construct($value, $description, $extras = [])
    {
        $this->value = $value;

        $this->description = $description;

        $this->extras = $extras;
    }

    public function toArray() {
        return [
            'value' => $this->value,
            'description' => $this->description,
            'extras' => $this->extras,
        ];
    }

    /**
     * Get this object hash
     *
     * @param array $overrides
     * @return string
     */
    public function getHash($overrides = []) {
        $id = array_merge($this->toArray(), $overrides);

        return md5(json_encode($id));
    }

}
