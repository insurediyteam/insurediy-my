<?php 

namespace Services\Client;

use ArrayAccess;
use Countable;

class OptionalArray implements ArrayAccess, Countable {

    /** @var mixed */
    protected $defaultReturn = null;

    /** @var array */
    protected $container;

    public function __construct($array = [])
    {
        $this->container = $array;
    }

    /** @return bool */
    public function offsetExists($offset) {
        return isset($this->container[$offset]);
    }

    /** @return mixed */
    public function offsetGet($offset) {
        return $this->__get($offset);
    }

    /** @return void */
    public function offsetSet($offset, $value) {
        $this->container[$offset] = $value;
    }

    /** @return void */
    public function offsetUnset($offset) {
        unset($this->container[$offset]);
    }

    /** @return int */
    public function count() {
        return count($this->container);
    }

    /** @return mixed */
    public function __get($property) {
        return $this->offsetExists($property) ? $this->container[$property] : $this->defaultReturn;
    }

    /** @return array */
    public function toArray() {
        return $this->container;
    }

    /** @return object */
    public function toObject() {
        return (object) $this->container;
    }

    public function __toString()
    {
        return json_encode($this->container);
    }


    /**
     * Set the value of defaultReturn
     *
     * @return  self
     */ 
    public function setDefaultReturn($defaultReturn)
    {
        $this->defaultReturn = $defaultReturn;

        return $this;
    }
}
