<?php
namespace Services\Client;

use Exception;
use Models\Eloquent\Provider;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * Persists cookies attached for each client
 */
class ProviderCookieJar extends CookieJar
{
    /** @var Provider provider */
    private $provider;

    /** @var bool Control whether to persist session cookies or not. */
    private $storeSessionCookies;

    /**
     * Create a new ProviderCookieJar object
     *
     * @param string $providerModel        File to store the cookie data
     * @param bool $storeSessionCookies Set to true to store session cookies
     *                                  in the cookie jar.
     *
     * @throws \RuntimeException if the file cannot be found or created
     */
    public function __construct($providerModel, $storeSessionCookies = false)
    {
        parent::__construct();
        $this->provider = $providerModel;
        $this->storeSessionCookies = $storeSessionCookies;

        if ($providerModel instanceof Provider) {
            $this->load($providerModel);
        }
    }

    /**
     * Saves the file when shutting down
     */
    public function __destruct()
    {
        $this->save($this->provider);
    }

    /**
     * Saves the cookies to a provider recored.
     *
     * @param Provider $provider Provider to save
     * @throws \RuntimeException if the provider recored cannot be found or created
     */
    public function save($provider)
    {
        $json = [];
        foreach ($this as $cookie) {
            /** @var SetCookie $cookie */
            if (CookieJar::shouldPersist($cookie, $this->storeSessionCookies)) {
                $json[] = $cookie->toArray();
            }
        }

        $jsonStr = \GuzzleHttp\json_encode($json);

        DB::beginTransaction();

        try {
            $provider->update([
                'cookie' => $jsonStr,
            ]);

            DB::commit();
        }
        catch (Exception $e) {
            DB::rollBack();
            
            throw new \RuntimeException("Unable to save provider #{$provider->id}");
        }
    }

    /**
     * Load cookies from a JSON formatted file.
     *
     * Old cookies are kept unless overwritten by newly loaded ones.
     *
     * @param Provider $provider Cookie file to load.
     * @throws \RuntimeException if the file cannot be loaded.
     */
    public function load($provider)
    {
        if($provider->cookie) {
            $json = $provider->cookie;
        }
        else {
            return;
        }

        $data = \GuzzleHttp\json_decode($json, true);
        if (is_array($data)) {
            foreach (json_decode($json, true) as $cookie) {
                $this->setCookie(new SetCookie($cookie));
            }
        } else {
            throw new \RuntimeException("Invalid cookie");
        }
    }
}
