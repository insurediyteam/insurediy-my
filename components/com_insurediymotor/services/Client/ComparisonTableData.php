<?php 

namespace Services\Client;

use Concerns\InteractsWithJConfig;
use JComponentHelper;
use Services\BaseClient;

class ComparisonTableData {

    const GROUP_COVER_TYPE = [
        'comparison-table-cover-type',
        'Cover Type',
    ];
    const GROUP_OWN_DAMAGE_EXCESS = [
        'comparison-table-own-damage-excess',
        'Own Damage Excess',
    ];
    const GROUP_COMPULSORY_EXCESS = [
        'comparison-table-compulsory-excess',
        'Compulsory Excess',
    ];
    const GROUP_NCD_PERCENT = [
        'comparison-table-ncd-percent',
        'NCD Percent',
    ];
    const GROUP_SUM_INSURED = [
        'comparison-table-sum-insured',
        'Sum Insured',
    ];
    const GROUP_BASIS_VALUATION = [
        'comparison-table-basis-valuation',
        'Basis Valuation',
    ];
    const GROUP_EXTRA_COVERAGE_MAP = [
        'cart' => "Compensation For Assessed Repair Time",
        'specialPeril' => "Cover For Special Peril",
        'liabilityPass' => "Liability Of Passenger",
        'ehailing' => "E-Hailing",
        'strike' => "Strike Riot & Civil Commotion",
        'waiver' => "Waiver Of Betterment",
        'laminated' => "Windscreens Laminated",
        'tinting' => "Windscreens Tinted",
    ];
    const GROUP_EXTRA_COVERAGE_PREFIX = 'comparison-table-extra-coverage-';
    const GROUP_BROCHURES = [
        'comparison-table-brochures',
        'Download Brochure',
    ];

    /** @var string */
    public $id;
    /** @var string */
    public $group;
    /** @var string */
    public $text;

    public function __construct($groupId, $groupName, $value)
    {
        $this->id = $groupId;
        $this->group = $groupName;
        $this->text = $value;
    }

    public static function make($groupId, $groupName, $value) {
        return new static($groupId, $groupName, $value);
    }
    
    public static function valueWithIcon($value, $currency = false) {
        $value = \MyHelper::unformatCurrency(str_replace('RM ', '', $value));

        $componentRegistry = JComponentHelper::getParams('com_insurediymotor');

        if($currency) {
            return $componentRegistry->get('currency') . ' ' . number_format($value, 2);
        }

        if($value) {
            return $value > 1 ? 
                $componentRegistry->get('currency') . ' ' . number_format($value, 2) :
                "<i class=\"fa fa-2x comparison-table-icon fa-check\"></i>";
        }

        return "<i class=\"fa fa-2x comparison-table-icon fa-times\"></i>";
    }

    public static function getExtraCoverageGroupId($name) {
        return static::GROUP_EXTRA_COVERAGE_PREFIX . $name;
    }

    /**
     * Generate extra coverage comparison table data
     *
     * @param BaseClient $client
     * @param string $name
     * @return self
     */
    public static function makeExtraCoverageFromClient(BaseClient $client, $name) {
        return new static(
            static::getExtraCoverageGroupId($name),
            static::GROUP_EXTRA_COVERAGE_MAP[$name],
            static::valueWithIcon($client->generator->extraCoverages[$name])
        );
    }

}
