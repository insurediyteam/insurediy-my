<?php 

namespace Services\Client;

use Exception;

class BaseClientException extends Exception {

    public static function clientLockdown() {
        return new static('Client status: lockdown!');
    }

}
