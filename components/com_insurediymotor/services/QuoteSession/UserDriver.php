<?php

namespace Services\QuoteSession;

use Models\Eloquent\User;
use Concerns\InteractsWithUser;
use Services\QuoteSession\Contracts\DriverInterface;

class UserDriver implements DriverInterface
{

    use InteractsWithUser;

    /** @var User */
    protected $user;

    public function __construct() 
    {
        $this->user = $this->getUserModel();
    }

    protected function getTemporaryQuote() {
        return $this->user->temporary_quotes()->first();
    }

    /**
     * Get quote data
     *
     * @return array
     */
    public function get()
    {
        $temporaryQuote = $this->getTemporaryQuote();

        if (!$temporaryQuote) {
            return [];
        }

        return $temporaryQuote->data;
    }

    /**
     * save data
     *
     * @param array $data
     * @return void
     */
    public function save($data)
    {
        $temporaryQuote = $this->getTemporaryQuote();

        if (!$temporaryQuote) {
            $this->user->temporary_quotes()->create([
                'data' => $data,
            ]);

            return;
        }

        $temporaryQuote->update([
            'data' => $data,
        ]);
    }

    /**
     * Flush data persistance
     *
     * @return void
     */
    public function flush() {
        $temporaryQuote = $this->getTemporaryQuote();

        $temporaryQuote->delete();
    }

}
