<?php

namespace Services\QuoteSession;

use Services\QuoteSession\Contracts\DriverInterface;

class GuestDriver implements DriverInterface {

    /** @var string */
    protected $sessionKey = 'quote';

    /**
     * get session key
     *
     * @return string
     */
    protected function sessionKey() {
        return $this->sessionKey;
    }
    
    /**
     * Get quote data
     *
     * @return array
     */
    public function get() {
        if (!isset($_SESSION[$this->sessionKey()])) {
            return [];
        }

        return $_SESSION[$this->sessionKey()];
    }

    /**
     * save data
     *
     * @param array $data
     * @return void
     */
    public function save($data) {
        $_SESSION[$this->sessionKey()] = $data;
    }

    /**
     * Flush data persistance
     *
     * @return void
     */
    public function flush() {
        unset($_SESSION[$this->sessionKey()]);
    }

}
