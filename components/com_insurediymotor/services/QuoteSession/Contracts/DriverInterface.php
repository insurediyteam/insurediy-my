<?php 

namespace Services\QuoteSession\Contracts;

interface DriverInterface {
    
    /**
     * Get quote data
     *
     * @return array
     */
    public function get();

    /**
     * save data
     *
     * @param array $data
     * @return void
     */
    public function save($data);

    /**
     * Flush data persistance
     *
     * @return void
     */
    public function flush();

}
