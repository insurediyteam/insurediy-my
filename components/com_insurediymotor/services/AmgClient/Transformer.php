<?php

namespace Services\AmgClient;

use Exception;
use Concerns\IcData;
use Services\AmgClient;
use Models\Eloquent\Quote;
use Concerns\TransformerTrait;
use Services\BaseClientSubclass;
use Services\Client\ComparisonTableData;

class Transformer extends BaseClientSubclass
{

    use TransformerTrait;

    /**
     * airbag:abs
     *
     * @var array
     */
    public static $safeties = [
        '1:no' => '01',
        '2:no' => '02',
        '1:yes' => '05',
        '2:yes' => '06',
    ];

    public static $vehicleFroms = [
        'new' => 'CBN',
        'old' => 'CBU',
    ];

    public static $coverTypes = [
        'C' => 'VPPCO0201',
        'T' => 'VPPTP0201',
    ];

    public static $garages = [
        '1' => '5',
        '2' => '4',
    ];

    public static $antitds = [
        '1' => '12',
    ];

    public static $drvexps = [
        '1' => '5',
    ];

    public static $provNCDs = [
        '2' => '0.00',
        '3' => '25.00',
        '4' => '30.00',
        '5' => '38.33',
        '6' => '45.00',
        '7' => '55.00',
    ];

    public static $genders = [
        'MALE' => '3',
        'FEMALE' => '2',
    ];

    public static $maritalstats = [
        'Married' => '2',
        'Single' => '3',
    ];

    public static $areas = [
        '1' => '2',
        '2' => '5',
        '3' => '6',
        '4' => '4',
        '5' => '7',
        '6' => '8',
        '7' => '9',
        '8' => '3',
        '9' => '1',
        '10' => '12',
        '11' => '10',
        '12' => '13',
        '13' => '11',
        '14' => '14',
    ];

    /**
     * Transform to compatible generator overrides
     *
     * @param array $inputs
     * @return array
     */
    public function inputs($inputs)
    {
        // convert to optional array, returns null when calling undefined key / index / props
        $inputs = $this->optional($inputs);

        $mainDriver = $this->getMainDriver($inputs);

        $transformedInputs = [
            'vehicleNo' => $inputs['vehicleRegNo'],
            'txtVehicleNo' => $inputs['vehicleRegNo'],
            'txtChassisNo' => $inputs['chassisNo'],
            'txtEngineNo' => $inputs['engineNo'],
            'txtMakeCode' => $this->getCarModelCode($inputs['carModel']),
            'txtModelDesc' => $this->getProviderChildAttribute('model', $inputs['carModel'], 'name', $strict = true),
            'vehicleModelId' => $this->getCarModelCode($inputs['carModel']),
            'txtCapacity' => $inputs['vehicleEngineCc'],
            'txtSelectedCapacity' => $inputs['vehicleEngineCc'],
            // 'capacityChange' => $inputs['vehicleEngineCc'],
            'nvicCapacity' => $inputs['vehicleEngineCc'],
            'sumInsured' => (int) $inputs['marketValue'],
            'safety' => $this->select('safeties', $inputs['airbags'] . ':' . $inputs['abs']),
            'vehiclefrom' => $this->select('vehicleFroms', $inputs['vehType']),
            'txtNoOfSeats' => $inputs['seatNumber'],
            'cover_type' => $this->select('coverTypes', $inputs['coverType']),
            'cboGarage' => $this->select('garages', $inputs['garage']),
            'antitd' => $this->select('antitds', $inputs['antiTeft']),
            'drvexp' => $inputs['drivingExp'],
            'provNCD' => $this->select('provNCDs', $inputs['ncd']),
            'claimyr' => $inputs['damageOwn'],
            'claimyr1' => $inputs['windscreen'],
            'claimyr2' => $inputs['theft'],
            'claimyr3' => $inputs['tpClaim'],
            'cboState' => $this->select('areas', $inputs['location']),
            'occupation' => $this->getOccupationCode($inputs['occupation'], $this->config('filter_occupation', false)),
            'txtNewIcNo' => $mainDriver['icNum'],
            'txtSearchClientName' => $mainDriver['name'],
            'txtClientName' => $mainDriver['name'],
            'rdoGstVehBusUseInd' => $inputs['vehicleUse'] == 1 ? 'Y' : 'N',
            'txtRegion' => Quote::locationRegion($inputs['location']) == 'west' ? 'WEST' : 'EAST',
            'region' => Quote::locationRegion($inputs['location']) == 'west' ? 'WEST' : 'EAST',
            'cboFinancialType' => $inputs['loan'] ? ($inputs['loan'] == 'yes' ? '1' : '2') : '2',
            'txtLoanCompany' => $this->getProviderChildAttribute('loan_company', $inputs['loanCompany'], 'name'),
            'txtYearOfMfg' => $inputs['carMakeYear'],
            'tmpYearOfMfg' => $inputs['carMakeYear'],
            'hdnClientPostcode' => $mainDriver['postalCode'],
            'cboCnType' => $inputs['vehType'] == 'new' ? '1' : '2',
        ];

        if($transformedInputs['cboCnType'] == '1') {
            // unset($transformedInputs['vehicleNo']);
            // unset($transformedInputs['txtVehicleNo']);
            $transformedInputs['vehicleNo'] = 'NA';
            $transformedInputs['txtVehicleNo'] = 'NA';
        }

        /**
         * Override sum insured by the agreed value
         */
        if ($inputs['sumInsured']) {
            $transformedInputs['sumInsured'] = $inputs['sumInsured'];
            $this->client->setSumInsured(intval($inputs['sumInsured']));
        }

        // transform optional driver data
        $this->transformDriver($transformedInputs, $inputs);

        // filter, remove null-equal values
        return $this->filterValue($transformedInputs);
    }

    protected function transformDriver(&$transformedInputs, $inputs)
    {
        AmgClient::$drivers = $this->getDrivers($inputs);

        if (count(AmgClient::$drivers)) {
            $mainDriver = AmgClient::$drivers[0];
            $transformedInputs['cboGender'] = $this->select('genders', $mainDriver['Gender']);
            $transformedInputs['gender0'] = $this->select('genders', $mainDriver['Gender']);
            $transformedInputs['genderId'] = $this->select('genders', $mainDriver['Gender']);
            $transformedInputs['cboSalutation'] = $this->select('genders', $mainDriver['Gender']);
        }
    }

}
