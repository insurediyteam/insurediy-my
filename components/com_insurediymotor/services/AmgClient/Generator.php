<?php 

namespace Services\AmgClient;

use Carbon\Carbon;
use Concerns\GeneratorTrait;
use Concerns\IcData;
use Services\BaseClientSubclass;

class Generator extends BaseClientSubclass {

    use GeneratorTrait;

    public static $extraCoverageCodes = [
        'cart' =>  38,
        'specialPeril' => 19,
        'liabilityPass' => 21,
        'ehailing' => 72,
        'strike' => 13,
        'waiver' => 74,
        'laminated' => 26,
        'tinting' => 27,
    ];

    public $unselectedCoverageCodes = [
        22,
        37,
    ];

    public $sessionId;

    public function getSessionId() {
        if(!isset($this->sessionId)) {
            $response = $this->quotationFormRequest();

            $response = (string) $response->getBody();

            $sessionId = $this->stringBetween($response, '<input type="hidden" name="sessionId" value="', '">');
            $sessionId = count($sessionId) ? $sessionId[0] : '';
            
            $this->sessionId = $sessionId;
        }

        return $this->sessionId;
    }

    /**
     * Generate template / defaults data for axa quotation
     *
     * @param array $overrides
     * @return array
     */
    public function step1QuotationData(array $overrides = []) {
        $ic = new IcData(isset($overrides['txtNewIcNo']) ? $overrides['txtNewIcNo'] : 811228015284);

        $defaults = [
            'txtMobilePrefix' => '012,013,016,017,019,0146,0147,0148,0149,0142,014,018,010,011',
            'txtValidationMsg' => '',
            'clientId' => '',
            'screenId' => "1",
            'menuClickVal' => 'N',
            'genderId' => '2',
            'stateId' => "0",
            'oldClientId' => "0",
            'blnReferAgeInd' => "false",
            'blnRefererAgeInd' => "false",
            'blnRefererInd' => "false",
            'quotationId' => "0",
            'quotationId' => "0",
            'currentDate' => Carbon::today()->format('d-m-Y'),
            'lengthField' => "250",
            'maxCummLoadEditable' => 'N',
            'insAgeLoadEditable' => 'N',
            'vehicleAgeLoadEditable' => 'N',
            'icValidateMsg' => '',
            'loginId' => "115759",
            'sessionId' => $this->getSessionId(),
            'searchInd' => 'N',
            'postCodeAjaxInd' => '',
            'oldContactTypeId' => "1",
            'screenTabSource' => '',
            'sourceType' => '',
            'hdNationalityId' => "131",
            'gstCountryId' => "131",
            'gstCountryCode' => 'MY',
            'designatedAreaCode' => "0",
            'designatedArea' => '',
            'hdnLegacyInd' => 'Y',
            'hdnAdcDeTariffInd' => 'N',
            'bancaInd' => '',
            'contactCenterInd' => '',
            'langMandatoryInd' => 'N',
            'txtOccuDesc' => 'SELF-EMPLOYED',
            'txtSearchClientName' => 'LAI HUEY VOON',
            'cboAccountCode' => "37152",
            'cboContactType' => "1",
            'txtNewIcNo' => $ic->number,
            'txtOldIcNo' => '',
            'txtBusinessRegNo' => '',
            'cboSalutation' => "9",
            'txtClientName' => 'LAI HUEY VOON',
            'cboRace' => "1",
            'txtDateOfBirth' => $ic->dob->format('d-m-Y'),
            'txtAge' => (string) $ic->dob->age,
            'cboOccupation' => "147",
            'txtTrade' => '',
            'cboMaritalStatus' => "2",
            'txtEmployerName' => '',
            'cboNatureOfBiz' => "0",
            'rdoGSTRegInd' => 'N',
            'rdoDncInd' => 'Y',
            'cboPreferredLanguage' => "2",
            'txtAddress1' => 'NO 36 JALAN KEMBIA 17',
            'txtAddress2' => 'TAMAN PUTERI WANGSA',
            'txtAddress3' => '',
            'txtAddress4' => '',
            'txtPostCode' => "81800",
            'txtPostCodeDesc' => '',
            'txtHouseFaxNo' => '',
            'txtOfficeFaxNo' => '',
            'txtMobileNo' => '0143396778',
            'txtEmailId' => '',
            'cboContactInsured' => "1",
            'txtReferedBy' => '',
            'cboContactStatus' => "1",
            'txtRemarks' => '',
            'groupButtons.btnClientProfileNext.name' => 'Next',
        ];

        $merged = $this->mergeAndOverride($defaults, $overrides, true);

        return $merged;
    }

    /**
     * generate second (step 2) quotation
     *
     * @param array $overrides
     * @return array
     */
    public function step2QuotationData($overrides = []) {
        $expiry = Carbon::today()->addYears(1)->subDays(1)->format('d-m-Y');
        $today = Carbon::today()->format('d-m-Y');
        $ic = new IcData($this->getSharedQuotationData('txtNewIcNo', 811228015284));

        $defaults = [
            'adcDownInd' => '',
            'adcErrorMessage' => '',
            'agentId' => '37152',
            'backMenuClickInd' => 'N',
            'blnClaimsReferer' => 'false',
            'blnReferAgeInd' => 'false',
            'blnRefererAgeInd' => 'false',
            'blnRefererInd' => 'false',
            'blockMsg' => '',
            'businessReg' => '',
            'cboAddUsage' => '5',
            'cboCnType' => '2',
            'cboCoverageType' => '1',
            'cboUseOfVehicle' => '19',
            'cboVehicleType' => '1',
            'cnBackDateMsg' => '',
            'contactType' => '1',
            'contactTypeId' => '1',
            'expiryDateActual' => $expiry,
            'groupButtons.btNbCnCoverNoteNext.name' => 'NEXT',
            'hdngstCoverNoteId' => '0',
            'highRisk' => 'N',
            'icNoFieldValue' => $ic->number,
            'insAgeLoadEditable' => 'N',
            'loginId' => '115759',
            'masterPolicyExpiryDate' => '',
            'maxCummLoadEditable' => 'N',
            'nextMenuClickInd' => 'N',
            'oldCoverNoteTypeId' => '0',
            'oldIcNo' => '',
            'quotInd' => 'N',
            'screenId' => '1',
            'screenTabSource' => 'C',
            'sessionId' => $this->getSessionId(),
            'txtBusInd' => 'N',
            'txtExpiryDate' => $expiry,
            'txtInceptionDate' => $today,
            'txtIssuedDate' => $today,
            'txtMasterPolicyNo' => '',
            'txtYearOfMfg' => '',
            'vehicleAgeLoadEditable' => 'N',
            'vehicleInd' => '',
            'vehicleName' => 'PRIVATE CAR EX GOODS',
            'vehicleUOM' => '',
        ];

        return $this->mergeAndOverride($defaults, $overrides, true);
    }

    public function vehicleInfoRequest($overrides = []) {
        $ic = new IcData($this->getSharedQuotationData('txtNewIcNo', 811228015284));

        $defaults = [
            "requestTypeParam" => "asyncXML",
            "formElement" => "fnGetVehicleDetails",
            "classId" => "1",
            "vehicleNo" => "JQR3339",
            "newIcBusRegNo" => $ic->number,
            "contactTypeInd" => "I",
            "clientIdVal" => "20972754",
            'expiryDate' => Carbon::today()->addYears(1)->subDays(1)->format('d-m-Y'),
            'inceptionDate' => Carbon::today()->format('d-m-Y'),
            "coverNoteTypeId" => "2",
            "makeCode" => "",
            "subClassCoverId" => "1",
            "ownerShipTypeId" => "2",
            "rdoHighRisk" => "N",
            "allRiderStatus" => "N",
            "additionalUsageId" => "5",
            "agentId" => "37152",
            "txtCapacity" => "",
            "region" => "WEST",
            "vehicleUsageId" => "19",
            "nvicId" => "0",
            "yearOfMake" => "",
            "busInd" => "N",
            "nvicCapacity" => "0",
        ];

        return $this->mergeAndOverride($defaults, $overrides, true);
    }

    public function addDrivers($overrides = []) {
        $defaults = [
            "vehicleTypeId" => "1",
            "coverageTypeId" => "1",
            "ownerShipTypeId" => "2",
            "vehicleNo" => "JQR3339",
            "oldVehicleTypeId" => "0",
            "additionalUsageId" => "5",
            "riderStatus" => "N",
            "onLoadStatus" => "NO",
            "vehicleModelId" => "7244",
            "isMake" => "N",
            "chkRiderStatus" => "N",
        ];

        return $this->mergeAndOverride($defaults, $overrides, true);
    }

    public function step3QuotationData($overrides = []) {
        $ic = new IcData($this->getSharedQuotationData('txtNewIcNo', 811228015284));

        $defaults = [
            'adcAlertMessage' => '',
            'adcMessageType' => 'N',
            'age' => (string) $ic->dob->age,
            'age0' => (string) $ic->dob->age,
            'agentId' => '37152',
            'allDriverInd' => 'N',
            'bdmLbl' => '',
            'blackListCheckVal' => '',
            'blnChkNVNR' => '',
            'blnChkNVNRInd' => '',
            'blnClaimsReferer' => 'false',
            'blnReferAgeInd' => 'false',
            'blnRefererAgeInd' => 'false',
            'blnRefererInd' => 'false',
            'blnRefererLoadInd' => 'false',
            'blnVehicleActiveReferer' => 'false',
            'blockMsg' => '',
            'capacityChange' => '0',
            'capacityChangeRestrict' => '',
            'capacityRange' => '50',
            'cboAddUsage' => '5',
            'cboCoverageType' => '1',
            'cboFinancialType' => '2',
            'cboGarage' => '5',
            'cboOwnerShip' => '2',
            'cboSafteyCode' => '6',
            'cboUseOfVehicle' => '19',
            'cboVariantSeriesTrans' => '597960',
            'cboVehicleType' => '1',
            'ccReferMsg' => '',
            'chasisBlackList' => '',
            'chkDriverVisibleInd' => 'Y',
            'chrImgMake' => 'N',
            'claimCount' => '0',
            'claimsExpLoadEditable' => 'N',
            'claimsReadOnly' => 'N',
            'clientId' => "17495222",
            'comPremInd' => '',
            'contactType' => '1',
            'coverNoteTabInd' => 'N',
            'coverNoteTypeId' => "2",
            'currentDate' => Carbon::today()->format('d-m-Y'),
            'dob' => $ic->dob->format('d-m-Y'),
            'dob0' => $ic->dob->format('d-m-Y'),
            'doubleInsuranceAlert' => '',
            'driverBtnDisable' => 'N',
            'driverCheck' => 'N',
            'driverCount' => "1",
            'driverExp0' => '',
            'driverGrid_colType' => '1/5/6/5/6/4/8/7/',
            'engineBlackList' => '',
            'fromMake' => 'N',
            'gender0' => '2',
            'genderId' => '2',
            'grdDob' => '',
            'groupButtons.btnNbCnVehicleNext.name' => 'Next',
            'hdnAdcDeTariffInd' => 'Y',
            'hdnClaimsTypeList' => '',
            'hdnClientAddress1' => 'NO 36 JALAN KEMBIA 17',
            'hdnClientAddress2' => 'TANJONG PIANDANG',
            'hdnClientAddress3' => '',
            'hdnClientAddress4' => '',
            'hdnClientCountryId' => '131',
            'hdnClientPostcode' => '81800',
            'hdnLegacyInd' => 'N',
            'hdnMakeCode' => '',
            'hdnNvicId' => '0',
            'hdnTariffInd' => 'N',
            'hdnVIXResponseInd' => 'Y',
            'hdnVehicleClassCode' => '02',
            'hdnVehicleOriginId' => '0',
            'highRiskInd' => 'N',
            'hirePurchaseDetailsInd' => '',
            'hrtvInd' => 'N',
            'insAgeLoadEditable' => 'N',
            'insName0' => 'THE POLICYHOLDER',
            'ismRespErrorInd' => '2',
            'ismResponseMsg' => '',
            'ismSubRespCode' => '',
            'ismSumInsInd' => 'Y',
            'isthereTrailerattached' => '',
            'keyWordList' => 'ALL DRIVERS,ANY AUTHORISED DRIVER,THE POLICY HOLDER,ALL RIDER,ALLDRIVERS,ALL DRIVER,ALLDRIVER,N/A,NA,NA,N/A,-',
            'loanCompanyId' => '0',
            'loginId' => '115759',
            'maxCummLoadEditable' => 'N',
            'maxSeatsForBus' => '50',
            'maxSeatsForBusBodyVan' => '30',
            'maxSeatsForPc' => '20',
            'menuClickCn' => 'N',
            'menuClickSchedule' => 'N',
            'minCapacityControl' => '0',
            'minCapacityLimit' => '0',
            "modelExcludeBlockMsg" => "",
            "modelExcludeInd" => "N",
            'modelId' => '7244',
            'namedDriverAmendInd' => '',
            'newIc0' => $ic->number,
            'newIcNo' => $ic->number,
            'nvicCapacity' => '1372',
            'oldClassId' => '1',
            'oldCoverageType' => '1',
            'oldIc0' => '',
            'oldIcNo' => '',
            'oldOwnerShipTypeId' => '2',
            'oldVehicleTypeId' => '1',
            'primeMoverClassId' => "1",
            'primeMoverClientId' => "17495222",
            'primeMoverClientMatchInd' => 'N',
            'primeMoverCoverageId' => "1",
            'primeMoverDetails' => '',
            'primeMoverInd' => 'N',
            'rdoGstVehBusUseInd' => 'N',
            'rdoHighRisk' => 'N',
            'rdoVehicleKeptAddressInd' => 'Y',
            'rdovehkeptaddInd' => 'Y',
            'riderCheck' => 'N',
            'riderEnable' => 'N',
            'riskLocationCode' => '0',
            'riskLocationDesc' => '',
            'sNo0' => '1',
            'sbdPiamCode' => '',
            'sbdSplAgentInd' => '',
            'screenId' => '1',
            'sessionId' => $this->getSessionId(),
            'specialTypeTrailerInd' => 'N',
            'tabSourceFrom' => 'V',
            'tmpOwnerShipTypeId' => '',
            'tmpYearOfMfg' => '2014',
            'txtBdm' => '',
            'txtBtm' => '',
            'txtBusInd' => 'N',
            'txtCapacity' => '1497',
            'txtCapacityDesc' => 'CC',
            'txtChassisNo' => 'PPPHZC82SDP107049',
            'txtClientStateId' => "5",
            'txtEngineNo' => 'K14BS123520',
            'txtExpiryDate' => Carbon::today()->addYears(1)->subDays(1)->format('d-m-Y'),
            'txtInceptionDate' => Carbon::today()->format('d-m-Y'),
            'txtLoanCompany' => '',
            'txtLogBookNo' => '',
            'txtMakeCode' => '2806',
            'txtModelDesc' => 'SUZUKI SWIFT GLX',
            'txtModelDescNew' => '',
            'txtNewIcBusRegNoVal' => $ic->number,
            'txtNoOfClaims' => '',
            'txtNoOfSeats' => '5',
            'txtPiamMakeCode' => '28',
            'txtPiamModelCode' => '06',
            'txtPurchasePrice' => '',
            'txtRegion' => 'WEST',
            'txtSelectedCapacity' => '1372.0',
            'txtTrailerNo' => '',
            'txtValidationMsg' => '',
            'txtVehPurchaseDate' => '',
            'txtVehicleBodyId' => "88",
            'txtVehicleKeptPostCodeDesc' => 'ULU TIRAM',
            'txtVehicleNo' => 'JQR3339',
            'txtYearOfMfg' => '2014',
            'vehicleAgeLoadEditable' => 'N',
            'vehicleInd' => '',
            'vehicleKeptCountryCode' => '',
            'vehicleKeptCountryId' => '131',
            'vehicleKeptStateId' => '2',
            'vehicleName' => 'PRIVATE CAR EX GOODS',
            'vehicleUOM' => 'CC',
            'yearofMfgLockInd' => 'N',
            'youngDriverInd' => 'N',
        ];

        $merged = $this->mergeAndOverride($defaults, $overrides);

        $merged['nvicCapacity'] = number_format($merged['nvicCapacity'], 1, '.', '');
        $merged['txtSelectedCapacity'] = number_format($merged['txtSelectedCapacity'], 0, '.', '');
        $merged['txtModelDesc'] = strtoupper($merged['txtModelDesc']);
        $merged['yearofMfgLockInd'] = 'N';
        $merged['ismRespErrorInd'] = '0';

        // echo json_encode($merged);exit;

        return $merged;
    }

    public function step4QuotationData($overrides = []) {
        $defaults = [
            'requestTypeParam' => 'asyncXML',
            'formElement' => 'fnIsmEnquiryConfirmation',
            'sumInsured' => '27000.00',
            'trailerSum' => '',
            'ncdVehicleNo' => $this->getSharedQuotationData('txtVehicleNo', 'JQR3339'),
            'ncdPolicyNo' => '',
            'control' => 'btnNCDConf',
            'cboNcdPercent' => '0.00',
            'ncdVerificationPercent' => '0.00',
            'cboVoluntaryExcess' => '0.0',
            'BasisOfCoverage' => 'A',
            'rebatePercentCheckedInd' => '',
            'NcdEffectDate' => Carbon::today()->format('d-m-Y'),
        ];

        $merged = $this->mergeAndOverride($defaults, $overrides, true);

        $merged['sumInsured'] = number_format($merged['sumInsured'], 2, '.', '');

        return $merged;
    }
    

    public function extraCoverageUpdatePayload($overrides = []) {
        $sample = 'agentId=37152&region=W&extCoverageCnt=0&txtConfirmMsg=&txtConfirmStatus=&extCoverCalMode=ES&extCoverFlatAmt=0.00&extCoverEachAmt=0.00&extCoverExceedingCnt=0&extCoverMinPremium=30.00&extCoverBackInd=N&txtNcdAmt=0.00&txtAlertMsg=&additionalUsageId=5&txtPrintPending=&coverNoteId=0&coverNoteNo=&receiptInd=N&inceptionDate=05-01-2020&expiryDate=04-01-2021&coverNoteTypeId=2&classId=1&txtTrailerNo=&printPendingStatus=N&convertDppaInd=CVT&buttonSource=&blnReferAgeInd=false&blnRefererAgeInd=false&blnRefererInd=false&useOfVehicleId=19&extCoverMinSI=200.00&extCoverMaxSI=0.00&tabSourceFrom=V&maxCummLoadEditable=N&blnRefererLoadInd=false&txtECTPWorkRisk=&txtExcursionTrip=&txtAddUsageExTrip=&screenId=1&loginId=115759&sessionId=0000ypJw6wEdVq0_UpPWBMH0Wle%3A-1&buttonInd=N&refreshCnt=1&blnMakeModelRefererInd=false&epayNotAllowInd=N&accessDenied=&totPayableMsg=&totalPayable=1294.23&grossPremium=1211.54&clientEpayInd=N&btnDisableInfo=&epayCount=0&editIndication=N&blnMotorRefererRisk=false&agentBlockMsg=&blnClaimsReferer=false&convertType=&blnMotorCycleReferRisk=false&txtBusInd=N&epayDisable=N&addExtra=A&subClassId=1&dppaBlock=&icNoFieldValue=811228015284&masterPolicyExpiryDate=&oldIcNo=&blnMProtector=false&contactTypeId=1&mcpiBlockStatus=N&blnSubClassReferer=false&blnSubClassCodeReferer=false&blnWindScreenReferer=false&blnVehicleUsageReferer=false&blnVehicleBlackLstReferer=false&blnTpftReferer=false&blnVehicleActiveReferer=false&cartInd=N&txtYearOfMfg=0&onLoadInd=&branchId=34&ncdConfirmationMsg=&ismRespErrorInd=4&blnReferHotModelInd=false&calcMode=ES&blnReferCartInd=false&cartDeclineInd=&blnCoverageReferer=false&errorMsg=&blnCCReferred=false&hdnExtraCoverageId=27&hdnRatePercent=0.00&hdnExtraCoverNextSelect=true&hdnSelectedExtCoverId=27&hdnPremium=&extraCoverageSelIdList=38%2C19%2C21%2C72%2C13%2C74%2C26%2C27&extraCoverageUnSelIdList=22%2C37&noOfDayList=&amtPerDayList=&sumInsuredList=&hdnLegacyInd=N&adcMessageType=N&adcAlertMessage=&blnAdcRefer=false&subClassCoverRefer=false&ismNcdCue=false&onLoBackdInd=N&hdnExtCoverageInd=N&vehicleSumInsured=32000.0&existingWindscreenSumInsured=0.0&cartVisible=Y&blnSubClassChangeReferer=false&extracoverageAmendInd=&ismNcdPromptMessage=&fromCart=N&hiddenNeedPolicyWordLang=&cartNoOfDayListSize=1&cartAmtListSize=0&ehailingAlertShowInd=N&cboNoOfDays=14&cboAmtPerDay=50.0&txtExtCoverSumInsured=700.00&txtExtCoverSumInsured19=32000.00&txtExtCoverSumInsured21=0.00&txtExtCoverSumInsured72=0.00&txtExtCoverSumInsured13=32000.00&txtExtCoverSumInsured74=0.00&txtExtCoverSumInsured26=201.00&txtExtCoverSumInsured27=202.00&txtExtraCoverageTotal=0.00';
        parse_str($sample, $parse);

        $sumInsured = $this->getSharedQuotationData('sumInsured', 32000) . '.00';
        
        $defaults = array_merge($parse, [
            'sessionId' => $this->getSessionId(),
            'extraCoverageSelIdList' => $this->getExtraCoverageSelectedIdList(),
            'extraCoverageUnSelIdList' => $this->getExtraCoverageUnSelectedIdList(),
            'txtExtCoverSumInsured19' => $sumInsured,
            'txtExtCoverSumInsured13' => $sumInsured,
            'vehicleSumInsured' => $sumInsured,
            'txtExtCoverSumInsured26' => $this->extraCoverages['laminated'] . '.00',
            'txtExtCoverSumInsured27' => $this->extraCoverages['tinting'] . '.00',
            'inceptionDate' => $this->getSharedQuotationData('txtInceptionDate', $parse['inceptionDate']),
            'expiryDate' => $this->getSharedQuotationData('txtExpiryDate', $parse['expiryDate']),
            'groupButtons.btnAdd.name' => 'Add',
        ]);

        return $this->mergeAndOverride($defaults, $overrides);
    }

    protected function getExtraCoverageSelectedIdList() {
        $codes = static::$extraCoverageCodes;
        return implode(
            ',', 
            array_map(
                function($selected) use ($codes) {
                    return $codes[$selected];
                }, 
                $this->selectedExtraCoverages
            )
        );
    }

    protected function getExtraCoverageUnSelectedIdList() {
        return implode(
            ',', 
            array_merge(
                $this->unselectedCoverageCodes,
                array_diff(
                    array_values(static::$extraCoverageCodes),
                    explode(',', $this->getExtraCoverageSelectedIdList())
                )
            )
        );
    }

    public function motorCoverNoteSchedulePayload($overrides = []) {
        $sample = 'txtKurniaInd=N&txtNcdFromId=0&txtPrevPolicyNcdPercent=&classId=1&subClassId=1&agentId=37152&stampDutyId=1&commissionId=1&scheduleMenuClick=N&extCoverMenuClick=N&vehicleTypeId=1&txtTrailerNo=&inceptionDate=05-01-2020&expiryDate=04-01-2021&searchClickInd=N&ncdSelectedPercent=0&actualLoadPercent=0.00&quotationId=0&minLoadingPercent=&maxLoadingPercent=&coverNoteTypeId=2&contactTypeId=1&ncdSearchSourceInd=N&icNoVal=811228015284&icInd=I&minExcessAmt=&maxExcessAmt=0.00&maxExcessUWAmt=0.00&blnReferAgeInd=false&blnRefererAgeInd=false&blnRefererInd=false&screenId=1&txtVehAgeLoadMaxG15pct=0.00&txtInsAgeLoadMaxG15Pct=0.00&cumulativeLoadingPercent=0.00&tabSourceFrom=V&maxCumLoadEditable=N&insAgeLoadEditable=N&vehicleAgeLoadEditable=N&actualVehAgeLoadPercent=0.00&actualInsAgeLoadPercent=0.00&actualMaxCummLoadPercent=0.00&blnRefererLoadInd=false&loginId=115759&sessionId=0000M2GtBapbWvgIelKyoFljE_6%3A-1&blnMakeModelRefererInd=false&txtOldICNo=&ncdMatch=&hpModelInd=&firstTrailerInd=&txtVehicleAgeLoadPercent=0.00&txtInsAgeLoadPercent=0.00&sbdAgentInd=N&ismNcdEnqId=56541210&ismNcdControlOptions=Y&ismRespErrorInd=4&vehAge=8&txtVehicleNo=PKV8098&hdnMinSI=&hdnMaxSI=&hdnMinSiTpft=&hdnMaxSiTpft=&hdnSiDeclineInd=&claimsExpLoadEditable=N&txtClaimsLoadPercent=0.00&voluntaryExcess=0.0&hdnLegacyInd=N&hdnNvicId=0&hdnMessage=Please+click+NCD+Confirmation+button.&hdnBasisofCoverage=&coverageDesc=&hdnDiscountPct=0.0&hdnPreRQS=0.0&hdnPostRQS=0.0&adcIndicator=D&pmLowMarketValue=25000.0&pmHighMarketValue=39000.0&hdnAdcInElligibleMessage=&hdnAdcCommentMessage=&hdnAdcErrorMessage=&hdnAdcReferRiskMessage=&rqsButtonEnableInd=N&omniCorporateInd=N&tcSumInsured=0.0&tcSumInsuredTo=0.0&tcAllowedInd=N&coverNoteTabInd=N&tcVehicleAge=0&tcSumInsuredCondition=N&tcSumInsuredActiveInd=N&tcVehicleAgeCondition=N&tcVehicleAgeActiveInd=N&tcVehicleFromAge=0&tcVehicleToAge=0&policySubclassId=0&hdnVehicleAge=8&adcReferInd=A&avFromAge=0&avToAge=15&taxName=Service+Tax+%25&gstVisibleInd=N&txtMarketValue=32000.00&rdoBasisofCoverage=A&txtSumInsured=32000.00&txtBasicPremium=1211.54&txtTrailerSum=0.00&txtTrailerPremium=0.00&txtAllRiderAmt=0.00&txtTotalBasic=1211.54&txtVehicleAgeLoadPct=0.00&txtVehicleAgeLoadAmt=0.00&txtInsAgeLoadPct=0.00&txtInsAgeLoadAmt=0.00&txtClaimsExpLoadPct=0.00&txtClaimsExpLoadAmt=0.00&txtHPLoadPercent=0.00&txtHPLoadAmt=0.00&txtMaxCummLoadPercent=0.00&txtMaxCummLoadAmt=0.00&txtLoadingPercent=0.00&txtLoadingAmt=0.00&txtNcdFrom=&txtNcdVehicleNo=PKV8098&txtCNPolicyNo=&txtNcdPolicyEffectDate=&txtNcdPolicyExpDate=&txtNcdEffectDate=05-01-2020&cboNcdVerificationPer=0.00&txtAnnualPremium=1211.54&txtAfterLoading=1211.54&txtNcdAmt=0.00&txtExtraCoverageAmt=0.00&txtGrossPremium=1211.54&txtGSTPremiumPercent=6.00&txtGSTPremiumAmt=72.69&txtStampDutyAmt=10.00&txtCommissionPercent=10.00&txtCommissionAmt=121.15&txtTotalPayable=1294.23&txtActPremium=398.97&txtExcess=0.00&cboVoluntaryExcess=1&txtTotalExcess=0.00';
        parse_str($sample, $parse);

        $sumInsured = $this->getSharedQuotationData('sumInsured', 32000) . '.00';
        
        $defaults = array_merge($parse, [
            'sessionId' => $this->getSessionId(),
            'inceptionDate' => $this->getSharedQuotationData('txtInceptionDate', $parse['inceptionDate']),
            'expiryDate' => $this->getSharedQuotationData('txtExpiryDate', $parse['expiryDate']),
            'newIcNo' => $this->getSharedQuotationData('icNoVal', 811228015284),
            'txtVehicleNo' => $this->getSharedQuotationData('txtVehicleNo', 'PKV8098'),
            'txtSumInsured' => $sumInsured,
            'txtMarketValue' => $sumInsured,
            'groupButtons.btnScheduleNext.name' => 'Next',
        ]);

        return $this->mergeAndOverride($defaults, $overrides);
    }

    /**
     * Add Extra Coverage
     *
     * @param string $name
     * @param int $sumInsured
     * @return void
     */
    public function addExtra($name, $sumInsured) {
        if($name == 'tinting') {
            $name = 'laminated';
        }

        if(!in_array($name, $this->selectedExtraCoverages)) {
            array_push($this->selectedExtraCoverages, $name);
        }

        $this->extraCoverages[$name] = $sumInsured;

        $this->onAfterAddExtra($name);
    }

}
