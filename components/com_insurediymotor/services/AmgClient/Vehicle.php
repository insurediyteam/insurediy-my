<?php 

namespace Services\AmgClient;

use Exception;
use Models\Eloquent\BodyType;
use Services\AmgClient;
use Models\Eloquent\Make;
use Services\BaseClientSubclass;
use Services\Client\BaseListData;
use Services\Contracts\Client\Subclass\VehicleContract;

class Vehicle extends BaseClientSubclass implements VehicleContract {

    public function makeListData() {
        return Make::get()
            ->map(function($make) {
                return new BaseListData($make->name, $make->name);
            })
            ->toArray();
    }

    public function modelListData($makeCode, $type = null, $cover = null, $use = null) {
        $datas = $this->search($makeCode);

        if(!$datas) {
            return [];
        }

        $datas = array_filter($datas, function($data) use ($makeCode) {
            return $data['makecode'] == strtoupper($makeCode);
        });

        return array_map(function($data) {
            return new BaseListData($data['code'], $data['modelDesc'], [
                [
                    'value' => $data['body'],
                    'description' => 'body',
                ],
                [
                    'value' => $data['capacity'],
                    'description' => 'capacity',
                ],
                [
                    'value' => $data['capacityRange'],
                    'description' => 'capacityRange',
                ],
                [
                    'value' => $data['ismModelDesc'],
                    'description' => 'ismModelDesc',
                ],
                [
                    'value' => $data['makecode'],
                    'description' => 'makecode',
                ],
                [
                    'value' => $data['marketValue'],
                    'description' => 'marketValue',
                ],
                [
                    'value' => $data['mfgYear'],
                    'description' => 'mfgYear',
                ],
                [
                    'value' => $data['modelId'],
                    'description' => 'modelId',
                ],
                [
                    'value' => $data['noOfSeats'],
                    'description' => 'noOfSeats',
                ],
                [
                    'value' => $data['nvicId'],
                    'description' => 'nvicId',
                ],
                [
                    'value' => $data['origin'],
                    'description' => 'origin',
                ],
                [
                    'value' => $data['originCode'],
                    'description' => 'originCode',
                ],
                [
                    'value' => $data['piamMake'],
                    'description' => 'piamMake',
                ],
                [
                    'value' => $data['piamModel'],
                    'description' => 'piamModel',
                ],
                [
                    'value' => $data['uom'],
                    'description' => 'uom',
                ],
                [
                    'value' => $data['vehBodyId'],
                    'description' => 'vehBodyId',
                ],
            ]);
        }, $datas);
    }

    public function bodyListData() {
        return BodyType::get()
            ->map(function($bodyType) {
                return new BaseListData($bodyType->name, $bodyType->name);
            })
            ->toArray();
    }

    public function search($keyword) {
        if(strlen($keyword) < 3) {
            return [];
        }

        $this->authenticate();
        
        $response = $this->getHttpClient()->get('/amg/Agent/commonutils/searchmakemodel.do', [
            'query' => [
                "type" => "1",
                "screenId" => "1",
                "classId" => "1",
                "agentId" => "37152",
                "strQuotRNInd" => "N",
                "hdnPolicyYear" => "undefined",
            ],
        ]);

        $response = $this->getHttpClient()->get(AmgClient::ROUTE_VEHICLE_SEARCH, [
            'query' => [
                'formElement' => 'fnVehicleModelSearch',
                'data' => $keyword,
                'txtYear' => '',
                'txtCC' => '',
                'classId' => '1',
                'agentId' => '37152',
                'filterscount' => '0',
                'groupscount' => '0',
                'sortorder' => '',
                'pagenum' => '0',
                'pagesize' => '10',
                'recordstartindex' => '0',
                'recordendindex' => '10',
                '_' => number_format(microtime(true) * 1000, 0, '.', ''),
            ],
        ]);

        $response = (string) $response->getBody();

        $json = utf8_encode($response);

        $data = json_decode($json, true);

        return $data;
    }

}
