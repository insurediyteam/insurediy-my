<?php

namespace Services\AmgClient;

use Services\BaseClientSubclass;
use Services\AmgClient;
use Services\Client\BaseListData;
use Services\Contracts\Client\Subclass\LoanCompanyContract;
use Symfony\Component\DomCrawler\Crawler;

class LoanCompany extends BaseClientSubclass implements LoanCompanyContract
{

    public function listData()
    {
        $this->authenticate();

        $results = collect([]);

        /** @var AmgClient $this */
        $alphabets = range('A', 'Z');
        foreach ($alphabets as $key => $alphabet) {
            $response = $this->loanCompanyPageRequest($alphabet);

            $dom = $this->htmlToDom((string) $response->getBody());

            /** @var self $this */
            $results = $results->concat($this->parseTable($dom));

            $pageTotal = $dom->filter('form > table > tr:nth-child(2) > td > table > tr:nth-child(2) > td.contd > table:nth-child(1) > tr:nth-child(3) > td > div > table > tr:nth-child(1) > td > b')->first();
            $pageTotal = $pageTotal->text();
            $pageTotal = intval(str_replace('Page 1 of ', '', $pageTotal));
            
            if($pageTotal > 1) {
                for($i = 2; $i <= $pageTotal; $i++) {
                    $response = $this->loanCompanyPageRequest($alphabet, $i);
                    $dom = $this->htmlToDom((string) $response->getBody());
                    $results = $results->concat($this->parseTable($dom));
                }
            }
        }
        
        return $results->map(function($option) {
            return new BaseListData($option['code'], $option['name']);
        })->toArray();
    }

    public function parseTable($dom) {
        $results = collect([]);
        $tableRows = $dom->filter('body > form > table > tr:nth-child(2) > td > table > tr:nth-child(2) > td.contd > table:nth-child(1) > tr:nth-child(4) > td > div > table > tr > td > table > tr');
        $tableRows->each(function(Crawler $node, $i) use ($results) {
            if(!$i) {
                return;
            }

            if(!$node->attr('id')) {
                return;
            }

            $columns = $node->filter('td');

            $results->push([
                'code' => $columns->eq(0)->text(),
                'name' => $columns->eq(1)->text(),
            ]);
        });

        return $results;
    }

}
