<?php 

namespace Services\AmgClient;

use Services\BaseClientSubclass;

class Occupation extends BaseClientSubclass {

    public function listData() {
        $occupationDataRequest = $this->quotationFormRequest();

        $occupationDataHtml = (string) $occupationDataRequest->getBody();

        $htmlDom = $this->htmlToDom($occupationDataHtml);

        $selectDom = $htmlDom->filter('select[name=\'cboOccupation\']')->first();

        $data = $this->parser->listDataFromDom($selectDom);

        array_shift($data);

        return $data;
    }

}
