<?php 

namespace Services\AmgClient;

use Concerns\ParserTrait;
use Services\BaseClientSubclass;
use Services\Client\OptionalArray;

class Parser extends BaseClientSubclass {

    use ParserTrait;

    /**
     * Parse quotation html data to array
     *
     * @param string $htmlString
     * @return array
     */
    public function quotationData($xmlString) {
        $data = $this->xmlToArray($xmlString);

        $ncdPercent = isset($data['SELECT']) ? (float) $data['SELECT']['selectedValue'] : null;

        $textData = new OptionalArray([]);
        foreach($data['TEXT'] as $dataValue) {
            $textData[(string) $dataValue['property']] = (string) $dataValue['value']; 
        }

        $this->setQuotationValue($textData['txtTotalPayable']);

        return [
            [
                'description' => 'NCD Percentage',
                'value' => $ncdPercent,
            ], 
            [
                'description' => 'Basic Premium',
                'value' => (float) $textData['txtBasicPremium'],
            ], 
            [
                'description' => 'Extra Coverage',
                'value' => (float) $textData['txtExtraCoverageAmt'],
            ], 
            [
                'description' => 'NCD',
                'value' => 0 - $textData['txtNcdAmt'],
            ], 
            [
                'description' => 'Gross Premium',
                'value' => (float) $textData['txtGrossPremium'],
            ], 
            [
                'description' => 'GST Premium Percentage',
                'value' => $textData['txtGSTPremiumPercent'],
            ], 
            [
                'description' => 'GST Premium',
                'value' => (float) $textData['txtGSTPremiumAmt'],
            ], 
            [
                'description' => 'Stamp Duty',
                'value' => (float) $textData['txtStampDutyAmt'],
            ], 
            [
                'description' => 'Compulsory Excess',
                'value' => (float) $textData['txtExcess'],
            ], 
            [
                'description' => 'Total Payable',
                'value' => (float) $textData['txtTotalPayable'],
            ], 
        ];
    }

    public function overrideNcdCalculation($quotationData) {
        $quotationData = collect($quotationData)->keyBy('description');

        $quotationData['NCD Percentage']['value'] = $this->ncdPercent;
        
        $basicPremium = $quotationData['Basic Premium']['value'];
        $stampDuty = $quotationData['Stamp Duty']['value'];
        $taxPercent = $quotationData['GST Premium Percentage']['value'];
        
        $ncd = $basicPremium * ($this->ncdPercent / 100);
        $quotationData['NCD']['value'] = $ncd;

        $grossPremium = $basicPremium - $ncd;
        $quotationData['Gross Premium']['value'] = $grossPremium;

        $tax = ($taxPercent / 100) * $grossPremium;
        $quotationData['GST Premium']['value'] = $tax;
        
        $totalPayable = $grossPremium - $tax - $stampDuty;
        $quotationData['Total Payable']['value'] = $tax;

        return $quotationData->values()->toArray();
    }

    public function readOptions($htmlString) {
        preg_match_all('/new Option\((.+?)\);/', $htmlString, $matchesAll, PREG_SET_ORDER);

        return array_map(function($matches) {
            $array = explode(',', trim(str_replace('"', '', $matches[1])));

            return [
                'value' => $array[1],
                'text' => $array[0],
            ];
        }, $matchesAll);
    }

    /**
     * Parse occupation data
     *
     * @param string $htmlString
     * @return array
     */
    public function occupationData($htmlString) {
        return $this->readOptions($htmlString);
    }

    public function vehicleBodyData($htmlString) {
        $bodyDataJsHtml = $this->stringBetween($htmlString, '/** display vehicle body**/', '/** display safety code**/')[0];

        return $this->readOptions($bodyDataJsHtml);
    }

    public function vehicleInfo($response) {
        $xmlArray = $this->xmlToArray($response->getBody());

        $vehicleInfo = [];
        foreach ($xmlArray['TEXT'] as $key => $value) {
            $vehicleInfo[(string) $value['property']] = (string) $value['value'];
        }
        foreach ($xmlArray['HIDDEN'] as $key => $value) {
            $vehicleInfo[(string) $value['property']] = (string) $value['value'];
        }
        $selectVariant = $xmlArray['SELECT']->OPTION[1];
        $vehicleInfo[(string) $selectVariant['property']] = (string) $selectVariant['value'];

        return $vehicleInfo;
    }

}
