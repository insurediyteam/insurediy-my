<?php 

namespace Services\MsigClient;

use Services\BaseClientSubclass;
use Services\Client\BaseListData;

class Occupation extends BaseClientSubclass {

    protected function getMakeModelDom() {
        if(!isset($this->makeModelDom)) {
            $response = $this->step1QuotationRequest();
            $this->makeModelDom = $this->htmlToDom((string) $response->getBody());
        }

        return $this->makeModelDom;
    }

    public function listData() {
        $selectDomObject = $this->getMakeModelDom()->filter('select[name=\'occupationCode\']')->first();

        $selectOptions = $this->parser->selectDataFromDom($selectDomObject);
        array_shift($selectOptions);

        $data = array_map(function($value) {
            return new BaseListData($value['value'], $value['text']);
        }, $selectOptions);

        return $data;
    }

}
