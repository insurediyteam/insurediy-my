<?php 

namespace Services\MsigClient;

use Services\MsigClient;
use Concerns\ParserTrait;
use Services\BaseClientSubclass;
use Services\Client\ComparisonTableData;
use Symfony\Component\DomCrawler\Crawler;

class Parser extends BaseClientSubclass {

    use ParserTrait;

    public function getDomInputValue(Crawler $dom, $inputName) {
        return $dom->filter("input[name={$inputName}]")
            ->first()
            ->attr('value');
    }

    public function premiumParser($htmlString) {
        $dom = $this->htmlToDom($htmlString);

        $data = [
            'motorBasicPremium' => $this->getDomInputValue($dom, 'motorBasicPremium'),
            'grossBasicPremium' => $this->getDomInputValue($dom, 'grossBasicPremium'),
            'nettPremium' => $this->getDomInputValue($dom, 'nettPremium'),
            'totalPremium' => $this->getDomInputValue($dom, 'totalPremium'),
            'postedPremium' => $this->getDomInputValue($dom, 'postedPremium'),
            // 'excessAmount' => $this->getDomInputValue($dom, 'excessAmount'),
        ];

        return $data;
    }

    /**
     * Parse quotation html data to array
     *
     * @param string $htmlString
     * @return array
     */
    public function quotationData($responseBody) {
        $dom = $this->htmlToDom($responseBody);

        $tableRows = $dom->filter('body form table:nth-child(30) tr');

        $data = [];
        $quotationValue = 0;
        $self = $this;

        $tableRows->each(function(Crawler $node, $i) use (&$data, &$quotationValue, $self) {
            $tableDatas = $node->filter('td');

            if(count($tableDatas) != 5) {
                return;
            }

            $description = $self->trim($tableDatas->eq(1)->text());
            $value = \MyHelper::unformatCurrency($self->trim($tableDatas->eq(2)->text()));

            if($description) {
                array_push($data, [
                    'description' => $description,
                    'value' => $value,
                ]);
    
                if($description == 'Nett Premium') {
                    $quotationValue = $value;
                }
            }
        });

        $this->setQuotationValue($quotationValue);

        return $data;
    }

    public function extraCoverage($htmlString) {
        $dom = $this->htmlToDom($htmlString);

        foreach ($this->generator->selectedExtraCoverages as $key => $value) {
            $inputName = MsigClient::getExtraCoverageInputName($value, '.premium');
            $inputElement = $dom->filter("[name='{$inputName}']");
            if(count($inputElement)) {
                $this->setExtraCoverageSumInsured($value, $inputElement->first()->attr('value'));
            }
        }

        $ownDamageExcess = $dom->filter("input[name='excessAmount']")->first()->attr('value');
        $this->addCoverTypeAndExcesses(
            new ComparisonTableData(
                ComparisonTableData::GROUP_OWN_DAMAGE_EXCESS[0],
                ComparisonTableData::GROUP_OWN_DAMAGE_EXCESS[1],
                ComparisonTableData::valueWithIcon($ownDamageExcess, true)
            )
        );
    }

    public function checkNcdData(\GuzzleHttp\Psr7\Response $response) {
        $dom = $this->htmlToDom((string) $response->getBody());

        return [
            'nvic' => $dom->filter("input[name='nvic']")->first()->attr('value'),
            'variantNvic' => $dom->filter("select[name='variantNvic'] option:nth-child(2)")->first()->attr('value'),
        ];
    }

    public function postalCodeData(\GuzzleHttp\Psr7\Response $response, $code) {
        $body = (string) $response->getBody();


        $startFrom = "document.updPCForm.postal.value ==\"{$code}\"";
        $endAt = "isFound =\"Y\";";
        $data = $this->stringBetween($body, $startFrom, $endAt)[0];

        return [
            'townDesc' => $this->stringBetween($data, "document.updPCForm.town.value = \"", "\";")[0],
            'townCode' => $this->stringBetween($data, "document.updPCForm.townCode.value = \"", "\";")[0],
        ];
    }

    public function enquireNCD(\GuzzleHttp\Psr7\Response $response) {
        $dom = $this->htmlToDom((string) $response->getBody());

        $nextNCD = $dom->filter('body > table:nth-child(1) > tr > td:nth-child(2) > form > table > tr:nth-child(6) > td:nth-child(3) > table > tr:nth-child(6) > td:nth-child(2) > font')->first()->text();
        $currentNCD = $dom->filter('body > table:nth-child(1) > tr > td:nth-child(2) > form > table > tr:nth-child(6) > td:nth-child(3) > table > tr:nth-child(9) > td:nth-child(2) > font')->first()->text();

        return [
            'nextNCD' => str_replace(': ', '', $this->trim($nextNCD)),
            'currentNCD' => str_replace(': ', '', $this->trim($currentNCD)),
        ];
    }

}
