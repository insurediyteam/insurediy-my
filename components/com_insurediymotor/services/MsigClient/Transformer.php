<?php

namespace Services\MsigClient;

use Exception;
use Concerns\IcData;
use Models\Eloquent\Quote;
use Concerns\TransformerTrait;
use Services\BaseClientSubclass;
use Services\Client\ComparisonTableData;

class Transformer extends BaseClientSubclass
{

    use TransformerTrait;

    /**
     * airbag:abs
     *
     * @var array
     */
    public static $safeties = [
        '1:no' => '01',
        '2:no' => '02',
        '1:yes' => '05',
        '2:yes' => '06',
        '3:yes' => '07',
    ];

    public static $vehicleFroms = [
        'new.local' => '01',
        'old.local' => '01',
        'new.imported' => '03',
        'old.imported' => '04',
    ];

    public static $coverTypes = [
        'C' => 'VPPCO0201',
        'T' => 'VPPTP0201',
    ];

    public static $garages = [
        '1' => '31',
        '2' => '32',
    ];

    public static $antitds = [
        '1' => '12',
        '2' => '13',
        '3' => '11',
        '4' => '10',
        '5' => '19',
    ];

    public static $drvexps = [
        '1' => '5',
    ];

    public static $provNCDs = [
        '2' => '0.00',
        '3' => '25.00',
        '4' => '30.00',
        '5' => '38.33',
        '6' => '45.00',
        '7' => '55.00',
    ];

    public static $genders = [
        'MALE' => 'M',
        'FEMALE' => 'F',
    ];

    public static $nametitles = [
        '1' => 'DATO',
    ];

    public static $maritalstats = [
        'Married' => 'M',
        'Single' => 'S',
    ];

    public static $areas = [
        '1' => 'SEL',
        '2' => 'JHR',
        '3' => 'KDH',
        '4' => 'KLT',
        '5' => 'LBN',
        '6' => 'MLK',
        '7' => 'NS',
        '8' => 'PHG',
        '9' => 'PRK',
        '10' => 'PER',
        '11' => 'PP',
        '12' => 'SBH',
        '13' => 'SWK',
        '14' => 'TRG',
    ];

    public static $regions = [
        'west' => 'W',
        'east' => 'E',
    ];

    /**
     * Transform to compatible generator overrides
     *
     * @param array $inputs
     * @return array
     */
    public function inputs($inputs)
    {
        // convert to optional array, returns null when calling undefined key / index / props
        $inputs = $this->optional($inputs);

        $makeCode = $this->getCarMakeCode($inputs['carMake']);

        $mainDriver = $this->getMainDriver($inputs);

        $transformedInputs = [
            'regno' => $inputs['vehicleRegNo'],
            'registrationNo' => $inputs['vehicleRegNo'],
            'previousVehicleRegNo' => $inputs['vehicleRegNo'],
            'chassisNo' => $inputs['chassisNo'],
            'engineNo' => $inputs['engineNo'],
            'occupationCode' => $this->getOccupationCode($inputs['occupation'], false),
            'occupID' => $this->getOccupationCode($inputs['occupation'], false),
            'occupationDesc' => $this->getProviderChildAttribute('occupation', $inputs['carMake'], 'name', false),
            'makeCode' => $makeCode,
            'makeDesc' => "{$this->getProviderChildAttribute('make', $inputs['carMake'], 'name')} {$this->getProviderChildAttribute('model', $inputs['carModel'], 'name')}",
            'modelCode' => "{$makeCode}    {$this->getCarModelCode($inputs['carModel'])}",
            'capacity' => $inputs['vehicleEngineCc'],
            'tonnage' => number_format($inputs['vehicleEngineCc'], 2, '.', ''),
            'ncd' => $this->select('provNCDs', $inputs['ncd']),
            'yearOfManufacture' => $inputs['carMakeYear'],
            'capacity' => $inputs['vehicleEngineCc'],
            'vehicleSumInsured' => (string) intval($inputs['marketValue']),
            'totalSumInsured' => (string) intval($inputs['marketValue']),
            'purchasePrice' => number_format($inputs['purchasePrice'], 2, '.', ''),
            'numOfSeats' => $inputs['seatNumber'],
            'location' => $this->select('areas', $inputs['location']),
            'garageLocation' => $this->select('garages', $inputs['garage']),
            'antiTheftDevices' => $this->select('antitds', $inputs['antiTeft']),
            'safetyFeatures' => $this->select('safeties', $inputs['airbags'] . ':' . $inputs['abs']),
            'importedType' => $this->select('vehicleFroms', "{$inputs['vehType']}.{$inputs['carIsLocal']}"),
            'postalCode' => $mainDriver['postalCode'],
            'postal' => $mainDriver['postalCode'],
            'fullName' => $mainDriver['name'],
            'address1' => $mainDriver['addressOne'],
            'address2' => $mainDriver['addressTwo'],
            'salutation' => strtoupper($mainDriver['salutation']),
            'vehicleClassCode' => $inputs['vehicleUse'] ? ($inputs['vehicleUse'] == 1 ? 'PC 01' : 'PC 02') : null,
        ];

        /**
         * Override sum insured by the agreed value
         */
        if ($inputs['sumInsured']) {
            $transformedInputs['vehicleSumInsured'] = (string) intval($inputs['sumInsured']);
            $transformedInputs['totalSumInsured'] = (string) intval($inputs['sumInsured']);
            $this->client->setSumInsured(intval($inputs['sumInsured']));
        }

        if($inputs['carIsLocal'] == 'imported') {
            switch ($inputs['vehType']) {
                case 'new':
                    $transformedInputs['importedType'] = '03';
                    break;

                case 'old':
                    $transformedInputs['importedType'] = '04';
                    break;
                
                default:
                    # code...
                    break;
            }
        }

        if($inputs['location']) {
            $transformedInputs['region'] = $this->select('regions', Quote::locationRegion($inputs['location']));
        }

        // transform optional driver data
        $this->transformDriver($transformedInputs, $inputs);

        // filter, remove null-equal values
        return $this->filterValue($transformedInputs);
    }

    protected function transformDriver(&$transformedInputs, $inputs)
    {
        $drivers = $this->getDrivers($inputs);

        if (count($drivers) === 0) {
            return;
        }

        $driver = $drivers[0];

        $ic = new IcData($driver['icNum']);

        $transformedInputs['clientName'] = $driver['name'];
        $transformedInputs['clntNRIC'] = $ic->numberDashed;
        $transformedInputs['NRIC'] = $ic->numberDashed;
        $transformedInputs['nric'] = $ic->numberDashed;
        $transformedInputs['DOB'] = $ic->dob->format('d/m/Y');
        $transformedInputs['gender'] = $ic->gender == 'male' ? 'M' : 'F';
    }

}
