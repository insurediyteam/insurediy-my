<?php 

namespace Services\MsigClient;

use Carbon\Carbon;
use Concerns\GeneratorTrait;
use Services\BaseClientSubclass;
use Services\MsigClient;

class Generator extends BaseClientSubclass {

    use GeneratorTrait;

    protected $extraCoverList = [];

    public static $extraCoverCodeMap = [
        'liabilityPass' => 'E72',
        'cart' => 'EC14',
        'waiver' => '	M018',
        'specialPeril' => 'E57A',
        'strike' => 'E25',
        'laminated' => 'E89',
    ];

    public $extraCoverCodes = [
        [
            'key' => 'hCoverID',
            'value' => 'E101',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'E102',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'GDI',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'M003',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'M011',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'M012',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'M013',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'M018',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'ND',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'EC07',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'EC14',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'EC21',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'M001',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'E111',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'E89',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'E97',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'E97A',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'M010',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'E105',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'E22',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'LLP',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'E72',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'E25',
        ],
        [
            'key' => 'hCoverID',
            'value' => 'E57A',
        ],
    ];

    public function selectCoverage($overrides = []) {
        $datenow = Carbon::today();
        $dateExp = $datenow->copy()->addYears(1)->subDays(1);

        $defaults = [
            'trnRefNo' => '',
            'trnIssueTime' => '',
            'fwconvert' => '',
            'fwcmsTxId' => '',
            'riskID' => '1',
            'currentClientType' => '2',
            'saveStatus' => '0',
            'transactionType' => '1',
            'proposalType' => 'NB',
            'prevProposalIncepDt' => '',
            'isDifferentInceptDt' => 'true',
            'travelTo' => '',
            'isSTPProd' => 'false',
            'isHCRProd' => 'false',
            'isFPBProd' => 'false',
            'isMOGProd' => 'false',
            'isSDLProd' => 'false',
            'isDMPProd' => 'false',
            'isShowPremiumBtnClicked' => 'true',
            'oldExpiryDate' => '',
            'covernoteFound' => 'N',
            'selectedId' => '',
            'bSubmitFlag' => 'false',
            'prodTemplate' => '5',
            'prodCode' => 'MPD',
            'maxDay' => '0',
            'coverNo' => 'N/A',
            'issuedDate' => '',
            'receivedDate' => '',
            'clientNumber' => '',
            'clientName' => 'LAI HUEY VOON',
            'inceptionDate' => $datenow->format('d/m/Y'),
            'prevInceptionDate' => '',
            'expiryDate' => $dateExp->format('d/m/Y'),
            'masterPolicyNo' => '',
            'payPlan' => 'D000',
            'reference' => '',
            'effectiveDate' => '',
            'currentAnnualPremium' => '0.0',
            'prevAnnualPremium' => '0.0',
            'topupAmountS' => '0.0',
            // 'cDDConducted' => 'on',
            // 'photocopyAttd' => 'on',
            // 'pdpaFlag' => 'on',
            'cDDConductedP' => 'false',
            'photocopyAttdP' => 'false',
            'chkTelephoneP' => 'false',
            'chkEmailP' => 'false',
            'postP' => 'false',
            'smsP' => 'false',
            'pdpaFlagP' => 'false',
            'currentRemarks' => '',
            'prevStandardProposal' => 'false',
        ];

        $merged = $this->mergeAndOverride($defaults, $overrides, true);

        return $merged;
    }

    public function checkNCDQuery($overrides = []) {
        $defaults = [
            'regno' => 'PKV8098',
            'type' => 'vehRegNo',
            'proposalType' => 'NB',
            'proposalStatus' => 'DE',
            'brokerID' => 'KLFIND01',
        ];

        return $this->mergeAndOverride($defaults, $overrides, true);
    }

    public function checkNCDFormData($overrides = []) {
        $datenow = Carbon::today();

        $defaults = [
            "ncdReplyNo" => "",
            "oldCoverTypeCode" => "",
            "headerEffDate" => $this->getSharedQuotationData('headerEffDate', $datenow->format('d/m/Y')),
            "encdVehClass" => "",
            "encdPercent" => "0.0",
            "isFoundInWNCD" => "false",
            "encdCutOffDate" => "20110701",
            "isFoundInWESI" => "false",
            "isCovernoteFound" => "N",
            "hasReadFromWESI" => "false",
            "clntNRIC" => "580413-07-5750",
            "clntOldIC" => "",
            "effDt" => $this->getSharedQuotationData('effDt', $datenow->format('Ymd')),
            "hNcdReplyMsg" => "null",
            "esiReplyMsg" => "",
            "esiSucessfulRefNo" => "",
            "ismMarketValue" => "0",
            "recommendedSI" => "0",
            "prevInputOfYrManf" => "null",
            "prevIsReqISMValuation" => "",
            "currIsReqISMValuation" => "",
            "isESIClauseSelected" => "false",
            "searchType" => "",
            "selectedId" => "",
            "ncdsent" => "",
            "standard" => "false",
            "isTheftProne" => "false",
            "esiReply" => "",
            "autoRated" => "true",
            "occupationCode" => "3ACC",
            "occupationDesc" => "ACCOUNTS CLERK",
            "gender" => "F",
            "coverTypeCode" => "CO",
            "vehicleClass" => "PC",
            "CIFormNo" => "MX1",
            "registrationNo" => "WPS5133",
            "location" => "",
            "region" => "",
            "engineNo" => "",
            "chassisNo" => "",
            "makeCode" => "",
            "modelCode" => "",
            "prevModelCode" => "",
            "prevModelDesc" => "",
            "makeDesc" => "",
            "modelDesc" => "",
            "tonnage" => "0.00",
            "capacityHidden" => "0",
            "capacity" => "0",
            "capacityType" => "C",
            "yearOfManufacture" => "",
            "variantNvic" => "",
            "nvic" => "NA",
            "numOfSeats" => "0",
            "logBookNo" => "",
            "color" => "",
            "permittedDrivers" => "01",
            "vehicleRegnClass" => "02",
            "antiTheftDevices" => "",
            "safetyFeatures" => "",
            "garageLocation" => "",
            "importedType" => "",
            "purposeOfUse" => "01",
            "purchaseDate" => "",
            "purchasePrice" => "0.00",
            "exInsurer" => "",
            "exPolicyNo" => "",
            "exVehicleRegNo" => "",
            "previousInceptionDate" => "",
            "previousExpiryDate" => "",
            "newScreen" => "false",
            "vehicleSumInsured" => "0",
            "oldSumInsured" => "0.0",
            "totalSumInsured" => "0",
            "motorBasicPremium" => "0.00",
            "loadingAmount" => "0",
            "grossBasicPremium" => "0",
            "ncbPercent" => "0.000",
            "ncbAmount" => "0.00",
            "ncbVariation" => "true",
            "ncdClaimsFreeYr" => "0",
            "ncdAnniversaryDay" => "",
            "ncdAnniversaryMth" => "",
            "nettPremium" => "0.00",
            "hCoverID" => "E57A",
            "additionalCovers[0].premium" => "0.0",
            "additionalCovers[1].premium" => "0.0",
            "additionalCovers[2].premium" => "0.0",
            "additionalCovers[3].premium" => "0.0",
            "additionalCovers[4].premium" => "0.0",
            "additionalCovers[5].premium" => "0.0",
            "additionalCovers[6].premium" => "0.0",
            "additionalCovers[7].premium" => "0.0",
            "additionalCovers[8].premium" => "0.0",
            "additionalCovers[9].limitAmount" => "0",
            "additionalCovers[9].premium" => "0.0",
            "additionalCovers[10].limitAmount" => "0",
            "additionalCovers[10].premium" => "0.0",
            "additionalCovers[11].limitAmount" => "0",
            "additionalCovers[11].premium" => "0.0",
            "additionalCovers[12].limitAmount" => "0.00",
            "additionalCovers[12].premium" => "0.0",
            "additionalCovers[13].premium" => "0.0",
            "additionalCovers[14].limitAmount" => "0.00",
            "additionalCovers[14].premium" => "0.0",
            "additionalCovers[15].limitAmount" => "0.00",
            "additionalCovers[15].premium" => "0.0",
            "additionalCovers[16].limitAmount" => "0.00",
            "additionalCovers[16].premium" => "0.0",
            "additionalCovers[17].limitAmount" => "0.00",
            "additionalCovers[17].premium" => "0.0",
            "additionalCovers[18].premium" => "0.0",
            "additionalCovers[19].premium" => "0.0",
            "additionalCovers[20].premium" => "0.0",
            "additionalCovers[21].premium" => "0.0",
            "additionalCovers[22].premium" => "0.0",
            "additionalCovers[23].premium" => "0.0",
            "totalPremium" => "0.00",
            "postedPremium" => "0.00",
            "excessType" => "",
            "excessAmount" => "0",
            "oldExcessAmount" => "0.0",
            "interestedParties[0].ipName" => "",
            "interestedParties[0].roleDesc" => "",
            "interestedParties[0].bankRefNo" => "",
            "interestedParties[0].ipId" => "",
            "interestedParties[0].roleId" => "",
        ];

        $merged = $this->mergeAndOverride($defaults, $overrides, true);

        return $merged;
    }

    public function quotation($overrides = []) {
        $datenow = Carbon::today();

        $defaults = [
            'ncdReplyNo' => '',
            'oldCoverTypeCode' => '',
            'headerEffDate' => $this->getSharedQuotationData('headerEffDate', $datenow->format('d/m/Y')),
            'encdVehClass' => '',
            'encdPercent' => '0.0',
            'isFoundInWNCD' => 'false',
            'encdCutOffDate' => '20110701',
            'isFoundInWESI' => 'false',
            'isCovernoteFound' => 'N',
            'hasReadFromWESI' => 'false',
            'clntNRIC' => '811228-01-5284',
            'clntOldIC' => '',
            'effDt' => $this->getSharedQuotationData('effDt', $datenow->format('Ymd')),
            'hNcdReplyMsg' => 'null',
            'esiReplyMsg' => '',
            'esiSucessfulRefNo' => '',
            'ismMarketValue' => '0',
            'recommendedSI' => '0',
            'prevInputOfYrManf' => 'null',
            'prevIsReqISMValuation' => '',
            'currIsReqISMValuation' => '',
            'isESIClauseSelected' => 'false',
            'searchType' => '',
            'selectedId' => '',
            'ncdsent' => '',
            'standard' => 'true',
            'isTheftProne' => 'false',
            'esiReply' => '',
            'autoRated' => 'true',
            'occupationCode' => '5ACC',
            'occupationDesc' => 'ACCOUNTANT',
            'gender' => 'F',
            'coverTypeCode' => 'CO',
            'vehicleClass' => 'PC',
            'CIFormNo' => 'MX1',
            'registrationNo' => $this->getSharedQuotationData('regno', 'PKV8098'),
            'location' => $this->getSharedQuotationData('location', ''),
            'region' => $this->getSharedQuotationData('region', ''),
            'engineNo' => $this->getSharedQuotationData('engineNo', ''),
            'chassisNo' => $this->getSharedQuotationData('chassisNo', ''),
            'makeCode' => $this->getSharedQuotationData('makeCode', ''),
            'modelCode' => $this->getSharedQuotationData('modelCode', ''),
            'prevModelCode' => '',
            'prevModelDesc' => '',
            'makeDesc' => $this->getSharedQuotationData('makeDesc', ''),
            'modelDesc' => '',
            'tonnage' => number_format($this->getSharedQuotationData('tonnage', 0), 2, '.', ''),
            'capacityHidden' => '0',
            'capacity' => $this->getSharedQuotationData('capacity', '0'),
            'capacityType' => 'C',
            'yearOfManufacture' => $this->getSharedQuotationData('yearOfManufacture', ''),
            'variantNvic' => $this->getSharedQuotationData('variantNvic', ''),
            'nvic' => $this->getSharedQuotationData('nvic', 'NA'),
            'numOfSeats' => $this->getSharedQuotationData('numOfSeats', '0'),
            'logBookNo' => '123456',
            'color' => '',
            'permittedDrivers' => '01',
            'vehicleRegnClass' => '02',
            'antiTheftDevices' => $this->getSharedQuotationData('antiTheftDevices', ''),
            'safetyFeatures' => $this->getSharedQuotationData('safetyFeatures', ''),
            'garageLocation' => $this->getSharedQuotationData('garageLocation', ''),
            'importedType' => $this->getSharedQuotationData('importedType', ''),
            'purposeOfUse' => '01',
            'purchaseDate' => '',
            'purchasePrice' => '0.00',
            'exInsurer' => '',
            'exPolicyNo' => '',
            'exVehicleRegNo' => '',
            'previousInceptionDate' => '',
            'previousExpiryDate' => '',
            'newScreen' => 'false',
            'vehicleSumInsured' => (string) $this->getSharedQuotationData('vehicleSumInsured', '0'),
            'oldSumInsured' => $this->getSharedQuotationData('oldSumInsured', '0.0'),
            'totalSumInsured' => '0',
            'motorBasicPremium' => $this->getSharedQuotationData('motorBasicPremium', '0.00'),
            'loadingAmount' => '0',
            'grossBasicPremium' => $this->getSharedQuotationData('grossBasicPremium', '0'),
            'ncbPercent' => '0.000',
            'ncbAmount' => '0.00',
            'ncbVariation' => 'true',
            'ncdClaimsFreeYr' => '0',
            'ncdAnniversaryDay' => '',
            'ncdAnniversaryMth' => '',
            'nettPremium' => $this->getSharedQuotationData('nettPremium', '0.00'),
            'additionalCovers[0].premium' => '0.0',
            'additionalCovers[1].premium' => '0.0',
            'additionalCovers[2].premium' => '0.0',
            'additionalCovers[3].premium' => '0.0',
            'additionalCovers[4].premium' => '0.0',
            'additionalCovers[5].premium' => '0.0',
            'additionalCovers[6].premium' => '0.0',
            'additionalCovers[7].premium' => '0.0',
            'additionalCovers[8].premium' => '0.0',
            'additionalCovers[9].limitAmount' => '0',
            'additionalCovers[9].premium' => '0.0',
            'additionalCovers[10].limitAmount' => '0',
            'additionalCovers[10].premium' => '0.0',
            'additionalCovers[11].limitAmount' => '0',
            'additionalCovers[11].premium' => '0.0',
            'additionalCovers[12].limitAmount' => '0.00',
            'additionalCovers[12].premium' => '0.0',
            'additionalCovers[13].premium' => '0.0',
            // 'additionalCovers[13].limitAmount' => '0.00',
            'additionalCovers[14].premium' => '0.0',
            'additionalCovers[14].limitAmount' => '0.00',
            'additionalCovers[15].limitAmount' => '0.00',
            'additionalCovers[15].premium' => '0.0',
            'additionalCovers[16].limitAmount' => '0.00',
            'additionalCovers[16].premium' => '0.0',
            'additionalCovers[17].limitAmount' => '0.00',
            'additionalCovers[17].premium' => '0.0',
            // 'additionalCovers[18].limitAmount' => '0.00',
            'additionalCovers[18].premium' => '0.0',
            'additionalCovers[19].premium' => '0.0',
            'additionalCovers[20].premium' => '0.0',
            'additionalCovers[21].premium' => '0.0',
            'additionalCovers[22].premium' => '0.0',
            'additionalCovers[23].premium' => '0.0',
            // 'additionalCovers[24].premium' => '0.0',
            'totalPremium' => '0.00',
            'postedPremium' => '0.00',
            'excessType' => 'D',
            'excessAmount' => '0',
            'oldExcessAmount' => '0.0',
            'interestedParties[0].ipName' => '',
            'interestedParties[0].roleDesc' => '',
            'interestedParties[0].bankRefNo' => '',
            'interestedParties[0].ipId' => '',
            'interestedParties[0].roleId' => '',
        ];

        $merged = $this->mergeAndOverride($defaults, $overrides, true);

        return $merged;
    }

    public function quotationQuery($quotationData) {
        $quotationDataQuery = http_build_query($quotationData);
        $hCoverIdsQuery = collect($this->extraCoverCodes)
        ->map(function($item) {
            return http_build_query([$item['key'] => $item['value']]);
        })
        ->implode('&');

        return $quotationDataQuery . '&' . $hCoverIdsQuery;
    }

    public function findBasicPremium($overrides = []) {
        $defaults = [
            'location' => 'JHR',
            'region' => 'W',
            'engineNo' => 'L15A74603698',
            'chassisNo' => 'PMHGM2660CD203734',
            'makeCode' => '11',
            'modelCode' => '11    03',
            'makeDesc' => 'HONDA CITY',
            'tonnage' => '1497.00',
            'capacity' => '1497',
            'yearOfManufacture' => '2012',
            'variantNvic' => 'E 4D SEDAN 5 SP AUTOMATIC-HH012A',
            'nvic' => 'HH012A',
            'numOfSeats' => '4',
            'antiTheftDevices' => '12',
            'safetyFeatures' => '06',
            'garageLocation' => '31',
            'importedType' => '01',
            'vehicleSumInsured' => '29000',
            'totalSumInsured' => '29000',
        ];

        return $this->mergeAndOverride($defaults, $overrides, true);
    }

    public function getExtraCoverageInputName($name) {
        $code = static::$extraCoverCodeMap[$name];
        $key = collect($this->extraCoverCodes)->where('value', $code);
        $inputKey = intval($key->keys()->first()) + 1;
        return "additionalCovers[{$inputKey}]";
    }

    /**
     * After add extra callback
     *
     * @param string $name
     * @return void
     */
    public function onAfterAddExtra($name) {
        $inputName = $this->getExtraCoverageInputName($name);

        switch($name) {
            case 'liabilityPass': 
                $this->extraCoverList["{$inputName}.bought"] = 'true';
            break;

            case 'cart': 
                $this->extraCoverList["{$inputName}.bought"] = 'true';
                $this->extraCoverList["{$inputName}.limitAmount"] = '50.0';
            break;
            
            case 'waiver':
                $this->extraCoverList["{$inputName}.bought"] = 'true';
            break;
            
            case 'specialPeril':
                $this->extraCoverList["{$inputName}.bought"] = 'true';
            break;
            
            case 'strike':
                $this->extraCoverList["{$inputName}.bought"] = 'true';
            break;
            
            case 'laminated':
                $this->extraCoverList["{$inputName}.bought"] = 'true';
                $this->extraCoverList["{$inputName}.limitAmount"] = $this->extraCoverages['laminated'];
            break;

            default:
            
            break;
        }
    }

    public function addExtraCover($data) {
        if(!$this->extraCoverList) {
            return $data;
        }
        
        return array_merge($data, $this->extraCoverList);
    } 

    public function createClient($overrides = []) {
        $defaults = [
            "test" => "false",
            "requestTo" => "saveNew",
            "recordType" => "1",
            "from" => "PolicyMgmt",
            "clientUpdate" => "",
            "postalCode" => "47600",
            "townCode" => "BPJ",
            "townDesc" => "U.E.P., SUBANG JAYA, PJ",
            "oldFullName" => "",
            "clientInd" => "",
            "ctryCompanyID" => "1",
            "salutation" => "MRS",
            "fullName" => "HASMAHH BINTI AHMAD",
            "nationalityID" => "MAL",
            "NRIC" => "580413-07-5750",
            "oldIC" => "",
            "sex" => "F",
            "maritalID" => "M",
            "DOB" => "13/04/1958",
            "occupID" => "7RET",
            "category" => "1",
            "raceID" => "MAL",
            "address1" => "39 USJ 2/4C",
            "address2" => "",
            "address3" => "",
            "address4" => "",
            "postal" => "47600",
            "countryID" => "MAL",
            "areaHome" => "014",
            "telHome" => "",
            "areaOffice" => "",
            "telOffice" => "",
            "areaHandphone" => "",
            "handphone" => "",
            "areaFax" => "",
            "fax" => "",
            "email" => "",
            "regNo" => "",
            "regDate" => "",
            "regExpDate" => "",
            "cDDConducted1" => "true",
            "photocopyAttd1" => "true",
            "pdpaFlag" => "true",
            "Submit" => "Submit",
        ];

        return $this->mergeAndOverride($defaults, $overrides);
    }

    public function enquireNCD($overrides = []) {
        $defaults = [
            "act" => "send",
            "fr" => "pol",
            "transactionType" => "CF",
            "confirmationType" => "ENQ",
            "previousVehicleRegNo" => "CDL1800",
            "coverCode" => "CO",
            "vehicleClassCode" => "PC 01",
            "nric" => "860519-29-5379",
            "oldic" => "",
            "registrationNo" => "CDL1800",
            "chassisNo" => "PL1BT3SNREB450263",
            "exInsurer" => "",
            "exPolicyNo" => "",
        ];

        return $this->mergeAndOverride($defaults, $overrides);
    }

    /**
     * Add Extra Coverage
     *
     * @param string $name
     * @param int $sumInsured
     * @return void
     */
    public function addExtra($name, $sumInsured) {
        if($name == 'tinting') {
            $name = 'laminated';
        }

        if(!in_array($name, $this->selectedExtraCoverages)) {
            array_push($this->selectedExtraCoverages, $name);
        }

        $this->extraCoverages[$name] = $sumInsured;

        $this->onAfterAddExtra($name);
    }

}
