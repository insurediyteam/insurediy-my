<?php 

namespace Services\MsigClient;

use Exception;
use Models\Eloquent\BodyType;
use Models\Eloquent\Detailable;
use Services\AmgClient;
use Models\Eloquent\Make;
use Models\Eloquent\MakeProvider;
use Models\Eloquent\ModelProvider;
use Services\BaseClientSubclass;
use Services\Client\BaseListData;
use Services\Contracts\Client\Subclass\VehicleContract;

class Vehicle extends BaseClientSubclass implements VehicleContract {

    protected $mappedModelOptions;

    protected function getMakeModelDom() {
        if(!isset($this->makeModelDom)) {
            $response = $this->step1QuotationRequest();
            $this->makeModelDom = $this->htmlToDom((string) $response->getBody());
        }

        return $this->makeModelDom;
    }

    protected function getMappedModelOptions() {
        if(!isset($this->mappedModelOptions)) {
            $selectDomObject = $this->getMakeModelDom()->filter('select[name=\'modelCode\']')->first();

            $selectOptions = $this->parser->selectDataFromDom($selectDomObject);
            array_shift($selectOptions);

            $this->mappedModelOptions = collect($selectOptions)
                ->map(function($item) {
                    $makeModelCode = explode(' ', preg_replace('/\s+/', ' ', $item['value']));
                    return [
                        'make' => $makeModelCode[0],
                        'model' => $makeModelCode[1],
                        'modelDesc' => trim($item['text']),
                    ];
                })
                ->groupBy('make');
        }

        return $this->mappedModelOptions;
    }

    public function makeListData() {
        $selectDomObject = $this->getMakeModelDom()->filter('select[name=\'makeCode\']')->first();
        
        $data = $this->parser->selectDataFromDom($selectDomObject);

        array_shift($data);

        return array_map(function($value) {
            return new BaseListData($value['value'], $value['text']);
        }, $data);
    }

    public function modelListData($makeCode, $type = null, $cover = null, $use = null) {
        $modelOptions = isset($this->getMappedModelOptions()[$makeCode]) ? $this->getMappedModelOptions()[$makeCode] : [];
        return array_map(function($value) {
            return new BaseListData($value['model'], $value['modelDesc']);
        }, $modelOptions ? $modelOptions->toArray() : []);
    }

    public function bodyListData() {
        return BodyType::get()
            ->map(function($bodyType) {
                return new BaseListData($bodyType->name, $bodyType->name);
            })
            ->toArray();
    }

}
