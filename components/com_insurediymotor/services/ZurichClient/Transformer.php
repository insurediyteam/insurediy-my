<?php

namespace Services\ZurichClient;

use Exception;
use Concerns\IcData;
use Models\Eloquent\Quote;
use Concerns\TransformerTrait;
use Services\BaseClientSubclass;
use Services\Client\ComparisonTableData;

class Transformer extends BaseClientSubclass
{

    use TransformerTrait;

    /**
     * airbag:abs
     *
     * @var array
     */
    public static $safeties = [
        '1:no' => '01',
        '2:no' => '02',
        '1:yes' => '05',
        '2:yes' => '06',
    ];

    public static $vehicleFroms = [
        'new' => 'CBN',
        'old' => 'CBU',
    ];

    public static $coverTypes = [
        'C' => 'V-CO',
        'T' => 'VPPTP0201',
    ];

    public static $garages = [
        '1' => '5',
    ];

    public static $antitds = [
        '1' => '12',
    ];

    public static $drvexps = [
        '1' => '5',
    ];

    public static $provNCDs = [
        '2' => '0.00',
        '3' => '25.00',
        '4' => '30.00',
        '5' => '38.33',
        '6' => '45.00',
        '7' => '55.00',
    ];

    public static $genders = [
        'MALE' => 'M',
        'FEMALE' => 'F',
    ];

    public static $nametitles = [
        '1' => 'DATO',
    ];

    public static $maritalstats = [
        'Married' => 'M',
        'Single' => 'S',
    ];

    public static $areas = [
        '1' => '05',
        '2' => '07',
        '3' => '02',
        '4' => '12',
        '5' => '15',
        '6' => '08',
        '7' => '09',
        '8' => '10',
        '9' => '04',
        '10' => '01',
        '11' => '03',
        '12' => '13',
        '13' => '14',
        '14' => '11',
    ];

    /**
     * Transform to compatible generator overrides
     *
     * @param array $inputs
     * @return array
     */
    public function inputs($inputs)
    {
        // convert to optional array, returns null when calling undefined key / index / props
        $inputs = $this->optional($inputs);

        $drivers = $this->getDrivers($inputs);
        
        $transformedInputs = [
            'vehicleRegNo' => $inputs['vehicleRegNo'],
            'makeYear' => $inputs['carMakeYear'],
            'capacity' => $inputs['vehicleEngineCc'],
            'chassis' => $inputs['chassisNo'],
            'engine' => $inputs['engineNo'],
            'ncd' => $this->select('provNCDs', $inputs['ncd']),
            'state' => $this->select('areas', $inputs['location']),
            'model' => $this->getCarModelCode($inputs['carModel']),
            'sumInsured' => $inputs['marketValue'],
            'bodyType' => $this->getCarBodyTypeCode($inputs['carBodyType'], false),
            'occupation' => $this->getOccupationCode($inputs['occupation'], $this->config('filter_occupation', false)),
            'region' => Quote::locationRegion($inputs['location']) == 'west' ? 'W' : 'E',
        ];

        /**
         * Override sum insured by the agreed value
         */
        if ($inputs['sumInsured']) {
            $transformedInputs['sumInsured'] = $inputs['sumInsured'];
            $this->client->setSumInsured(intval($inputs['sumInsured']));
        }

        // transform optional driver data
        $this->transformDriver($transformedInputs, $inputs);

        // filter, remove null-equal values
        return $this->filterValue($transformedInputs);
    }

    protected function transformDriver(&$transformedInputs, $inputs)
    {
        $drivers = $this->getDrivers($inputs);

        if (count($drivers) === 0) {
            return;
        }

        $driver = $drivers[0];
        
        $ic = new IcData($drivers[0]['icNum']);

        /** @var \Services\ZurichClient $this */
        $postCodeData = $this->getPostCodeData($driver['postalCode']);

        $transformedInputs['ic'] = $ic->number;
        $transformedInputs['clientName'] = $driver['name'];
        $transformedInputs['postCode'] = $driver['postalCode'];
        $transformedInputs['town'] = $postCodeData['town'];
        $transformedInputs['state'] = $postCodeData['state'][0];
        $transformedInputs['country'] = $postCodeData['country'][0];
        $transformedInputs['driverCount'] = (string) count($drivers);
        $transformedInputs['allDriver'] = '0';
        $transformedInputs['gender'] = $driver['Gender'] == 'MALE' ? 'M' : 'F';
    }

}
