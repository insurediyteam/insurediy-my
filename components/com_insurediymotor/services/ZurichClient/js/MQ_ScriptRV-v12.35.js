https://eins.zurich.com.my/Motor2/Scripts/CoverNote/MQ_ScriptRV.js?V12.35
function fnGetIP() {
    $.ajax({
        url: "https://eins4.zurich.com.my/GetMyIP.aspx",
        success: function(n) {
            n != undefined && $("[id$=hdfIPAddrs]").val(n)
        }
    })
}

function TimerInsertFun(n, t) {
    try {
        if ($("[id$=hdChkTimer]").val() == "Y") {
            var i = JSON.stringify({
                EventName: n,
                PrimaryId: t
            });
            $.ajax({
                type: "POST",
                async: !0,
                contentType: "application/json; charset=utf-8",
                url: "NewCNote.aspx/UpdateProcessTime",
                data: i,
                dataType: "json",
                success: function(n) {
                    $("[id$=hdPrimaryId]")[0].value = n.d
                },
                error: function() {}
            })
        }
    } catch (r) {}
}

function TimerUpdateFun(n) {
    try {
        if ($("[id$=hdChkTimer]").val() == "Y") {
            var t = JSON.stringify({
                EventName: n,
                PrimaryId: $("[id$=hdPrimaryId]")[0].value
            });
            $.ajax({
                type: "POST",
                async: !0,
                contentType: "application/json; charset=utf-8",
                url: "NewCNote.aspx/UpdateProcessTime",
                data: t,
                dataType: "json",
                success: function() {
                    $("[id$=hdPrimaryId]")[0].value = ""
                },
                error: function(n) {
                    DisplayError(n)
                }
            })
        }
    } catch (i) {
        DisplayError(i)
    }
}

function pageLoad() {
    try {
        fnCICode_IndexChanged();
        var n = document.getElementById($("[id$=hdfControlIDs]")[0].id),
            t = document.getElementById(n.attributes.txtYearofMake.value),
            i = $find(n.attributes.ddlMake.value),
            r = $find(n.attributes.ddlModel.value),
            u = $find(n.attributes.txtcapacity.value),
            f = $find(n.attributes.ddlUOM.value),
            e = $find(n.attributes.txtLogBookNo.value);
        fnCheckExcelVer($("[id$=hdfQuoNo]")[0].value, "3", t.value, $("[id$=hdfVehModelId]")[0].value);
        LoadComboBox(i);
        LoadComboBox(r);
        LoadTextBox(u);
        LoadTextBox(e);
        LoadComboBox(f);
        sessionStorage.setItem("PreventEvent", "N");
        var n = document.getElementById($("[id$=hdfControlIDs]")[0].id),
            o = $find(n.attributes.ddlProductCode.value),
            s = $find(n.attributes.ddlTransactionType.value),
            h = $find(n.attributes.ddlCoverType.value);
        GetUIControlSettings(s.get_value(), o.get_value(), h.get_value());
        PremiumBind()
    } catch (c) {}
}

function PremiumBind() {
    var n = JSON.parse($("[id$=MstQuoDtls]")[0].value),
        t = $("[id$=dblPrevSumIns]")[0].value,
        i, r;
    n.mstMTQuo != undefined && n.mstMTQuo != "" && (i = document.getElementById($("[id$=hdfControlIDs]")[0].id), r = document.getElementById(i.attributes.txtPrevSI.value), $("[id$=dblPrevSumIns]")[0].value = n.mstMTQuo[0].RenewalPrevSumIns, t = n.mstMTQuo[0].PrevSumIns, t > 0 && (r.value = t), $("[id$=hdfOptionalInd]")[0].value = n.mstMTQuo[0].WindScreenInd)
}

function fnLowerToUpperCase(n) {
    n.set_displayValue(n._text.trim().toUpperCase());
    n._SetValue(n._text.trim().toUpperCase())
}

function TextBoxTextChanged(n, t) {
    fnLowerToUpperCase(n, t);
    String(n._text).trim() == "" && (n.set_displayValue(n._text.trim()), n._SetValue(n._text.trim()));
    n.updateCssClass()
}

function UpperCase(n) {
    var t = n.value.toUpperCase();
    document.getElementById(n.id).value = t
}

function fnRegionIndexChanged(n, t) {
    var i = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        o;
    ComboIndexChanged(n, t);
    var r = $find(i.attributes.ddlProductCode.value),
        u = document.getElementById(i.attributes.txtYearofMake.value),
        s = $find(i.attributes.ddlCoverType.value),
        f = parseInt($("[id$=hdfVcoApplicableYear]").val());
    if (n.get_value() != "" && n.get_value() != "0" && s.get_value() != "V-FT" && (new Date).getFullYear() - parseInt(u.value) > f && (r.get_value() == "PC01" || r.get_value() == "PZ01") && u.value != "") {
        var e = $find(i.attributes.mpeNotification_V.value),
            h = document.getElementById(i.attributes.trNotify_V.value),
            c = document.getElementById(i.attributes.lblNotifyMsg.value);
        e != null && (c.innerHTML = "Third Party Fire & Theft Cover is recommended for vehicle age above " + f + " years.<br/> <center>Do you want to change Cover Type?<center/>", h.style.display = "", e.show())
    } else(n.get_value() == "" || n.get_value() == "0") && (o = n.get_inputDomElement(), o.focus())
}

function ComboSelectedIndexChanged(n, t) {
    var f;
    try {
        var i = document.getElementById($("[id$=hdfControlIDs]")[0].id),
            s = $find(i.attributes.ddlIsOwnedCar.value),
            e = $find(i.attributes.ddlVOCYear.value),
            o = $find(i.attributes.ddlVOCMonth.value),
            h = $find(i.attributes.ddlIsModifiedCar.value),
            r = $find(i.attributes.ddlModifiedPerform.value),
            u = $find(i.attributes.ddlModifiedFunctional.value);
        n.get_id().indexOf("ddlIsOwnedCar") > -1 && (f = t.get_item(), e != undefined && (e.clearSelection(), e.set_emptyMessage("YYYY")), o != undefined && (o.clearSelection(), o.set_emptyMessage("MMM")), s != undefined && (s.get_inputDomElement().style.backgroundColor = "white"), f.get_value() == "1" && document.getElementById("trIsOwnedCar").style.display == "table-row" ? ($("#trVOCdt").show(), $("#spVOCdt").show()) : $("#trVOCdt").hide());
        n.get_id().indexOf("ddlIsModifiedCar") > -1 && (f = t.get_item(), r != undefined && (r.clearSelection(), r.set_emptyMessage("Select Performance Based Modification Type")), u != undefined && (u.clearSelection(), u.set_emptyMessage("Select Functional Based Modification Type")), h != undefined && (h.get_inputDomElement().style.backgroundColor = "white"), f.get_value() == "1" ? ($("#trModifiedPerform").show(), $("#trModifiedFunctional").show()) : ($("#trModifiedPerform").hide(), $("#trModifiedFunctional").hide(), UncheckComboItems(r), UncheckComboItems(u)))
    } catch (c) {
        DisplayError(c)
    }
}

function UncheckComboItems(n) {
    for (var i = n.get_checkedItems(), t = 0; t < i.length;) i[t].uncheck(), t++
}

function onlyNumber(n, t) {
    var i = n.get_value() + t.get_keyCharacter();
    i.match("^[0-9]+$") || t._keyCode == 8 || t._keyCode == 46 || t.set_cancel(!0)
}

function MobileIndexChanged(n, t) {
    var r = document.getElementById($("[id$=hdfControlIDs]")[0].id);
    ComboIndexChanged(n, t);
    var u = document.getElementById(r.attributes.lblErrorMobileNo.value),
        f = document.getElementById(r.attributes.chckMobile.value),
        i = $find(r.attributes.txtPhNo.value);
    u.innerHTML = "";
    f.checked || (n.get_value() == "" || n.get_value() == "0" ? (u.innerHTML = "* Please select prefix!", n.get_inputDomElement().style.backgroundColor = "#FFF284") : i._text != "" && i._text.length < 7 ? (i.focus(), u.innerHTML = "* Invalid Mobile Number!") : i._text == "" && i.focus())
}

function fnRedirectSummary(n) {
    window.location.href = "QuotationSummary.aspx?Ind=" + n._value
}

function MobileNo_TextChanged(n) {
    var i = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        t = document.getElementById(i.attributes.lblErrorMobileNo.value),
        r = $find(i.attributes.ddlMblPrefix.value);
    t.innerHTML = "";
    r.get_value() == "" || r.get_value() == "0" && n._text != "" ? t.innerHTML = "* Please select prefix!" : n._text.length < 7 && n._text.length > 0 && (n.focus(), t.innerHTML = "* Invalid Mobile Number!")
}

function LoadMblPrefix(n) {
    LoadComboBox(n);
    n._enabled || (n.get_inputDomElement().style.backgroundColor = "white")
}

function OccupationIndexChanged(n) {
    var t = $("#lblErrorOccupation");
    t.innerHTML = "";
    n.get_value() != "" && n.get_value() != "0" ? n.get_inputDomElement().style.backgroundColor = "white" : (n.get_value() == "" || n.get_value() == "0") && (t.innerHTML = " * Please select Occupation.", n.get_inputDomElement().style.backgroundColor = "#FFF284")
}

function LoadNonMandatory(n) {
    n.get_styles().EmptyMessageStyle[0] = NonMandatory;
    n.get_styles().EnabledStyle[0] = NonMandatory;
    n.get_styles().DisabledStyle[0] = NonMandatory;
    n.updateCssClass()
}

function LoadTextBox(n) {
    n.get_styles().EmptyMessageStyle[0] += "background-color: #FFF284;";
    n.updateCssClass()
}

function fnDisplayMainErrorMsg(n, t) {
    var i = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        f = document.getElementById(i.attributes.lblErrorMsg.value),
        e = document.getElementById(i.attributes.lbltopErrorMsg.value),
        r = document.getElementById(i.attributes.pnlbtnErrorMsg.value),
        u = document.getElementById(i.attributes.pnltopErrorMsg.value);
    e.innerHTML = f.innerHTML = t;
    t != "" ? (r.style.display = "block", u.style.display = "block") : (r.style.display = "none", u.style.display = "none")
}

function GetUIControlSettings(n, t, i) {
    var r, u;
    try {
        jsonUISetting = "";
        r = "";
        r = $("[id$=hdfQuoStage]").val();
        u = JSON.stringify({
            QuoStage: r
        });
        $.ajax({
            type: "POST",
            async: !0,
            contentType: "application/json; charset=utf-8",
            url: ServiceURL() + "/fnGetUIControlSettings",
            data: u,
            dataType: "json",
            success: function(r) {
                jsonUISetting = JSON.parse(r.d);
                sessionStorage.setItem("jsonUISetting", JSON.stringify(jsonUISetting));
                window.location.href.indexOf("?QN") > -1 && SetUIControlSettings(n, t, i)
            },
            error: function(n) {
                DisplayError(n)
            }
        })
    } catch (f) {
        DisplayError(f)
    }
}

function TelNo_Textblur(n) {
    var r = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        u = document.getElementById(r.attributes.lblTelNoError.value),
        i, t;
    u.innerHTML = "";
    n.maxLength == 3 ? (t = document.getElementById(r.attributes.txtOffPhNo.value), i = n) : (i = document.getElementById(r.attributes.txtOffPrefix.value), t = n);
    i.value.length < 2 && (i.value != "" || t.value != "") ? u.innerHTML = i.value == "" ? "* The prefix number cannot be blank!" : "* The prefix number you entered is invalid!" : t.value.length < 6 && (i.value != "" || t.value != "") && (u.innerHTML = t.value == "" ? "* The number cannot be blank!" : "* The number you entered is invalid!")
}

function TelNo_TextChanged(n) {
    var u = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        f = document.getElementById(u.attributes.lblTelNoError.value),
        t, i, r;
    return f.innerHTML = "", t = 0, n.maxLength == 3 ? (r = document.getElementById(u.attributes.txtOffPhNo.value), i = n, t = 1) : (i = document.getElementById(u.attributes.txtOffPrefix.value), r = n, t = 2), t == 1 ? i.value.length == 3 && r.focus() : t == 2 && i.value.length < 2 && (i.value != "" || r.value != "") && (f.innerHTML = "* The prefix number you entered is invalid!"), ""
}

function fnChkSplChars(n, t) {
    var i = t.get_keyCharacter();
    /[^a-zA-Z0-9-]/g.test(i) && t.set_cancel(!0)
}

function DisplayError(n) {
    $("[id$=_pnlbtnErrorMsg]").css("display", "block");
    $("[id$=_pnltopErrorMsg]").css("display", "block");
    $("[id$=lbltopErrorMsg]").html(n);
    $("[id$=lblErrorMsg]").html(n)
}

function SetUIControlSettings(n, t, i) {
    var r, y;
    try {
        if (n = n == "BR" ? $("[id$=hdfTrnsType]").val() : n, jsonUISetting = sessionStorage.getItem("jsonUISetting") == null ? "" : JSON.parse(sessionStorage.getItem("jsonUISetting")), filterUICtrlSetting = jsonUISetting.filter(function(r) {
                return r.sdfTransTypeID === n && r.sdfProductCodeID === t && r.sdfCoverTypeID === i
            }), filterUICtrlSetting.length > 0) {
            var f = document.getElementById($("[id$=hdfControlIDs]")[0].id),
                a = $find(f.attributes.txtEmgContactName.value),
                v = $find(f.attributes.txtEmgMobNo.value),
                o = $find(f.attributes.ddlEmgMobNoPrefix.value),
                e = $find(f.attributes.ddlIsOwnedCar.value),
                s = $find(f.attributes.ddlVOCYear.value),
                h = $find(f.attributes.ddlVOCMonth.value),
                u = $find(f.attributes.ddlIsModifiedCar.value),
                c = $find(f.attributes.ddlModifiedPerform.value),
                l = $find(f.attributes.ddlModifiedFunctional.value);
            for (r = 0, y = filterUICtrlSetting.length; r < y; r++) switch (filterUICtrlSetting[r].sdfUIControlID) {
                case "OwnedCar":
                    filterUICtrlSetting[r].DisplayInd == "true" ? (document.getElementById("trIsOwnedCar").style.display = "table-row", $("#trIsOwnedCar").show(), window.location.href.indexOf("?QN") > -1 || (e.clearSelection(), n == "V" ? e.findItemByValue("1").select() : e.set_emptyMessage("Select"), u.commitChanges(), u.trackChanges(), s.clearSelection(), s.set_text(""), h.clearSelection(), h.set_emptyMessage("MMM"), h.set_text("")), e.get_attributes().setAttribute("ValidationInd", filterUICtrlSetting[r].ValidationInd == "true"), s.get_attributes().setAttribute("ValidationInd", filterUICtrlSetting[r].ValidationInd == "true"), filterUICtrlSetting[r].ValidationInd == "true" ? ($("#spIsOwnedCar").show(), e.get_inputDomElement().style.backgroundColor = e.get_value() == "" ? "#FFF284" : "white") : ($("#spIsOwnedCar").hide(), e.get_inputDomElement().style.backgroundColor = "white")) : ($("#trIsOwnedCar").hide(), $("#trVOCdt").hide());
                    break;
                case "ModifiedCar":
                    filterUICtrlSetting[r].DisplayInd == "true" ? ($("#trIsModifiedCar").show(), window.location.href.indexOf("?QN") > -1 || (n == "V" ? u.findItemByValue("0").select() : u.set_emptyMessage("Select"), u.clearSelection(), u.set_value(""), u.commitChanges(), u.trackChanges()), window.location.href.indexOf("?QN") > -1 || (c.clearSelection(), c.set_emptyMessage("Select Performance Based Modification Type"), l.clearSelection(), l.set_emptyMessage("Select Functional Based Modification Type")), c.get_attributes().setAttribute("ValidationInd", filterUICtrlSetting[r].ValidationInd == "true"), l.get_attributes().setAttribute("ValidationInd", filterUICtrlSetting[r].ValidationInd == "true"), filterUICtrlSetting[r].ValidationInd == "true" ? ($("#spIsModifiedCar").show(), u.get_inputDomElement().style.backgroundColor = u.get_value() == "" ? "#FFF284" : "white") : ($("#spIsModifiedCar").hide(), u.get_inputDomElement().style.backgroundColor = "white")) : $("#trIsModifiedCar").hide();
                    break;
                case "NamedDr":
                    IsNamedDriverValidate = filterUICtrlSetting[r].ValidationInd;
                    fnNDSetBackColor(filterUICtrlSetting[r].DisplayInd == "true", filterUICtrlSetting[r].ValidationInd == "true");
                    break;
                case "EmrngContact":
                    filterUICtrlSetting[r].DisplayInd == "true" ? ($("#trEmgContactName").show(), $("#trEmgMobNo").show(), a._textBoxElement.title = filterUICtrlSetting[r].ValidationInd, v._textBoxElement.title = filterUICtrlSetting[r].ValidationInd, filterUICtrlSetting[r].ValidationInd == "true" ? (TextBoxTextChanged(a), TextBoxTextChanged(v), $("#spEmgMobNoPrefix").show(), $("#spEmgContactName").show()) : ($("#spEmgMobNoPrefix").hide(), $("#spEmgContactName").hide(), o.get_inputDomElement().style.backgroundColor = "white"), window.location.href.indexOf("?QN") > -1 || (o.clearSelection(), o.set_emptyMessage("Select"))) : ($("#trEmgContactName").hide(), $("#trEmgMobNo").hide())
            }
        } else HideControls()
    } catch (p) {
        blockUI(!1)
    }
}

function HideControls() {
    $("#trIsOwnedCar").hide();
    $("#trVOCdt").hide();
    $("#trIsModifiedCar").hide();
    $("#trModifiedPerform").hide();
    $("#trModifiedFunctional").hide();
    $("#trEmgContactName").hide();
    $("#trEmgMobNo").hide()
}

function OnBlurRaised(n) {
    var t = n._clientID;
    $("#" + t).data("ValidationInd") && n.get_value() == "" || $("#" + t).attr("style", "background-color: white;")
}

function fnSelectTodayDate(n) {
    var t = n.get_selectedDate();
    t == null && n.set_selectedDate(new Date)
}

function ComboIndexChanged(n) {
    n.get_value() != "" && n.get_value() != "0" ? n.get_inputDomElement().style.backgroundColor = "white" : n.get_value() == "" || n.get_value() == "0"
}

function fnChckCapacity(n, t) {
    var f = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        c = document.getElementById(f.attributes.hdfCapacityDtls.value),
        u = $find(f.attributes.ddlUOM.value),
        o = $find(f.attributes.ddlModel.value),
        e = document.getElementById(f.attributes.lblErrorCapacity.value),
        s = document.getElementById(f.attributes.lblUOMError.value),
        h = document.getElementById(f.attributes.lblModelErrorMsg.value),
        r, i;
    if (e.innerHTML = s.innerHTML = h.innerHTML = "", r = c.value.split("Â±"), u.get_value() != "0" && u.get_value() != "") {
        for (i = 0; i < r.length; i++)
            if (r[i].split("Ã¾")[1] == u.get_value()) {
                parseFloat(n._value) < parseFloat(r[i].split("Ã¾")[2]) || parseFloat(n._value) > parseFloat(r[i].split("Ã¾")[3]) ? (e.style.color = "#FF0000", e.innerHTML = "* Invalid Capacity ! It should be from " + r[i].split("Ã¾")[2] + " to " + r[i].split("Ã¾")[3], n.sender = null, n._text = "", n.focus()) : n._text == "" && (e.style.color = "#0000FF", e.innerHTML = "* Capacity from " + r[i].split("Ã¾")[2] + " to " + r[i].split("Ã¾")[3], parseFloat(r[i].split("Ã¾")[2]) == parseFloat(r[i].split("Ã¾")[3]) ? (n._value = parseFloat(r[i].split("Ã¾")[2]), n._text = r[i].split("Ã¾")[2]) : (n._value = null, n._text = "", n.focus()));
                break
            }
    } else o.get_value() == "0" || o.get_value() == "" ? (h.style.color = "#FF0000", h.innerHTML = "Please select Model.", ComboIndexChanged(o, t), n._value = null, n._text = "") : (u.get_value() == "0" || u.get_value() == "") && (s.style.color = "#FF0000", s.innerHTML = "Please select UOM.", u._text = "", n._value = null, n._text = "");
    ComboIndexChanged(u, t);
    TextBoxTextChanged(n, t)
}

function LoadDatePicker(n, t, i) {
    var r = i.DatePicker,
        u = r.get_dateInput();
    u.get_value() == "" || (r.get_dateInput()._textBoxElement.style.backgroundColor = "white")
}

function fnclsNotifyCoverage() {
    var n = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        u = $find(n.attributes.btnNotifyYes1.value),
        i = document.getElementById(n.attributes.hdfNo.value);
    i.value = "No";
    var t = $find(n.attributes.mpeNotification_C.value),
        r = document.getElementById(n.attributes.trNotify_C.value);
    t != null && (r.style.display = "", t.hide())
}

function fnCloseModelPopup() {
    var n = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        t = $find(n.attributes.mpeNotification_M.value),
        i = document.getElementById(n.attributes.trNotify_M.value);
    t != null && (i.style.display = "none", t.hide())
}

function fnSetComboItem(n, t, i) {
    var r = new Telerik.Web.UI.RadComboBoxItem;
    r.set_text(i);
    r.set_value(t);
    n.trackChanges();
    n.get_items().add(r);
    r.select();
    n.commitChanges();
    LoadComboBox(n)
}

function fnRequesting(n, t) {
    var r = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        i = t.get_context();
    n._uniqueId.indexOf("ddlTransactionType") != -1 ? i.ComboType = "TransactionType" : n._uniqueId.indexOf("ddlProductCode") != -1 ? (i.ComboType = "ProductCode", i.AgentSource = $("#hdfAgentDtls")[0].value != "" ? JSON.parse($("#hdfAgentDtls")[0].value)[0].BusinessSource : "") : n._uniqueId.indexOf("ddlCoverType") != -1 ? (i.ComboType = "CoverType", i.ProductCode = $find(r.attributes.ddlProductCode.value).get_value()) : n._uniqueId.indexOf("ddlCICode") != -1 ? (i.ComboType = "CICode", i.ProductCode = $find(r.attributes.ddlProductCode.value).get_value()) : n._uniqueId.indexOf("ddlInsIndicator") != -1 ? i.ComboType = "InsuredType" : n._uniqueId.indexOf("ddlModel") != -1 ? (i.ComboType = "Model", i.FilterKeys = $find(r.attributes.ddlProductCode.value).get_value() + "Â±" + 
        $find(r.attributes.ddlMake.value).get_value() + "Â±" + 
        $("[id$=txtYearofMake]").val() + "Â±" + 
        ($("[id$=chckRecon]").prop("checked") == !0 ? "Y" : "N") + "Â±" + 
        ($("[id$=lblTrnType]").val().split("-")[0] == "R" ? $("[id$=hdfVehModelId]").val() : "")) : n._uniqueId.indexOf("ddlMake") != -1 ? (i.ComboType = "Make", i.ProductCode = $find(r.attributes.ddlProductCode.value).get_value()) : n._uniqueId.indexOf("ddlNoofNamedDrivers") != -1 ? (i.ComboType = "NoofDrivers", i.ProductCode = $find(r.attributes.ddlCICode.value).get_value()) : n._uniqueId.indexOf("ddlUOM") != -1 ? i.ComboType = "Capacity" : n._uniqueId.indexOf("ddlRegLocation") != -1 ? i.ComboType = "RegisterLoc" : n._uniqueId.indexOf("ddlRegionCode") != -1 ? i.ComboType = "Region" : n._uniqueId.indexOf("ddlLoanType") != -1 ? i.ComboType = "LoanType" : n._uniqueId.indexOf("ddlMonth") != -1 ? i.ComboType = "Month" : n._uniqueId.indexOf("ddlMaritalSts") != -1 ? i.ComboType = "MaritalStatus" : n._uniqueId.indexOf("ddlOccupation") != -1 ? i.ComboType = "Occupation" : n._uniqueId.indexOf("ddlMblPrefix") != -1 ? i.ComboType = "Telco" : n._uniqueId.indexOf("ddlState") != -1 ? i.ComboType = "State" : n._uniqueId.indexOf("ddlCountry") != -1 ? i.ComboType = "Country" : n._uniqueId.indexOf("ddlRelation") != -1 ? i.ComboType = "Relation" : n._uniqueId.indexOf("ddlcostCenter") != -1 ? i.ComboType = "CostCenter" : n._uniqueId.indexOf("ddlEmgMobNoPrefix") != -1 ? i.ComboType = "Telco" : n._uniqueId.indexOf("ddlThBussUnit") != -1 ? (i.ComboType = "BussUnit", i.FilterKeys = "") : n._uniqueId.indexOf("ddlThCovType") != -1 ? (i.ComboType = "CovType", i.FilterKeys = "") : n._uniqueId.indexOf("ddlThARMCode") != -1 && (i.ComboType = "ARMCode", i.FilterKeys = "")
}

function fnValidateCovertDt(n, t, i) {
    if (n.get_selectedDate() != null) {
        var r = new Date(n.get_selectedDate().format("MM/dd/yyyy"));
        r.setMonth(r.getMonth() + 12);
        r.setDate(r.getDate() - 1);
        i.ToDt.set_selectedDate(r)
    }
}

function fnRemoveComboItems(n) {
    try {
        n.clearSelection();
        n.set_text("");
        n.clearItems()
    } catch (t) {
        blockUI(!1)
    }
}

function fnClearComboItems(n, t) {
    (n._uniqueId.indexOf("ddlModel") != -1 || n._uniqueId.indexOf("ddlMake") != -1) && n.clearItems();
    var i = n.get_selectedItem(),
        r = n.get_items()._array.length;
    i && r == 1 && (n.trackChanges(), n.get_items().remove(i));
    n.clearSelection();
    n.set_text("");
    t != null && n.requestItems("", !1);
    n.commitChanges()
}

function GetAutoComplst(n, t, i, r, u, f) {
    var e = AbsURL + "Endorsement/Setting/EndtTypeSetting.aspx/GetAutoCompleteDDL";
    n.autocomplete({
        source: function(n, i) {
            var o = JSON.stringify({
                SearchTerm: n.term,
                Ind: t,
                LoginID: r,
                Code: u,
                UWName: f
            });
            CallWebMethod(e, o, function(n) {
                if (JSON.parse(n.d) != undefined) {
                    var t = JSON.parse(n.d);
                    t.Table.length > 0 && i(t.Table)
                }
            })
        },
        minLength: 0,
        focus: function(n, t) {
            n.preventDefault();
            $(this).val(t.item.sdfEndtTypeID)
        },
        select: function(n, t) {
            $(this).val(t.item.value);
            i !== "" && i.val(t.item.value);
            fnEndtTypeAutoSel(t.item.value)
        },
        change: function() {
            return fnChkEndtType(this), !1
        }
    }).bind("focus", function() {
        n.autocomplete("search")
    })
}

//QuoCovNo, EventName, MstData, SchData, ClaimDtls, ExtraCover, PacExtraCover, MstRider, BtnType

function fnLoadPremPage(n, t, i, r, u, f, e, o, s) {
    var h, c;
    try {
        switch (t) {
            case "Initial_Prem":
                h = ServiceURL() + "/GetPremiumDtls";
                c = JSON.stringify({
                    QuoCovNo: n,
                    EventName: t,
                    MstData: i,
                    SchData: r,
                    ClaimDtls: u,
                    ExtraCover: f,
                    PacExtraCover: e,
                    hdfMstQuoDtls: $("[id$=MstQuoDtls]")[0].value
                });
                fnCommonPremiumSyncCall(h, c, t);
                break;
            case "IssueCN":
                h = ServiceURL() + "/btnCoverNote_Onclick";
                c = JSON.stringify({
                    QuoCovNo: n,
                    EventName: t,
                    MstData: i,
                    SchData: r,
                    ClaimDtls: u,
                    ExtraCover: f,
                    PacExtraCover: e,
                    hdfMstQuoDtls: $("[id$=MstQuoDtls]")[0].value,
                    MstRider: o,
                    BtnType: s
                });
                fnCommonAjaxAsyncCall(h, c, t);
                break;
            case "fnSI_Change":
                h = ServiceURL() + "/fnSI_Change";
                c = JSON.stringify({
                    QuoCovNo: n,
                    EventName: t,
                    MstData: i,
                    SchData: r,
                    ClaimDtls: u,
                    ExtraCover: f,
                    PacExtraCover: e,
                    hdfMstQuoDtls: $("[id$=MstQuoDtls]")[0].value,
                    MstRider: o,
                    BtnType: s
                });
                fnCommonPremiumSyncCall(h, c, t);
                break;
            case "PAC_Ind_Change":
                h = ServiceURL() + "/ddlPAC_OnSelectedIndexChanged";
                c = JSON.stringify({
                    QuoCovNo: n,
                    EventName: t,
                    MstData: i,
                    SchData: r,
                    ClaimDtls: u,
                    ExtraCover: f,
                    PacExtraCover: e,
                    hdfMstQuoDtls: $("[id$=MstQuoDtls]")[0].value,
                    MstRider: o,
                    BtnType: s
                });
                fnCommonPremiumSyncCall(h, c, t);
                break;
            case "PAC_Unit_Change":
                h = ServiceURL() + "/ddlPACUnit_SelectedIndexChanged";
                c = JSON.stringify({
                    QuoCovNo: n,
                    EventName: t,
                    MstData: i,
                    SchData: r,
                    ClaimDtls: u,
                    ExtraCover: f,
                    PacExtraCover: e,
                    hdfMstQuoDtls: $("[id$=MstQuoDtls]")[0].value,
                    MstRider: o,
                    BtnType: s
                });
                fnCommonPremiumSyncCall(h, c, t);
                break;
            case "Motor_ExtCover_Item_Change":
                h = ServiceURL() + "/btnGetItem_Click";
                c = JSON.stringify({
                    QuoCovNo: n,
                    EventName: t,
                    MstData: i,
                    SchData: r,
                    ClaimDtls: u,
                    ExtraCover: f,
                    PacExtraCover: e,
                    hdfMstQuoDtls: $("[id$=MstQuoDtls]")[0].value,
                    MstRider: o,
                    BtnType: s
                });
                fnCommonPremiumSyncCall(h, c, t);
                break;
            case "Motor_ExtCover_Control_Change":
                h = ServiceURL() + "/Motor_ExtCover_Control_Change";
                c = JSON.stringify({
                    QuoCovNo: n,
                    EventName: t,
                    MstData: i,
                    SchData: r,
                    ClaimDtls: u,
                    ExtraCover: f,
                    PacExtraCover: e,
                    hdfMstQuoDtls: $("[id$=MstQuoDtls]")[0].value,
                    MstRider: o,
                    BtnType: s
                });
                fnCommonPremiumSyncCall(h, c, t);
                break;
            case "PAC_ExtCover_Item_Change":
                h = ServiceURL() + "/btnGetPacItem_Click";
                c = JSON.stringify({
                    QuoCovNo: n,
                    EventName: t,
                    MstData: i,
                    SchData: r,
                    ClaimDtls: u,
                    ExtraCover: f,
                    PacExtraCover: e,
                    hdfMstQuoDtls: $("[id$=MstQuoDtls]")[0].value,
                    MstRider: o,
                    BtnType: s
                });
                fnCommonPremiumSyncCall(h, c, t);
                break;
            case "PAC_ExtCover_Control_Change":
                h = ServiceURL() + "/ddlUnit_SelectedIndexChanged";
                c = JSON.stringify({
                    QuoCovNo: n,
                    EventName: t,
                    MstData: i,
                    SchData: r,
                    ClaimDtls: u,
                    ExtraCover: f,
                    PacExtraCover: e,
                    hdfMstQuoDtls: $("[id$=MstQuoDtls]")[0].value,
                    MstRider: o,
                    BtnType: s
                });
                fnCommonPremiumSyncCall(h, c, t);
                break;
            case "Pre_SI_Change":
                h = ServiceURL() + "/PrevInsured_TextChanged";
                c = JSON.stringify({
                    QuoCovNo: n,
                    EventName: t,
                    MstData: i,
                    SchData: r,
                    ClaimDtls: u,
                    ExtraCover: f,
                    PacExtraCover: e,
                    hdfMstQuoDtls: $("[id$=MstQuoDtls]")[0].value,
                    MstRider: o,
                    BtnType: s
                });
                fnCommonPremiumSyncCall(h, c, t);
                break;
            case "Vol_Excess_Change":
                h = ServiceURL() + "/ddlVoluntaryExcess_SelectedIndexChanged";
                c = JSON.stringify({
                    QuoCovNo: n,
                    EventName: t,
                    MstData: i,
                    SchData: r,
                    ClaimDtls: u,
                    ExtraCover: f,
                    PacExtraCover: e,
                    hdfMstQuoDtls: $("[id$=MstQuoDtls]")[0].value,
                    MstRider: o,
                    BtnType: s
                });
                fnCommonPremiumSyncCall(h, c, t);
                break;
            case "Chk_SI_Change":
                h = ServiceURL() + "/rbAvSI_CheckedChanged";
                c = JSON.stringify({
                    QuoCovNo: n,
                    EventName: t,
                    MstData: i,
                    SchData: r,
                    ClaimDtls: u,
                    ExtraCover: f,
                    PacExtraCover: e,
                    hdfMstQuoDtls: $("[id$=MstQuoDtls]")[0].value,
                    MstRider: o,
                    BtnType: s
                });
                fnCommonPremiumSyncCall(h, c, t);
                break;
            case "rbRecSI":
                h = ServiceURL() + "/rbRecSI_CheckedChanged";
                c = JSON.stringify({
                    QuoCovNo: n,
                    EventName: t,
                    MstData: i,
                    SchData: r,
                    ClaimDtls: u,
                    ExtraCover: f,
                    PacExtraCover: e,
                    hdfMstQuoDtls: $("[id$=MstQuoDtls]")[0].value,
                    MstRider: o,
                    BtnType: s
                });
                fnCommonPremiumSyncCall(h, c, t);
                break;
            case "Referral":
                h = ServiceURL() + "/Referral";
                c = JSON.stringify({
                    QuoCovNo: n,
                    EventName: t,
                    MstData: i,
                    SchData: r,
                    ClaimDtls: u,
                    ExtraCover: f,
                    PacExtraCover: e,
                    hdfMstQuoDtls: $("[id$=MstQuoDtls]")[0].value,
                    MstRider: o,
                    BtnType: s
                });
                fnCommonPremiumSyncCall(h, c, t);
                break;
            case "btnPrintQuotation":
                h = ServiceURL() + "/btnPrintQuotation";
                c = JSON.stringify({
                    QuoCovNo: n,
                    EventName: t,
                    MstData: i,
                    SchData: r,
                    ClaimDtls: u,
                    ExtraCover: f,
                    PacExtraCover: e,
                    hdfMstQuoDtls: $("[id$=MstQuoDtls]")[0].value,
                    MstRider: o,
                    BtnType: s
                });
                fnCommonPremiumSyncCall(h, c, t);
                break;
            default:
                h = ServiceURL() + "/btnNext_Save_Click";
                c = JSON.stringify({
                    QuoCovNo: n,
                    EventName: t,
                    MstData: i,
                    SchData: r,
                    ClaimDtls: u,
                    ExtraCover: f,
                    PacExtraCover: e,
                    hdfMstQuoDtls: $("[id$=MstQuoDtls]")[0].value,
                    MstRider: o,
                    BtnType: s
                });
                fnCommonAjaxAsyncCall(h, c, t)
        }
    } catch (l) {
        CommonErrorlog("PremCalc_fnCommonAjaxCall", l.message, PricingPageURL);
        blockUI(!1)
    }
}

function fnLoadMstSave(n, t, i, r, u, f, e, o, s) {
    var h = ServiceURL() + "/btnNext_Save_Click",
        c = JSON.stringify({
            QuoCovNo: n,
            EventName: t,
            MstData: i,
            SchData: r,
            ClaimDtls: u,
            ExtraCover: f,
            PacExtraCover: e,
            hdfMstQuoDtls: $("[id$=MstQuoDtls]")[0].value,
            MstRider: o,
            BtnType: s
        });
    fnCommonAjaxAsyncCall(h, c, "MstSave")
}

function blockUI(n) {
    return n ? ($("#DivBlockUI").block({
        message: '<P style="font-size: 120%;font-weight: bold;">Processing..Please wait...<P/>',
        centerY: !1,
        baseZ: 1e3,
        ignoreIfBlocked: !0,
        css: {
            textAlign: "center",
            cursor: "wait",
            padding: "15px",
            opacity: .5,
            border: "3px solid #a00",
            height: 50,
            backgroundColor: "#000",
            "-webkit-border-radius": "10px",
            "-moz-border-radius": "10px",
            color: "#FFFFFF"
        }
    }), !0) : ($("#DivBlockUI").unblock(), $("#Body").context.body.disabled = !1, !0)
}

function fnHideError() {
    $("[id$=lbltopErrorMsg]").text("");
    $("[id$=lblbtmErrorMsg]").text("");
    $("[id$=pnlbtnErrorMsg]").hide();
    $("[id$=pnlbtmErrorMsg]").hide()
}

function fnCommonPremiumSyncCall(n, t, i) {
    fnDisplayMainErrorMsg("", "");
    try {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: t,
            url: n,
            async: !0,
            dataType: "text",
            success: function(n) {
                var u, r, t;
                if (n != "")
                    if (JSON.parse(n).d.toUpperCase().indexOf("ERROR ") < 0 || JSON.parse(n).d.toUpperCase().length > 1e3) switch (i) {
                        case "Initial_Prem":
                            Initial_prem(n);
                            break;
                        case "fnSI_Change":
                            fnSI_Change(n);
                            break;
                        case "Pre_SI_Change":
                            Pre_SI_Change(n);
                            break;
                        case "Chk_SI_Change":
                            Chk_SI_Change(n);
                            break;
                        case "PAC_Ind_Change":
                            PAC_Ind_Change(n);
                            break;
                        case "PAC_Unit_Change":
                            PAC_Unit_Change(n);
                            break;
                        case "Motor_ExtCover_Item_Change":
                            Motor_ExtCover_Item_Change(n);
                            break;
                        case "Motor_ExtCover_Control_Change":
                            Motor_ExtCover_Control_Change(n);
                            break;
                        case "PAC_ExtCover_Item_Change":
                            PAC_ExtCover_Item_Change(n);
                            break;
                        case "PAC_ExtCover_Control_Change":
                            PAC_ExtCover_Control_Change(n);
                            break;
                        case "Referral":
                            u = JSON.parse(n).d;
                            t = AbsURL + "CoverNote/QuotationSummary.aspx?Ind=P%2bjUwjxdIhc%3d";
                            window.location = t;
                            break;
                        case "btnPrintQuotation":
                            r = JSON.parse(n).d;
                            r != "F" && (t = AbsURL + "CoverNote/NewPrintQuotation.ashx?RT=" + r, window.open(t, "", "width=5px,height=5px,scrollbars=yes,menubar=no,status=yes,resizable=no,directories=false,location=false,left=0,top=0", !1));
                            blockUI(!1);
                            break;
                        default:
                            BindPremiumSection(n)
                    } else i == "Initial_Prem" && JSON.parse(n).d.toUpperCase().indexOf("MANDATORY FIELDS MISSING: PREVIOUS YEAR SUM INSURED (RM) CANNOT BE ") > 0 ? (Initial_prem(n), fnDisplayMainErrorMsg("", "Mandatory fields missing: Previous Year Sum Insured (RM) cannot be zero")) : fnDisplayMainErrorMsg("", JSON.parse(n).d.split("Â±")[0]), blockUI(!1)
            },
            failure: function() {
                blockUI(!1)
            },
            error: function() {
                blockUI(!1)
            }
        })
    } catch (r) {
        CommonErrorlog("PremCalc_fnCommonAjaxCall", r.message, PricingPageURL);
        blockUI(!1)
    }
}

function fnAsync(n, t) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "NewCNote.aspx/fnAsynCall",
        async: !0,
        dataType: "text",
        success: function() {
            var i, r;
            if (t != "") switch (n) {
                case "MstSave":
                    i = JSON.parse(JSON.parse(t).d.split("`")[0]);
                    i.VerifyChanges == "T" ? (MstSave(JSON.parse(JSON.parse(t).d.split("`")[2])), r = JSON.parse(t).d.split("`")[1], Initial_prem_VerifyChanges(JSON.stringify(r))) : MstSave(JSON.parse(JSON.parse(t).d.split("`")[1]))
            }
        }
    })
}

function fnCommonAjaxAsyncCall(n, t, i) {
    fnDisplayMainErrorMsg("", "");
    try {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: t,
            url: n,
            async: !0,
            dataType: "text",
            success: function(n) {
                var r, u, f, e, t, o;
                if (n != "")
                    if (JSON.parse(n).d.toUpperCase().indexOf("ERROR ") < 0 || JSON.parse(n).d.toUpperCase().length > 1e3) switch (i) {
                        case "Initial_Prem":
                            r = JSON.parse(JSON.parse(n).d);
                            Initial_prem(r.d);
                            break;
                        case "MstSave":
                            OpenPremiumTab();
                            fnDisplayPremRow();
                            u = JSON.parse(JSON.parse(n).d.split("`")[0]);
                            u.VerifyChanges == "T" ? ImageShow(JSON.parse(JSON.parse(n).d.split("`")[2])) : ImageShow(JSON.parse(JSON.parse(n).d.split("`")[1]));
                            fnAsync("MstSave", n);
                            blockUI(!1);
                            break;
                        case "Preview":
                            JSON.parse(n).d.toUpperCase().indexOf("R: ") < 0 ? fnDisplayValuesPreview(JSON.parse(JSON.parse(n).d)) : (t = JSON.parse(n).d.split("`")[0], f = JSON.parse(JSON.parse(n).d.split("`")[1]), ReferralPopup(t, f), $("[id$=tblReferReason]").css("display", "table-row"), document.getElementById("lblReferReason").innerHTML = t.replace(/R: /g, ""));
                            blockUI(!1);
                            break;
                        case "IssueCN":
                            JSON.parse(n).d.toUpperCase().indexOf("RCP: ") > -1 ? ($("[id$=hdfConfirmInd]")[0].value = "2", t = JSON.parse(n).d.split("`")[1], e = JSON.parse(n).d.split("`")[2], ReferralPopup(t, e), blockUI(!1)) : JSON.parse(n).d.toUpperCase().indexOf("R: ") > -1 ? ($("[id$=hdfConfirmInd]")[0].value = "1", t = JSON.parse(n).d.split("`")[0], ReferralPopup(t), $("[id$=tblReferReason]").css("display", "table-row"), document.getElementById("lblReferReason").innerHTML = t.replace(/R: /g, ""), blockUI(!1)) : JSON.parse(n).d.toUpperCase().indexOf("OLDCN: ") > -1 ? (o = JSON.parse(n).d.split("`")[1], $("[id$=hdfConfirmInd]")[0].value = "6", OldCoverNotePopup(o), blockUI(!1)) : IssueCN(JSON.parse(JSON.parse(n).d))
                    } else JSON.parse(n).d.split("Â±")[0].length > 200 ? fnDisplayMainErrorMsg("", "Unknown error: Please contact support team") : fnDisplayMainErrorMsg("", JSON.parse(n).d.split("Â±")[0]), blockUI(!1)
            },
            failure: function() {
                blockUI(!1)
            },
            error: function() {
                blockUI(!1)
            }
        })
    } catch (r) {
        CommonErrorlog("PremCalc_fnCommonAjaxCall", r.message, PricingTagURL);
        blockUI(!1)
    }
}

function OldCoverNotePopup(n) {
    $("<div  title=Alert id=AgePopup > " + n + " <\/div>").dialog({
        title: "Notification!",
        modal: !0,
        open: function() {},
        width: 500,
        buttons: {
            OK: function() {
                var n = AbsURL + "CoverNote/QuotationSummary.aspx?Ind=P%2bjUwjxdIhc%3d";
                window.location = n;
                $(this).dialog("close")
            }
        }
    })
}

function Initial_prem_VerifyChanges(n) {
    var s = JSON.parse(JSON.parse(n).split("Ã½")[1]),
        t = JSON.parse(JSON.parse(n).split("Ã½")[0]),
        g = t.Allowable_CART_Amount,
        h = t.Allowable_VolExcess,
        u = t.Allowable_Ext_Cvr,
        c = t.Allowable_PA_Ext_Cvr,
        i = t[$("[id$=hdnBizAppName]")[0].value],
        r = t.Error_Display,
        l = t.Allowable_CART_Amount,
        a = t.Allowable_CART_Days,
        v = t.Allowable_PA_Basic_Unit,
        y = t.Allowable_Courtesy_Car,
        p = t.Allowable_Water_Damage,
        w = t.Allowable_Key_Replacement,
        nt = t.Referral_Decline_Summary,
        b = t.PAC_Extra_Cover_Premium,
        f = t.Motor_Extra_Cover_Premium,
        k = t.OutputData,
        e, d, o;
    sessionStorage.setItem("Water_Damage", JSON.stringify(p));
    sessionStorage.setItem("Courtesy_Car", JSON.stringify(y));
    sessionStorage.setItem("sdfExtCover", JSON.stringify(u));
    sessionStorage.setItem("Cart_Amount", JSON.stringify(l));
    sessionStorage.setItem("Cart_Days", JSON.stringify(a));
    sessionStorage.setItem("KeyReplacement", JSON.stringify(w));
    sessionStorage.setItem("PABasic", JSON.stringify(v));
    sessionStorage.setItem("PAC_Extra_Cover_Premium", JSON.stringify(b));
    sessionStorage.setItem("Motor_Extra_Cover_Premium", JSON.stringify(f));
    sessionStorage.setItem("Allowable_PA_Ext_Cvr", JSON.stringify(c));
    sessionStorage.setItem("PacRiderDtlsValue", JSON.stringify(JSON.parse(JSON.parse(n).split("Ã½")[2])));
    e = document.getElementById($("[id$=hdfControlIDs]")[0].id);
    d = document.getElementById(e.attributes.txtPrmInsured.value).value;
    $("[id$=MstQuoDtls]")[0].value = JSON.stringify(s);
    fnBindPremiumDtls(i, h);
    fnCheckRebindExtCover(u, f);
    fnAssignAVSI(i, !1);
    fnBindAV(i);
    o = fnBindPACDtls(k, i, t);
    o == "" && $("#ddlPAC option:selected").val() == "Y" && fnBindPACPremiumDtls(i);
    r != undefined && r.length > 0 && fnDisplayMainErrorMsg("", r[0].Remarks);
    blockUI(!1)
}

function Initial_prem(n) {
    var h = JSON.parse(JSON.parse(n).d.split("Ã½")[1]),
        t = JSON.parse(JSON.parse(n).d.split("Ã½")[0]),
        g = t.Allowable_CART_Amount,
        c = t.Allowable_VolExcess,
        u = t.Allowable_Ext_Cvr,
        l = t.Allowable_PA_Ext_Cvr,
        i = t[$("[id$=hdnBizAppName]")[0].value],
        r = t.Error_Display,
        a = t.Allowable_CART_Amount,
        v = t.Allowable_CART_Days,
        y = t.Allowable_PA_Basic_Unit,
        p = t.Allowable_Courtesy_Car,
        w = t.Allowable_Water_Damage,
        b = t.Allowable_Key_Replacement,
        nt = t.Referral_Decline_Summary,
        k = t.PAC_Extra_Cover_Premium,
        f = t.Motor_Extra_Cover_Premium,
        d = t.OutputData,
        e, o, s;
    sessionStorage.setItem("Water_Damage", JSON.stringify(w));
    sessionStorage.setItem("Courtesy_Car", JSON.stringify(p));
    sessionStorage.setItem("sdfExtCover", JSON.stringify(u));
    sessionStorage.setItem("Cart_Amount", JSON.stringify(a));
    sessionStorage.setItem("Cart_Days", JSON.stringify(v));
    sessionStorage.setItem("KeyReplacement", JSON.stringify(b));
    sessionStorage.setItem("PABasic", JSON.stringify(y));
    sessionStorage.setItem("PAC_Extra_Cover_Premium", JSON.stringify(k));
    sessionStorage.setItem("Motor_Extra_Cover_Premium", JSON.stringify(f));
    sessionStorage.setItem("Allowable_PA_Ext_Cvr", JSON.stringify(l));
    sessionStorage.setItem("PacRiderDtlsValue", JSON.stringify(JSON.parse(JSON.parse(n).d.split("Ã½")[2])));
    e = document.getElementById($("[id$=hdfControlIDs]")[0].id);
    o = document.getElementById(e.attributes.txtPrmInsured.value).value;
    $("[id$=MstQuoDtls]")[0].value = JSON.stringify(h);
    fnCheckRebindExtCover(u, f);
    fnAssignAVSI(i, !1, o);
    fnBindAV(i);
    fnBindPremiumDtls(i);
    fnBindVolExcess(c);
    s = fnBindPACDtls(d, i, t);
    s == "" && $("#ddlPAC option:selected").val() == "Y" && fnBindPACPremiumDtls(i);
    r != undefined && r.length > 0 && fnDisplayMainErrorMsg("", r[0].Remarks);
    blockUI(!1)
}

function fnSI_Change(n) {
    var o = JSON.parse(JSON.parse(n).d.split("Ã½")[1]),
        t = JSON.parse(JSON.parse(n).d.split("Ã½")[0]),
        s = t.Allowable_VolExcess,
        h = t.Allowable_PA_Ext_Cvr,
        i = t[$("[id$=hdnBizAppName]")[0].value],
        c = t.PAC_Extra_Cover_Premium,
        l = t.OutputData,
        u = t.Allowable_Ext_Cvr,
        f = t.Motor_Extra_Cover_Premium,
        a, e, r;
    sessionStorage.setItem("PAC_Extra_Cover_Premium", JSON.stringify(c));
    sessionStorage.setItem("Allowable_PA_Ext_Cvr", JSON.stringify(h));
    sessionStorage.setItem("PacRiderDtlsValue", JSON.stringify(JSON.parse(JSON.parse(n).d.split("Ã½")[2])));
    sessionStorage.setItem("Motor_Extra_Cover_Premium", JSON.stringify(f));
    sessionStorage.setItem("sdfExtCover", JSON.stringify(u));
    a = document.getElementById($("[id$=hdfControlIDs]")[0].id);
    $("[id$=MstQuoDtls]")[0].value = JSON.stringify(o);
    fnCheckRebindExtCover(u, f);
    fnBindAV(i);
    fnBindVolExcess(s);
    fnBindPremiumDtls(i);
    e = fnBindPACDtls(l, i, t);
    e == "" && $("#ddlPAC option:selected").val() == "Y" && fnBindPACPremiumDtls(i);
    r = t.Error_Display;
    r != undefined && r.length > 0 && fnDisplayMainErrorMsg("", r[0].Remarks);
    blockUI(!1)
}

function Pre_SI_Change(n) {
    var s = JSON.parse(JSON.parse(n).d.split("Ã½")[1]),
        t = JSON.parse(JSON.parse(n).d.split("Ã½")[0]),
        g = t.Allowable_CART_Amount,
        h = t.Allowable_VolExcess,
        u = t.Allowable_Ext_Cvr,
        c = t.Allowable_PA_Ext_Cvr,
        i = t[$("[id$=hdnBizAppName]")[0].value],
        r = t.Error_Display,
        l = t.Allowable_CART_Amount,
        a = t.Allowable_CART_Days,
        v = t.Allowable_PA_Basic_Unit,
        y = t.Allowable_Courtesy_Car,
        p = t.Allowable_Water_Damage,
        w = t.Allowable_Key_Replacement,
        nt = t.Referral_Decline_Summary,
        b = t.PAC_Extra_Cover_Premium,
        f = t.Motor_Extra_Cover_Premium,
        k = t.OutputData,
        e, d, o;
    sessionStorage.setItem("Water_Damage", JSON.stringify(p));
    sessionStorage.setItem("Courtesy_Car", JSON.stringify(y));
    sessionStorage.setItem("sdfExtCover", JSON.stringify(u));
    sessionStorage.setItem("Cart_Amount", JSON.stringify(l));
    sessionStorage.setItem("Cart_Days", JSON.stringify(a));
    sessionStorage.setItem("KeyReplacement", JSON.stringify(w));
    sessionStorage.setItem("PABasic", JSON.stringify(v));
    sessionStorage.setItem("PAC_Extra_Cover_Premium", JSON.stringify(b));
    sessionStorage.setItem("Motor_Extra_Cover_Premium", JSON.stringify(f));
    sessionStorage.setItem("Allowable_PA_Ext_Cvr", JSON.stringify(c));
    sessionStorage.setItem("PacRiderDtlsValue", JSON.stringify(JSON.parse(JSON.parse(n).d.split("Ã½")[2])));
    e = document.getElementById($("[id$=hdfControlIDs]")[0].id);
    d = document.getElementById(e.attributes.txtPrmInsured.value).value;
    $("[id$=MstQuoDtls]")[0].value = JSON.stringify(s);
    fnCheckRebindExtCover(u, f);
    fnAssignAVSI(i, !1);
    fnBindAV(i);
    fnBindPremiumDtls(i);
    fnBindVolExcess(h);
    o = fnBindPACDtls(k, i, t);
    o == "" && $("#ddlPAC option:selected").val() == "Y" && fnBindPACPremiumDtls(i);
    r != undefined && r.length > 0 && fnDisplayMainErrorMsg("", r[0].Remarks);
    blockUI(!1)
}

function Chk_SI_Change(n) {
    var s = JSON.parse(JSON.parse(n).d.split("Ã½")[1]),
        t = JSON.parse(JSON.parse(n).d.split("Ã½")[0]),
        g = t.Allowable_CART_Amount,
        h = t.Allowable_VolExcess,
        u = t.Allowable_Ext_Cvr,
        c = t.Allowable_PA_Ext_Cvr,
        i = t[$("[id$=hdnBizAppName]")[0].value],
        r = t.Error_Display,
        l = t.Allowable_CART_Amount,
        a = t.Allowable_CART_Days,
        v = t.Allowable_PA_Basic_Unit,
        y = t.Allowable_Courtesy_Car,
        p = t.Allowable_Water_Damage,
        w = t.Allowable_Key_Replacement,
        nt = t.Referral_Decline_Summary,
        b = t.PAC_Extra_Cover_Premium,
        f = t.Motor_Extra_Cover_Premium,
        k = t.OutputData,
        e, d, o;
    sessionStorage.setItem("Water_Damage", JSON.stringify(p));
    sessionStorage.setItem("Courtesy_Car", JSON.stringify(y));
    sessionStorage.setItem("sdfExtCover", JSON.stringify(u));
    sessionStorage.setItem("Cart_Amount", JSON.stringify(l));
    sessionStorage.setItem("Cart_Days", JSON.stringify(a));
    sessionStorage.setItem("KeyReplacement", JSON.stringify(w));
    sessionStorage.setItem("PABasic", JSON.stringify(v));
    sessionStorage.setItem("PAC_Extra_Cover_Premium", JSON.stringify(b));
    sessionStorage.setItem("Motor_Extra_Cover_Premium", JSON.stringify(f));
    sessionStorage.setItem("Allowable_PA_Ext_Cvr", JSON.stringify(c));
    sessionStorage.setItem("PacRiderDtlsValue", JSON.stringify(JSON.parse(JSON.parse(n).d.split("Ã½")[2])));
    e = document.getElementById($("[id$=hdfControlIDs]")[0].id);
    d = document.getElementById(e.attributes.txtPrmInsured.value).value;
    $("[id$=MstQuoDtls]")[0].value = JSON.stringify(s);
    fnBindVolExcess(h);
    fnAssignAVSI(i, !1);
    fnBindAV(i);
    fnBindPremiumDtls(i);
    o = fnBindPACDtls(k, i, t);
    o == "" && $("#ddlPAC option:selected").val() == "Y" && fnBindPACPremiumDtls(i);
    fnCheckRebindExtCover(u, f);
    r != undefined && r.length > 0 && fnDisplayMainErrorMsg("", r[0].Remarks);
    blockUI(!1)
}

function PAC_Ind_Change(n) {
    var u = $("#ddlPAC option:selected").val(),
        h, c, r;
    if (u != "Y") fnClearPACDtls();
    else {
        var f = JSON.parse(JSON.parse(n).d.split("Ã½")[1]),
            t = JSON.parse(JSON.parse(n).d.split("Ã½")[0]),
            i = t[$("[id$=hdnBizAppName]")[0].value],
            e = t.Allowable_PA_Ext_Cvr,
            o = t.PAC_Extra_Cover_Premium,
            s = t.OutputData;
        sessionStorage.setItem("PAC_Extra_Cover_Premium", JSON.stringify(o));
        sessionStorage.setItem("Allowable_PA_Ext_Cvr", JSON.stringify(e));
        sessionStorage.setItem("PacRiderDtlsValue", JSON.stringify(JSON.parse(JSON.parse(n).d.split("Ã½")[2])));
        h = document.getElementById($("[id$=hdfControlIDs]")[0].id);
        $("[id$=MstQuoDtls]")[0].value = JSON.stringify(f);
        c = fnBindPACDtls(s, i, t);
        fnBindPACPremiumDtls(i);
        fnBindPremiumDtls(i);
        fnBindAV(i);
        r = t.Error_Display;
        r != undefined && r.length > 0 && fnDisplayMainErrorMsg("", r[0].Remarks)
    }
    blockUI(!1)
}

function PAC_Unit_Change(n) {
    var u = JSON.parse(JSON.parse(n).d.split("Ã½")[1]),
        r = JSON.parse(JSON.parse(n).d.split("Ã½")[0]),
        i = r[$("[id$=hdnBizAppName]")[0].value],
        f = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        t;
    $("[id$=MstQuoDtls]")[0].value = JSON.stringify(u);
    fnBindPACPremiumDtls(i);
    fnBindPremiumDtls(i);
    fnBindAV(i);
    fnDisplayPACInsuredDtls();
    t = r.Error_Display;
    t != undefined && t.length > 0 && fnDisplayMainErrorMsg("", t[0].Remarks);
    blockUI(!1)
}

function Motor_ExtCover_Item_Change(n) {
    var o = JSON.parse(JSON.parse(n).d.split("Ã½")[1]),
        t = JSON.parse(JSON.parse(n).d.split("Ã½")[0]),
        b = t.Allowable_CART_Amount,
        k = t.Allowable_VolExcess,
        r = t.Allowable_Ext_Cvr,
        s = t.Allowable_PA_Ext_Cvr,
        u = t[$("[id$=hdnBizAppName]")[0].value],
        i = t.Error_Display,
        h = t.Allowable_CART_Amount,
        c = t.Allowable_CART_Days,
        l = t.Allowable_PA_Basic_Unit,
        a = t.Allowable_Courtesy_Car,
        v = t.Allowable_Water_Damage,
        y = t.Allowable_Key_Replacement,
        d = t.Referral_Decline_Summary,
        p = t.PAC_Extra_Cover_Premium,
        f = t.Motor_Extra_Cover_Premium,
        g = t.OutputData,
        e, w;
    sessionStorage.setItem("Water_Damage", JSON.stringify(v));
    sessionStorage.setItem("Courtesy_Car", JSON.stringify(a));
    sessionStorage.setItem("sdfExtCover", JSON.stringify(r));
    sessionStorage.setItem("Cart_Amount", JSON.stringify(h));
    sessionStorage.setItem("Cart_Days", JSON.stringify(c));
    sessionStorage.setItem("KeyReplacement", JSON.stringify(y));
    sessionStorage.setItem("PABasic", JSON.stringify(l));
    sessionStorage.setItem("PAC_Extra_Cover_Premium", JSON.stringify(p));
    sessionStorage.setItem("Motor_Extra_Cover_Premium", JSON.stringify(f));
    sessionStorage.setItem("Allowable_PA_Ext_Cvr", JSON.stringify(s));
    sessionStorage.setItem("PacRiderDtlsValue", JSON.stringify(JSON.parse(JSON.parse(n).d.split("Ã½")[2])));
    e = document.getElementById($("[id$=hdfControlIDs]")[0].id);
    w = document.getElementById(e.attributes.txtPrmInsured.value).value;
    $("[id$=MstQuoDtls]")[0].value = JSON.stringify(o);
    fnBindPACPremiumDtls(u);
    fnBindPremiumDtls(u);
    fnCheckRebindExtCover(r, f);
    i != undefined && i.length > 0 && fnDisplayMainErrorMsg("", i[0].Remarks);
    blockUI(!1)
}

function Motor_ExtCover_Control_Change(n) {
    var o = JSON.parse(JSON.parse(n).d.split("Ã½")[1]),
        t = JSON.parse(JSON.parse(n).d.split("Ã½")[0]),
        b = t.Allowable_CART_Amount,
        k = t.Allowable_VolExcess,
        r = t.Allowable_Ext_Cvr,
        s = t.Allowable_PA_Ext_Cvr,
        u = t[$("[id$=hdnBizAppName]")[0].value],
        i = t.Error_Display,
        h = t.Allowable_CART_Amount,
        c = t.Allowable_CART_Days,
        l = t.Allowable_PA_Basic_Unit,
        a = t.Allowable_Courtesy_Car,
        v = t.Allowable_Water_Damage,
        y = t.Allowable_Key_Replacement,
        d = t.Referral_Decline_Summary,
        p = t.PAC_Extra_Cover_Premium,
        f = t.Motor_Extra_Cover_Premium,
        g = t.OutputData,
        e, w;
    sessionStorage.setItem("Water_Damage", JSON.stringify(v));
    sessionStorage.setItem("Courtesy_Car", JSON.stringify(a));
    sessionStorage.setItem("sdfExtCover", JSON.stringify(r));
    sessionStorage.setItem("Cart_Amount", JSON.stringify(h));
    sessionStorage.setItem("Cart_Days", JSON.stringify(c));
    sessionStorage.setItem("KeyReplacement", JSON.stringify(y));
    sessionStorage.setItem("PABasic", JSON.stringify(l));
    sessionStorage.setItem("PAC_Extra_Cover_Premium", JSON.stringify(p));
    sessionStorage.setItem("Motor_Extra_Cover_Premium", JSON.stringify(f));
    sessionStorage.setItem("Allowable_PA_Ext_Cvr", JSON.stringify(s));
    sessionStorage.setItem("PacRiderDtlsValue", JSON.stringify(JSON.parse(JSON.parse(n).d.split("Ã½")[2])));
    e = document.getElementById($("[id$=hdfControlIDs]")[0].id);
    w = document.getElementById(e.attributes.txtPrmInsured.value).value;
    $("[id$=MstQuoDtls]")[0].value = JSON.stringify(o);
    fnCheckRebindExtCover(r, f);
    fnBindPACPremiumDtls(u);
    fnBindPremiumDtls(u);
    i != undefined && i.length > 0 && fnDisplayMainErrorMsg("", i[0].Remarks);
    blockUI(!1)
}

function PAC_ExtCover_Item_Change(n) {
    var f = JSON.parse(JSON.parse(n).d.split("Ã½")[1]),
        t = JSON.parse(JSON.parse(n).d.split("Ã½")[0]),
        b = t.Allowable_CART_Amount,
        k = t.Allowable_VolExcess,
        e = t.Allowable_Ext_Cvr,
        o = t.Allowable_PA_Ext_Cvr,
        i = t[$("[id$=hdnBizAppName]")[0].value],
        r = t.Error_Display,
        s = t.Allowable_CART_Amount,
        h = t.Allowable_CART_Days,
        c = t.Allowable_PA_Basic_Unit,
        l = t.Allowable_Courtesy_Car,
        a = t.Allowable_Water_Damage,
        v = t.Allowable_Key_Replacement,
        d = t.Referral_Decline_Summary,
        y = t.PAC_Extra_Cover_Premium,
        p = t.Motor_Extra_Cover_Premium,
        g = t.OutputData,
        u, w;
    sessionStorage.setItem("Water_Damage", JSON.stringify(a));
    sessionStorage.setItem("Courtesy_Car", JSON.stringify(l));
    sessionStorage.setItem("sdfExtCover", JSON.stringify(e));
    sessionStorage.setItem("Cart_Amount", JSON.stringify(s));
    sessionStorage.setItem("Cart_Days", JSON.stringify(h));
    sessionStorage.setItem("KeyReplacement", JSON.stringify(v));
    sessionStorage.setItem("PABasic", JSON.stringify(c));
    sessionStorage.setItem("PAC_Extra_Cover_Premium", JSON.stringify(y));
    sessionStorage.setItem("Motor_Extra_Cover_Premium", JSON.stringify(p));
    sessionStorage.setItem("Allowable_PA_Ext_Cvr", JSON.stringify(o));
    sessionStorage.setItem("PacRiderDtlsValue", JSON.stringify(JSON.parse(JSON.parse(n).d.split("Ã½")[2])));
    u = document.getElementById($("[id$=hdfControlIDs]")[0].id);
    w = document.getElementById(u.attributes.txtPrmInsured.value).value;
    $("[id$=MstQuoDtls]")[0].value = JSON.stringify(f);
    fnDisplayPACInsuredDtls();
    fnBindPACPremiumDtls(i);
    fnBindPremiumDtls(i);
    fnBindAV(i);
    r != undefined && r.length > 0 && fnDisplayMainErrorMsg("", r[0].Remarks);
    blockUI(!1)
}

function PAC_ExtCover_Control_Change(n) {
    var f = JSON.parse(JSON.parse(n).d.split("Ã½")[1]),
        t = JSON.parse(JSON.parse(n).d.split("Ã½")[0]),
        b = t.Allowable_CART_Amount,
        k = t.Allowable_VolExcess,
        e = t.Allowable_Ext_Cvr,
        o = t.Allowable_PA_Ext_Cvr,
        r = t[$("[id$=hdnBizAppName]")[0].value],
        i = t.Error_Display,
        s = t.Allowable_CART_Amount,
        h = t.Allowable_CART_Days,
        c = t.Allowable_PA_Basic_Unit,
        l = t.Allowable_Courtesy_Car,
        a = t.Allowable_Water_Damage,
        v = t.Allowable_Key_Replacement,
        d = t.Referral_Decline_Summary,
        y = t.PAC_Extra_Cover_Premium,
        p = t.Motor_Extra_Cover_Premium,
        g = t.OutputData,
        u, w;
    sessionStorage.setItem("Water_Damage", JSON.stringify(a));
    sessionStorage.setItem("Courtesy_Car", JSON.stringify(l));
    sessionStorage.setItem("sdfExtCover", JSON.stringify(e));
    sessionStorage.setItem("Cart_Amount", JSON.stringify(s));
    sessionStorage.setItem("Cart_Days", JSON.stringify(h));
    sessionStorage.setItem("KeyReplacement", JSON.stringify(v));
    sessionStorage.setItem("PABasic", JSON.stringify(c));
    sessionStorage.setItem("PAC_Extra_Cover_Premium", JSON.stringify(y));
    sessionStorage.setItem("Motor_Extra_Cover_Premium", JSON.stringify(p));
    sessionStorage.setItem("Allowable_PA_Ext_Cvr", JSON.stringify(o));
    sessionStorage.setItem("PacRiderDtlsValue", JSON.stringify(JSON.parse(JSON.parse(n).d.split("Ã½")[2])));
    u = document.getElementById($("[id$=hdfControlIDs]")[0].id);
    w = document.getElementById(u.attributes.txtPrmInsured.value).value;
    $("[id$=MstQuoDtls]")[0].value = JSON.stringify(f);
    fnDisplayPACInsuredDtls();
    fnBindPACPremiumDtls(r);
    fnBindAV(r);
    i != undefined && i.length > 0 && fnDisplayMainErrorMsg("", i[0].Remarks);
    blockUI(!1)
}

function BindPremiumSection(n) {
    var o = JSON.parse(JSON.parse(n).d.split("Ã½")[1]),
        t = JSON.parse(JSON.parse(n).d.split("Ã½")[0]),
        i = t[$("[id$=hdnBizAppName]")[0].value],
        e, r;
    $("[id$=MstQuoDtls]")[0].value = JSON.stringify(o);
    var u = t.Allowable_Ext_Cvr,
        f = t.Motor_Extra_Cover_Premium,
        s = t.Allowable_CART_Amount,
        h = t.Allowable_CART_Days,
        c = t.Allowable_PA_Basic_Unit,
        l = t.Allowable_Courtesy_Car,
        a = t.Allowable_Water_Damage,
        v = t.Allowable_Key_Replacement;
    sessionStorage.setItem("Water_Damage", JSON.stringify(a));
    sessionStorage.setItem("Courtesy_Car", JSON.stringify(l));
    sessionStorage.setItem("sdfExtCover", JSON.stringify(u));
    sessionStorage.setItem("Cart_Amount", JSON.stringify(s));
    sessionStorage.setItem("Cart_Days", JSON.stringify(h));
    sessionStorage.setItem("KeyReplacement", JSON.stringify(v));
    sessionStorage.setItem("PABasic", JSON.stringify(c));
    sessionStorage.setItem("Motor_Extra_Cover_Premium", JSON.stringify(f));
    sessionStorage.setItem("sdfExtCover", JSON.stringify(u));
    e = t.OutputData;
    fnBindPACDtls(e, i, t);
    fnCheckRebindExtCover(u, f);
    fnBindPremiumDtls(i);
    fnBindPACPremiumDtls(i);
    fnBindAV(i);
    r = t.Error_Display;
    r != undefined && r.length > 0 && fnDisplayMainErrorMsg("", r[0].Remarks);
    blockUI(!1)
}

function ImageShow(n) {
    var i = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        r = document.getElementById(i.attributes.txtPrmInsured.value),
        t = n;
    t.BtnType != "" && t.BtnType != null && t.QuoNo != "" && t.QuoNo != null && (t.ISMMV == !0 && ($("[id$=lblABIErrorMsg]").text("Market value is loading from ISM ABI database.."), r.value = "0", $("[id$=imgMV]").show(), $("#hdfISMMV")[0].value = "1", $("#dblABISumIns")[0].value = "0"), t.ISMNCD == !0 && ($("[id$=trNCD]").css("display", "table-row"), $("[id$=lblNCDErrorMsg]").text("NCD value is loading from ISM NCD database.."), $("#hdfISMNCD")[0].value = "1", $("[id$=txtCN_NCDPer]")[0].innerHTML = "0.00", $("[id$=imgLoading]").show(), $("[id$=hdfNCDValidInd]")[0].value = "0", $("[id$=hdfNCDRefNo]")[0].value = t.hdfNCDRefNo, $("#lblNCDErrorMsg").css("color", '#0000FF"')))
}

function MstSave(n) {
    var f, e;
    try {
        var t = n,
            i = document.getElementById($("[id$=hdfControlIDs]")[0].id),
            r = document.getElementById(i.attributes.txtPrevSI.value),
            u = document.getElementById(i.attributes.txtPrmInsured.value);
        t.QuoNo != "" && ($("[id$=lblQuoCovNo]")[0].innerText = t.QuoNo, $("[id$=hdfQuoNo]")[0].value = t.QuoNo, f = $("[id$=trQuoCovNo]"), f[0].style.display = "table-row");
        t.fnISMNCDEnquiryInd == "Y" && (r.value = "0.00", $("#lblRecSI").text("0.00"), $("#lblAvSI").text("0.00"), r.disabled = !0, u.disabled = !0, $("*[name$='rbAvSI']").attr("disabled", "disabled"), $("*[name$='rbRecSI']").attr("disabled", "disabled"), $("#txtTuitionLoadPer").text("0.00"), $("#txtDsTuitionLoadPer").text("0.00"), $("#txtDsTuitionLoadAmt").text("0.00"), $("#txtDsBasicPremium").text("0.00"), $("#txtDsTrlLoadingPer").text("0.00"), $("#txtDSLoadingAmt").text("0.00"), $("#txtTrlBasicNet").text("0.00"), $("#txtBasicNet").text("0.00"), $("#txtAllRider").text("0.00"), $("#txtDsTrailerPremium").text("0.00"), $("#txtDsTrlLoadingAmt").text("0.00"), $("#txtTuitionLoadAmt").text("0.00"), $("#txtttlTrailerPrmAmt").text("0.00"), $("#txtExcessAmt").text("0.00"), $("#txtPrmStampDutyAmt").text("0.00"), $("#hdfActPrem")[0].value = "0.00", $("#hdfNCDAmt")[0].value = "0.00", $("#hdfQuoStage")[0].value ? $("#txtCN_DsNCDAmt").text("0.00") : $("#txtDsNCDAmt").text("0.00"), $("#txtttlPMNet").text("0.00"), $("#lblPrmExtraCovAmt").text("0.00"), $("#txtTtlCoveragePremium").text("0.00"), $("#txtTtlPayablePremium").text("0.00"), $("#lblTtlMotorPremium").text("0.00"), $("#lblRenewalBonus").text("0.00"), $("#lblVoluntaryExcessPrem").text("0.00"), e = $("#ddlPAC"), $("#ddlPAC")[0].value = "No", $("#ddlPACUnit").empty(), $("#lblPACDsc").text("0.00"), $("#lblPACDesc").text("0.00"), $("#txtPersonalSumInsured").text("0.00"), $("#txtDsPersonalPremium").text("0.00"), $("#txtDsPersonalAddPremium").text("0.00"), $("#txtDsPersonalStampDuty").text("0.00"), $("#txtDsPersonalServiceTax").text("0.00"), $("#txtPACRebateAmt").text("0.00"), $("#txtPACRebateAmt").text("0.00"), $("#txtPACRebatePer").text("0.00"), $("#txtDsPACExtraCover").text("0.00"), $("#lblPACRiderExtCovError").text("0.00"), document.getElementById("trPACRiderDtls").style.display = "none", document.getElementById("trPacRiderExtCoverDtls").style.display = "none", $("#txtTtlPayablePremium")[0].innerHTML = $("#lblTtlMotorPremium")[0].innerHTML);
        t.BtnType != "" && t.BtnType != null && t.QuoNo != "" && t.QuoNo != null && ($("#hdfTabPrevValue")[0].value = "1", $("#hdfExlSpecialInd")[0].value = t.ExlSpecialInd, $("#hdfISMNCD")[0].value = t.hdfISMNCD, $("#hdfEXLTempPassin")[0].value = t.hdfEXLTempPassin, $("#hdfWarn")[0].value = t.hdfWarn, $("#hdfTempISM")[0].value = t.hdfTempISM, $("#hdfNCDValidInd")[0].value = t.NCDValueInd, $("#hdfAllowInd")[0].value = t.hdfAllowInd, $("#hdfTempMV")[0].value = t.hdfTempMV, $("#hdfClaimDtls")[0].value = t.hdfClaimDtls, $("#hdfNCDReply")[0].value = t.hdfNCDReply, $("#hdfNCDValue")[0].value = t.hdfNCDValue, $("#TempPassInValues")[0].value = t.TempPassInValues, $("#hdfSumInsChangInd")[0].innerHTML = t.hdfSumInsChangInd, $("#MstQuoDtls")[0].value = t.hdfMstQuoDtls, $("#lblNCDErrorMsg").text(t.lblNCDErrorMsg), $("#lblNCDErrorMsg").text(t.lblCN_NCDErrorMsg), t.ISMMV == !0 && ($("[id$=lblABIErrorMsg]").text("Market value is loading from ISM ABI database.."), u.value = "0", $("[id$=imgMV]").show(), $("#hdfISMMV")[0].value = "1", $("#dblABISumIns")[0].value = "0", fnBindSumIns(), (t.ISMNCD == !1 || t.ISMNCD == undefined) && Concatenate("NCD")), t.ISMNCD == !0 && ($("[id$=trNCD]").css("display", "table-row"), $("[id$=lblNCDErrorMsg]").text("NCD value is loading from ISM NCD database.."), $("#hdfISMNCD")[0].value = "1", $("[id$=txtCN_NCDPer]")[0].innerHTML = "0.00", $("[id$=imgLoading]").show(), $("[id$=hdfNCDValidInd]")[0].value = "0", $("[id$=hdfNCDRefNo]")[0].value = t.hdfNCDRefNo, fnCallNCDReply(), $("#lblNCDErrorMsg").css("color", '#0000FF"')));
        blockUI(!1)
    } catch (o) {
        CommonErrorlog("NewCNote_fnCommonAjaxCall", o.message, PricingPageURL);
        blockUI(!1)
    }
}

function OpenPremiumTab() {
    var n = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        t = $find(n.attributes.rtsMQ.value),
        r = $find(n.attributes.rmpMQ_InusredDetails.value),
        i = r.findPageViewByID("rpvPremium");
    t._selectedIndex = 1;
    t.commitChanges();
    i.set_selected(!0);
    i.select();
    $("#rtsMQ div ul li.rtsLI a").addClass("rtsSelected");
    $("#rtsMQ div ul li.rtsLI.rtsFirst a").removeClass("rtsSelected");
    $("#rtsMQ div ul li.rtsLI.rtsLast a").removeClass("rtsSelected");
    $("#rtsMQ div ul li.rtsLI a").removeClass("rtsAfter");
    $("[id$=trCoverageview]").css("display", "none");
    $("[id$=trPremPreview]").css("display", "table-row");
    $("[id$=btnSubmit]").show();
    $("[id$=hdfQuoStage]")[0].value == "0" && $("[id$=btnPrintQuotation]").hide()
}

function formatNumber(n) {
    return n.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

function CurrencyFormat(n) {
    n.value = formatNumber(n.value.replace(",", ""));
    Concatenate(n)
}

function fnNumberonly(n) {
    if (window.event) {
        if (n = window.event, (n.keyCode < 48 || n.keyCode > 57) & n.keyCode != 8) return event.returnValue = !1, !1
    } else if ((n.which < 48 || n.which > 57) & n.which != 8) return n.preventDefault(), !1
}

function fnchangeSum() {}

function btnClearDoc_Click() {
    var n = AbsURL + "CoverNote/QuotationSummary.aspx?Ind=P%2bjUwjxdIhc%3d";
    window.location = n
}

function fnupdPremDtls(n) {
    n = $(n);
    var t = n.find("QuoDtls");
    $("#lblDispVehNo")[0].innerHTML = $(t).find("VehNo").text();
    $.each(t, function() {
        $("#txtPrmInsured")[0].value = addCommas($(this).find("SumIns").text());
        $("#lblNCDPerCurPer")[0].innerHTML = $(this).find("NCDPct").text();
        $("#dtFromPOC")[0].innerHTML = $(this).find("EffDate").text();
        $("#dtToPOC")[0].innerHTML = $(this).find("ExpDate").text()
    });
    fnBindHeaderDtls(t)
}

function ProceedToCoverNote() {
    try {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "NewCNote.aspx/fnRedirect",
            data: "",
            dataType: "json",
            success: function(n) {
                var t = n.d;
                window.location = t
            },
            error: function(n) {
                DisplayError(n)
            }
        })
    } catch (n) {
        DisplayError(n)
    }
}

function fnCallPACExtPremium(n) {
    var r, o, i, u;
    try {
        var f = n.get_items(),
            t = "",
            e = f._parent._checkedIndices,
            p = e.length;
        for (r = 0; r < p; r++) o = f.getItem(e[r]), t += o._properties._data.value.trim() + "Â±";
        if (t = t.substr(0, t.length > 0 ? t.length - 1 : 0), $("[id$=hdfPACExtCover]").val() != t) {
            $("[id$=hdfPACExtCover]").val(t);
            var s = sessionStorage.getItem("Allowable_PA_Ext_Cvr") != null ? JSON.parse(sessionStorage.getItem("Allowable_PA_Ext_Cvr")) : "",
                w = sessionStorage.getItem("PAC_Extra_Cover_Premium") != null ? JSON.parse(sessionStorage.getItem("PAC_Extra_Cover_Premium")) : "",
                h = "";
            for (i = 0; i < t.split("Â±").length; i++)
                for (u = 0; u < s.length; u++) {
                    var c = s[u].PAC_Ext_Cvr_Code,
                        l = "0.00",
                        a = "1",
                        v = "",
                        y = "";
                    t.split("Â±")[i].trim() == c && ($.each(w, function() {
                        c == $(this)[0].PAC_Ext_Cvr_Code && (a = $(this)[0].PAC_Ext_Cvr_Unit != "0" ? $(this)[0].PAC_Ext_Cvr_Unit : "1", l = $(this)[0].PAC_Ext_Cvr_SI, v = $(this)[0].PAC_Ext_Cvr_Premium, y = $(this)[0].PAC_Ext_Cvr_Rate)
                    }), h += t.split("Â±")[i] + "Ã¾" + a + "Ã¾" + l + "Ã¾" + v + "Ã¾" + y + "Â±")
                }
            $("#hdfPACExtCover_dt").val(h);
            Concatenate("ddlPACExtraCover")
        }
    } catch (b) {
        CommonErrorlog("PremCalc_fnCallPACExtPremium", b.message, PricingPageURL);
        blockUI(!1)
    }
}

function fnCallExtPremium(n, t, i) {
    var h, c, f, e, a;
    try {
        var r = "",
            g = n.get_items(),
            nt = g._parent._checkedIndices,
            et = nt.length;
        for (h = 0; h < et; h++) c = g.getItem(nt[h]), c != undefined && (r += c._properties._data.value.trim() + "Â±", c == "03" && $("[id$=txtAllRiderInd]").text("Yes"));
        if (i != undefined && (r += i), r = r.substr(0, r.length > 0 ? r.length - 1 : 0), $("[id$=hdfExtCover]").val() != r) {
            var l = sessionStorage.getItem("sdfExtCover") != null ? JSON.parse(sessionStorage.getItem("sdfExtCover")) : "",
                tt = sessionStorage.getItem("Cart_Days") != null ? JSON.parse(sessionStorage.getItem("Cart_Days")) : "",
                it = sessionStorage.getItem("Cart_Amount") != null ? JSON.parse(sessionStorage.getItem("Cart_Amount")) : "",
                ot = sessionStorage.getItem("KeyReplacement") != null ? JSON.parse(sessionStorage.getItem("KeyReplacement")) : "",
                st = sessionStorage.getItem("Water_Damage") != null ? JSON.parse(sessionStorage.getItem("Water_Damage")) : "",
                ht = sessionStorage.getItem("Courtesy_Car") != null ? JSON.parse(sessionStorage.getItem("Courtesy_Car")) : "",
                rt = sessionStorage.getItem("PABasic") != null ? JSON.parse(sessionStorage.getItem("PABasic")) : "",
                ct = JSON.parse(sessionStorage.getItem("Motor_Extra_Cover_Premium")),
                ut = document.getElementById($("[id$=hdfControlIDs]")[0].id),
                lt = $find(ut.attributes.rcFromPeriodofCover.value),
                at = $find(ut.attributes.rcToPeriodofCover.value),
                v = "";
            for (f = 0; f < r.split("Â±").length; f++)
                for (e = 0; e < l.length; e++) {
                    var u = l[e].EC_Input_Type,
                        y = l[e].Ext_Cvr_Code,
                        o = "0.00",
                        p = "",
                        s = "",
                        w = "",
                        b = "",
                        ft = !0,
                        k = "",
                        d = "";
                    if (r.split("Â±")[f].trim() == y) {
                        if ($.each(ct, function() {
                                $(this)[0].Ext_Cvr_Ind == "Y" && y == $(this)[0].Ext_Cvr_Code && (ft = !1, s = u == "CRT" ? $(this)[0].Ext_Cvr_CART_Days != "" ? $(this)[0].Ext_Cvr_CART_Days : tt[0].CART_Days : "", w = u == "CRT" ? $(this)[0].Ext_Cvr_CART_Amount != "" ? $(this)[0].Ext_Cvr_CART_Amount : it[0].CART_Amount : "", p = u == "UNIT" ? $(this)[0].EC_UOM_Value != "" ? $(this)[0].EC_UOM_Value : rt[0].PA_Basic_Unit : "", s = u == "DAY" ? $(this).find("Day").text() : s, o = $(this)[0].Ext_Cvr_SI != undefined && $(this)[0].Ext_Cvr_SI != "" ? $(this)[0].Ext_Cvr_SI : "", b = $(this)[0].Ext_Cvr_Premium_Displayed, k = u == "POC" ? $(this)[0].Ext_Cvr_Effective_Date == "" ? lt.get_dateInput().get_selectedDate().format("dd/MM/yyyy") : $(this)[0].Ext_Cvr_Effective_Date : "", d = u == "POC" ? $(this)[0].Ext_Cvr_Expiry_Date == "" ? at.get_dateInput().get_selectedDate().format("dd/MM/yyyy") : $(this)[0].Ext_Cvr_Expiry_Date : "")
                            }), r.split("Â±")[f].trim() == "89" && (a = $("#hdfWScreenSI")[0].value, o = a != "" ? a.trim() != "" ? a : "0" : 0), ft) switch (y) {
                            case "112":
                                s = u == "CRT" ? tt[0].CART_Days : "";
                                w = u == "CRT" ? it[0].CART_Amount : "";
                                break;
                            case "200":
                                p = rt[0].PA_Basic_Unit;
                                break;
                            case "201":
                                s = ht[0].Courtesy_Car_Days;
                                break;
                            case "202":
                                o = st[0].Water_Damage_SI;
                                break;
                            case "203":
                                o = ot[0].Key_Replacement_SI;
                                break;
                            case "101":
                                k = u == "POC" ? $("#txtEdate").context.fileCreatedDate : "";
                                d = u == "POC" ? $("#txtXdate").context.fileCreatedDate : ""
                        }
                        v += r.split("Â±")[f] + "Ã¾" + (o == "" ? "0" : o) + "Ã¾" + (b == "" ? "0" : b) + "Ã¾" + s;
                        v += "Ã¾" + w + "Ã¾Ã¾" + k + "Ã¾" + d + "Ã¾" + p + "Ã¾" + u + "Ã¾" + l[e].Compulsory_Ind + "Â±"
                    }
                }
            $("#hdfExtCover_dt").val(v);
            Concatenate("ddlExtraCoverage")
        }
    } catch (vt) {
        CommonErrorlog("fnCallExtPremium", vt.message, PricingPageURL);
        blockUI(!1)
    }
}

function fnCommonExPremcall(n, t, i, r) {
    var u = ServiceURL() + "/fnCreateReQuot",
        f = JSON.stringify({
            MstXML: sessionStorage.getItem("PolicyXML"),
            SeqNo: sessionStorage.getItem("SeqNo") != null ? sessionStorage.getItem("SeqNo") : "1",
            QuoNo: n,
            EventName: t,
            PolPremInd: i,
            Passin: r,
            EndtEffDate: $("#dtEndtEffDate")[0] != undefined ? $("#dtEndtEffDate")[0].value : ""
        });
    fnCommonAjaxAsyncCall(u, f, t)
}

function ExtraCoverageItemChecked() {
    var n = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        t = document.getElementById(n.attributes.btnGetItem.value);
    t.click()
}

function fnShowError(n) {
    var t = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        u = document.getElementById(t.attributes.lblErrorMsg.value),
        f = document.getElementById(t.attributes.lbltopErrorMsg.value),
        i = document.getElementById(t.attributes.pnlbtnErrorMsg.value),
        r = document.getElementById(t.attributes.pnltopErrorMsg.value);
    f.innerHTML = u.innerHTML = strError;
    strError != "" ? (i.style.display = "block", r.style.display = "block") : (i.style.display = "none", r.style.display = "none");
    $("#trerrorId").show();
    document.getElementById("trerrorId").style.display = "Show";
    $("#pnlbtmErrorMsg").show();
    $("[id$=pnlbtnErrorMsg]").show();
    $("[id$=lbltopErrorMsg]").text(n);
    $("[id$=lblErrorMsg]").text(n)
}

function CommonErrorlog(n, t, i) {
    var i = i + "/fnErrorlog",
        r = JSON.stringify({
            MethodName: n,
            ExMessage: t
        });
    fnCommonAjaxCall(i, r, "Error")
}

function fnCommonAjaxCall(n, t, i) {
    fnDisplayMainErrorMsg("", "");
    try {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: t,
            url: n,
            async: !1,
            dataType: "text",
            success: function(n) {
                if (n != "")
                    if (i != "NCDReply" ? JSON.parse(n).d.toUpperCase().indexOf("ERROR ") < 0 : !0) switch (i) {
                        case "NCDReply":
                            fnNCDReply(JSON.parse(n).d);
                            break;
                        case "HstMarketValue":
                            fnBindHstMarketValue(JSON.parse(n).d);
                            break;
                        case "IsmMarketValue":
                            fnIsmMarketValue(JSON.parse(n).d)
                    } else fnDisplayMainErrorMsg("", n.d)
            },
            failure: function() {
                blockUI(!1)
            },
            error: function() {
                blockUI(!1)
            }
        })
    } catch (r) {
        CommonErrorlog("Motor_fnCommonAjaxCall", r.message, PricingPageURL);
        blockUI(!1)
    }
}

function fnAddonExtCover(n, t, i, r) {
    fnDisplayMainErrorMsg("", "");
    try {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: t,
            url: n,
            async: !0,
            dataType: "text",
            success: function(n) {
                if (n != "")
                    if (i != "NCDReply" ? JSON.parse(n).d.toUpperCase().indexOf("ERROR ") < 0 : !0) switch (i) {
                        case "AddonExtCover":
                            var t = JSON.parse(n).d;
                            t.length > 1 ? ($("[id$=hdnExtra]")[0].value = JSON.stringify(t), fnExtraCover()) : $("#hdfQuoStage")[0].value == "0" && r.innerText != "Save & Preview" ? ProceedToCoverNote() : Concatenate(r)
                    } else fnDisplayMainErrorMsg("", n.d)
            },
            failure: function() {
                blockUI(!1)
            },
            error: function() {
                blockUI(!1)
            }
        })
    } catch (u) {
        CommonErrorlog("fnAddonExtCover", u.message, PricingPageURL);
        blockUI(!1)
    }
}

function alphanumericsonly(n) {
    var t = /([^a-z0-9])/gi;
    t.test(n.value) && (n.value = n.value.replace(t, ""))
}

function ReferralPopup(n, t) {
    var r = "<table><tbody >",
        i;
    r += "<tr >";
    r += "<td><label for='desc'> " + n.replace(/R: /g, "") + "<br/><center> Do you wish to continue?<center/><\/label><\/td><\/tr>";
    r += "<\/tbody><\/table>";
    i = $("[id$=hdfConfirmInd]")[0].value;
    $("<div  id=Extracover >" + r + "<\/div>").dialog({
        title: i == 2 ? "Notification " : "Current Risk Requires Underwriting Approval.",
        modal: !0,
        open: function() {},
        close: function() {
            $(this).dialog("close")
        },
        width: 500,
        buttons: {
            OK: function() {
                if (i == "0" || $("#hdfQuoStage")[0].value == "0") fnDisplayValuesPreview(t), $("[id$=hdfConfirmInd]")[0].value = "1";
                else if (i == "1") Concatenate("Referral");
                else if (i == "2") {
                    var n = AbsURL + "CoverNote/UpdateReceiptNo.aspx?QNo=" + t;
                    window.location = n
                }
                $(this).dialog("close")
            },
            No: function() {
                var n = AbsURL + "CoverNote/QuotationSummary.aspx?Ind=P%2bjUwjxdIhc%3d";
                window.location = n;
                $(this).dialog("close")
            }
        }
    })
}

function fnCoverTypeIndexChanged(n, t) {
    var r = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        i = $find(r.attributes.ddlProductCode.value),
        e = $find(r.attributes.ddlTransactionType.value),
        f = $find(r.attributes.txtYearofMake.value),
        u;
    ComboIndexChanged(n, t);
    SetUIControlSettings(e.get_value(), i.get_value(), n.get_value());
    u = parseInt($("[id$=hdfVcoApplicableYear]").val());
    n.get_value() != "" && n.get_value() != "0" && n.get_value() != "V-FT" && (new Date).getFullYear() - parseInt(f._value) > u && (i.get_value() == "PC01" || i.get_value() == "PZ01") && f._value != "" && $('<div  title=Alert id="AgePopup" > Third Party Fire & Theft Cover is recommended for vehicle age above ' + u + " years. <br>  Do you want to change Cover Type? <\/div>").dialog({
        title: "Notification!",
        modal: !0,
        open: function() {},
        width: 600,
        buttons: {
            Yes: function() {
                fnCoverTypeWarning();
                $(this).dialog("close")
            },
            No: function() {
                $(this).dialog("close")
            }
        }
    });
    fnGetReconInd(i.get_value(), n.get_value());
    ddlProductCode_IndexChanged();
    blockUI(!1)
}

function fnGetReconInd(n, t) {
    n != "" && t != "" && $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ServiceURL() + "/fnGetReconInd",
        data: "{'ProductCode':'" + n + "','CoverType':'" + t + "'}",
        dataType: "json",
        success: function(n) {
            n.d == "Y" ? ($("[id$=chckRecon]")[0].checked = !0, $("[id$=chckRecon]").css("display", "inline-block"), $("[id$=hdfReconInd]")[0].value = "Y", $("label[for='chckRecon']").text("Recon Car")) : ($("[id$=hdfReconInd]")[0].value = "N", $("[id$=chckRecon]")[0].checked = !1, $("[id$=chckRecon]").css("display", "none"), $("label[for='chckRecon']").text(""))
        },
        failure: function() {},
        error: function() {}
    })
}

function fnQuoExit() {
    var n = AbsURL + "Menu.aspx";
    window.location.href = n
}

function fnAssignModelDetails(n, t) {
    var o = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        i = t != undefined && t == "VIX" ? n.split("Ã»") : n.d.split("Ã»"),
        c = $("[id$=lblMakeError]"),
        l = $find(o.attributes.txtBodyType.value),
        u = $find(o.attributes.ddlUOM.value),
        f = $find(o.attributes.txtNoofPassenger.value),
        v = $("[id$=lblErrorCapacity]"),
        r, s, e, h, a;
    if (f.set_displayValue(""), f._SetValue(null), c.text(""), v.text(""), fnRemoveComboItems(u), l.set_value(""), i[0] == "Y" && i[1] != "")
        for (document.getElementById("hdfWScreenSI").value = i[5], document.getElementById("hdfVehBodyID").value = i[4], document.getElementById("hdfCapacityDtls").value = i[1], f.set_displayValue(i[3]), f._SetValue(parseInt(i[3])), TextBoxTextChanged(f), l.set_value(i[6]), $("[id$=hdfVehSegment]").val(i[7]), r = new Telerik.Web.UI.RadComboBoxItem, s = i[2].split("Â±"), r = new Telerik.Web.UI.RadComboBoxItem, r.set_text("Select"), r.set_value("0"), u.trackChanges(), u.get_items().add(r), u.commitChanges(), e = 0; e < s.length; e++) h = s[e].split("Ã¾"), r = new Telerik.Web.UI.RadComboBoxItem, r.set_text(h[1]), r.set_value(h[0]), u.trackChanges(), u.get_items().add(r), u.commitChanges(), e == 0 && (r.select(), ComboIndexChanged(u), fnUOMIndexChanged(u));
    else c.text("* Please select Model, you may enter some keyword(s) to search."), a = $find(o.attributes.ddlModel.value), fnRemoveComboItems(a)
}

function fnCheckedEmgMobNo(n) {
    var f = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        t = $find(f.attributes.ddlEmgMobNoPrefix.value),
        i = $find(f.attributes.txtEmgMobNo.value),
        r = $find(f.attributes.txtEmgContactName.value),
        u;
    $("[id$=lblErrorEmgMobileNo]").text("");
    n.checked ? (u = new Telerik.Web.UI.RadComboBoxItem, u.set_text("select"), u.set_value("0"), t.trackChanges(), t.get_items().add(u), u.select(), t.commitChanges(), i.set_displayValue(""), i._SetValue(""), i.get_styles().DisabledStyle[0] = i.get_styles().EnabledStyle[0].replace("#FFF284", "#FFFFFF"), i.updateCssClass(), i.disable(), r.set_displayValue(""), r._SetValue(""), r.get_styles().DisabledStyle[0] = r.get_styles().EnabledStyle[0].replace("#FFF284", "#FFFFFF"), r.updateCssClass(), r.disable(), t.get_inputDomElement().style.backgroundColor = "white", t.disable()) : (i.enable(), r.enable(), t.enable(), t.set_value("0"), LoadComboBox(t))
}

function LoadComboBox(n) {
    n.get_inputDomElement().style.backgroundColor = n.get_value() == "" || n.get_value() == "0" ? "#FFF284" : "white"
}

function fnPremBack() {
    var r = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        t = $find(r.attributes.rmpMQ_InusredDetails.value),
        n = $find(r.attributes.rtsMQ.value);
    $("[id$=trCoverageview]").css("display", "table-row");
    $("[id$=trPremPreview]").css("display", "none");
    var i = t.findPageViewByID("rpvVechInsuredDtls"),
        u = t.findPageViewByID("rpvPremium"),
        f = t.findPageViewByID("rpvPreview");
    if (t._selectedIndex == 0) n._selectedIndex = 1, n.commitChanges(), i.set_selected(!0), i.select();
    else return t._selectedIndex == 1 ? (n._selectedIndex = 0, n.commitChanges(), i.set_selected(!0), i.select(), $("#rtsMQ div ul li.rtsLI a").removeClass("rtsSelected"), $("#rtsMQ div ul li.rtsLI.rtsFirst a").addClass("rtsSelected"), $("#rtsMQ div ul li.rtsLI.rtsLast a").removeClass("rtsSelected"), $("[id$=trCoverageview]").css("display", "table-row"), $("[id$=trPremPreview]").css("display", "none"), $("[id$=btnSubmit]").hide(), $("[id$=hdfQuoStage]")[0].value == "0" && $("[id$=btnPrintQuotation]").hide(), !0) : (n._selectedIndex = 1, n.commitChanges(), u.set_selected(!0), u.select(), n._selectedIndex = 1, $("#rtsMQ div ul li.rtsLI a").addClass("rtsSelected"), $("#rtsMQ div ul li.rtsLI.rtsFirst a").removeClass("rtsSelected"), $("#rtsMQ div ul li.rtsLI.rtsLast a").removeClass("rtsSelected"), $("#rtsMQ div ul li.rtsLI a").removeClass("rtsAfter"), $("[id$=trCoverageview]").css("display", "none"), $("[id$=trPremPreview]").css("display", "table-row"), $("[id$=btnSubmit]").show(), $("[id$=hdfQuoStage]")[0].value == "0" && $("[id$=btnPrintQuotation]").hide(), !0)
}

function Enablefn(n) {
    var t = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        i = document.getElementById("trComReg"),
        r = document.getElementById("trIC"),
        u = document.getElementById("trAge"),
        f = document.getElementById("trOldIC"),
        e = document.getElementById("trDOB"),
        o = document.getElementById("trMaritalSts"),
        s = document.getElementById("trGender"),
        a = document.getElementById(t.attributes.hdfQuoStage.value),
        v = document.getElementById(t.attributes.txtName.value),
        h = document.getElementById("trOccupation"),
        c = document.getElementById("trInsuredNationality"),
        l;
    return (n._lastKeyCode != 9, n.get_value() == "P") ? (r.style.display = "table-row", u.style.display = "table-row", f.style.display = "table-row", e.style.display = "table-row", i.style.display = "none", s.style.display = "table-row", a.value == "1" && (c.style.display = "table-row", o.style.display = "table-row", h.style.display = "table-row"), !0) : n.get_value() == "C" ? (r.style.display = "none", u.style.display = "none", f.style.display = "none", e.style.display = "none", s.style.display = "none", o.style.display = "none", h.style.display = "none", c.style.display = "none", i.style.display = "table-row", !0) : n.get_value() == "" || n.get_value() == "0" ? (l = n.get_inputDomElement(), l.focus(), !0) : void 0
}

function HidePac() {
    document.getElementById("trPacRiderExtCoverItems").style.display = "none";
    document.getElementById("trPacRiderExtCoverDtls").style.display = "none"
}

function PacClear() {
    $("#lblSumInsured")[0].innerHTML = "0.00";
    $("#lblDiffPremium")[0].innerHTML = "0.00";
    $("#lblAddPremium")[0].innerHTML = "0.00";
    $("#lblTotPac")[0].innerHTML = "0.00";
    $("#lblDiffGSt")[0].innerHTML = "0.00";
    $("#lblDiffStampDuty")[0].innerHTML = "0.00";
    $("#lblPAExtraCoverage")[0].innerHTML = "0.00"
}

function fnDoPostBack(n, t) {
    for (var e, r = "", u = t.get_items(), f = u._parent._checkedIndices, o = f.length, i = 0; i < o; i++) e = u.getItem(f[i]), r += e._properties._data.value + "Â±";
    return (n = n != "" ? n + "Â±" : "", r != n) ? !0 : !1
}

function autotab(n, t) {
    n.getAttribute && n.value.length == n.getAttribute("maxlength") && t.focus()
}

function OnClientTabSelect(n, t) {
    if (!fnValidateQuotationDtls($("[id$=btnSubmit]"))) return t.set_cancel(!0), !1
}

function fnSumInsOnBlur(n) {
    String(n.get_value()).trim() == "" && n.set_value(0);
    n._element.control._element.value == 0 && alert("Notification : The sum insured cannot be zero")
}

function fnReset() {
    var n = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        f = $find(n.attributes.ddlModel.value),
        e = $find(n.attributes.ddlMake.value),
        o = $find(n.attributes.txtCapacity.value),
        i = $find(n.attributes.txtChassisNo.value),
        s = $find(n.attributes.txtNoofPassenger.value),
        h = $find(n.attributes.txtLogBookNo.value),
        r = $find(n.attributes.txtEngineNo.value),
        it = $find(n.attributes.ddlMotorTrade.value),
        c = $find(n.attributes.txtPurchaseCompany.value),
        l = $find(n.attributes.txtTrailerNo.value),
        a = $find(n.attributes.txtTrailerChassisNo.value),
        v = $find(n.attributes.ddlIsOwnedCar.value),
        rt = $find(n.attributes.ddlVOCYear.value),
        ut = $find(n.attributes.ddlVOCMonth.value),
        y = $find(n.attributes.ddlIsModifiedCar.value),
        ft = $find(n.attributes.ddlModifiedPerform.value),
        u = $find(n.attributes.ddlModifiedFunctional.value),
        p = $find(n.attributes.txtModel.value),
        w = $find(n.attributes.ddlUOM.value),
        b = $find(n.attributes.ddlLoanType.value),
        k = $find(n.attributes.txtYearofMake.value),
        d = $find(n.attributes.ddlRegLocation.value),
        et = $find(n.attributes.ddlRegionCode.value),
        g = $find(n.attributes.ddlcostCenter.value),
        nt = $find(n.attributes.txtVehicleNo.value),
        tt = $find(n.attributes.txtPreVehicleNo.value),
        t = "";
    nt.set_value(t);
    tt.set_value(t);
    p.set_value(t);
    $("[id$=lblErrorCapacity]").text(t);
    $("[id$=lblErrorChassisNo]").text(t);
    $("[id$=lblLogBookError]").text(t);
    $("[id$=lblUOMError]").text(t);
    $("[id$=lblYearError]").text(t);
    $("[id$=txtBodyType]")[0].value = t;
    b.clearItems();
    d.clearItems();
    c.set_value(t);
    l.set_value(t);
    a.set_value(t);
    k.set_value(t);
    i.set_value(t);
    h.set_value(t);
    r.set_value(t);
    g.clearSelection();
    e.clearSelection();
    f.clearSelection();
    w.clearSelection();
    s.set_value(null);
    o.set_value(null);
    v.clearItems();
    y.clearItems();
    u.clearSelection();
    u.clearSelection();
    i._enabled = !0;
    r._enabled = !0;
    fnClear();
    fnCoverageClear()
}

function fnExtraCoverageItemChecked(n, t) {
    var i = $("[id$=hdfChckItems]").val();
    fnDoPostBack(i, n) && ExtraCoverageItemChecked(n, t)
}

function fnChckAddonExtCover(n, t) {
    var u = "Preview",
        f = $("[id$=hdnBlnInd]")[0].value;
    if (u == "Preview" && f == "1") {
        var e = document.getElementById($("[id$=hdfControlIDs]")[0].id),
            o = $find(e.attributes.ddlProductCode.value),
            s = $find(e.attributes.ddlCoverType.value),
            i = sessionStorage.getItem("Motor_Extra_Cover_Premium") != null ? JSON.parse(sessionStorage.getItem("Motor_Extra_Cover_Premium")) : "",
            r = sessionStorage.getItem("sdfExtCover") != null ? JSON.parse(sessionStorage.getItem("sdfExtCover")) : "";
        i != "" && (i = JSON.stringify(i));
        r != "" && (r = JSON.stringify(r));
        var h = u + "Â±" + 
            o._value + "Â±" + 
            s._value + "Â±" + 
            f,
            c = ServiceURL() + "/fnChckAddonExtCover",
            l = JSON.stringify({
                MstData: h,
                ExtraCover: i,
                sdfExtCover: r
            });
        fnAddonExtCover(c, l, "AddonExtCover", n)
    } else $("#hdfQuoStage")[0].value == "0" && t == undefined && n.innerText != "Save & Preview" ? ProceedToCoverNote() : Concatenate(n)
}

function SIRoundOff(n) {
    var f;
    try {
        var e = document.getElementById($("[id$=hdfControlIDs]")[0].id),
            u = document.getElementById(e.attributes.txtPrmInsured.value),
            t = parseFloat((u.value != null ? u.value : 0).replace(",", "")),
            i = 1e3,
            r = $("[id$=ddlProductCode]").val().split("-")[0];
        (r == "MC01" || r == "MC02" || r == "MC03" || r == "MZ01") && (i = 100);
        f = Math.floor(t / i) * i;
        t - f > 0 && (t = (Math.floor(t / i) + 1) * i);
        u.value = formatNumber(t);
        Concatenate(n)
    } catch (o) {}
}

function GetCheckedItems(n) {
    for (var f, i = [], e = n, r = e.get_items(), u = r._parent._checkedIndices, o = u.length, t = 0; t < o; t++) f = r.getItem(u[t]), i.push(f._properties._data.value);
    return i
}

function Concatenate(n) {
    var o, s, a, e;
    blockUI(!0);
    TimerInsertFun("", "");
    var t = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        u = $("#lblQuoCovNo").text(),
        h = $find(t.attributes.ddlTransactionType.value),
        nt = $("[id$=hdfTrnsType]").val(),
        tt = h._value == "BR" ? nt : h._value,
        it = $find(t.attributes.txtVehicleNo.value),
        rt = $find(t.attributes.txtPreVehicleNo.value),
        ut = $find(t.attributes.rcFromPeriodofCover.value),
        ft = $find(t.attributes.rcToPeriodofCover.value),
        et = $find(t.attributes.ddlProductCode.value),
        v = $find(t.attributes.ddlCoverType.value),
        ot = $find(t.attributes.ddlModel.value),
        c = $find(t.attributes.ddlCICode.value),
        st = document.getElementById(t.attributes.txtYearofMake.value),
        ht = $find(t.attributes.txtCapacity.value),
        ct = $find(t.attributes.ddlUOM.value),
        lt = $find(t.attributes.txtChassisNo.value),
        at = $find(t.attributes.txtNoofPassenger.value),
        vt = $find(t.attributes.txtName.value),
        yt = $find(t.attributes.txtOldIC.value),
        y = document.getElementById("txtNewIC1"),
        p = document.getElementById("txtNewIC2"),
        w = document.getElementById("txtNewIC3"),
        pt = $find(t.attributes.ddlDay.value),
        wt = $find(t.attributes.ddlMonth.value),
        bt = $find(t.attributes.ddlYear.value),
        kt = $find(t.attributes.txtPostCode.value),
        dt = $find(t.attributes.ddlState.value),
        gt = $find(t.attributes.ddlCountry.value),
        ni = $find(t.attributes.txtAge.value),
        ti = $find(t.attributes.txtCmpRegNo.value),
        ii = $find(t.attributes.txtPhNo.value),
        ri = $find(t.attributes.ddlMblPrefix.value),
        ui = $find(t.attributes.txtEmgContactName.value),
        fi = $find(t.attributes.txtEmgMobNo.value),
        ei = $find(t.attributes.ddlEmgMobNoPrefix.value),
        oi = $find(t.attributes.ddlIsOwnedCar.value),
        si = $find(t.attributes.ddlVOCYear.value),
        hi = $find(t.attributes.ddlVOCMonth.value),
        ci = $find(t.attributes.ddlIsModifiedCar.value),
        l = $find(t.attributes.ddlModifiedPerform.value);
    l = GetCheckedItems(l);
    o = $find(t.attributes.ddlModifiedFunctional.value);
    o = GetCheckedItems(o);
    var li = document.getElementById(t.attributes.chckIsInsuredIC.value),
        ai = $find(t.attributes.txtStaffID.value),
        vi = $find(t.attributes.txtStaffIC.value),
        rs = document.getElementById(t.attributes.trStaffID.value),
        us = document.getElementById(t.attributes.trStaffIC.value),
        yi = $find(t.attributes.ddlcostCenter.value),
        f = $find(t.attributes.ddlNoofNamedDrivers.value),
        pi = document.getElementById(t.attributes.txtPrevSI.value),
        wi = y.value + p.value + w.value,
        bi = ut.get_dateInput(),
        ki = ft.get_dateInput(),
        fs = document.getElementById(t.attributes.spnLogBookNo.value),
        di = $find(t.attributes.txtLogBookNo.value),
        es = document.getElementById(t.attributes.spnEnginNo.value),
        gi = $find(t.attributes.txtEngineNo.value),
        os = document.getElementById(t.attributes.trTrailerNo.value),
        nr = $find(t.attributes.txtTrailerNo.value),
        tr = $find(t.attributes.txtTrailerChassisNo.value),
        ir = $find(t.attributes.ddlRegionCode.value),
        rr = $find(t.attributes.ddlRegLocation.value),
        ur = $find(t.attributes.txtPurchaseCompany.value),
        fr = $find(t.attributes.ddlGender.value),
        er = $find(t.attributes.ddlMaritalSts.value),
        or = $find(t.attributes.ddlOccupation.value),
        sr = $find(t.attributes.txtAddress.value),
        ss = document.getElementById(t.attributes.lblLogBookError.value),
        hr = $find(t.attributes.ddlMotorTrade.value),
        cr = document.getElementById(t.attributes.txtOffPhNo.value),
        lr = document.getElementById(t.attributes.txtOffPrefix.value),
        ar = $find(t.attributes.ddlInsuredNationality.value),
        vr = $find(t.attributes.txtRelaMemNo.value),
        yr = $("#chkSighted").is(":checked") ? "Y" : "N",
        pr = $("#chckRecon").is(":checked") ? "Y" : "N",
        wr = $("[id$=chckMobile]").is(":checked") ? "Y" : "N",
        br = $("#chckEmgMobNo").is(":checked") ? "Y" : "N",
        li = $("#chckIsInsuredIC").is(":checked") ? "1" : "0",
        kr = $("#hdfVehBodyID").val(),
        dr = $find(t.attributes.ddlInsIndicator.value),
        gr = $find(t.attributes.txtTown.value),
        nu = $("#hdfISMMV").val(),
        tu = $("#hdfISMNCD").val(),
        iu = $("#hdfEXLTempPassin").val(),
        ru = $("#hdfWarn").val(),
        uu = $("#hdfTempISM").val(),
        fu = $("#hdfExlSpecialInd").val(),
        eu = $("#hdfAllowInd")[0].value,
        ou = $("#hdfNCDReply").val(),
        su = $("#hdfQuoStage").val(),
        hu = $("#TempPassInValues").val(),
        cu = $("#hdfAgentDtls").val(),
        lu = $("#hdfTrailerInd").val(),
        au = $("#dblABISumIns").val(),
        vu = $("#dblABIOrgSumIns").val(),
        yu = $("#hdfReQuoteInd").val(),
        pu = $("#hdfOptionalInd").val(),
        wu = $("#hdfWScreenSI").val(),
        bu = $("#hdfISMValidInd").val(),
        ku = $("#hdfErrorMsg").val(),
        du = $("#hdfTabPrevValue").val(),
        gu = $("#hdfTempMV").val(),
        nf = $("#hdfNCDValue").val(),
        tf = $("#hdfIPAddrs").val(),
        rf = $("#hfSightedICInd").val(),
        uf = $("#hdfNCDValidInd").val(),
        ff = $("#hdfISMMrkValueInd").val(),
        ef = $("[id$=hdnMtrComm]")[0].value,
        of = $("[id$=hdnPACComm]")[0].value,
        sf = $("[id$=ddlABIInd]")[0].value,
        hf = $("#txtAllRiderInd").text(),
        b = document.getElementById(t.attributes.txtPrmInsured.value).value,
        cf = $("#ddlAgreeValue option:selected").text(),
        lf = $("#rbAvSI").is(":checked") ? "Y" : "N",
        af = $("#rbRecSI").is(":checked") ? "Y" : "N",
        vf = $("[id$=lblPACDsc]")[0].innerHTML,
        yf = $("#txtCN_NCDPer").text(),
        pf = $("#ddlVoluntaryExcess option:selected").text(),
        wf = $("#lblCurAgreeValue").text(),
        bf = $("#ddlPACUnit option:selected").text(),
        s = $("#ddlPAC option:selected").val(),
        kf = $("#ddlNCDPer option:selected").val(),
        df = $("#hdfExtCover").val(),
        hs = $("#hdfPrevSumInsured").val(),
        gf = $("#hdnPacType").val(),
        ne = $("[id$=txtCN_NCDPer]")[0].textContent,
        te = $("[id$=txtCN_DsNCDAmt]")[0].textContent,
        ie = $("#txtDsBasicPremium").text(),
        re = $("#txtDSLoadingPer").text(),
        ue = $("#txtDSLoadingAmt").text(),
        fe = $("#txtDsTuitionLoadPer").text(),
        ee = $("#txtDsTuitionLoadAmt").text(),
        oe = $("#txtAllRider").text(),
        se = $("#txtPrmServicTaxAmt").text(),
        he = $("#txtPrmStampDutyAmt").text(),
        ce = $("#txtExcessAmt").text(),
        le = $("#hdfActPrem").val(),
        ae = $("#hdfQuoValExpDate").val(),
        ve = $("#hdfDirectInd").val(),
        ye = $("#txtTtlPayablePremium").text(),
        pe = $("#txtTtlCoveragePremium").text(),
        we = $("#hdfServiceTax").val(),
        be = $("#dblABIMktValue").val(),
        k = $("#dblPrevSumIns").val(),
        ke = $("#txtPrmRebatePer").text(),
        de = $("#txtPrmRebateAmt").text(),
        ge = $("#lblVoluntaryExcessPrem").text(),
        no = $("#lblRenewalBonus").text(),
        to = $("#lblTotalExcess").text(),
        io = $("#lblPrevSI").text(),
        ro = $("#lblAvSI").text(),
        uo = $("#lblRecSI").text(),
        fo = $("#lblAdjustmentPct").text(),
        eo = $("#lblAdjustmentAmt").text(),
        oo = document.getElementById("txtPersonalNoPass").value,
        so = $("#txtPersonalSumInsured").text(),
        ho = $("#txtDsPersonalPremium").text(),
        co = $("#txtDsPersonalAddPremium").text(),
        lo = $("#txtDsPersonalServiceTax").text(),
        ao = $("#txtDsPersonalStampDuty").text(),
        vo = $("#txtDsPACExtraCover").text(),
        yo = $("#txtPACRebatePer").text(),
        po = $("#txtPACRebateAmt").text(),
        wo = document.getElementById(t.attributes.txtPrmInsured.value).disabled == !1 ? "Y" : "N",
        bo = document.getElementById(t.attributes.txtPrevSI.value).disabled == !1 ? "Y" : "N",
        ko = $("#lblPrevSI").context.body.disabled == !1 ? "Y" : "N",
        go = $("ddlAgreeValue").context.body.disabled = "N";
    $("#trPacRiderExtCoverItems").show();
    document.getElementById("trPacRiderExtCoverItems").style.display = "Show";
    var ns = $find(t.attributes.ddlLoanType.value),
        ts = $find(t.attributes.txtBodyType.value),
        d = document.getElementById(t.attributes.trThorHdr.value),
        g = ddlThCovType = txtThCovTypeDesc = ddlThARMCode = "";
    d != undefined && d.style.display == "table-row" && (g = $find(t.attributes.ddlThBussUnit.value).get_value() == "0" ? "" : $find(t.attributes.ddlThBussUnit.value).get_value(), ddlThCovType = $find(t.attributes.ddlThCovType.value).get_value(), txtThCovTypeDesc = $find(t.attributes.txtThCovTypeDesc.value)._text, ddlThARMCode = $find(t.attributes.ddlThARMCode.value).get_value());
    v._value == "V-TP" ? (b = "0", document.getElementById(t.attributes.txtPrmInsured.value).disabled == !0, document.getElementById(t.attributes.txtPrmInsured.value).value == "0") : (document.getElementById(t.attributes.txtPrmInsured.value).disabled == !1, document.getElementById(t.attributes.txtPrmInsured.value).value = document.getElementById(t.attributes.txtPrmInsured.value).value);
    f = f._value == "" ? f._emptyMessage : f._value;
    n == undefined && $("#hdfQuoStage")[0].value == "1" && fnSaveNameDrDtls();
    fnSavePacRiderDtls();

    // mstdata
    var i = h._value + "Â±" + // BR transaction type
        tt + "Â±" + // $("[id$=hdfTrnsType]").val() / h_.value B 
        it.get_value() + "Â±" + // vehregno
        rt.get_value() + "Â±" + // oldvehregno
        bi._text + "Â±" + // pol start date 
        ki._text + "Â±" + // pol end date
        et._value + "Â±" + // product code
        v._value + "Â±" +  // cover type
        ot._value + "Â±" + // model
        (c._value == "" ? c._text.split("-")[0] : c._value) + "Â±" + // MX1
        st.value + "Â±" + // make year
        ht.get_value() + "Â±" + // capacity 1490
        ct._value + "Â±" + // CC
        lt.get_value().replace(/`/g, "") + "Â±" + // chassis no
        at.get_value() + "Â±" +  // seats 5
        vt.get_value().replace("`", "'") + "Â±" + // name
        yt.get_value() + "Â±" + // old ic
        wi + "Â±" + // new IC without dash
        dt._value + "Â±" + // ddl state
        gt._value + "Â±" + // ddl country
        ni.get_value() + "Â±" + // age 37
        ti.get_value() + "Â±" + // cmp reg no (phone num)
        ii.get_value() + "Â±" + // phone code
        ri._value + "Â±" +  // ddl mbl prefix
        ui.get_value() + "Â±" + // emg contact name
        fi.get_value() + "Â±" + // emg mob no
        ei._value + "Â±" + // mob no prefix
        oi._value + "Â±" + // is owned car
        si._value + "Â±" + // VOC year
        hi._value + "Â±" + // VOC month
        ci._value + "Â±" + // is mod car
        String(l).replace(/[&\/\\#+()$~%.'":*?<>{}]/g, "").replace(",", ";") + "Â±" + // modified perf
        String(o).replace(/[&\/\\#+()$~%.'":*?<>{}]/g, "").replace(",", ";") + "Â±" + // functional mod
        ai.get_value() + "Â±" + // staff id
        vi.get_value() + "Â±" + // staff IC
        yi.get_value() + "Â±" + // ddlcostcenter
        di.get_value().replace(/`/g, "") + "Â±" + // logbookno
        gi.get_value().replace(/`/g, "") + "Â±" + // engine no
        nr.get_value() + "Â±" + // trailerno
        tr.get_value() + "Â±" +  //txtTrailerChassisNo
        ir._value + "Â±" + // ddlRegionCode
        rr._value + "Â±" + // ddlRegLocation
        ur.get_value().replace("`", "'") + "Â±" + // txtPurchaseCompany
        fr._value + "Â±" + // ddlGender
        er._value + "Â±" + // ddlMaritalSts
        or._value + "Â±" + // ddlOccupation
        sr.get_value().replace("`", "'") + "Â±" + // txtAddress
        hr._value + "Â±" + // ddlMotorTrade
        cr.value + "Â±" + // txtOffPhNo
        lr.value + "Â±" + // txtOffPrefix
        f + "Â±" +  // ddlNoofNamedDrivers
        ar._value + "Â±" + // ddlInsuredNationality
        $("#hdnNamedDriver")[0].defaultValue + "Â±" + // #hdnNamedDriver
        $("#hdfOldPolNo")[0].value + "Â±" + 
        $("#hdfRenewalSameAgentInd")[0].value + "Â±" + 
        vr.get_value() + "Â±" + // txtRelaMemNo
        yr + "Â±" + // #chkSighted
        pr + "Â±" + // #chckRecon
        li + "Â±" + // chckIsInsuredIC
        k + "Â±" + // dblPrevSumIns
        pi.value + "Â±" + // txtPrevSI
        kr.replace(/Â±/g, "Ã¾") + "Â±" + // hdfVehBodyID
        iu + "Â±" + // hdfEXLTempPassin
        fu + "Â±" + // hdfExlSpecialInd
        ru + "Â±" + // hdfWarn
        uu + "Â±" + // hdfTempISM
        dr.get_value() + "Â±" + // ddlInsIndicator
        eu + "Â±" + // hdfAllowInd
        ou + "Â±" + // hdfNCDReply
        su + "Â±" + // hdfQuoStage
        y.value + "Â±" + // txtNewIC1
        p.value + "Â±" + // txtNewIC2
        w.value + "Â±" + // txtNewIC3
        pt._value + "Â±" + // ddlDay
        wt._originalText + "Â±" + // ddlMonth
        bt._value + "Â±" + // ddlYear
        kt.get_value() + "Â±" + // txtPostCode
        gr.get_value() + "Â±" + // txtTown
        hu + "Â±" + // TempPassInValues
        cu + "Â±" + // hdfAgentDtls
        lu + "Â±" + // hdfTrailerInd
        wr + "Â±" + // id$=chckMobile
        br + "Â±" + // chckEmgMobNo
        tf + "Â±" + // hdfIPAddrs
        rf + "Â±" + // hfSightedICInd
        ts.get_value() + "Â±" + // txtBodyType
        ($("[id$=lblErrorMsg]").text() + "Â±" + 
        g + "Â±" + // ''
        ddlThCovType + "Â±" + 
        txtThCovTypeDesc + "Â±" + 
        ddlThARMCode),

        // schdata
        r = b + "Â±" + // txtPrmInsured 0
            '0' + "Â±" + // 0
            hf + "Â±" + // txtAllRiderInd No
            cf + "Â±" + // #ddlAgreeValue option:selected No
            yf + "Â±" + // txtCN_NCDPer
            pf + "Â±" + // #ddlVoluntaryExcess option:selected
            bf + "Â±" + // #ddlPACUnit option:selected // 1
            s + "Â±" + // #ddlPAC option:selected // Y
            wf + "Â±" + // #lblCurAgreeValue
            gf + "Â±" + // #hdnPacType
            kf + "Â±" + // #ddlNCDPer option:selected // undefined
                 "Â±" + //
            sf + "Â±" + // [id$=ddlABIInd] / N
            $("#hdfSumInsChangInd")[0].defaultValue + "Â±" + // 0
            au + "Â±" + // dblABISumIns // 0
            vu + "Â±" +  // dblABIOrgSumIns /0 
            lf + "Â±" +  // rbAvSI / N
            af + "Â±" +  // rbRecSI / Y
            vf + "Â±" +  // [id$=lblPACDsc] innerHTML TAGPLUS.PAC
            yu + "Â±" + // hdfReQuoteInd null
            pu + "Â±" + // hdfOptionalInd 0|0|0
            wu + "Â±" + // hdfWScreenSI 2000.00
            ku + "Â±" + // hdfErrorMsg null
            bu + "Â±" + // hdfISMValidInd 1
            du + "Â±" + // hdfTabPrevValue 1
            "0" + "Â±" + // 0
            "0" + "Â±" + 
            "0" + "Â±" + 
            "0" + "Â±" + 
            "0" + "Â±" + 
            ne + "Â±" + // [id$=txtCN_NCDPer] textcontext 0.00
            te + "Â±" + // $("[id$=txtCN_DsNCDAmt]")[0].textContent 0.00
            ie + "Â±" + // $("#txtDsBasicPremium").text() 336.05
            re + "Â±" + // $("#txtDSLoadingPer").text() 0.00
            fe + "Â±" + // $("#txtDsTuitionLoadPer").text() 0.00
            ue + "Â±" + // $("#txtDSLoadingAmt") 0.00
            ee + "Â±" + // $("#txtDsTuitionLoadAmt").text() 0.00
            oe + "Â±" + // $("#txtAllRider").text() 0.00
            se + "Â±" + // $("#txtPrmServicTaxAmt").text() 39.51
            he + "Â±" + // $("#txtPrmStampDutyAmt").text() 10.00
            ce + "Â±" + // $("#txtExcessAmt").text() 0.00
            le + "Â±" + // $("#hdfActPrem").val() 133.65
            ye + "Â±" + // $("#txtTtlPayablePremium").text() 794.38
            pe + "Â±" + // $("#txtTtlCoveragePremium").text() null
            we + "Â±" + // $("#hdfServiceTax").val() 6.00
            be + "Â±" + // $("#dblABIMktValue").val() 0
            k + "Â±" + // $("#dblPrevSumIns").val() 0
            ke + "Â±" + // $("#txtPrmRebatePer").text() 0.00
            de + "Â±" + // $("#txtPrmRebateAmt").text() 0.00
            ge + "Â±" + // $("#lblVoluntaryExcessPrem").text() 0
            no + "Â±" + // $("#lblRenewalBonus").text() 0.00
            to + "Â±" + // $("#lblTotalExcess").text() 0.00
            io + "Â±" + // $("#lblPrevSI").text() null
            ro + "Â±" + // $("#lblAvSI").text() 0.00
            uo + "Â±" + // $("#lblRecSI").text() 0.00
            fo + "Â±" + // $("#lblAdjustmentPct").text() 10.00
            eo + "Â±" + // $("#lblAdjustmentAmt").text() null
            oo + "Â±" + // document.getElementById("txtPersonalNoPass").value 5
            so + "Â±" + // $("#txtPersonalSumInsured").text() 250,000.00
            ho + "Â±" + // $("#txtDsPersonalPremium").text() 72.00
            co + "Â±" + // $("#txtDsPersonalAddPremium").text() 0.00
            lo + "Â±" + // $("#txtDsPersonalServiceTax").text() 4.32
            ao + "Â±" + // $("#txtDsPersonalStampDuty").text() 10.00
            vo + "Â±" + // $("#txtDsPACExtraCover").text() 0.00
            yo + "Â±" + // $("#txtPACRebatePer").text() 0.00
            po + "Â±" + // $("#txtPACRebateAmt").text() 0.00
            ko + "Â±" + // $("#lblPrevSI").context.body.disabled == !1 ? "Y" : "N" // Y
            ae + "Â±" + // $("#hdfQuoValExpDate").val() 6-Jan-20
            ve + "Â±" + // $("#hdfDirectInd").val() null
            go + "Â±" + // $("ddlAgreeValue").context.body.disabled = "N" // N
            wo + "Â±" + // document.getElementById(t.attributes.txtPrmInsured.value).disabled == !1 ? "Y" : "N" // Y
            bo + "Â±" + // document.getElementById(t.attributes.txtPrevSI.value).disabled == !1 ? "Y" : "N" // Y
            gu + "Â±" + // $("#hdfTempMV").val() // PZ01|V-CO|JQR3339|2014|28*1007|06 Jan 2020|N
            uf + "Â±" + // $("#hdfNCDValidInd").val() 1
            nu + "Â±" + //  $("#hdfISMMV").val() 1
            tu + "Â±" + // $("#hdfISMNCD").val() 0
            nf.replace(/Â±/g, "Ã¾") + "Â±" + // $("#hdfNCDValue").val().replace(/Â±/g, "Ã¾") 0.00þ0
            ff + "Â±" + // $("#hdfISMMrkValueInd").val() 1
            ns.get_value() + "Â±" + // $find(t.attributes.ddlLoanType.value).get_value() null
            ef + "Â±" + // $("[id$=hdnMtrComm]")[0].value 0
            of +"Â±" + // $("[id$=hdnPACComm]")[0].value 0
            $("#hdfExtCover_dt")[0].defaultValue + "Â±" + // $("#hdfExtCover_dt")[0].defaultValue 72þ0þ7.50þþþþþþþDEFþN
            $("#hdfPACExtCover_dt")[0].defaultValue + "Â±" + // $("#hdfPACExtCover_dt")[0].defaultValue 89Aþ2,000.00þ300.00þþþþþþþSUMþN
            $("[id$=hdfPACRiderDtls]")[0].value + "Â±" + // $("[id$=hdfPACRiderDtls]")[0].value null
            df,
        is = n != undefined ? n.id == "btnCoverNote" ? !0 : !1 : !1;
    $("#hdfMaster")[0].defaultValue != i || 
    $("#hdfSchedule")[0].defaultValue != r ||
    n == "ddlExtraCoverage" ||
    n == "ddlPACExtraCover" ||
    (is = !0) ? (s = $("#ddlPAC option:selected").val(), s != "Y" && fnClearPACDtls(), n == undefined ? (a = sessionStorage.getItem("Error_Store") != null ? sessionStorage.getItem("Error_Store") : "", $("#hdfMaster")[0].defaultValue != i ||
    a != "" ? fnLoadMstSave(u, "MstSave", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue, $("[id$=hdfPACRiderDtls]")[0].value, "CovNext") : (OpenPremiumTab(), blockUI(!1)), TimerUpdateFun("btnNext")) : n == "NCD" ? (fnLoadPremPage(u, "Initial_Prem", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue, $("[id$=hdfPACRiderDtls]")[0].value, ""), TimerUpdateFun("NCD")) : n.id == "btnSubmit" ? (a = sessionStorage.getItem("Error_Store") != null ? sessionStorage.getItem("Error_Store") : "", e = fnValidateExtCover(), e == "" && (e = fnValidatePremiumDtls()), e == "" ? fnLoadPremPage(u, "Preview", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue, "", "Preview") : (fnDisplayMainErrorMsg("", e), blockUI(!1)), TimerUpdateFun("btnSubmit")) : n.id == "btnCoverNote" ? (fnLoadPremPage(u, "IssueCN", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue, $("[id$=hdfPACRiderDtls]")[0].value, "IssueCN"), TimerUpdateFun("btnCoverNote")) : n.id == "txtPrmInsured" ? (fnLoadPremPage(u, "fnSI_Change", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue, $("[id$=hdfPACRiderDtls]")[0].value, "fnSI_Change"), TimerUpdateFun("txtPrmInsured")) : n.id == "ddlPAC" ? (fnLoadPremPage(u, "PAC_Ind_Change", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue, $("[id$=hdfPACRiderDtls]")[0].value, "PAC_Ind_Change"), TimerUpdateFun("ddlPAC")) : n.id == "ddlPACUnit" ? (fnLoadPremPage(u, "PAC_Unit_Change", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue, $("[id$=hdfPACRiderDtls]")[0].value, "PAC_Unit_Change"), TimerUpdateFun("ddlPACUnit")) : n.id == "ddlExtraCoverage" ||
    n == "ddlExtraCoverage" ? (

        // QuoCovNo,
        // EventName,
        // MstData,
        // SchData,
        // ClaimDtls,
        // ExtraCover,
        // PacExtraCover,
        // MstRider,
        // BtnType
        fnLoadPremPage(
            u, // ignore
            "Motor_ExtCover_Item_Change",
            i, // MstData
            r, // SchData
            $("#hdfClaimDtls")[0].defaultValue,
            $("#hdfExtCover_dt")[0].defaultValue, // ExtraCover
            $("#hdfPACExtCover_dt")[0].defaultValue,
            $("[id$=hdfPACRiderDtls]")[0].value,
            "Motor_ExtCover_Item_Change"),
            TimerUpdateFun("btnGetItem")) : n == "ddlExtraCoverage_change" ? (fnLoadPremPage(u,
                "Motor_ExtCover_Control_Change",
            i,
            r,
            $("#hdfClaimDtls")[0].defaultValue,
            $("#hdfExtCover_dt")[0].defaultValue,
            $("#hdfPACExtCover_dt")[0].defaultValue,
            $("[id$=hdfPACRiderDtls]")[0].value,
            "Motor_ExtCover_Control_Change"),
            TimerUpdateFun("ddlExtraCoverage_change")
        ) : n.id == "ddlPACExtraCover" ||
    n == "ddlPACExtraCover" ? (fnLoadPremPage(u, "PAC_ExtCover_Item_Change", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue, $("[id$=hdfPACRiderDtls]")[0].value, "PAC_ExtCover_Item_Change"), TimerUpdateFun("btnGetPAC")) : n.id == "PAC_ExtCover_Control_Change" || 
    n == "PAC_ExtCover_Control_Change" ? (fnLoadPremPage(u, "PAC_ExtCover_Control_Change", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue, $("[id$=hdfPACRiderDtls]")[0].value, "PAC_ExtCover_Control_Change"), TimerUpdateFun("PAC_ExtCover_Control_Change")) : n.id == "btnPrintQuotation" ? (fnLoadPremPage(u, "btnPrintQuotation", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue, $("[id$=hdfPACRiderDtls]")[0].value, "PAC_ExtCover_Control_Change"), TimerUpdateFun("btnPrintQuotation")) : n.id == "txtPrevSI" ? (fnLoadPremPage(u, "Pre_SI_Change", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue, $("[id$=hdfPACRiderDtls]")[0].value, "Pre_SI_Change"), TimerUpdateFun("txtPrevSI")) : n.id == "ddlVoluntaryExcess" ? (fnLoadPremPage(u, "Vol_Excess_Change", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue, $("[id$=hdfPACRiderDtls]")[0].value, "Vol_Excess_Change"), TimerUpdateFun("ddlVoluntaryExcess")) : n.id == "rbAvSI" ? (fnLoadPremPage(u, "Chk_SI_Change", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue, $("[id$=hdfPACRiderDtls]")[0].value, "Chk_SI_Change"), TimerUpdateFun("rbAvSI")) : n.id == "rbRecSI" ? (fnLoadPremPage(u, "rbRecSI", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue, $("[id$=hdfPACRiderDtls]")[0].value, "rbRecSI"), TimerUpdateFun("rbRecSI")) : n == "Referral" ? fnLoadPremPage(u, "Referral", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue, $("[id$=hdfPACRiderDtls]")[0].value, "rbRecSI") : n.id == "ddlNCDPer" ? fnLoadPremPage(u, "Initial_Prem", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue) : n.id == "txtPrmInsured" ? (fnLoadPremPage(u, "SI_Change", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue), TimerUpdateFun("rbRecSI")) : n.id == "ddlPAC" && s == "Y" && (fnLoadPremPage(u, "PAC_Ind_Change", i, r, $("#hdfClaimDtls")[0].defaultValue, $("#hdfExtCover_dt")[0].defaultValue, $("#hdfPACExtCover_dt")[0].defaultValue), TimerUpdateFun("ddlPAC")), $("#hdfMaster").val(i), $("#hdfSchedule").val(r)) : n == undefined ? (OpenPremiumTab(), blockUI(!1)) : n.id == "btnSubmit" ? (OpenPreviewTab(), blockUI(!1)) : (OpenPremiumTab(), blockUI(!1))
}

function fnNDSetBackColor(n, t) {
    var i, s, e;
    try {
        if (n == null && t == null) {
            if (filterUICtrlSetting.length > 0)
                for (i = 0, s = filterUICtrlSetting.length; i < s; i++) switch (filterUICtrlSetting[i].sdfUIControlID) {
                    case "NamedDr":
                        IsNamedDriverValidate = filterUICtrlSetting[i].ValidationInd;
                        fnNDSetBackColor(filterUICtrlSetting[i].DisplayInd, filterUICtrlSetting[i].ValidationInd)
                }
            return
        }
        if ($find("<%= gvNamedDriver.ClientID %>") != null && (e = $find("<%= gvNamedDriver.ClientID %>").get_masterTableView(), e != null && e != undefined)) {
            var r = e.get_dataItems(),
                f = [],
                u = [],
                o = [];
            for (i = 0; i < r.length; i++) try {
                r[i].get_visible() && ($telerik.findControl(r[i].get_element(), "txtName") != null && u.push($telerik.findControl(r[i].get_element(), "txtName")), $telerik.findControl(r[i].get_element(), "txtICNumber") != null && u.push($telerik.findControl(r[i].get_element(), "txtICNumber")))
            } catch (h) {}
            for (i = 0; i < f.length; i++) try {
                f[i].get_inputDomElement().style.backgroundColor = t && (f[i].get_value() == ""
                f[i].get_value() == "0") ? "#FFF284" : "white";
                f[i].get_attributes().setAttribute("ValidationInd", t)
            } catch (h) {}
            for (i = 0; i < u.length; i++) try {
                u[i].get_styles().EmptyMessageStyle[0] = t ? Mandatory : NonMandatory;
                u[i].updateCssClass();
                $("#" + u[i].get_id()).data("ValidationInd", t)
            } catch (h) {}
            for (i = 0; i < o.length; i++) try {
                o[i].get_styles().EmptyMessageStyle[0] = t ? Mandatory : NonMandatory;
                o[i].updateCssClass();
                $("#" + o[i].get_id()).data("ValidationInd", t)
            } catch (h) {}
        }
    } catch (h) {}
}

function createXMLHttp() {
    var t, n;
    if (typeof XMLHttpRequest != "undefined") return new XMLHttpRequest;
    if (window.ActiveXObject)
        for (t = ["MSXML2.XMLHttp.5.0", "MSXML2.XMLHttp.4.0", "MSXML2.XMLHttp.3.0", "MSXML2.XMLHttp", "Microsoft.XMLHttp"], n = 0; n < t.length; n++) try {
            return new ActiveXObject(t[n])
        } catch (i) {}
    throw new Error("MSXML is not installed.");
}

function fnSetCoboItem(n, t, i) {
    var r = new Telerik.Web.UI.RadComboBoxItem;
    r.set_text(t);
    r.set_value(i);
    n.trackChanges();
    n.get_items().add(r);
    r.select();
    n.commitChanges()
}

function monthNameToNum(n) {
    var t = months.indexOf(n);
    return t ? t + 1 : 0
}

function fnContactNoValidation(n, t) {
    if (n != "-" && t == "H") {
        if (!/^(([0-9]{2,3})(-)([0-9]{6,8}))$/.test(n)) return "* Invalid Telephone Number format! eg:- 99-999999 OR 999-99999999"
    } else if (t == "M" && !/^(([0-9]{2,4})(-)([0-9]{7,8}))$/.test(n)) return "* Invalid Mobile Number format!";
    return ""
}

function OnClientTabSelecting(n, t) {
    var u = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        r = $("[id$=btnSubmit]")[0],
        f = document.getElementById(u.attributes.hdfTabPrevValue.value),
        i;
    if (n._selectedIndex == 0)
        if (r.value = "CovNext", fnValidateQuotationDtls(r, t)) t.set_cancel(!0);
        else {
            t.set_cancel(!0);
            return
        }
    else n._selectedIndex == 1 && t._tab._properties._data.pageViewID == "rpvPreview" && (i = fnAllowtoProceed(n, !1), i != "" ? (fnDisplayMainErrorMsg(n, i), t.set_cancel(!0), OpenPreviewTab()) : (fnChckAddonExtCover($("[id$=btnSubmit]")[0], "Y"), t.set_cancel(!0)));
    t._tab._properties._data.pageViewID == "rpvVechInsuredDtls" ? ($("#rtsMQ div ul li.rtsLI a").removeClass("rtsSelected"), $("#rtsMQ div ul li.rtsLI.rtsFirst a").addClass("rtsSelected"), $("#rtsMQ div ul li.rtsLI.rtsLast a").removeClass("rtsSelected"), $("[id$=trCoverageview]").css("display", "table-row"), $("[id$=trPremPreview]").css("display", "none"), $("[id$=btnSubmit]").hide(), "#hdfQuoStage" [0].value == "0" && $("[id$=btnPrintQuotation]").hide()) : t._tab._properties._data.pageViewID == "rpvPremium" && OpenPremiumTab();
    f.value = n._selectedIndex
}

function fnAllowtoProceed(n, t) {
    var i = "",
        r = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        u = document.getElementById(r.attributes.hdfISMValidInd.value),
        f = document.getElementById(r.attributes.hdfAllowInd.value),
        e = document.getElementById(r.attributes.hdfErrorMsg.value);
    return f.value == "0" ? i = "Please wait..." : u.value == "0" && t && (i = e.value), fnDisplayMainErrorMsg(n, i), i
}

function fnValidateQuotationDtls(n, t) {
    var i = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        cu = $find(i.attributes.rmpMQ_InusredDetails.value),
        lu = $find(i.attributes.rtsMQ.value),
        li = document.getElementById(i.attributes.hdfQuoStage.value),
        h = $find(i.attributes.ddlInsIndicator.value),
        f = "",
        at, nt, o, tt, er, or, yt;
    if (n.value == "CovNext") {
        var r = 0,
            ft = $find(i.attributes.ddlTransactionType.value),
            et = $("[id$=hdfTrnsType]").val(),
            g = $find(i.attributes.txtVehicleNo.value),
            ot = $find(i.attributes.txtPreVehicleNo.value),
            cr = document.getElementById(i.attributes.spnPrvVehicleNo.value),
            lr = document.getElementById(i.attributes.spnVehicleNo.value),
            pt = $find(i.attributes.rcFromPeriodofCover.value),
            wt = $find(i.attributes.rcToPeriodofCover.value),
            s = $find(i.attributes.ddlProductCode.value),
            bt = $find(i.attributes.ddlCoverType.value),
            kt = $find(i.attributes.ddlModel.value),
            st = $find(i.attributes.ddlCICode.value),
            dt = document.getElementById(i.attributes.txtYearofMake.value),
            ai = $find(i.attributes.txtCapacity.value),
            gt = $find(i.attributes.ddlUOM.value),
            ar = document.getElementById(i.attributes.spnChassisNo.value),
            ni = $find(i.attributes.txtChassisNo.value),
            au = $find(i.attributes.txtNoofPassenger.value),
            vr = $find(i.attributes.txtName.value),
            y = $find(i.attributes.txtOldIC.value),
            ht = document.getElementById("txtNewIC1"),
            ti = document.getElementById("txtNewIC2"),
            ii = document.getElementById("txtNewIC3"),
            l = $find(i.attributes.ddlDay.value),
            p = $find(i.attributes.ddlMonth.value),
            w = $find(i.attributes.ddlYear.value),
            yr = document.getElementById(i.attributes.lblDOBError.value),
            pr = $find(i.attributes.txtPostCode.value),
            ri = $find(i.attributes.ddlState.value),
            ui = $find(i.attributes.ddlCountry.value),
            wr = $find(i.attributes.txtAge.value),
            vi = $find(i.attributes.txtCmpRegNo.value),
            ct = $find(i.attributes.txtPhNo.value),
            b = $find(i.attributes.ddlMblPrefix.value),
            yi = document.getElementById(i.attributes.chckMobile.value),
            c = $find(i.attributes.txtEmgContactName.value),
            a = $find(i.attributes.txtEmgMobNo.value),
            fi = $find(i.attributes.ddlEmgMobNoPrefix.value),
            k = document.getElementById(i.attributes.chckEmgMobNo.value),
            vu = $find(i.attributes.ddlIsOwnedCar.value),
            yu = $find(i.attributes.ddlVOCYear.value),
            pu = $find(i.attributes.ddlVOCMonth.value),
            wu = $find(i.attributes.ddlIsModifiedCar.value),
            bu = $find(i.attributes.ddlModifiedPerform.value),
            ku = $find(i.attributes.ddlModifiedFunctional.value),
            br = document.getElementById(i.attributes.chckIsInsuredIC.value),
            pi = $find(i.attributes.txtStaffID.value),
            wi = $find(i.attributes.txtStaffIC.value),
            kr = document.getElementById(i.attributes.trStaffID.value),
            dr = document.getElementById(i.attributes.trStaffIC.value),
            gr = $find(i.attributes.ddlcostCenter.value),
            nu = document.getElementById(i.attributes.trCostCenter.value),
            tu = $find(i.attributes.rmpMQ_InusredDetails.value),
            du = tu.findPageViewByID("rpvPremium"),
            bi = ht.value + ti.value + ii.value,
            iu = pt.get_dateInput(),
            ru = wt.get_dateInput(),
            u = null,
            d = [],
            lt = $("[id$=ddlProductCode]").val().split("-")[0];
        if (d.ddlDay = l, d.ddlMonth = p, d.ddlYear = w, d.lblDOBError = yr, d.txtAge = wr, at = !1, li.value == "0" && (nt = document.getElementById(i.attributes.trThorHdr.value), nt != undefined && nt.style.display == "table-row" && (o = $find(i.attributes.ddlThARMCode.value), tt = $find(i.attributes.ddlThBussUnit.value), at = !0)), ft.get_value() == "" || ft.get_value() == "0" ? (u = ft.get_inputDomElement(), u.focus(), r = 1) : g._text == "" && lr.style.display == "inline-block" ? (g.focus(), r = 1, LoadTextBox(g)) : ot._text.trim() == "" && cr.style.display == "inline-block" ? (ot.focus(), r = 1) : s.get_value() == "" || s.get_value() == "0" ? (u = s.get_inputDomElement(), u.focus(), r = 1) : bt.get_value() == "" || bt.get_value() == "0" ? (u = bt.get_inputDomElement(), u.focus(), r = 1) : (st.get_value() == "" || st.get_value() == "0") && st._cachedText == "Select" ? (u = st.get_inputDomElement(), u.focus(), r = 1) : dt.value == "" ? (dt.focus(), r = 1) : kt.get_value() == "" || kt.get_value() == "0" ? (u = kt.get_inputDomElement(), u.focus(), r = 1) : ai._text == "" ? (ai.focus(), r = 1) : gt.get_value() == "" || gt.get_value() == "0" ? (u = gt.get_inputDomElement(), u.focus(), r = 1) : ni._text == "" && ar.style.display == "inline-block" && (lt != "MT01" || lt != "CT01") ? (ni.focus(), r = 1) : h.get_value() == "0" || h.get_value() == "" ? (u = h.get_inputDomElement(), u.focus(), r = 1) : vr._text == "" ? r = 1 : h.get_value() == "C" && vi._text == "" ? (vi.focus(), r = 1) : h.get_value() == "P" && (l.get_value() == "0" || l.get_value() == "" || p.get_value() == "0" || p.get_value() == "" || w.get_value() == "0" || w.get_value() == "") && (l.get_value() == "0" || l.get_value() == "" ? (u = l.get_inputDomElement(), u.focus()) : p.get_value() == "0" || p.get_value() == "" ? (u = p.get_inputDomElement(), u.focus()) : (w.get_value() == "0" || w.get_value() == "") && (u = w.get_inputDomElement(), u.focus()), r = 1), r != 0 || yi.checked || ct._text != "" && b.get_value() != "" && b.get_value() != "0" || (b.get_value() == "" || b.get_value() == "0" ? (u = b.get_inputDomElement(), u.focus()) : ct._text == "" && ct.focus(), r = 1), r == 0 && h.get_value() == "P" && y._text == "" && bi == "" ? (ht.focus(), f = "* Please enter New IC or Old IC No./ Others.", r = 2) : iu.get_value() == "" ? (pt.get_dateInput()._textBoxElement.style.backgroundColor = "#FFF284", pt.get_dateInput().focus(), r = 1) : ru.get_value() == "" ? (wt.get_dateInput()._textBoxElement.style.backgroundColor = "#FFF284", wt.get_dateInput().focus(), r = 1) : pr._text == "" ? r = 1 : ri.get_value() == "0" || ri.get_value() == "" ? (u = ri.get_inputDomElement(), u.focus(), r = 1) : ui.get_value() == "0" || ui.get_value() == "" ? (u = ui.get_inputDomElement(), u.focus(), r = 1) : kr.style.display == "table-row" && pi._text == "" ? (pi.focus(), r = 1) : br.checked || dr.style.display != "table-row" || wi._text != "" ? nu.style.display == "table-row" && gr._text == "" ? r = 1 : at && (o.get_value() == "0" || o.get_value() == "") ? (fnClearComboItems(o, t), u = o.get_inputDomElement(), u.focus(), r = 1) : at && (tt.get_value() == "0" || tt.get_value() == "") ? (u = o.get_inputDomElement(), u.focus(), f = "Unable to find the BizUnit for the ARM Code '" + o.get_value() + "', please select the valid ARM Code", fnClearComboItems(o, t), r = 2) : (g._text != "" && (f = fnValidateVehNo(g)), f == "" && ot._text != "" && (f = fnValidateVehNo(ot)), bi != "" && f == "" && (f = fnChckNewIC(ht.value, ti.value, ii.value, 0)), y._text != "" && fnValidateOldIC(y), f == "" && (f = fnYearofMake(dt, !0)), f == "" && (f = fnValidateChassisNo(ni, t)), f != "" || yi.checked || (f = fnContactNoValidation(b.get_value() + "-" + ct._text, "M")), f == "" && h.get_value() == "P" && (f = fnDOBValidation(l, d)), f != "" && (r = 2)) : (wi.focus(), r = 1), li.value == "1" && r == 0 && f == "") {
            var ki = document.getElementById(i.attributes.spnLogBookNo.value),
                di = $find(i.attributes.txtLogBookNo.value),
                uu = document.getElementById(i.attributes.spnEnginNo.value),
                gi = $find(i.attributes.txtEngineNo.value),
                nr = document.getElementById(i.attributes.trTrailerNo.value),
                ei = document.getElementById(i.attributes.trEmgContactName.value),
                tr = $find(i.attributes.txtTrailerNo.value),
                tr = $find(i.attributes.txtTrailerNo.value),
                ir = $find(i.attributes.txtTrailerChassisNo.value),
                e = $find(i.attributes.ddlRegionCode.value),
                oi = $find(i.attributes.ddlRegLocation.value),
                rr = $find(i.attributes.txtPurchaseCompany.value),
                fu = document.getElementById(i.attributes.trHirePurchase.value),
                it = $find(i.attributes.ddlGender.value),
                rt = $find(i.attributes.ddlMaritalSts.value),
                v = $find(i.attributes.ddlOccupation.value),
                ur = $find(i.attributes.txtAddress.value),
                eu = document.getElementById(i.attributes.lblLogBookError.value),
                si = $find(i.attributes.ddlMotorTrade.value),
                ou = document.getElementById(i.attributes.trMotorTrade.value),
                su = document.getElementById(i.attributes.txtOffPhNo.value),
                hu = document.getElementById(i.attributes.txtOffPrefix.value),
                hi = $find(i.attributes.ddlInsuredNationality.value),
                ut = !1,
                nt = document.getElementById(i.attributes.trThorHdr.value);
            if (nt != undefined && nt.style.display == "table-row") {
                var tt = $find(i.attributes.ddlThBussUnit.value),
                    ci = $find(i.attributes.ddlThCovType.value),
                    fr = $find(i.attributes.txtThCovTypeDesc.value),
                    o = $find(i.attributes.ddlThARMCode.value);
                ut = !0
            }
            if (eu.innerHTML = "", e.get_value() == "" || e.get_value() == "0" && e._text != "") {
                er = e._enabled;
                e.enable();
                or = e.get_items();
                or.clear();
                var vt = new Telerik.Web.UI.RadComboBoxItem,
                    sr = e._text,
                    hr = "W";
                sr == "E-EAST MALAYSIA" && (hr = "E");
                e.trackChanges();
                vt.set_text(sr);
                vt.set_value(hr);
                e.get_items().add(vt);
                vt.select();
                e.commitChanges();
                er ? e.enable() : e.disable();
                ComboIndexChanged(e, t)
            }
            ou.style.display == "table-row" && (si.get_value() == "" || si.get_value() == "0") ? (u = si.get_inputDomElement(), u.focus(), r = 1) : ki.style.display == "inline-block" && di._text == "" && ki.innerHTML == "*" ? (f = "* Please enter Log Book number.", di.focus(), r = 2) : gi._text == "" && uu.style.display == "inline-block" && (lt != "MT01" || lt != "CT01") ? (f = "* Please enter Engine number.", gi.focus(), r = 2) : nr.style.display == "table-row" && ir._text == "" ? (ir.focus(), r = 1) : nr.style.display == "table-row" && tr._text == "" && ft.get_value() != "V" ? (tr.focus(), r = 1) : e.get_value() == "" || e.get_value() == "0" ? (u = e.get_inputDomElement(), u.focus(), r = 1) : oi.get_value() == "" || oi.get_value() == "0" ? (u = oi.get_inputDomElement(), u.focus(), r = 1) : fu.style.display == "table-row" && rr._text == "" ? (rr.focus(), r = 1) : ht.value == "" && ti.value == "" && ii.value == "" && y._text != "" && hi.get_value() != "F" && v.get_value() != "10" && (f = "* Old IC Insured must be Foreign (Local/Foreign) or Uniformed Personnel (Occupation)!", u = hi.get_inputDomElement(), u.focus(), r = 2);
            hi.get_value() == "F" && v.get_value() != "10" && y._text == "" && h.get_value() == "P" ? (f = "* Old IC Number/Others Field Cannot Be Blank", y.focus(), r = 2) : h.get_value() == "P" && (it.get_value() == "0" || it.get_value() == "" || rt.get_value() == "0" || rt.get_value() == "" || v.get_value() == "0" || v.get_value() == "") && (it.get_value() == "0" || it.get_value() == "" ? (u = it.get_inputDomElement(), u.focus()) : rt.get_value() == "0" || rt.get_value() == "" ? (u = rt.get_inputDomElement(), u.focus()) : (v.get_value() == "0" || v.get_value() == "") && (u = v.get_inputDomElement(), u.focus()), r = 1);
            r == 0 && ur._text == "" ? (ur.focus(), r = 1) : k == null || c == null || ei.style.display != "table-row" || k.checked || c == null || c.get_value() != "" || c._textBoxElement.title != "true" || et == "R" && (s.get_value() == "PC01" || s.get_value() == "PC02") ? k == null || c == null || ei.style.display != "table-row" || k.checked || a == null || a.get_value() != "" || a._textBoxElement.title != "true" || et == "R" && (s.get_value() == "PC01" || s.get_value() == "PC02") ? k == null || c == null || ei.style.display != "table-row" || k.checked || a == null || a._textBoxElement.title != "true" || fi.get_value() != "0" && fi.get_value() != "" || et == "R" && (s.get_value() == "PC01" || s.get_value() == "PC02") ? ut && (ci.get_value() == "0" || ci.get_value() == "") ? (u = ci.get_inputDomElement(), u.focus(), r = 1) : ut && fr._text == "" ? (fr.focus(), r = 1) : ut && (o.get_value() == "0" || o.get_value() == "") ? (fnClearComboItems(o, t), u = o.get_inputDomElement(), u.focus(), r = 1) : ut && (tt.get_value() == "0" || tt.get_value() == "") ? (u = o.get_inputDomElement(), u.focus(), f = "Unable to find the BizUnit for the ARM Code '" + o.get_value() + "', please select the valid ARM Code", fnClearComboItems(o, t), r = 2) : (yt = "", yt = CheckNDValidation(!(et == "R" && (s.get_value() == "PC01" || s.get_value() == "PC02"))), yt != "" && (r = 2, f = yt), f == "" && (f = fnContactNoValidation(hu.value + "-" + su.value, "H"), f != "" && (r = 2))) : (u = fi.get_inputDomElement(), u.focus(), r = 1) : (a.set_value(), a.focus(), r = 1) : (c.set_value(), c.focus(), r = 1)
        }
        return r == 1 ? (fnDisplayMainErrorMsg(n, "* Please enter the mandatory field(s)."), !1) : r == 2 ? (fnDisplayMainErrorMsg(n, f), !1) : (sessionStorage.setItem("Error_Store", $("[id$=lblErrorMsg]").text()), fnDisplayMainErrorMsg(n, ""), window.scrollTo(0, 0), Concatenate(), !0)
    }
    return n._value == "Preview" ? (f = fnAllowtoProceed(n, !0), f == "" && (f = fnValidatePACInsuredDtls(n)), f != "" ? (fnDisplayMainErrorMsg(n, f), !1) : !0) : !0
}

function CheckNDValidation() {
    var n = "",
        t = !0;
    return $(".dummyclsdriver").each(function(i) {
        if (i != 0) {
            if ($(this)[0].childNodes[1].childNodes[0].value == "" || $(this)[0].childNodes[2].childNodes[0].value == "") return n == "" && (n += "* Named Driver,IC Number should not be empty"), t = !1, !1;
            if ($(this)[0].childNodes[2].childNodes[0].value == "-" || $(this)[0].childNodes[2].childNodes[0].value.toUpperCase() == "N.A" || $(this)[0].childNodes[2].childNodes[0].value.toUpperCase() == "NA" || $(this)[0].childNodes[2].childNodes[0].value.toUpperCase() == "N/A" || $(this)[0].childNodes[2].childNodes[0].value.toUpperCase() == "NULL" || $(this)[0].childNodes[2].childNodes[0].value.toUpperCase() == "NIL") return n == "" && (n += "* Invalid Driver IC."), t = !1, !1
        }
    }), t != !1 && $(".dummyclsdriver").each(function() {
        if ($(this)[0].childNodes[8].childNodes[0].checked == !1) n = "* Please choose atleast one Is Main Driver", t = !1;
        else return t = !0, !1
    }), t == !0 ? "" : n
}

function fnCoverTypeWarning() {
    var i = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        r = $find(i.attributes.ddlProductCode.value),
        n = $find(i.attributes.ddlCoverType.value),
        t = new Telerik.Web.UI.RadComboBoxItem;
    t.set_text("V-FT-MOTOR THIRD PARTY FIRE AND THEFT");
    t.set_value("V-FT");
    n.trackChanges();
    n.get_items().add(t);
    t.select();
    n.commitChanges();
    fnGetReconInd(r.get_value(), n.get_value())
}

function Mainmenu() {
    var n = GetAbsoluateURL();
    window.location.href = GetAbsoluateURL() + "Menu.aspx"
}

function ThCoverTypeIndexChanged() {
    return GetThCoverTypeDesc() ? !0 : !1
}

function GetThCoverTypeDesc() {
    var n = !1;
    return $.ajax({
        type: "POST",
        async: !1,
        contentType: "application/json; charset=utf-8",
        url: "../wsAutoComplete.asmx/fnGetThCoverTypeDesc",
        success: function(t) {
            fnBindCovType(JSON.parse(t.d));
            n = !0
        },
        error: function() {
            n = !1
        }
    }), n
}

function fnBindCovType(n) {
    var r = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        u = $find(r.attributes.ddlThCovType.value),
        i = document.getElementById(r.attributes.txtThCovTypeDesc.value),
        f, t;
    if (n != undefined || n != "")
        for (f = "", t = 0; t < n.length; t++) u._value == n[t].CoverTypeID && (i.value = n[t].DetailDesc);
    ComboIndexChanged(u);
    i.style.backgroundColor = "white";
    i.focus()
}

function ThARMCodeIndexChanged() {
    var n = !1;
    return $.ajax({
        type: "POST",
        async: !1,
        contentType: "application/json; charset=utf-8",
        url: "../wsAutoComplete.asmx/fnGetThARMCode",
        success: function(t) {
            fnBindARMCode(JSON.parse(t.d));
            n = !0
        },
        error: function() {
            n = !1
        }
    }), n
}

function fnBindARMCode(n) {
    var f = document.getElementById($("[id$=hdfControlIDs]")[0].id),
        e = $find(f.attributes.ddlThARMCode.value),
        u = $find(f.attributes.ddlThBussUnit.value),
        o, i, t, r;
    if (n != undefined || n != "")
        for (o = "", i = 0; i < n.length; i++) e._value == n[i].ARMCode && (t = n[i].BIZUnit, r = new Telerik.Web.UI.RadComboBoxItem, (t == null || t == "") && (t = "0"), r.set_text(t), r.set_value(t), u.get_items().add(r), r.select(), u.commitChanges());
    ComboIndexChanged(e);
    ComboIndexChanged(u)
}

function IsSQLAgent() {
    var n = !1;
    return $.ajax({
        type: "POST",
        async: !1,
        contentType: "application/json; charset=utf-8",
        url: "../wsAutoComplete.asmx/fnGetSQLAgent",
        success: function(t) {
            n = JSON.parse(t.d);
            n = n == !0 ? !0 : !1
        },
        error: function() {
            n = !1
        }
    }), n
}
var Mandatory = "font-size:12px;width:155px;background-color: #FFFFFF;",
    NonMandatory = "font-size:12px;width:155px;background-color: #FFFFFF;",
    href = window.location.href.split("/"),
    ChrCovFM = "|",
    jsonUISetting, IsNamedDriverValidate, filterUICtrlSetting, AbsURL = GetAbsoluateURL(),
    PricingPageURL = AbsURL + "CoverNote/NewCNote.aspx",
    Page_Cart_Amount, Page_Cart_Days, Page_KeyReplacement, Motor_Extra_Cover_Premium;
$(document).ready(function() {
    window.location.href.indexOf("?QN") > -1 || (HideControls(), GetUIControlSettings(null, null, null));
    var n = $("[id$=hdChkTimer]");
    n.val() == "Y" && addToPostBack(function() {
        return !0
    });
    $("[id$=hdfQuoStage]")[0].value == "1" && (jQuery.support.cors = !0, $("[id$=hdfIsLocalProxy]").val() == "Y" && fnGetIP())
});
addToPostBack = function(n) {
    var t = __doPostBack;
    __doPostBack = typeof __doPostBack != "function" ? n : function(i, r) {
        n(i, r) && t(i, r)
    }
};
