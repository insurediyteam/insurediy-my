<?php 

namespace Services\ZurichClient;

use Exception;
use Models\Eloquent\Make;
use Services\ZurichClient;
use Services\BaseClientSubclass;
use Models\Eloquent\MakeProvider;
use Services\Client\BaseListData;
use Services\Contracts\Client\Subclass\VehicleContract;

class Vehicle extends BaseClientSubclass implements VehicleContract {

    public function makeListData() {
        $self = &$this;

        return Make::get()
            ->map(function($make) use ($self) {
                try {
                    $response = $self->makeDataRequest($make->name);
                }
                catch (Exception $e) {
                    // skip

                    return null;
                }

                $data = json_decode((string) $response->getBody(), true);
                $data = $data['d']['Items'];

                if(count($data) == 1) {
                    return null;
                }

                $data = collect($data)->filter(function($item) use ($make) {
                    return $item['Text'] == $make->name;
                })->first();

                if(!$data) {
                    return null;
                }

                return new BaseListData($data['Value'], $make->name);
            })
            ->filter(function($make) {
                return $make !== null;
            })
            ->toArray();
    }

    public function modelListData($makeCode, $type = null, $cover = null, $use = null) {
        $makeProvider = MakeProvider::whereCode($makeCode)->first();

        $yearNow = date('Y');
        $years = range($yearNow, ($yearNow - 20), -1);

        $allDatas = collect([]);

        foreach ($years as $year) {
            $response = $this->modelDataRequest($makeCode, $makeProvider->make->name, $year);
    
            $datas = json_decode((string) $response->getBody(), true);
            $datas = $datas['d']['Items'];

            $allDatas = $allDatas->concat($datas);
        }

        $allDatas = $allDatas->unique()->values()->toArray();

        return array_map(function($data) {
            $name = str_replace('-' . $data['Value'], '', $data['Text']);
            $name = trim(preg_replace('/\s\s+/', ' ', $name));

            return new BaseListData($data['Value'], $name, [
                [
                    'value' => $data['Attributes']['BodyType'],
                    'description' => 'Body Type',
                ],
                [
                    'value' => $data['Attributes']['Capacity'],
                    'description' => 'Capacity',
                ],
                [
                    'value' => $data['Attributes']['ISM'],
                    'description' => 'ISM',
                ],
                [
                    'value' => $data['Attributes']['Recon'],
                    'description' => 'Recon',
                ],
            ]);
        }, $allDatas);
    }

    public function bodyListData() {
        $provider = $this->getModel();

        return $provider->models
            ->map(function($model) {
                return $model->detailables()->whereDescription('Body Type')->first()->value;
            })
            ->unique()
            ->values()
            ->map(function($bodyType) {
                return new BaseListData($bodyType, $bodyType);
            })
            ->toArray();
    }

    public function renewalDtls($vehicleNo) {
        /** @var ZurichClient $this */
        $response = $this->getRenewalDtlsRequest([
            'VNo' => $vehicleNo,
        ]);

        $data = (string) $response->getBody();
        return $this->client->parser->renewalDtls($data);
    }

}
