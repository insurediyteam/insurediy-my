<?php 

namespace Services\ZurichClient;

use Services\BaseClientSubclass;
use Services\Client\BaseListData;

class Occupation extends BaseClientSubclass {

    public function listData() {
        $occupationDataRequest = $this->occupationRequest();

        $data = (string) $occupationDataRequest->getBody();
        
        $data = json_decode($data, true);

        $data = array_map(function($value) {
            return new BaseListData($value['sdfOccupID'], $value['Description']);
        }, json_decode($data['d'], true));

        return $data;
    }

}
