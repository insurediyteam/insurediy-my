<?php 

namespace Services\ZurichClient;

use Exception;

class ZurichClientException extends Exception {

    public static function ismMarketValueFailed() {
        return new static("Failed to get ISM Market Value!");
    }

}
