<?php 

namespace Services\ZurichClient;

use Concerns\ParserTrait;
use Services\ZurichClient;
use Services\BaseClientSubclass;
use Services\Client\ComparisonTableData;

class Parser extends BaseClientSubclass {

    use ParserTrait;

    /**
     * Parse quotation html data to array
     *
     * @param string $htmlString
     * @return array
     */
    public function quotationData($responseBody) {
        $data = json_decode($responseBody, true);
        $data = explode('ý', $data['d']);
        
        $data = json_decode($data[0], true);
        $motorExtraCoverPremium = collect($data['Motor_Extra_Cover_Premium'])
            ->whereIn('Ext_Cvr_Code', array_values(Generator::$extraCoverageCodes))
            ->toArray();
        $extraCoverageCodesFlipped = array_flip(Generator::$extraCoverageCodes);
        foreach ($motorExtraCoverPremium as $key => $value) {
            $amount = intval($value['Ext_Cvr_Premium_Displayed']);
            
            if($amount) {
                $nameByCode = $extraCoverageCodesFlipped[$value['Ext_Cvr_Code']];
                /** @var ZurichClient $this */
                $this->setExtraCoverageSumInsured($nameByCode, $amount);
            }
        }

        $motorData = $data['Motor'][0];

        $this->setQuotationValue($motorData["TtlPayablePremium"]);

        $this->addCoverTypeAndExcesses(
            new ComparisonTableData(
                ComparisonTableData::GROUP_OWN_DAMAGE_EXCESS[0],
                ComparisonTableData::GROUP_OWN_DAMAGE_EXCESS[1],
                ComparisonTableData::valueWithIcon($motorData['TotExcessAmt'], true)
            )
        );

        return [
            [
                'description' => 'Basic Premium',
                'value' => $motorData["Basic_Premium"],
            ],
            [
                'description' => 'NCD',
                'value' => 0 - $motorData["NCDAmt"],
            ], 
            [
                'description' => 'Gross Premium',
                'value' => $motorData["Gross_Premium"],
            ], 
            [
                'description' => 'GST Premium Percentage',
                'value' => $motorData["GST_Per"],
            ], 
            [
                'description' => 'GST Premium',
                'value' => $motorData["PrmServicTaxAmt"],
            ], 
            [
                'description' => 'Stamp Duty',
                'value' => $motorData["PrmStampDutyAmt"],
            ], 
            [
                'description' => 'Total Premium',
                'value' => $motorData["TtlMotorPremium"],
            ], 
            [
                'description' => 'Total Payable Premium',
                'value' => $motorData["TtlPayablePremium"],
            ], 
        ];
    }

    /**
     * Parse quotation html data to array
     *
     * @param string $htmlString
     * @return array
     */
    public function quotationDataPreview($inputs, $quotationData, $responseBody) {
        $NCD = $inputs['ncd'];

        $data = json_decode($responseBody, true);
        $data = json_decode($data['d'], true);
        
        $quotationData = collect($quotationData);
        $totalPremium = \MyHelper::unformatCurrency(
            $quotationData->where('description', "TtlPayablePremium")->first()['value']
        );
        
        $payablePremiums = collect($data['tblPayablePremium']);
        $totalPremiumByNCD = $payablePremiums->where('NCDValue', $NCD)->first()['PayablePremium'];

        $this->setQuotationValue($totalPremiumByNCD);

        $previewData = $data['tblPreviewQuoDtls'][0];

        return [
            [
                'description' => 'Stamp Duty',
                'value' => $previewData["StampDuty"],
            ],
            [
                'description' => 'Total Payable Premium',
                'value' => $totalPremiumByNCD,
            ], 
        ];
    }

    public function renewalDtls($responseBody) {
        $data = json_decode($responseBody, true);
        $data = $data['d'];
        $data = explode('û', $data);

        $output = array_combine([
            'unknownCode1',
            'vehModelAndCap',
            'vehUOM',
            'seats',
            'hdfVehBodyID',
            'windscreenSI',
            'vehBodyType',
            'vehModelId',
            'vehModelName',
            'vehMakeId',
            'vehRenewalData',
        ], $data);

        return $output;
    }

    public function ismMarketValue($responseBody) {
        $data = json_decode($responseBody, true);
        $data = $data['d'];
        $data = json_decode($data, true);
        $data = $data[0];

        return $data;
    }

    public function ncdReply(\GuzzleHttp\Psr7\Response $response) {
        $data = json_decode((string) $response->getBody(), true);
        $data = $data['d'];
        $data = json_decode($data, true);
        $data = $data[0];

        return array_merge($data, [
            'arrayNCDRTN' => explode('|', $data['NCDRTN']),
        ]);
    }

}
