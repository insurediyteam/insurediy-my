<?php 

namespace Services\ZurichClient;

use Carbon\Carbon;
use Concerns\IcData;
use Services\BaseClient;
use Illuminate\Support\Str;
use Concerns\GeneratorTrait;
use Services\BaseClientSubclass;
use Services\Client\OptionalArray;

class Generator extends BaseClientSubclass {

    use GeneratorTrait;

    const DATA_DELIMITER = '±';
    const DATA_DELIMITER_2 = 'þ';

    public static $extraCoverageCodes = [
        'cart' =>  '112',
        'specialPeril' => '57',
        'liabilityPass' => '02',
        'ehailing' => '221',
        'strike' => '25',
        'waiver' => '207',
        'laminated' => '89A',
        'tinting' => '89',
    ];

    public $viewState;
    public $mstData = '';
    public $schData = '';
    public $claimDtls = '0';
    public $hdfEXLTempPassin = '';
    public $hdfExlSpecialInd = '';
    public $hdfWarn = '';
    public $hdfTempISM = '';
    public $tempPassInValues = '';
    public $hdfNCDReply = '';
    public $hdfMstQuoDtls = '{"tblExtraCoverageDtls":[]}';
    public $hdfTempMV = '';
    public $hdfNCDRefNo = '';
    public $hdfNCDValue = '0.00þ0';
    public $hdfISMNCD = '0';
    public $renewalDtls;
    public $ismMarketValue;
    public $extraCoverList = [
        '01Aþ0þ20.00þþþþþþþDEFþY',
    ];
    public $policyStartDate;
    public $policyEndDate;
    public $debug = false;

    public function __construct(BaseClient $client)
    {
        parent::__construct($client);

        $this->policyStartDate = Carbon::today();
        $this->policyEndDate = $this->policyStartDate->copy()
            ->addYears(1)
            ->subDays(1);
    }

    public function getViewState() {
        if(!isset($this->viewState)) {
            $response = $this->quotationMenuRequest();
            $menuDom = $this->htmlToDom((string) $response->getBody());
            $this->viewState = $menuDom->filter('#__VIEWSTATE')->first()->attr('value');
        }

        return $this->viewState;
    }

    public function getData($overrides = []) {
        $policyStartDate = Carbon::today()->format('d-M-Y');

        $ic = new IcData(isset($overrides['ic']) ? $overrides['ic'] : '811228015284');

        $defaults = [
            'vehicleRegNo' => 'JQR3339',
            'policyStartDate' => $this->policyStartDate->format('d-M-Y'),
            'policyEndDate' => $this->policyEndDate->format('d-M-Y'),
            'makeYear' => '2014',
            'capacity' => '1490',
            'chassis' => 'PPPHZC82SDP107049',
            'clientName' => 'LAI HUEY VOON',
            'ic' => $ic->number,
            'engine' => 'K14BS123520',
            'ncd' => '0.00',
            'dob' => $ic->dob->format('d/m/Y'),
            'gender' => 'F',
            'postCode' => '81800',
            'make' => '28',
            'model' => '28*1007',
            'bodyType' => 'HATCHBACK',
            'phoneNumber' => '',
            'phoneCode' => '014',
            'sumInsured' => '0',
            'productCode' => 'PZ01',
            'coverType' => 'V-CO',
            'seats' => '5',
            'driverCount' => '1',
            'allDriver' => '1',
            'state' => '07',
            'town' => 'ULU TIRAM',
            'country' => 'MAS',
            'region' => 'W',
        ];
        
        $merged = $this->mergeAndOverride($defaults, $overrides, true);

        return $merged;
    }

    public function updatePremData($data) {
        $data = new OptionalArray($data);
        $data->setDefaultReturn('0');
        
        $dateOfBirth = Carbon::createFromFormat('d/m/Y', $data['dob'] ? $data['dob'] : '1981-12-28');
        
        // convert to right format
        $data['sumInsured'] = (int) $data['sumInsured'];
        $sumInsuredFmt = number_format($data['sumInsured'], 0);

        $ic = new IcData($data['ic']);

        $renewalDtls = $this->oneTimeRenewalDtls($data['vehicleRegNo']);
        $ismMarketValue = $this->hdfTempISM ? $this->oneTimeIsmMarketValue($data) : null;

        $mstData = [
            'BR',
            'B',
            $data['vehicleRegNo'],
            '',
            $data['policyStartDate'],
            $data['policyEndDate'],
            $data['productCode'],
            $data['coverType'],
            $data['model'],
            'MX1',
            $data['makeYear'],
            $data['capacity'],
            'CC',
            $data['chassis'],
            $data['seats'],
            $data['clientName'],
            '', // old ic
            $data['ic'],
            $data['state'], // ddl state
            $data['country'], // country
            $dateOfBirth->age, //age
            // '',
            $data['phoneCode'],
            $data['phoneNumber'],
            '', //ddl mbl prefix
            '', // emg contact name
            '', // emg mob no
            '', // mob no prefix
            '', // is owned car
            '', // VOC year
            '', // VOC month
            '', // is mod car
            '', // modified perf
            '', // functional mod
            '', // staff id
            '', // staff IC
            '', // ddlcostcenter
            '', // logbookno
            $data['engine'],  // engine no
            '',  // trailerno
            '',  //txtTrailerChassisNo
            $data['region'], // ddlRegionCode
            'L', // ddlRegLocation
            '',  // txtPurchaseCompany
            $data['gender'], //ddlGender
            '', // ddlMaritalSts
            '', // ddlOccupation
            '', // txtAddress
            '', // ddlMotorTrade
            '',  // txtOffPhNo
            '', // txtOffPrefix
            $data['driverCount'], // ddlNoofNamedDrivers
            'L', // ddlInsuredNationality
            '', // #hdnNamedDriver
            '', // #hdfOldPolNo
            'N', // #hdfRenewalSameAgentInd
            '', // txtRelaMemNo
            'N', // #chkSighted
            'N', // #chckRecon
            '0', // chckIsInsuredIC
            '0', // dblPrevSumIns
            '0.00', // txtPrevSI
            str_replace(Generator::DATA_DELIMITER, Generator::DATA_DELIMITER_2, $renewalDtls['hdfVehBodyID']), // hdfVehBodyID
            $this->hdfEXLTempPassin, // hdfEXLTempPassin
            $this->hdfExlSpecialInd, // hdfExlSpecialInd
            $this->hdfWarn, // hdfWarn
            $this->hdfTempISM, // hdfTempISM
            'P', // ddlInsIndicator
            '1', // hdfAllowInd
            $this->hdfNCDReply, // hdfNCDReply
            '0', // hdfQuoStage
            $ic->split->first(), // txtNewIC1
            $ic->split->second(), // txtNewIC2
            $ic->split->third(), // txtNewIC3
            $dateOfBirth->format('d'), // ddlDay
            $dateOfBirth->format('M'), // ddlMonth
            $dateOfBirth->format('Y'), // ddlYear
            $data['postCode'], // txtPostCode
            $data['town'], // txtTown
            $this->tempPassInValues, // TempPassInValues
            '[{"AgentCode":"D15022-000","ECMIS_AgentName":"INSUREDIY SDN BHD","ECMIS_RegionCode":"W","ECMIS_Branch":"B01","ECMIS_RegionDesc":"WEST MALAYSIA","ECMIS_Address":"UNIT NO. 17-2, LEVEL 17 WISMA UOA II NO. 21, JALAN PINANG 50450 KUALA LUMPUR","ECMIS_TelNo":"03-21490997 €","BusinessSource":"8","ECMIS_RegInd":"N","ECMIS_MstPolInd":"N","FranchiseCode":""}]', // hdfAgentDtls
            'N', // hdfTrailerInd
            'N', // id$=chckMobile
            'N', // chckEmgMobNo
            '', // hdfIPAddrs
            'undefined', // hfSightedICInd
            $data['bodyType'],
            '', // id$=lblErrorMsg
            '', // ''
            '', // ddlThCovType
            '', // txtThCovTypeDesc
            '', // ddlThARMCode
            '1', //$data['allDriver'], // id$=chkAllDrv [1|0]
        ];
        
        $this->mstData = implode(static::DATA_DELIMITER, $mstData);
        // if(static::$debug) {
        //     echo ($this->hdfTempISM);exit;
        //     echo ($this->mstData);exit;
        // }

        $extraCoverCodes = implode(static::DATA_DELIMITER, [
            '[]',
            '01A',
        ]);
        
        $prmInsured = $data['sumInsured'];
        if(!$prmInsured && $ismMarketValue) {
            $prmInsured = $ismMarketValue['SumInsured'];
        }
        $prmInsuredFmt = number_format($prmInsured, 0);

        $this->schData = implode(static::DATA_DELIMITER, [
            $prmInsuredFmt, //txtPrmInsured /0
            '0', // 0
            'No', // txtAllRiderInd
            'No', // #ddlAgreeValue option:selected
            '', // txtCN_NCDPer
            '0', #ddlVoluntaryExcess option:selected
            '1', // #ddlPACUnit option:selected
            'Y', // #ddlPAC option:selected
            '', // #lblCurAgreeValue
            '', //#hdnPacType
            'undefined', // #ddlNCDPer option:selected
            '', // 
            'Y', // [id$=ddlABIInd]
            '0', //hdfSumInsChangInd
            $ismMarketValue ? $ismMarketValue['SumInsured'] : '0', // dblABISumIns
            $ismMarketValue ? $ismMarketValue['OrgABISI'] : '0', // dblABIOrgSumIns
            'N', // rbAvSI
            'Y', // rbRecSI
            'TAGPLUS.PAC', // [id$=lblPACDsc] innerHTML null|TAGPLUS.PAC
            '', // hdfReQuoteInd
            '0|0|0', // hdfOptionalInd
            $renewalDtls['windscreenSI'], // hdfWScreenSI windscreen suminsured 2000.00
            '', // hdfErrorMsg
            '1', // hdfISMValidInd
            '1', // hdfTabPrevValue
            '0',
            '0',
            '0',
            '0',
            '0',
            '0.00', //[id$=txtCN_NCDPer] textcontext 0.00
            '0.00', // $("[id$=txtCN_DsNCDAmt]")[0].textContent 0.00
            '0.00', // $("#txtDsBasicPremium").text() 336.05
            '0.00', // $("#txtDSLoadingPer").text() 0.00
            '0.00', // $("#txtDsTuitionLoadPer").text() 0.00
            '0.00', // $("#txtDSLoadingAmt")
            '0.00', // $("#txtDsTuitionLoadAmt").text() 0.00
            '0.00', // $("#txtAllRider").text() 0.00
            '0.00', // $("#txtPrmServicTaxAmt").text() 39.51
            '0.00', // $("#txtPrmStampDutyAmt").text() 10.00
            '0.00', // $("#txtPrmStampDutyAmt").text() 10.00
            '0.00', // $("#txtExcessAmt").text() 0.00
            '0.00', // $("#hdfActPrem").val() 133.65
            '', // $("#txtTtlCoveragePremium").text() null
            '0.00', // $("#hdfServiceTax").val() 6.00
            $ismMarketValue ? $ismMarketValue['CurrentMarketValue'] : (isset($data['sumInsured']) ? $data['sumInsured'] : '0'), // $("#dblABIMktValue").val() 0
            '0', // $("#dblPrevSumIns").val() 0
            '0.00', // $("#txtPrmRebatePer").text() 0.00
            '0.00', // $("#txtPrmRebateAmt").text() 0.00
            '0.00', // $("#lblVoluntaryExcessPrem").text() 0
            '0.00', // $("#lblRenewalBonus").text() 0.00
            '0.00', // $("#lblTotalExcess").text() 0.00
            '', // $("#lblPrevSI").text() null
            $ismMarketValue ? number_format($ismMarketValue['AVSI_Value'], 2) : '0.00', // $("#lblAvSI").text() 0.00
            $ismMarketValue ? number_format($ismMarketValue['OrgABISI'], 0) : '0.00', // $("#lblRecSI").text() 0.00
            '0.00', // $("#lblAdjustmentPct").text() 10.00
            '', // $("#lblAdjustmentAmt").text() null
            '5', // document.getElementById("txtPersonalNoPass").value 5
            '250,000.00', // $("#txtPersonalSumInsured").text() 250,000.00
            '0.00', // $("#txtDsPersonalPremium").text() 72.00
            '0.00', // $("#txtDsPersonalAddPremium").text() 0.00
            '0.00', // $("#txtDsPersonalServiceTax").text() 4.32
            '0.00', // $("#txtDsPersonalStampDuty").text() 10.00
            '0.00', // $("#txtDsPACExtraCover").text() 0.00
            '0.00', // $("#txtPACRebatePer").text() 0.00
            '0.00', // $("#txtPACRebateAmt").text() 0.00
            'Y', // $("#lblPrevSI").context.body.disabled == !1 ? "Y" : "N" // Y
            ((int) $this->policyStartDate->format('d')) . '-' . $this->policyStartDate->format('M') . '-' . substr($this->policyStartDate->format('Y'), 2, 2), // $("#hdfQuoValExpDate").val() 6-Jan-20
            '', // $("#hdfDirectInd").val() null
            'N', // $("ddlAgreeValue").context.body.disabled = "N" // N
            'Y', // document.getElementById(t.attributes.txtPrmInsured.value).disabled == !1 ? "Y" : "N" // Y
            'N', // document.getElementById(t.attributes.txtPrevSI.value).disabled == !1 ? "Y" : "N" // Y
            $this->hdfTempMV, // $("#hdfTempMV").val() // PZ01|V-CO|JQR3339|2014|28*1007|06 Jan 2020|N
            '1', // $("#hdfNCDValidInd").val() 1
            '1', // $("#hdfISMMV").val() 1
            $this->hdfISMNCD, // $("#hdfISMNCD").val() 0
            $this->hdfNCDValue, //'0.00þ0', // $("#hdfNCDValue").val().replace(/Â±/g, "Ã¾") 0.00þ0
            '1', // $("#hdfISMMrkValueInd").val() 1
            '', // $find(t.attributes.ddlLoanType.value).get_value() null
            '0', // $("[id$=hdnMtrComm]")[0].value 0
            '0', // $("[id$=hdnPACComm]")[0].value 0
            '01Aþ0þ20.00þþþþþþþDEFþY', // $("#hdfExtCover_dt")[0].defaultValue 72þ0þ7.50þþþþþþþDEFþN
            '', // $("#hdfPACExtCover_dt")[0].defaultValue 89Aþ2,000.00þ300.00þþþþþþþSUMþN
            '', // $("[id$=hdfPACRiderDtls]")[0].value null
            $extraCoverCodes,
        ]);
    }

    public function step1QuotationData(array $overrides = []) {
        return [
            'RadStyleSheetManager1_TSSM' => ';Telerik.Web.UI, Version=2019.2.514.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4:en-US:c6f7a274-ce18-4c5a-9edc-a9c0d704746c:ed2942d4:45085116;Telerik.Web.UI.Skins, Version=2019.2.514.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4:en-US:2afae1e7-f50c-4dbb-88d2-fe5bc1b491b4:2219f022:53cda90b;Telerik.Web.UI, Version=2019.2.514.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4:en-US:c6f7a274-ce18-4c5a-9edc-a9c0d704746c:ed2942d4:45085116;Telerik.Web.UI.Skins, Version=2019.2.514.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4:en-US:2afae1e7-f50c-4dbb-88d2-fe5bc1b491b4:2219f022:53cda90b',
            'radScriptMgr_TSM' => ';;AjaxControlToolkit, Version=4.1.51116.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e:en-US:fd384f95-1b49-47cf-9b47-2fa2a921a36a:ea597d4b:b25378d2;Telerik.Web.UI, Version=2019.2.514.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4:en-US:c6f7a274-ce18-4c5a-9edc-a9c0d704746c:16e4e7cd:33715776:f7645509:24ee1bba:e330518b:2003d0b8:c128760b:1e771326:88144a7a:c8618e41:1a73651d:333f8d94:ed16cbdc',
            'ctl00_rdMenu_ClientState' => '',
            'ctl00$MainContent$hdfAllowInd' => 0,
            '__EVENTTARGET' => 'ctl00$MainContent$lnkRVQuote',
            '__EVENTARGUMENT' => '',
            '__VIEWSTATE' => $this->getViewState(),
            '__VIEWSTATEGENERATOR' => 'AEC19854',
            '__PREVIOUSPAGE' => '9FI4mUL4octRR5rcNdTkstfF41DHi5tkOUlf2ii8tt_GxKFrAS48Af7QxyT2WhpDy5N4HFt2Yw_TuvVwPztN7aSuhVM1',
        ];
    }

    public function mstSavePayload($overrides = []) {
        $this->updatePremData($this->getData($overrides));

        return [
            'ClaimDtls' => $this->claimDtls,
            'EventName' => "MstSave",
            'ExtraCover' => "",
            'MstData' => $this->mstData,
            'PacExtraCover' => '',
            'QuoCovNo' => '',
            'SchData' => $this->schData,
            'hdfMstQuoDtls' => '{"tblExtraCoverageDtls":[]}',
            'MstRider' => "[]",
            'BtnType' => "CovNext",
        ];
    }

    /**
     * Generate template / defaults data for axa quotation
     *
     * @param array $overrides
     * @return array
     */
    public function step2QuotationData(array $overrides = []) {
        $this->updatePremData($this->getData($overrides));
        return [
            'ClaimDtls' => $this->claimDtls,
            'EventName' => "Initial_Prem",
            'ExtraCover' => "",
            'MstData' => $this->mstData,
            'PacExtraCover' => '',
            'QuoCovNo' => '',
            'SchData' => $this->schData,
            'hdfMstQuoDtls' => $this->hdfMstQuoDtls,
        ];
    }
    
    public function extCoverControlChange(array $overrides = []) {
        $this->updatePremData($this->getData($overrides));

        // $this->mstData = 'BR±B±WPX223±±09-Mar-2020±08-Mar-2021±PZ01±V-CO±29*0101±MX1±2007±2362±CC±MR053BK4007011468±5±TAN BOON KIN±±890201105587±05±MAS±31±±60141231±014±±±±±±±±±±±±±±2AZE035203±±±W±L±±M±±±±±±±1±L±±±N±±N±N±0±0±0.00±SLNþ29*04þ004þ006±WPX223þV-COþ2AZE035203þ09-Mar-2020þPZ01þ890201-10-5587þþþþþTAN BOON KIN±Nþ0þNþSSTþYþVPC01þNþþNþþNþþ0þ0þ0þ0±0±WPX223|WPX223|MR053BK4007011468|PZ01|V-CO|MX1|890201-10-5587|09-Mar-2020±P±1±0.00%|27 Jul 2019|26 Jul 2020|55.000000%|27 Jul 2020|WPX223|||PZ01|MR053BK4007011468|WPX223|MX1||WSNCDENQ20000537475|ENQ000|E002,E004,E006|1,1,26072020|0|5|45±0±890201±10±5587±01±Feb±1989±47100±PUCHONG±WPX223|B|P|PZ01|V-CO|09-Mar-2020|08-Mar-2021|29*0101|L|5|2362|CC|2007||MX1|2AZE035203|MR053BK4007011468||W|TAN BOON KIN||890201|10|5587||01/02/1989|31|M|||1|47100|05|MAS||L|N±[{"AgentCode":"D15022-000","ECMIS_AgentName":"INSUREDIY SDN BHD","ECMIS_RegionCode":"W","ECMIS_Branch":"B01","ECMIS_RegionDesc":"WEST MALAYSIA","ECMIS_Address":"UNIT NO. 17-2, LEVEL 17 WISMA UOA II NO. 21, JALAN PINANG 50450 KUALA LUMPUR","ECMIS_TelNo":"03-21490997 €","BusinessSource":"8","ECMIS_RegInd":"N","ECMIS_MstPolInd":"N","FranchiseCode":""}]±N±N±N±±undefined±SALOON±±±±±±1';
        // $this->schData = '75,000±0±No±No±±0±1±Y±±±undefined±±Y±0±65000±54000±N±Y±TAGPLUS.PAC±±0|0|0±2000.00±±1±1±0±0±0±0±0±0.00±0.00±2,452.45±0.00±0.00±0.00±0.00±0.00±157.35±10.00±0.00±490.49±2,876.12±±6.00±53600±0±0.00±0.00±0.00±0.00±0.00±±65,000±54,000±10.00±±5±250,000.00±72.00±0.00±4.32±10.00±0.00±0.00±0.00±Y±12-Mar-20±±N±Y±N±PZ01|V-CO|VCD1883|2018|79*1084|12 Mar 2020|N±1±1±1±±1±±0±0±01Aþ0þ20.00þþþþþþþDEFþY±±±[]±57±01A';
        
        $payload = [
            'ClaimDtls' => $this->claimDtls,
            'EventName' => "Motor_ExtCover_Control_Change",
            'ExtraCover' => implode(static::DATA_DELIMITER, $this->extraCoverList),
            'MstData' => $this->mstData,
            'PacExtraCover' => '',
            'QuoCovNo' => '',
            'SchData' => $this->schData,
            'hdfMstQuoDtls' => $this->hdfMstQuoDtls,
            'MstRider' => '[]',
            'BtnType' => 'Motor_ExtCover_Control_Change',
        ];

        // print_r($payload);exit;

        return $payload;
    }
    
    public function previewCoverNote(array $overrides = []) {
        $this->updatePremData($this->getData($overrides));
        // echo ($this->schData);exit;
        // echo ($this->mstData);exit;

        // $this->mstData = 'BR±B±WPS5133±±04-Mar-2020±03-Mar-2021±PZ01±V-CO±03*1040±MX1±2006±1995±CC±WBAPB120X0WC23610±5±HASMAHH BINTI AHMAD±±580413075750±05±MAS±61±±60142315±014±±±±±±±±±±±±±±91495878±±±W±L±±F±±±±±±±1±L±±±N±±N±N±0±0±0.00±STWþ03*34þ003þ017±WPS5133þV-COþ91495878þ04-Mar-2020þPZ01þ580413-07-5750þþþþþHASMAHH BINTI AHMAD±Nþ0þNþSSTþYþVPC01þNþþNþþNþþ0þ0þ0þ0±0±WPS5133|WPS5133|WBAPB120X0WC23610|PZ01|V-CO|MX1|580413-07-5750|04-Mar-2020±P±1±0.00%|29 Aug 2019|28 Aug 2020|25.000000%|29 Aug 2020|WPS5133|||PZ01|WBAPB120X0WC23610|WPS5133|MX1||WSNCDENQ20000499380|ENQ000|E002,E003|2,2|0|1|00±0±580413±07±5750±13±Apr±1958±47600±SUBANG JAYA±WPS5133|B|P|PZ01|V-CO|04-Mar-2020|03-Mar-2021|03*1040|L|5|1995|CC|2006||MX1|91495878|WBAPB120X0WC23610||W|HASMAHH BINTI AHMAD||580413|07|5750||13/04/1958|61|F|||1|47600|05|MAS||L|N±[{"AgentCode":"D15022-000","ECMIS_AgentName":"INSUREDIY SDN BHD","ECMIS_RegionCode":"W","ECMIS_Branch":"B01","ECMIS_RegionDesc":"WEST MALAYSIA","ECMIS_Address":"UNIT NO. 17-2, LEVEL 17 WISMA UOA II NO. 21, JALAN PINANG 50450 KUALA LUMPUR","ECMIS_TelNo":"03-21490997 €","BusinessSource":"8","ECMIS_RegInd":"N","ECMIS_MstPolInd":"N","FranchiseCode":""}]±N±N±N±±undefined±STATIONWAGON±±±±±±1';
        // $this->schData = '35,000±0±No±No±±0±1±Y±±±undefined±±Y±0±35000±35000±N±Y±TAGPLUS.PAC±±0|0|0±4500.00±±1±1±0±0±0±0±0±0.00±0.00±1,494.14±0.00±0.00±0.00±0.00±0.00±90.85±10.00±350.00±298.83±1,701.31±±6.00±34700±0±0.00±0.00±0.00±0.00±350.00±±0.00±35,000±22.16±±5±250,000.00±72.00±0.00±4.32±10.00±0.00±0.00±0.00±Y±4-Mar-20±±N±Y±N±PZ01|V-CO|WPS5133|2006|03*1040|04 Mar 2020|N±1±1±0±0.00þ0±1±±0±0±01Aþ0þ20.00þþþþþþþDEFþY±±±[]±01A';
        return [
            'ClaimDtls' => $this->claimDtls,
            'EventName' => "Preview",
            'ExtraCover' => implode(static::DATA_DELIMITER, $this->extraCoverList),
            'MstData' => $this->mstData,
            'PacExtraCover' => '',
            'QuoCovNo' => '',
            'SchData' => $this->schData,
            'hdfMstQuoDtls' => $this->hdfMstQuoDtls,
            'MstRider' => '[]',
            'BtnType' => 'Preview',
        ];
    }

    public function mstDataWithKeys($mstData) {
        if(!is_array($mstData)) {
            $mstData = explode(static::DATA_DELIMITER, $mstData);
        }

        $keys = [
            'transactionType',
            'transType',
            'vehicleRegNo',
            'oldVehicleRegNo',
            'policyStartDate',
            'policyEndDate',
            'productCode',
            'coverType',
            'model',
            'ddlCICode',
            'makeYear',
            'capacity',
            'ddlUOM',
            'chassis',
            'seats',
            'clientName',
            'oldIc', // old ic
            'ic',
            'ddlState', // ddl state
            'country', // country
            'age', //age
            'unknown1',
            'phoneNumber',
            'phoneCode',
            'dlMblPrefix',
            'emgContactName',
            'emgMobNo',
            'mob no prefix',
            'is owned car',
            'VOC year',
            'VOC month',
            'is mod car',
            'modified perf',
            'functional mod',
            'staff id',
            'staff IC',
            'logbookno',
            'engine no',
            'trailerno',
            'xtTrailerChassisNo',
            'ddlRegionCode',
            'ddlRegLocation',
            'txtPurchaseCompany',
            'dlGender',
            'ddlMaritalSts',
            'ddlOccupation',
            'txtAddress',
            'ddlMotorTrade',
            'txtOffPhNo',
            'txtOffPrefix',
            'ddlNoofNamedDrivers',
            'ddlInsuredNationality',
            '#hdnNamedDriver',
            '#hdfOldPolNo',
            '#hdfRenewalSameAgentInd',
            'txtRelaMemNo',
            '#chkSighted',
            '#chckRecon',
            'chckIsInsuredIC',
            'dblPrevSumIns',
            'txtPrevSI',
            'hdfVehBodyID',
            'hdfEXLTempPassin',
            'hdfExlSpecialInd',
            'hdfWarn',
            'hdfTempISM',
            'ddlInsIndicator',
            'hdfAllowInd',
            'hdfNCDReply',
            'hdfQuoStage',
            'txtNewIC1',
            'txtNewIC2',
            'txtNewIC3',
            'ddlDay',
            'ddlMonth',
            'ddlYear',
            'txtPostCode',
            'txtTown',
            'TempPassInValues',
            'hdfAgentDtls',
            'hdfTrailerInd',
            'id$=chckMobile',
            'chckEmgMobNo',
            'hdfIPAddrs',
            'hfSightedICInd',
            'bodyType',
            'id$=lblErrorMsg',
            'unknown2',
            'ddlThCovType',
            'txtThCovTypeDesc',
            'ddlThARMCode',
            'id$=chkAllDrv',
        ];

        $output = [
            'data' => [],
            'unknowns' => [],
        ];

        foreach ($mstData as $key => $value) {
            if(isset($keys[$key])) {
                $output['data'][Str::camel($keys[$key])] = $value;
            }
            else {
                array_push($output['unknowns'], $value);
            }
        }

        return $output;
    }

    public function schDataWithKeys($mstData) {
        if(!is_array($mstData)) {
            $mstData = explode(static::DATA_DELIMITER, $mstData);
        }

        $keys = [
            'txtPrmInsured',
            'unknownZero1',
            'txtAllRiderInd',
            '#ddlAgreeValue option:selected',
            'txtCN_NCDPer',
            '#ddlVoluntaryExcess option:selected',
            '#ddlPACUnit option:selected',
            '#ddlPAC option:selected',
            '#lblCurAgreeValue',
            'hdnPacType',
            '#ddlNCDPer option:selected',
            'unknownNull1',
            '[id$=ddlABIInd]',
            'dfSumInsChangInd',
            'dblABISumIns',
            'dblABIOrgSumIns',
            'rbAvSI',
            'rbRecSI',
            '[id$=lblPACDsc] innerHTML',
            'hdfReQuoteInd',
            'hdfOptionalInd',
            'hdfWScreenSI windscreen suminsured 2000.00',
            'hdfErrorMsg',
            'hdfISMValidInd',
            'hdfTabPrevValue',
            'unknownZero2',
            'unknownZero3',
            'unknownZero4',
            'unknownZero5',
            'unknownZero6',
            'id$=txtCN_NCDPer] textcontext',
            '[id$=txtCN_DsNCDAmt] textContent',
            '#txtDsBasicPremium text',
            '#txtDSLoadingPer text',
            '#txtDsTuitionLoadPer text',
            '#txtDSLoadingAmt',
            '#txtDsTuitionLoadAmt text',
            '#txtAllRider text',
            '#txtPrmServicTaxAmt text',
            '#txtPrmStampDutyAmt text',
            '#txtPrmStampDutyAmt text',
            '#txtExcessAmt text',
            '#hdfActPrem',
            '#txtTtlCoveragePremium text',
            '#hdfServiceTax',
            '#dblABIMktValue',
            '#dblPrevSumIns',
            '#txtPrmRebatePer text',
            '#txtPrmRebateAmt text',
            '#lblVoluntaryExcessPrem text',
            '#lblRenewalBonus text',
            '#lblTotalExcess text',
            '#lblPrevSI text',
            '#lblAvSI text',
            '#lblRecSI text',
            '#lblAdjustmentPct text',
            '#lblAdjustmentAmt text',
            'document.getElementById "txtPersonalNoPass" value',
            '#txtPersonalSumInsured text 250,000.00',
            '#txtDsPersonalPremium text 72.00',
            '#txtDsPersonalAddPremium text',
            '#txtDsPersonalServiceTax text 4.32',
            '#txtDsPersonalStampDuty text',
            '#txtDsPACExtraCover text',
            '#txtPACRebatePer text',
            '#txtPACRebateAmt text',
            '#lblPrevSI context.body.disabled is not 1',
            '#hdfQuoValExpDate',
            '#hdfDirectInd',
            'ddlAgreeValue context.body.disabled',
            'document.getElementById t.attributes.txtPrmInsured.value disabled is not 1',
            'document.getElementById t.attributes.txtPrevSI.value disabled is not 1',
            '#hdfTempMV',
            '#hdfNCDValidInd',
            '#hdfISMMV',
            '#hdfISMNCD',
            '#hdfNCDValue',
            '#hdfISMMrkValueInd',
            '$find t.attributes.ddlLoanType.value get_value',
            '[id$=hdnMtrComm] value 0',
            '[id$=hdnPACComm] value 0',
            '#hdfExtCover_dt defaultValue',
            '#hdfPACExtCover_dt defaultValue',
            '[id$=hdfPACRiderDtls] value',
        ];

        $output = [
            'extraCoverageDatas' => [],
        ];

        foreach ($mstData as $key => $value) {
            if(!isset($keys[$key])) {
                array_push($output['extraCoverageDatas'], $value);

                continue;
            }

            $output[Str::camel($keys[$key])] = $value;
        }

        return $output;
    }

    public function renewalDtlsPayload($overrides = []) {
        $defaults = [
            'VNo' => $this->getSharedQuotationData('vehicleRegNo', 'JQR3339'),
            'TransType' => 'BR',
            'ProductCode' => 'PZ01',
        ];

        return $this->mergeAndOverride($defaults, $overrides);
    }
    
    /**
     * get one time cached renewal dtl data
     *
     * @param string $vehicleNo
     * @return array
     */
    public function oneTimeRenewalDtls($vehicleNo) {
        if(!isset($this->renewalDtls)) {
            $this->renewalDtls = $this->client->vehicle->renewalDtls($vehicleNo);
        } 

        return $this->renewalDtls;
    }

    public function ismMarketValuePayload($data = []) {
        $renewalDtls = $this->oneTimeRenewalDtls($this->getSharedQuotationData('vehicleRegNo', 'JQR3339'));

        $vehData = explode('^', $renewalDtls['vehRenewalData']);

        $hdfTempISM = explode('|', $this->hdfTempISM);
        $hdfVehBodyID = explode(static::DATA_DELIMITER, $renewalDtls['hdfVehBodyID']);

        $hdfExlSpecialInd = explode(static::DATA_DELIMITER_2, $this->hdfExlSpecialInd);

        $param = implode(static::DATA_DELIMITER, [
            $hdfTempISM[0],
            $hdfTempISM[1],
            $hdfTempISM[2],
            $hdfTempISM[3],
            $data['model'],
            $vehData[2],
            $vehData[1],
            $this->policyStartDate->format('d-M-Y'),
            $this->policyEndDate->format('d-M-Y'),
            'L',
            'B',
            '',
            '0.00',
            '0',
            'Q',
            $hdfExlSpecialInd[5],
            'B01',
            '8',
            '',
            'N',
            $data['region'],
        ]);

        $payload = [
            'VehDtls' => $renewalDtls['hdfVehBodyID'],
            'Param' => $param,
        ];

        return $payload;
    }
    
    /**
     * get one time cached renewal dtl data
     *
     * @param string $vehicleNo
     * @return array
     */
    public function oneTimeIsmMarketValue($vehicleNo) {
        if(!isset($this->ismMarketValue)) {
            $response = $this->getIsmMarketValueRequest($vehicleNo);

            $this->ismMarketValue = $this->client->parser->ismMarketValue((string) $response->getBody());
        }

        if(!intval($this->ismMarketValue['CurrentMarketValue'])) {
            throw ZurichClientException::ismMarketValueFailed();
        }

        return $this->ismMarketValue;
    }

    /**
     * After add extra callback
     *
     * @param string $name
     * @return void
     */
    public function onAfterAddExtra($name) {
        switch($name) {
            case 'liabilityPass': 
                array_push($this->extraCoverList, '02þ0.00þ0þþþþþþþDEFþN');
            break;

            case 'cart': 
                array_push($this->extraCoverList, '112þ0.00þ0þ14þ50.00þþþþþCRTþN');
            break;
            
            case 'waiver':
                array_push($this->extraCoverList, '207þ0.00þ0þþþþþþþDEFþN');
            break;
            
            case 'ehailing':
                array_push($this->extraCoverList, '221þ0.00þ0þþþþþþþDEFþN');
            break;
            
            case 'strike':
                array_push($this->extraCoverList, '25þ0.00þ0þþþþþþþDEFþN');
            break;
            
            case 'laminated':
                array_push($this->extraCoverList, '89Aþ' . number_format($this->extraCoverages['laminated'], 0) . 'þ30.00þþþþþþþSUMþN');
            break;

            default:
            
            break;
        }
    }

}
