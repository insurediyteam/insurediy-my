<?php 

namespace Services;

use Bugsnag\Client;
use Bugsnag\Handler;
use RuntimeException;
use Concerns\InteractsWithJConfig;
use Concerns\InteractsWithUser;

class Bugsnag {

    use InteractsWithJConfig, InteractsWithUser;

    public static $instance;

    public function register() {
        $bugsnagKey = $this->getConfig('bugsnag_key');

        if($bugsnagKey) {
            // register user
            $user = $this->getUser();
            static::getInstance()->registerCallback(function ($report) use ($user) {
                $report->setUser((array) $user);
            });

            // set release stage
            static::getInstance()->setReleaseStage($this->getConfig('stage', 'development'));

            // register exception handler
            Handler::register(static::getInstance());
        }
    }

    /**
     * Get bugsnag client instance
     *
     * @return Client
     */
    public static function getInstance() {
        $thisClass = new static;
        $bugsnagKey = $thisClass->getConfig('bugsnag_key');

        if(!isset(static::$instance)) {
            static::$instance = Client::make($bugsnagKey);
        }
        
        return static::$instance;
    }

    public static function test() {
        static::getInstance()->notifyException(new RuntimeException("Test error"));
    }
    
}
