<?php 

namespace Services;

use Concerns\InteractsWithUser;
use Services\QuoteSession\UserDriver;
use Services\QuoteSession\GuestDriver;
use Services\QuoteSession\Contracts\DriverInterface;

class QuoteSession {

    use InteractsWithUser;

    /** @var self */
    public static $instance;

    /** @var array */
    protected $attributes;

    /** @var DriverInterface */
    protected $driver;

    /**
     * Initialize instance
     */
    public function __construct()
    {
        $this->driver();
    }

    /**
     * singleton instance
     *
     * @return self
     */
    public static function getInstance() {
        if (!isset(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function driver($driverClass = null) {
        if ($driverClass) {
            $this->setDriver(new $driverClass);
            return $this;
        }

        $user = $this->getUser();

        if ($user->guest) {
            $this->setDriver(new GuestDriver);

            return $this;
        }

        $this->setDriver(new UserDriver);

        return $this;
    }

    /**
     * Automagically get properties from attributes
     *
     * @param string $property
     * @return mixed
     */
    public function __get($property) {
        if (isset($this->attributes[$property])) {
            return $this->attributes[$property];
        }

        return null;
    }

    /**
     * Automagically set property to attributes
     *
     * @param string $property
     * @param mixed $value
     * 
     * @return self
     */
    public function __set($property, $value) {
        $this->set($property, $value);
    }

    /**
     * save session
     *
     * @return self
     */
    public function save() {
        $this->driver->save($this->attributes);

        return $this;
    }
    

    /**
     * Get the value of driver
     */ 
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * Set the value of driver
     *
     * @return  self
     */ 
    public function setDriver(DriverInterface $driver)
    {
        $this->driver = $driver;
        
        $this->attributes = $this->driver->get();

        return $this;
    }

    public function toArray() {
        return $this->attributes;
    }

    public function set($key, $value = null) {
        if (is_array($key)) {
            $this->attributes = $key;
        }
        else {
            $this->attributes[$key] = $value;
        }
        
        return $this->save();
    }

    public function flush() {
        $this->driver->flush();
    }

    public function setMany($dataArray) {
		foreach ($dataArray as $key => $value) {
			$this->{$key} = $value;
        }
        
        return $this->save();
    }

}
