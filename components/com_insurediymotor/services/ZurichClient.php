<?php 

namespace Services;

use Exception;
use Services\BaseClient;
use Illuminate\Support\Str;
use GuzzleHttp\Psr7\Response;
use Services\ZurichClient\Parser;
use Services\ZurichClient\Vehicle;
use Services\ZurichClient\Generator;
use Services\ZurichClient\Occupation;
use Services\ZurichClient\Transformer;
use Services\Client\ComparisonTableData;

class ZurichClient extends BaseClient {

    const HOST = 'https://eins.zurich.com.my';

    const ROUTE_LOGIN = '/ZInsurance/LandingPage/GetLoginDetail';
    const ROUTE_HOME = '/ZInsurance/LandingPage/Home';
    const ROUTE_MENU = '/Motor2/Menu.aspx';

    const ROUTE_FORM = '/Motor2/CoverNote/NewQuote.aspx';
    const ROUTE_QUOTATION = '/Motor2/CoverNote/NewCNote.aspx/GetPremiumDtls';
    const ROUTE_EXTRA_COVER = '/Motor2/CoverNote/NewCNote.aspx/Motor_ExtCover_Control_Change';

    const ROUTE_GET_DROPDOWN = '/Motor2/CoverNote/NewCNote.aspx/GetDropDown';

    const ROUTE_MAKE = '/Motor2/wsAutoComplete.asmx/fnGetMakeData';
    const ROUTE_MODEL = '/Motor2/wsAutoComplete.asmx/fnGetModelData';

    public static $guest = true;

    /** @var Vehicle */
    public $vehicle;

    /** @var Generator */
    public $generator;

    /** @var Parser */
    public $parser;

    /** @var Occupation */
    public $occupation;

    /** @var string */
    public $step1QuotationForm;

    /** @var \Symfony\Component\DomCrawler\Crawler */
    public $step1QuotationFormDom;

    protected $quotationValue;

    public $enableNCD = false;

    public $postCodeData;

    /**
     * Instantiate class
     */
    public function __construct()
    {
        parent::__construct();

        $this->vehicle = new Vehicle($this);
        $this->generator = new Generator($this);
        $this->parser = new Parser($this);
        $this->occupation = new Occupation($this);
        $this->transformer = new Transformer($this);
    }

    public function __destruct()
    {
        $this->getModel()->update([
            'cookie' => null,
        ]);
    }

    /**
     * get Base URL
     *
     * @return string
     */
    public function getBaseUri() {
        return self::HOST;
    }

    /**
     * Keys to filter generated data
     *
     * @return array
     */
    protected function sharedQuotationDataKeys() {
        return [
            'vehicleRegNo',
        ];
    }

    /**
     * Configuration key index
     *
     * @return string
     */
    public function configKey() {
        return 'zurich';
    }

    /**
     * database code
     *
     * @return string
     */
    public function code() {
        return $this->configKey();
    }

    /**
     * Get options for generating http client
     *
     * @return array
     */
    public function getHttpClientOptions() {
        return [
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36;OS = Windows',
                'Upgrade-Insecure-Requests' => '1',
                'Sec-Fetch-User' => '?1',
                'Sec-Fetch-Site' => 'same-origin',
                'Sec-Fetch-Mode' => 'navigate',
            ],
        ];
    }

    /**
     * Authenticate to server
     *
     * @return self
     */
    public function authenticate() {
        if(!$this->checkAuth()) {
            // login
            $auth = $this->config('auth');

            $loginData = [
				'loginPs' => $auth['password'],
                'loginID' => $auth['user'],
            ];
            
            try {
                $response = $this->getHttpClient()->post(self::ROUTE_LOGIN, [
                    'json' => $loginData,
                    'headers' => [
                        'Content-Type' => 'application/json; charset=UTF-8',
                        'Origin' => static::HOST,
                        'Referer' => static::HOST . '/zInsurance/',
                    ],
                ]);

                if(Str::contains((string) $response->getBody(), 'Invalid User Password')) {
                    $this->doLockDown();
                }

                static::$guest = false;
            }
            catch (\Exception $e) {
                //
                var_dump($e);
                exit;
            }

        }

        return $this;
    }

    /**
     * Get quotation
     *
     * @param array $inputs
     * @return array
     */
    public function quotation($inputs = []) {
        $this->authenticate();

        $inputs = $this->transformer->inputs($inputs);

        $response = $this->step1QuotationRequest($inputs);

        // nextbtn
        $response = $this->mstSaveRequest($inputs);
        
        $response = $this->step2QuotationRequest($inputs);
        $this->updateHdfMstQuoDtls($response);
        
        $response = $this->resetExtraCoverRequest($inputs);
        $this->updateHdfMstQuoDtls($response);

        $quotationData = $this->parser->quotationData((string) $response->getBody());

        if($this->enableNCD && isset($inputs['ncd']) && $inputs['ncd'] != 0.00) {
            $response = $this->previewCoverNoteRequest($inputs);
            $this->updateHdfMstQuoDtls($response);
    
            $quotationData = $this->parser->quotationDataPreview($inputs, $quotationData, (string) $response->getBody());
        }

        return $quotationData;
    }

    /**
     * Check authentication
     *
     * @return bool
     */
    public function checkAuth() {
        if (static::$guest === false) {
            return true;
        }

        $response = $this->getHttpClient()->get(self::ROUTE_HOME);

        $responseBody = (string) $response->getBody();

        return ! (Str::contains($responseBody, 'Login') && Str::contains($responseBody, 'Password'));
    }

    public function quotationMenuRequest() {
        $this->authenticate();
        
        $response = $this->getHttpClient()->get(static::ROUTE_MENU);

        return $response;
    }

    /**
     * Step1 Form Submit Request
     *
     * @param array $inputs
     * @return Response
     */
    public function step1QuotationRequest($inputs) {
        $this->authenticate();
        
        $quotationData = $this->generator->step1QuotationData($inputs);

        $response = $this->getHttpClient()->post(static::ROUTE_FORM, [
            'form_params' => $quotationData,
        ]);

        return $response;
    }

    /**
     * Step 2 Form Submit Request
     *
     * @param array $inputs
     * @return Response
     */
    public function step2QuotationRequest($inputs) {
        $this->authenticate();
        
        $quotationData = $this->generator->step2QuotationData($inputs);

        $response = $this->getHttpClient()->post(static::ROUTE_QUOTATION, [
            'json' => $quotationData,
        ]);

        return $response;
    }

    /**
     * Set the value of quotationValue
     *
     * @return  self
     */ 
    public function setQuotationValue($quotationData)
    {
        $this->quotationValue = $quotationData;

        return $this;
    }

    public function occupationRequest() {
        return $this->getHttpClient()->post(static::ROUTE_GET_DROPDOWN, [
            'json' => [
                'DdlName' => "sdfOccup",
            ],
        ]);
    }

    public function makeDataRequest($name) {
        $this->authenticate();
        
        return $this->getHttpClient()->post(static::ROUTE_MAKE, [
            'json' => [
                'context' => [
                    'ComboType' => "Make",
                    'NumberOfItems' => 0,
                    'ProductCode' => 'PZ01',
                    'Text' => $name,
                ],
            ],
        ]);
    }

    public function modelDataRequest($makeCode, $keyword, $makeYear = null) {
        $this->authenticate();

        return $this->getHttpClient()->post(static::ROUTE_MODEL, [
            'json' => [
                'context' => [
                    'ComboType' => 'Model',
                    'FilterKeys' => "PZ01±{$makeCode}±{$makeYear}±N±",
                    'NumberOfItems' => 0,
                    'Text' => $keyword,
                ],
            ],
        ]);
    }

    public function ncdReplyRequest($refNo) {
        $response = $this->getHttpClient()->post('/Motor2/CoverNote/NewCNote.aspx/fngetNCDReply', [
            'json' => [
                'NCDRefNo' => $refNo,
            ],
        ]);

        return $response;
    }

    public function mstSaveRequest($inputs) {
        $response = $this->getHttpClient()->post('/Motor2/CoverNote/NewCNote.aspx/btnNext_Save_Click', [
            'json' => $this->generator->mstSavePayload($inputs),
        ]);

        $data = json_decode((string) $response->getBody(), true);

        $data = explode('`', $data['d']);

        if(!isset($data[1])) {
            throw new Exception('Invalid mst data!: ' . json_encode(['response' => (string) $response->getBody()]));
        }

        $data = json_decode($data[1], true);

        $this->generator->hdfEXLTempPassin = $data['hdfEXLTempPassin'];
        $this->generator->hdfExlSpecialInd = $data['ExlSpecialInd'];
        $this->generator->hdfWarn = $data['hdfWarn'];

        $delimiter = '|';
        $this->generator->hdfTempISM = explode($delimiter, $data['hdfTempISM']);
        array_shift($this->generator->hdfTempISM);
        $this->generator->hdfTempISM = implode($delimiter, $this->generator->hdfTempISM);

        $this->generator->tempPassInValues = $data['TempPassInValues'];
        $this->generator->hdfNCDReply = $data['hdfNCDReply'];
        $this->generator->hdfNCDValue = $data['hdfNCDValue'];
        $this->generator->hdfTempMV = $data['hdfTempMV'];

        $this->generator->claimDtls = $data['hdfClaimDtls'];

        if($data['hdfNCDRefNo']) {
            $this->generator->hdfNCDRefNo = $data['hdfNCDRefNo'];

            $response = $this->ncdReplyRequest($this->generator->hdfNCDRefNo);

            $ncdData = $this->parser->ncdReply($response);

            if(isset($ncdData['NCDRTN'])) {
                $this->generator->hdfNCDReply = $ncdData['NCDRTN'];
                $provNcdFlipped = array_flip(Transformer::$provNCDs);
                // if(isset($inputs['ncd']) && $inputs['ncd']) {
                //     // enforce NCD from input
                //     $this->generator->hdfNCDValue = $inputs['ncd'] . Generator::DATA_DELIMITER_2 . (string) ($provNcdFlipped[$inputs['ncd']] - 2);
                // }
                // else {
                //     $this->generator->hdfNCDValue = str_replace('%', '', $ncdData['arrayNCDRTN'][3]) . Generator::DATA_DELIMITER_2 . $ncdData['arrayNCDRTN'][18];
                // }
                $ncdValue = number_format($ncdData['arrayNCDRTN'][19], 2, '.', '');
                if(!isset($provNcdFlipped[$ncdValue]) && intval($ncdValue) == 38) {
                    $ncdValue = '38.33';
                }
                $ncdCodeExpected = (string) ($provNcdFlipped[$ncdValue] - 2);
                if($ncdCodeExpected == $ncdData['arrayNCDRTN'][18]) {
                    $this->generator->hdfNCDValue = $ncdValue . Generator::DATA_DELIMITER_2 . $ncdCodeExpected;
                    
                    $this->addCoverTypeAndExcesses(
                        new ComparisonTableData(
                            ComparisonTableData::GROUP_NCD_PERCENT[0],
                            ComparisonTableData::GROUP_NCD_PERCENT[1],
                            $ncdValue . '%'
                        )
                    );
                }
            }
        }
    }

    public function resetExtraCoverRequest($inputs) {
        return $this->getHttpClient()->post('/Motor2/CoverNote/NewCNote.aspx/Motor_ExtCover_Control_Change', [
            'json' => $this->generator->extCoverControlChange($inputs),
        ]);
    }

    public function getRenewalDtlsRequest($inputs) {
        $response = $this->getHttpClient()->post('/Motor2/CoverNote/NewCNote.aspx/fnGetRenewalDtls', [
            'json' => $this->generator->renewalDtlsPayload($inputs),
        ]);

        return $response;
    }

    public function getIsmMarketValueRequest($inputs) {
        $response = $this->getHttpClient()->post('/Motor2/CoverNote/NewCNote.aspx/fngetIsmMarketValue', [
            'json' => $this->generator->ismMarketValuePayload($inputs),
        ]);

        return $response;
    }

    public function previewCoverNoteRequest($inputs) {
        return $this->getHttpClient()->post('/Motor2/CoverNote/NewCNote.aspx/btnNext_Save_Click', [
            'json' => $this->generator->previewCoverNote($inputs),
        ]);
    }


    /**
     * Get the value of postCodeData
     */ 
    public function getPostCodeData($postCode)
    {
        if(!isset($this->postCodeData[$postCode])) {
            $response = $this->getHttpClient()->get('/Motor2/CoverNote/PopPostCode.aspx', [
                'query' => [
                    'PCode' => $postCode,
                ],
            ]);

            $response = (string) $response->getBody();
            list($unknown, $town, $state, $country) = explode(Generator::DATA_DELIMITER, $response);

            $this->postCodeData[$postCode] = [
                'unknown' => $unknown,
                'town' => $town,
                'state' => explode(Generator::DATA_DELIMITER_2, $state),
                'country' => explode(Generator::DATA_DELIMITER_2, $country),
            ];
        }

        return $this->postCodeData[$postCode];
    }

    /**
     * Should be run on after each requests with mstdata & sch
     *
     * @param \GuzzleHttp\Psr7\Response $response
     * @return void
     */
    public function updateHdfMstQuoDtls(\GuzzleHttp\Psr7\Response $response) {
        
        $data = json_decode((string) $response->getBody(), true);
        $data = explode('ý', $data['d']);
        if(!isset($data[1])) {
            return;
        }
        
        $this->generator->hdfMstQuoDtls = $data[1];
    }
}
