<?php 

namespace Services;

use Concerns\IcData;
use Services\BaseClient;
use Illuminate\Support\Str;
use GuzzleHttp\Psr7\Response;
use Services\AmgClient\Parser;
use Services\AmgClient\Vehicle;
use Services\AmgClient\Generator;
use Services\AmgClient\LoanCompany;
use Services\AmgClient\Occupation;
use Services\AmgClient\Transformer;
use Services\Client\BaseClientException;
use Services\Client\ComparisonTableData;
use Symfony\Component\DomCrawler\Crawler;

class AmgClient extends BaseClient {

    const HOST = 'https://kec.ambankgroup.com';

    const ROUTE_LOGIN = '/amg/Agent/authenticate/agentshome.do';
    const ROUTE_HOME = '/amg/Agent/authenticate/agentsmain.do';
    const ROUTE_SELECT_BRAND = '/amg/Agent/authenticate/agentsmain.do';
    const ROUTE_BULLETIN_BOARD = '/amg/Agent/home/bulletinboard.do';

    const ROUTE_QUOTATION = '/amg/Agent/transactions/nbcnclientprofile.do';
    const ROUTE_QUOTATION_STEP_TWO = '/amg/Agent/transactions/nbcncovernote.do';
    const ROUTE_QUOTATION_STEP_THREE = '/amg/Agent/transactions/motorcnvehicle.do';
    const ROUTE_QUOTATION_STEP_FOUR = '/amg/Agent/transactions/motorcommonelementlistener.do';
    const ROUTE_QUOTATION_EXTRA_COVERAGE = '/amg/Agent/transactions/motorcnextracoverage.do';

    const ROUTE_VEHICLE_SEARCH = '/amg/Agent/administration/administrationdetgrid.do';

    public static $guest = true;

    /** @var Vehicle */
    public $vehicle;

    /** @var Generator */
    public $generator;

    /** @var Parser */
    public $parser;

    /** @var Occupation */
    public $occupation;

    /** @var Transformer */
    public $transformer;

    /** @var LoanCompany */
    public $loanCompany;

    /** @var string */
    public $step1QuotationForm;

    /** @var \Symfony\Component\DomCrawler\Crawler */
    public $step1QuotationFormDom;

    public static $drivers = [];

    /**
     * Instantiate class
     */
    public function __construct()
    {
        parent::__construct();

        $this->vehicle = new Vehicle($this);
        $this->generator = new Generator($this);
        $this->parser = new Parser($this);
        $this->occupation = new Occupation($this);
        $this->transformer = new Transformer($this);
        $this->loanCompany = new LoanCompany($this);
    }


    /**
     * get Base URL
     *
     * @return string
     */
    public function getBaseUri() {
        return self::HOST;
    }

    /**
     * Keys to filter generated data
     *
     * @return array
     */
    protected function sharedQuotationDataKeys() {
        return [
            'txtVehicleNo',
            'txtNewIcNo',
            'sumInsured',
            'txtExpiryDate',
            'txtInceptionDate',
        ];
    }

    /**
     * Configuration key index
     *
     * @return string
     */
    public function configKey() {
        return 'amg';
    }

    /**
     * database code
     *
     * @return string
     */
    public function code() {
        return $this->configKey();
    }

    /**
     * Get options for generating http client
     *
     * @return array
     */
    public function getHttpClientOptions() {
        return [
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36;OS = Windows',
                'Upgrade-Insecure-Requests' => '1',
                'Sec-Fetch-User' => '?1',
                'Sec-Fetch-Site' => 'same-origin',
                'Sec-Fetch-Mode' => 'navigate',
            ],
        ];
    }

    /**
     * Authenticate to server
     *
     * @return self
     */
    public function authenticate() {
        if(!$this->checkAuth()) {
            // login
            $auth = $this->config('auth');

            $loginData = [
				'browserId' => '12',
				'browserVersionDet' => '5!Chrome',
				'compBrowserDetails' => 'Browser Details = Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36;OS = Windows',
				'groupButtons.btnGotoLogin.name' => '12',
				'loginCounter' => '0',
				'loginExistInd' => '',
				'loginExistIndication' => '',
				'maintenanceMsgStr' => '',
				'noLogin' => 'null',
				'reDirect' => 'null',
				'scriptInd' => '',
				'txtPassword' => $auth['password'],
                'txtUserId' => $auth['user'],
                'txtValidationMsg' => '',
            ];
            
            try {
                $response = $this->getHttpClient()->post(self::ROUTE_LOGIN, [
                    'form_params' => $loginData,
                    'headers' => [
                        'Origin' => 'https://kec.ambankgroup.com',
                        'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
                    ],
                ]);

                $loginData['loginExistInd'] = 'Y';
                $loginData['loginExistIndication'] = 'Y';

                $response = $this->getHttpClient()->post(self::ROUTE_LOGIN, [
                    'form_params' => $loginData,
                    'headers' => [
                        'Origin' => 'https://kec.ambankgroup.com',
                        'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
                    ],
                ]);

                if(Str::contains((string) $response->getBody(), 'Account Locked / Forgot Password')) {
                    // $this->doLockDown();
                    throw BaseClientException::clientLockdown();
                }

                // select brand (amg / kurnia)
                $this->getHttpClient()->post(static::ROUTE_SELECT_BRAND, [
                    'form_params' => [
                        'btnSubmitInd' => 'true',
                        'cboBrand' => 'K',
                        'txtbrandName' => '',
                    ]
                ]);

                static::$guest = false;
            }
            catch (\Exception $e) {
                //
                var_dump($e);
                exit;
            }

        }

        return $this;
    }

    /**
     * Get quotation
     *
     * @param array $inputs
     * @return array
     */
    public function quotation($inputs = []) {
        $this->authenticate();

        $inputs = $this->transformer->inputs($inputs);

        $response = $this->step1QuotationRequest($inputs);
        
        $response = $this->step2QuotationRequest($inputs);

        if(isset($inputs['cboCnType']) && $inputs['cboCnType'] == '2') {
            $response = $this->vehicleInfoRequest($inputs);
            $vehicleInfo = $this->parser->vehicleInfo($response);
            $response = $this->addDriversRequest(array_merge($inputs, [
                'vehicleModelId' => $vehicleInfo['modelId'],
            ]));

            $inputs = array_merge($inputs, $vehicleInfo);
        }

        $response = $this->step3QuotationRequest($inputs);
        
        $response = $this->step4QuotationRequest($inputs);

        $quotationData = $this->parser->quotationData((string) $response->getBody());

        if(count($this->generator->selectedExtraCoverages)) {
            $quotationData = collect($quotationData);
            $inputs['grossPremium'] = $quotationData->where('description', 'Gross Premium')->first()['value'];
            $inputs['totalPayable'] = $quotationData->where('description', 'Total Payable')->first()['value'];

            $extraCoverageRequest = $this->extraCoverageRequest($inputs);
        
            $response = $this->step4QuotationRequest($inputs);
            $quotationData = $this->parser->quotationData((string) $response->getBody());
        }

        $compulsoryExcess = collect($quotationData)->where('description', 'Compulsory Excess')
            ->first();
        if($compulsoryExcess) {
            $this->addCoverTypeAndExcesses(
                new ComparisonTableData(
                    ComparisonTableData::GROUP_COMPULSORY_EXCESS[0],
                    ComparisonTableData::GROUP_COMPULSORY_EXCESS[1],
                    ComparisonTableData::valueWithIcon($compulsoryExcess['value'], true)
                )
            );
        }

        return $quotationData;
    }

    /**
     * Check authentication
     *
     * @return bool
     */
    public function checkAuth() {
        if (static::$guest === false) {
            return true;
        }

        $response = $this->getHttpClient()->get(self::ROUTE_BULLETIN_BOARD);

        $responseBody = (string) $response->getBody();

        return ! (Str::contains($responseBody, 'Login') && Str::contains($responseBody, 'AGENCY SUITE'));
    }

    public function quotationFormRequest() {
        $this->authenticate();
        
        $response = $this->getHttpClient()->get(static::ROUTE_QUOTATION, [
            'query' => [
                'loadInd' => 'Y',
                'MN' => 'Y',
                'screenId' => 1,
            ],
        ]);

        return $response;
    }

    /**
     * Step1 Form Submit Request
     *
     * @param array $inputs
     * @return Response
     */
    public function step1QuotationRequest($inputs) {
        $this->authenticate();
        
        $quotationData = $this->generator->step1QuotationData($inputs);

        $response = $this->getHttpClient()->post(static::ROUTE_QUOTATION, [
            'form_params' => $quotationData,
        ]);

        return $response;
    }

    /**
     * Step 2 Form Submit Request
     *
     * @param array $inputs
     * @return Response
     */
    public function step2QuotationRequest($inputs) {
        $this->authenticate();
        
        $quotationData = $this->generator->step2QuotationData($inputs);

        $response = $this->getHttpClient()->post(static::ROUTE_QUOTATION_STEP_TWO, [
            'form_params' => $quotationData,
        ]);

        return $response;
    }

    public function vehicleInfoRequest($inputs) {
        $this->authenticate();

        $data = $this->generator->vehicleInfoRequest($inputs);

        $response = $this->getHttpClient()->post(static::ROUTE_QUOTATION_STEP_FOUR, [
            'form_params' => $data,
        ]);

        return $response;
    }

    public function addDriversRequest($inputs) {
        $this->authenticate();

        $data = $this->generator->addDrivers($inputs);

        $response = $this->getHttpClient()->post('/amg/Agent/transactions/nameddriversgrid.do', [
            'form_params' => $data,
            'query' => [
                'formElement' => 'fnAddDrivers',
            ],
        ]);

        return $response;
    }

    /**
     * Step 3 Form Submit Request
     *
     * @param array $inputs
     * @return Response
     */
    public function step3QuotationRequest($inputs) {
        $this->authenticate();
        
        $quotationData = $this->generator->step3QuotationData($inputs);
        
        // add driver if any
        if(count(static::$drivers)) {
            foreach (static::$drivers as $key => $driver) {
                $ic = new IcData($driver['icNum']);
                $quotationData = array_merge($quotationData, [
                    "sNo" . $key => $key + 1,
                    "insName" . $key => "THE POLICYHOLDER",
                    "newIc" . $key => $ic->number,
                    "oldIc" . $key => "",
                    "age" . $key => $ic->dob->age,
                    "driverExp" . $key => $driver['yearsOfDrivingExp'],
                    "dob" . $key => $ic->dob->format('d-m-Y'),
                    "gender" . $key => substr($ic->number, 0, -1) % 2 === 0 ? '1' : '2',
                ]);
            }
        }

        $response = $this->getHttpClient()->post(static::ROUTE_QUOTATION_STEP_THREE, [
            'form_params' => $quotationData,
        ]);

        return $response;
    }

    /**
     * Step 4 Form Submit Request
     *
     * @param array $inputs
     * @return Response
     */
    public function step4QuotationRequest($inputs) {
        $this->authenticate();
        
        $quotationData = $this->generator->step4QuotationData($inputs);

        $response = $this->getHttpClient()->post(static::ROUTE_QUOTATION_STEP_FOUR, [
            'form_params' => $quotationData,
        ]);

        return $response;
    }

    /**
     * Set the value of quotationValue
     *
     * @return  self
     */ 
    public function setQuotationValue($quotationData)
    {
        $this->quotationValue = $quotationData;

        return $this;
    }

    public function extraCoverageRequest($inputs = []) {
        if($this->generator->extraCoverages['cart'] && in_array('cart', $this->generator->selectedExtraCoverages)) {
            $response = $this->getHttpClient()->post('/amg/Agent/transactions/motorcommonelementlistener.do', [
                'form_params' => [
                    'requestTypeParam' => 'asyncXML',
                    'formElement' => 'fnGetExtraCoverageDet',
                    'extraCoverageId' => '38',
                    'extraCoverageCode' => '112',
                    'agentId' => '37152',
                    'controlIndication' => 'E',
                    'noofDays' => '0',
                    'amtPerDay' => '0',
                    'txtExtCoverEffDate' => '',
                    'txtExtCoverExpDate' => '',
                ],
            ]);
            $response = $this->getHttpClient()->post('/amg/Agent/transactions/motorcommonelementlistener.do', [
                'form_params' => [
                    'requestTypeParam' => 'asyncXML',
                    'formElement' => 'fnComputeCARTPremium',
                    'controlInd' => 'D',
                    'noofDays' => '14',
                    'amtPerDay' => '0',
                    'formName' => 'MotorCnExtraCoverageForm',
                ],
            ]);
            $response = $this->getHttpClient()->post('/amg/Agent/transactions/motorcommonelementlistener.do', [
                'form_params' => [
                    'requestTypeParam' => 'asyncXML',
                    'formElement' => 'fnComputeCARTPremium',
                    'controlInd' => 'A',
                    'noofDays' => '14',
                    'amtPerDay' => '50.0',
                    'formName' => 'MotorCnExtraCoverageForm',
                ],
            ]);
        }
        else {
            $response = $this->getHttpClient()->post('/amg/Agent/transactions/motorcommonelementlistener.do', [
                'form_params' => [
                    'requestTypeParam' => 'asyncXML',
                    'formElement' => 'fnRemoveExtraCoverageDet',
                    'extraCoverageId' => '38',
                    'hdnExtraCoverNextSelect' => 'Y',
                ],
            ]);
        }

        if($this->generator->extraCoverages['specialPeril']) {
            $response = $this->getHttpClient()->post('/amg/Agent/transactions/motorcommonelementlistener.do', [
                'form_params' => [
                    'requestTypeParam' => 'asyncXML',
                    'formElement' => 'fnGetExtraCoverageDet',
                    'extraCoverageId' => '19',
                    'extraCoverageCode' => '57',
                    'agentId' => '37152',
                    'controlIndication' => 'E',
                    'noofDays' => '14',
                    'amtPerDay' => '50.0',
                    'txtExtCoverEffDate' => '',
                    'txtExtCoverExpDate' => '',
                ],
            ]);
        }

        if($this->generator->extraCoverages['liabilityPass']) {
            $response = $this->getHttpClient()->post('/amg/Agent/transactions/motorcommonelementlistener.do', [
                'form_params' => [
                    'requestTypeParam' => 'asyncXML',
                    'formElement' => 'fnGetExtraCoverageDet',
                    'extraCoverageId' => '21',
                    'extraCoverageCode' => '72',
                    'agentId' => '37152',
                    'controlIndication' => 'E',
                    'noofDays' => '14',
                    'amtPerDay' => '50.0',
                    'txtExtCoverEffDate' => '',
                    'txtExtCoverExpDate' => '',
                ],
            ]);
        }

        if($this->generator->extraCoverages['ehailing']) {
            $response = $this->getHttpClient()->post('/amg/Agent/transactions/motorcommonelementlistener.do', [
                'form_params' => [
                    'requestTypeParam' => 'asyncXML',
                    'formElement' => 'fnGetExtraCoverageDet',
                    'extraCoverageId' => '72',
                    'extraCoverageCode' => 'C001',
                    'agentId' => '37152',
                    'controlIndication' => 'E',
                    'noofDays' => '14',
                    'amtPerDay' => '50.0',
                    'txtExtCoverEffDate' => '',
                    'txtExtCoverExpDate' => '',
                ],
            ]);
        }

        if($this->generator->extraCoverages['strike']) {
            $response = $this->getHttpClient()->post('/amg/Agent/transactions/motorcommonelementlistener.do', [
                'form_params' => [
                    'requestTypeParam' => 'asyncXML',
                    'formElement' => 'fnGetExtraCoverageDet',
                    'extraCoverageId' => '13',
                    'extraCoverageCode' => '25',
                    'agentId' => '37152',
                    'controlIndication' => 'E',
                    'noofDays' => '14',
                    'amtPerDay' => '50.0',
                    'txtExtCoverEffDate' => '',
                    'txtExtCoverExpDate' => '',
                ],
            ]);
        }

        if($this->generator->extraCoverages['waiver']) {
            $response = $this->getHttpClient()->post('/amg/Agent/transactions/motorcommonelementlistener.do', [
                'form_params' => [
                    'requestTypeParam' => 'asyncXML',
                    'formElement' => 'fnGetExtraCoverageDet',
                    'extraCoverageId' => '74',
                    'extraCoverageCode' => 'C002',
                    'agentId' => '37152',
                    'controlIndication' => 'E',
                    'noofDays' => '14',
                    'amtPerDay' => '50.0',
                    'txtExtCoverEffDate' => '',
                    'txtExtCoverExpDate' => '',
                ],
            ]);
        }
        
        if($this->generator->extraCoverages['laminated']) {
            $response = $this->getHttpClient()->post('/amg/Agent/transactions/motorcommonelementlistener.do', [
                'form_params' => [
                    'requestTypeParam' => 'asyncXML',
                    'formElement' => 'fnGetExtraCoverageDet',
                    'extraCoverageId' => '26',
                    'extraCoverageCode' => '89',
                    'agentId' => '37152',
                    'controlIndication' => 'E',
                    'noofDays' => '14',
                    'amtPerDay' => '50.0',
                    'txtExtCoverEffDate' => '',
                    'txtExtCoverExpDate' => '',
                ],
            ]);
            $response = $this->getHttpClient()->post('/amg/Agent/transactions/motorcommonelementlistener.do', [
                'form_params' => [
                    'requestTypeParam' => 'asyncXML',
                    'formElement' => 'fnSetEachAmtForPassenger',
                    'extCoverSI' => $this->generator->extraCoverages['laminated'],
                    'ratePercent' => '0.00',
                    'inceptionDate' => $this->getSharedQuotationData('txtInceptionDate', '05-01-2020'),
                    'expiryDate' => $this->getSharedQuotationData('txtExpiryDate', '04-01-2021'),
                    'extCoverMinPremium' => '30.00',
                    'extraCoverageId' => '26',
                ],
            ]);
        }

        if($this->generator->extraCoverages['tinting']) {
            $response = $this->getHttpClient()->post('/amg/Agent/transactions/motorcommonelementlistener.do', [
                'form_params' => [
                    'requestTypeParam' => 'asyncXML',
                    'formElement' => 'fnGetExtraCoverageDet',
                    'extraCoverageId' => '27',
                    'extraCoverageCode' => '89(a)',
                    'agentId' => '37152',
                    'controlIndication' => 'E',
                    'noofDays' => '14',
                    'amtPerDay' => '50.0',
                    'txtExtCoverEffDate' => '',
                    'txtExtCoverExpDate' => '',
                ],
            ]);
            $response = $this->getHttpClient()->post('/amg/Agent/transactions/motorcommonelementlistener.do', [
                'form_params' => [
                    'requestTypeParam' => 'asyncXML',
                    'formElement' => 'fnSetEachAmtForPassenger',
                    'extCoverSI' => $this->generator->extraCoverages['tinting'],
                    'ratePercent' => '0.00',
                    'inceptionDate' => $this->getSharedQuotationData('txtInceptionDate', '05-01-2020'),
                    'expiryDate' => $this->getSharedQuotationData('txtExpiryDate', '04-01-2021'),
                    'extCoverMinPremium' => '30.00',
                    'extraCoverageId' => '27',
                ],
            ]);
        }

        // print_r((string) $response->getBody());exit;

        $payload = $this->generator->motorCoverNoteSchedulePayload($inputs);

        // print_r($payload);exit;

        // $response = $this->getHttpClient()->post('/amg/Agent/transactions/motorcnschedule.do', [
        //     'form_params' => $payload,
        //     'headers' => [
        //         'Origin' => 'https://kec.ambankgroup.com',
        //         'Referer' => 'https://kec.ambankgroup.com/amg/Agent/transactions/motorcnschedule.do',
        //     ],
        // ]);

        $payload = $this->generator->extraCoverageUpdatePayload($inputs);
        $response = $this->getHttpClient()->post(static::ROUTE_QUOTATION_EXTRA_COVERAGE, [
            'form_params' => $payload,
            'headers' => [
                'Origin' => 'https://kec.ambankgroup.com',
                'Referer' => 'https://kec.ambankgroup.com/amg/Agent/transactions/motorcnschedule.do',
            ],
        ]);

        $responseDom = $this->htmlToDom((string) $response->getBody());

        $extraCovTableRows = $responseDom->filter('table.Nrml_table')
            ->first()
            ->filter('tr');

        $self = &$this;

        $extraCovTableRows->each(function(Crawler $node, $i) use ($self) {
            if($i == 0) {
                return false;
            }

            $tableDatas = $node->filter('td');

            $name = trim($tableDatas->eq(0)->text());
            $value = trim($tableDatas->eq(2)->text());

            $self->addToExtraCoverageSumIns($name, $value);
        });

        return $response;
    }

    public function addToExtraCoverageSumIns($name, $value) {
        $maps = [
            [
                'COMPENSATION FOR ASSESSED REPAIR TIME',
                'cart',
            ],
            [
                'INCLUSION OF SPECIAL PERILS / CONVULSIONS OF NATURE',
                'specialPeril',
            ],
            [
                'LEGAL LIABILITY OF PASSENGERS',
                'liabilityPass',
            ],
            [
                'STRIKE, RIOT AND CIVIL COMMOTION',
                'strike',
            ],
            [
                'WAIVER OF BETTERMENT',
                'waiver',
            ],
            [
                'WINDSCREEN DAMAGE (TEMPERED/LAMINATED GLASS INCLUSIVE LABOUR COST)',
                'laminated',
            ],
            [
                'PRIVATE HIRE CAR (E-HAILING)',
                'ehailing',
            ],
        ];

        $found = null;
        foreach ($maps as $key => $map) {
            if(Str::contains($name, $map[0])) {
                $found = $map;

                break;
            }
        }

        if($found) {
            $this->setExtraCoverageSumInsured($map[1], $value);
        }
    }

    public function loanCompanyPageRequest($alphabet = 'A', $pageNo = 0, $additionalPayload = []) {
        $defaultPayload = [
            "searchType" => "2",
            "newIcNo" => "",
            "id" => "0",
            "desc" => "",
            "code" => "",
            "noOfSeats" => "",
            "capacity" => "",
            "screenId" => "0",
            "nonMotorInd" => "",
            "icIndication" => "",
            "kurniaInd" => "",
            "policyNo" => "",
            "effDate" => "",
            "expDate" => "",
            "ncdPercentage" => "",
            "pageNo" => "0",
            "lastPageNo" => "54",
            "currentPageNo" => "1",
            "pagecount" => "0",
            "nextPageCount" => "0",
            "nextPageStrart" => "0",
            "oldData" => "",
            "enterKeyInd" => "NO",
            "agentName" => "",
            "clientID" => "0",
            "clientName" => "",
            "polMotorId" => "0",
            "piamId" => "0",
            "clauseId" => "0",
            "warrantyId" => "0",
            "mstPolicyId" => "0",
            "field" => "1",
            "condition" => "2",
            "data" => "",
            "strCriteria" => 'A',
        ];

        return $this->getHttpClient()->post('/amg/Agent/commonutils/loanCompanySearch.do', [
            'form_params' => array_merge($defaultPayload, [
                'strCriteria' => $alphabet,
                'currentPageNo' => (string) ($pageNo == 1 ? 1 : $pageNo - 1),
                'pageNo' => (string) $pageNo,
            ], $additionalPayload),
        ]);
    }
    
}
