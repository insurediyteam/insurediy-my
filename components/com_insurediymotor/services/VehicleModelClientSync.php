<?php 

namespace Services;

use Concerns\InteractsWithJInput;
use Models\Eloquent\Make;
use Models\Eloquent\Model;
use Models\Eloquent\Detailable;
use Models\Eloquent\ModelProvider;
use Services\Client\BaseListData;

class VehicleModelClientSync extends BaseClientSync {

    use InteractsWithJInput;

    protected function reset($switch) {
        if($switch) {
            Model::getQuery()->delete();
            ModelProvider::getQuery()->delete();
            Detailable::where('detailable_type', ModelProvider::class)->delete();
        }
    }

    protected function getMakeList() {
        $self = &$this;
        $makeIdStart = (int) $this->getInput('makeIdStart', 0);
        $makeCodeFilter = $this->getInput('make');

        return $this->client
            ->getModel()
            ->makes()
            ->when($makeIdStart > 0, function($query) use ($makeIdStart) {
                $query->where('make_id', '>=', $makeIdStart);
            })
            ->when($makeCodeFilter, function($query) use ($makeCodeFilter) {
                $query->whereCode($makeCodeFilter);
            })
            ->with('make')
            ->get();
    }

    protected function getModelList($makeCode) {
        $models = $this->getClientVehicle()
            ->modelListData($makeCode);

        return $models;
    }

    public function createVehicleModel(Make $vehicleMake, BaseListData $listData) {
        $model = Model::firstOrCreate(
            [
                'make_id' => $vehicleMake->id,
                'code' => $this->codeSlug($listData->description),
            ],
            [
                'name' => $listData->description,
            ]
        );

        $messageDescription = "model #{$model->id}: {$listData->description}" . PHP_EOL;
        if($model->wasRecentlyCreated) {
            echo "Created new {$messageDescription}";
        }
        else {
            echo "Skipped {$messageDescription}";
        }

        return $model;
    }

    public function createVehicleModelProvider(Model $vehicleModel, BaseListData $listData) {
        return ModelProvider::firstOrCreate([
                'hash' => $listData->getHash([
                    'model_id' => $vehicleModel->id,
                    'provider_id' => $this->getModelId(),
                ]),
            ], [
                'model_id' => $vehicleModel->id,
                'provider_id' => $this->getModelId(),
                'code' => $listData->value,
                'name' => $listData->description,
            ]);
    }

    public function run() {
        foreach($this->getMakeList() as $vehicleMake) {
            foreach ($this->getModelList($vehicleMake->code) as $model) {
                $vehicleModel = $this->createVehicleModel($vehicleMake->make, $model);
    
                $vehicleModelProvider = $this->createVehicleModelProvider($vehicleModel, $model);
    
                $messageDescription = "model provider ({$this->client->code()}): {$model->description}" . PHP_EOL;
                if($vehicleModelProvider->wasRecentlyCreated) {
                    echo "Created new {$messageDescription}";

                    foreach($model->extras as $extra) {
                        $vehicleModelProvider->detailables()
                            ->create($extra);
                    }
                }
                else {
                    echo "Skipped {$messageDescription}";
                }
            }
        }
    }

}
