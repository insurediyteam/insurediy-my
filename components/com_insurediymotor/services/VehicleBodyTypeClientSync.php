<?php 

namespace Services;

use Models\Eloquent\Detailable;
use Models\Eloquent\BodyType;
use Models\Eloquent\BodyTypeProvider;
use Services\Client\BaseListData;

class VehicleBodyTypeClientSync extends BaseClientSync {

    protected function reset($switch) {
        if($switch) {
            BodyType::getQuery()->delete();
            BodyTypeProvider::getQuery()->delete();
            Detailable::where('detailable_type', BodyTypeProvider::class)->delete();
        }
    }

    protected function getBodyTypeList() {
        return $this->client->vehicle->bodyListData();
    }

    public function createBodyType(BaseListData $listData) {
        $bodyType = BodyType::firstOrCreate(
            [
                'name' => $listData->description,
            ],
            [
                'code' => $this->codeSlug($listData->description),
            ]
        );

        $messageDescription = "Body Type #{$bodyType->id}: {$listData->description}" . PHP_EOL;
        if($bodyType->wasRecentlyCreated) {
            echo "Created new {$messageDescription}";
        }
        else {
            echo "Skipped {$messageDescription}";
        }

        return $bodyType;
    }

    public function createBodyTypeProvider(BodyType $bodyType, BaseListData $listData) {
        return BodyTypeProvider::firstOrCreate([
                'hash' => $listData->getHash([
                    'body_type_id' => $bodyType->id,
                    'provider_id' => $this->getModelId(),
                ]),
            ], [
                'body_type_id' => $bodyType->id,
                'provider_id' => $this->getModelId(),
                'code' => $listData->value,
                'name' => $listData->description,
            ]);
    }

    public function run() {
        foreach($this->getBodyTypeList() as $listData) {
            $bodyType = $this->createBodyType($listData);

            $bodyTypeProvider = $this->createBodyTypeProvider($bodyType, $listData);

            $messageDescription = "Body Type provider ({$this->client->code()}): {$listData->description}" . PHP_EOL;
            if($bodyTypeProvider->wasRecentlyCreated) {
                echo "Created new {$messageDescription}";

                foreach($listData->extras as $extra) {
                    $bodyTypeProvider->detailables()
                        ->create($extra);
                }
            }
            else {
                echo "Skipped {$messageDescription}";
            }
        }
    }

}
