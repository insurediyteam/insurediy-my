<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('JPATH_BASE') or die;

/**
 * Clicks Field class for the Joomla Framework.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 * @since       1.6
 */
JLoader::import('incs.form.fields.customlabel', JPATH_ROOT);

class JFormFieldInsureDIYLifeTobacco extends JformFieldCustomLabel {

	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   1.6
	 */
	protected $type = 'InsureDIYLifeTobacco';

	/**
	 * Method to get the field input markup.
	 *
	 * @return  string	The field input markup.
	 * @since   1.6
	 */
	protected function getInput() {
		//$onchange	= ' onchange="document.id(\''.$this->id.'_unlimited\').checked=document.id(\''.$this->id.'\').value==\'\';"';
		//$onclick	= ' onclick="if (document.id(\''.$this->id.'_unlimited\').checked) document.id(\''.$this->id.'\').value=\'\';"';
		$inputClass = (isset($this->element['class'])) ? $this->element['class'] : "";
		$value = empty($this->value) ? 0 : $this->value;
		$checked = ' checked="checked" ';
		$class = ' class="checked" ';
		if (!$value) {
			$value = $this->default;
		}

//		$session = JFactory::getSession();
//		$data = $session->get('details');
//		if (isset($data['has_used_tobacco'])) {
//			$this->default = $data['has_used_tobacco'];
//		}


		return '<div class="insurediy-custom-radio">'
				. '<div class="insurediy-custom-radio-smoke-yes"><input class="' . $inputClass . '" type="radio" id="insurediy-smoke-1" value="1" name="' . $this->name . '" ' . (($value) ? $checked : '') . ' /><label for="insurediy-smoke-1" ' . (($value) ? $class : '') . '>&nbsp;</label></div>'
				. '<div class="insurediy-custom-radio-smoke-no"><input class="' . $inputClass . '" type="radio" id="insurediy-smoke-2" value="0" name="' . $this->name . '" ' . (($value) ? '' : $checked) . ' /><label for = "insurediy-smoke-2" ' . (($value) ? '' : $class) . '>&nbsp;</label></div>'
				. '<div style = "clear:both"></div>'
				. '</div>';
	}

}
