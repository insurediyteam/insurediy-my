<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_insurediylife
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediylife
 * @since       1.5
 */
class InsureDIYLifeControllerAjax extends JControllerForm {

	/**
	 * @since   1.6
	 */
	protected $view_item = 'ajax';

	/**
	 * @since   1.6
	 */
	protected $view_list = 'ajaxaddrow';

	/**
	 * The URL edit variable.
	 *
	 * @var    string
	 * @since  3.2
	 */
	protected $urlVar = 'a.id';

	/**
	 * Method to add a new record.
	 *
	 * @return  boolean  True if the article can be added, false if not.
	 * @since   1.6
	 */
	public function addRow() {
		require_once JPATH_COMPONENT . '/helpers/insurediylife.php';

		$app = JFactory::getApplication();
		$jinput = JFactory::getApplication()->input;

		$row = $jinput->get('row', '');
		$section = $jinput->get('section', '');

		switch ($section) {
			default:
				?>
				<script type="text/javascript">
					jQuery(document).ready(function() {
						/*setTimeout(function() {
						 // Do something after 1 seconds
						 jQuery('#existing-policy-insurance-contact-detail-<?php echo $row ?> select').chosen();
						 }, 1000);
						 */
					});
				</script>

				<div id="existing-policy-insurance-contact-detail-<?php echo $row ?>" style="margin-bottom:10px;padding-bottom:10px;border-bottom:1px solid #ccc;">
					<div>
						<div class="control-group-contact-detail-insurer-name">
							<div class="control-label"><label class="hasTooltip" for="jform_insurer_name">Name of Insurer <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="Name of Insurer"></label></div>
							<div class="controls"><input type="text" id="jform_insurer_name" name="jform[insurer_name][]" /></div>
						</div>
						<div class="control-group-contact-detail-policy-type">
							<div class="control-label"><label class="hasTooltip">Type of Policy <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="Sum Insured"></label></div>
							<div class="controls" style="width:100%;"><?php echo InsureDIYLifeHelper::getPolicyType() ?></div>
						</div>
						<div class="clear"></div>
					</div>

					<div>
						<div class="control-group-contact-detail-sum-insured">
							<div class="control-label"><label class="hasTooltip" for="jform_sum_insured">Sum Insured <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="Sum Insured"></label></div>
							<div class="controls" style="position:relative;">
								<div class="hkd-word">&nbsp;</div>
								<input type="text" id="jform_sum_insured" name="jform[sum_insured][]" value="" style="padding-left:40px;width:86%;" />
							</div>
						</div>
						<div class="control-group-contact-detail-issuance-date">
							<div class="control-label"><label class="hasTooltip" for="jform_issuance_date">Date of Issuance <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="Date of Issuance"></label></div>
							<div class="controls"><input type="text" id="jform_issuance_date" name="jform[issuance_date][]" /></div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
				<?php
				break;
		}
		$app->close();
		exit;
	}

	public function loadBranches() {
		$app = JFactory::getApplication();
		$response = array();

		$jinput = JFactory::getApplication()->input;
		$code = $jinput->get("code");
		$branches = $this->getBranches($code);
		if (!$branches) {
			$response['error'] = "Cannot load branches.";
		} else {
			$tmp = array();
			foreach ($branches as $branch) {
				$value = $branch['branch_code'];
				$text = $branch['branch_name'] . " - " . $branch['branch_code'] . "";
				$tmp[] = "<option value='{$value}'>" . $text . "</option>";
			}
			$response['data'] = implode(" ", $tmp);
		}
		echo json_encode($response);
		$app->close();
		exit;
	}

	protected function getBranches($code) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("branch_name, branch_code");
		$query->from("#__insure_branch");
		$query->where("code=" . $db->quote($code));
		$db->setQuery($query);
		return $db->loadAssocList();
	}

}
