<?php
defined('_JEXEC') or die('Restricted access');

jimport('libraries.legacy.model.legacy');

class PopupModelPopup extends JModelLegacy {
	public $_data = NULL;
	public $_total = NULL;
	public $_pagination = NULL;
	public $_filters = array();

	public function __construct() {
		parent::__construct();
	}

}
