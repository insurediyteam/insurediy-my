<?php
defined('_JEXEC') or die;

global $helper;

$prefix = 'Popup';
$helper = 'Site';
$helperName = "{$prefix}Helper{$helper}";

JLoader::import('helpers.helper', JPATH_COMPONENT);
$helper = new $helperName();

//add table path
JTable::addIncludePath(JPATH_COMPONENT .DS. 'tables');

$controller	= JControllerLegacy::getInstance($prefix);
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
