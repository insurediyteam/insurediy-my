<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Remarketing
 * @author     Yossava A.S <yossava.adhi@insurediy.com>
 * @copyright  2019
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\MVC\Controller\BaseController;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Remarketing', JPATH_COMPONENT);
JLoader::register('RemarketingController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = BaseController::getInstance('Remarketing');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
