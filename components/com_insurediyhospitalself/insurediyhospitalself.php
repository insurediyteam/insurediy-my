<?php

defined('_JEXEC') or die;
JLoader::import('incs.form.field', JPATH_ROOT);

require_once JPATH_COMPONENT . '/helpers/insurediyhospitalself.php';

$controller = JControllerLegacy::getInstance('InsureDIYHospitalSelf');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
