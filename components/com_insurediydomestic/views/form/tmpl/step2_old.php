<?php
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-plan-options select');
	JHtml::_('bootstrap.tooltip');
	
	$params = $this->state->get('params');
	$quotation = $this->quotation;
	
	$total_plans = count($this->policy_plans);
	$width = ($total_plans) ? 670 / $total_plans : 670;
	
	// remarketing email
	$user = $this->user;
	$products = [];
	$pol = [];
	foreach ($this->policy_plans as $key => $value) {
		$pol["premium"] = $value->premium;
		$pol["code"] = $value->insurer_code;
		array_push($products, $pol);
	}
	$mailQueue = (object)array(
		"email" => $user->email,
		"products" => $products,
		"type" => "dhi-before",
		"parent" => "dhi"
	);
	RemarketingHelpersRemarketing::newRemarketing($mailQueue);
	//end of remarketing email
?>
<script type="text/javascript">
	var checkboxHeight = "33";
	var radioHeight = "18";
	var selectWidth = "190";
	function chooseYourPlans() {
		var has_chosen = 0;
		$$('input[type="checkbox"]:checked').each(function (elem) {
			has_chosen = 1;
		});
		
		if (has_chosen == 0) {
			$('insurediy-popup-box').setStyle('display', 'block');
			} else {
			document.choosePlanForm.submit();
		}
	}
</script>
<div class="insurediy-form">
	<div class="header-top-wrapper">
		<?php echo InsureDIYDomesticHelper::renderHeader('icon-domestic', JText::_('COM_INSUREDIYDOMESTIC_PAGE_HEADING_YOUR_DOMESTIC_HELPER'), 2); ?>
	</div>
	<div class="insurediy-form-content" style="margin-top: 62px;">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<div class="edit<?php echo $this->pageclass_sfx; ?>">
			<form action="<?php echo JRoute::_('index.php?option=com_insurediydomestic'); ?>" method="post" name="choosePlanForm" id="choosePlanForm" class="form-validate form-vertical">
				<?php if (!empty($this->policy_plans)) : ?>
				<table class="insurediy-table-plans">
					<tr>
						<td class="td-first" style="width:350px;">
							<div class="premuim-text">Key Features</div>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner" style="position: relative;width: <?php echo $width ?>px;color:#fff;<?php echo (!empty($policy->color_code)?'background-color:'.$policy->color_code:''); ?>">
							<div class="center premuim-text">
								<?php echo $policy->key_features ?>
							</div>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr class="first">
						<td class="td-first">
							<!--<div class="ico-quotation-mark" style="padding-left: 30px;"><?php echo JText::_("COM_INSUREDIYDOMESTIC_YOUR_QUOTATIONS"); ?></div>-->
							<div class="premuim-text">Best Match for you</div>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner" style="position: relative;width: <?php echo $width ?>px;">
							<?php if ($policy->logo) : ?>
							<div class="center">
								<img src="<?php echo $policy->logo ?>" />
							</div>
							<?php endif; ?>
							<?php /*if (isset($policy->flag_banner) && strlen($policy->flag_banner) > 0): ?>
								<div style="position:absolute;top:0;"><img src="<?php echo $this->flag_banner_path . '/' . $policy->flag_banner ?>" /></div>
							<?php endif;*/ ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first premuim-text">
							<?php echo JText::_("COM_INSUREDIYDOMESTIC_FIELD_PREMIUM_TITLE"); ?> (<?php echo $this->currency; ?>) <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYDOMESTIC_FIELD_PREMIUM_DESC"); ?>"><br>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner premuim-text">
							<?php echo MyHelper::numberFormat($policy->premium); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<?php /*
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_DIY_POINTS_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_DIY_POINTS_DESC"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo MyHelper::numberFormat($policy->pur_points); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					*/ ?>
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_PERSONAL_ACCIDENT_TITLE"); ?> (<?php echo $this->currency; ?>) <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_PERSONAL_ACCIDENT_TOOLTIPS"); ?>"><br>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->personal_accident); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_PERSONAL_ACCIDENT_MED_EXP_TITLE"); ?> (<?php echo $this->currency; ?>) <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_PERSONAL_ACCIDENT_MED_EXP_TOOLTIPS"); ?>"><br>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->personal_accident_med_exp); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_PERSONAL_ACCIDENT_AMBULANCE_TITLE"); ?> (<?php echo $this->currency; ?>) <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_PERSONAL_ACCIDENT_AMBULANCE_TOOLTIPS"); ?>"><br>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->personal_accident_ambulance); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_REPATRIATION_TITLE"); ?> (<?php echo $this->currency; ?>) <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_REPATRIATION_TOOLTIPS"); ?>"><br>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->repatriation); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_HS_EXPENSE_TITLE"); ?> (<?php echo $this->currency; ?>) <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_HS_EXPENSE_TOOLTIPS"); ?>"><br>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->hs_expense); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_WEEKLY_BEN_TITLE"); ?> (<?php echo $this->currency; ?>) <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_WEEKLY_BEN_TOOLTIPS"); ?>"><br>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->weekly_ben); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_THIRD_PARTY_LIABILITY_TITLE"); ?> (<?php echo $this->currency; ?>) <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_THIRD_PARTY_LIABILITY_TOOLTIPS"); ?>"><br>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->third_party_liability); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_FIDELITY_PROTECTION_TITLE"); ?> (<?php echo $this->currency; ?>) <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_FIDELITY_PROTECTION_TOOLTIPS"); ?>"><br>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->fidelity_guarantee); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_PERSONAL_LIABILITY_TITLE"); ?> (<?php echo $this->currency; ?>) <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_PERSONAL_LIABILITY_TOOLTIPS"); ?>"><br>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->personal_liability); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					
					<tr>
						<td class="td-first" >
							<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_CI_OTHER_BENEFIT_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_CI_OTHER_BENEFIT_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner" >
							<?php echo InsureDIYHelper::printPlanAttr($policy->other_benefit); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					
					<tr>
						<td class="td-first" >
							<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_PRODUCT_BROCHURE_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_PRODUCT_BROCHURE_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner" >
							<?php echo ($policy->brochure) ? '<a href="' . $policy->brochure . '" target="_blank"><img src="templates/protostar/images/icon/ico-downloadfile.png" /></a>' : 'N/A' ?>
						</td>
						<?php endforeach; ?>
					</tr>
					
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_PURCHASE_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYDOMESTIC_PLAN_PURCHASE_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<button class="btn btn-primary btn-rounded" type="submit" name="plan_id" value="<?php echo $policy->plan_id; ?>"><?php echo JText::_("COM_INSUREDIYDOMESTIC_BUTTONS_APPLY_NOW"); ?></button>
						</td>
						<?php endforeach; ?>
					</tr>
				</table>
				<?php else : ?>
				<div><?php echo JText::_("NO_PLANS_MSG"); ?></div>
				<?php endif; ?>
				<input type="hidden" name="task" value="form.step2save" />
				<input type="hidden" name="quotation_id" value="<?php echo $quotation['id']; ?>" />
				<?php echo JHtml::_('form.token'); ?>
				
			<div style="float:left;margin:15px 0;">
			<div>{modulepos insurediy-download-adobe}</div>
			<div>{modulepos insurediy-secured-ssl}</div>
			</div>
			<div class="btn-toolbar" style="float:right">
			<button style="margin-right:10px;" type="button" onclick="javascript:document.choosePlanForm.task.value = 'form.back';
			document.choosePlanForm.submit();" class="btn btn-primary validate"><?php echo JText::_('BTN_BACK') ?></button>
			</div>
			<div class="clear"></div>
			</form>
			</div>
			</div>
			</div>
			
			<?php if (FALSE): ?>
			<tr>
			<td class="td-first"><?php echo JText::_("PLAN_PLANS_TITLE"); ?></td>
			<?php foreach ($this->policy_plans as $key => $policy) : ?>
			<td class="td-inner">
			<div class="center" style="font-family:montserrat-regular;font-size:14px;"><?php echo $policy->plan_name; ?></div>
			</td>
			<?php endforeach; ?>
			</tr>
			<?php endif; ?>			