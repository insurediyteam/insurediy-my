<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

require_once JPATH_COMPONENT . '/controller.php';

class UsersControllerPolicy extends UsersController {

	public function getModel($name = 'Policy', $prefix = 'UsersModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	public function submit() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app = JFactory::getApplication();

		$model = $this->getModel();
		$data = JRequest::getVar('jform', array(), 'post', 'array');
		$id = isset($data['id']) ? $data['id'] : FALSE;
		if ($data['id']) {
			$app->setUserState("users.policy.form.data.edit." . $id, $data);
		} else {
			$app->setUserState("users.policy.form.data", $data);
		}


		if (!$model) {
			JError::raiseError(500, "Invalid Request");
			return FALSE;
		}

		// Validate the posted data.
		$form = $model->getForm($data);
		if (!$form) {
			JError::raiseError(500, $model->getError());
			return FALSE;
		}

		// Validate the posted data.
		$validatedData = $model->validate($form, $data);
		// Check for errors.
		if ($validatedData === FALSE) {
			// Get the validation messages.
			$errors = $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++) {
				if ($errors[$i] instanceof Exception) {
					$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
				} else {
					$app->enqueueMessage($errors[$i], 'warning');
				}
			}
			if ($id) {
				MyUri::redirect("index.php?option=com_users&view=policy&id=" . $id);
			}
			MyUri::redirect("index.php?option=com_users&view=policy");
		}


		$result = $model->save($validatedData);

		// Check for errors.
		if ($result === FALSE) {
			MyUri::redirect("index.php?option=com_users&view=policy&layout=edit", $model->getError(), "warning");
		}
		if ($id) {
			$app->setUserState("users.policy.form.data.edit." . $id, NULL);
			MyUri::redirect("index.php?Itemid=136", "Policy successfully saved!");
		}
		$app->setUserState("users.policy.form.data", NULL);
		MyUri::redirect("index.php?Itemid=136", "Policy successfully added!");
	}

	public function delete() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app = JFactory::getApplication();
		$id = $app->input->get("id", FALSE);

		$msg = "";
		if ($id) {
			$model = $this->getModel();
			$model->delete($id);

			$msg = Jtext::_("COM_USERS_PORTFOLIO_POLICY_DELETE_SUCESSS_MSG");
		} else {
			$msg = Jtext::_("COM_USERS_PORTFOLIO_POLICY_DELETE_ERR_MSG");
		}
		MyUri::redirect("index.php?Itemid=136", $msg);
	}

	public function deleteall() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app = JFactory::getApplication();
		$ids = $app->input->get("cid", array(), "array");

		$msg = "";
		if ($ids) {
			$model = $this->getModel();
			$model->deleteall($ids);
			$count = count($ids);
			$msg = Jtext::sprintf("COM_USERS_PORTFOLIO_POLICIES_DELETE_SUCESSS_MSG", $count);
		} else {
			$msg = Jtext::_("COM_USERS_PORTFOLIO_POLICIES_DELETE_ERR_MSG");
		}
		MyUri::redirect("index.php?Itemid=136", $msg);
	}

	public function emailAll() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$user = JFactory::getUser();
		$config = JFactory::getConfig();
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("ip.insurer, ip.policy_number, ip.type, c.symbol AS currency, ip.cover_amt, ip.start_date, ip.end_date, ip.status");
		$query->from("#__insure_policies AS ip");
		$query->leftJoin("#__currencies AS c ON ip.currency = c.id");
		$query->where("user_id =" . $db->quote($user->id));
		$db->setQuery($query);
		$policies = $db->loadObjectList();
//		MyHelper::debug($policies);exit;
		$email = JFactory::getApplication()->input->get("email", "string", FALSE);
		$body = JText::_("PORTFOLIO_EMAIL_ALL_EMAIL_BODY");
		$body.= $this->getPolicyString($policies);
		$subject = JText::_("PORTFOLIO_EMAIL_ALL_EMAIL_SUBJECT");
		$recepient = ($email) ? explode(";", $email) : $user->email;
		$from = $config->get("mailfrom");
		$fromName = $config->get("fromname");
		$mailer = JFactory::getMailer();
		$mailer->sendMail($from, $fromName, $recepient, $subject, $body, TRUE);
		$msg = JText::_("PORTFOLIO_EMAIL_ALL_EMAIL_SUCCESS");
		MyUri::redirect("index.php?Itemid=136", $msg);
	}

	private function getPolicyString($policies) {
		$str = "<table><tr><td>No</td><td>Insurer</td><td>Policy Number</td><td>Type of Policy</td><td>Currency</td><td>Cover Amount</td><td>Policy Start Date</td><td>Policy End Date</td><td>Status</td></tr>";
		foreach ($policies as $key => $policy) {
			$str.= "<tr>";
			$str.= "<td>" . ($key + 1) . "</td>";
			$str.= "<td>" . $policy->insurer . "</td>";
			$str.= "<td>" . $policy->policy_number . "</td>";
			$str.= "<td>" . JText::_("POLICY_TYPE_" . $policy->type) . "</td>";
			$str.= "<td>" . $policy->currency . "</td>";
			$str.= "<td>" . $policy->cover_amt . "</td>";
			$str.= "<td>" . $policy->start_date . "</td>";
			$str.= "<td>" . $policy->end_date . "</td>";
			$str.= "<td>" . JText::_("COM_USERS_POLICY_FORM_STATUS_OPTION_" . $policy->status) . "</td>";
			$str.= "</tr>";
		}
		$str.= "</table>";
		return $str;
	}

}
