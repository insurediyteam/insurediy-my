<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

/**
 * Impressions Field class for the Joomla Framework.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 * @since       1.6
 */
class JFormFieldInsureDIYLifeGender extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   1.6
	 */
	protected $type = 'InsureDIYLifeGender';

	/**
	 * Method to get the field label markup.
	 *
	 * @return  string  The field label markup.
	 *
	 * @since   11.1
	 */
	protected function getLabel()
	{
		$label = '';
		$imghelpquote = '';
		
		if ($this->hidden)
		{
			return $label;
		}

		// Get the label text from the XML element, defaulting to the element name.
		$text = $this->element['label'] ? (string) $this->element['label'] : (string) $this->element['name'];
		$text = $this->translateLabel ? JText::_($text) : $text;

		// Build the class for the label.
		$class = !empty($this->description) ? 'hasTooltip' : '';
		$class = $this->required == true ? $class . ' required' : $class;
		$class = !empty($this->labelClass) ? $class . ' ' . $this->labelClass : $class;

		// Add the opening label tag and main attributes attributes.
		$label .= '<label id="' . $this->id . '-lbl" for="' . $this->id . '" class="' . $class . '"';
		$imghelpquote .= ' ';
		
		// If a description is specified, use it to build a tooltip.
		/*if (!empty($this->description))
		{
			
			JHtml::_('bootstrap.tooltip');
			$imghelpquote .= '&nbsp;<img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="' . JHtml::tooltipText(trim($text, ':'), JText::_($this->description), 0) . '" />' ;
			
		}
		*/

		// Add the label text and closing tag.
		if ($this->required)
		{
			$label .= '>' . $text . '<span class="star">&#160;*</span>'.$imghelpquote.'</label>';
		}
		else
		{
			$label .= '>' . $text . $imghelpquote . '</label>';
		}

		return $label;
	}
	
	/**
	 * Method to get the field input markup.
	 *
	 * @return  string	The field input markup.
	 * @since   1.6
	 */
	protected function getInput()
	{
		$value		= empty($this->value) ? '' : $this->value;
		$checked	= ' checked="checked" ' ;
		$class = ' class="checked" ';
		
		$db = JFactory::getDBO();
		$user	=	JFactory::getUser();
		
		$query = " SELECT gender FROM #__users WHERE id = '".$user->id."' ";
			$db->setQuery( $query );
			$gender = $db->loadResult();
		
		if($gender) {
			$this->default = $gender;
		}
		
		return '<div class="insurediy-custom-radio">'
					.'<div class="insurediy-custom-radio-gender-male"><input type="radio" id="insurediy-gender-male" '.(($this->default == 'M')?$checked:'').' value="M" name="'.$this->name.'" /><label for="insurediy-gender-male" style="padding-right:15px;" '.(($this->default == 'M')?$class:'').'>&nbsp;</label><div style="clear:both"></div></div>'
					.'<div class="insurediy-custom-radio-gender-female"><input type="radio" id="insurediy-gender-female" '.(($this->default == 'F')?$checked:'').' value="F" name="'.$this->name.'" /><label for="insurediy-gender-female" '.(($this->default == 'F')?$class:'').'>&nbsp;</label><div style="clear:both"></div></div>'
					.'<div style="clear:both"></div>'
					.'</div>';
	}
}
