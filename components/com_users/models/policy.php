<?php

defined('_JEXEC') or die;

class UsersModelPolicy extends JModelForm {

	public $_table_name = "#__insure_policies";

	public function getForm($data = array(), $loadData = true) {
		$form = $this->loadForm('com_users.policy', 'policy', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		return $form;
	}

	public function loadFormData() {
		$app = JFactory::getApplication();
		$id = $app->input->get('id', FALSE);
		if ($id) {
			$data = $app->getUserState('users.policy.form.data.edit.' . $id, array());
		} else {
			$data = $app->getUserState('users.policy.form.data', array());
		}

		if ($id) {
			$item = $this->getItem($id);
			foreach ($item as $k => $v) {
				$data[$k] = $v;
			}
		}
		$this->preprocessData('com_users.policy', $data);
		return $data;
	}

	public function populateState() {
		$params = JFactory::getApplication()->getParams('com_users');
		$this->setState('params', $params);
	}

	public function preprocessForm(JForm $form, $data, $group = 'fields') {
		parent::preprocessForm($form, $data, $group);
	}

	public function getTable($name = 'Policy', $prefix = 'InsureTable', $options = array()) {
		parent::getTable($name, $prefix, $options);
	}

	public function save($data = array()) {
		$user = JFactory::getUser();
		$data['user_id'] = $user->id;
		$data['start_date'] = ($data['start_date']) ? JDate::getInstance($data['start_date'])->toSql() : "0000-00-00";
		$data['end_date'] = ($data['end_date']) ? JDate::getInstance($data['end_date'])->toSql() : "0000-00-00";
		switch ($data['type']) {
			case "1":
				$data['room_type'] = "";
				$data['cover_type'] = "";
				break;
			case "2":
				$data['room_type'] = "";
				$data['cover_type'] = "";
				break;
			case "3":
				$data['currency'] = "";
				$data['cover_amt'] = "";
				break;
			case "4":
				$data['currency'] = "";
				$data['cover_amt'] = "";
				$data['room_type'] = "";
				$data['cover_type'] = "";
				break;
			case "5":
				$data['currency'] = "";
				$data['cover_amt'] = "";
				$data['room_type'] = "";
				$data['cover_type'] = "";
				break;
			case "6":
				$data['currency'] = "";
				$data['cover_amt'] = "";
				$data['room_type'] = "";
				$data['cover_type'] = "";
				break;
			default:
				break;
		}

		$obj = MyHelper::array2jObject($data);
		$table = JTable::getInstance("Policy", "InsureTable");
		$table->bind($obj);
		if (!$table->store()) {
			// Shit happens
		}
		return TRUE;
	}

	public function getItem($id) {
		$user = JFactory::getUser();
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_table_name)
				->where("id = " . $id)
				->where("user_id = " . $user->id);

		$db->setQuery($query);
		$result = $db->loadAssoc();
		return $result;
	}

	public function delete($id) {
		$user = JFactory::getUser();
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->delete($this->_table_name)
				->where("id = " . $id)
				->where("user_id = " . $user->id);
		$db->setQuery($query);
		$result = $db->execute();
		return $result;
	}

	public function deleteall($ids = array()) {
		if (count($ids) > 0) {
			$user = JFactory::getUser();
			$db = JFactory::getDbo();
			$query = $db->getQuery(TRUE)
					->delete($this->_table_name)
					->where("id IN (" . implode(", ", $ids) . ")")
					->where("user_id = " . $user->id);
			$db->setQuery($query);
			$result = $db->execute();
			return $result;
		}
		return FALSE;
	}

}
