<?php
// require_once(JPATH_ADMINISTRATOR.'/components/com_joomailermailchimpintegration/libraries/Mailchimp/MailchimpException.php');

// use Akeeba\Engine\Util\RandomValue;

defined('_JEXEC') or die;
class JoomailerMailchimpHelper{

    private static $apiKey = 'apikey feacc8d91ce8773aa96b81274f14fc80-us3';
    private static $beforeQuoteStore = 'ci_bq';
    private static $afterQuoteStore = 'ci_aq';

    public static function addCart($store, $customerEmail, $products = []){
        self::removeCart('ci_bq', $customerEmail);
        self::removeCart('ci_aq', $customerEmail);
        return self::createCart($store, $customerEmail, $products);
    }

    public static function travel_addCart($store , $customerId , $products=[], $insurance = "Travel", $checkout_url = "https://www.insurediy.com.sg/buy-now/travel-insurance.html"){
        self::removeCart('travel_product_list', $customerId);
        self::removeCart('travel_product', $customerId);
        return self::createCart($store , $customerId , $products, $insurance , $checkout_url);   
    }

    public static function dhi_addCart($store , $customerId , $products=[], $insurance = "DHI", $checkout_url = "https://www.insurediy.com.sg/buy-now/travel-insurance.html"){
        self::removeCart('dhi_product_list', $customerId);
        self::removeCart('dhi_product', $customerId);
        return self::createCart($store , $customerId , $products, $insurance , $checkout_url);   
    }

    public static function life_addCart($store , $customerId , $products=[], $insurance = "DHI", $checkout_url = "https://www.insurediy.com.sg/buy-now/travel-insurance.html"){
        self::removeCart('life_product_list', $customerId);
        self::removeCart('life_product', $customerId);
        return self::createCart($store , $customerId , $products, $insurance , $checkout_url);   
    }


    public static function existingCustomer($store, $email, $idReturn = false){
        $_getCustomer = '/ecommerce/stores/'.$store.'/customers?email_address=';
        $result = self::getCurl($_getCustomer.urlencode($email));
        if ($result["status"]) {
            if (isset($result["data"]->customers)) {
                if(count($result["data"]->customers) == 0){
                    return false;
                }else{
                    if ($idReturn) {
                        return $result["data"]->customers[0]["id"];
                    }else{
                        return true;
                    }
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public static function createCustomer($store = 'ci_bq', $email = "hazimomar33@gmail.com" ){
        $_createCustomer = '/ecommerce/stores/ci_bq/customers';
        $result  = self::postCurl($_createCustomer, array(
                                    "id" => $email,
                                    "email_address" => $email,
                                    "opt_in_status" => true
                                ));
        if ($result["status"]) {
            if (isset($result["data"]->detail)) {
                return array(
                    "status" => false,
                    "message" => $result["data"]->detail
                );
            }else{
                return ["status" => true,"id" => $email];
            }
        }else{
            return $result;
        }
    }

    public static function createCart($store , $customerId , $products=[], $insurance = "Car", $checkout_url = "https://www.insurediy.com.sg/buy-now/buy-car-insurance.html"){
        $_createCart = '/ecommerce/stores/'.$store.'/carts';

        $sub_array = array();
        if ($products & is_array($products)) {
            foreach($products as $product){
                if (isset($product["product_id"]) & isset($product["product_variant_id"])) {
                    if (self::validProductId($product["product_id"]) & self::validVariantId($product["product_variant_id"])) {
                        $price = 0;
                        $_productId = $product["product_id"];
                        $_product_variant_id = $product['product_variant_id'];
                        if (isset($product["price"]) & !empty($product["price"])) {
                            if ($product["price"]) {
                                //create individuals product
                                $_productData = self::findProduct($product["product_id"], $insurance);
                                if ($_productData) {
                                    $createResult = self::createIndividualsProduct($store, $customerId, $_productData["title"], $_productData["image_url"], $_productData["title"], $product["price"], $_productData["image_url"]);
                                    if ($createResult["status"]) {
                                        $_productId = $createResult["product_id"];
                                        $_product_variant_id = $createResult["product_variant_id"];
                                    }
                                }
                            }
                            $price = $product["price"];
                        }

                        $sub_array[] = [
                            "id" => $store.$customerId.$_productId,
                            "product_id" => $_productId,
                            "product_variant_id" => $_product_variant_id,
                            "quantity" => 1,
                            "price" => $price
                        ];
                    }else{
                        return ["status" => false, "message" => "Product Invalid!"];
                    }
                }else{
                    //create new Product
                    $title = $product["title"];
                    $image = self::getImageUrl($product["insurer"]);
                    $variants_title = $product["variant_title"];
                    $price = $product["price"];
                    $productCreated = self::createIndividualsProduct($store, $customerId, $title, $image, $variants_title, $price, $image);
                    if ($productCreated["status"]) {
                        $sub_array[] = [    
                            "id" => $store.$customerId.$productCreated["product_id"],
                            "product_id" => $productCreated["product_id"],
                            "product_variant_id" => $productCreated["product_variant_id"],
                            "quantity" => 1,
                            "price" => $price ? $price : 0
                        ];
                    }
                }
            }
        }else{
            return ["status" => false, "message" => "No Products Found!"];
        }

        $params = [
            "id" => $store.$customerId,
            "customer" => ["id" => $customerId, "email_address" => $customerId, "opt_in_status" => true],
            "currency_code" => "SGD",
            "checkout_url" => $checkout_url,
            "order_total" => 0,
            "lines" => $sub_array
        ];

        $result = self::postCurl($_createCart, $params);

        if ($result["status"]) {
            $data = $result["data"];
            if (isset($data->status)) {
                return ["status" => false, "message" => $data->detail];
            }else{
                return ["status" => true, "message" => json_encode($result)];
            }
        }else{
            return $result;
        }
    }

    public static function findProduct($id, $insurance = "Car"){
        $data = [
            [
                "id" => "tokio_new",
                "title" => "Tokio Marine ".$insurance." Insurance",
                "image_url" => "https://www.insurediy.com.sg/images/logo_companies/f7bg/7-SG-Logo-TokioMarine.jpg"
            ],
            [
                "id" => "tokio_renewal",
                "title" => "Renew Your Tokio Marine ".$insurance." Insurance",
                "image_url" => "https://www.insurediy.com.sg/images/logo_companies/f7bg/7-SG-Logo-TokioMarine.jpg"
            ],
            [
                "id" => "income_renewal",
                "title" => "Renew Your NTUC ".$insurance." Insurance",
                "image_url" => "https://www.insurediy.com.sg/images/logo_companies/f7bg/5-SG-Logo-INCOME.jpg"
            ],
            [
                "id" => "income_new",
                "title" => "NTUC ".$insurance." Insurance",
                "image_url" => "https://www.insurediy.com.sg/images/logo_companies/f7bg/5-SG-Logo-INCOME.jpg"
            ],
            [
                "id" => "liberty_new",
                "title" => "Liberty ".$insurance." Insurance",
                "image_url" => "https://www.insurediy.com.sg/images/logo_companies/f7bg/3-SG-Logo-Liberty.jpg"
            ],
            [
                "id" => "liberty_renewal",
                "title" => "Renew Your Liberty ".$insurance." Insurance",
                "image_url" => "https://www.insurediy.com.sg/images/logo_companies/f7bg/3-SG-Logo-Liberty.jpg"
            ],
            [
                "id" => "axa_renewal",
                "title" => "Renew Your AXA ".$insurance." Insurance",
                "image_url" => "https://www.insurediy.com.sg/images/logo_companies/f7bg/1-SG-Logo-AXA.jpg"
            ],
            [
                "id" => "axa_new",
                "title" => "AXA ".$insurance." Insurance",
                "image_url" => "https://www.insurediy.com.sg/images/logo_companies/f7bg/1-SG-Logo-AXA.jpg"
            ],
            [
                "id" => "hla_new",
                "title" => "HLA ".$insurance." Insurance",
                "image_url" => "https://www.insurediy.com.sg/images/logo_companies/f7bg/2-SG-Logo-HLA.jpg"
            ],
            [
                "id" => "hla_renewal",
                "title" => "Renew Your HLA ".$insurance." Insurance",
                "image_url" => "https://www.insurediy.com.sg/images/logo_companies/f7bg/2-SG-Logo-HLA.jpg"
            ],
            [
                "id" => "msig_renewal",
                "title" => "Renew Your MSIG ".$insurance." Insurance",
                "image_url" => "https://www.insurediy.com.sg/images/logo_companies/f7bg/4-SG-Logo-MISG.jpg"
            ],
            [
                "id" => "msig_new",
                "title" => "MSIG ".$insurance." Insurance",
                "image_url" => "https://www.insurediy.com.sg/images/logo_companies/f7bg/4-SG-Logo-MISG.jpg"
            ],
            [
                "id" => "sompo_new",
                "title" => "SOMPO ".$insurance." Insurance",
                "image_url" => "https://www.insurediy.com.sg/images/logo_companies/f7bg/6-SG-Logo-Sompo.jpg"
            ],
            [
                "id" => "sompo_renewal",
                "title" => "Renew Your SOMPO ".$insurance." Insurance",
                "image_url" => "https://www.insurediy.com.sg/images/logo_companies/f7bg/6-SG-Logo-Sompo.jpg"
            ],

        ];

        $key = array_search($id, array_column($data, 'id'));
        if ($key) {
            return $data[$key];
        }else{
            return false;
        }
    }

    private static function getImageUrl($insurer_code){

        switch (strtolower(trim($insurer_code))) {
            case 'tm':
                return "https://www.insurediy.com.sg/images/logo_companies/f7bg/7-SG-Logo-TokioMarine.jpg";
                break;
            case 'ntu':
                return "https://www.insurediy.com.sg/images/logo_companies/f7bg/5-SG-Logo-INCOME.jpg";
                break;
            case 'lib':
                return "https://www.insurediy.com.sg/images/logo_companies/f7bg/3-SG-Logo-Liberty.jpg";
                break;
            case 'axa':
                return "https://www.insurediy.com.sg/images/logo_companies/f7bg/1-SG-Logo-AXA.jpg";
                break;
            case 'hla':
                return "https://www.insurediy.com.sg/images/logo_companies/f7bg/2-SG-Logo-HLA.jpg";
                break;
            case 'msi':
                return "https://www.insurediy.com.sg/images/logo_companies/f7bg/4-SG-Logo-MISG.jpg";
                break;
            case 'som':
                return "https://www.insurediy.com.sg/images/logo_companies/f7bg/6-SG-Logo-Sompo.jpg";
                break;
            
            default:
                return "https://www.insurediy.com.sg/images/logo_companies/f7bg/insurediy-logo.jpg";
                break;
        }
    }

    public static function createIndividualsProduct($store, $email, $title, $image_url, $variants_title, $price, $variant_image){
        $now = new Datetime();
        $productId = $now->format('U').rand();
        $product_variant_id = $email.$now->format('U');
        $params = [
            "id" => $productId,
            "title" => $title,
            "image_url" => $image_url,
            "variants" => [
                [
                    "id" => $product_variant_id,
                    "title" => $variants_title,
                    "price" => $price,
                    "image_url" => $variant_image
                ]
            ],
        ];  
        
        $result = self::postCurl("/ecommerce/stores/".$store."/products", $params);

        if ($result) {
            $data = $result["data"];
            if (isset($data->status)) {
                return ["status" => false, "message" => $data->detail];
            }else{
                return ["status" => true, "product_id" => $productId, "product_variant_id" => $product_variant_id];
            }
        }else{
            return ["status"=> false];
        }
    }

    public static function removeCart($store, $customerId){
        $cartId = $store.$customerId;
        $deleteCart = '/ecommerce/stores/'.$store.'/carts/'.$cartId.'';

        $result = self::customCurl($deleteCart, "DELETE");
        if ($result["status"]) {
            $data = $result["data"];
            if ($data) {
                if (isset($data->status)) {
                    return ["status" => false, "message" => $data->detail];
                }
            }

            return ["status" => true, "message" => ""];
        }else{
            return $result;
        }
    }

    private static function validProductId($product_id){
        $_ids = ["tokio_new", "tokio_renewal", "income_renewal", "income_new", "liberty_new", "liberty_renewal", "axa_renewal", "axa_new", "hla_new", "hla_renewal", "msig_renewal", "msig_new", "sompo_new", "sompo_renewal"];
        foreach($_ids as $key){
            if ($product_id == $key) {
                return true;
            }
        }
        return false;
    }

    private static function validVariantId($product_variant_id){
        $_variants = ["tokio01", "income01", "liberrty01", "axa01", "hla01", "msig01", "msig02", "sompo01", "sompo02", "sompo03"];
        foreach($_variants as $key){
            if ($product_variant_id == $key) {
                return true;
            }
        }

        return false;
    }

    public static function existingCart($store = 'ci_bq', $customerId = "", $idReturn = false){
        $cartId = $store.$customerId;
        $getCart = '/ecommerce/stores/'.$store.'/carts/'.$cartId.'';
        $result = self::getCurl($getCart);

        if ($result["status"]) {
            $data = $result["data"];
            if (isset($data->status)) {
                return ["status" => false, "message" => $data->detail];
            }else{
                if ($idReturn) {
                    return ["status" => true, "cartId" => $data->id];
                }else{
                    return ["status" => true, "message" => ""];
                }
            }
        }else{
            return $result;
        }
    }

    private static function getCurl($url){
        $_apiUrl = 'https://'. substr(self::$apiKey, -3).'.api.mailchimp.com/3.0';
        $_apiUrl .= $url;

        $ch = curl_init($_apiUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_USERAGENT, 'Joomlamailer/3.0');
        
        // // curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', "Authorization: ".self::$apiKey));
        // curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        if ($error) {
            return array(
                "status" => false,
                "message" => $error
            );
        }else{
            return array(
                "status" => true,
                "data" => json_decode($output)
            );
        }
    }

    private static function postCurl($url, $params){
        $_apiUrl = 'https://'. substr(self::$apiKey, -3).'.api.mailchimp.com/3.0';
        $_apiUrl .= $url;

        $ch = curl_init($_apiUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_USERAGENT, 'Joomlamailer/3.0');
        
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', "Authorization: ".self::$apiKey));
        // curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        if ($error) {
            return array(
                "status" => false,
                "message" => $error
            );
        }else{
            return array(
                "status" => true,
                "data" => json_decode($output)
            );
        }
    }

    private static function customCurl($url, $custom, $params = []){
        $_apiUrl = 'https://'. substr(self::$apiKey, -3).'.api.mailchimp.com/3.0';
        $_apiUrl .= $url;

        $ch = curl_init($_apiUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $custom);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Joomlamailer/3.0');
        if ($params) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', "Authorization: ".self::$apiKey));
        // curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        if ($error) {
            return array(
                "status" => false,
                "message" => $error
            );
        }else{
            return array(
                "status" => true,
                "data" => json_decode($output)
            );
        }   
    }

    public static function createProduct($store, $_apiKey = null){
        $url = "/ecommerce/stores/".$store."/products";
        if ($_apiKey) {
            $apiKey = $_apiKey;
        }else{
            $apiKey = self::$apiKey;
        }
        $products = [
            [
                "product_id" => "tokio_new",
                "product_variant_id" => "tokio01",
                "title" => "Tokio Marine Car Insurance",
                "variant_title" => "tokio_01",
                "description" => "Buy Tokio Marine car insurance with Us. Guaranteed best price!",
                "image" => self::getImageUrl('tm')
            ],
            [
                "product_id" => "tokio_renewal",
                "title" => "Renew Your Tokio Marine Car Insurance",
                "description" => "Buy Tokio Marine car insurance with Us. Guaranteed best price!",
                "image" => self::getImageUrl('tm'),
                "product_variant_id" => "tokio01",
                "variant_title" => "tokio_01"
            ],
            [
                "product_id" => "income_renewal",
                "title" => "Renew Your Income Car Insurance",
                "description" => "Buy Income car insurance with Us. Guaranteed best price!",
                "image" => self::getImageUrl('ntu'),
                "product_variant_id" => "income01",
                "variant_title" => "income_01"
            ],
            [
                "product_id" => "income_new",
                "title" => "Income Car Insurance",
                "description" => "Buy Income car insurance with Us. Guaranteed best price!",
                "image" => self::getImageUrl('ntu'),
                "product_variant_id" => "income01",
                "variant_title" => "income_01"
            ],
            [
                "product_id" => "liberty_new",
                "title" => "Liberty Car Insurance",
                "description" => "Buy Liberty car insurance with Us. Guaranteed best price!",
                "image" => self::getImageUrl('lib'),
                "product_variant_id" => "liberrty01",
                "variant_title" => "liberty_01"
            ],
            [
                "product_id" => "liberty_renewal",
                "title" => "Renew Your Liberty Car Insurance",
                "description" => "Buy Liberty car insurance with Us. Guaranteed best price!",
                "image" => self::getImageUrl('lib'),
                "product_variant_id" => "liberrty01",
                "variant_title" => "liberty_01"
            ],
            [
                "product_id" => "axa_renewal",
                "title" => "Renew Your AXA Car Insurance",
                "description" => "Buy AXA car insurance with Us. Guaranteed best price!",
                "image" => self::getImageUrl('axa'),
                "product_variant_id" => "axa01",
                "variant_title" => "axa_01"
            ],
            [
                "product_id" => "axa_new",
                "title" => "AXA Car Insurance",
                "description" => "Buy AXA car insurance with Us. Guaranteed best price!",
                "image" => self::getImageUrl('axa'),
                "product_variant_id" => "axa01",
                "variant_title" => "axa_01"
            ],
            [
                "product_id" => "hla_new",
                "title" => "HLA Car Insurance",
                "description" => "Buy HLA car insurance with Us. Guaranteed best price!",
                "image" => self::getImageUrl('hla'),
                "product_variant_id" => [[
                    "variant_id" => "hla01",
                    "title" => "HLA Car Protect360",
                ]],
            ],
            [
                "product_id" => "hla_renewal",
                "title" => "Renew Your HLA Car Insurance",
                "description" => "Buy HLA car insurance with Us. Guaranteed best price!",
                "image" => self::getImageUrl('hla'),
                "product_variant_id" => [[
                    "variant_id" => "hla01",
                    "title" => "HLA Car Protect360",
                ]],
                "variant_title" => "axa_01"
            ], 
            [
                "product_id" => "msig_renewal",
                "title" => "Renew Your MSIG Car Insurance",
                "description" => "Buy MSIG car insurance with Us. Guaranteed best price!",
                "image" => self::getImageUrl('msi'),
                "product_variant_id" => [
                    [
                        "variant_id" => "msig01",
                        "title" => "motorMax",
                    ],
                    [
                        "variant_id" => "msig02",
                        "title" => "motorMax Plus",
                    ],

                ],
            ],
            [
                "product_id" => "msig_new",
                "title" => "MSIG Car Insurance",
                "description" => "Buy MSIG car insurance with Us. Guaranteed best price!",
                "image" => self::getImageUrl('msi'),
                "product_variant_id" => [
                    [
                        "variant_id" => "msig01",
                        "title" => "motorMax",
                    ],
                    [
                        "variant_id" => "msig02",
                        "title" => "motorMax Plus",
                    ],

                ],
            ],
            [
                "product_id" => "sompo_new",
                "title" => "SOMPO Car Insurance",
                "description" => "Buy SOMPO insurance with Us. Guaranteed best price!",
                "image" => self::getImageUrl('som'),
                "product_variant_id" => [
                    [
                        "variant_id" => "sompo01",
                        "title" => "Focus",
                    ],
                    [
                        "variant_id" => "sompo02",
                        "title" => "Gold",
                    ],
                    [
                        "variant_id" => "sompo03",
                        "title" => "Prestege",
                    ]

                ],
            ],
            [
                "product_id" => "sompo_renewal",
                "title" => "Renew Your SOMPO Car Insurance",
                "description" => "Buy SOMPO insurance with Us. Guaranteed best price!",
                "image" => self::getImageUrl('som'),
                "product_variant_id" => [
                    [
                        "variant_id" => "sompo01",
                        "title" => "Focus",
                    ],
                    [
                        "variant_id" => "sompo02",
                        "title" => "Gold",
                    ],
                    [
                        "variant_id" => "sompo03",
                        "title" => "Prestege",
                    ]

                ],
            ],
        ];

        foreach($products as $product){
            $params = [
                "id" => $product["product_id"],
                "title" => $product["title"], 
                "handle" => "car_insurance",
                "url" => "https://www.insurediy.com.sg/buy-now/buy-car-insurance.html",
                "description" => $product["description"],
                "type" => "insurance",
                "vendor" => "insurediy",
                "image_url" => $product["image"]
            ];
            $params["variants"] = array();

            if (is_array($product["product_variant_id"])) {
                $sub_array = array();
                foreach($product["product_variant_id"] as $key){
                    $sub_array[] = [
                        "id" => $key["variant_id"], 
                        "title" => $key["title"],
                        "url" => "https://www.insurediy.com.sg/buy-now/buy-car-insurance.html", 
                        "sku" => "", 
                        "price" => 0, 
                        "inventory_quantity" => 100000000, 
                        "image_url" => $product["image"], 
                        "backorders" => "false", 
                        "visibility" => "visible",
                    ];
                }
                $params["variants"] = $sub_array;
            }else{
                $params["variants"][] = [
                    "id" => $product["product_variant_id"], 
                    "title" => $product["variant_title"],
                    "url" => "https://www.insurediy.com.sg/buy-now/buy-car-insurance.html", 
                    "sku" => "", 
                    "price" => 0, 
                    "inventory_quantity" => 100000000, 
                    "image_url" => $product["image"], 
                    "backorders" => "false", 
                    "visibility" => "visible",
                ];
            }

            $_apiUrl = 'https://'. substr($apiKey, -3).'.api.mailchimp.com/3.0';
            $_apiUrl .= $url;

            $ch = curl_init($_apiUrl);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_USERAGENT, 'Joomlamailer/3.0');
            
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', "Authorization: ".$apiKey));
            // curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $output = curl_exec($ch);
            $error = curl_error($ch);
            curl_close($ch);

            if ($error) {
                 var_dump($error);
            }else{
                if (isset($output->status)) {
                    print_r($output);
                }else{
                    print_r('True');
                }
            }
        }
    }

    public static function getUserEmailByQuotation($remarks ,$request_id){
        $db = JFactory::getDbo();
       
        if ($remarks == "motor") {
            $query = $db->getQuery(TRUE)
            ->select("userId")
            ->from("#__insure_motor_quote_master")
            ->where("`quote_payment_request_id` = '" . $db->quote($request_id) . "' AND " . "quote_deleted_time IS NULL");
            $db->setQuery($query);
            $userId = $db->loadResult();

            if ($userId) {
                $details = self::getUserDetail($userId);
                if ($detail) {
                    return $detail["email"];
                }
            }else{
                return false;
            }
        }else{
            $query = $db->getQuery(TRUE)
                        ->select("email")
                        ->from("#__insure_".$remarks."_quotations")
                        ->where("unique_order_no = ". $db->quote($qid));
            $quotation = $db->setQuery($query)->loadAssoc();
            if ($quotation) {
                return $quotation["email"];
            }else{
                return false;
            }
        }

    }

    private static function getUserDetail($userId){
        $db = JFactory::getDbo();
        return $db->setQuery($db->getQuery(TRUE)
                        ->select("*")
                        ->from("#__users")
                        ->where("id = " . $db->quote($userId)))->loadAssoc();
    }

}