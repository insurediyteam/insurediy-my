<?php

defined('_JEXEC') or die;

class CalculatorHelper {

	public static function getLifeSumInsured() {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("sum_insured")
				->from("#__insure_life_sum_insured");
		return $db->setQuery($query)->loadAssocList();
	}
}
