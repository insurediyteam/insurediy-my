<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_calculator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

// Create shortcut to parameters.
$params = $this->params;
$return = $this->return;
$Itemid = $this->Itemid;
?>
<div class="item-page faq">
	<div class="page-header ">
		<h1>
			<i class="icon-calculator"></i><?php echo JText::_("COM_CALCULATOR_LIFE_HEADER"); ?></h1>
		<div class="clear"></div>
	</div>
	<div class="padding1520 clearfix">
		<div style="width:22%;float:left;">&nbsp; <?php echo MyHelper::load_module_pos("faq-menu-html"); ?></div>
		<div style="width: 78%; float: left;">
			<div class="edit<?php echo $this->pageclass_sfx; ?>">
				<?php //echo MyHelper::renderDefaultMessage(); ?>
				<div style="font-weight:bold;font-size:16px;">
					Please log in or register to proceed.
				</div>
				<fieldset class="calculator-login">
					<form action="<?php echo JRoute::_('index.php', true); ?>" method="post" id="login-form" class="form-inline form-validate">
						<h2>Login</h2>
						<div class="userdata">
							<div style="height: 130px;">
								<div id="form-login-username" class="control-group">
									<div class="controls">
										<div class="input-prepend">
											<input id="modlgn-username" type="text" name="username" class="input-small required validate-username" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_EMAIL') ?>" />
										</div>
									</div>
								</div>
								<div id="form-login-password" class="control-group">
									<div class="controls">
										<div class="input-prepend">
											<input id="modlgn-passwd" type="password" name="password" class="input-small required" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" />
										</div>
									</div>
								</div>
							</div>
							<div style="margin-top:29px;">
								<div id="form-login-submit" class="control-group" style="float:left">
									<div class="controls">
										<button type="submit" tabindex="0" name="Submit" class="btn btn-primary" style="padding:5px 20px;font-size:14px !important;height:30px;"><?php echo JText::_('JSUBMIT') ?></button>
									</div>
								</div>
								<div class="clear"></div>
							</div>
							<input type="hidden" name="option" value="com_calculator" />
							<input type="hidden" name="task" value="user.login" />
							<input type="hidden" name="return" value="<?php echo $return; ?>" />
							<input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>" />
							<?php echo JHtml::_('form.token'); ?>
						</div>
					</form>
				</fieldset>
				<div style="width:5%;float: left">
					<div class="mid-line"></div><div style="text-align: center;"><a href="#" onclick="return false;" class="btn btn-primary or-btn">OR</a></div><div class="mid-line"></div>
				</div>
				<fieldset class="calculator-register">
					<form action="<?php echo JRoute::_('index.php', true); ?>" method="post" id="register-form" class="form-inline form-validate">
						<h2>Register</h2>
						<div class="userdata">
							<div style="height: 130px;">
								<div id="form-login-username" class="control-group">
									<div class="controls">
										<div class="input-prepend">
											<input id="email" type="text" name="email" class="input-small required validate-email" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_EMAIL') ?>" />
										</div>
									</div>
								</div>
								<div id="form-login-password" class="control-group">
									<div class="controls">
										<div class="input-prepend">
											<input id="password" type="password" name="password" class="input-small required" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" />
										</div>
									</div>
								</div>
								<div id="form-login-password" class="control-group">
									<div class="controls">
										<div class="input-prepend">
											<input id="password2" type="password" name="password2" class="input-small required" tabindex="0" size="18" placeholder="Confirm <?php echo JText::_('JGLOBAL_PASSWORD') ?>" />
										</div>
									</div>
								</div>
							</div>
							<div style="margin-top:29px;">
								<div id="form-login-submit" class="control-group" style="float:left">
									<div class="controls">
										<button type="submit" tabindex="0" name="Submit" class="btn btn-primary" style="padding:5px 20px;font-size:14px !important;height:30px;"><?php echo JText::_('JSUBMIT') ?></button>
									</div>
								</div>
								<div class="clear"></div>
							</div>
							<?php $usersConfig = JComponentHelper::getParams('com_users'); ?>

							<input type="hidden" name="option" value="com_calculator" />
							<input type="hidden" name="task" value="user.register" />
							<input type="hidden" name="return" value="<?php echo $return; ?>" />
							<input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>" />
							<?php echo JHtml::_('form.token'); ?>
						</div>
					</form>
				</fieldset>
				<div class="clear"></div>
				<div style="float:left;margin:15px 0;">
					{modulepos insurediy-secured-ssl}
				</div>
				<div class="clear"></div>
				</form>
			</div>
		</div>
	</div>
</div>
