<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_calculator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Calculator model.
 *
 * @package     Joomla.Site
 * @subpackage  com_calculator
 * @since       1.6
 */
class CalculatorModelHospital extends JModelForm {

	/**
	 * Model typeAlias string. Used for version history.
	 *
	 * @var        string
	 */
	public $typeAlias = 'com_calculator.hospital';
	public $_tb_hospital = "#__calculator_hospital";

	/**
	 * Get the return URL.
	 *
	 * @return  string	The return URL.
	 * @since   1.6
	 */
	public function getReturnPage() {
		return base64_encode($this->getState('return_page'));
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 */
	protected function populateState() {
		$app = JFactory::getApplication();

		$return = $app->input->get('return', null, 'base64');

		if (!JUri::isInternal(base64_decode($return))) {
			$return = null;
		}

		$this->setState('return_page', base64_decode($return));

		// Load the parameters.
		$params = $app->getParams();
		$this->setState('params', $params);

		$this->setState('layout', $app->input->get('layout'));
	}

	public function getForm($data = array(), $loadData = TRUE) {
		// Get the form.
		$form = $this->loadForm('com_calculator.form', 'hospital', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	public function getItem() {
		$user = JFactory::getUser();
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__calculator_hospital")
				->where("user_id = " . $user->id);
		return $db->setQuery($query)->loadObject();
	}

	public function save($post) {
		$db = JFactory::getDBO();
		$user = JFactory::getUser();

		$query = $db->getQuery(TRUE)
				->select("count(*)")
				->from($this->_tb_hospital)
				->where("user_id = " . $user->id);

		$isNew = $db->setQuery($query)->loadResult();

		$postObj = MyHelper::array2jObject($post);

		if ($isNew) {
			$db->updateObject($this->_tb_hospital, $postObj, "user_id");
		} else {
			$db->insertObject($this->_tb_hospital, $postObj, "id");
		}
		return TRUE;
	}

}
