<?php
defined('_JEXEC') or die;
?>
<div class="insurediy-form">
	<form action="index.php?option=com_insurediy&task=payment.feedme" method="post" enctype="multipart/form-data">
		<div style="padding: 25px;">
			<?php foreach ($this->form->getFieldset("basic") as $field): ?>
				<div>
					<div style="width:200px;float: left;"><?php echo $field->label; ?></div>
					<div style="width:300px;float: left;"><?php echo $field->input; ?></div>
				</div>
				<div style="clear:both;"></div>
			<?php endforeach; ?>
			<div class="row-fluid">
				<div class="span3"></div>
				<div class="span9"><input type="submit" value="Submit" /></div>
			</div>
		</div>
	</form>
</div>

<?php exit; ?>