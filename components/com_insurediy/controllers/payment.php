<?php //MY

use Models\Eloquent\Quote;

defined('_JEXEC') or die;
JLoader::import("components.com_insurediytravel.models.form", JPATH_ROOT);
JLoader::import("components.com_insurediytravel.helpers.insurediytravel", JPATH_ROOT);

JLoader::import("components.com_insurediydomestic.models.form", JPATH_ROOT);
JLoader::import("components.com_insurediydomestic.helpers.insurediydomestic", JPATH_ROOT);

class InsureDIYControllerPayment extends JControllerForm {

	public function feedme() {
		echo "OK"; // let AsiaPay know we are cool.
		// Payment Statuses  (payment_status or payment_status)
		// C = Complete
		// N = No payment yet (default)
		// X = Cancelled/Error
		// P = Pending
		$app = JFactory::getApplication();
		$jInput = $app->input;
		$post = $app->input->post->getArray();

		if ($post) {
			// Save the feed
			$feedTable = JTable::getInstance("PaydollarFeed", "InsureTable");
			$feedTable->bind($post);
			$feedTable->store();

			$successcode = $jInput->get('successcode');
			if ($successcode != "0") { // unsuccessful payment, ignored.
				exit;
			}

			// Successful Payment, save the transaction record.
			$remark = $jInput->get('remark');
			$ref = $jInput->get('Ref');

			$validRemarks = array("life", "hospitalself", "hospitalchild", "travel", "domestic", "ci", 'motor');
			$quote_stages = array(// this is last stage of each insurances
				"life" => 7,
				"ci" => 7,
				"hospitalself" => 6,
				"hospitalchild" => 7,
				"travel" => 6,
				"domestic" => 5,
				'motor' => 5,
			);

			$transactionTable = JTable::getInstance("Transaction", "InsureTable");
			$transactionTable->feed_id = $feedTable->id;
			$transactionTable->unique_order_no = $ref;
			$transactionTable->successcode = $successcode;
			$transactionTable->PayRef = $jInput->get('PayRef');
			$transactionTable->Amt = $jInput->get('Amt');
			$transactionTable->TxTime = $jInput->get('TxTime');
			$transactionTable->remark = $remark;

			if (!in_array($remark, $validRemarks)) { // error with remark
				$transactionTable->comment = "Error with remark!";
				$transactionTable->store();
				exit;
			}

			if($remark !== 'motor') {
				$table = JTable::getInstance($remark, "InsureTable");
				if (!$table->load(array("unique_order_no" => $ref))) { // quotation not found
					$transactionTable->comment = "Quotation Not Found!";
					$transactionTable->store();
					exit;
				}
				
				$table->payment_status = "C";
				$table->quote_status = 1;
				$table->quote_stage = $quote_stages[$remark];
			}
			else {
				if($table = Quote::where('payment_request_id', $ref)->first()) {
					$table->payment_stage = 'Success';
					$table->payment_amount = $transactionTable->Amt;
					$table->payment_txn_id = $transactionTable->feed_id;
				}
				else {
					$transactionTable->comment = "Quotation Not Found!";
					$transactionTable->store();
					exit;
				}
			}
			$quotation_id = $table->id;
			// All checkings are done.
			$transactionTable->success = 1;
			$transactionTable->store();
			$table->store();

			// After payment stuffs
			$pdfarr = array(array()); // empty attachments

			/*
			 * Need to send the ack mails here, since travel insurance flow ends after payment status
			 */
			if (strtolower($remark) == "travel") {
				/** @var InsureDIYTravelModelForm $model */
				$model = $this->getModel("Form", "InsureDIYTravelModel");
				$model->sendEmails($pdfarr, TRUE, $quotation_id);
				sleep(5);
				$model->sendEmails($pdfarr, FALSE, $quotation_id);
				$model->createPolicyRecord($quotation_id);
				$model->createPoints($quotation_id);
				exit;
			}

			/*
			 * Need to send the ack mails here, since domestic insurance flow ends after payment status
			 */
			if (strtolower($remark) == "domestic") {
				/** @var InsureDIYDomesticModelForm $model */
				$model = $this->getModel("Form", "InsureDIYDomesticModel");
				$model->sendEmails($pdfarr, TRUE, $quotation_id);
				sleep(5);
				$model->sendEmails($pdfarr, FALSE, $quotation_id);
				$model->createPolicyRecord($quotation_id);
				$model->createPoints($quotation_id);
				exit;
			}

			if (strtolower($remark) == "motor") {
				/** @var Quote $table */
				$table->finishPayment();
				exit;
			}
		}
		exit;
	}

}
