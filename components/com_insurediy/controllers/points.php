<?php

/**
 * @package     Joomla.Administrator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
require_once JPATH_COMPONENT . '/controller.php';

/**
 * Weblink controller class.
 *
 * @package     Joomla.Administrator
 * @since       1.6
 */
class InsureDIYControllerPoints extends JControllerForm {

	public function updateStatus() {
		$params = JComponentHelper::getParams("COM_INSUREDIYREFERRAL");
		$autoApprove = $params->get("auto_approve", TRUE);
		if ($autoApprove) {
			$days = $params->get("day_to_approve", 10);
			$db = JFactory::getDBO();
			$query = $db->getQuery(TRUE)
					->update("#__insure_points")
					->set("status = 'A'")
					->where("status = 'P'")
					->where("DATEDIFF(NOW(), created) > " . $days);
			$db->setQuery($query)->execute();
		}
		exit;
	}

}
