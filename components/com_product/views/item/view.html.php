<?php

defined('_JEXEC') or die;

class ProductViewItem extends JViewLegacy {

	protected $item;

	public function display($tpl = null) {
		$model = $this->getModel();
		$id = JRequest::getInt("id", FALSE);
		$error = FALSE;
		if (!$id) {
			MyUri::redirect('index.php?option=com_product', "No Product selected.");
		}
		$this->item = $model->getItem($id);
		// Check for errors.
		if (!$this->item) {
			$error = "Work in progress, coming to you soon…";
		}
		parent::display($tpl);
	}

}
