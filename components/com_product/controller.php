<?php

defined('_JEXEC') or die;

class ProductController extends JControllerLegacy {

	public function display($cachable = false, $urlparams = false) {
		global $reqView, $reqLayout;
//		MyUri::redirect("index.php?option=com_product");
		$needLogin = FALSE;
		switch ($reqView) {
			case 'list':
				$model = "list";
				$xtraModel = "search";
				break;
			case 'item':
				$model = "item";
				break;
			default:
				if (empty($reqView)) {
					$reqView = 'list';
				}
		}

		if ($needLogin) {
			$my = JFactory::getUser();
			if (1 == $my->get('guest') || $my->id < 1) {
				MyUri::toLogin('', 'current'); // Redirect to login page
			}
		}


		if (($view = $this->getView($reqView, 'html'))) {
			$document = JFactory::getDocument();
			$model = $this->getModel((!empty($model)) ? $model : $reqView);

			if (isset($xtraModel) && $xtraModel) {
				if (is_array($xtraModel)) {
					foreach ($xtraModel as $x) {
						$view->setModel($this->getModel($x), FALSE);
					}
				} else {
					$view->setModel($this->getModel($xtraModel), FALSE);
				}
			}

			// Push the model into the view (as default)
			$view->setModel($model, TRUE);
			$view->setLayout($reqLayout);
			$view->assignRef('document', $document);
			$view->display();
		}
	}

}
