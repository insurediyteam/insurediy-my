<?php

defined('_JEXEC') or die;

global $reqOption, $reqView, $reqLayout;

$reqOption = 'com_product';
$reqView = JRequest::getCmd('view', 'list');
$reqLayout = JRequest::getCmd('layout', 'default');

$prefix = 'Product';

$controller = JControllerLegacy::getInstance($prefix);
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();