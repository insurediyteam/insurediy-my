<?php

defined('_JEXEC') or die;

class MySimpleFPDIHelper {

	public static function generate($source, $xml, $id, $name, $bg = TRUE) {
		if (!($xml instanceof SimpleXMLElement)) {
			return FALSE;
		}

		$pdf = new FPDI();

		$pageCount = $pdf->setSourceFile($source);

		$presetImageList = array("black-check" => JPATH_SITE . "/images/ico-checked-black.png", "black-check-mini" => JPATH_SITE . "/images/ico-checked-black-mini.png");
		$family = 'Inconsolata-Regular';

		for ($n = 1; $n <= $pageCount; $n++) {
			$pdf->AddPage();
			if ($bg) {
				$tplIdx = $pdf->importPage($n, '/TrimBox');
				$pdf->useTemplate($tplIdx, 0, 0);
			}

			$fields = $xml->xpath("descendant::page[@name=" . $n . "]");
			if (!isset($fields[0])) {
				continue;
			}
			$font_size = isset($fields[0]['font_size']) ? (int) $fields[0]['font_size'] : 12;
			$image = isset($fields[0]['image']) ? (string) $fields[0]['image'] : "black-check";
			$pdf->SetFont($family);
			$pdf->SetFontSize($font_size);
			$pdf->SetTextColor(0, 0, 0);

			foreach ($fields[0] as $field) {
				switch ((string) $field['type']) {
					case "text":
						$chunk_split = (int) $field['chunk_split'];
						$x = (int) $field['x'];
						$y = (int) $field['y'];

						$style = isset($field['style']) ? (string) $field['style'] : "";
						$value = (string) $field['value'];
						$field_font_size = isset($field['font_size']) ? (int) $field['font_size'] : $font_size;

						$pdf->SetXY($x, $y);
						$pdf->SetFont($family, $style);
						$pdf->SetFontSize($field_font_size);
						if ($chunk_split) {
							$pdf->Write(0, chunk_split($value, 1, '  '));
						} else {
							$pdf->Write(0, $value);
						}
						break;
					case "image":
						$img = isset($presetImageList[(string) $field['img']]) ? $presetImageList[(string) $field['img']] : $presetImageList[$image];
						if (isset($field['value'])) {
							$value = (string) $field['value'];
							$option = $field->xpath("descendant::option[@value='" . strtolower($value) . "']");
							$x = (int) $option[0]['x'];
							$y = (int) $option[0]['y'];
							if ($x && $y) {
								$pdf->Image($img, $x, $y);
							}
						}
						break;
					default:
						break;
				}
			}
		}
		JLoader::import('joomla.filesystem.folder');
		$deepPath = MyHelper::getDeepPath(LIFE_PDF_SAVE_PATH, $id);
		$path = JPATH_SITE . DS . $deepPath . $name;
		$pdf->Output($path, 'F');
		return $name;
	}

}
