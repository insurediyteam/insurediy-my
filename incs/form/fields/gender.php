<?php

defined('JPATH_BASE') or die;

JFormHelper::loadFieldClass('radio');

class JFormFieldGender extends JFormFieldRadio {

	protected $type = 'Gender';

	protected function getInput() {
		$inputClass = (isset($this->element['class'])) ? (string) $this->element['class'] : "";
		$value = empty($this->value) ? $this->default : $this->value;
		$checked = ' checked="checked" ';
		$class = ' class="checked" ';

		$inputstr = "";
		$inputstr.= '<div class="idy-radio clearfix">';
		// Male
		$inputstr.= '<div class="idy-radio-wrapper idy-radio-gender-male">';
		$inputstr.= '<input class="' . $inputClass . '" type="radio" id="' . $this->id . '-insurediy-gender-male" ' . (($value == 'M') ? $checked : '') . ' value="M" name="' . $this->name . '" />';
		$inputstr.= '<label for="' . $this->id . '-insurediy-gender-male" ' . (($value == 'M') ? $class : '') . '>' . JText::_("GENDER_MALE") . '</label>';
		$inputstr.= '</div>';
		// Female
		$inputstr.= '<div class="idy-radio-wrapper idy-radio-gender-female">';
		$inputstr.= '<input class="' . $inputClass . '" type="radio" id="' . $this->id . '-insurediy-gender-female" ' . (($value == 'F') ? $checked : '') . ' value="F" name="' . $this->name . '" />';
		$inputstr.= '<label for="' . $this->id . '-insurediy-gender-female" ' . (($value == 'F') ? $class : '') . '>' . JText::_("GENDER_FEMALE") . '</label>';
		$inputstr.= '</div>';

		$inputstr.= '</div>';

		return $inputstr;
	}

}
