<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('JPATH_BASE') or die;

/**
 * Impressions Field class for the Joomla Framework.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 * @since       1.6
 */
JLoader::import('incs.form.fields.customlabel', JPATH_ROOT);

class JFormFieldGroupType extends JformFieldCustomLabel {

	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   1.6
	 */
	protected $type = 'GroupType';

	protected function getInput() {

		$inputClass = (isset($this->element['class'])) ? (string) $this->element['class'] : "";
		$value = empty($this->value) ? $this->default : $this->value;
		$checked = ' checked="checked" ';
		$class = ' class="checked" ';

		$inputstr = "";
		$inputstr .= '<div class="idy-radio clearfix">';

		$inputstr .= '	<div class="idy-radio-wrapper idy-radio-just-me">';
		$inputstr .= '		<input class="' . $inputClass . '" type="radio" id="insurediy-just-me" ' . (($value == 'JM') ? $checked : '') . ' value="JM" name="' . $this->name . '" />';
		$inputstr .= '		<label for="insurediy-just-me" ' . (($value == 'JM') ? $class : '') . '>' . JText::_("GROUP_TYPE_JM") . '</label>';
		$inputstr .= '	</div>';

		$inputstr .= '	<div class="idy-radio-wrapper idy-radio-with-group">';
		$inputstr .= '		<input class="' . $inputClass . '" type="radio" id="insurediy-with-group" ' . (($value == 'WG') ? $checked : '') . ' value="WG" name="' . $this->name . '" />';
		$inputstr .= '		<label for="insurediy-with-group" ' . (($value == 'WG') ? $class : '') . '>' . JText::_("GROUP_TYPE_WG") . '</label>';
		$inputstr .= '	</div>';

		$inputstr .= '	<div class="idy-radio-wrapper idy-radio-with-family">';
		$inputstr .= '		<input class="' . $inputClass . '" type="radio" id="insurediy-with-family" ' . (($value == 'WF') ? $checked : '') . ' value="WF" name="' . $this->name . '" />';
		$inputstr .= '		<label for="insurediy-with-family" ' . (($value == 'WF') ? $class : '') . '>' . JText::_("GROUP_TYPE_WF") . '</label>';
		$inputstr .= '	</div>';

		$inputstr .= '</div>';

		return $inputstr;

//		$value = empty($this->value) ? $this->default : $this->value;
//		$checked = ' checked="checked" ';
//
//		return '<div class="idy-radio">'
//				. '<div class="idy-radio-just-me"><input type="radio" id="insurediy-just-me" ' . (($value == 'JM') ? $checked : '') . ' value="JM" name="' . $this->name . '" /><label for="insurediy-just-me">&nbsp;</label></div>'
//				. '<div class="idy-radio-with-group"><input type="radio" id="insurediy-with-group" ' . (($value == 'WG') ? $checked : '') . ' value="WG" name="' . $this->name . '" /><label for="insurediy-with-group">&nbsp;</label></div>'
//				. '<div class="idy-radio-with-family"><input type="radio" id="insurediy-with-family" ' . (($value == 'WF') ? $checked : '') . ' value="WF" name="' . $this->name . '" /><label for="insurediy-with-family">&nbsp;</label></div>'
//				. '<div style="clear:both"></div>'
//				. '</div>';
	}

}
