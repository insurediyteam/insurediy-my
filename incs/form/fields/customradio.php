<?php

defined('JPATH_BASE') or die;

JFormHelper::loadFieldClass('radio');

class JFormFieldCustomRadio extends JFormFieldRadio {

	protected $type = 'CustomRadio';

}
