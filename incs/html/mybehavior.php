<?php

defined('JPATH_PLATFORM') or die;

abstract class JHtmlMyBehavior {

	protected static $loaded = array();

	public static function jQuery() {
		if (isset(self::$loaded[__METHOD__])) {
			return;
		}

		JHtml::_('behavior.framework', TRUE); //load mootool first

		JHtml::_('Jquery.framework', TRUE);

		$jsUrls = MyUri::getUrls('js_scripts');
		JHtml::_('script', $jsUrls['jquery_noConflict'], FALSE, FALSE);

		//set before addScriptDeclaration avoid recursion
		self::$loaded[__METHOD__] = TRUE;
	}

	public static function jQueryUi() {
		if (isset(self::$loaded[__METHOD__])) {
			return;
		}

		self::jQuery(); //include jquery
//		JHtml::_('Jquery.ui');

		$jsUrls = MyUri::getUrls('js_scripts');
		$cssUrls = MyUri::getUrls('css_scripts');
		JHtml::_('script', $jsUrls['jquery_ui'], FALSE, FALSE);

		//css
		JHtml::_('stylesheet', $cssUrls['jquery_ui'], array(), FALSE);

		self::$loaded[__METHOD__] = TRUE;
	}

	public static function jsInsurediy() {
		if (isset(self::$loaded[__METHOD__])) {
			return;
		}

//		self::jQuery(); //include jquery

		$jsUrl = MyUri::getUrls('js');
		JHtml::_('script', "{$jsUrl}insurediy.js", FALSE, FALSE);

		self::$loaded[__METHOD__] = TRUE;
	}
}
