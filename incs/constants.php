<?php

defined('_JEXEC') or die('Restricted access');

defined('LIFE_PDF_SAVE_PATH') or define('LIFE_PDF_SAVE_PATH', "media/com_insurediylife/forms");
defined('CI_PDF_SAVE_PATH') or define('CI_PDF_SAVE_PATH', "media/com_insurediyci/forms");
defined('TRAVEL_PDF_SAVE_PATH') or define('TRAVEL_PDF_SAVE_PATH', "media/com_insurediytravel/forms");
defined('HOSPITALSELF_PDF_SAVE_PATH') or define('HOSPITALSELF_PDF_SAVE_PATH', "media/com_insurediyhospitalself/forms");
defined('HOSPITALCHILD_PDF_SAVE_PATH') or define('HOSPITALCHILD_PDF_SAVE_PATH', "media/com_insurediyhospitalchild/forms");
defined('DOMESTIC_PDF_SAVE_PATH') or define('DOMESTIC_PDF_SAVE_PATH', "media/com_insurediydomestic/forms");
defined('MOTOR_PDF_SAVE_PATH') or define('MOTOR_PDF_SAVE_PATH', "media/com_insurediymotor/forms");

defined('SESSION_KEY_REFID') or define('SESSION_KEY_REFID', "insurediy.refid");

defined('ERR_WHAT_ARE_YOU_TRYING_TO_DO') or define('ERR_WHAT_ARE_YOU_TRYING_TO_DO', "Hi, what are you trying to do?");

