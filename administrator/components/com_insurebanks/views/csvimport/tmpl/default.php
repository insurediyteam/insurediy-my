<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cedeledepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>

<div class="row-fluid">
	<?php if (!empty($this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
	<?php endif; ?>
	<div class="span10 form-horizontal">
		<fieldset>
			<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'import')); ?>

			<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'import', JText::_('COM_INSUREBANKS_CSV_IMPORT_DETAILS')); ?>
			<form action="<?php echo JRoute::_('index.php?option=com_insurebanks&task=csvimport.import'); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data" class="form-validate">
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('banks'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('banks'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('branches'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('branches'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"></div>
					<div class="controls"><input type="submit" value="<?php echo JText::_("JGLOBAL_BUTTON_IMPORT"); ?>" /></div>
				</div>
				<input type="hidden" name="task" value="csvimport.import" />
				<?php echo JHtml::_('form.token'); ?>
			</form>
			<?php echo JHtml::_('bootstrap.endTab'); ?>
			<?php echo JHtml::_('bootstrap.endTabSet'); ?>
		</fieldset>
	</div>
	<!-- End Weblinks -->
	<!-- Begin Sidebar -->
	<?php //echo JLayoutHelper::render('joomla.edit.details', $this);  ?>
	<!-- End Sidebar -->
</div>		

