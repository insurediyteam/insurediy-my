<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_insurediytravel
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Insurediy Travel model.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_insurediytravel
 * @since       1.5
 */
class InsureDIYTravelModelCSVExport extends JModelAdmin {

	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_INSUREDIYTRAVEL';
	protected $_tb_quotation = "#__insure_travel_quotations";
	protected $_tb_quotation_plan = "#__insure_travel_quotations_to_plans";
	protected $_tb_traveller = "#__insure_travel_quotations_to_travellers";
	protected $_tb_paydollar_log = "#__paydollar_feed_log";

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record) {
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return;
			}
			$user = JFactory::getUser();

			if ($record->catid) {
				return $user->authorise('core.delete', 'com_insurediytravel.category.' . (int) $record->catid);
			} else {
				return parent::canDelete($record);
			}
		}
	}

	/**
	 * Method to test whether a record can have its state changed.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to change the state of the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canEditState($record) {
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_insurediytravel.category.' . (int) $record->catid);
		} else {
			return parent::canEditState($record);
		}
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   type	The table type to instantiate
	 * @param   string	A prefix for the table class name. Optional.
	 * @param   array  Configuration array for model. Optional.
	 * @return  JTable	A database object
	 * @since   1.6
	 */
	public function getTable($type = 'Plan', $prefix = 'InsureDIYTravelTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true) {
		// Get the form.
		$form = $this->loadForm('com_insurediytravel.csvexport', 'csvexport', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}
		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 * @since   1.6
	 */
	protected function loadFormData() {
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_insurediytravel.edit.plan.data', array());

		if (empty($data)) {
			$data = $this->getItem();
			if ($this->getState('plan.id') == 0) {

			}
		}

		$this->preprocessData('com_insurediytravel.plan', $data);
		return $data;
	}

	public function getItem($pk = null) {
		if ($item = parent::getItem($pk)) {

			if (!empty($item->id)) {

			}
		}

		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since   1.6
	 */
	protected function prepareTable($table) {

	}

	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param   object	A record object.
	 * @return  array  An array of conditions to add to add to ordering queries.
	 * @since   1.6
	 */
	protected function getReorderConditions($table) {
		$condition = array();

		return $condition;
	}

	/**
	 * Method to save the form data.
	 *
	 * @param   array  $data  The form data.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since	3.1
	 */
	public function save($data) {
		$jinput = JFactory::getApplication()->input->getArray($_POST);
		$data['id'] = $jinput['jform']['id'];

		$return = parent::save($data);

		return $return;
	}

	public function export_csv() {
		ini_set('memory_limit', '-1'); //**************IMPORTANT

		$app = JFactory::getApplication();
		$db = JFactory::getDBO();

		$jform = $app->input->post->get('jform', '', 'filter');
		$headers_array = array();
		$datatype = $jform['export_type'];
		set_time_limit(0);
		$field_names = array(
			"plan_name",
			"insurer_code",
			"trip_type",
			"group_type",
			"zone_type",
			"no_of_days",
			"no_of_travellers",
			"no_of_adults",
			"no_of_children",
			"no_of_s_children",
			"key_features",
			"premium",
			"premium_original",
			"medical_expenses",
			"emergency_me",
			"personal_accident",
			"personal_property",
			"baggage_delay",
			"interruption",
			"cancellation",
			"trip_rerouting",
			"rental_vehicle",
			"liability",
			"amateur_sport",
			"other_benefits",
			"tnc AS brochure",
			"state AS Published",
			"ref_points",
			"pur_points"
		);

		switch ($datatype) {
			case 'plans':
				$query = $db->getQuery(TRUE)
						->select($field_names)
						->from("#__insure_travel_plans");
				$db->setQuery($query);
				$items = $db->loadAssocList();
				$headers_array = array_keys(reset($items));
				break;
			case 'all_quotations':
			case 'completed_quotations':
			case 'pending_quotations':
				$query = $db->getQuery(TRUE)
						->select("q.*, qp.insurer_code, qp.plan_name, qp.premium, pl.Amt as premium_collected, qp.zone_type, qp.key_features")
						->from($this->_tb_quotation . " AS q")
						->leftjoin($this->_tb_quotation_plan . " AS qp ON q.id = qp.quotation_id")
						->leftjoin($this->_tb_paydollar_log . " AS pl ON pl.Ref = q.unique_order_no")
						->order("q.id", "ASC");

				if ($datatype == "completed_quotations") {
					$query->where("q.quote_status = 1");
				}
				if ($datatype == "pending_quotations") {
					$query->where("q.quote_status != 1");
				}

				if (strlen($jform['date_start']) > 0 && strlen($jform['date_end']) > 0) {
					$sdate = new JDate($jform['date_start']);
					$edate = new JDate($jform['date_end']);
					$query->where("DATE(" . $db->quote($sdate->toSql()) . ') <= DATE(q.created_date)');
					$query->where('DATE(q.created_date) <= DATE(' . $db->quote($edate->toSql()) . ")");
				} elseif (strlen($jform['date_start']) > 0) {
					$sdate = new JDate($jform['date_start']);
					$query->where('DATE(q.created_date) >= DATE(' . $db->quote($sdate->toSql()) . ")");
				} elseif (strlen($jform['date_end']) > 0) {
					$edate = new JDate($jform['date_end']);
					$query->where('DATE(q.created_date) <= DATE(' . $db->quote($edate->toSql()) . ")");
				}

				$db->setQuery($query);
				$quotations = $db->loadAssocList();
			
				if (empty($quotations)) {
					break;
				}
				
				// Travellers
				$query->clear();
				$query->select("*")->from("#__insure_travel_quotations_to_travellers");

				$travellers = $db->loadObjectList();

				$groupedTravellers = array();
				foreach ($travellers as $k => $traveller) { $groupedTravellers[$traveller->quotation_id][] = $traveller;}			
				
				// Beneficiaries
				$query->clear();
				$query->select("*")->from("#__insure_travel_beneficiaries");

				$beneficiaries = $db->loadObjectList();
				
				$groupedBeneficiaries = array();
				foreach ($beneficiaries as $ben) { $groupedBeneficiaries[$ben->quotation_id][] = $ben;}
				
				$headers_array = array();
				
				foreach($quotations as $k => &$quotation) {
					
					if(!empty($groupedTravellers[$quotation['id']])) {
						foreach($groupedTravellers[$quotation['id']] as $t => $traveller) {
							$quotation['travel_firstname_'.$t] = $traveller->firstname;
							$quotation['travel_lastname_'.$t] = $traveller->lastname;
							$quotation['travel_gender_'.$t] = $traveller->gender;
							$quotation['travel_identity_type_'.$t] = $traveller->identity_type;
							$quotation['travel_id_no_'.$t] = $traveller->id_no;
							$quotation['travel_relation_'.$t] = $traveller->relation;
							$quotation['travel_dob_'.$t] = $traveller->dob;
							$quotation['travel_valid_pass_type_'.$t] = $traveller->valid_pass_type;
							$quotation['travel_valid_pass_no_'.$t] = $traveller->valid_pass_no;
						}
					}
					
					if(!empty($groupedBeneficiaries[$quotation['id']])) {
						foreach($groupedBeneficiaries[$quotation['id']] as $t => $ben) {
							$quotation['ben_name_'.$t] = $ben->ben_name;
							$quotation['ben_identity_card_no_'.$t] = $ben->ben_identity_card_no;
							$quotation['ben_gender_'.$t] = $ben->gender;
							$quotation['ben_address_'.$t] = $ben->ben_address;
							$quotation['ben_relationship_'.$t] = $ben->ben_relationship;
							$quotation['ben_nationality_'.$t] = $ben->ben_nationality;
							$quotation['ben_dob_'.$t] = $ben->ben_dob;
							$quotation['ben_share_'.$t] = $ben->ben_percent_share;
							$quotation['ben_valid_pass_type_'.$t] = $ben->ben_valid_pass_type;
							$quotation['ben_valid_pass_no_'.$t] = $ben->ben_valid_pass_no;
						}
					}
					
					// Collect all headers
					$header = array_keys($quotation);
					$headers_array = array_unique(array_merge($headers_array, $header));
				}
				
				// Consolidate them
				$items = array();
				
				foreach ($quotations as $k => $qt) {

					$consolidate = array();
					foreach($headers_array as $header) {
						$consolidate[$header] = (!empty($qt[$header])?$qt[$header]:'');
					}
					$items[] = $consolidate;
					
				}
				
			break;
			case 'policies':
				$fields = array(
					'p.insurer',
					'p.policy_number',
					'c.code AS currency',
					'p.cover_amt',
					'p.start_date',
					'p.end_date',
					'p.status'
				);
				
				$query = $db->getQuery(TRUE)
					->select($fields)
					->from("#__insure_policies AS p ")
					->join("left", "#__currencies AS c ON c.id = p.currency ")
					->where("p.type = 4 ");
				
				$db->setQuery($query);
				$items = $db->loadAssocList();
				
				foreach($items as $i => &$item) {
					$item['status'] = InsureDIYHelper::getPoliciesStatus($item['status']);
				}
			
			break;
		}

		if (count($items) == 0) {
			echo 'No Items';
		} else {

			ob_start();

$df = fopen("php://output", 'w');

			fputcsv($df, $headers_array);

			foreach ($items as $row) {
				switch ($datatype) {
					case 'plans':
						break;
					case 'all_quotations' :
					case 'completed_quotations':
					case 'pending_quotations':
						$row['created_date'] = JHtml::_('date', $row['created_date'], 'd-m-Y H:i');
						break;
					default:
						break;
				}

				fputcsv($df, $row);
			}
			fclose($df);
		}


		// disable caching
		$filename = 'travel_' . $datatype . '_' . JHtml::date(NULL, 'EXPORT_DATE_FORMAT') . '.csv';

		$now = gmdate("D, d M Y H:i:s");
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
		header("Last-Modified: {$now} GMT");

		// force download
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");

		// disposition / encoding on response body
		header("Content-Disposition: attachment;filename={$filename}");
		header("Content-Transfer-Encoding: binary");

		return true;
	}

	function exportQuotations() {
		$csvData = $this->getCSVData();
		$csvTitle = JHtml::date('now', "d_M_Y_H_i_s") . "_space_bookings.csv";
		MyHelper::fileDownload($csvTitle, $csvData);
		return true;
	}

	public function getCSVData($completedOnly, $pendingOnly) { // this is nasty. need to fix later.
		$items = $this->getItems($completedOnly, $pendingOnly);
		$qids = array_keys($items);
		$temps = $this->getTravellers($qids);
		$travellers = array();
		foreach ($temps as $temp) {
			$travellers[$temp['quotation_id']][] = $temp;
		}

		foreach ($travellers as $key => $q_travellers) {
			if (isset($items[$key])) {
				foreach ($q_travellers as $qkey => $q_traveller) {
					foreach (array_keys($q_traveller) as $tkey) {
						if ($tkey != "traveller_id" && $tkey != "quotation_id") {
							$field_key = $tkey . "_" . ($qkey + 1);
							$items[$key][$field_key] = $q_traveller[$tkey];
						}
					}
				}
			}
		}
		$headers = array();
		foreach ($items as $item) {
			if (count(array_keys($item)) > count(array_keys($headers))) {
				$headers = array_keys($item);
			}
		}

		$csv = array();
		foreach ($items as $item) {
			$tmp = array();
			foreach ($item as $k => $v) {
				$tmp[] = $v;
			}
			$csv[] = implode(',', MyHelper::escapeCSV($tmp, TRUE));
		}

		$csvMasterData = array(
			implode(',', MyHelper::escapeCSV($headers, TRUE)), //csv header
			implode("\n", $csv) //csv items
		);
		return (!empty($csvMasterData)) ? implode("\n", $csvMasterData) : '';
	}

	public function getItems($completedOnly, $pendingOnly) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("q.*, qp.insurer_code, qp.plan_name, qp.premium")
				->from($this->_tb_quotation . " AS q")
				->leftjoin($this->_tb_quotation_plan . " AS qp ON q.id = qp.quotation_id")
				->order("q.id", "ASC");
		if ($completedOnly) {
			$query->where("q.quote_status = 1");
		}
		if ($pendingOnly) {
			$query->where("q.quote_status = 0");
		}
		return $db->setQuery($query)->loadAssocList("id");
	}

	public function getTravellers($quotations) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_traveller)
				->where("quotation_id IN (" . implode(",", $quotations) . ")");
		return $db->setQuery($query)->loadAssocList();
	}

}
