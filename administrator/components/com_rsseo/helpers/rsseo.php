<?php
/**
* @package RSSeo!
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Restricted access');
require_once JPATH_ADMINISTRATOR.'/components/com_rsseo/helpers/version.php';

class rsseoHelper {
	
	// Get component configuration
	public static function getConfig($name = null, $default = null) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		static $config;
		
		if (empty($config)) {
			$query->clear();
			$query->select($db->qn('params'));
			$query->from($db->qn('#__extensions'));
			$query->where($query->qn('type') . ' = ' . $db->q('component'));
			$query->where($query->qn('element') . ' = ' . $db->q('com_rsseo'));
			$db->setQuery($query);
			$params = $db->loadResult();
			
			$registry = new JRegistry;
			$registry->loadString($params);
			$config = $registry->toObject();
		}
		
		if ($name != null) {
			if (isset($config->$name)) { 
				return $config->$name;
			} else {
				if (!is_null($default))
					return $default;
				else
					return false;
			}
		}
		else return $config;
	}
	
	// Update configuration
	public static function updateConfig($name, $value) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$query->select($db->qn('extension_id'))->select($db->qn('params'))
			->from($db->qn('#__extensions'))
			->where($query->qn('type') . ' = ' . $db->q('component'))
			->where($query->qn('element') . ' = ' . $db->q('com_rsseo'));
		$db->setQuery($query);
		if ($extension = $db->loadObject()) {
			$registry = new JRegistry;
			$registry->loadString($extension->params);
			$registry->set($name, $value);
				
			$query->clear()
				->update($db->qn('#__extensions'))
				->set($db->qn('params'). ' = '.$db->q((string) $registry->toString()))
				->where($db->qn('extension_id'). ' = '. $db->q($extension->extension_id));
			
			$db->setQuery($query);
			$db->execute();
		}
	}
	
	// Get key code for update
	public static function genKeyCode() {
		$code = rsseoHelper::getConfig('global_register_code');
		$version = new RSSeoVersion();
		return md5($code.$version->key);
	}
	
	// Check for Joomla! version
	public static function isJ3() {
		return version_compare(JVERSION, '3.0', '>=');
	}
	
	// Add backend submenus
	public static function addSubmenu($vName) {
		$layout = JFactory::getApplication()->input->getCmd('layout');
		
		JHtmlSidebar::addEntry(JText::_('COM_RSSEO_MENU_DASHBOARD'),		'index.php?option=com_rsseo',						($vName == '' || $vName == 'default') && $layout != 'update');
		JHtmlSidebar::addEntry(JText::_('COM_RSSEO_MENU_SEO_PERFORMANCE'),	'index.php?option=com_rsseo&view=competitors',		$vName == 'competitors');
		JHtmlSidebar::addEntry(JText::_('COM_RSSEO_MENU_PAGES'),			'index.php?option=com_rsseo&view=pages',			$vName == 'pages');
		JHtmlSidebar::addEntry(JText::_('COM_RSSEO_MENU_CRAWLER'),			'index.php?option=com_rsseo&view=crawler',			$vName == 'crawler');
		JHtmlSidebar::addEntry(JText::_('COM_RSSEO_MENU_SITEMAP' ),			'index.php?option=com_rsseo&view=sitemap',			$vName == 'sitemap');
		JHtmlSidebar::addEntry(JText::_('COM_RSSEO_MENU_ERRORS'),			'index.php?option=com_rsseo&view=errors',			$vName == 'errors');
		JHtmlSidebar::addEntry(JText::_('COM_RSSEO_MENU_ERROR_LINKS'),		'index.php?option=com_rsseo&view=errorlinks',		$vName == 'errorlinks');
		JHtmlSidebar::addEntry(JText::_('COM_RSSEO_MENU_REDIRECTS'),		'index.php?option=com_rsseo&view=redirects',		$vName == 'redirects');
		JHtmlSidebar::addEntry(JText::_('COM_RSSEO_MENU_KEYWORDS' ),		'index.php?option=com_rsseo&view=keywords',			$vName == 'keywords');
		JHtmlSidebar::addEntry(JText::_('COM_RSSEO_MENU_BACKUP_RESTORE'),	'index.php?option=com_rsseo&view=backup',			$vName == 'backup');
		JHtmlSidebar::addEntry(JText::_('COM_RSSEO_MENU_ANALYTICS'),		'index.php?option=com_rsseo&view=analytics',		$vName == 'analytics');
		JHtmlSidebar::addEntry(JText::_('COM_RSSEO_MENU_UPDATE'),			'index.php?option=com_rsseo&layout=update',			$layout == 'update');
	}
	
	// Set scripts and stylesheets
	public static function setScripts($from) {
		$doc = JFactory::getDocument();
		
		if ($from == 'administrator') {
			JHtml::_('behavior.framework');
			$doc->addScript(JURI::root(true).'/administrator/components/com_rsseo/assets/js/rsseo.js?v='.RSSEO_REVISION);
			$doc->addStyleSheet(JURI::root(true).'/administrator/components/com_rsseo/assets/css/style.css?v='.RSSEO_REVISION);
			
			if (rsseoHelper::isJ3()) {
				$doc->addStyleSheet(JURI::root(true).'/administrator/components/com_rsseo/assets/css/j3.css?v='.RSSEO_REVISION);
				JHtml::_('formbehavior.chosen', 'select');
			} else {
				$doc->addStyleSheet(JURI::root(true).'/administrator/components/com_rsseo/assets/css/j2.css?v='.RSSEO_REVISION);
			}
		}
	}
	
	// Main function to get content
	public static function fopen($url, $headers = 1, $test = false, $onlyHeaders = false) {
		require_once JPATH_ADMINISTRATOR.'/components/com_rsseo/helpers/http.php';
		
		$config	= rsseoHelper::getConfig();
		$proxy	= false;
		
		if ($config->proxy_enable) {
			$proxy = array();
			$proxy['proxy_server'] = $config->proxy_server;
			$proxy['proxy_port'] = $config->proxy_port;
			$proxy['proxy_usrpsw'] = $config->proxy_username.':'.$config->proxy_password;
		}
		
		$options	= array('test' => $test, 'url' => $url, 'proxy' => $proxy);
		$http		= rsseoHttp::getInstance($options);
		
		if ($onlyHeaders) {
			return $http->getStatus();
		}
		
		if ($test) {
			return $http->getErrors();
		}
		
		$response 	= $http->getResponse();
		
		if (empty($response)) {
			return 'RSSEOINVALID';
		}
		
		return $response;
	}
	
	// Convert time in a readable format
	public static function convertseconds($sec) {
		$text = '';

		$hours = intval(intval($sec) / 3600); 
		$text .= str_pad($hours, 2, "0", STR_PAD_LEFT). ":";

		$minutes = intval(($sec / 60) % 60); 
		$text .= str_pad($minutes, 2, "0", STR_PAD_LEFT). ":";

		$seconds = intval($sec % 60); 
		$text .= str_pad($seconds, 2, "0", STR_PAD_LEFT);

		return $text;
	}
	
	// Convert a timestamp to a years-days format
	public static function convertage($time) {
		$years	= floor($time / 31556926);
		$days	= floor(($time % 31556926) / 86400);
		
		if ($years == '1') {
			$y = '1 '.JText::_('COM_RSSEO_YEAR');
		} else {
			$y = $years.' '.JText::_('COM_RSSEO_YEARS');
		}
		
		if ($days == '1') {
			$d = '1 '.JText::_('COM_RSSEO_DAY');
		} else {
			$d = $days.' '.JText::_('COM_RSSEO_DAYS');
		}
		
		return $y.', '.$d;
	}
	
	// Copy keywords to density keywords
	public static function keywords() {
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$config = rsseoHelper::getConfig();
		
		if ($config->copykeywords) {
			$query->clear();
			$query->update($db->qn('#__rsseo_pages'))->set($db->qn('keywordsdensity').' = '.$db->qn('keywords'));
			
			if (!$config->overwritekeywords)
				$query->where($db->qn('keywordsdensity').' = '.$db->q(''));
			
			$db->setQuery($query);
			if ($db->execute()) {
				$component	= JComponentHelper::getComponent('com_rsseo');
				$cparams	= $component->params;
				
				if ($cparams instanceof JRegistry) {
					$cparams->set('copykeywords', 0);
					$cparams->set('overwritekeywords', 0);
					$query->clear();
					$query->update($db->qn('#__extensions'));
					$query->set($db->qn('params'). ' = '.$db->q((string) $cparams));
					$query->where($db->qn('extension_id'). ' = '. $db->q($component->id));
					
					$db->setQuery($query);
					$db->execute();
				}
			}
		}
	}
	
	// Check broken URLs
	public static function checkBroken($id, $pageId) {
		$db			= JFactory::getDbo();
		$query		= $db->getQuery(true);
		$response	= array('finished' => 1);
		
		require_once JPATH_ADMINISTRATOR. '/components/com_rsseo/helpers/phpQuery.php';
		
		// Get all internal/external links
		if (!$pageId) {
			$query->select($db->qn('url'))
				->from($db->qn('#__rsseo_pages'))
				->where($db->qn('id').' = '.(int) $id);
			$db->setQuery($query);
			$url = $db->loadResult();
			
			$url		= JURI::root().$url;
			$url		= str_replace(' ','%20',$url);
			$contents	= rsseoHelper::fopen($url,1);
			
			if (strpos($contents,'<html') === false || (strpos($contents,'RSSEOINVALID') !== false && $url != '')) {
				return json_encode($response);
			}
			
			$contents	= preg_replace('#<script.*?>.*?</script>#is','',$contents);
			$dom		= phpQuery::newDocumentHTML($contents);
			
			$query->clear()
				->delete($db->qn('#__rsseo_broken_links'))
				->where($db->qn('pid').' = '.(int) $id);
			$db->setQuery($query);
			$db->execute();
			
			$brokenLinks = array();
			foreach ($dom->find('a') as $href) {
				$href = phpQuery::pq($href)->attr('href');
				if ($href = rsseoHelper::getUrl($href)) {
					$brokenLinks[]  = $href;
				}
			}
			
			if ($brokenLinks = array_unique($brokenLinks)) {
				foreach ($brokenLinks as $brokenLink) {
					$query->clear()
						->insert($db->qn('#__rsseo_broken_links'))
						->set($db->qn('pid').' = '.(int) $id)
						->set($db->qn('url').' = '.$db->q($brokenLink))
						->set($db->qn('published').' = 0');
					
					$db->setQuery($query);
					$db->execute();
				}
				
				$query->clear()
					->select($db->qn('id'))
					->from($db->qn('#__rsseo_broken_links'))
					->where($db->qn('published').' = 0')
					->where($db->qn('pid').' = '.(int) $id);
				
				$db->setQuery($query,0,1);
				$nextId = (int) $db->loadResult();
				
				if ($nextId) {
					$response['finished']	= 0;
					$response['id']			= $nextId;
					$response['percent']	= 0;
				}
				
				return json_encode($response);
			} else {
				return json_encode($response);
			}
		} else {
			// Check URL's
			$query->clear()
				->select($db->qn('url'))
				->from($db->qn('#__rsseo_broken_links'))
				->where($db->qn('id').' = '.(int) $pageId);
			$db->setQuery($query);
			$currentUrl = $db->loadResult();
			
			$code = rsseoHelper::fopen($currentUrl, 0, false, true);
			
			$query->clear()
				->update($db->qn('#__rsseo_broken_links'))
				->where($db->qn('id').' = '.(int) $pageId);
			
			if (intval($code) == 200) {
				$query->set($db->qn('published').' = '.$db->q('-1'));
			} else {
				$query->set($db->qn('code').' = '.$db->q($code));
				$query->set($db->qn('published').' = '.$db->q(1));
			}
			
			$db->setQuery($query);
			$db->execute();
			
			$query->clear()
				->select('COUNT('.$db->qn('id').')')
				->from($db->qn('#__rsseo_broken_links'))
				->where($db->qn('pid').' = '.(int) $id);
			
			$db->setQuery($query);
			$total = (int) $db->loadResult();
			
			$query->clear()
				->select('COUNT('.$db->qn('id').')')
				->from($db->qn('#__rsseo_broken_links'))
				->where($db->qn('published').' = 0')
				->where($db->qn('pid').' = '.(int) $id);
			
			$db->setQuery($query);
			$remaining = (int) $db->loadResult();
			
			$query->clear()
				->select($db->qn('id'))
				->from($db->qn('#__rsseo_broken_links'))
				->where($db->qn('published').' = 0')
				->where($db->qn('pid').' = '.(int) $id);
			
			$db->setQuery($query,0,1);
			$nextId = (int) $db->loadResult();
			
			if ($nextId) {
				$response['finished']	= 0;
				$response['id']			= $nextId;
				$response['percent']	= ceil(($total - $remaining) * 100 / $total);
			}
			
			return json_encode($response);
		}
	}
	
	// Correctly build the URL
	public static function getUrl($url) {
		// Skip unwanted links
		if (strpos($url,'mailto:') !== FALSE) return false;
		if (strpos($url,'javascript:') !== FALSE) return false;
		if (strpos($url,'ymsgr:im') !== FALSE) return false;
		if (substr($url,0,1) == '#') return false;
		
		$uri	= JURI::getInstance();
		$root	= JURI::root();
		$base	= JURI::root(true);
		$site	= $uri->toString(array('scheme','host'));
		
		// Internal link
		if (substr($url,0,4) == 'http' && strpos($url,$root) !== false)
			return $url;
		
		// External link
		if (substr($url,0,4) == 'http' && strpos($url,$root) === false)
			return $url;
		
		// Internal link
		if (substr($url,0,strlen($base)) == $base)
			return $site.$url;
		
		// Internal link
		if (substr($url,0,9) == 'index.php')
			return $root.$url;
		
		return $url;
	}
	
	// Get error message
	public static function getError($code) {
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		
		$query->select('*')
			->from($db->qn('#__rsseo_errors'))
			->where($db->qn('error').' = '.(int) $code)
			->where($db->qn('published').' = 1');
		
		$db->setQuery($query);
		return $db->loadObject();
	}
	
	// Log errors
	public static function saveURL($code) {
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$url	= (string) JURI::getInstance();
		$enable	= rsseoHelper::getConfig('log_errors',1);
		
		if (!$enable)
			return false;
		
		$query->select($db->qn('id'))
			->from($db->qn('#__rsseo_error_links'))
			->where($db->qn('url').' = '.$db->q($url));
		$db->setQuery($query);
		if ($uid = (int) $db->loadResult()) {
			$query->clear()
				->update($db->qn('#__rsseo_error_links'))
				->set($db->qn('count').' = '.$db->qn('count').' + 1')
				->where($db->qn('id').' = '.$db->q($uid));
			$db->setQuery($query);
			$db->execute();
		} else {
			$query->clear()
				->insert($db->qn('#__rsseo_error_links'))
				->set($db->qn('url').' = '.$db->q($url))
				->set($db->qn('code').' = '.$db->q($code))
				->set($db->qn('count').' = 1');
			$db->setQuery($query);
			$db->execute();
		}
	}
	
	// Get a list of response codes
	public static function getResponseMessage($code) {
		$http_status_codes = array(	100 => "Continue", 
									101 => "Switching Protocols", 
									102 => "Processing", 
									200 => "OK", 
									201 => "Created", 
									202 => "Accepted", 
									203 => "Non-Authoritative Information", 
									204 => "No Content", 
									205 => "Reset Content", 
									206 => "Partial Content", 
									207 => "Multi-Status", 
									300 => "Multiple Choices", 
									301 => "Moved Permanently", 
									302 => "Found", 
									303 => "See Other", 
									304 => "Not Modified", 
									305 => "Use Proxy", 
									306 => "(Unused)", 
									307 => "Temporary Redirect", 
									308 => "Permanent Redirect", 
									400 => "Bad Request", 
									401 => "Unauthorized", 
									402 => "Payment Required", 
									403 => "Forbidden", 
									404 => "Not Found", 
									405 => "Method Not Allowed", 
									406 => "Not Acceptable", 
									407 => "Proxy Authentication Required", 
									408 => "Request Timeout", 
									409 => "Conflict", 
									410 => "Gone", 
									411 => "Length Required", 
									412 => "Precondition Failed", 
									413 => "Request Entity Too Large", 
									414 => "Request-URI Too Long", 
									415 => "Unsupported Media Type", 
									416 => "Requested Range Not Satisfiable", 
									417 => "Expectation Failed", 
									418 => "I'm a teapot", 
									419 => "Authentication Timeout", 
									420 => "Enhance Your Calm", 
									422 => "Unprocessable Entity", 
									423 => "Locked", 
									424 => "Failed Dependency", 
									424 => "Method Failure", 
									425 => "Unordered Collection", 
									426 => "Upgrade Required", 
									428 => "Precondition Required", 
									429 => "Too Many Requests", 
									431 => "Request Header Fields Too Large", 
									444 => "No Response", 
									449 => "Retry With", 
									450 => "Blocked by Windows Parental Controls", 
									451 => "Unavailable For Legal Reasons", 
									494 => "Request Header Too Large", 
									495 => "Cert Error", 
									496 => "No Cert", 
									497 => "HTTP to HTTPS", 
									499 => "Client Closed Request", 
									500 => "Internal Server Error", 
									501 => "Not Implemented", 
									502 => "Bad Gateway", 
									503 => "Service Unavailable", 
									504 => "Gateway Timeout", 
									505 => "HTTP Version Not Supported", 
									506 => "Variant Also Negotiates", 
									507 => "Insufficient Storage", 
									508 => "Loop Detected", 
									509 => "Bandwidth Limit Exceeded", 
									510 => "Not Extended", 
									511 => "Network Authentication Required", 
									598 => "Network read timeout error", 
									599 => "Network connect timeout error",
									0 => "Unknown error"
								);
		return isset($http_status_codes[$code]) ? $http_status_codes[$code] : '';
	}
	
	// Get statistics
	public static function getStatistics() {
		$db			= JFactory::getDbo();
		$query		= $db->getQuery(true);
		$url		= JURI::root();
		$statistics = array();
		$canrun		= false;
		
		$query->clear()
			->select('*')
			->from($db->qn('#__rsseo_statistics'));
		$db->setQuery($query);
		$statistics = (array) $db->loadObject();
		
		if ($statistics) {
			if (JFactory::getDate($statistics['date'])->toUnix() + 86400 < JFactory::getDate()->toUnix()) {
				$canrun = true;
			} else {
				unset($statistics['id'], $statistics['date']);
				return $statistics;
			}
		} else {
			$canrun = true;
		}
		
		if ($canrun) {
			require_once JPATH_ADMINISTRATOR. '/components/com_rsseo/helpers/competitors.php';
			$competitors = competitorsHelper::getInstance(null, $url, true);
			$competitor = $competitors->check();
			return $competitor;
		}
	}
	
	// Get the most visited pages
	public static function getMostVisited() {
		$db			= JFactory::getDbo();
		$query		= $db->getQuery(true);
		
		$query->select($db->qn('id'))->select($db->qn('url'))->select($db->qn('hits'))
			->from($db->qn('#__rsseo_pages'))
			->where($db->qn('hits').' > 0')
			->order($db->qn('hits').' DESC');
		
		$db->setQuery($query,0,10);
		return $db->loadObjectList();
	}
	
	// Set the sitemap cron function
	public static function cronSitemap() {
		$db			= JFactory::getDbo();
		$query		= $db->getQuery(true);
		$config		= rsseoHelper::getConfig();
		
		if (!file_exists(JPATH_SITE.'/sitemap.xml') && !file_exists(JPATH_SITE.'/ror.xml')) {
			return;
		}
		
		$query->clear()
			->select($db->qn('id'))->select($db->qn('url'))->select($db->qn('title'))
			->select($db->qn('level'))->select($db->qn('priority'))->select($db->qn('frequency'))
			->from($db->qn('#__rsseo_pages'))
			->where($db->qn('sitemap').' = 0')
			->where($db->qn('insitemap').' = 1')
			->where($db->qn('published').' != -1')
			->where($db->qn('canonical').' = '.$db->q(''))
			->order($db->qn('level'));
		$db->setQuery($query,0,250);
		
		if ($pages = $db->loadObjectList()) {
			require_once JPATH_ADMINISTRATOR.'/components/com_rsseo/helpers/sitemap.php';
			$protocol	= isset($config->sitemapprotocol) ? $config->sitemapprotocol : 0;
			$sitemap	= sitemapHelper::getInstance(0, $protocol, JHtml::_('date','NOW','Y-m-d'), $config->sitemapauto);
			
			if ((file_exists(JPATH_SITE.'/sitemap.xml') && filesize(JPATH_SITE.'/sitemap.xml') < 99) || (file_exists(JPATH_SITE.'/ror.xml') && filesize(JPATH_SITE.'/ror.xml') < 103)) {
				$sitemap->clear();
				$sitemap->setHeader(true);
				$sitemap->close();
			}
			
			foreach ($pages as $page) {
				$sitemap->add($page, true);
				
				$query->clear()
					->update($db->qn('#__rsseo_pages'))
					->set($db->qn('sitemap').' = 1')
					->where($db->qn('id').' = '.$db->q($page->id));
				$db->setQuery($query);
				$db->execute();
			}
		}
	}
}