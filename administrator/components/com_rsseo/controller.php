<?php
/**
* @package RSSeo!
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-2.0.html
*/

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class RSSeoController extends JControllerLegacy
{
	public function __construct() {
		parent::__construct();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_rsseo/tables');
	}

	/**
	 * Method to display a view.
	 *
	 * @param	boolean			If true, the view output will be cached
	 * @param	array			An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display($cachable = false, $urlparams = false) {
		rsseoHelper::addSubmenu(JFactory::getApplication()->input->getCmd('view'));
		
		parent::display();
		return $this;
	}
	
	/**
	 *	Method to display the RSSeo! Dashboard
	 *
	 * @return void
	 */
	public function main() {
		return $this->setRedirect('index.php?option=com_rsseo');
	}
	
	/**
	 *	Method to check a page loading time and size
	 *
	 * @return string
	 */
	public function pagecheck() {
		require_once JPATH_SITE.'/administrator/components/com_rsseo/helpers/class.webpagesize.php';
		$db		= JFactory::getDBO();
		$query	= $db->getQuery(true);
		$id		= JFactory::getApplication()->input->getInt('id',0);
		
		$query->clear()
			->select($db->qn('url'))
			->from($db->qn('#__rsseo_pages'))
			->where($db->qn('id').' = '.$id);
		$db->setQuery($query);
		$url = $db->loadResult();
		
		set_time_limit(100);
		$size = new WebpageSize(JURI::root().$url);
		$page_size = $size->sizeofpage();
		$time_total = $size->getTime();
		$page_load = number_format($time_total,3);
	
		echo JText::sprintf('COM_RSSEO_PAGE_SIZE_DESCR',$page_size,$id)."RSDELIMITER".JText::sprintf('COM_RSSEO_PAGE_TIME_DESCR',$page_load);
		JFactory::getApplication()->close();
	}
	
	/**
	 *	Method to search for pages
	 *
	 * @return string
	 */
	public function search() {
		$db		= JFactory::getDBO();
		$query	= $db->getQuery(true);
		$search	= JFactory::getApplication()->input->getString('search');
		$type	= JFactory::getApplication()->input->getString('type','');
		$html	= array();
		
		$query->select($db->qn('title'))->select($db->qn('url'))
			->from($db->qn('#__rsseo_pages'))
			->where($db->qn('url').' LIKE '.$db->q('%'.$search.'%').' OR '.$db->qn('title').' LIKE '.$db->q('%'.$search.'%'));
		$db->setQuery($query);
		$results = $db->loadObjectList();
		
		$add 	= $type == 'redirect' ? 'addRedirect' : 'addCanonical';
		$close 	= $type == 'redirect' ? 'closeRedirectSearch();' : 'closeCanonicalSearch();';
		
		$html[] = '<li class="rss_close"><a href="javascript:void(0);" onclick="'.$close.'">'.JText::_('COM_RSSEO_GLOBAL_CLOSE').'</a></li>';
		
		if (!empty($results)) {
			foreach ($results as $result) {
				$url = $type == 'redirect' ? $result->url : JURI::root().$result->url;
				$html[] = '<li><a href="javascript:void(0);" onclick="'.$add.'(\''.$url.'\')">'.$result->title.'<br/>'.$url.'</a></li>';
			}
		} else $html[] = '<li>'.JText::_('COM_RSSEO_NO_RESULTS').'</li>';
		
		echo implode("\n",$html);
		JFactory::getApplication()->close();
	}
	
	/**
	 *	Method to check for connectivity
	 *
	 * @return void
	 */
	public function connectivity() {
		$app	= JFactory::getApplication();
		$google = $app->input->getInt('google',0);
		
		if ($google) {
			require_once JPATH_ADMINISTRATOR. '/components/com_rsseo/helpers/google.php';
			
			$google = new RSSeoGoogle('http://www.rsjoomla.com');
			$response = $google->check();
		
			if ($response === true) {
				return $this->setRedirect('index.php?option=com_rsseo',JText::_('COM_RSSEO_CONNECTIVITY_OK'));
			} else {
				echo $response;
				$app->close();
			}
		} else {
			$functions	= array('cURL','file_get_contents','fopen','fsockopen');
			$errors		= rsseoHelper::fopen(JURI::root(), 1, true);
			
			if (count($errors) == 4) {
				$msg = JText::_('COM_RSSEO_CONNECTIVITY_ERROR');
			} elseif (empty($errors)) {
				$msg = JText::_('COM_RSSEO_CONNECTIVITY_OK');
			} else {
				$ok = array_diff($functions,$errors);
				$msg = JText::sprintf('COM_RSSEO_CONNECTIVITY_MESSAGE', implode(',',$errors), implode(',',$ok));
			}
			
			return $this->setRedirect('index.php?option=com_rsseo', $msg);
		}
	}
	
	/**
	 *	Method to crawl a page
	 *
	 * @return void
	 */
	public function crawl() {
		$app		= JFactory::getApplication();
		$initialize = $app->input->getInt('init');
		$id			= $app->input->getInt('id');
		$original	= $app->input->getInt('original',0);
		
		require_once JPATH_ADMINISTRATOR. '/components/com_rsseo/helpers/crawler.php';
		$crawler = crawlerHelper::getInstance($initialize, $id, $original);
		echo $crawler->crawl();
		$app->close();
	}
}