<?php
/**
* @package RSSeo!
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction')); ?>

<form action="<?php echo JRoute::_('index.php?option=com_rsseo&view=errorlinks');?>" method="post" name="adminForm" id="adminForm">
<div class="row-fluid">
	<div class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div class="span10">
		<?php echo $this->filterbar->show(); ?>
		<div class="clr"> </div>
		<table class="table table-striped adminlist">
			<thead>
				<th width="1%" align="center" class="hidden-phone center"><input type="checkbox" name="checkall-toggle" value="" onclick="Joomla.checkAll(this);"/></th>
				<th><?php echo JHtml::_('grid.sort', 'COM_RSSEO_ERROR_LINK_URL', 'url', $listDirn, $listOrder); ?></th>
				<th width="4%" align="center" class="center hidden-phone"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_ERROR_LINK_CODE', 'code', $listDirn, $listOrder); ?></th>
				<th width="3%" align="center" class="center hidden-phone"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_ERROR_LINK_URL_COUNT', 'count', $listDirn, $listOrder); ?></th>
				<th width="3%" align="center" class="center hidden-phone"><?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'id', $listDirn, $listOrder); ?></th>
			</thead>
			<tbody>
				<?php foreach ($this->items as $i => $item) { ?>
				<tr class="row<?php echo $i % 2; ?>">
					<td class="center hidden-phone"><?php echo JHTML::_('grid.id', $i, $item->id); ?></td>
					<td class="nowrap has-context">
						<?php echo $this->escape($item->url); ?>
					</td>
					<td align="center" class="center hidden-phone">
						<?php echo $item->code; ?>
					</td>
					<td align="center" class="center hidden-phone">
						<?php echo $item->count; ?>
					</td>
					<td align="center" class="center hidden-phone">
						<?php echo $item->id; ?>
					</td>
				</tr>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="5">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>

	<?php echo JHTML::_( 'form.token' ); ?>
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="task" value="" />
</form>