<?php
/**
* @package RSSeo!
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$parent		= $this->escape($this->state->get('filter.parent')); ?>

<script type="text/javascript">	
	Joomla.submitbutton = function(task) {
		if (task == 'back') {
			$('filter_parent').value = 0;
			Joomla.submitform();
			return false;
		} else {
			Joomla.submitform(task);
		}
	}
</script>

<div class="row-fluid">
	<div class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div class="span10">
		<form action="<?php echo JRoute::_('index.php?option=com_rsseo&view=competitors');?>" method="post" name="adminForm" id="adminForm">
			<?php echo $this->filterbar->show(); ?>
			<div class="clr"> </div>
			<table class="table table-striped adminlist">
				<thead>
					<th width="1%" align="center" class="small hidden-phone center"><input type="checkbox" name="checkall-toggle" value="" onclick="Joomla.checkAll(this);"/></th>
					<?php if (!$parent) { ?>
					<th width="2%" class="small hidden-phone"><?php echo JText::_('COM_RSSEO_COMPETITORS_HISTORY'); ?></th>
					<th class="small"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_COMPETITORS_COMPETITOR', 'name', $listDirn, $listOrder); ?></th>
					<?php } ?>
					<?php if ($this->config->enable_age) { ?><th class="center small hidden-phone" width="5%"><?php echo JHtml::_('grid.sort','COM_RSSEO_COMPETITORS_DOMAIN_AGE', 'age', $listDirn, $listOrder); ?></th><?php } ?>
					<?php if ($this->config->enable_pr) { ?><th class="center small hidden-phone" width="5%"><?php echo JHtml::_('grid.sort','COM_RSSEO_COMPETITORS_PAGE_RANK', 'pagerank', $listDirn, $listOrder); ?></th><?php } ?>
					<?php if ($this->config->enable_googlep) { ?><th class="center small hidden-phone" width="5%"><?php echo JHtml::_('grid.sort','COM_RSSEO_COMPETITORS_GOOGLE_PAGES', 'googlep', $listDirn, $listOrder); ?></th><?php } ?>
					<?php if ($this->config->enable_googleb) { ?><th class="center small hidden-phone" width="5%"><?php echo JHtml::_('grid.sort','COM_RSSEO_COMPETITORS_GOOGLE_BACKLINKS', 'googleb', $listDirn, $listOrder); ?></th><?php } ?>
					<?php if ($this->config->enable_googler) { ?><th class="center small hidden-phone" width="5%"><?php echo JHtml::_('grid.sort','COM_RSSEO_COMPETITORS_GOOGLE_RELATED', 'googler', $listDirn, $listOrder); ?></th><?php } ?>
					<?php if ($this->config->enable_bingp) { ?><th class="center small hidden-phone" width="5%"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_COMPETITORS_BING_PAGES', 'bingp', $listDirn, $listOrder); ?></th><?php } ?>
					<?php if ($this->config->enable_bingb) { ?><th class="center small hidden-phone" width="5%"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_COMPETITORS_BING_BACKLINKS', 'bingb', $listDirn, $listOrder); ?></th><?php } ?>
					<?php if ($this->config->enable_alexa) { ?><th class="center small hidden-phone" width="5%"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_COMPETITORS_ALEXA_RANK', 'alexa', $listDirn, $listOrder); ?></th><?php } ?>
					<?php if ($this->config->enable_tehnorati) { ?><th class="center small hidden-phone" width="5%"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_COMPETITORS_TECHNORATI_RANK', 'technorati', $listDirn, $listOrder); ?></th><?php } ?>
					<?php if ($this->config->enable_dmoz) { ?><th class="center small hidden-phone" width="5%"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_COMPETITORS_DMOZ_RANK', 'dmoz', $listDirn, $listOrder); ?></th><?php } ?>
					<th class="small center hidden-phone" align="center" width="5%"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_COMPETITORS_DATE', 'date', $listDirn, $listOrder); ?></th>
					<?php if (!$parent) { ?>
					<th class="small center hidden-phone" align="center" width="5%"><?php echo JText::_('COM_RSSEO_GLOBAL_REFRESH'); ?></th>
					<?php } ?>
					<th width="1%" align="center" class="small center hidden-phone"><?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'id', $listDirn, $listOrder); ?></th>
				</thead>
				<tbody id="competitorsTable">
					<?php foreach ($this->items as $i => $item) { ?>
					<tr class="row<?php echo $i % 2; ?>">
						<td class="center small hidden-phone"><?php echo JHTML::_('grid.id', $i, $item->id); ?></td>
						<?php if (!$parent) { ?>
						<td align="center" class="center small hidden-phone">
							<a href="javascript:void(0)" onclick="rsseo_history(<?php echo $item->id; ?>)">
								<span class="icon-history"></span>
							</a>
						</td>
						<td class="nowrap small has-context">
							<a href="<?php echo JRoute::_('index.php?option=com_rsseo&task=competitor.edit&id='.$item->id); ?>" id="competitor<?php echo $item->id; ?>">
								<?php echo $this->escape($item->name); ?>
							</a>
						</td>
						<?php } ?>
						
						<?php if ($this->config->enable_age) { ?>
						<td align="center" class="center small hidden-phone">
							<span id="age<?php echo $item->id; ?>">
								<?php echo (int) $item->age <= 0 ? '-' : rsseoHelper::convertage($item->age); ?>
							</span>
						</td>
						<?php } ?>
						
						<?php if ($this->config->enable_pr) { ?>
						<td align="center" class="center small hidden-phone">
							<span class="badge badge-<?php echo $item->pagerankbadge; ?>" id="pagerank<?php echo $item->id; ?>">
								<?php echo $item->pagerank; ?>
							</span>
						</td>
						<?php } ?>
						
						<?php if ($this->config->enable_googlep) { ?>
						<td align="center" class="center small hidden-phone">
							<span class="badge badge-<?php echo $item->googlepbadge; ?>" id="googlep<?php echo $item->id; ?>">
								<?php echo $item->googlep; ?>
							</span>
						</td>
						<?php } ?>
						
						<?php if ($this->config->enable_googleb) { ?>
						<td align="center" class="center small hidden-phone">
							<span class="badge badge-<?php echo $item->googlebbadge; ?>" id="googleb<?php echo $item->id; ?>">
								<?php echo $item->googleb; ?>
							</span>
						</td>
						<?php } ?>
						
						<?php if ($this->config->enable_googler) { ?>
						<td align="center" class="center small hidden-phone">
							<span class="badge badge-<?php echo $item->googlerbadge; ?>" id="googler<?php echo $item->id; ?>">
								<?php echo $item->googler; ?>
							</span>
						</td>
						<?php } ?>
						
						<?php if ($this->config->enable_bingp) { ?>
						<td align="center" class="center small hidden-phone">
							<span class="badge badge-<?php echo $item->bingpbadge; ?>" id="bingp<?php echo $item->id; ?>">
								<?php echo $item->bingp; ?>
							</span>
						</td>
						<?php } ?>
						
						<?php if ($this->config->enable_bingb) { ?>
						<td align="center" class="center small hidden-phone">
							<span class="badge badge-<?php echo $item->bingbbadge; ?>" id="bingb<?php echo $item->id; ?>">
								<?php echo $item->bingb; ?>
							</span>
						</td>
						<?php } ?>
						
						<?php if ($this->config->enable_alexa) { ?>
						<td align="center" class="center small hidden-phone">
							<span class="badge badge-<?php echo $item->alexabadge; ?>" id="alexa<?php echo $item->id; ?>">
								<?php echo $item->alexa; ?>
							</span>
						</td>
						<?php } ?>
						
						<?php if ($this->config->enable_tehnorati) { ?>
						<td align="center" class="center small hidden-phone">
							<span class="badge badge-<?php echo $item->technoratibadge; ?>" id="technorati<?php echo $item->id; ?>">
								<?php echo $item->technorati; ?>
							</span>
						</td>
						<?php } ?>
						
						<?php if ($this->config->enable_dmoz) { ?>
						<td align="center" class="center small hidden-phone">
							<span class="badge badge-<?php echo $item->dmozbadge; ?>" id="dmoz<?php echo $item->id; ?>">
								<?php
									if ($item->dmoz == -1) 
										echo '-';
									else if ($item->dmoz == 1) 
										echo JText::_('JYES');
									else if ($item->dmoz == 0) 
										echo JText::_('JNO');
								?>
							</span>
						</td>
						<?php } ?>
						
						<td align="center" class="center small hidden-phone">
							<span id="date<?php echo $item->id; ?>">
								<?php echo JHtml::_('date', $item->date, $this->config->global_dateformat); ?>
							</span>
						</td>
						
						<?php if (!$parent) { ?>
						<td align="center" class="center small hidden-phone">
							<a href="javascript:void(0)" onclick="rsseo_competitor(<?php echo $item->id; ?>)" id="refresh<?php echo $item->id; ?>">
								<?php echo JText::_('COM_RSSEO_GLOBAL_REFRESH'); ?>
							</a>
							<img src="<?php echo JURI::root(); ?>administrator/components/com_rsseo/assets/images/loader.gif" alt="" id="loading<?php echo $item->id; ?>" style="display:none;" />
						</td>
						<?php } ?>
						
						<td align="center" class="center small hidden-phone">
							<?php echo $item->id; ?>
						</td>
					</tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="15">
							<?php echo $this->pagination->getListFooter(); ?>
						</td>
					</tr>
				</tfoot>
			</table>
			
			<?php echo JHTML::_( 'form.token' ); ?>
			<input type="hidden" name="boxchecked" value="0" />
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="filter_parent" id="filter_parent" value="<?php echo $this->state->get('filter.parent'); ?>" />
		</form>
	</div>
</div>