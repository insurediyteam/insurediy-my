<?php
/**
* @package RSSeo!
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction')); ?>

<form action="<?php echo JRoute::_('index.php?option=com_rsseo&view=keywords');?>" method="post" name="adminForm" id="adminForm">
<div class="row-fluid">
	<div class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div class="span10">
		<?php echo $this->filterbar->show(); ?>
		<div class="clr"> </div>
		<table class="table table-striped adminlist">
			<thead>
				<th width="1%" align="center" class="hidden-phone"><input type="checkbox" name="checkall-toggle" value="" onclick="Joomla.checkAll(this);"/></th>
				<th><?php echo JHtml::_('grid.sort', 'COM_RSSEO_KEYWORDS_KEYWORD', 'keyword', $listDirn, $listOrder); ?></th>
				<th width="15%" class="center hidden-phone" align="center"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_KEYWORDS_IMPORTANCE', 'importance', $listDirn, $listOrder); ?></th>
				<th width="6%" class="center" align="center"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_KEYWORDS_POSITION', 'position', $listDirn, $listOrder); ?></th>
				<th width="15%" class="center hidden-phone" align="center"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_KEYWORDS_DATE', 'date', $listDirn, $listOrder); ?></th>
				<th width="3%" class="center hidden-phone" align="center"><?php echo JText::_('COM_RSSEO_GLOBAL_REFRESH'); ?></th>
				<th width="4%" align="center" class="center hidden-phone"><?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'id', $listDirn, $listOrder); ?></th>
			</thead>
			<tbody>
				<?php foreach ($this->items as $i => $item) { ?>
				<tr class="row<?php echo $i % 2; ?>">
					<td class="center hidden-phone"><?php echo JHTML::_('grid.id', $i, $item->id); ?></td>
					<td class="nowrap has-context">
						<a href="<?php echo JRoute::_('index.php?option=com_rsseo&task=keyword.edit&id='.$item->id); ?>">
							<?php echo $this->escape($item->keyword); ?>
						</a> 
					</td>
					
					<td align="center" class="center hidden-phone">
						<?php echo JText::_('COM_RSSEO_KEYWORD_IMPORTANCE_'.$item->importance); ?>
					</td>
					
					<td align="center" class="center nowrap">
						<span class="badge badge-<?php echo $item->badge; ?>" id="position<?php echo $item->id; ?>">
							<?php echo $item->position; ?>
						</span>
					</td>
					
					<td align="center" class="center hidden-phone">
						<span id="date<?php echo $item->id; ?>">
							<?php echo JHtml::_('date', $item->date, rsseoHelper::getConfig('global_dateformat')); ?>
						</span>
					</td>
					
					<td align="center" class="center hidden-phone">
						<a href="javascript:void(0)" onclick="rsseo_keyword(<?php echo $item->id; ?>)" id="refresh<?php echo $item->id; ?>">
							<?php echo JText::_('COM_RSSEO_GLOBAL_REFRESH'); ?>
						</a>
						<img src="<?php echo JURI::root(); ?>administrator/components/com_rsseo/assets/images/loader.gif" alt="" id="loading<?php echo $item->id; ?>" style="display:none;" />
					</td>
					
					<td align="center" class="center hidden-phone">
						<?php echo $item->id; ?>
					</td>
				</tr>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="7">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>

	<?php echo JHTML::_( 'form.token' ); ?>
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="task" value="" />
</form>