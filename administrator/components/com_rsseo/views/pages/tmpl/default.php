<?php
/**
* @package RSSeo!
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction')); ?>

<script type="text/javascript">
	Joomla.submitbutton = function(task)  {
		if (task == 'refresh') {
			$$('input[name^=cid[]').each(function (el) {
				if (el.checked) {
					rsseo_page(el.value,0);
				}
			});
		} else if (task == 'restore') {
			$$('input[name^=cid[]').each(function (el) {
				if (el.checked) {
					rsseo_page(el.value,1);
				}
			});
		} else Joomla.submitform(task);
		
		return false;
	}
</script>


<form action="<?php echo JRoute::_('index.php?option=com_rsseo&view=pages');?>" method="post" name="adminForm" id="adminForm">
<div class="row-fluid">
	<div class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div class="span10">
		<?php echo $this->filterbar->show(); ?>
		<div class="clr"> </div>
		<table class="table table-striped adminlist">
			<thead>
				<th width="1%" align="center" class="small hidden-phone"><input type="checkbox" name="checkall-toggle" value="" onclick="Joomla.checkAll(this);"/></th>
				<th class="small hidden-phone"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_PAGES_URL', 'url', $listDirn, $listOrder); ?></th>
				<th class="center small" align="center"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_PAGES_TITLE', 'title', $listDirn, $listOrder); ?></th>
				<th width="6%" class="center small hidden-phone" align="center"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_PAGES_LEVEL', 'level', $listDirn, $listOrder); ?></th>
				<th width="6%" class="center small" align="center"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_PAGES_GRADE', 'grade', $listDirn, $listOrder); ?></th>
				<th width="8%" class="center small hidden-phone" align="center"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_PAGES_LAST_CRAWLED', 'date', $listDirn, $listOrder); ?></th>
				<th width="1%" class="center small hidden-phone" align="center"><?php echo JText::_('COM_RSSEO_PAGES_STATUS'); ?></th>
				<th width="7%" class="center small hidden-phone" align="center"><?php echo JText::_('COM_RSSEO_PAGES_PAGE_MODIFIED'); ?></th>
				<th width="7% "class="center small hidden-phone" align="center"><?php echo JText::_('COM_RSSEO_PAGES_ADD_TO_SITEMAP'); ?></th>
				<th width="5%" class="center small hidden-phone" align="center"><?php echo JText::_('COM_RSSEO_GLOBAL_REFRESH'); ?></th>
				<th width="1%" align="center" class="center small hidden-phone"><?php echo JHtml::_('grid.sort', 'COM_RSSEO_HITS', 'hits', $listDirn, $listOrder); ?></th>
				<th width="1%" align="center" class="center small hidden-phone"><?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'id', $listDirn, $listOrder); ?></th>
			</thead>
			<tbody>
				<?php foreach ($this->items as $i => $item) { ?>
				<tr class="row<?php echo $i % 2; ?>">
					<td class="center small hidden-phone"><?php echo JHTML::_('grid.id', $i, $item->id); ?></td>
					<td class="small hidden-phone rstd">
						<a href="<?php echo JRoute::_('index.php?option=com_rsseo&task=page.edit&id='.$item->id); ?>">
							<?php echo $item->url; ?>
						</a> 
						<a href="<?php echo JURI::root().addslashes($item->url); ?>" target="_blank">
							<img src="<?php echo JURI::root(); ?>administrator/components/com_rsseo/assets/images/external-link.png" alt="" border="0" />
						</a>
					</td>
					<td class="small has-context rstd">
						<a href="<?php echo JRoute::_('index.php?option=com_rsseo&task=page.edit&id='.$item->id); ?>">
							<span id="title<?php echo $item->id; ?>">
								<?php echo empty($item->title) ? JText::_('COM_RSSEO_GLOBAL_NO_TITLE') : $this->escape($item->title); ?>
							</span>
						</a>
					</td>
					
					<td align="center" class="center small hidden-phone">
						<?php echo ($item->level >= 127) ? JText::_('COM_RSSEO_GLOBAL_UNDEFINED') : $item->level; ?>
					</td>
					
					<td align="center" class="center small">
						<?php $grade = ($item->grade <= 0) ? 0 : ceil($item->grade); ?>
						<div class="rsj-progress" style="width: 100%">
							<span id="page<?php echo $item->id; ?>" style="width: <?php echo $grade; ?>%;" class="<?php echo $item->color; ?>">
								<span><?php echo $grade; ?>%</span>
							</span>
						</div>
					</td>
					
					<td align="center" class="center small hidden-phone">
						<span id="date<?php echo $item->id; ?>">
							<?php echo JHtml::_('date', $item->date, $this->config->global_dateformat); ?>
						</span>
					</td>
					
					<td align="center" class="center small hidden-phone">
						<?php echo JHtml::_('jgrid.published', $item->published, $i, 'pages.'); ?>
					</td>
					
					<td align="center" class="center small hidden-phone">
						<?php echo JHtml::_('icon.modified', $item->modified, $item->id); ?>
					</td>
					
					<td align="center" class="center small hidden-phone">
						<?php echo JHtml::_('icon.insitemap', $item->insitemap, $i); ?>
					</td>
					
					<td align="center" class="center small hidden-phone">
						<a href="javascript:void(0)" onclick="rsseo_page(<?php echo $item->id; ?>,0)" id="refresh<?php echo $item->id; ?>">
							<?php echo JText::_('COM_RSSEO_GLOBAL_REFRESH'); ?>
						</a>
						<img src="<?php echo JURI::root(); ?>administrator/components/com_rsseo/assets/images/loader.gif" alt="" id="loading<?php echo $item->id; ?>" style="display:none;" />
					</td>
					
					<td align="center" class="center small hidden-phone">
						<?php echo $item->hits; ?>
					</td>
					
					<td align="center" class="center small hidden-phone">
						<?php echo $item->id; ?>
					</td>
				</tr>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="16">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>

	<?php echo JHTML::_( 'form.token' ); ?>
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="md5title" value="<?php echo $this->escape($this->state->get('filter.md5title')); ?>" />
	<input type="hidden" name="md5descr" value="<?php echo $this->escape($this->state->get('filter.md5descr')); ?>" />
</form>