<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Transactions helper.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 * @since       1.6
 */
class ProductHelper {

	/**
	 * Configure the Linkbar.
	 *
	 * @param   string	The name of the active view.
	 * @since   1.6
	 */
	public static function addSubmenu($vName = 'list') {
		JHtmlSidebar::addEntry(
				JText::_('COM_PRODUCT_SUBMENU_LIST'), 'index.php?option=com_product&view=list', $vName == 'list'
		);
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @param   integer  The category ID.
	 * @return  JObject
	 * @since   1.6
	 */
	public static function getActions($categoryId = 0) {
		$user = JFactory::getUser();
		$result = new JObject;

		if (empty($categoryId)) {
			$assetName = 'com_product';
			$level = 'component';
		} else {
			$assetName = 'com_product.category.' . (int) $categoryId;
			$level = 'category';
		}

		$actions = JAccess::getActions('com_product', $level);

		foreach ($actions as $action) {
			$result->set($action->name, $user->authorise($action->name, $assetName));
		}

		return $result;
	}

}
