<?php

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'policy.cancel' || document.formvalidator.isValid(document.id('policy-form')))
		{
<?php //echo $this->form->getField('description')->save();   ?>
			Joomla.submitform(task, document.getElementById('policy-form'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_insurepolicies'); ?>" method="post" name="adminForm" id="policy-form" class="form-validate">
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset>
				<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'info_details')); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'info_details', JText::_('COM_INSUREPOLICIES_POLICY_FORM_DETAIL')); ?>

				<?php foreach ($this->form->getFieldset("fields") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<input type="hidden" name="task" value="" />
				<?php echo JHtml::_('form.token'); ?>

				<?php echo JHtml::_('bootstrap.endTabSet'); ?>
			</fieldset>
		</div>
		<!-- End Weblinks -->
		<!-- Begin Sidebar -->
		<?php //echo JLayoutHelper::render('joomla.edit.details', $this);  ?>
		<!-- End Sidebar -->
	</div>
</form>
