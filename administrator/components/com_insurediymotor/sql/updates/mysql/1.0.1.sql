CREATE TABLE IF NOT EXISTS `#__insure_motor_make_master` (
  `id` INTEGER NOT NULL auto_increment,
  `make_code` VARCHAR(255) NOT NULL DEFAULT '',
  `make_name` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_model_master` (
  `id` INTEGER NOT NULL auto_increment,
  `make_code` VARCHAR(255) NOT NULL DEFAULT '',
  `model_code` VARCHAR(255) NOT NULL DEFAULT '',
  `model_name` VARCHAR(255) NOT NULL DEFAULT '',
  `model_cc` INTEGER NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_make_model_sompo` (
  `id` INTEGER NOT NULL auto_increment,
  `model_code` VARCHAR(255) NOT NULL DEFAULT '',
  `model_name` VARCHAR(255) NOT NULL DEFAULT '',
  `model_master_code` VARCHAR(255) NOT NULL DEFAULT '',
  `model_cc` INTEGER NOT NULL DEFAULT '',
  `model_bodytype` VARCHAR(255) NOT NULL DEFAULT '',
  `model_body_desc` VARCHAR(255) NOT NULL DEFAULT '',
  `model_no_passenger` VARCHAR(255) NOT NULL DEFAULT '',
  `model_make_code` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_quote_master` (
  `id` INTEGER NOT NULL auto_increment,
  `userId` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_plan_workshop` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_plan_los` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_plan_ncdpro` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_policy_start_date` DATETIME NOT NULL DEFAULT '0000-00-00T00:00:00',
  `quote_policy_selected_plan_code` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_policy_selected_partner_code` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_created_time` DATETIME NOT NULL DEFAULT '0000-00-00T00:00:00',
  `quote_updated_time` DATETIME NOT NULL DEFAULT '0000-00-00T00:00:00',
  `quote_deleted_time` DATETIME NOT NULL DEFAULT '0000-00-00T00:00:00',
  `quote_process_stage` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_payment_stage` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_payment_txn_id` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_payment_request_id` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_payment_amount` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_cs_required` VARCHAR(255) NOT NULL DEFAULT 'false', 
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_driver_info` (
  `id` INTEGER NOT NULL auto_increment,
  `quote_master_id` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_make` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_model` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_variant` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_com_registered` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_reg_no` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_make_year` INTEGER NOT NULL DEFAULT 2007,
  `quote_car_reg_year` INTEGER NOT NULL DEFAULT 2007,
  `quote_car_ncd` INTEGER NOT NULL DEFAULT 0,
  `quote_car_zero_ncd_reason` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_claim_amount` INTEGER NOT NULL DEFAULT 0,
  `quote_car_coe_expire_date` DATETIME NOT NULL DEFAULT '0000-00-00T00:00:00',
  `quote_driver_gender` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_first_name` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_last_name` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_email` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_nric` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_nric_type` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_contact` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_dob` DATETIME NOT NULL DEFAULT '0000-00-00T00:00:00'
  `quote_driver_exp` INTEGER NOT NULL DEFAULT 3,
  `quote_driver_occ` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_marital_status` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_main_driver_relation` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_demerit_point` INTEGER NOT NULL DEFAULT 0,
  `quote_driver_is_appliant` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_claim_no` INTEGER NOT NULL DEFAULT 0,
  `quote_driver_claim_amount` INTEGER NOT NULL DEFAULT 0,
  `quote_driver_valid_license` VARCHAR(255) NOT NULL DEFAULT 'Y',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_quote_sompo` (
  `id` INTEGER NOT NULL auto_increment,
  `quote_master_id` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_req_unique_id` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_app_quote_ref_id` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_pol_agent_ref_id` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_id` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_name` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_premium_before_tax` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_premium_tax_amt` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_premium_tax_percent` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_order` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_category` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_setting_sompo` (
  `id` INTEGER NOT NULL auto_increment,
  `sompo_application_id` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_user_id` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_sc_code` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_sub_prod_code` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_trip_type` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_discount` INTEGER NOT NULL DEFAULT 0,
  `sompo_plan_rewards_point` INTEGER NOT NULL DEFAULT 0,
  `sompo_plan_damage_coverage` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_windscreen_cover` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_roadside_assist` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_evacuation_oversea` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_personal_accident` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_medic_expense` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_third_party_liability` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_other_benefit` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

ALTER TABLE `#__insure_motor_make_master` AUTO_INCREMENT=1000000;
ALTER TABLE `#__insure_motor_model_master` AUTO_INCREMENT=1000000;
ALTER TABLE `#__insure_motor_make_model_sompo` AUTO_INCREMENT=1000000;
ALTER TABLE `#__insure_motor_quote_master` AUTO_INCREMENT=5000000;
ALTER TABLE `#__insure_motor_driver_info` AUTO_INCREMENT=1000000;
ALTER TABLE `#__insure_motor_quote_sompo` AUTO_INCREMENT=1000000;
ALTER TABLE `#__insure_motor_setting_sompo` AUTO_INCREMENT=1000000;


