<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

use Models\Eloquent\Provider;

defined('_JEXEC') or die;

/**
 * Weblink controller class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 * @since       1.6
 */
class InsureDIYMotorControllerApiconfig extends JControllerForm {

    public function update() {
        $app = JFactory::getApplication();

        $data = $app->input->get('jform', [], 'array');

        $providers = Provider::get()->keyBy('id');

        foreach ($data as $key => $value) {
            list($field, $id) = explode('.', $key);

            $providers[$id]->{$field} = $value;
        }

        foreach ($providers->values() as $key => $provider) {
            /** @var Provider $provider */
            if($provider->isDirty()) {
                $provider->save();
            }
        }

        $this->setRedirect(JRoute::_('index.php?option=com_insurediymotor&view=apiconfig'));
    }

}
