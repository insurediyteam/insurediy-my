<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cedeledepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;


JHtml::_('behavior.tooltip');

/** @var InsureDIYMotorViewApiconfig $this */
?>

<form action="<?php echo JRoute::_('index.php?option=com_insurediymotor&view=apiconfig&task=apiconfig.update'); ?>" method="post" name="adminForm" id="adminForm">
	<h1>Api Config</h1>

	<table class="table table-striped">
		<thead>
			<th>Provider</th>
			<th>Short Description</th>
			<th>Username</th>
			<th>Password</th>
		</thead>
		<tbody>
			<?php foreach($this->items as $key => $provider): ?>
			<tr>
				<td><?php echo $provider->name; ?></td>
				<td>
					<textarea name="jform[short_description.<?php echo $provider->id; ?>]"><?php echo $provider->short_description; ?></textarea>
				</td>
				<td>
					<input type="text" name="jform[user.<?php echo $provider->id; ?>]" value="<?php echo $provider->user; ?>" />
				</td>
				<td>
					<input type="password" name="jform[password.<?php echo $provider->id; ?>]" value="<?php echo $provider->password; ?>" />
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div class="control-group">
		<div class="control-label"></div>
		<div class="controls"><input class="btn btn-primary" type="submit" value="Save" /></div>
	</div>
</form>

<script>
	jQuery(`#toolbar-cancel button`).on('click', function(e) {
		e.preventDefault();

		window.history.back();
	});
</script>
