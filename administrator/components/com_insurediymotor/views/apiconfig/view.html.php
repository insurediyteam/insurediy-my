<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

use Models\Eloquent\Provider;

defined('_JEXEC') or die;

/**
 * View class for a list of CedeleDepots.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 * @since       1.5
 */
class InsureDIYMotorViewApiconfig extends JViewLegacy {

	protected $items;
	/** @var \Joomla\CMS\Pagination\Pagination */
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null) {
		$this->items = Provider::get();

		InsureDIYMotorHelper::addSubmenu('quotations');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		
		JToolbarHelper::title(JText::_('COM_INSUREDIYMOTOR_MANAGER_PLAN'), 'insurediymotor.png');
		JToolbarHelper::cancel('quotation.cancel', 'JTOOLBAR_CLOSE');

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

}
