<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cedeledepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('MyBehavior.jsInsurediy');
JHtml::_('script', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', false, true);
JHtml::_('stylesheet', 'https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css', false, true);
$form = $this->form;
$item = $this->item;
// print_r($item);
$activetab = JFactory::getApplication()->input->get("activetab", "info_details");

?>
<style>
.payment-column-label {
	text-align: right;
	font-weight: bold;
	width: 35%;
    padding-bottom: 10px;
}
.payment-column-value {
	padding-left: 10px;
    padding-bottom: 10px;
}

</style>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'quotation.cancel' || document.formvalidator.isValid(document.id('quotation-form')))
		{
			Joomla.submitform(task, document.getElementById('quotation-form'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_insurediymotor&view=quotation&layout=edit&id='.$item->id); ?>" method="post" name="adminForm" id="quotation-form" class="form-validate">
	<div class="row-fluid">
		<div class="span11 form-horizontal">
			<fieldset>
				<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => $activetab)); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'info_details', JText::_('COM_INSUREDIYMOTOR_QUOTATION_BASIC_INFORMATION')); ?>
				<?php if($item->user): ?>
				<div class="control-group">
					<div class="control-label">
						User
					</div>
					<div class="controls">
						<?php echo "{$item->user->name} ({$item->user->email})"; ?>
					</div>
				</div>
				<?php endif; ?>
				<div class="control-group">
					<div class="control-label">
						Sum Insured
					</div>
					<div class="controls">
						RM <?php echo number_format($item->sum_insured, 2); ?>
					</div>
				</div>
				<div class="control-group">
					<div class="control-label">
						Total Payable
					</div>
					<div class="controls">
						RM <?php echo number_format($item->value, 2); ?>
					</div>
				</div>
				<div class="control-group">
					<div class="control-label">
						Policy Start Date
					</div>
					<div class="controls">
						<?php echo $item->policy_start_date->format('d-M-Y'); ?>
					</div>
				</div>
				<div class="control-group">
					<div class="control-label">
						Policy End Date
					</div>
					<div class="controls">
						<?php echo $item->policy_start_date->copy()->addYear()->subDay()->format('d-M-Y'); ?>
					</div>
				</div>
				<div class="control-group">
					<div class="control-label">
						Created At
					</div>
					<div class="controls">
						<?php echo $item->created_at->format('d-M-Y'); ?>
					</div>
				</div>
				<div class="control-group">
					<div class="control-label">
						<label id="jform_quote_cs_required-lbl" for="jform_quote_cs_required" class="">
							User Referral Code
						</label>
					</div>
					<div class="controls" style="margin-top: 4px; color: red; font-weight: bold; font-size: 16px;">
						<?php if(isset($item->referral_id)) { echo $item->referral_id; } ?>
					</div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Plan Workshop</div>
					<div class="controls"><?php echo $item->plan_workshop; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Plan Los</div>
					<div class="controls"><?php echo $item->plan_los; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Plan NCD Pro</div>
					<div class="controls"><?php echo $item->plan_ncdpro; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Plan Los</div>
					<div class="controls"><?php echo $item->plan_los; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Current Insurer</div>
					<div class="controls"><?php echo $item->previous_provider ? $item->previous_provider->name : '-'; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Previous Sum Insured</div>
					<div class="controls"><?php echo number_format($item->previous_sum_insured, 2); ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Cover Type</div>
					<div class="controls"><?php echo $item->cover_type_fmt; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Main Driver's Occupation</div>
					<div class="controls"><?php echo $item->occupation->name; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Main Driver's Year of Experience</div>
					<div class="controls"><?php echo $item->driving_experience_fmt; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">NCD Rate</div>
					<div class="controls"><?php echo $item->ncd_rate_fmt; ?></div>
				</div>

				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'driver-info', JText::_('COM_INSUREDIYMOTOR_QUOTATION_DRIVERS')); ?>
				<div class="span7">
					<?php
					$no_of_drivers = $item->drivers->count();
					if ($no_of_drivers> 0):
						foreach ($item->drivers as $k => $driver):
							$kplus = $k + 1;
							$formname = "";
							$form->setFormControl($formname);
							$fid = "driver-fields-" . $kplus;
							?>
							<fieldset id="<?php echo $fid; ?>">
								<legend>
									<?php echo JText::_("COM_INSUREDIYMOTOR_TEXT_DRIVER") . " " . $kplus; ?>
								</legend>
								
								<div class="control-group">
									<div class="control-label">Salutation</div>
									<div class="controls" style="padding:5px;"><?php echo $driver->salutation; ?></div>
								</div>
								
								<div class="control-group">
									<div class="control-label">Name</div>
									<div class="controls" style="padding:5px;"><?php echo $driver->name; ?></div>
								</div>
								
								<div class="control-group">
									<div class="control-label">Gender</div>
									<div class="controls" style="padding:5px;"><?php echo $driver->gender; ?></div>
								</div>
								
								<div class="control-group">
									<div class="control-label">Email</div>
									<div class="controls" style="padding:5px;"><?php echo $driver->email; ?></div>
								</div>
								
								<div class="control-group">
									<div class="control-label">NRIC</div>
									<div class="controls" style="padding:5px;"><?php echo $driver->nric; ?></div>
								</div>
								
								<div class="control-group">
									<div class="control-label">Experience</div>
									<div class="controls" style="padding:5px;"><?php echo $driver->exp; ?> Years</div>
								</div>
								
								<div class="control-group">
									<div class="control-label">Occupation</div>
									<div class="controls" style="padding:5px;"><?php echo $driver->occ; ?></div>
								</div>
								
								<div class="control-group">
									<div class="control-label">Occupation Status</div>
									<div class="controls" style="padding:5px;"><?php echo $driver->occ_status; ?></div>
								</div>
								
								<div class="control-group">
									<div class="control-label">Occupation Nature</div>
									<div class="controls" style="padding:5px;"><?php echo $driver->occ_nature; ?></div>
								</div>
								
								<div class="control-group">
									<div class="control-label">Marital Status</div>
									<div class="controls" style="padding:5px;"><?php echo $driver->marital_status; ?></div>
								</div>
								
								<div class="control-group">
									<div class="control-label">Is Applicant</div>
									<div class="controls" style="padding:5px;"><?php echo $driver->is_applicant ? 'Yes' : 'No'; ?></div>
								</div>
								
								<div class="control-group">
									<div class="control-label">address</div>
									<div class="controls" style="padding:5px;">
										<p><?php echo $driver->address_one; ?></p>
										<p><?php echo $driver->address_two; ?></p>
									</div>
								</div>

								<input type="hidden" name="formname" value="<?php echo $formname; ?>" />
								<input type="hidden" name="quotation_id" value="<?php echo $item->id; ?>" />
								<input type="hidden" name="traveller_id" value="<?php //echo $driver['traveller_id']; ?>" />
								<?php echo JHtml::_('form.token'); ?>
								<!--  <a href="#deleteModel" role="button" style="width:65px;" class="btn btn-small btn-danger" data-toggle="modal" onclick="javascript:jsInsurediy.callDeleteForm('deleteForm', '<?php //echo $driver['traveller_id']; ?>', 'quotation.deleteDriver');"><?php echo JText::_("COM_INSUREDIYMOTOR_BUTTON_DELETE"); ?></a>
								<a href="#" role="button" style="width: 65px;" class="btn btn-small btn-success" onclick="javascript:jsInsurediy.saveWithAjax('<?php echo $fid; ?>', 'quotation.saveDriver', 'com_insurediymotor');
												return false;"><?php echo JText::_("COM_INSUREDIYMOTOR_BUTTON_SAVE"); ?></a> -->
							</fieldset>
							<div style="height:20px;"></div>
						<?php endforeach; ?>
						<!-- <fieldset>
							<legend>
								<?php //echo JText::_("COM_INSUREDIYMOTOR_TEXT_ADD_DRIVER"); ?>
							</legend>
							<div id="travellerAddForm" name="travellerAddForm">
								<?php
								$form->setFormControl("jform");
								foreach ($form->getFieldset("driver-details") as $field):
									?>
									<div class="control-group">
										<div class="control-label"><?php //echo $field->label; ?></div>
										<div class="controls"><?php //echo $field->input; ?></div>
									</div>
								<?php endforeach; ?>
								<input type="hidden" name="quotation_id" value="<?php echo $item->id; ?>" />
								<?php //echo JHtml::_('form.token'); ?>
								<a href="#" role="button" style="width:65px" class="btn btn-small btn-success" onclick="javascript:jsInsurediy.addWithAjax('travellerAddForm', 'quotation.addDriver', 'com_insurediymotor');
											return false;"><?php //echo JText::_("COM_INSUREDIYMOTOR_BUTTON_ADD"); ?></a>
							</div>
						</fieldset> -->
						
	
					<?php else: ?>
						<?php echo "No Driver Details!"; ?>
					<?php endif; ?>
				</div>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<!-- Vehicle Details -->
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'vehicle_details', JText::_('COM_INSUREDIYMOTOR_QUOTATION_VEHICLE')); ?>
				
				<div class="control-group">
					<div class="control-label">Registration No</div>
					<div class="controls"><?php echo $item->car_reg_no; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Make Year</div>
					<div class="controls"><?php echo $item->car_make_year; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Registration Year</div>
					<div class="controls"><?php echo $item->car_reg_year; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">NCD</div>
					<div class="controls"><?php echo $item->car_ncd; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Zero NCD Reason</div>
					<div class="controls"><?php echo $item->car_zero_ncd_reason; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Claim Amount</div>
					<div class="controls"><?php echo $item->car_claim_amount; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">COE Expire Date</div>
					<div class="controls"><?php echo $item->car_coe_expire_date ? $item->car_coe_expire_date->format('d-M-Y') : null; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Chassis No</div>
					<div class="controls"><?php echo $item->car_chassis_no; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Engine No</div>
					<div class="controls"><?php echo $item->car_engine_no; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Body Type</div>
					<div class="controls"><?php echo $item->body_type->name; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Functional Mod</div>
					<div class="controls"><?php echo $item->car_has_functional_mod ? 'Yes' : 'No'; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Condition</div>
					<div class="controls"><?php echo $item->car_condition_fmt; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Engine Capacity</div>
					<div class="controls"><?php echo $item->car_engine_capacity; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Market Value</div>
					<div class="controls"><?php echo $item->car_market_value; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Airbags</div>
					<div class="controls"><?php echo $item->car_airbags; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Performance Mod</div>
					<div class="controls"><?php echo $item->car_has_performance_mod ? 'Yes' : 'No'; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Vehicle Use</div>
					<div class="controls"><?php echo $item->car_use_fmt; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Purchase Date</div>
					<div class="controls"><?php echo $item->car_purchase_date->format('d-M-Y'); ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Purchase Price</div>
					<div class="controls"><?php echo number_format($item->car_purchase_price, 2); ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Fuel</div>
					<div class="controls"><?php echo $item->car_fuel; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Is New</div>
					<div class="controls"><?php echo $item->car_is_new ? 'Yes' : 'No'; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Seats</div>
					<div class="controls"><?php echo $item->car_seats; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">ABS</div>
					<div class="controls"><?php echo $item->car_has_abs ? 'Yes' : 'No'; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Garaged</div>
					<div class="controls"><?php echo $item->car_garage_fmt; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Anti Theft</div>
					<div class="controls"><?php echo $item->car_anti_theft_fmt; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">E-Hailing?</div>
					<div class="controls"><?php echo $item->car_has_ehailing ? 'Yes' : 'No'; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Past Claims?</div>
					<div class="controls"><?php echo $item->has_past_claims ? 'Yes' : 'No'; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Damage Claims</div>
					<div class="controls"><?php echo $item->car_damage; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Windscreen Claims</div>
					<div class="controls"><?php echo $item->car_windscreen; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Theft Claims</div>
					<div class="controls"><?php echo $item->car_theft; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">TP Claims</div>
					<div class="controls"><?php echo $item->car_tp_claims; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Location</div>
					<div class="controls"><?php echo $item->car_location_fmt; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Address</div>
					<div class="controls"><?php echo $item->car_address; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Logbook</div>
					<div class="controls"><?php echo $item->car_logbook; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Tax Expiry</div>
					<div class="controls"><?php echo $item->car_tax_expiry->format('d-M-Y'); ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Loan?</div>
					<div class="controls"><?php echo $item->car_loan ? 'Yes' : 'No'; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Loan Company</div>
					<div class="controls"><?php echo $item->car_loan_company_fmt; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Sum Insured</div>
					<div class="controls"><?php echo number_format($item->sum_insured, 2); ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">NCD Protector</div>
					<div class="controls"><?php echo $item->ncd_protector ? 'Yes' : 'No'; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Old Registration No</div>
					<div class="controls"><?php echo $item->car_old_registration_number; ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label">Car Log Document</div>
					<div class="controls">
						<?php if($item->car_log_document_url): ?>
							<a onclick="window.open(this.href);return false;" href="<?php echo $item->car_log_document_url; ?>"><?php echo $item->car_log_document_url; ?></a>
						<?php else: ?>
							<span>Haven't uploaded yet</span>
						<?php endif; ?>
					</div>
				</div>

				<?php echo JHtml::_('bootstrap.endTab'); ?>


				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'plan_details', JText::_('COM_INSUREDIYMOTOR_QUOTATION_PLAN_SELECTED')); ?>
					<?php 
						foreach ($item->details as $detail): 
							$decription = $detail->description;
							if($detail->code) {
								$decription .= "[{$detail->code}]";
							}
					?>
						<div class="control-group">
							<div class="control-label"><b><?php echo $decription; ?></b></div>
							<div class="controls" style="padding:5px;"><b><?php echo $detail->value; ?></b></div>
						</div>
					<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'payment-details', JText::_('COM_INSUREDIYMOTOR_QUOTATION_PAYMENT_DETAILS')); ?>
				<?php foreach ($form->getFieldset("payment-details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls" style="padding:5px;"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<!-- <button type="button" id="payment-check-button" data-quotation-id="<?php echo $item->id; ?>">Payment Status</button> -->
				<table class="table">
					<tr>
						<td class="payment-column-label">Payment Status : </td>
						<td class="payment-column-status payment-column-value"><?php echo $this->item->payment_stage; ?></td>
					</tr>
					<tr>
						<td class="payment-column-label">Payment Time : </td>
						<td class="payment-column-time payment-column-value"><?php echo $this->item->transaction ? $this->item->transaction->created : ''; ?></td>
					</tr>
					<tr>
						<td class="payment-column-label">Payment Amount : </td>
						<td class="payment-column-amount payment-column-value"><?php echo $this->item->payment_amount; ?></td>
					</tr>
					<tr>
						<td class="payment-column-label">Card Type : </td>
						<td class="payment-column-card-type payment-column-value"><?php echo $this->item->transaction ? $this->item->transaction->feed_log->payMethod : ''; ?></td>
					</tr>
					<tr>
						<td class="payment-column-label">Card Holder : </td>
						<td class="payment-column-card-name payment-column-value"><?php echo $this->item->transaction ? $this->item->transaction->feed_log->Holder : ''; ?></td>
					</tr>
					<tr>
						<td class="payment-column-label">IP Address : </td>
						<td class="payment-column-ip-address payment-column-value"><?php echo $this->item->transaction ? $this->item->transaction->feed_log->sourceIp : ''; ?></td>
					</tr>
				</table>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<input type="hidden" name="task" value="quotation.saveQuote" />
				<?php echo JHtml::_('form.token'); ?>
				<?php echo JHtml::_('bootstrap.endTabSet'); ?>
			</fieldset>
		</div>
	</div>
</form>

<script>
	jQuery( "#dialog" ).dialog({ autoOpen: false, width: 500 });
	
	jQuery("#payment-check-button").on("click", function(obj){
		params = [
			{'name': 'id', 'value': jQuery(this).attr("data-quotation-id")},
			{'name': 'option', 'value': 'com_insurediymotor'},
			{'name': 'task', 'value': 'quotation.getPaymentStatus'},
			{'name': 'tmpl', 'value': 'ajax'},
			{'name': 'type', 'value': 'raw'}
		];
		jQuery.ajax({
			type: "POST",
			url: "index.php",
			data: params,
			dataType: "json",
			beforeSend: function() {
				jQuery(".payment-column-status").html("");
				jQuery(".payment-column-time").html("");
				jQuery(".payment-column-amount").html("");
				jQuery(".payment-column-card-no").html("");
				jQuery(".payment-column-card-type").html("");
				jQuery(".payment-column-card-name").html("");
				jQuery(".payment-column-ip-address").html("");
			},
			success: function(result){
				console.log(result);
				jQuery(".payment-column-status").html(result.status);
				jQuery(".payment-column-time").html(result.time);
				jQuery(".payment-column-amount").html(result.amt);
				jQuery(".payment-column-card-no").html(result.cardNo);
				jQuery(".payment-column-card-type").html(result.cardType);
				jQuery(".payment-column-card-name").html(result.cardName);
				jQuery(".payment-column-ip-address").html(result.ipAddress);
				
				jQuery( "#dialog" ).dialog( "open" );
			},
			error: function(xhr, textStatus, errorThrown){
				jQuery(".payment-column-status").html("");
				jQuery(".payment-column-time").html("");
				jQuery(".payment-column-amount").html("");
				jQuery(".payment-column-card-no").html("");
				jQuery(".payment-column-card-type").html("");
				jQuery(".payment-column-card-name").html("");
				jQuery(".payment-column-ip-address").html("");
				
				alert('request failed : '+errorThrown);
			}
		});
	});
</script>
