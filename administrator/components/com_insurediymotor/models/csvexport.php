<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_insurediytravel
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

use Carbon\Carbon;
use Models\Eloquent\Quote;
use Services\Client\OptionalArray;

defined('_JEXEC') or die;

/**
 * Insurediy Motor model.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_insurediymotor
 * @since       1.5
 */
class InsureDIYMotorModelCSVExport extends JModelAdmin {

	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_INSUREDIYMOTOR';
	protected $_tb_quotation = "#__insure_motor_quotations";
	protected $_tb_quotation_plan = "#__insure_motor_quotations_to_plans";
	protected $_tb_traveller = "#__insure_motor_quotations_to_drivers";

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record) {
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return;
			}
			$user = JFactory::getUser();

			if ($record->catid) {
				return $user->authorise('core.delete', 'com_insurediymotor.category.' . (int) $record->catid);
			} else {
				return parent::canDelete($record);
			}
		}
	}

	/**
	 * Method to test whether a record can have its state changed.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to change the state of the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canEditState($record) {
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_insurediymotor.category.' . (int) $record->catid);
		} else {
			return parent::canEditState($record);
		}
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   type	The table type to instantiate
	 * @param   string	A prefix for the table class name. Optional.
	 * @param   array  Configuration array for model. Optional.
	 * @return  JTable	A database object
	 * @since   1.6
	 */
	public function getTable($type = 'Plan', $prefix = 'InsureDIYMotorTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true) {
		// Get the form.
		$form = $this->loadForm('com_insurediymotor.csvexport', 'csvexport', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}
		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 * @since   1.6
	 */
	protected function loadFormData() {
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_insurediymotor.edit.plan.data', array());

		if (empty($data)) {
			$data = $this->getItem();
			if ($this->getState('plan.id') == 0) {

			}
		}

		$this->preprocessData('com_insurediymotor.plan', $data);
		return $data;
	}

	public function getItem($pk = null) {
		if ($item = parent::getItem($pk)) {

			if (!empty($item->id)) {

			}
		}

		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since   1.6
	 */
	protected function prepareTable($table) {

	}

	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param   object	A record object.
	 * @return  array  An array of conditions to add to add to ordering queries.
	 * @since   1.6
	 */
	protected function getReorderConditions($table) {
		$condition = array();

		return $condition;
	}

	/**
	 * Method to save the form data.
	 *
	 * @param   array  $data  The form data.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since	3.1
	 */
	public function save($data) {
		$jinput = JFactory::getApplication()->input->getArray($_POST);
		$data['id'] = $jinput['jform']['id'];

		$return = parent::save($data);

		return $return;
	}

	public function export_csv() {
		ini_set('memory_limit', '-1'); //**************IMPORTANT

		$app = JFactory::getApplication();
		$db = JFactory::getDBO();

		$jform = $app->input->post->get('jform', '', 'filter');

		$datatype = $jform['export_type'];
		set_time_limit(0);
		
		$items = Quote::when($datatype == 'pending_quotations', function($query) {
				$query->pending();
			})
			->when($datatype == 'completed_quotations', function($query) {
				$query->completed();
			})
			->when($jform['date_start'] && !$jform['date_end'], function($query) use ($jform) {
				$query->whereDate('updated_at', Carbon::createFromFormat('d-m-Y', $jform['date_start'])->format('Y-m-d'));
			})
			->when($jform['date_start'] && $jform['date_end'], function($query) use ($jform) {
				$query->whereDate('updated_at', '>=', Carbon::createFromFormat('d-m-Y', $jform['date_start'])->format('Y-m-d'))
					->whereDate('updated_at', '<=', Carbon::createFromFormat('d-m-Y', $jform['date_end'])->format('Y-m-d'));
			})
			->with([
				'user',
				'details',
				'body_type',
				'drivers',
				'drivers.occupation',
				'occupation',
				'loan_company',
				'provider',
				'previous_provider',
			])
			->get();
		
		$resultArr = array();
		
		if (count($items) == 0) {
			echo 'No Items';
		} else {
			foreach ($items as $key => $row) {
				$mainDriver = new OptionalArray($row->drivers->first() ? $row->drivers->first() : []);
				$quoteDataArr = array(
					"Quotation Id" => $row['id'],
					"Username" => $row->user ? $row->user->username : '-', 
					"Account Email" => $row->user ? $row->user->email : '-',
					"Terms Agreed" => 'Yes', 
					"Privacy Agreed" => 'Yes', 
					"Remarketing Agreed" => 'Yes', 
					"Declaration Agreed" => 'Yes', 
					"Last Updated Time" => (string) $row['updated_at'],
					"Quote Created Time" => (string) $row['created_at'],
					"Selected Car Variant" => $row->model ? "{$row->model->name} ({$row->model->make->name})" : '-', 
					"Promo Code" => $row->details->filter(function($detail) {
							return $detail->code !== null;
						})
						->pluck('code')
						->implode(', '), 
					"Process Stage" => $row->quote_stage, 
					"Policy Start Date" => (string) $row->policy_start_date, 
					"Payment Transaction No." => $row['payment_txn_id'],
					"Payment Steps" => $row['payment_stage'], 
					"Payment Request Id" => $row['payment_request_id'], 
					"Payment Amount" => $row['payment_amount'], 
					"Quote Deleted Time" => $row['deleted_at'], 
					"Car Chassis No." => $row['car_chassis_no'],
					"Car Engine No." => $row['car_engine_no'],
					"Car Make" => $row->model ? $row->model->make->name : '-',
					"Car Manufacture Year" => $row['car_make_year'],
					"Body Type" => $row->body_type ? $row->body_type->name : '-',
					"Car Model" => $row->model ? $row->model->name : '-',
					"Engine CC" => $row['car_engine_cc'],
					"Estimated Car Value" => $row['car_market_value'],
					"NCD" => $row['ncd_rate'],
					"Past Claims" => $row['has_past_claims'] ? 'Yes' : 'No',
					"Car Damage" => $row['car_damage'],
					"Theft" => $row['car_theft'],
					"Third Party Claim" => $row['car_tpclaim'],
					"Theft" => $row['car_theft'],
					"Cover Type" => $row->cover_type_fmt,
					"Car Registration No." => $row['car_reg_no'], 
					"Car Address" => $row['car_address'], 
					"Driver Address" => $mainDriver['address_one'] . ', ' . $mainDriver['address_one'],
					"Driver Contact" => $mainDriver['contact'], 
					"Current Insurer" => $row->previous_insurer ? $row->previous_insurer->name : '-', 
					"Driver Date Of Birth" => (string) $mainDriver['dob'],
					"Driver Email" => $mainDriver['email'], 
					"Driver Driving Experience" => $mainDriver['exp'], 
					"Driver Name" => $row->drivers->pluck('name')->implode(', '), 
					"Driver Gender" => $mainDriver['gender'], 
					"Driver NRIC" => $row->drivers->pluck('nric')->implode(', '),
					"Driver Occupation" => $row->drivers->map(function($driver) {
						return $driver->occupation ? $driver->occupation->name : '-';
					})->implode(', '),
					"Driver Occupation Type" => $mainDriver['occ_nature'] == "INDO" ? "Indoor" : "Outdoor",
					"Driver Race" => $mainDriver['race'], 
					"Driver Marital Status" => $mainDriver['marital_status'], 
					"Is Car under Loan" => $row->car_loan ? 'Yes' : 'No', 
					"Loan Company" => $row->car_loan ? $row->loan_company->mortgagee_name : '',
					"Plan Name" => $row->provider ? $row->provider->name : '-',
					"Premium Total" => $row->value,
				);

				// $docsArr = array(
				// 		"Car Registration File" => $row['quote_file_vehicle_reg'],
				// 		"Driving Lincense File" => $row['quote_file_driving_license'],
				// 		"HKID File" => $row['quote_file_hkid'],
				// 		"Renewal Notice File" => $row['quote_file_renewal_notice'],
				// 		"NCD Proof File" => $row['quote_file_ncd'],
				// 		"Copy of Named Driver 1 Driving License" => $row['quote_file_vehicle_reg_2'],
				// 		"Copy of Named Driver 1 HKID Card" => $row['quote_file_hkid_2'],
				// 		"Copy of Named Driver 2 Driving License" => $row['quote_file_vehicle_reg_3'],
				// 		"Copy of Named Driver 2 HKID Card" => $row['quote_file_hkid_3']
				// );
				
				array_push($resultArr, $quoteDataArr);
			}

			unset($items);

			// column names
			$keys = array_keys($resultArr[0]);
			
			// remove id
			// if (($check = array_search("id", $keys)) !== false) {
			// 	unset($keys[$check]);
			// }
			
			$headers = $keys;
			
			ob_start();

			$df = fopen("php://output", 'w');

			fputcsv($df, $headers);

			foreach ($resultArr as $row) {
				fputcsv($df, $row);
			}
			fclose($df);
		}


		// disable caching
		$filename = 'motor_' . $datatype . '_' . JHtml::date(NULL, 'EXPORT_DATE_FORMAT') . '.csv';

		$now = gmdate("D, d M Y H:i:s");
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
		header("Last-Modified: {$now} GMT");

		// force download
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");

		// disposition / encoding on response body
		header("Content-Disposition: attachment;filename={$filename}");
		header("Content-Transfer-Encoding: binary");

		return true;
	}

	public function exportQuotations() {
		$csvData = $this->getCSVData();
		$csvTitle = JHtml::date('now', "d_M_Y_H_i_s") . "_space_bookings.csv";
		MyHelper::fileDownload($csvTitle, $csvData);
		return true;
	}

	public function getCSVData($completedOnly, $pendingOnly) { // this is nasty. need to fix later.
		$items = $this->getItems($completedOnly, $pendingOnly);
		$qids = array_keys($items);
		$temps = $this->getDrivers($qids);
		$travellers = array();
		foreach ($temps as $temp) {
			$travellers[$temp['quotation_id']][] = $temp;
		}

		foreach ($travellers as $key => $q_travellers) {
			if (isset($items[$key])) {
				foreach ($q_travellers as $qkey => $q_traveller) {
					foreach (array_keys($q_traveller) as $tkey) {
						if ($tkey != "traveller_id" && $tkey != "quotation_id") {
							$field_key = $tkey . "_" . ($qkey + 1);
							$items[$key][$field_key] = $q_traveller[$tkey];
						}
					}
				}
			}
		}
		$headers = array();
		foreach ($items as $item) {
			if (count(array_keys($item)) > count(array_keys($headers))) {
				$headers = array_keys($item);
			}
		}

		$csv = array();
		foreach ($items as $item) {
			$tmp = array();
			foreach ($item as $k => $v) {
				$tmp[] = $v;
			}
			$csv[] = implode(',', MyHelper::escapeCSV($tmp, TRUE));
		}

		$csvMasterData = array(
			implode(',', MyHelper::escapeCSV($headers, TRUE)), //csv header
			implode("\n", $csv) //csv items
		);
		return (!empty($csvMasterData)) ? implode("\n", $csvMasterData) : '';
	}

	public function getItems($completedOnly, $pendingOnly) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("q.*, qp.insurer_code, qp.plan_name, qp.premium")
				->from($this->_tb_quotation . " AS q")
				->leftjoin($this->_tb_quotation_plan . " AS qp ON q.id = qp.quotation_id")
				->order("q.id", "ASC");
		if ($completedOnly) {
			$query->where("q.quote_status = 1");
		}
		if ($pendingOnly) {
			$query->where("q.quote_status = 0");
		}
		return $db->setQuery($query)->loadAssocList("id");
	}

	public function getDrivers($quotations) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_traveller)
				->where("quotation_id IN (" . implode(",", $quotations) . ")");
		return $db->setQuery($query)->loadAssocList();
	}

}
