<?php

use Services\Bugsnag;
use Services\BootEloquent;
/**
 * @package     Joomla.Administrator
 * @subpackage  com_weblinks
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once JPATH_COMPONENT_SITE . '/vendor/autoload.php';

$bugsnag = new Bugsnag;
$bugsnag->register();

$bootEloquent = new BootEloquent;
$bootEloquent->run();

global $reqOption, $reqView, $reqLayout, $helper;

$reqOption = 'com_insurediymotor';
$reqView = JRequest::getCmd('view', 'quotations');
$reqLayout = JRequest::getCmd('layout', 'default');

if (!JFactory::getUser()->authorise('core.manage', 'com_insurediymotor'))
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

JLoader::register('InsureDIYMotorHelper', __DIR__ . '/helpers/insurediymotor.php');

$controller	= JControllerLegacy::getInstance('InsureDIYMotor');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
