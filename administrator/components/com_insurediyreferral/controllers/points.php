<?php

/**
 * @package     Joomla.Administrator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
require_once JPATH_COMPONENT . '/controller.php';

/**
 * Weblink controller class.
 *
 * @package     Joomla.Administrator
 * @since       1.6
 */
class InsurediyReferralControllerPoints extends InsurediyReferralController {

	/**
	 * Method override to check if you can add a new record.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	protected function allowAdd($data = array()) {
		$user = JFactory::getUser();
		//$categoryId = JArrayHelper::getValue($data, 'catid', $this->input->getInt('filter_category_id'), 'int');
		$allow = null;

		/* if ($categoryId)
		  {
		  // If the category has been passed in the URL check it.
		  $allow = $user->authorise('core.create', $this->option . '.category.' . $categoryId);
		  }
		 */

		if ($allow === null) {
			// In the absense of better information, revert to the component permissions.
			return parent::allowAdd($data);
		} else {
			return $allow;
		}
	}

	/**
	 * Method to check if you can add a new record.
	 *
	 * @param   array   $data  An array of input data.
	 * @param   string  $key   The name of the key for the primary key.
	 *
	 * @return  boolean
	 * @since   1.6
	 */
	protected function allowEdit($data = array(), $key = 'id') {
		$recordId = (int) isset($data[$key]) ? $data[$key] : 0;
		$categoryId = 0;
		if ($recordId) {
			$categoryId = (int) $this->getModel()->getItem($recordId)->catid;
		}

		if ($categoryId) {
			// The category has been set. Check the category permissions.
			return JFactory::getUser()->authorise('core.edit', $this->option . '.category.' . $categoryId);
		} else {
			// Since there is no asset tracking, revert to the component permissions.
			return parent::allowEdit($data, $key);
		}
	}

	function publish() {
		JRequest::checkToken() or jexit('Invalid Token');
		$model = $this->getModel('points');
		$model->publish();
	}

	function unpublish() {
		JRequest::checkToken() or jexit('Invalid Token');
		$model = $this->getModel('points');
		$model->unpublish();
	}

	function add() {
		MyUri::redirect("index.php?option=com_insurediyreferral&view=point");
	}

	public function restore() {
		JRequest::checkToken() or jexit('Invalid Token');
		$model = $this->getModel('points');
		$model->restore();
	}

	public function trash() {
		JRequest::checkToken() or jexit('Invalid Token');
		$model = $this->getModel('points');
		$model->trash();
	}

	public function delete() {
		JRequest::checkToken() or jexit('Invalid Token');
		$model = $this->getModel('points');
		$model->delete();
	}

	public function edit() {
		JRequest::checkToken() or jexit('Invalid Token');
		$app = JFactory::getApplication();
		$post = $app->input->get('cid', '', 'array');
		if (!empty($post)) {
			MyUri::redirect("index.php?option=com_insurediyreferral&view=point&layout=edit&point_id=" . $post[0]);
		}
	}

	public function updateStatus() {
		$params = JComponentHelper::getParams("COM_INSUREDIYREFERRAL");
		$autoApprove = $params->get("auto_approve", TRUE);
		if ($autoApprove) {
			$days = $params->get("day_to_approve", 10);
			$db = JFactory::getDBO();
			$query = $db->getQuery(TRUE)
					->update("#__insure_points")
					->set("status = 'A'")
					->where("status = 'P'")
					->where("DATEDIFF(NOW(), created) > " . $days);
			$db->setQuery($query)->execute();
		}


		exit;
	}

}
