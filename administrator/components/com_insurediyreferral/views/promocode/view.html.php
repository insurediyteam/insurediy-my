<?php

/**
 * @package     Joomla.Administrator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * View to edit a weblink.
 *
 * @package     Joomla.Administrator
 * @since       1.5
 */
class InsurediyReferralViewPromocode extends JViewLegacy {

	protected $state;
	protected $item;
	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->item = $this->get('Item');
		$this->form = $this->get('Form');

		// Check for errors.
		if (empty($this->item)) {
			JError::raiseError(500, "Item not found");
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		$app = JFactory::getApplication();
		$app->input->set('hidemainmenu', true);

		$user = JFactory::getUser();
		$userId = $user->get('id');
		$isNew = ($this->item->id == 0);
		if ($isNew) {
			JToolbarHelper::title(JText::_('COM_INSUREDIYREFERRAL_PROMOCODE_NEW_TITLE'));
		} else {
			JToolbarHelper::title(JText::_('COM_INSUREDIYREFERRAL_PROMOCODE_EDIT_TITLE'));
		}

		JToolbarHelper::apply('promocode.apply');
		JToolbarHelper::save('promocode.save');
		JToolbarHelper::cancel('promocode.cancel');
	}

}
