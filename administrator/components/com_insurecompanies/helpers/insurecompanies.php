<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Transactions helper.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 * @since       1.6
 */
class InsureCompaniesHelper
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param   string	The name of the active view.
	 * @since   1.6
	 */
	 
	public static function addSubmenu($vName = 'companies')
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_INSURECOMPANIES_SUBMENU_COMPANIES'),
			'index.php?option=com_insurecompanies&view=companies',
			$vName == 'companies'
		);
		
		/*JHtmlSidebar::addEntry(
			JText::_('COM_CEDELEDEPOTS_SUBMENU_CATEGORIES'),
			'index.php?option=com_categories&extension=com_cedeledepots',
			$vName == 'categories'
		);
		*/
		/*if ($vName == 'categories')
		{
			JToolbarHelper::title(
				JText::sprintf('COM_CATEGORIES_CATEGORIES_TITLE', JText::_('COM_CEDELEDEPOTS')),
				'cedeledepots-categories');
		}
		*/
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @param   integer  The category ID.
	 * @return  JObject
	 * @since   1.6
	 */
	public static function getActions($categoryId = 0)
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		if (empty($categoryId))
		{
			$assetName = 'com_insurecompanies';
			$level = 'component';
		}
		else
		{
			$assetName = 'com_insurecompanies.category.'.(int) $categoryId;
			$level = 'category';
		}

		$actions = JAccess::getActions('com_insurecompanies', $level);

		foreach ($actions as $action)
		{
			$result->set($action->name,	$user->authorise($action->name, $assetName));
		}

		return $result;
	}
	
	public static function getInsuranceCompanyOptions()
	{
		$db = JFactory::getDBO();
	
		$options = array();

		$query = " SELECT * FROM #__insure_companies ";
			$db->setQuery( $query );
			$rows = $db->loadObjectList();
		
		foreach($rows as $r) :
			$options[] = JHtml::_('select.option', $r->insurer_code, $r->company_name);
		endforeach;		
			
		return $options;
	}
	
}
