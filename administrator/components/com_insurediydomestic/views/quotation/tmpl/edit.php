<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cedeledepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('MyBehavior.jsInsurediy');


$item = $this->item;
$form =& $this->form;
$activetab = JFactory::getApplication()->input->get("activetab", "info_details");
$selected_plan = $item->selected_plan;
$maids = $this->maids;
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'quotation.cancel' || document.formvalidator.isValid(document.id('quotation-form')))
		{
			Joomla.submitform(task, document.getElementById('quotation-form'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_insurediydomestic'); ?>" method="post" name="adminForm" id="quotation-form" class="form-validate">
	<div class="row-fluid">
		<div class="span11 form-horizontal">
			<fieldset>
				<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => $activetab)); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'info_details', JText::_('COM_INSUREDIYDOMESTIC_QUOTATION_BASIC_INFORMATION')); ?>
				<?php foreach ($this->form->getFieldset("quote-details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'contact', JText::_('COM_INSUREDIYDOMESTIC_QUOTATION_CONTACT')); ?>
				<?php foreach ($this->form->getFieldset("contact-details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'transaction_logs', JText::_('COM_INSUREDIYDOMESTIC_QUOTATION_TRANSACTION_LOGS')); ?>
				<?php foreach ($this->form->getFieldset("transaction-details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'plan', JText::_('COM_INSUREDIYDOMESTIC_QUOTATION_PLAN_SELECTED')); ?>
				<?php foreach ($this->form->getFieldset("plan-details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
				
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'maids', JText::_('COM_INSUREDIYDOMESTIC_QUOTATION_MAID')); ?>
				<div class="span8">
					<?php if (count($maids) > 0): ?>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th><?php echo JText::_("COM_INSUREDIYDOMESTIC_FIRSTNAME_LABEL"); ?></th>
									<th><?php echo JText::_("COM_INSUREDIYDOMESTIC_LASTNAME_LABEL"); ?></th>
									<th><?php echo JText::_("COM_INSUREDIYDOMESTIC_GENDER_LABEL"); ?></th>
									<th><?php echo JText::_("COM_INSUREDIYDOMESTIC_DOB_LABEL"); ?></th>
									<th><?php echo JText::_("COM_INSUREDIYDOMESTIC_NATIONALITY_LABEL"); ?></th>
									<th><?php echo JText::_("COM_INSUREDIYDOMESTIC_MARITAL_STATUS_LABEL"); ?></th>
									<th><?php echo JText::_("COM_INSUREDIYDOMESTIC_ID_NO_LABEL"); ?></th>
									<th><?php echo JText::_("COM_INSUREDIYDOMESTIC_KIN_NAME_LABEL"); ?></th>
									<th><?php echo JText::_("COM_INSUREDIYDOMESTIC_KIN_RELATION_LABEL"); ?></th>
								</tr>
							</thead>
							<?php foreach ($maids as $k => $maid): ?>
								<tr>
									<td>
										<?php echo $k + 1 ?>
									</td>
									<td>
										<?php echo $maid['firstname']; ?>
									</td>
									<td>
										<?php echo $maid['lastname']; ?>
									</td>
									<td>
										<?php echo $maid['gender']; ?>
									</td>
									<td>
										<?php echo $maid['dob']; ?>
									</td>
									<td>
										<?php echo $maid['nationality']; ?>
									</td>
									<td>
										<?php echo $maid['marital_status']; ?>
									</td>
									<td>
										<?php echo $maid['id_no']; ?>
									</td>
									<td>
										<?php echo $maid['kin_name']; ?>
									</td>
									<td>
										<?php echo $maid['kin_relation']; ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</table>
					<?php else: ?>
						<?php echo "No maid entry"; ?>
					<?php endif; ?>
				</div>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'files', JText::_('COM_INSUREDIYDOMESTIC_QUOTATION_FILES')); ?>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('file_passport'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('file_passport'); ?></div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('file_visa_pass'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('file_visa_pass'); ?></div>
				</div>
				
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('file_temporary_pass'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('file_temporary_pass'); ?></div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('file_employers_passport'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('file_employers_passport'); ?></div>
				</div>

				<?php echo JHtml::_('bootstrap.endTab'); ?>
				
				<?php echo JHtml::_('bootstrap.endTabSet'); ?>
			</fieldset>
			<input type="hidden" name="task" value="" />
				<?php echo JHtml::_('form.token'); ?>
		</div>
	</div>
</form>

<div id="deleteModel" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3><?php echo JText::_("COM_INSUREDIYDOMESTIC_WARNING_HEADER"); ?></h3>
	</div>
	<div class="modal-body">
		<?php echo JText::_("COM_INSUREDIYDOMESTIC_EXISTING_INSURANCES_DELETE_WARNING"); ?>
	</div>
	<div class="modal-footer center">
		<form style="margin: 0;" action="<?php echo JRoute::_('index.php?option=com_insurediydomestic'); ?>" method="post" name="deleteForm" id="deleteForm">
			<input type="hidden" id="task" name="task" value="" />
			<input type="hidden" id="element_id" name="element_id" value="" />
			<input type="hidden" name="quotation_id" value="<?php echo $item->id; ?>" />
			<?php echo JHtml::_('form.token'); ?>
			<a href="#" data-dismiss="modal" class="btn btn-small"><?php echo JText::_("COM_INSUREDIYDOMESTIC_BUTTON_CANCEL"); ?></a>
			<button type="submit" class="btn btn-small btn-danger"><?php echo JText::_("COM_INSUREDIYDOMESTIC_BUTTON_DELETE"); ?></button>
		</form>
	</div>
</div>
